-- -*- lua -*-
-- Date: 23/08/2021
-- Author : Maxime KERMARQUER
--
--

-- Module description
whatis([[Name : ansible]])
whatis([[Version : 4.4.0]])
whatis([[Short description : ]])
whatis([[Configure options : pip install ansible ]])
help([[ ]])

-- Set up environment variables
prepend_path("PATH", "/network/lustre/iss01/apps/tools/ansible/miniconda3/bin", ":")
--setenv("ANSIBLE_CONFIG", "/network/lustre/iss01/dsi/deployment/ansible/config/ansible.cfg")
setenv("ANSIBLE_NOCOWS", "1")

