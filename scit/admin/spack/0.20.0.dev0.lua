-- -*- lua -*-
-- Date: 28/04/2023
-- Author : Maxime KERMARQUER
--

-- Module description
whatis([[Name : spack ]])
whatis([[Version : 0.20.0.dev0 ]])
whatis([[Short description : Spack is a package manager for supercomputers, Linux, and macOS. It makes installing scientific software easy. Spack isn’t tied to a particular language; you can build a software stack in Python or R, link to libraries written in C, C++, or Fortran, and easily swap compilers or target specific microarchitectures.  ]])
whatis([[Configure options : git clone -c feature.manyFiles=true https://github.com/spack/spack.git spack/0.20.0.dev0 ]])
help([[ https://spack.readthedocs.io/en/latest/getting_started.html#installation ]])
help([[ https://spack.io/ ]])


-- Set up environment variables
depends_on("proxy")
prepend_path("PATH", "/network/lustre/iss01/apps/lang/miniconda/3/spack-deps/bin", ":")
prepend_path("PATH", "/network/lustre/iss01/apps/software/spack/0.20.0.dev0/bin", ":")
setenv("SPACK_ROOT", "/network/lustre/iss01/apps/software/spack/0.20.0.dev0")
prepend_path("MODULEPATH", "/network/lustre/iss01/apps/modules/spack", ":")

