-- -*- lua -*-
-- Date: 20/11/2019
-- Author : Maxime KERMARQUER
--

-- Module description
whatis([[Name : BBMap]])
whatis([[Version : 38.73]])
whatis([[Short description : BBMap is a splice-aware global aligner for DNA and RNA sequencing reads.]])


depends_on("java")

-- Set up environment variables
prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/bbmap/38.73", ":")
