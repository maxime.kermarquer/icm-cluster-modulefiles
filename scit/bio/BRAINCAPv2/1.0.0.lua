-- 30/08/2018
whatis("Version: 2.7")
whatis("Keywords: Python")
whatis("URL: http://anaconda.org")
whatis("Description: Anaconda environment for ATACseq pipeline")


if(not isloaded("latex/3.14159265")) then
    load("latex/3.14159265")
end

depends_on("vcftools/0.1.13")
--if(not isloaded("mariadb")) then
--    load("mariadb")
--end

prepend_path("PATH","/network/lustre/iss01/apps/lang/anaconda/3/5.3.0/BRAINCAPv2/bin")
execute {cmd="source activate BRAINCAPv2",modeA={"load"}} 
