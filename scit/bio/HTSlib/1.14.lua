-- -*- lua -*-
-- Date   : 2022-02-16
-- Author : Martial Bornet
--
--

-- Module description
whatis([[Name : HTSlib]])
whatis([[Version : 1.14]])
whatis([[Short description : HTSlib is an implementation of a unified C library for accessing common file formats, such as SAM, CRAM and VCF]])
whatis([[Build with : gcc/7.3.0 ]])



-- For module dependencies
depends_on("gcc/7.3.0")
depends_on("zlib/1.2.11-64vg6e4")
depends_on("libbz2/1.0.8")
depends_on("lzma/5.2.4")
depends_on("ncurses/6.1-ree7wyp")
depends_on("curl/7.60.0-nqidbsk")


-- Set up environment variables
prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/HTSlib/1.14", ":")
prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/HTSlib/1.14", ":")
prepend_path("CPATH", "/network/lustre/iss01/apps/software/scit/HTSlib/1.14", ":")
prepend_path("LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/HTSlib/1.14", ":")
