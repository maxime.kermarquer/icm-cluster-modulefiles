-- -*- lua -*-
-- Date: 03/10/2018
-- Author : Maxime KERMARQUER
--
--

-- Module description
whatis([[Name : MapSplice]])
whatis([[Version : 2.1.8]])
whatis([[Short description : MapSplice is a software for mapping RNA-seq data to reference genome for splice junction discovery that depends only on reference genome, and not on any further annotations.]])



-- For module dependencies
--if(not isloaded("")) then
--        load("modulefile")
--end
depends_on("bowtie/0.12.7")
depends_on("ncurses/6.1-ree7wyp")
depends_on("python/2.7")

-- Set up environment variables
prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/MapSplice/2.1.8/bin", ":")
prepend_path("MAPSPLICE_DIR", "/network/lustre/iss01/apps/software/scit/MapSplice/2.1.8", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/MapSplice/2.1.8", ":")

