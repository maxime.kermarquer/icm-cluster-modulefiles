-- -*- lua -*-
-- Date: 04/10/2018
-- Author : Maxime KERMARQUER
--

-- Module description
whatis([[Name : PHAT]])
whatis([[Version : 0.9.1-beta.1]])
whatis([[Short description : The Pathogen-Host Analysis Tool (PHAT) is an application for processing and analyzing next-generation sequencing (NGS) data as it relates to relationships between pathogen and host organisms.]])
whatis([[Sources : https://github.com/chgibb/PHAT/releases/download/0.9.1-beta.1/phat-linux-x64-portable.tar.gz]])


-- For module dependencies
depends_on("libxss1/1.2")

-- Set up environment variables
prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/PHAT/0.9.1-beta.1", ":")
