-- -*- lua -*-
-- Date: 16/10/2018
-- Author : Maxime KERMARQUER
--

-- Module description
whatis([[Name : RADIA]])
whatis([[Version : 1.1.3]])
whatis([[Short description : RADIA: RNA and DNA Integrated Analysis for Somatic Mutation Detection]])
whatis([[Configure options : ]])



depends_on("python/2.7")
depends_on("samtools")
depends_on("blat")
depends_on("snpEff")
depends_on("java")

-- Set up environment variables
prepend_path("RADIA_DIR", "/network/lustre/iss01/apps/software/scit/RADIA/1.1.3", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/RADIA/1.1.3", ":")

