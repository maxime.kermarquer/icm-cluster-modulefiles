-- 30/08/2018
whatis("Version: 2.7")
whatis("Keywords: Python")
whatis("URL: http://anaconda.org")
whatis("Description: Anaconda environment for RNAseq pipeline")


if(not isloaded("latex/3.14159265")) then
    load("latex/3.14159265")
end

depends_on("git")
depends_on("proxy")

--execute {cmd="source deactivate deformetrica",modeA={"unload"}} 
prepend_path("PATH","/network/lustre/iss01/apps/lang/anaconda/2/5.2.0/RNAseq/bin")
execute {cmd="source activate RNAseq",modeA={"load"}} 
