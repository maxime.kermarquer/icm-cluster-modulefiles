-- -*- lua -*-
-- Date: 24/03/2022
-- Author : Maxime KERMARQUER
--

-- Module description
whatis([[Name : SICILIAN ]])
whatis([[Version : commit 0b5644ad1f45849be8ce1f1669a22c4d9ed326ba - 2022/01/09 ]])
whatis([[Short description : ]])


depends_on("STAR/2.7.5a")
depends_on("R/3.6.2")

-- Set up environment variables
prepend_path("PATH", "/network/lustre/iss01/apps/lang/anaconda/3/2021.05/sicilian/commit-0b5644a/bin", ":")
setenv("SICILIAN_REFS","/network/lustre/iss01/apps/software/scit/sicilian/commit-0b5644ad1f45849be8ce1f1669a22c4d9ed326ba/Genome_data")
--prepend_path("LD_LIBRARY_PATH", "lib", ":")
--prepend_path("LIBRARY_PATH", "lib", ":")
--prepend_path("CPATH", "include", ":")
--prepend_path("MANPATH", "share", ":")
--prepend_path("CMAKE_PREFIX_PATH", "", ":")

