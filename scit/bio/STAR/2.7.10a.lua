-- -*- lua -*-
-- Last update : 2022-01-21
-- Author      : Martial Bornet
--
--

-- Module description
whatis([[Name : STAR]])
whatis([[Version : 2.7.10a]])
whatis([[Short description : Spliced Transcripts Alignment to a Reference © Alexander Dobin, 2009-2022]])
help([[https://github.com/alexdobin/STAR]])


-- Set up environment variables
prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/STAR/2.7.10a/bin/Linux_x86_64_static", ":")
