-- -*- lua -*-
-- Date: 31/10/2019
-- Author : Maxime KERMARQUER
--
--

-- Module description
whatis([[Name : STAR]])
whatis([[Version : 2.7.3a]])
whatis([[Short description : Spliced Transcripts Alignment to a Reference © Alexander Dobin, 2009-2019]])
help([[https://github.com/alexdobin/STAR]])


-- Set up environment variables
prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/STAR/2.7.3a/bin/Linux_x86_64", ":")

