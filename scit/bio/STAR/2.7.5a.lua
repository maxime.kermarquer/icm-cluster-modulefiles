-- -*- lua -*-
-- Date: 24/03/2022
-- Author : Maxime KERMARQUER
--
--

-- Module description
whatis([[Name : STAR]])
whatis([[Version : 2.7.5c]])
whatis([[Short description : Spliced Transcripts Alignment to a Reference © Alexander Dobin, 2009-2022 https://www.ncbi.nlm.nih.gov/pubmed/23104886]])
help([[https://github.com/alexdobin/STAR]])



-- Set up environment variables
prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/STAR/2.7.5a/bin/Linux_x86_64", ":")

