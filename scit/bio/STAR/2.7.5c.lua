-- -*- lua -*-
-- Date: 24/03/2022
-- Author : Maxime KERMARQUER
--
--

-- Module description
whatis([[Name : STAR]])
whatis([[Version : 2.7.5c]])
whatis([[Short description : Spliced Transcripts Alignment to a Reference © Alexander Dobin, 2009-2022 https://www.ncbi.nlm.nih.gov/pubmed/23104886]])
whatis([[Configure options :
 wget https://github.com/alexdobin/STAR/archive/refs/tags/2.7.5c.tar.gz
 tar -xvf 2.7.5c.tar.gz
 mv 2.7.5c.tar.gz STAR-2.7.5c
 mv STAR-2.7.5c 2.7.5c
 module load gcc/7.3.0
 make -j 28 -C /network/lustre/iss01/apps/software/scit/STAR/2.7.5c/source STAR
 mkdir 2.7.5c/bin/compiled
 mv -v 2.7.5c/source/STAR 2.7.5c/bin/compiled
]])
help([[https://github.com/alexdobin/STAR]])



-- Set up environment variables
prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/STAR/2.7.5c/bin/compiled", ":")

