-- -*- lua -*-
-- Date: 11/07/2019
-- Author : Maxime KERMARQUER
--
--

-- Module description
whatis([[Name : SinQC]])
whatis([[Version : 1.5]])
whatis([[Usage : python $SINQC_PATH/SINQC.py <Parameters> ]])


-- Execute a command
depends_on("R")
depends_on("python/2.7")

-- Set up environment variables
prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/SINQC/1.5", ":")
prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/SINQC/1.5/bin", ":")
prepend_path("SINQC_PATH", "/network/lustre/iss01/apps/software/scit/SINQC/1.5", ":")
