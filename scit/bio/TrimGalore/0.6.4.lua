-- -*- lua -*-
-- Date: 30/10/2019
-- Author : Maxime KERMARQUER
--
--

-- Module description
whatis([[Name : Trim Galore]])
whatis([[Version : 0.6.4]])
whatis([[Short description : Trim Galore is a wrapper around Cutadapt and FastQC to consistently apply adapter and quality trimming to FastQ files, with extra functionality for RRBS data.]])



-- Set up environment variables
prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/TrimGalore/0.6.4", ":")
