-- -*- lua -*-
-- Date:
-- Author : Maxime KERMARQUER
--
--

-- Module description
whatis([[Name : VarScan ]])
whatis([[Version : 2.3.9]])
whatis([[Help : http://dkoboldt.github.io/varscan/using-varscan.html ]])


-- Execute a command
execute {cmd="echo -e '\\nTo use VarScan :\\n\\e[1;33mjava -jar ${VARSCAN_JAR_PATH} [COMMAND] [OPTIONS]\\e[0m\\n'",modeA={"load"}} 


depends_on("java")

-- Set up environment variables
setenv("VARSCAN_JAR_PATH", "/network/lustre/iss01/apps/software/scit/VarScan/VarScan.v2.3.9.jar")

