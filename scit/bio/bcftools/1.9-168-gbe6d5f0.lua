-- -*- lua -*-
-- Date: 15/05/2019
-- Author : Maxime KERMARQUER
--
--

-- Module description
whatis([[Name : bcftools]])
whatis([[Version : 1.9-168-gbe6d5f0]])
whatis([[Short description : BCFtools is a program for variant calling and manipulating files in the Variant Call Format (VCF) and its binary counterpart BCF.]])
whatis([[Configure options : --enable-perl-filters]])
help([[ ]])



depends_on("zlib")
depends_on("bzip2/1.0.6-zfy3jck")
depends_on("perl/5.26.2-lc5lzoi")
depends_on("lzma/5.2.4")

-- Set up environment variables
prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/bcftools/1.9-168-gbe6d5f0/bin", ":")
prepend_path("MANPATH", "/network/lustre/iss01/apps/software/scit/bcftools/1.9-168-gbe6d5f0/share/man", ":")
prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/bcftools/1.9-168-gbe6d5f0/libexec/bcftools", ":")
prepend_path("LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/bcftools/1.9-168-gbe6d5f0/libexec/bcftools", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/bcftools/1.9-168-gbe6d5f0/", ":")

