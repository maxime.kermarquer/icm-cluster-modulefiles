-- -*- lua -*-
-- Date: 30/10/2019
-- Author : Maxime KERMARQUER
--
--

-- Module description
whatis([[Name : bioinformatics-tools]])
whatis([[Version : 1.0.0]])
whatis([[Short description : meta-module to load nextflow, Cutadapt, MultiQC, HTSeq, Trim Galore, kallisto, STAR, STAR-Fusion, samtools ]])

depends_on("nextflow")
depends_on("TrimGalore")
depends_on("kallisto")
depends_on("perl/5.26.2-lc5lzoi")
depends_on("STAR-Fusion")
depends_on("subread")
