-- -*- lua -*-
-- Date: 16/10/2018
-- Author : Maxime KERMARQUER
--
--

-- Module description
whatis([[Name : blat]])
whatis([[Version : 3.5]])
whatis([[Short description : BLAT is a sequence analysis tool which performs rapid mRNA/DNA and cross-species protein alignments. ]])
whatis([[Configure options : module load libpng/1.6.34-nig4xtt]])



-- Set up environment variables
prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/blat/35/bin/x86_64", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/blat/35/bin/x86_64", ":")

