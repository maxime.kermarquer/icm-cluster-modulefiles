-- -*- lua -*-
-- Date: 03/10/2018
-- Author : Maxime KERMARQUER
--
--

-- Module description
whatis([[Name : bowtie]])
whatis([[Version : 0.12.7]])
whatis([[Short description : Bowtie is an ultrafast, memory-efficient short read aligner geared toward quickly aligning large sets of short DNA sequences (reads) to large genomes.]])


-- Set up environment variables
prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/bowtie/bowtie-0.12.7", ":")
prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/bowtie/bowtie-0.12.7/scripts",":")
prepend_path("BOWTIE_DIR", "/network/lustre/iss01/apps/software/scit/bowtie/bowtie-0.12.7", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/bowtie/bowtie-0.12.7", ":")

