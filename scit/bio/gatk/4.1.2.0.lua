-- -*- lua -*-
-- Date: 16/05/2019
-- Fix : 22/02/2023
-- Author : Maxime KERMARQUER
--
--

-- Module description
whatis([[Name : gatk]])
whatis([[Version : 4.1.2.0]])
whatis([[Short description : GATK is a collection of command-line tools for analyzing high-throughput sequencing data with a primary focus on variant discovery.]])

--depends_on("java/8")
depends_on("java/8u251")
depends_on("R/3.5.0")
depends_on("python/3.6")

-- Set up environment variables
prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/gatk/4.1.2.0", ":")
