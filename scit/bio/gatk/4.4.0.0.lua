-- -*- lua -*-
--
-- Author : Martial Bornet
-- Date   : 2023-05-24
--

-- Module description
whatis([[Name : gatk]])
whatis([[Version : 4.4.0.0]])
whatis([[Short description : GATK is a collection of command-line tools for analyzing high-throughput sequencing data with a primary focus on variant discovery.]])

depends_on("java/17.0.7+7")
depends_on("R/3.5.0")
depends_on("python/3.6")

-- Set up environment variables
prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/gatk/4.4.0.0/gatk", ":")
