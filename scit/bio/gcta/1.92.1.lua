-- -*- lua -*-
-- Date: 15/05/2019
-- Author : Maxime KERMARQUER
--
--

-- Module description
whatis([[Name : GCTA]])
whatis([[Version : 1.92.1beta6]])
whatis([[Short description : GCTA (Genome-wide Complex Trait Analysis) was originally designed to estimate the proportion of phenotypic variance explained by all genome-wide SNPs for complex traits (the GREML method), and has subsequently extended for many other analyses to better understand the genetic architecture of complex traits.]])



-- Set up environment variables
prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/gcta/1.92.1beta6", ":")
