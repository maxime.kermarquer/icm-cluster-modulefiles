-- -*- lua -*-
-- --
-- -- gemini modules - 28/08/2018
-- --
--
whatis([[Name : gemini]])
whatis([[Version : 0.20.1]])
whatis([[Short description : GEMINI (GEnome MINIng) is a flexible framework for exploring genetic variation in the context of the wealth of genome annotations available for the human genome.]])
--
prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/gemini/0.20.1/bin")
prepend_path("GEMINI_DATA", "/network/lustre/iss01/apps/software/scit/gemini/0.20.1/data_files")
