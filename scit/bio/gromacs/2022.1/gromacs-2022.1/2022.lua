-- -*- lua -*-
--
-- Author      : Martial Bornet
-- Last update : 2022-05-04
--
--
-- Module description
whatis([[Name: Gromacs ]])
whatis([[Version : 2022.1 ]])
whatis([[Short description : A free and open-source software suite for high-performance molecular dynamics and output analysis ]])

-- Module dependencies
depends_on("proxy/1.0.0")
depends_on("gcc/7.3.0")
depends_on("cmake/3.23.1")

-- Set up environments variables
prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/gromacs/2022.1/gromacs-2022.1/build/bin", ":")
