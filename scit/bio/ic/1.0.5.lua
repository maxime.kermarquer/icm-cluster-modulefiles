-- -*- lua -*-
-- Date:
-- Author : Maxime KERMARQUER
--
--

-- Module description
whatis([[Name : ic]])
whatis([[Version : 1.0.5]])
whatis([[Short description : ic is a set of programs designed to produce a single html page visual summary of one or more imputed data sets from the most common imputation programs. ]])


depends_on("perl/5.26.2-lc5lzoi")
depends_on("libgd")
depends_on("libpng")
depends_on("libjpeg")
depends_on("zlib")
depends_on("vcfparse")

-- Set up environment variables
set_alias ("ic","perl /network/lustre/iss01/apps/software/scit/IC/1.0.5/ic.pl")
