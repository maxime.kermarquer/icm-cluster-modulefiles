-- -*- lua -*-
-- Date: 30/10/2019
-- Author : Maxime KERMARQUER
--
--

-- Module description
whatis([[Name : kallisto]])
whatis([[Version : 0.46.0]])
whatis([[Short description : kallisto is a program for quantifying abundances of transcripts from bulk and single-cell RNA-Seq data, or more generally of target sequences using high-throughput sequencing reads.]])



-- Set up environment variables
prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/kallisto/0.46.0", ":")
