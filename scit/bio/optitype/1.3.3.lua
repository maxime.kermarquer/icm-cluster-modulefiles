-- -*- lua -*-
-- Last update : 2022-03-03
-- Author      : Martial Bornet
--
--

-- Module description
whatis([[Name : OptiType]])
whatis([[Version : 1.3.3]])
whatis([[Short description : Precision HLA typing from next-generation sequencing data]])
help([[https://github.com/FRED-2/OptiType]])

-- depends_on("python/2.7")
-- depends_on("razers/3.4")
depends_on("samtools/1.14")
-- depends_on("hdfs/1.8.15")
depends_on("hdf5/1.10.4")
-- depends_on("cplex/12.5")

-- Set up environment variables
prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/OptiType/1.3.3/anaconda/bin", ":")
