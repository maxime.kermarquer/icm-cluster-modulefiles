-- -*- lua -*-
-- Date: 31/03/2022
-- Author : Maxime KERMARQUER
--

-- Module description
whatis([[Name : OptiType ]])
whatis([[Version : 1.3.1 ]])
whatis([[Short description : OptiType is a novel HLA genotyping algorithm based on integer linear programming, capable of producing accurate 4-digit HLA genotyping predictions from NGS data by simultaneously selecting all major and minor HLA Class I alleles. ]])
help([[ https://github.com/FRED-2/OptiType ]])


-- Module dependencies
depends_on("razers3/3.4.0")
depends_on("samtools/1.2")
depends_on("hdf5/1.8.21")
depends_on("glpk")
depends_on("git")

-- Set up environment variables
prepend_path("PATH", "/network/lustre/iss01/apps/lang/anaconda/2/2019.10/optitype/1.3.1/bin", ":")

