-- -*- lua -*-
-- Date: 15/05/2019
-- Author : Maxime KERMARQUER
--
--

-- Module description
whatis([[Name : plink]])
whatis([[Version : v2.00a2LM]])
whatis([[Short description : PLINK is a free, open-source whole genome association analysis toolset, designed to perform a range of basic, large-scale analyses in a computationally efficient manner. ]])



-- Set up environment variables
prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/plink/v2.00a2LM", ":")
