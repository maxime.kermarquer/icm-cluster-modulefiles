-- -*- lua -*-
-- Date   : 2021-11-23 / Update 2021-12-01
-- Author : Martial Bornet / Maxime KERMARQUER
--
--

-- Module description
whatis([[Name : pVACtools]])
whatis([[Version : 2.0.4]])
whatis([[Short description : pVACtools is a cancer immunotherapy tools suite]])
help([[https://pvactools.readthedocs.io/en/latest/index.html]])


-- Set up environment variables
--prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/pVACtools/2.0.4/bin", ":")
prepend_path("PATH", "/network/lustre/iss01/apps/lang/anaconda/3/2021.11/pvactools/2.0.4/bin", ":")
