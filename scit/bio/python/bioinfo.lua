help([[
...
]])
whatis("https://www.python.org/doc/")
whatis("Keywords: Python 3.6.2")
whatis("Description: Bio-informatique Softwares ")


if(not isloaded("latex/3.14159265")) then
    load("latex/3.14159265")
end
if(not isloaded("ICU/58.2")) then
    load("ICU/58.2")
end
prepend_path( "PATH" ,"/network/lustre/iss01/apps/lang/anaconda/bioinfo/bin")
