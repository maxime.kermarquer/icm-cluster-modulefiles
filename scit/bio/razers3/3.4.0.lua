-- -*- lua -*-
-- Date: 31/03/2022
-- Author : Maxime KERMARQUER
--

-- Module description
whatis([[Name : RazerS 3]])
whatis([[Version : 3.4.0 ]])
whatis([[Short description : razers3 - Faster, fully sensitive read mapping ]])


-- Set up environment variables
prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/razers3/3.4.0/bin", ":")

