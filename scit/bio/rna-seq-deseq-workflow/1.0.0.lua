help([[
For detailed instructions, go to:
       https://anaconda.org

]])
whatis("Version: 3.7")
whatis("Keywords: Python")
whatis("URL: http://anaconda.org")
whatis("Description: Python interperter")

depends_on("STAR")
depends_on("proxy")

prepend_path("PATH","/network/lustre/iss01/apps/lang/anaconda/3/2019.3/rna-seq-deseq-workflow/bin")

execute {cmd="source activate rna-seq-deseq-workflow",modeA={"load"}} 
execute {cmd="conda deactivate",modeA={"unload"}} 
