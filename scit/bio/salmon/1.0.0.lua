-- -*- lua -*-
-- Date: 20/11/2019
-- Author : Maxime KERMARQUER
--

-- Module description
whatis([[Name : Salmon ]])
whatis([[Version : 1.0.0 ]])
whatis([[Short description : Salmon is a wicked-fast program to produce a highly-accurate, transcript-level quantification estimates from RNA-seq data. ]])
whatis([[Configure options : https://github.com/COMBINE-lab/salmon/releases/download/v1.0.0/salmon-1.0.0_linux_x86_64.tar.gz]])

-- Set up environment variables
prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/salmon/1.0.0/bin", ":")
prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/salmon/1.0.0/lib", ":")
prepend_path("LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/salmon/1.0.0/lib", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/salmon/1.0.0", ":")

