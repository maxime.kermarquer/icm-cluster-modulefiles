-- -*- lua -*-
-- Date: 04/10/2018
-- Author : Maxime KERMARQUER
--
--

-- Module description
whatis([[Name : SAMtools]])
whatis([[Version : 0.1.9]])
whatis([[Short description : SAM (Sequence Alignment/Map) is a flexible generic format for storing nucleotide sequence alignment. SAMtools provide efficient utilities on manipulating alignments in the SAM format. ]])
whatis([[Build with : gcc/7.3.0 ]])



-- For module dependencies
depends_on("ncurses/6.1-ree7wyp")


-- Set up environment variables
prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/samtools/0.1.9/bin", ":")

