-- -*- lua -*-
-- Date   : 2022-02-15
-- Author : Martial Bornet
--
--

-- Module description
whatis([[Name : SAMtools]])
whatis([[Version : 1.14]])
whatis([[Short description : SAM (Sequence Alignment/Map) is a flexible generic format for storing nucleotide sequence alignment. SAMtools provide efficient utilities on manipulating alignments in the SAM format. ]])
whatis([[Build with : gcc/7.3.0 ]])



-- For module dependencies
depends_on("gcc/7.3.0")
depends_on("libbz2/1.0.8")
depends_on("HTSlib/1.14")
-- depends_on("zlib/1.2.11-64vg6e4")
-- depends_on("ncurses/6.1-ree7wyp")


-- Set up environment variables
prepend_path("PATH",    "/network/lustre/iss01/apps/software/scit/samtools/1.14/usr/local/bin", ":")
prepend_path("MANPATH", "/network/lustre/iss01/apps/software/scit/samtools/1.14/usr/local/share/man/man1", ":")
