-- -*- lua -*-
-- Date   : 2021-04-07
-- Author : Martial Bornet
--
--

-- Module description
whatis([[Name : Scanpy – Single-Cell Analysis in Python]])
whatis([[Version : 1.7.2]])
whatis([[Short description : Scanpy is a scalable toolkit for analyzing single-cell gene expression data built jointly with anndata. It includes preprocessing, visualization, clustering, trajectory inference and differential expression testing..]])
--whatis("URL: https://scanpy.readthedocs.io/en/latest/index.html")

depends_on("python/3.6")

-- Set up environment variables
prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/scanpy/1.7.2/bin")
