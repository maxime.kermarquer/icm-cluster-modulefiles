-- -*- lua -*-
-- Date:
-- Author : Maxime KERMARQUER
--
--

-- Module description
whatis([[Name : seq2HLA ]])
whatis([[Version : 2.2-2 ]])
whatis([[Short description : seq2HLA - HLA typing from RNA-Seq sequence reads ]])
whatis([[Configure options : anaconda 5.3.0 / conda install -c bioconda seq2hla ]])
help([[ seq2HLA - HLA typing from RNA-Seq sequence reads ]])

-- Set up environment variables
prepend_path("PATH", "/network/lustre/iss01/apps/lang/anaconda/2/5.3.0/seq2hla/2.2/bin", ":")

