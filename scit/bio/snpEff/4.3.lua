-- -*- lua -*-
-- Date: 16/10/2018
-- Author : Maxime KERMARQUER
--

-- Module description
whatis([[Name : SnpEff]])
whatis([[Version : 4.3]])
whatis([[Short description : SnpEff is a variant annotation and effect prediction tool. It annotates and predicts the effects of variants on genes (such as amino acid changes).]])


-- Set up environment variables
prepend_path("SNPEFF_DIR", "/network/lustre/iss01/apps/software/scit/snpEff/4.3", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/snpEff/4.3", ":")

