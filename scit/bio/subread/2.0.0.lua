-- -*- lua -*-
-- Date: 16/01/2020
-- Author : Maxime KERMARQUER
--
--

-- Module description
whatis([[Name : subread ]])
whatis([[Version : 2.0.0 ]])
whatis([[Short description : The Subread package is developed in The Walter and Eliza Hall Institute of Medical Research, Melbourne, Australia. It consists of a suite of programs for processing next-gen sequencing data. ]])
whatis([[Configure options : module load gcc/7.3.0 ]])
help([[ ]])


-- Execute a command
execute {cmd="source deactivate deformetrica",modeA={"unload"}} 

-- Set up environment variables
prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/subread/2.0.0/bin", ":")
prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/subread/2.0.0/bin/utilities", ":")

