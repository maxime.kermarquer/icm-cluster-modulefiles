-- -*- lua -*-
-- Date: 21/02/2020
-- Author : Maxime KERMARQUER
--
--

-- Module description
whatis([[Name : vcf2maf]])
whatis([[Version : 1.16.17]])
help([[ https://github.com/mskcc/vcf2maf ]])

depends_on("vep/95.3")

-- Set up environment variables
setenv("VCF2MAF_PATH", "/network/lustre/iss01/apps/software/scit/vcf2maf/1.6.17", ":")
set_alias("vcf2maf", "perl ${VCF2MAF_PATH}/vcf2maf.pl --vep-path $VEP_PATH --vep-data $VEP_DATA")
set_alias("maf2maf", "perl ${VCF2MAF_PATH}/maf2maf.pl --vep-path $VEP_PATH --vep-data $VEP_DATA")
set_alias("vcf2vcf", "perl ${VCF2MAF_PATH}/vcf2vcf.pl")
set_alias("maf2vcf", "perl ${VCF2MAF_PATH}/maf2vcf.pl")


