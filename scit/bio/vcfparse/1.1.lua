-- -*- lua -*-
-- Date:
-- Author : Maxime KERMARQUER
--
--

-- Module description
whatis([[Name : vcfparse]])
whatis([[Version : 1.1]])
whatis([[Short description : program to extract the first 8 columns from a vcf for the IC.pl program ]])


depends_on("perl/5.26.2-lc5lzoi")

-- Set up environment variables
set_alias ("vcfparse.pl","perl /network/lustre/iss01/apps/software/scit/vcfparse/vcfparse.pl")
