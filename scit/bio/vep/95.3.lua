-- -*- lua -*-
-- Date: 20/02/2020
-- Author : Maxime KERMARQUER
--
--

-- Module description
whatis([[Name : Variant Effect Predictor]])
whatis([[Version : 95.3]])
whatis([[Short description : VEP determines the effect of your variants (SNPs, insertions, deletions, CNVs or structural variants) on genes, transcripts, and protein sequence, as well as regulatory regions.]])

depends_on("perl/5.26.2-lc5lzoi")
depends_on("bzip2")
depends_on("lzma/5.2.4")
depends_on("ncurses/6.1-ree7wyp")
                                                                                   

-- Set up environment variables
prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/ensembl-vep/release-95.3", ":")
prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/ensembl-vep/release-95.3/dependencies/bin", ":")
prepend_path("CPATH", "/network/lustre/iss01/apps/software/scit/ensembl-vep/release-95.3/dependencies/include", ":")
prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/ensembl-vep/release-95.3/dependencies/lib", ":")
prepend_path("MANPATH", "/network/lustre/iss01/apps/software/scit/ensembl-vep/release-95.3/dependencies/share", ":")

setenv("VEP_PATH", "/network/lustre/iss01/apps/software/scit/ensembl-vep/release-95.3")
setenv("VEP_DATA", "/network/lustre/iss01/apps/software/scit/ensembl-vep/references")
setenv("VER", "95")
