-- -*- lua -*-
-- Date: 15/05/2019
-- Author : Maxime KERMARQUER
--
--

-- Module description
whatis([[Name : Variant Effect Predictor]])
whatis([[Version : 0.96]])
whatis([[Short description : VEP determines the effect of your variants (SNPs, insertions, deletions, CNVs or structural variants) on genes, transcripts, and protein sequence, as well as regulatory regions.]])
whatis([[Configure options : --NO_HTSLIB]])

depends_on("perl/5.26.2-lc5lzoi")

-- Set up environment variables
prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/tabix/0.2.6/bin", ":")
prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/ensembl-vep/release-96", ":")
