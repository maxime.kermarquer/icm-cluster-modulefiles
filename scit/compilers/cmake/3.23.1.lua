-- -*- lua -*-
--
--	Author      : Martial Bornet
--	Last update : 2022-05-03
--
--
whatis([[Name : cmake]])
whatis([[Version : 3.23.1]])
whatis([[Short description : A cross-platform, open-source build system. CMake is a family of tools designed to build, test and package software.]])
--
help([[A cross-platform, open-source build system. CMake is a family of tools designed to build, test and package software.]])
--
--
--
prepend_path("PATH", "/network/lustre/iss01/apps/software/linux-centos7-x86_64/gcc/7.3.0/cmake/3.23.1/bin")
-- prepend_path("ACLOCAL_PATH", "/network/lustre/iss01/apps/software/linux-centos7-x86_64/gcc/7.3.0/cmake/3.10.2/jeyewb75k7xx5rghjbyez56jsfu24b6j/share/aclocal")
-- prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/linux-centos7-x86_64/gcc/7.3.0/cmake/3.10.2/jeyewb75k7xx5rghjbyez56jsfu24b6j/")
--
