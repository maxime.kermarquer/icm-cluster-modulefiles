-- -*- lua -*-
-- Date: 10/02/2020
-- Author : Maxime KERMARQUER
--

-- Module description
whatis([[Name : mpich]])
whatis([[Version : 3.3.2]])
whatis([[Short description : MPICH is a high-performance and widely portable implementation of the Message Passing Interface (MPI) standard (MPI-1, MPI-2 and MPI-3).  ]])
whatis([[Configure options : ]])
help([[http://www.mpich.org/]])


-- Set up environment variables
prepend_path("PATH", "/network/lustre/iss01/apps/compilers/mpich/rhel/7/3.3.2/bin", ":")
prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/compilers/mpich/rhel/7/3.3.2/lib", ":")
prepend_path("CPATH", "/network/lustre/iss01/apps/compilers/mpich/rhel/7/3.3.2/include", ":")
prepend_path("MANPATH", "/network/lustre/iss01/apps/compilers/mpich/rhel/7/3.3.2/share", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/compilers/mpich/rhel/7/3.3.2", ":")

