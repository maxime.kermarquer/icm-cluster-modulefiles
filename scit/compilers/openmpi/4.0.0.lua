-- -*- lua -*-
-- Date: 07/01/2019
-- Author : Maxime KERMARQUER
--
--

-- Module description
whatis([[Name : OpenMPI]])
whatis([[Version : 4.0.0]])
whatis([[Short description : ]])
whatis([[Configure options : ]])
help([[ ]])



-- Set up environment variables
prepend_path("PATH", "/network/lustre/iss01/apps/compilers/openmpi/4.0.0/bin", ":")
prepend_path("MANPATH", "/network/lustre/iss01/apps/compilers/openmpi/4.0.0/share/man", ":")
prepend_path("CPATH", "/network/lustre/iss01/apps/compilers/openmpi/4.0.0/include", ":")
prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/compilers/openmpi/4.0.0/lib", ":")
prepend_path("LIBRARY_PATH", "/network/lustre/iss01/apps/compilers/openmpi/4.0.0/lib", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/compilers/openmpi/4.0.0", ":")

