-- -*- lua -*-
-- Date: 03/02/2021
-- Author : Maxime KERMARQUER
--
--

-- Module description
whatis([[Name : OpenMPI]])
whatis([[Version : 4.0.0]])
whatis([[Short description : ]])
whatis([[Configure options : ]])
help([[ ]])



-- Set up environment variables
prepend_path("PATH", "/opt/binaries/openmpi-4.0.0/bin", ":")
prepend_path("MANPATH", "/opt/binaries/openmpi-4.0.0/share/man", ":")
prepend_path("CPATH", "/opt/binaries/openmpi-4.0.0/include", ":")
prepend_path("LD_LIBRARY_PATH", "/opt/binaries/openmpi-4.0.0/lib", ":")
prepend_path("LIBRARY_PATH", "/opt/binaries/openmpi-4.0.0/bin", ":")
prepend_path("CMAKE_PREFIX_PATH", "/opt/binaries/openmpi-4.0.0", ":")

