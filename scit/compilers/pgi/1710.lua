whatis([===[Changes the PGI home directory to linux86-64 17.10 ]===])
setenv("PGI","/network/lustre/iss01/apps/compilers/pgi/2017.1710")
setenv("CC","/network/lustre/iss01/apps/compilers/pgi/2017.1710/linux86-64/17.10/bin/pgcc")
setenv("FC","/network/lustre/iss01/apps/compilers/pgi/2017.1710/linux86-64/17.10/bin/pgfortran")
setenv("F90","/network/lustre/iss01/apps/compilers/pgi/2017.1710/linux86-64/17.10/bin/pgf90")
setenv("F77","/network/lustre/iss01/apps/compilers/pgi/2017.1710/linux86-64/17.10/bin/pgf77")
setenv("CPP","/network/lustre/iss01/apps/compilers/pgi/2017.1710/linux86-64/17.10/bin/pgcc -Mcpp")
setenv("CXX","/network/lustre/iss01/apps/compilers/pgi/2017.1710/linux86-64/17.10/bin/pgc++")
prepend_path{"PATH","/network/lustre/iss01/apps/compilers/pgi/2017.1710/linux86-64/17.10/bin",delim=":",priority="0"}
prepend_path{"MANPATH","/network/lustre/iss01/apps/compilers/pgi/2017.1710/linux86-64/17.10/man",delim=":",priority="0"}
prepend_path{"LD_LIBRARY_PATH","/network/lustre/iss01/apps/compilers/pgi/2017.1710/linux86-64/17.10/lib",delim=":",priority="0"}
conflict("pgi")
