-- R modulefile
-- 17/09/2021
whatis("Name: R")
whatis("Version: 4.1.1")
whatis("Keywords: Tools, statistical computing")
whatis("Description: R is a language and environment for statistical computing and graphics.")
whatis("URL: https://cran.r-project.org/")

--depends_on("libpng-1.6.29-gcc-4.8.5-brjdlhi")
-- Lib PNG

depends_on("openssl")
depends_on("proxy")
depends_on("readline/7.0-56pj7kg")
depends_on("bzip2/1.0.6-zfy3jck")
depends_on("lzma/5.2.4")
depends_on("pcre/8.42-fx4feox")
depends_on("curl/7.60.0-nqidbsk")
depends_on("libjpeg-turbo")
depends_on("zlib")
depends_on("libjpeg")
depends_on("libpng")
depends_on("perl/5.26.2-lc5lzoi")

prepend_path("PATH","/network/lustre/iss01/apps/lang.preprod/r/rcran/4.1.1/bin")
prepend_path("LD_LIBRARY_PATH","/network/lustre/iss01/apps/lang.preprod/r/rcran/4.1.1/lib64")
prepend_path("MANPATH","/network/lustre/iss01/apps/lang.preprod/r/rcran/4.1.1/share")
