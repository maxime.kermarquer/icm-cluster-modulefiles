-- -*- lua -*-
-- Date: 19/11/2018
-- Author : Maxime KERMARQUER
--
--

-- Module description
whatis([[Name : GO]])
whatis([[Version : 1.11.2]])
whatis([[Short description : Go is an open source programming language that makes it easy to build simple, reliable, and efficient software.]])
whatis([[Configure options : ]])
help([[ ]])

gopath = os.getenv("HOME") .. "/go" -- ".." concatenation operator --> create gotpath = $HOME/go

-- Set up environment variables
prepend_path("PATH", "/network/lustre/iss01/apps/lang/go/1.11.2/bin", ":")
prepend_path("GOPATH", gopath, ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/lang/go/1.11.2", ":")

