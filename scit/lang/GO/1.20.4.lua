-- -*- lua -*-
--
-- Author : Martial Bornet
-- Date   : 2023-05-24
--
--

-- Module description
whatis([[Name : GO]])
whatis([[Version : 1.20.4]])
whatis([[Short description : Go is an open source programming language that makes it easy to build simple, reliable, and efficient software.]])
whatis([[Configure options : ]])
whatis("URL: https://go.dev/doc/install")

gopath = os.getenv("HOME") .. "/go" -- ".." concatenation operator --> create gotpath = $HOME/go

-- Set up environment variables
prepend_path("PATH", "/network/lustre/iss01/apps/lang/go/1.20.4/bin", ":")
prepend_path("GOPATH", gopath, ":")
-- prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/lang/go/1.20.4", ":")
