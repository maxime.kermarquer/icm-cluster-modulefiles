-- MATLAB modulefile
-- 19/03/2018
--

whatis("Version: R2016a")
whatis("Keywords: MATLAB")
whatis("URL: https://fr.mathworks.com/products/matlab.html")
whatis("Description: MATLAB combines a desktop environment tuned for iterative analysis and design processes with a programming language that expresses matrix and array mathematics directly.")

setenv("MATLAB_HOME","/network/lustre/iss01/apps/lang/matlab/R2016a")
setenv("LM_LICENSE_FILE","27000@10.10.3.47")
prepend_path("PATH","/network/lustre/iss01/apps/lang/matlab/R2016a/bin")
