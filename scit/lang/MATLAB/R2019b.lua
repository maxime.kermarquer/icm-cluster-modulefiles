-- MATLAB modulefile
-- Date : 29/01/2020
-- Author : Maxime Kermarquer

whatis("Version: R2019b")
whatis("Keywords: MATLAB")
whatis("URL: https://fr.mathworks.com/products/matlab.html")
whatis("Description: MATLAB combines a desktop environment tuned for iterative analysis and design processes with a programming language that expresses matrix and array mathematics directly.")

-- libxtst
prepend_path("MANPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/libxtst-1.2.2-zsdzu74enw425jfdzekxbqk3266n3f4g/share/man", ":")
prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/libxtst-1.2.2-zsdzu74enw425jfdzekxbqk3266n3f4g/lib", ":")
prepend_path("LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/libxtst-1.2.2-zsdzu74enw425jfdzekxbqk3266n3f4g/lib", ":")
prepend_path("CPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/libxtst-1.2.2-zsdzu74enw425jfdzekxbqk3266n3f4g/include", ":")
prepend_path("PKG_CONFIG_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/libxtst-1.2.2-zsdzu74enw425jfdzekxbqk3266n3f4g/lib/pkgconfig", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/libxtst-1.2.2-zsdzu74enw425jfdzekxbqk3266n3f4g/", ":")


-- libXi
prepend_path("MANPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/libxi-1.7.6-oxc5xryb5evzdbn2yfwjb2bl7kwbxmui/share/man", ":")
prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/libxi-1.7.6-oxc5xryb5evzdbn2yfwjb2bl7kwbxmui/lib", ":")
prepend_path("LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/libxi-1.7.6-oxc5xryb5evzdbn2yfwjb2bl7kwbxmui/lib", ":")
prepend_path("CPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/libxi-1.7.6-oxc5xryb5evzdbn2yfwjb2bl7kwbxmui/include", ":")
prepend_path("PKG_CONFIG_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/libxi-1.7.6-oxc5xryb5evzdbn2yfwjb2bl7kwbxmui/lib/pkgconfig", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/libxi-1.7.6-oxc5xryb5evzdbn2yfwjb2bl7kwbxmui/", ":")

-- libxcomposite
prepend_path("MANPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/libxcomposite-0.4.4-374lzng5i7nd2gxfw5lw4ljxnhr2qrfg/share/man", ":")
prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/libxcomposite-0.4.4-374lzng5i7nd2gxfw5lw4ljxnhr2qrfg/lib", ":")
prepend_path("LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/libxcomposite-0.4.4-374lzng5i7nd2gxfw5lw4ljxnhr2qrfg/lib", ":")
prepend_path("CPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/libxcomposite-0.4.4-374lzng5i7nd2gxfw5lw4ljxnhr2qrfg/include", ":")
prepend_path("PKG_CONFIG_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/libxcomposite-0.4.4-374lzng5i7nd2gxfw5lw4ljxnhr2qrfg/lib/pkgconfig", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/libxcomposite-0.4.4-374lzng5i7nd2gxfw5lw4ljxnhr2qrfg/", ":")

-- libxcursor
prepend_path("MANPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/libxcursor-1.1.14-gc2n5blg7m7hreyux6v4dahuzrwetrno/share/man", ":")
prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/libxcursor-1.1.14-gc2n5blg7m7hreyux6v4dahuzrwetrno/lib", ":")
prepend_path("LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/libxcursor-1.1.14-gc2n5blg7m7hreyux6v4dahuzrwetrno/lib", ":")
prepend_path("CPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/libxcursor-1.1.14-gc2n5blg7m7hreyux6v4dahuzrwetrno/include", ":")
prepend_path("PKG_CONFIG_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/libxcursor-1.1.14-gc2n5blg7m7hreyux6v4dahuzrwetrno/lib/pkgconfig", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/libxcursor-1.1.14-gc2n5blg7m7hreyux6v4dahuzrwetrno/", ":")

-- libxrander
prepend_path("MANPATH","/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/libxrandr-1.5.0-h3w7kwwbjvvwlmzhjwnfjixja3rxotau/share/man")
prepend_path("LD_LIBRARY_PATH","/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/libxrandr-1.5.0-h3w7kwwbjvvwlmzhjwnfjixja3rxotau/lib")
prepend_path("LIBRARY_PATH","/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/libxrandr-1.5.0-h3w7kwwbjvvwlmzhjwnfjixja3rxotau/lib")
prepend_path("CPATH","/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/libxrandr-1.5.0-h3w7kwwbjvvwlmzhjwnfjixja3rxotau/include")
prepend_path("PKG_CONFIG_PATH","/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/libxrandr-1.5.0-h3w7kwwbjvvwlmzhjwnfjixja3rxotau/lib/pkgconfig")
prepend_path("CMAKE_PREFIX_PATH","/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/libxrandr-1.5.0-h3w7kwwbjvvwlmzhjwnfjixja3rxotau/")


setenv("MATLAB_HOME","/network/lustre/iss01/apps/lang/matlab/R2019b/")
setenv("LM_LICENSE_FILE","27000@10.10.3.47")
prepend_path("PATH","/network/lustre/iss01/apps/lang/matlab/R2019b/bin/")
