help([[
...
]])
whatis("Name: R")
whatis("Version: 3.4.3")
whatis("Keywords: Tools, statistical computing")
whatis("Description: R is a language and environment for statistical computing and graphics.")
whatis("URL: https://mran.microsoft.com/")

--depends_on("libpng-1.6.29-gcc-4.8.5-brjdlhi")
-- Lib PNG
prepend_path("PATH","/network/lustre/iss01/apps/software/linux-centos7-x86_64/gcc/4.8.5/libpng/1.6.29/brjdlhiiwd5bklazyekiszua63i6snj3/bin")
prepend_path("MANPATH","/network/lustre/iss01/apps/software/linux-centos7-x86_64/gcc/4.8.5/libpng/1.6.29/brjdlhiiwd5bklazyekiszua63i6snj3/share/man")
prepend_path("LIBRARY_PATH","/network/lustre/iss01/apps/software/linux-centos7-x86_64/gcc/4.8.5/libpng/1.6.29/brjdlhiiwd5bklazyekiszua63i6snj3/lib")
prepend_path("LD_LIBRARY_PATH","/network/lustre/iss01/apps/software/linux-centos7-x86_64/gcc/4.8.5/libpng/1.6.29/brjdlhiiwd5bklazyekiszua63i6snj3/lib")
prepend_path("CPATH","/network/lustre/iss01/apps/software/linux-centos7-x86_64/gcc/4.8.5/libpng/1.6.29/brjdlhiiwd5bklazyekiszua63i6snj3/include")
prepend_path("PKG_CONFIG_PATH","/network/lustre/iss01/apps/software/linux-centos7-x86_64/gcc/4.8.5/libpng/1.6.29/brjdlhiiwd5bklazyekiszua63i6snj3/lib/pkgconfig")
prepend_path("CMAKE_PREFIX_PATH","/network/lustre/iss01/apps/software/linux-centos7-x86_64/gcc/4.8.5/libpng/1.6.29/brjdlhiiwd5bklazyekiszua63i6snj3/")

prepend_path( "LD_LIBRARY_PATH"   ,"/network/lustre/iss01/apps/lang/r/ropen/3.4.3/lib64/R/lib")
prepend_path( "LD_LIBRARY_PATH"   ,"/network/lustre/iss01/apps/lang/r/ropen/3.4.3/lib64/R/library")
prepend_path( "LIBRARY_PATH"   ,"/network/lustre/iss01/apps/lang/r/ropen/3.4.3/lib64/R/lib")
prepend_path( "LIBRARY_PATH"   ,"/network/lustre/iss01/apps/lang/r/ropen/3.4.3/lib64/R/library")
prepend_path( "PATH", "/network/lustre/iss01/apps/lang/r/ropen/3.4.3/lib64/R/bin")
prepend_path ("MANPATH" , "/network/lustre/iss01/apps/lang/r/ropen/3.4.3/lib64/R/")
prepend_path ("INFOPATH" , "/network/lustre/iss01/apps/lang/r/ropen/3.4.3/lib64/R/")
