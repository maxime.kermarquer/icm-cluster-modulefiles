-- R modulefile
-- 26/04/2018
whatis("Name: R")
whatis("Version: 3.5.0")
whatis("Keywords: Tools, statistical computing")
whatis("Description: R is a language and environment for statistical computing and graphics.")
whatis("URL: https://cran.r-project.org/")

--depends_on("libpng-1.6.29-gcc-4.8.5-brjdlhi")
-- Lib PNG

depends_on("openssl")

prepend_path("PATH","/network/lustre/iss01/apps/lang/r/rcran/3.5.0/bin")
prepend_path("LD_LIBRARY_PATH","/network/lustre/iss01/apps/lang/r/rcran/3.5.0/lib64")
