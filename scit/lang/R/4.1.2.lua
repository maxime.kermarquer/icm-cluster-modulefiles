-- R modulefile
-- 31/12/2021
--
-- Author      : Martial Bornet
-- Last update : 2021-12-31
--
whatis("Name: R")
whatis("Version: 4.1.2")
whatis("Keywords: Tools, statistical computing")
whatis("Description: R is a language and environment for statistical computing and graphics.")
whatis("URL: https://cran.r-project.org/")

--depends_on("libpng-1.6.29-gcc-4.8.5-brjdlhi")
-- Lib PNG

depends_on("openssl")
depends_on("proxy")
depends_on("readline/7.0-56pj7kg")
depends_on("bzip2/1.0.6-zfy3jck")
depends_on("lzma/5.2.4")
depends_on("pcre/8.42-fx4feox")
depends_on("curl/7.60.0-nqidbsk")
depends_on("libjpeg-turbo")
depends_on("libjpeg")
depends_on("libpng")
depends_on("zlib")
depends_on("perl/5.26.2-lc5lzoi")
depends_on("gdal")
depends_on("sqlite")
depends_on("geos")
depends_on("libxml2")
depends_on("netcdf")
depends_on("libiconv")
depends_on("udunits")
depends_on("proj")


prepend_path("PATH","/network/lustre/iss01/apps/lang/r/rcran/4.1.2/bin")
prepend_path("LD_LIBRARY_PATH","/network/lustre/iss01/apps/lang/r/rcran/4.1.2/lib64")
prepend_path("MANPATH","/network/lustre/iss01/apps/lang/r/rcran/4.1.2/share")
