--
-- Module      : R/4.1.2_2023-04-03
-- File        : R/4.1.2_2023-04-03.lua
-- Author      : Martial Bornet
-- Last update : 2023-05-10
--
whatis("Name: R")
whatis("Version: 4.1.2_2023-04-03")
whatis("Keywords: Tools, statistical computing")
whatis("Description: R is a language and environment for statistical computing and graphics.")
whatis("URL: https://cran.r-project.org/")

depends_on("openssl")
depends_on("proxy")
depends_on("fftw/3.3.8-g7jugkl")
depends_on("readline/7.0-56pj7kg")
depends_on("bzip2/1.0.6-zfy3jck")
depends_on("lzma/5.2.4")
depends_on("libpcre2/libpcre2-8")
depends_on("curl/7.60.0-nqidbsk")
depends_on("libjpeg-turbo")
depends_on("image-magick/6.9.12")
depends_on("libjpeg")
depends_on("libpng")
depends_on("zlib")
depends_on("perl/5.26.2-lc5lzoi")
depends_on("gdal")
depends_on("sqlite")
depends_on("geos")
depends_on("libxml2")
depends_on("netcdf")
depends_on("libiconv")
depends_on("udunits")
depends_on("proj")
depends_on("harfbuzz")


prepend_path("PATH","/network/lustre/iss01/apps/lang/r/rcran/4.1.2_2023-04-03/bin")
prepend_path("LD_LIBRARY_PATH","/network/lustre/iss01/apps/lang/r/rcran/4.1.2_2023-04-03/lib64")
prepend_path("MANPATH","/network/lustre/iss01/apps/lang/r/rcran/4.1.2_2023-04-03/share")
