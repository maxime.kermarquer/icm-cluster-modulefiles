help([[
For detailed instructions, go to:
       https://anaconda.org

]])
whatis("Version: 3.7.6")
whatis("Keywords: Python")
whatis("URL: http://anaconda.org")
whatis("Description: Python interperter")

prepend_path("PATH","/network/lustre/iss01/apps/lang/anaconda/3/2020.02/global/bin")
