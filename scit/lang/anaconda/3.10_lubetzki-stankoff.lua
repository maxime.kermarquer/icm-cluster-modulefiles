-- Author : Maxime KERMARQUER
-- Date : 06/06/2023

-- Module description
whatis([[Name : anaconda]])
whatis([[Version : 2023.03]])
whatis([[Short description : Anaconda is a free and open-source[6] distribution of the Python and R programming languages for scientific computing (data science, machine learning applications, large-scale data processing, predictive analytics, etc.), that aims to simplify package management and deployment.]])


-- Set up environment variables
prepend_path("PATH", "/network/lustre/iss01/apps/teams/lubetzki-stankoff/anaconda/2023.03-1-Linux-x86_64/bin", ":")
