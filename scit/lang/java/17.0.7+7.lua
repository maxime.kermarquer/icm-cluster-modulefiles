--
-- Module	: java/17.0.7+7
-- File		: 17.0.7+7.lua
-- Author	: Martial Bornet
-- Last update	: 2023-05-24
--
whatis("Version: 17.0.7")
whatis("Keywords: Java")
whatis("URL: https://adoptium.net/download/")
whatis("Description: Java Development kit")
setenv("JAVA_HOME","/network/lustre/iss01/apps/lang/java/17/jdk-17.0.7+7")
prepend_path("PATH","/network/lustre/iss01/apps/lang/java/17/jdk-17.0.7+7/bin")
prepend_path("LD_LIBRARY_PATH","/network/lustre/iss01/apps/lang/java/17/jdk-17.0.7+7/lib")
prepend_path("LD_LIBRARY_PATH","/network/lustre/iss01/apps/lang/java/17/jdk-17.0.7+7/lib/server")
