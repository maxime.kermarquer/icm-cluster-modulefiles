--
-- Module	: java/17.0.7
-- File		: 17.0.7.lua
-- Author	: Martial Bornet
-- Last update	: 2023-05-23
--
whatis("Version: 17.0.7")
whatis("Keywords: Java")
whatis("URL: https://www.oracle.com/java/technologies/javase/jdk17-archive-downloads.html")
whatis("Description: Java Development kit")
setenv("JAVA_HOME","/network/lustre/iss01/apps/lang/java/17/jdk-17.0.7")
prepend_path("PATH","/network/lustre/iss01/apps/lang/java/17/jdk-17.0.7/bin")
prepend_path("LD_LIBRARY_PATH","/network/lustre/iss01/apps/lang/java/17/jdk-17.0.7/lib")
prepend_path("LD_LIBRARY_PATH","/network/lustre/iss01/apps/lang/java/17/jdk-17.0.7/lib/server")
