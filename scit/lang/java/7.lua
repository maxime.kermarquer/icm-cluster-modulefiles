help([[
For detailed instructions, go to:
           https://java.com

           ]])
whatis("Version: 1.7.0.80")
whatis("Keywords: Java")
whatis("URL: http://java.com")
whatis("Description: Java Development kit")
setenv("JAVA_HOME","/network/lustre/iss01/apps/lang/java/7/7u80/jdk1.7.0_80")
prepend_path("PATH","/network/lustre/iss01/apps/lang/java/7/7u80/jdk1.7.0_80/bin")
