help([[
For detailed instructions, go to:
           https://java.com

           ]])
whatis("Version: 1.8.0.161")
whatis("Keywords: Java")
whatis("URL: http://java.com")
whatis("Description: Java Development kit")
setenv("JAVA_HOME","/network/lustre/iss01/apps/lang/java/8/8u161/jdk1.8.0_161")
prepend_path("PATH","/network/lustre/iss01/apps/lang/java/8/8u161/jdk1.8.0_161/bin")
