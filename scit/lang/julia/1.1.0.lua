-- ANTs modulefile
-- 19/03/2018
whatis([[Name : Julia]])
whatis([[Version : 1.1.0]])
whatis([[Description : : Julia is a flexible dynamic language, appropriate for scienধfic and numerical compuধng, with performance comparable to tradiধonal staধcally-typed languages.]])

prepend_path("PATH", "/network/lustre/iss01/apps/lang/julia/1.1.0/bin")
prepend_path("LIBRARY_PATH", "/network/lustre/iss01/apps/lang/julia/1.1.0/lib")
prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/lang/julia/1.1.0/lib")
prepend_path("MANPATH","/network/lustre/iss01/apps/lang/julia/1.1.0/share/man")
prepend_path("CPATH","/network/lustre/iss01/apps/lang/julia/1.1.0/include")
prepend_path("JULIA_DEPOT_PATH","/network/lustre/iss01/apps/lang/julia/1.1.0/installed-packages")

