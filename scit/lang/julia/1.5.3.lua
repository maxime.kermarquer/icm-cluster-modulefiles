-- Author : Maxime KERMARQUER
-- 19/01/2021
whatis([[Name : Julia]])
whatis([[Version : 1.5.3]])
whatis([[Description : : Julia is a flexible dynamic language, appropriate for scientific and numerical computing, with performance comparable to traditional statically-typed languages.]])

prepend_path("PATH", "/network/lustre/iss01/apps/lang/julia/1.5.3/bin")
prepend_path("LIBRARY_PATH", "/network/lustre/iss01/apps/lang/julia/1.5.3/lib")
prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/lang/julia/1.5.3/lib")
prepend_path("MANPATH","/network/lustre/iss01/apps/lang/julia/1.5.3/share/man")
prepend_path("CPATH","/network/lustre/iss01/apps/lang/julia/1.5.3/include")
prepend_path("JULIA_DEPOT_PATH","/network/lustre/iss01/apps/lang/julia/1.5.3/installed-packages")

