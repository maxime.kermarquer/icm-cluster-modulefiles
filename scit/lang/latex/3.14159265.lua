help([[

]])
whatis("URL: https://www.latex-project.org/")
whatis("Description: LaTeX is a high-quality typesetting system; it includes features designed for the production of technical and scientific documentation. LaTeX is the de facto standard for the communication and publication of scientific documents. ")

prepend_path ("PATH" , "/network/lustre/iss01/apps/lang/latex/texlive/2017/bin/x86_64-linux")
prepend_path ("MANPATH" , "/network/lustre/iss01/apps/lang/latex/texlive/2017/texmf-dist/doc/man")
prepend_path ("INFOPATH" , "/network/lustre/iss01/apps/lang/latex/texlive/2017/texmf-dist/doc/info")

