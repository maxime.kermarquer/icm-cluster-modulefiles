-- -*- lua -*-
-- -- Module file created by spack (https://github.com/spack/spack) on 2018-03-11 22:00:06.239643
-- --
-- -- perl@5.24.1%gcc@7.3.0+cpanm+shared arch=linux-centos7-x86_64 /ahzi5hr
-- --
--
 whatis([[Name : perl]])
 whatis([[Version : 5.24.1]])
 whatis([[Short description : Perl 5 is a highly capable, feature-rich programming language with over 27 years of development.]])
 whatis([[Configure options : -des -Dprefix=/network/lustre/iss01/apps/software/linux-centos7-x86_64/gcc/7.3.0/perl/5.24.1/ahzi5hrefrxdwcpdx7d5rrv5yquckr2b -Dlocincpth=/network/lustre/iss01/apps/software/linux-centos7-x86_64/gcc/7.3.0/gdbm/1.14.1/codgpslsr5agetrxl3se5mq6dxi3n7yw/include -Dloclibpth=/network/lustre/iss01/apps/software/linux-centos7-x86_64/gcc/7.3.0/gdbm/1.14.1/codgpslsr5agetrxl3se5mq6dxi3n7yw/lib  -Accflags=-DAPPLLIB_EXP=\"/network/lustre/iss01/apps/software/linux-centos7-x86_64/gcc/7.3.0/perl/5.24.1/ahzi5hrefrxdwcpdx7d5rrv5yquckr2b/lib/perl5\" -Duseshrplib]])

 help([[Perl 5 is a highly capable, feature-rich programming language with over 27 years of development.]])



 prepend_path("PATH", "/network/lustre/iss01/apps/software/linux-centos7-x86_64/gcc/7.3.0/perl/5.24.1/ahzi5hrefrxdwcpdx7d5rrv5yquckr2b/bin")
 prepend_path("MANPATH", "/network/lustre/iss01/apps/software/linux-centos7-x86_64/gcc/7.3.0/perl/5.24.1/ahzi5hrefrxdwcpdx7d5rrv5yquckr2b/man")
 prepend_path("LIBRARY_PATH", "/network/lustre/iss01/apps/software/linux-centos7-x86_64/gcc/7.3.0/perl/5.24.1/ahzi5hrefrxdwcpdx7d5rrv5yquckr2b/lib")
 prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/software/linux-centos7-x86_64/gcc/7.3.0/perl/5.24.1/ahzi5hrefrxdwcpdx7d5rrv5yquckr2b/lib")
 prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/linux-centos7-x86_64/gcc/7.3.0/perl/5.24.1/ahzi5hrefrxdwcpdx7d5rrv5yquckr2b/")

