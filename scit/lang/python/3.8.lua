-- Author : Maxime KERMARQUER
-- Date : 15/09/2020

help([[
For detailed instructions, go to:
       https://anaconda.org

]])
whatis("Version: 3.8")
whatis("Keywords: Python")
whatis("URL: http://anaconda.org")
whatis("Description: Python interperter")
prepend_path("PATH","/network/lustre/iss01/apps/lang/anaconda/3/2020.07/python_38/bin")
