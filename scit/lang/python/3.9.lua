--
-- Author      : Martial Bornet
-- Last update : 2022-03-03

help([[
For detailed instructions, go to:
       https://anaconda.org
]])

whatis("Version: 3.9")
whatis("Keywords: Python")
whatis("URL: http://anaconda.org")
whatis("Description: Python interperter")

prepend_path("PATH","/network/lustre/iss01/apps/lang/anaconda/3/2021.11-2/bin")
