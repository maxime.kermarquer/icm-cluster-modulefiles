-- AFNI modulefile
-- 24/04/2018
whatis([[Name : AFNI]])
whatis([[Version : 18.1.05]])
whatis([[Description :  AFNI (Analysis of Functional NeuroImages) is a set of C programs for processing, analyzing, and displaying functional MRI (FMRI) data - a technique for mapping human brain activity.]])

--depends_on("mesa-glu/9.0.0-u6kr3v6")
--depends_on("mesa")
--depends_on("libxp")
--depends_on("libxpm")
--depends_on("libpng/1.2.57-zgnlc7c")
prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/AFNI/18.1.05/linux_openmp_64")
-- prepend_path("LIBRARY_PATH", "")
-- prepend_path("LD_LIBRARY_PATH", "")
