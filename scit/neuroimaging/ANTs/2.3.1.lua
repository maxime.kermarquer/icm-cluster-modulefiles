-- ANTs modulefile
-- 12/01/2019
whatis([[Name : ANTs]])
whatis([[Version : 2.2.0]])
whatis([[Description : ANTs computes high-dimensional mappings to capture the statistics of brain structure and function. ]])

setenv("ANTSPATH", "/network/lustre/iss01/apps/software/scit/ANTs/2.3.1/bin")
prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/ANTs/2.3.1/bin")
prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/ANTs/2.3.1/Scripts")
prepend_path("LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/ANTs/2.3.1/lib")
prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/ANTs/2.3.1/lib")
