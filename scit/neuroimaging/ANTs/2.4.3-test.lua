-- ANTs modulefile
--
-- Author : Sangmin Park
-- Date   : 2023-04-20
--
whatis([[Name : ANTs]])
whatis([[Version : 2.4.3]])
whatis([[Description : Advanced Normalization Tools - ANTs computes high-dimensional mappings to capture the statistics of brain structure and function. ]])
whatis([[URL : https://github.com/ANTsX/ANTs]])
whatis([[Installation : https://github.com/ANTsX/ANTs/wiki/Compiling-ANTs-on-Linux-and-Mac-OS]])

setenv("ANTSPATH", "/network/lustre/iss01/apps/software/scit/ANTs/2.4.3/bin")
prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/ANTs/2.4.3/bin")
-- prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/ANTs/ANTs-2.4.1/2.4.1/Scripts")
prepend_path("LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/ANTs/2.4.3/lib64")
