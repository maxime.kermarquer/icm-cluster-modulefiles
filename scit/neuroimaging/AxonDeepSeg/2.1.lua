-- -*- lua -*-
-- Date: 06/12/2018
-- Author : Maxime KERMARQUER
--
--

-- Module description
whatis([[Name : AxonDeepSeg]])
whatis([[Version : 2.0.1]])
whatis([[Short description : AxonDeepSeg is an open-source software using deep learning and aiming at automatically segmenting axons and myelin sheaths from microscopy images. It performs 3-class semantic segmentation using a convolutional neural network. ]])


-- Set up environment variables
prepend_path("PATH", "/network/lustre/iss01/apps/lang/anaconda/3/5.2.0/axondeepcut/bin", ":")
--prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/tools/additional_librairies/gtk+/3.22.30/usr/lib64", ":")

