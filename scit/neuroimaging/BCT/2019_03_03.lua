-- BCT modulefile
-- Author : Maxime KERMARQUER
-- Date : 18/09/2020
whatis([[Name : BCT]])
whatis([[Version : Release 2019-03-03]])
whatis([[Description : The Brain Connectivity Toolbox (brain-connectivity-toolbox.net) is a MATLAB toolbox for complex-network analysis of structural and functional brain-connectivity data sets.  ]])
whatis("URL: https://sites.google.com/site/bctnet/")


depends_on("MATLAB")
prepend_path("MATLABPATH", "/network/lustre/iss01/apps/software/scit/BCT/2019_03_03")
