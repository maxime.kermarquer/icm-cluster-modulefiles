-- -*- lua -*-
-- Date: 27/11/2018
-- Author : Maxime KERMARQUER
--
--

-- Module description
whatis([[Name : CONN]])
whatis([[Version : 18a]])
whatis([[Short description : CONN is an open-source Matlab/SPM-based cross-platform software for the computation, display, and analysis of functional connectivity Magnetic Resonance Imaging (fcMRI). CONN is used to analyze resting state data (rsfMRI) as well as task-related designs. ]])
whatis([[Configure options : https://www.nitrc.org/frs/download.php/10562/conn18a.zip]])
help([[ ]])


-- Set up environment variables
depends_on("MATLAB")
depends_on("SPM")
prepend_path("MATLABPATH", "/network/lustre/iss01/apps/software/scit/conn/18a", ":")
