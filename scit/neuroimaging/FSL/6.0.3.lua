--#%Module1.0
--## Module file created by spack (https://github.com/spack/spack) on 2018-03-14 22:27:35.251224
--##
--## fsl@5.0.10%gcc@4.8.5 arch=linux-centos7-x86_64 /rvai3es
--##
--
--
--module-whatis "FSL is a comprehensive library of analysis tools for FMRI, MRI and DTI brain imaging data."
--
--proc ModulesHelp { } {
--puts stderr "FSL is a comprehensive library of analysis tools for FMRI, MRI and DTI"
--puts stderr "brain imaging data. Note: A manual download is required for FSL. Spack"
--puts stderr "will search your current directory for the download file. Alternatively,"
--puts stderr "add this file to a mirror so that Spack can find it. For instructions on"
--puts stderr "how to set up a mirror, see"
--puts stderr "http://spack.readthedocs.io/en/latest/mirrors.html"
--}
--
--
--prepend-path PATH "/network/lustre/iss01/apps/software/scit/fsl/6.0.3-xenial/bin"
--prepend-path LIBRARY_PATH "/network/lustre/iss01/apps/software/scit/fsl/6.0.3-xenial/lib"
--prepend-path LD_LIBRARY_PATH "/network/lustre/iss01/apps/software/scit/fsl/6.0.3-xenial/lib"
--prepend-path CPATH "/network/lustre/iss01/apps/software/scit/fsl/6.0.3-xenial/include"
--prepend-path CMAKE_PREFIX_PATH "/network/lustre/iss01/apps/software/scit/fsl/6.0.3-xenial/"
--setenv FSLDIR "/network/lustre/iss01/apps/software/scit/fsl/6.0.3-xenial"
--setenv FSLOUTPUTTYPE "NIFTI_GZ"
--setenv FSLMULTIFILEQUIT "TRUE"
--setenv FSLTCLSH "/network/lustre/iss01/apps/software/scit/fsl/6.0.3-xenial/bin/fsltclsh"
--setenv FSLWISH "/network/lustre/iss01/apps/software/scit/fsl/6.0.3-xenial/bin/fslwish"
--setenv FSLLOCKDIR ""
--setenv FSLMACHINELIST ""
--setenv FSLREMOTECALL ""
--setenv FSLGECUDAQ "cuda.q"
--prepend-path PATH "/network/lustre/iss01/apps/software/scit/fsl/6.0.3-xenial"


-- FSL modulefile
-- 19/03/2018
whatis([[Name : FSL]])
whatis([[Version : 6.0.3]])
whatis([[Short description : FSL is a comprehensive library of analysis tools for FMRI, MRI and DTI brain imaging data.]])

depends_on("mesa")
depends_on("mesa-glu")
depends_on("CUDA/9.1")
depends_on("libpng/1.2.57-zgnlc7c")

prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/fsl/6.0.3-xenial/bin")
prepend_path("LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/fsl/6.0.3-xenial/lib")
prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/fsl/6.0.3-xenial/lib")
prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/fsl/6.0.3-xenial/fslpython/envs/fslpython/lib/")
setenv("FSLDIR","/network/lustre/iss01/apps/software/scit/fsl/6.0.3-xenial/")
setenv("FSLGECUDAQ","cuda.q")
setenv("FSLLOCKDIR","")
setenv("FSLMACHINELIST","")
setenv("FSLMULTIFILEQUIT","TRUE")
setenv("FSLOUTPUTTYPE","NIFTI_GZ")
setenv("FSLREMOTECALL","")
setenv("FSLTCLSH","/network/lustre/iss01/apps/software/scit/fsl/6.0.3-xenial/bin/fsltclsh")
setenv("FSLWISH","/network/lustre/iss01/apps/software/scit/fsl/6.0.3-xenial/bin/fslwish")

