-- FreeSurfer modulefile
-- 16/03/2018
help([[
For detailed instructions, go to:
           https://surfer.nmr.mgh.harvard.edu/fswiki/FreeSurferWiki
           ]])
whatis("Version: 5.3.0")
whatis("Keywords: FreeSurfer")
whatis("URL: https://surfer.nmr.mgh.harvard.edu/")
whatis("Description: FreeSurfer is a software package for the analysis and visualization of structural and functional neuroimaging data from cross-sectional or longitudinal studies.")

setenv("FREESURFER_HOME","/network/lustre/iss01/apps/software/noarch/freesurfer/freesurfer_5.3.0")
setenv("FIX_VERTEX_AREA","")
setenv("FMRI_ANALYSIS_DIR","/network/lustre/iss01/apps/software/noarch/freesurfer/freesurfer_5.3.0/fsfast")
setenv("FSFAST_HOME","/network/lustre/iss01/apps/software/noarch/freesurfer/freesurfer_5.3.0/fsfast")
setenv("FSF_OUTPUT_FORMAT","nii.gz")
setenv("FS_OVERRIDE","0")
setenv("FUNCTIONALS_DIR","/network/lustre/iss01/apps/software/noarch/freesurfer/freesurfer_5.3.0/sessions")
setenv("LOCAL_DIR","/network/lustre/iss01/apps/software/noarch/freesurfer/freesurfer_5.3.0/local")
setenv("MINC_BIN_DIR","/network/lustre/iss01/apps/software/noarch/freesurfer/freesurfer_5.3.0/mni/bin")
setenv("MINC_LIB_DIR","/network/lustre/iss01/apps/software/noarch/freesurfer/freesurfer_5.3.0/mni/lib")
setenv("MNI_DATAPATH","/network/lustre/iss01/apps/software/noarch/freesurfer/freesurfer_5.3.0/mni/data")
setenv("MNI_DIR","/network/lustre/iss01/apps/software/noarch/freesurfer/freesurfer_5.3.0/mni")
setenv("MNI_PERL5LIB","/network/lustre/iss01/apps/software/noarch/freesurfer/freesurfer_5.3.0/mni/share/perl5")
setenv("OS","Linux")
prepend_path("PATH","/network/lustre/iss01/apps/software/noarch/freesurfer/freesurfer_5.3.0/mni/bin")
prepend_path("PATH","/network/lustre/iss01/apps/software/noarch/freesurfer/freesurfer_5.3.0/tktools")
prepend_path("PATH","/network/lustre/iss01/apps/software/noarch/freesurfer/freesurfer_5.3.0/fsfast/bin")
prepend_path("PATH","/network/lustre/iss01/apps/software/noarch/freesurfer/freesurfer_5.3.0/bin")
--setenv("PERL5LIB","/network/lustre/iss01/apps/software/noarch/freesurfer/freesurfer_5.3.0/mni/share/perl5")
setenv("PERL5LIB","/network/lustre/iss01/apps/software/noarch/freesurfer/freesurfer_5.3.0/mni/lib/perl5/5.8.5")
setenv("SUBJECTS_DIR","/network/lustre/iss01/apps/software/noarch/freesurfer/freesurfer_5.3.0/subjects")

