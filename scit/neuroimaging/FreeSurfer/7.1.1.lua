-- FreeSurfer modulefile
-- Author : Maxime KERMARQUER
-- 06/04/2021
help([[
For detailed instructions, go to:
           https://surfer.nmr.mgh.harvard.edu/fswiki/FreeSurferWiki
           ]])
whatis("Version: 6.0.0")
whatis("Keywords: FreeSurfer")
whatis("URL: https://surfer.nmr.mgh.harvard.edu/")
whatis("Description: FreeSurfer is a software package for the analysis and visualization of structural and functional neuroimaging data from cross-sectional or longitudinal studies.")

depends_on("libxp/1.0.3-nshdxod")
depends_on("libxpm/3.5.12-mj2dus4")     
depends_on("libpng/1.2.57-zgnlc7c")
depends_on("mesa-glu/9.0.0-u6kr3v6.lua","graphic-libraries")

setenv("FREESURFER_HOME","/network/lustre/iss01/apps/software/noarch/freesurfer/7.1.1")
setenv("FIX_VERTEX_AREA","")
setenv("FMRI_ANALYSIS_DIR","/network/lustre/iss01/apps/software/noarch/freesurfer/7.1.1/fsfast")
setenv("FSFAST_HOME","/network/lustre/iss01/apps/software/noarch/freesurfer/7.1.1/fsfast")
setenv("FSF_OUTPUT_FORMAT","nii.gz")
setenv("FS_OVERRIDE","0")
setenv("FUNCTIONALS_DIR","/network/lustre/iss01/apps/software/noarch/freesurfer/7.1.1/sessions")
setenv("LOCAL_DIR","/network/lustre/iss01/apps/software/noarch/freesurfer/7.1.1/local")
setenv("MINC_BIN_DIR","/network/lustre/iss01/apps/software/noarch/freesurfer/7.1.1/mni/bin")
setenv("MINC_LIB_DIR","/network/lustre/iss01/apps/software/noarch/freesurfer/7.1.1/mni/lib")
setenv("MNI_DATAPATH","/network/lustre/iss01/apps/software/noarch/freesurfer/7.1.1/mni/data")
setenv("MNI_DIR","/network/lustre/iss01/apps/software/noarch/freesurfer/7.1.1/mni")
setenv("MNI_PERL5LIB","/network/lustre/iss01/apps/software/noarch/freesurfer/7.1.1/mni/share/perl5")
setenv("OS","Linux")
prepend_path("PATH","/network/lustre/iss01/apps/software/noarch/freesurfer/7.1.1/mni/bin")
prepend_path("PATH","/network/lustre/iss01/apps/software/noarch/freesurfer/7.1.1/tktools")
prepend_path("PATH","/network/lustre/iss01/apps/software/noarch/freesurfer/7.1.1/fsfast/bin")
prepend_path("PATH","/network/lustre/iss01/apps/software/noarch/freesurfer/7.1.1/bin")
setenv("PERL5LIB","/network/lustre/iss01/apps/software/noarch/freesurfer/7.1.1/mni/share/perl5")
setenv("SUBJECTS_DIR","/network/lustre/iss01/apps/software/noarch/freesurfer/7.1.1/subjects")

