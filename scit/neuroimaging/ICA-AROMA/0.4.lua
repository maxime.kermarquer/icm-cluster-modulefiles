
whatis("Version: 0.4")
whatis("URL: https://github.com/maartenmennes/ICA-AROMA")
whatis("Description: ICA-AROMA (i.e. ‘ICA-based Automatic Removal Of Motion Artifacts’) concerns a data-driven method to identify and remove motion-related independent components from fMRI data.")
prepend_path("PATH","/network/lustre/iss01/apps/software/scit/ICA_AROMA/0.4")
