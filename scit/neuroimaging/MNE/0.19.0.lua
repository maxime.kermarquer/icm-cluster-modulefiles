-- 07/10/2019
-- Maxime KERMARQUER

help([[
For detailed instructions, go to:
       https://anaconda.org
       https://martinos.org/mne/stable/documentation.html
]])
whatis("Version: Python 3.6, MNE 0.19.0")
whatis("URL: http://anaconda.org")
whatis("Description: Python interperter for MNE")


local f = io.open("/etc/os-release", "r")
local operating_system = f:read("*all")
f:close()

local f2 = io.open("/etc/hostname", "r")
local hostname = f2:read("*all")
f2:close()



depends_on("mesa/18.1.2-ysz5dek")
depends_on("mesa-glu/9.0.0-u6kr3v6")
depends_on("xcb-proto/1.13-iye6omr")
depends_on("libxcb/1.13-exuaj64")
depends_on("libxi/1.7.6-oxc5xry")
depends_on("util-macros/1.19.1-p66qdrt")


if string.find(operating_system, "CentOS") then
    -- centOS
   prepend_path("PATH","/network/lustre/iss01/apps/lang/anaconda/3/5.3.0/MNE/19.0/bin")
else
   -- linux mint
   LmodError ("This module is not supported for Linux Mint")
end
