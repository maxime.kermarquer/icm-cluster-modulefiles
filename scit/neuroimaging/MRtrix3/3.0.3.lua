--
-- Mrtrix3 modulefile
--
-- Author      : Martial Bornet
-- Last update : 2022-07-06
--
whatis([[Name : mrtrix3]])
whatis([[Version : 3.0.3]])
whatis([[Short description : MRtrix provides a set of tools to perform various advanced diffusion MRI analyses, including constrained spherical deconvolution (CSD), probabilistic tractography, track-density imaging, and apparent fibre density.]])

help([[MRtrix provides a set of tools to perform various advanced diffusion MRI
analyses, including constrained spherical deconvolution (CSD),
probabilistic tractography, track-density imaging, and apparent fibre
density.]])


prepend_path("PATH", "/network/lustre/iss01/apps/lang/miniconda/mrtrix/3.0.3/bin")
-- prepend_path("PATH", "")
-- prepend_path("PERL5LIB", "")
-- prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/mrtrix3/mrtrix3")

depends_on("fftw/3.3.8-g7jugkl")
depends_on("zlib/1.2.11-64vg6e4")
depends_on("libpng/1.6.34-737hc7h")
depends_on("libtiff/4.0.9-23zhztp")
depends_on("mrdegibbs3D/2022-07-15")
