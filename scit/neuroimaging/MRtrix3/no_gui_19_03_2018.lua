-- Mrtrix3 modulefile
-- 19/03/2018
whatis([[Name : mrtrix3]])
whatis([[Version : 2018-03-19]])
whatis([[Short description : MRtrix provides a set of tools to perform various advanced diffusion MRI analyses, including constrained spherical deconvolution (CSD), probabilistic tractography, track-density imaging, and apparent fibre density.]])

help([[MRtrix provides a set of tools to perform various advanced diffusion MRI
analyses, including constrained spherical deconvolution (CSD),
probabilistic tractography, track-density imaging, and apparent fibre
density.]])

prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/mrtrix3/mrtrix3/bin")
prepend_path("LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/mrtrix3/mrtrix3/lib")
prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/mrtrix3/mrtrix3/lib")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/mrtrix3/mrtrix3")
-- prepend_path("PATH", "")
-- prepend_path("PERL5LIB", "")
-- prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/mrtrix3/mrtrix3")

