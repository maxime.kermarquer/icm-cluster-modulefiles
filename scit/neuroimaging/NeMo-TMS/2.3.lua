-- NeMo-TMS Module	: NeMo-TMS/v2_3
-- Author	: Sangmin Park
-- Last update	: 2023-06-12
--

whatis("Version: 2.3")
whatis("Keywords: Neuron Modeling (NeMo) for TMS (Transcranial Magnetic Stimulation) toolbox")
whatis("URL: https://github.com/OpitzLab/NeMo-TMS")
whatis("Description: Neuron Modeling for TMS (NeMo-TMS) toolbox is for multi-scale modeling of the effects of Transcranial Magnetic Stimulation on")

prepend_path("PATH","/network/lustre/iss01/apps/software/scit/NeMo-TMS/2.3")
