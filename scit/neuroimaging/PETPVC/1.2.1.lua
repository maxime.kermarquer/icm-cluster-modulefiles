-- PETPEV modulefile
-- 22/03/2018
whatis([[Name : PETPVC]])
whatis([[Version : 1.2.1]])
whatis([[Description : PETPVC: toolbox for partial volume correction (PVC) in positron emission tomography (PET)]])

setenv("PETPVC_DIR","/network/lustre/iss01/apps/software/scit/PETPVC/1.2.1")
prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/PETPVC/1.2.1/bin")
--prepend_path("LIBRARY_PATH", "")
--prepend_path("LD_LIBRARY_PATH", "")
