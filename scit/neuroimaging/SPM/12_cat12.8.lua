--
-- SPM modulefile for use with CAT 12.8
--
-- Author      : Martial Bornet
-- Last update : 2023-02-02
--
whatis([[Name : SPM]])
whatis([[Version : 12_cat12.8]])
whatis([[Description : Statistical Parametric Mapping refers to the construction and assessment of spatially extended statistical processes used to test hypotheses about functional imaging data.]])
whatis([[Additional toolbox : MarsBaR]])

depends_on("MATLAB/R2020b")
prepend_path("MATLABPATH", "/network/lustre/iss01/apps/software/scit/SPM/spm12_cat12.8/spm12")
prepend_path("MATLABPATH", "/network/lustre/iss01/apps/software/scit/SPM/spm12_cat12.8/spm12/toolbox")
