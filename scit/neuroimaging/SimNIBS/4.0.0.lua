-- SimNIBS
--
-- Author : Martial Bornet
-- Date   : 2023-02-15
--
whatis([[Name : SimNIBS]])
whatis([[Version : 4.0.0]])
whatis([[Description : Simulation of Non-invasive Brain Stimulation]])
whatis([[URL : https://simnibs.github.io/simnibs/build/html/index.html]])

prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/SimNIBS/4.0.0/soft/bin")
-- prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/SimNIBS/2023-02-15/soft/bin")
-- prepend_path("LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/ANTs/ANTs-2.4.1/2.4.1/lib64")
-- prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/ANTs/2.4.1/lib")
