-- -*- lua -*-
-- Date: 15/03/2023
-- Author : Maxime KERMARQUER
-- https://helpdesk.icm-institute.org/front/ticket.form.php?id=89563

-- Module description
whatis([[Name : StarTrack ]])
whatis([[Version : 2022-01-27 ]])
whatis([[Short description : StarTrack is a program developed in Matlab to analyse diffusion imaging data using DTI, spherical deconvolution (SD) and tractography. ]])
whatis([[URL : https://www.mr-startrack.com/ ]])


depends_on("MATLAB")

-- Set up environment variables
prepend_path("MATLABPATH", "/network/lustre/iss01/apps/software/scit/startrack/ST_20220127_pcode")
prepend_path("MATLABPATH", "/network/lustre/iss01/apps/software/scit/startrack/ST_20220127_pcode/MISC")
prepend_path("MATLABPATH", "/network/lustre/iss01/apps/software/scit/startrack/ST_20220127_pcode/MISC/niftitool")
prepend_path("MATLABPATH", "/network/lustre/iss01/apps/software/scit/startrack/ST_20220127_pcode/IO")
prepend_path("MATLABPATH", "/network/lustre/iss01/apps/software/scit/startrack/ST_20220127_pcode/MODELS")
prepend_path("MATLABPATH", "/network/lustre/iss01/apps/software/scit/startrack/ST_20220127_pcode/MODELS/SD")
prepend_path("MATLABPATH", "/network/lustre/iss01/apps/software/scit/startrack/ST_20220127_pcode/MODELS/Gradients")
prepend_path("MATLABPATH", "/network/lustre/iss01/apps/software/scit/startrack/ST_20220127_pcode/Engines")
prepend_path("MATLABPATH", "/network/lustre/iss01/apps/software/scit/startrack/ST_20220127_pcode/TRACT")
prepend_path("MATLABPATH", "/network/lustre/iss01/apps/software/scit/startrack/ST_20220127_pcode/MAIN")
 
