-- TDT modulefile
-- 23/04/2018
whatis([[Name : The Decoding Toolbox]])
whatis([[Version : 3.991]])
whatis([[Description : TDT is an easy to use, fast and versatile Matlab toolbox for the multivariate analysis of functional and structural MRI data.]])

depends_on("MATLAB")
depends_on("SPM")
prepend_path("MATLABPATH", "/network/lustre/iss01/apps/software/scit/the_decoding_toolbox/v3.991")
--prepend_path("LIBRARY_PATH", "")
--prepend_path("LD_LIBRARY_PATH", "")
