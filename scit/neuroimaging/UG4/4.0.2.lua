-- SimNIBS
--
-- Author : Martial Bornet
-- Date   : 2023-02-16
--
whatis([[Name : UG4]])
whatis([[Version : 4.0.2]])
whatis([[Description : UG4 is an extensive, flexible, cross-platform open source simulation framework for the numerical solution of systems of partial differential equations]])
whatis([[URL : https://github.com/UG4/ugcore]])

prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/UG4/4.0.2/bin")
