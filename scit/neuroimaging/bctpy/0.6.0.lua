--
-- Module	: bctpy/0.6.0
-- File		: 0.6.0.lua
-- Author	: Martial Bornet
-- Last update	: 2023-05-26
--
whatis("Version: 0.6.0")
whatis("Keywords: Brain Connectivity Toolbox")
whatis("URL: https://pypi.org/project/bctpy/")
whatis("Description: Brain Connectivity Toolbox")

prepend_path("PATH","/network/lustre/iss01/apps/software/scit/bctpy/0.6.0/bin")
