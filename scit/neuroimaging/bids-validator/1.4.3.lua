-- -*- lua -*-
-- Date: 04/03/2020
-- Author : Maxime KERMARQUER
--

-- fmriprep dependency
--

-- Module description
whatis([[Name : bids-validator]])
whatis([[Version : 1.4.3]])
whatis([[Short description : Validator for the Brain Imaging Data Structure. ]])

-- Set up environment variables
prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/node-js/12.16.1/bin", ":")
prepend_path("MANPATH", "/network/lustre/iss01/apps/software/scit/node-js/12.16.1/share", ":")
prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/node-js/12.16.1/lib", ":")
prepend_path("LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/node-js/12.16.1/lib", ":")
prepend_path("CPATH", "/network/lustre/iss01/apps/software/scit/node-js/12.16.1/include", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/node-js/12.16.1", ":")

