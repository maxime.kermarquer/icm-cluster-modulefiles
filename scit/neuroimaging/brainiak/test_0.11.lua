-- -*- lua -*-

-- Author : Martial Bornet
-- Date   : 2022-04-13

whatis("Version: 0.11")
whatis("Name: BrainIAK")
whatis("Description: Brain Imaging Analysis Kit - Advanced fMRI analyses in Python, optimized for speed under the hood with MPI, Cython, and C++.")
whatis("URL: https://brainiak.org/")
help([[https://brainiak.org/docs/#support]])

-- depends_on("proxy")


prepend_path("PATH","/network/lustre/iss01/apps/software/scit/brainiak/test_0.11/bin")
prepend_path("LIBRARY_PATH","/network/lustre/iss01/apps/software/scit/brainiak/test_0.11/lib")
prepend_path("LD_LIBRARY_PATH","/network/lustre/iss01/apps/software/scit/brainiak/test_0.11/lib")
