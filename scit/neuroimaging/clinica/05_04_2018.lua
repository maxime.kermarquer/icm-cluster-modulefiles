

whatis("Version: 05_04_2018")
whatis("Keywords: Clinica")
whatis("URL: http://www.clinica.run/")
whatis("Description: Clinica is a software platform for clinical neuroscience research studies.")
 
-- FSL
if(not isloaded("FSL")) then
    load("FSL")
end

-- FreeSurfer 
if(not isloaded("FreeSurfer")) then
    load("FreeSurfer/5.3.0")
end

-- ANTs
if(not isloaded("ANTs")) then
    load("ANTs")
end

-- ITK
if(not isloaded("ITK")) then
    load("ITK")
end

-- MATLAB
if(not isloaded("MATLAB")) then
    load("MATLAB")
end

-- SPM
if(not isloaded("SPM")) then
    load("SPM")
end

-- MRtrix3
if(not isloaded("MRtrix3")) then
    load("MRtrix3")
end

-- PETPVC
if(not isloaded("PETPVC")) then
    load("PETPVC")
end

-- dcm2niix
if(not isloaded("dcm2niix")) then
    load("dcm2niix")
end

-- Python - Clinica
prepend_path( "PATH" ,"/network/lustre/iss01/apps/software/scit/clinica/miniconda2/bin")
--execute    {cmd="eval \"$(register-python-argcomplete clinica)\"", modeA={"load"}}
