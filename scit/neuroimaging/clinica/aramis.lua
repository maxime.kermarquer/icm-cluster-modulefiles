
-- Last modification : 13/02/2019

whatis("Version: Development version")
whatis("Keywords: Clinica")
whatis("URL: http://www.clinica.run/")
whatis("Description: Clinica is a software platform for clinical neuroscience research studies.")
 

-- https://lmod.readthedocs.io/en/latest/050_lua_modulefiles.html

-- ANTs
setenv("ANTSPATH","/network/lustre/iss01/apps/teams/aramis/clinica/ANTS/2.2.0/bin")
prepend_path("PATH", "/network/lustre/iss01/apps/teams/aramis/clinica/ANTS/2.2.0/bin")
prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/teams/aramis/clinica/ANTS/2.2.0/lib")

-- dcm2niix
prepend_path("PATH", "/network/lustre/iss01/apps/teams/aramis/clinica/dcm2niix/1.0.20171215/bin")
prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/teams/aramis/clinica/dcm2niix/1.0.20171215/lib")

-- Freesurfer
setenv("FREESURFER_HOME", "/network/lustre/iss01/apps/teams/aramis/clinica/freesurfer/6.0.0/freesurfer")
setenv("FSFAST_HOME" ,"/network/lustre/iss01/apps/teams/aramis/clinica/freesurfer/6.0.0/freesurfer/fsfast")
setenv("FSF_OUTPUT_FORMAT", "nii")
setenv("SUBJECTS_DIR", "/network/lustre/iss01/apps/teams/aramis/clinica/freesurfer/6.0.0/freesurfer/subjects")
setenv("MNI_DIR", "/network/lustre/iss01/apps/teams/aramis/clinica/freesurfer/6.0.0/freesurfer/mni")
setenv("FS_OVERRIDE", "0")
setenv("PERL5LIB", "/network/lustre/iss01/apps/teams/aramis/clinica/freesurfer/6.0.0/freesurfer/mni/lib/perl5/5.8.5")
setenv("LOCAL_DIR", "/network/lustre/iss01/apps/teams/aramis/clinica/freesurfer/6.0.0/freesurfer/local")
setenv("MNI_PERL5LIB", "/network/lustre/iss01/apps/teams/aramis/clinica/freesurfer/6.0.0/freesurfer/mni/lib/perl5/5.8.5")
setenv("FMRI_ANALYSIS_DIR", "/network/lustre/iss01/apps/teams/aramis/clinica/freesurfer/6.0.0/freesurfer/fsfast")
setenv("MINC_BIN_DIR", "/network/lustre/iss01/apps/teams/aramis/clinica/freesurfer/6.0.0/freesurfer/mni/bin")
setenv("MINC_LIB_DIR", "/network/lustre/iss01/apps/teams/aramis/clinica/freesurfer/6.0.0/freesurfer/mni/lib")
setenv("MNI_DATAPATH", "/network/lustre/iss01/apps/teams/aramis/clinica/freesurfer/6.0.0/freesurfer/mni/data")
prepend_path("PATH", "/network/lustre/iss01/apps/teams/aramis/clinica/freesurfer/6.0.0/freesurfer/bin")
prepend_path("PATH", "/network/lustre/iss01/apps/teams/aramis/clinica/freesurfer/6.0.0/freesurfer/mni/bin")
prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/teams/aramis/clinica/freesurfer/6.0.0/freesurfer/lib")

-- FSL
setenv("FSLDIR", "/network/lustre/iss01/apps/teams/aramis/clinica/fsl/5.0.11")
setenv("FSLOUTPUTTYPE", "NIFTI_GZ")
setenv("FSLMULTIFILEQUIT", "TRUE")
setenv("FSLTCLSH", "/network/lustre/iss01/apps/teams/aramis/clinica/fsl/5.0.11/bin/fsltclsh")
setenv("FSLWISH", "/network/lustre/iss01/apps/teams/aramis/clinica/fsl/5.0.11/bin/fslwish")
setenv("FSLLOCKDIR", "")
setenv("FSLMACHINELIST", "")
setenv("FSLREMOTECALL", "")
setenv("FSLGECUDAQ", "cuda.q")
prepend_path("PATH", "/network/lustre/iss01/apps/teams/aramis/clinica/fsl/5.0.11/bin")
prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/teams/aramis/clinica/fsl/5.0.11/lib")

-- ITK
setenv("ITK_HOME", "/network/lustre/iss01/apps/teams/aramis/clinica/ITK/4.13.0")
prepend_path("PATH", "/network/lustre/iss01/apps/teams/aramis/clinica/ITK/4.13.0/bin")
prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/teams/aramis/clinica/ITK/4.13.0/lib")
prepend_path("CPATH", "/network/lustre/iss01/apps/teams/aramis/clinica/ITK/4.13.0/include")
prepend_path("C_PLUS_INCLUDE_PATH", "/network/lustre/iss01/apps/teams/aramis/clinica/ITK/4.13.0/include")
prepend_path("MANPATH", "/network/lustre/iss01/apps/teams/aramis/clinica/ITK/4.13.0/share/man")

-- Mrtrix3
setenv("MRTRIX_HOME", "/network/lustre/iss01/apps/teams/aramis/clinica/mrtrix3/0.3.16")
prepend_path("PATH", "/network/lustre/iss01/apps/teams/aramis/clinica/mrtrix3/0.3.16/bin")
prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/teams/aramis/clinica/mrtrix3/0.3.16/lib")

-- PETPVC
prepend_path("PATH", "/network/lustre/iss01/apps/teams/aramis/clinica/PETPVC/1.2.2/bin")

-- MATLAB
setenv("MATLAB_HOME", "/network/lustre/iss01/apps/lang/matlab/R2017b//bin")
setenv("MATLABCMD", "/network/lustre/iss01/apps/lang/matlab/R2017b//bin/matlab")
prepend_path("PATH", "/network/lustre/iss01/apps/lang/matlab/R2017b//bin")
setenv("LM_LICENSE_FILE", "27000@SRVFLEXLM01")

-- SPM12
setenv("SPM_HOME", "/network/lustre/iss01/apps/teams/aramis/clinica/spm12/r7487")
prepend_path("PATH", "/network/lustre/iss01/apps/teams/aramis/clinica/spm12/r7487/bin")

-- Python
prepend_path( "PATH" ,"/network/lustre/iss01/apps/teams/aramis/clinica/anaconda/3/2018.12/bin")

-- libc issue
prepend_path("LD_LIBRARY_PATH", "/usr/lib64")