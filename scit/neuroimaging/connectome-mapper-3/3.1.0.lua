-- Connectome Mapper 3
--
-- Author : Martial Bornet
-- Date   : 2023-01-18
--
whatis([[Name : Connectome Mapper 3]])
whatis([[Version : 3.1.0]])
whatis([[Description :  Connectome Mapper 3 is an open-source Python3 image processing pipeline software, with a Graphical User Interface, that implements full anatomical, diffusion, resting-state MRI, and EEG processing pipelines, from raw Diffusion / T1 / BOLD / preprocessed EEG to multi-resolution connection matrices, based on a new version of the Lausanne parcellation atlas, aka Lausanne2018]])

depends_on("FSL/6.0.3")
depends_on("FreeSurfer/7.1.1")
depends_on("ANTs/2.4.1")
depends_on("AFNI/18.1.05")
depends_on("qt/5.15.8")
prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/connectome-mapper-3/3.1.0/lib")
prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/connectome-mapper-3/3.1.0/bin")
prepend_path("PYTHONPATH", "/network/lustre/iss01/apps/software/scit/connectome-mapper-3/3.1.0/envs/py39cmp-gui/lib/python3.9/site-packages")
setenv("CONNECTOME_IMAGE", "/network/lustre/iss01/apps/software/scit/connectome-mapper-3/singularity_images/connectomicslab_default_connectomemapper-bidsapp.sif")

-- prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/connectome-mapper-3/3.1.0/lib/python3.9/site-packages/PyQt5/Qt5/lib")
