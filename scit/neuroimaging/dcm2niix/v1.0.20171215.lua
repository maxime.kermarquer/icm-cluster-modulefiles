-- ITK  modulefile
-- 23/03/2018
whatis([[Name : dcm2niix]])
whatis([[Version : v1.0.20171215]])
whatis([[Description : dcm2niix is a designed to convert neuroimaging data from the DICOM format to the NIfTI format.]])

prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/dcm2niix/v1.0.20171215/bin")
