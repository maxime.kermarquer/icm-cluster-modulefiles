 -- FieldTrip modulefile
 --  -- 26/06/2018
whatis([[Name : FieldTrip]])
whatis([[Version : 20160329]])
whatis([[Description : FieldTrip is the MATLAB software toolbox for MEG and EEG analysis. The toolbox offers advanced analysis methods of MEG, EEG, and invasive electrophysiological data, such as time-frequency analysis, source reconstruction using dipoles, distributed sources and beamformers and non-parametric statistical testing.]])


-- http://www.fieldtriptoolbox.org/faq/should_i_add_fieldtrip_with_all_subdirectories_to_my_matlab_path
depends_on("MATLAB")
prepend_path("MATLABPATH", "/network/lustre/iss01/apps/software/scit/fieldtrip/20160329")
