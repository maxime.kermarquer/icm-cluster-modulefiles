 -- FieldTrip modulefile
 --  -- 26/06/2018
whatis([[Name : FieldTrip]])
whatis([[Version : 20220104]])
whatis([[Description : FieldTrip is the MATLAB software toolbox for MEG and EEG analysis. The toolbox offers advanced analysis methods of MEG, EEG, and invasive electrophysiological data, such as time-frequency analysis, source reconstruction using dipoles, distributed sources and beamformers and non-parametric statistical testing.]])
     
depends_on("MATLAB/R2020b")
prepend_path("MATLABPATH", "/network/lustre/iss01/apps/software/scit/fieldtrip/20220104")
