help([[
For detailed instructions, go to:
       https://anaconda.org

]])
whatis("Version: 3.6")
whatis("Keywords: Python")
whatis("URL: http://anaconda.org")
whatis("Description: Python interperter")

depends_on("libwebp")
depends_on("svgo")
depends_on("ICA-AROMA")
depends_on("ANTs")
depends_on("FSL")
depends_on("FreeSurfer")
depends_on("SPM")
depends_on("AFNI")
depends_on("C3D")
prepend_path("PATH","/network/lustre/iss01/apps/lang/anaconda/fmriprep/bin")
