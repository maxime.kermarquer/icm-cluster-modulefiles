
-- Author : Maxime KERMARQUER
-- Date : 05/03/2020

whatis("Version: 20.0.0")
whatis("Name: fMRIPrep")
whatis("Description: fMRIPrep is a functional magnetic resonance imaging (fMRI) data preprocessing pipeline that is designed to provide an easily accessible, state-of-the-art interface that is robust to variations in scan acquisition protocols and that requires minimal user input, while providing easily interpretable and comprehensive error and output reporting. It performs basic processing steps (coregistration, normalization, unwarping, noise component extraction, segmentation, skullstripping etc.) providing outputs that can be easily submitted to a variety of group level analyses, including task-based or resting-state fMRI, graph theory measures, surface or volume-based statistics, etc.")
whatis("URL: https://fmriprep.readthedocs.io/en/stable")
help([[https://fmriprep.readthedocs.io/en/stable/]])

--depends_on("libwebp")
--depends_on("svgo")
--depends_on("SPM")

-- https://fmriprep.readthedocs.io/en/stable/installation.html

depends_on("FSL/5.0.10")
depends_on("ANTs/2.2.0")
depends_on("C3D/1.1.0")
depends_on("FreeSurfer/6.0.0")
depends_on("ICA-AROMA")
depends_on("bids-validator/1.4.3")
depends_on("connectome-workbench/1.4.2")
depends_on("AFNI")
depends_on("proxy")


prepend_path("PATH","/network/lustre/iss01/apps/lang/anaconda/3/2019.10/fmriprep/20.0.0/bin")
