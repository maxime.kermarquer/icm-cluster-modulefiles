
-- Author : Martial Bornet
-- Date   : 2022-12-15

whatis("Version: 21.0.2")
whatis("Name: fMRIPrep")
whatis("Description: fMRIPrep is a functional magnetic resonance imaging (fMRI) data preprocessing pipeline that is designed to provide an easily accessible, state-of-the-art interface that is robust to variations in scan acquisition protocols and that requires minimal user input, while providing easily interpretable and comprehensive error and output reporting. It performs basic processing steps (coregistration, normalization, unwarping, noise component extraction, segmentation, skullstripping etc.) providing outputs that can be easily submitted to a variety of group level analyses, including task-based or resting-state fMRI, graph theory measures, surface or volume-based statistics, etc.")
whatis("URL: https://fmriprep.readthedocs.io/en/stable")
help([[https://fmriprep.readthedocs.io/en/stable/]])

--depends_on("libwebp")
--depends_on("svgo")
--depends_on("SPM")

-- https://fmriprep.readthedocs.io/en/stable/installation.html

depends_on("FSL/5.0.10")
depends_on("ANTs/2.4.1")
depends_on("C3D/1.1.0")
depends_on("FreeSurfer/7.1.1")
depends_on("ICA-AROMA/0.4.4-beta")
depends_on("bids-validator/1.4.3")
depends_on("connectome-workbench/1.4.2")
depends_on("AFNI/18.1.05")
depends_on("proxy")


prepend_path("PATH","/network/lustre/iss01/apps/software/scit/fmriprep/21.0.2/bin")
