-- InterViews modulefile
--
-- Author : Sangmin Park
-- Date   : 2023-06-12
--
whatis([[Name : InterViews]])
whatis([[Version : 3.1]])
whatis([[Description : NEURON graphics]])
whatis([[https://www.neuron.yale.edu/neuron/download/compile_linux]])


depends_on("gcc/7.3.0")
depends_on("libice/1.0.9-il7as4v")
depends_on("xproto/7.0.31-tny2g5b")
depends_on("libxext/1.3.3-vytz36j")
depends_on("libtiff/4.0.9-23zhztp")
depends_on("xextproto/7.3.0-jmtrxor")
depends_on("libx11/1.6.5-eknhhrt")

prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/neuron/iv/3.1/x86_64/bin")
prepend_path("LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/neuron/iv/3.1/x86_64/lib")
prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/neuron/iv/3.1/x86_64/lib")
prepend_path("C_INCLUDE_PATH", "/network/lustre/iss01/apps/software/scit/neuron/iv/3.1/include")
prepend_path("CPATH", "/network/lustre/iss01/apps/software/scit/neuron/iv/3.1/include")
prepend_path("INCLUDE_PATH", "/network/lustre/iss01/apps/software/scit/neuron/iv/3.1/include")
