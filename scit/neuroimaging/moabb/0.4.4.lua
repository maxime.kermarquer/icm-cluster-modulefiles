-- -*- lua -*-
-- Date: 22/10/2021
-- Author : Maxime KERMARQUER
--
--

-- Module description
whatis([[Name : histomicstk ]])
whatis([[Version :0.4.4 ]])
whatis([[Short description : The Mother of all BCI Benchmarks allows to:

Build a comprehensive benchmark of popular BCI algorithms applied on an extensive list of freely available EEG datasets.

The code will be made available on github, serving as a reference point for the future algorithmic developments.

Algorithms can be ranked and promoted on a website, providing a clear picture of the different solutions available in the field. ]])
whatis([[Short description : https://pypi.org/project/moabb/#installation http://moabb.neurotechx.com/docs/index.html]])


-- Set up environment variables
prepend_path("PATH", "/network/lustre/iss01/apps/lang/anaconda/3/2021.05/moabb/0.4.4/bin", ":")

