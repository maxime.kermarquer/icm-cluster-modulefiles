--
-- mrdegibbs3D modulefile
--
-- Author      : Martial Bornet
-- Last update : 2023-01-04
--
whatis([[Name : mrdegibbs3D]])
whatis([[Version : 2022-07-15]])
whatis([[Short description : This implements the 3D extension to the unringing method proposed by Kellner et al. (2016)]])

prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/mrdegibbs3D/2022-07-15/bin")
