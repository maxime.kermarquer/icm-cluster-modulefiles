-- 30/08/2018
--
whatis([[Name : mriqc]])
whatis([[Version : 0.14.2]])
whatis([[Short description : MRIQC extracts no-reference IQMs (image quality metrics) from structural (T1w and T2w) and functional MRI (magnetic resonance imaging) data. ]])

depends_on("FSL")
depends_on("AFNI")
depends_on("ANTs")
depends_on("proxy")
depends_on("FreeSurfer")
prepend_path("PATH", "/network/lustre/iss01/apps/lang/anaconda/3/5.2.0/mriqc/bin/", ":")

