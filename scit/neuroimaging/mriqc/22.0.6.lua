--
-- Author      : Martial Bornet
-- Last update : 2022-12-21
--
whatis([[Name : mriqc]])
whatis([[Version : 22.0.6]])
whatis([[Short description : MRIQC provides a series of image processing workflows to extract and compute a series of NR (no-reference), IQMs (image quality metrics) to be used in QAPs (quality assessment protocols) for MRI (magnetic resonance imaging).) from structural (T1w and T2w) and functional MRI (magnetic resonance imaging) data. ]])

depends_on("FSL")
depends_on("AFNI")
depends_on("ANTs")
depends_on("proxy")
depends_on("FreeSurfer")
prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/mriqc/22.0.6/bin", ":")

