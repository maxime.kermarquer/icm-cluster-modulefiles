-- -*- lua -*-
-- Date: 24/12/2018
-- Author : Maxime KERMARQUER
--
--

-- Module description
whatis([[Name : muse]])
whatis([[Version : 1.9.31]])
whatis([[Short description : ]])


-- Built with the following modules
--  gcc/6.4.0
--  mesa/18.1.2-rwyp6rj 
--  mesa-glu/9.0.0-5jxiuvx

-- For module dependencies
depends_on("mesa-glu")

-- Set up environment variables
prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/muse/1.9.31/bin64", ":")
