-- -*- lua -*-
-- Date: 04/06/2019
-- Author : Maxime KERMARQUER
-- Jean Didier LEMARECHAL
--
--

-- Module description
whatis([[Name : muse]])
whatis([[Version : 1.9.34]])
whatis([[Short description : build on knode ]])


-- Built with the following modules
--  gcc/6.4.0
--  mesa/18.1.2-rwyp6rj 
--  mesa-glu/9.0.0-5jxiuvx  

-- For module dependencies
--depends_on("mesa-glu")

-- Set up environment variables
prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/muse/muse/knode/install/1.9.34/bin64", ":")
