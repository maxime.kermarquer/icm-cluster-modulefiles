-- -*- lua -*-
-- Date: 14/01/2020
-- Author : Maxime KERMARQUER
-- Jean Didier LEMARECHAL
--
--

-- Module description
whatis([[Name : muse]])
whatis([[Version : 1.9.35]])
whatis([[Short description : build on knode ]])


-- Built with the following modules
--  gcc/6.4.0
--  mesa/18.1.2-rwyp6rj 
--  mesa-glu/9.0.0-5jxiuvx  

-- For module dependencies
--depends_on("mesa-glu")

-- Set up environment variables
--

-- Warning 
execute {cmd="echo -e \"\\e[1;33m*****************************************************************\\n \\t\\t\\t /!\\ WARNING /!\\ \\n This muse version does not have been tested properly for now.\\n*****************************************************************\\e[0m\"" ,modeA={"load"}} 

prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/muse/knode01/1.9.35/install/bin64", ":")
