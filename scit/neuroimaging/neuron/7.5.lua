-- Neuron modulefile
--
-- Author : Sangmin Park 
-- Date   : 2023-06-12
--
whatis([[Name : Neuron]])
whatis([[Version : 7.5]])
whatis([[Description : The NEURON simulation environment is used in laboratories and classrooms around the world for building and using computational models of neurons and networks of neurons.]])
whatis([[URL : https://neuron.yale.edu/ftp/neuron/versions/v7.5/]])
whatis([[https://www.neuron.yale.edu/neuron/download/compile_linux]])

depends_on("iv/3.1")
depends_on("bison/3.8.2")
depends_on("ncurses/6.1-ree7wyp")
depends_on("readline/7.0-56pj7kg")
depends_on("flex/2.6.4-3edyour")
depends_on("python/3.6")
depends_on("openmpi/4.0.0")

prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/neuron/nrn/7.5/x86_64/bin")
prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/neuron/nrn/7.5/x86_64/lib")
prepend_path("LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/neuron/nrn/7.5/x86_64/lib")
prepend_path("CPATH", "/network/lustre/iss01/apps/software/scit/neuron/nrn/7.5/include")

prepend_path("PYTHONPATH","/network/lustre/iss01/apps/software/scit/neuron/nrn/7.5/lib/python3.6/site-packages")
