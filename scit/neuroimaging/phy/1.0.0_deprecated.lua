help([[
For detailed instructions, go to:
       https://anaconda.org

]])
whatis("Version: 1.0.0")
whatis("Keywords: Python")
whatis("https://github.com/kwikteam/phy")
whatis("Description: phy is an open source neurophysiological data analysis package in Python. It provides features for sorting, analyzing, and visualizing extracellular recordings made with high-density multielectrode arrays containing hundreds to thousands of recording sites.")

prepend_path("PATH","/network/lustre/iss01/apps/lang/anaconda/phy/envs/phy/bin")
prepend_path("LD_LIBRARY_PATH","/network/lustre/iss01/apps/lang/anaconda/phy/envs/phy/lib")
