
-- Author : Martial Bornet
-- Date   : 2023-04-03

whatis("Version: 0.97.1_test")
whatis("Keywords: Spyke interface")
whatis("URL: https://spikeinterface.readthedocs.io/en/latest/")
whatis("Description: SpikeInterface is a Python module to analyze extracellular electrophysiology data.")


-- Installation
--- Anaconda installation - Anaconda3-2021.05-Linux-x86_64.sh
--
-- TODO
--- conda install -c conda-forge -c intel -c spyking-circus spyking-circus
--- pip install colorcet pyopengl qtconsole requests traitlets tqdm joblib click mkdocs dask toolz mtscomp
--- pip install --upgrade https://github.com/cortex-lab/phy/archive/master.zip
--- pip install --upgrade https://github.com/cortex-lab/phylib/archive/master.zip

prepend_path("PATH","/network/lustre/iss01/apps/lang/anaconda/3/2021.05/spykeinterface/0.97.1_test/bin")
