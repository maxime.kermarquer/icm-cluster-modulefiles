-- -*- lua -*-
-- Date: 11/08/2022
-- Author : Maxime KERMARQUER
--

-- Module description
whatis([[Name : WSITools ]])
whatis([[Version : 0.1.0 ]])
whatis([[Short description : Tools for whole slide image (WSI) pre-processing, including tissue detection, patch extraction, annotation parsing etc. ]])
whatis([[URL : https://github.com/smujiang/WSITools ]])



-- Set up environment variables
prepend_path("PATH", "/network/lustre/iss01/apps/lang/anaconda/3/2021.11/wsitools/bin", ":")
prepend_path("PYTHONPATH", "/network/lustre/iss01/apps/lang/anaconda/3/2021.11/wsitools/src-github-WSITools", ":")
prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/lang/anaconda/3/2020.02/openslide/1.1.1/lib", ":")

