-- -*- lua -*-
-- Date: 2022-08-26
-- Author : Maxime KERMARQUER
-- Tests use case NFS
--

-- Module description
whatis([[Name : 2d_unet]])
whatis([[Version : ]])
whatis([[Short description : Test use cases over NFS]])


depends_on("CUDA/9.0")

-- Set up environment variables
--
prepend_path("PATH", "/network/lustre/iss01/apps/lang/anaconda/3/2021.11/bench-nfs-deeplearning/bin", ":")

-- Execute a command
execute {cmd="source activate 2d_unet",modeA={"load"}} 
execute {cmd="conda deactivate ",modeA={"unload"}} 
