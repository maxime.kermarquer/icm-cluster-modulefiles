-- C3D modulefile
-- 24/04/2018

-- dependencies of fmriprep
whatis("Version: 1.1.0")
whatis("URL: https://sourceforge.net/projects/c3d/")
whatis("Description: C3D is a command-line tool for converting 3D images between common file formats. The tool also includes a growing list of commands for image manipulation, such as thresholding and resampling.")
prepend_path("PATH","/network/lustre/iss01/apps/software/scit/c3d/1.1.0/bin/")
prepend_path("LD_LIBRARY_PATH","/network/lustre/iss01/apps/software/scit/c3d/1.1.0/lib/")
