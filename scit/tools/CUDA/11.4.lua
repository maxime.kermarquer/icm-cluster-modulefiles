-- CUDA modulefile
-- 07/09/2021
-- Maxime KERMARQUER
whatis([[Name : CUDA]])
whatis([[Version : 11.4]])
whatis([[Description : CUDA is a parallel computing platform and application programming interface (API) model created by Nvidia. It allows software developers and software engineers to use a CUDA-enabled graphics processing unit (GPU) for general purpose processing – an approach termed GPGPU (General-Purpose computing on Graphics Processing Units).]])

prepend_path("PATH", "/network/lustre/iss01/apps/compilers/cuda/11.4/bin")
prepend_path("CPATH", "/network/lustre/iss01/apps/compilers/cuda/11.4/include")
prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/compilers/cuda/11.4/lib64")
prepend_path("CUDA_PATH", "/network/lustre/iss01/apps/compilers/cuda/11.4")
