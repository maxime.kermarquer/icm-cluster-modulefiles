-- CUDA modulefile
-- 25/04/2018
whatis([[Name : CUDA]])
whatis([[Version : 9.0]])
whatis([[Description : CUDA is a parallel computing platform and application programming interface (API) model created by Nvidia. It allows software developers and software engineers to use a CUDA-enabled graphics processing unit (GPU) for general purpose processing – an approach termed GPGPU (General-Purpose computing on Graphics Processing Units).]])

prepend_path("PATH", "/network/lustre/iss01/apps/compilers/cuda/9.1/bin")
prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/compilers/cuda/9.1/lib64")
