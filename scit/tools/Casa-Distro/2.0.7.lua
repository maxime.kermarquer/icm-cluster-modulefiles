-- -*- lua -*-
-- Date: 02/08/2019
-- Author : Maxime KERMARQUER
--
--

-- Module description
whatis([[Name : Casa-Distro ]])
whatis([[Version : 2.0.7 ]])
whatis([[Short description : It’s a development environment which helps to provide developers with a working development environment for BrainVISA and CATI tools. ]])



depends_on("python/2.7")

set_alias("casa_distro","python /network/lustre/iss01/apps/software/scit/Casa-Distro/2.0.7/casa-distro.zip")
