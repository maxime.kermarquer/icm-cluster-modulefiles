-- -*- lua -*-
-- Date: 06/12/2018
-- Author : Maxime KERMARQUER
--
--

-- Module description
whatis([[Name : DeepLabCut]])
whatis([[Version : 2.0.1]])
whatis([[Short description : DeepLabCut: markerless pose estimation of user-defined body parts with deep learning ]])


-- Set up environment variables
prepend_path("PATH", "/network/lustre/iss01/apps/teams/wyart/anaconda/5.3.0/envs/deeplabcut/bin/", ":")
prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/tools/additional_librairies/gtk+/3.22.30/usr/lib64", ":")

