-- -*- lua -*-
-- Date: 26/04/2021
-- Author : Maxime KERMARQUER
--
--

-- Module description
whatis([[Name : DeepLabCut]])
whatis([[Version : 2.1.10.3 - CPU ]])
whatis([[Short description : DeepLabCut: markerless pose estimation of user-defined body parts with deep learning ]])


-- Set up environment variables
prepend_path("PATH", "/network/lustre/iss01/apps/lang/anaconda/3/2020.11/deeplabcut/2.1.10.3_CPU/bin", ":")
execute{cmd="source activate DLC-CPU-LITE", modeA={"load"}}
execute{cmd="conda deactivate", modeA={"unload"}}

