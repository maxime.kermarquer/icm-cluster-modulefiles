-- -*- lua -*-
-- Date: 13/04/2021
-- Author : Maxime KERMARQUER
--
--

-- Module description
whatis([[Name : DeepLabCut]])
whatis([[Version : 2.1.10.3]])
whatis([[Short description : DeepLabCut: markerless pose estimation of user-defined body parts with deep learning ]])


-- Set up environment variables
prepend_path("PATH", "/network/lustre/iss01/apps/lang/anaconda/3/2020.11/deeplabcut/2.1.10.3/bin", ":")
execute{cmd="source activate DLC-GPU-LITE", modeA={"load"}}
execute{cmd="conda deactivate", modeA={"unload"}}

