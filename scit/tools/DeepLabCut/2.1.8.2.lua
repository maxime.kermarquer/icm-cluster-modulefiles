-- -*- lua -*-
-- Date: 16/09/2020
-- Author : Maxime KERMARQUER
--
--

-- Module description
whatis([[Name : DeepLabCut]])
whatis([[Version : 2.1.8.2]])
whatis([[Short description : DeepLabCut: markerless pose estimation of user-defined body parts with deep learning ]])

depends_on("CUDA/10.0")
depends_on("graphic-libraries/1.0.0")

execute {cmd="sh /network/lustre/iss01/apps/modules/scit/tools/DeepLabCut/spyder-directories-dependencies.sh",modeA={"load"}}

home=os.getenv("HOME")
user=os.getenv("USER")

-- Set up environment variables
setenv( "QT_XKB_CONFIG_ROOT", home .. "/.share/X11/xkb")
setenv( "XDG_RUNTIME_DIR", home .. "/.runtime-" .. user)
prepend_path("PATH", "/network/lustre/iss01/apps/lang/anaconda/3/2019.07/deeplabcut/2.2b8/bin", ":")

