-- -*- lua -*-
--
-- Author : Martial Bornet
-- Date   : 2023-01-20
--
--

-- Module description
whatis([[Name : DeepLabCut]])
whatis([[Version : 2.2.3]])
whatis([[Short description : DeepLabCut: markerless pose estimation of user-defined body parts with deep learning ]])

depends_on("CUDA/10.0")
depends_on("graphic-libraries/1.0.0")
-- depends_on("MATLAB/R2020b")
-- depends_on("gcc/7.3.0")


-- Set up environment variables
prepend_path("PATH", "/network/lustre/iss01/apps/lang/anaconda/3/2019.07/deeplabcut/2.2.3/bin", ":")

-- DeepLabCut Python modules
prepend_path("PYTHONPATH", "/network/lustre/iss01/apps/lang/anaconda/3/2019.07/deeplabcut/2.2.3/DeepLabCut", ":")

-- ========== Dynamic libraries ==========
-- For libQt6Core.so.6 :
-- prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/lang/anaconda/3/2019.07/deeplabcut/2.2.3/envs/DEEPLABCUT/lib/python3.8/site-packages/PySide6/Qt/lib", ":")

-- For libnvinfer.so.7 :
-- prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/lang/matlab/R2020b/bin/glnxa64", ":")

-- For libcudart.so.11.0 :
prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/lang/anaconda/3/2019.07/deeplabcut/2.2.3/envs/DEEPLABCUT/lib/python3.8/site-packages/nvidia/cuda_runtime/lib", ":")
-- prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/lang/anaconda/3/2019.07/deeplabcut/2.2.3/lib/python3.9/site-packages/nvidia/cuda_runtime/lib/libcudart.so.11.0", ":")

-- For DeepLabCut environment :
prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/lang/anaconda/3/2019.07/deeplabcut/2.2.3/envs/DEEPLABCUT/lib", ":")

execute{cmd="source activate DEEPLABCUT", modeA={"load"}}
execute{cmd="source deactivate", modeA={"unload"}}




-- execute {cmd="sh /network/lustre/iss01/apps/modules/scit/tools/DeepLabCut/spyder-directories-dependencies.sh",modeA={"load"}}
-- depends_on("tensorflow/2.0.0")
depends_on("graphic-libraries/1.0.0")

execute {cmd="sh /network/lustre/iss01/apps/modules/scit/tools/DeepLabCut/spyder-directories-dependencies.sh",modeA={"load"}}
depends_on("tensorflow/1.15.2")

-- home=os.getenv("HOME")
-- user=os.getenv("USER")

-- Set up environment variables
prepend_path("PATH", "/network/lustre/iss01/apps/lang/anaconda/3/2019.07/deeplabcut/2.2.3/bin", ":")
execute{cmd="conda init bash", modeA={"load"}}
execute{cmd="conda activate DLC-GPU", modeA={"load"}}
-- execute{cmd="source activate /network/lustre/iss01/apps/lang/anaconda/3/2019.07/deeplabcut/2.2.3/envs/DLC-GPU", modeA={"load"}}
-- execute{cmd="conda deactivate", modeA={"unload"}}

-- setenv( "QT_XKB_CONFIG_ROOT", home .. "/.share/X11/xkb")
-- setenv( "XDG_RUNTIME_DIR", home .. "/.runtime-" .. user)
-- prepend_path("PATH", "/network/lustre/iss01/apps/lang/anaconda/3/2019.07/deeplabcut/2.2.3/bin", ":")

