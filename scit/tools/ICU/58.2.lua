--#%Module1.0
--## Module file created by spack (https://github.com/spack/spack) on 2018-03-14 09:12:19.445813
--##
--## icu4c@60.1%gcc@4.8.5 arch=linux-centos7-x86_64 /4yadato
--##
--## Configure options: --enable-rpath
--##
--
--
--module-whatis "ICU is a mature, widely used set of C/C++ and Java libraries providing Unicode and Globalization support for software applications. ICU4C is the C/C++ interface."
--
--proc ModulesHelp { } {
--puts stderr "ICU is a mature, widely used set of C/C++ and Java libraries providing"
--puts stderr "Unicode and Globalization support for software applications. ICU4C is"
--puts stderr "the C/C++ interface."
--}


--prepend-path PATH "/network/lustre/iss01/apps/software/linux-centos7-x86_64/gcc/4.8.5/icu4c/60.1/4yadatobymglukfhlkasskcetbbwgqxv/bin"
--prepend-path MANPATH "/network/lustre/iss01/apps/software/linux-centos7-x86_64/gcc/4.8.5/icu4c/60.1/4yadatobymglukfhlkasskcetbbwgqxv/share/man"
--prepend-path LIBRARY_PATH "/network/lustre/iss01/apps/software/linux-centos7-x86_64/gcc/4.8.5/icu4c/60.1/4yadatobymglukfhlkasskcetbbwgqxv/lib"
--prepend-path LD_LIBRARY_PATH "/network/lustre/iss01/apps/software/linux-centos7-x86_64/gcc/4.8.5/icu4c/60.1/4yadatobymglukfhlkasskcetbbwgqxv/lib"
--prepend-path CPATH "/network/lustre/iss01/apps/software/linux-centos7-x86_64/gcc/4.8.5/icu4c/60.1/4yadatobymglukfhlkasskcetbbwgqxv/include"
--prepend-path PKG_CONFIG_PATH "/network/lustre/iss01/apps/software/linux-centos7-x86_64/gcc/4.8.5/icu4c/60.1/4yadatobymglukfhlkasskcetbbwgqxv/lib/pkgconfig"
--prepend-path CMAKE_PREFIX_PATH "/network/lustre/iss01/apps/software/linux-centos7-x86_64/gcc/4.8.5/icu4c/60.1/4yadatobymglukfhlkasskcetbbwgqxv/"

whatis([[Name : ICU4C]])
whatis([[Version : 58.2]])
whatis([[Description : ICU is a mature, widely used set of C/C++ and Java libraries providing Unicode and Globalization support for software applications. ICU4C is the C/C++ interface.]])

prepend_path{"PATH","/network/lustre/iss01/apps/software/linux-centos7-x86_64/gcc/4.8.5/icu4c/58.2/5itbw2f2c7o7gxi3azynviabqmiimeqm/bin"}
prepend_path{"MANPATH","/network/lustre/iss01/apps/software/linux-centos7-x86_64/gcc/4.8.5/icu4c/58.2/5itbw2f2c7o7gxi3azynviabqmiimeqm/share/man"}
prepend_path{"LIBRARY_PATH","/network/lustre/iss01/apps/software/linux-centos7-x86_64/gcc/4.8.5/icu4c/58.2/5itbw2f2c7o7gxi3azynviabqmiimeqm/lib"}
prepend_path{"LD_LIBRARY_PATH","/network/lustre/iss01/apps/software/linux-centos7-x86_64/gcc/4.8.5/icu4c/58.2/5itbw2f2c7o7gxi3azynviabqmiimeqm/lib"}
prepend_path{"CPATH","/network/lustre/iss01/apps/software/linux-centos7-x86_64/gcc/4.8.5/icu4c/58.2/5itbw2f2c7o7gxi3azynviabqmiimeqm/include"}
prepend_path{"PKG_CONFIG_PATH","/network/lustre/iss01/apps/software/linux-centos7-x86_64/gcc/4.8.5/icu4c/58.2/5itbw2f2c7o7gxi3azynviabqmiimeqm/lib/pkgconfig"}
--prepend_path{"CMAKE_PREFIX_PATH","/network/lustre/iss01/apps/software/linux-centos7-x86_64/gcc/4.8.5/icu4c/58.2/5itbw2f2c7o7gxi3azynviabqmiimeqm/"}

