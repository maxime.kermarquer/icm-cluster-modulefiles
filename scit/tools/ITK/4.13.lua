-- ITK  modulefile
-- 21/03/2018
whatis([[Name : ITK]])
whatis([[Version : 4.13]])
whatis([[Description : ITK is an open-source, cross-platform system that provides developers with an extensive suite of software tools for image analysis.]])


-- prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/ITK/4.13.0/bin")
setenv("ITK_DIR","/network/lustre/iss01/apps/software/scit/ITK/4.13.0")
prepend_path("LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/ITK/4.13.0/lib")
prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/ITK/4.13.0/lib")
prepend_path("CPATH","/network/lustre/iss01/apps/software/scit/ITK/4.13.0/include/ITK-4.13")
prepend_path("C_INCLUDE_PATH","/network/lustre/iss01/apps/software/scit/ITK/4.13.0/include/ITK-4.13")
prepend_path("CPLUS_INCLUDE_PATH","/network/lustre/iss01/apps/software/scit/ITK/4.13.0/include/ITK-4.13")
