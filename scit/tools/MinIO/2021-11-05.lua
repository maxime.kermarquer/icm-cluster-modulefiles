-- -*- lua -*-
-- Date: 10/11/2021
-- Author : Maxime KERMARQUER
--
--

-- Module description
whatis([[Name : MinIO Client]])
whatis([[Version : 2021-11-05 ]])
whatis([[Short description : MinIO Client (mc) provides a modern alternative to UNIX commands like ls, cat, cp, mirror, diff etc. It supports filesystems and Amazon S3 compatible cloud storage service (AWS Signature v2 and v4).]])
whatis([[Configure options :  https://dl.min.io/client/mc/release/linux-amd64/mc]])

-- Set up environment variables
prepend_path("PATH", "/network/lustre/iss01/apps/tools/mc/2021-11-05/bin", ":")

