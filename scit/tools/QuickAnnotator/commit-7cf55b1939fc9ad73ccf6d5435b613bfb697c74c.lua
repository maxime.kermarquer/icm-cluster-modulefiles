-- -*- lua -*-
-- Date: 24/01/2022
-- Author : Maxime KERMARQUER

-- Module description
whatis([[Name : QuickAnnotator ]])
whatis([[Version : commit 7cf55b1939fc9ad73ccf6d5435b613bfb697c74c]])
whatis([[Short description : Quick Annotator is an open-source digital pathology annotation tool. ]])
whatis([[Configure options : Anaconda3-2021.11 installation + pip3 install -r /network/lustre/iss01/apps/lang/anaconda/3/2021.11/quickannotator/commit-7cf55b1939fc9ad73ccf6d5435b613bfb697c74c/src-github-QuickAnnotator/requirements.txt]])

depends_on("CUDA/10.2")

-- Set up environment variables
prepend_path("PATH", "/network/lustre/iss01/apps/lang/anaconda/3/2021.05/quickannotator/commit-7cf55b1939fc9ad73ccf6d5435b613bfb697c74c/bin", ":")
prepend_path("PATH", "/network/lustre/iss01/apps/lang/anaconda/3/2021.05/quickannotator/icm-script/bin", ":")
--LmodMessage("1. Allocate a GPU node")
--LmodMessage("salloc -p gpu -t 02:00:00 --mem=16G -c 4 --gres=gpu:1 -J 'QuickAnnotate'")
--LmodMessage("2. Connect on it ")
--LmodMessage("ssh $SLURM_NODELIST")
if ( mode() == "load" ) then
    LmodMessage("* Start QuickAnnotator with the command :")
    LmodMessage("run_QuickAnnotator.sh")
end

