-- -*- lua -*-
-- Date: 20/07/2021
-- Author : Maxime KERMARQUER
--

-- Module description
whatis([[Name : ZebraZoom-utilities ]])
whatis([[Version : 1.0.0 ]])



-- Set up environment variables
prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/ZebraZoom-utilities/bin", ":")

