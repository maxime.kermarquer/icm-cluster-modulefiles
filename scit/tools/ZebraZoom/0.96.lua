-- -*- lua -*-
-- Date: 19/07/2021
-- Author : Maxime KERMARQUER
--
--

-- Module description
whatis([[Name : ZebraZoom]])
whatis([[Version : 0.96 ]])
whatis([[Configure options : pip install zebrazoom ]])
help([[ ]])



-- Set up environment variables
prepend_path("PATH", "/network/lustre/iss01/apps/lang/miniconda/zebrazoom/0.96/bin", ":")
