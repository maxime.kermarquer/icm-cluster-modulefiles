-- -*- lua -*-
-- Date: 21/09/2019
-- Author : Maxime KERMARQUER
--
--

-- Module description
whatis([[Name : ZebraZoom]])
whatis([[Version : 1.0.0 ]])
whatis([[Short description : ]])
whatis([[Configure options : ]])
help([[ ]])


depends_on("MATLAB")
depends_on("zlib")
depends_on("libpng")
depends_on("libjpeg")
depends_on("ffmpeg")


-- Set up environment variables
--prepend_path("PATH", "", ":")
--repend_path("MANPATH", "", ":")
prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/tools/opencv/2.4.13.6/lib", ":")
--prepend_path("LIBRARY_PATH", "", ":")
--prepend_path("CMAKE_PREFIX_PATH", "", ":")

