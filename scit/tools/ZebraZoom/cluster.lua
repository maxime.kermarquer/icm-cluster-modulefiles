-- -*- lua -*-
-- Date: 02/04/2019
-- Author : Maxime KERMARQUER
--
--

-- Module description
whatis([[Name : ZebraZoom]])
whatis([[Version : cluster ]])
whatis([[Short description : ]])
whatis([[Configure options : ]])
help([[ ]])


depends_on("MATLAB")
depends_on("zlib")
depends_on("libpng")
depends_on("libjpeg")
depends_on("ffmpeg")


-- Set up environment variables
setenv("ZEBRAZOOM_PATH","/network/lustre/iss01/apps/software/scit/ZebraZoom/cluster")
setenv("ZEBRAZOOM_MATLAB_FILES", "/network/lustre/iss01/apps/software/scit/ZebraZoom/cluster/ZZmatlabAnalysis")
setenv("ZEBRAZOOM_VERSION", "/network/lustre/iss01/apps/software/scit/ZebraZoom/cluster/ZZversion.txt")
prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/ZebraZoom/cluster/bin", ":")
prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/ZebraZoom/cluster/scripts", ":")
prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/tools/opencv/2.4.13.6/lib", ":")
