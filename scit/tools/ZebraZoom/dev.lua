-- -*- lua -*-
-- Date: 22/03/2021
-- Author : Maxime KERMARQUER
--
--

-- Module description
whatis([[Name : ZebraZoom]])
whatis([[Version : dev ]])
whatis([[Configure options : pip install zebrazoom ]])
help([[ ]])



-- Set up environment variables
--setenv("ZEBRAZOOM_PATH","/network/lustre/iss01/apps/software/scit/ZebraZoom/cluster_v2.0")
--setenv("ZEBRAZOOM_MATLAB_FILES", "/network/lustre/iss01/apps/software/scit/ZebraZoom/cluster_v2.0/ZZmatlabAnalysis")
--setenv("ZEBRAZOOM_VERSION", "/network/lustre/iss01/apps/software/scit/ZebraZoom/cluster_v2.0/ZZversion.txt")
--prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/ZebraZoom/cluster_v2.0/scripts", ":")
--prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/tools/opencv/2.4.13.6/lib", ":")
prepend_path("PATH", "/network/lustre/iss01/apps/teams/wyart/anaconda/dev_zebrazoom/bin", ":")
