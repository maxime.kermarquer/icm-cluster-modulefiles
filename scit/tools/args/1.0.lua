-- -*- lua -*-
--
-- Author : Martial Bornet
-- Date   : 2023-03-27
--

-- Module description
whatis([[Name: args]])
whatis([[Version: 1.0]])
whatis([[Description: A program that displays the arguments passed to it]])

prepend_path("PATH", "/network/lustre/iss01/apps/tools/args/1.0/bin", ":")
