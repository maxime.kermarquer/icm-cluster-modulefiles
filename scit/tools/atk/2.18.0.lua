-- -*- lua -*-
--

whatis([[Name : atk]])
whatis([[Version : 2.18.0]])



prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/tools/additional_librairies/atk/2.18.0/lib", ":")
prepend_path("LIBRARY_PATH", "/network/lustre/iss01/apps/tools/additional_librairies/atk/2.18.0/lib", ":")
prepend_path("CPATH", "/network/lustre/iss01/apps/tools/additional_librairies/atk/2.18.0/include", ":")
prepend_path("CPATH", "/network/lustre/iss01/apps/tools/additional_librairies/atk/2.18.0/include/atk-1.0", ":")
prepend_path("PKG_CONFIG_PATH", "/network/lustre/iss01/apps/tools/additional_librairies/atk/2.18.0/lib/pkgconfig", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/tools/additional_librairies/atk/2.18.0/", ":")

