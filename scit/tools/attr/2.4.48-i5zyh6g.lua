-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2023-06-15 14:45:26.654497
--
-- attr@2.4.48%gcc@7.3.0 build_system=autotools arch=linux-centos7-broadwell/i5zyh6g
--

whatis([[Name : attr]])
whatis([[Version : 2.4.48]])
whatis([[Target : broadwell]])
whatis([[Short description : Commands for Manipulating Filesystem Extended Attributes]])
whatis([[Configure options : --disable-static]])

help([[Name   : attr]])
help([[Version: 2.4.48]])
help([[Target : broadwell]])
help()
help([[Commands for Manipulating Filesystem Extended Attributes]])



prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/attr-2.4.48-i5zyh6gopovrvlzi5esv2b6uvv2qfgvw/bin", ":")
prepend_path("CPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/attr-2.4.48-i5zyh6gopovrvlzi5esv2b6uvv2qfgvw/include", ":")
prepend_path("LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/attr-2.4.48-i5zyh6gopovrvlzi5esv2b6uvv2qfgvw/lib", ":")
prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/attr-2.4.48-i5zyh6gopovrvlzi5esv2b6uvv2qfgvw/lib", ":")
prepend_path("MANPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/attr-2.4.48-i5zyh6gopovrvlzi5esv2b6uvv2qfgvw/share/man", ":")
prepend_path("PKG_CONFIG_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/attr-2.4.48-i5zyh6gopovrvlzi5esv2b6uvv2qfgvw/lib/pkgconfig", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/attr-2.4.48-i5zyh6gopovrvlzi5esv2b6uvv2qfgvw/.", ":")

