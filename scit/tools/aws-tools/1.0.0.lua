-- -*- lua -*-
-- Date   : 2021-10-07
-- Author : Maxime KERMARQUER
--
--

-- Module description
whatis([[Name : AWS tools ]])
whatis([[Version : 1.0.0 ]])
whatis([[Short description : boto3 s3cmd ]])

-- Set up environment variables
prepend_path("PATH", "/network/lustre/iss01/apps/tools/aws-tools/bin", ":")

