-- -*- lua -*-
-- Date: 23/03/2020
-- Author : Maxime KERMARQUER
--
--

-- Module description
whatis([[Name : AWS CLI ]])
whatis([[Version : 1.19.34 ]])
whatis([[Short description : ]])

-- Set up environment variables
prepend_path("PATH", "/network/lustre/iss01/apps/tools/awscli/1.19.34/bin", ":")

