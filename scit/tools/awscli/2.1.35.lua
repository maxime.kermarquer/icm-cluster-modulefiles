-- -*- lua -*-
-- Date   : 2021-04-07
-- Author : Martial Bornet
--
--

-- Module description
whatis([[Name : AWS CLI ]])
whatis([[Version : 2.1.35 ]])
whatis([[Short description : ]])

-- Set up environment variables
prepend_path("PATH", "/network/lustre/iss01/apps/tools/awscli/2.1.35/bin", ":")

