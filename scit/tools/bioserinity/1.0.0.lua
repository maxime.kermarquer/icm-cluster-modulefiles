-- -*- lua -*-
-- Date: 16/11/2021
-- Author : Maxime KERMARQUER
--

-- Module description
whatis([[Name : Bioserinity ]])
whatis([[Version : 1.0.0  ]])
whatis([[Short description : BioSerinity Anaconda environment ]])


depends_on("CUDA/11.4")
depends_on("proxy")

-- Set up environment variables
prepend_path("PATH", "/network/lustre/iss01/apps/teams/charpier/bioserinity/anaconda/bin", ":")

