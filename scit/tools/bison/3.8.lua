-- -*- lua -*-
-- Date   : 2022-06-29
-- Author : Martial Bornet
--
--

-- Module description
whatis([[Name: bison]])
whatis([[Version: 3.8]])
whatis([[Description: Parser of the GNU project]])
help([[https://ftp.gnu.org/gnu/bison/]])

prepend_path("PATH", "/network/lustre/iss01/apps/tools/bison/bison-3.8/build/bin", ":")
prepend_path("MANPATH", "/network/lustre/iss01/apps/tools/bison/bison-3.8/build/doc", ":")
if not string.find("$MANPATH", "/usr/share/man") then
	append_path("MANPATH", "/usr/share/man")
end
