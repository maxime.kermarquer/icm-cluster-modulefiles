--

whatis([[Name : boost]])
whatis([[Version : 1.53.0]])
whatis([[Short description : Boost provides free peer-reviewed portable C++ source libraries, emphasizing libraries that work well with the C++ Standard Library.]])

help([[Boost provides free peer-reviewed portable C++ source libraries,
emphasizing libraries that work well with the C++ Standard Library.
Boost libraries are intended to be widely useful, and usable across a
broad spectrum of applications. The Boost license encourages both
commercial and non-commercial use.]])



prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/muse/external/installs/boost_1_53_0/lib", ":")
prepend_path("LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/muse/external/installs/boost_1_53_0/lib", ":")
prepend_path("CPATH", "/network/lustre/iss01/apps/software/scit/muse/external/installs/boost_1_53_0/include", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/muse/external/installs/boost_1_53_0/", ":")

