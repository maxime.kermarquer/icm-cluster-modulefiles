-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2018-09-17 15:51:58.872878
--
-- bzip2@1.0.6%gcc@6.4.0+shared arch=linux-centos7-x86_64 /zfy3jck
--

whatis([[Name : bzip2]])
whatis([[Version : 1.0.6]])
whatis([[Short description : bzip2 is a freely available, patent free high-quality data compressor. It typically compresses files to within 10% to 15% of the best available techniques (the PPM family of statistical compressors), whilst being around twice as fast at compression and six times faster at decompression.]])

help([[bzip2 is a freely available, patent free high-quality data compressor.
It typically compresses files to within 10% to 15% of the best available
techniques (the PPM family of statistical compressors), whilst being
around twice as fast at compression and six times faster at
decompression.]])



prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/bzip2-1.0.6-zfy3jckgmrl5yq44f5hscczcw7k2iihk/bin", ":")
prepend_path("MANPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/bzip2-1.0.6-zfy3jckgmrl5yq44f5hscczcw7k2iihk/man", ":")
prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/bzip2-1.0.6-zfy3jckgmrl5yq44f5hscczcw7k2iihk/lib", ":")
prepend_path("LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/bzip2-1.0.6-zfy3jckgmrl5yq44f5hscczcw7k2iihk/lib", ":")
prepend_path("CPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/bzip2-1.0.6-zfy3jckgmrl5yq44f5hscczcw7k2iihk/include", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/bzip2-1.0.6-zfy3jckgmrl5yq44f5hscczcw7k2iihk/", ":")

