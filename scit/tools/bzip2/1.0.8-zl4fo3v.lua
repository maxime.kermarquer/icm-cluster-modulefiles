-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2023-05-04 14:47:40.286282
--
-- bzip2@1.0.8%gcc@7.3.0~debug~pic+shared build_system=generic arch=linux-centos7-broadwell/zl4fo3v
--

whatis([[Name : bzip2]])
whatis([[Version : 1.0.8]])
whatis([[Target : broadwell]])
whatis([[Short description : bzip2 is a freely available, patent free high-quality data compressor. It typically compresses files to within 10% to 15% of the best available techniques (the PPM family of statistical compressors), whilst being around twice as fast at compression and six times faster at decompression.]])

help([[Name   : bzip2]])
help([[Version: 1.0.8]])
help([[Target : broadwell]])
help()
help([[bzip2 is a freely available, patent free high-quality data compressor.
It typically compresses files to within 10% to 15% of the best available
techniques (the PPM family of statistical compressors), whilst being
around twice as fast at compression and six times faster at
decompression.]])



prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/bzip2-1.0.8-zl4fo3vieedck4w2x53bgnrxdzxesfhc/bin", ":")
prepend_path("CPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/bzip2-1.0.8-zl4fo3vieedck4w2x53bgnrxdzxesfhc/include", ":")
prepend_path("LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/bzip2-1.0.8-zl4fo3vieedck4w2x53bgnrxdzxesfhc/lib", ":")
prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/bzip2-1.0.8-zl4fo3vieedck4w2x53bgnrxdzxesfhc/lib", ":")
prepend_path("MANPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/bzip2-1.0.8-zl4fo3vieedck4w2x53bgnrxdzxesfhc/man", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/bzip2-1.0.8-zl4fo3vieedck4w2x53bgnrxdzxesfhc/.", ":")

