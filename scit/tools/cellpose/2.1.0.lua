-- -*- lua -*-
-- Date:
-- Author : Maxime KERMARQUER
--
--

-- Module description
whatis([[Name : cellpose]])
whatis([[Version : 2.1.0]])
whatis([[Short description :  cellpose is an anatomical segmentation algorithm written in Python 3 by Carsen Stringer and Marius Pachitariu.]])
whatis([[URL: https://cellpose.readthedocs.io/en/latest/index.html]])
whatis([[Configure options : conda create -n cellpose pytorch=1.8.2 cudatoolkit=10.2 -c pytorch-lts
conda activate cellpose
pip install cellpose]])
help([[ ]])


-- Execute a command
execute {cmd="source activate cellpose",modeA={"load"}} 

depends_on("CUDA/10.2")

-- Set up environment variables
prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/cellpose/bin", ":")

