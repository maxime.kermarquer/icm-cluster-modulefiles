-- -*- lua -*-
--
-- Author : Martial Bornet
-- Date   : 2023-05-22
--

-- Module description
whatis([[Name : Clearmap]])
whatis([[Version : 2.1.0]])
whatis([[Short description : ClearMap is a toolbox for the analysis and registration of volumetric data from cleared tissues.]])
whatis("URL: https://github.com/ChristophKirst/ClearMap2/tree/dev")
whatis([[Configure options : ]])
help([[https://christophkirst.github.io/ClearMap2Documentation/html/home.html]])


-- Set up environment variables
prepend_path("PATH", "/network/lustre/iss01/apps/lang/miniconda/2023-05-22/clearmap/2.1.0/bin", ":")
-- prepend_path("PATH", "/network/lustre/iss01/apps/lang/miniconda/2023-05-22/clearmap/2.1.0", ":")
-- execute {cmd="source deactivate ClearMap3",modeA={"load"}} 
