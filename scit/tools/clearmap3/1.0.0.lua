-- -*- lua -*-
-- Date: 21/10/2019
-- Author : Maxime KERMARQUER
--
--

-- Module description
whatis([[Name : ]])
whatis([[Version : ]])
whatis([[Short description : ]])
whatis([[Configure options : ]])
help([[ ]])


-- Set up environment variables
prepend_path("PATH", "/network/lustre/iss01/apps/lang/anaconda/2/4.3.1/clearmap/1.0.0/bin", ":")
--execute {cmd="source deactivate ClearMap3",modeA={"load"}} 
