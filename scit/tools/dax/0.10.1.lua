
-- Author: Maxime KERMARQUER
-- Date : 17/05/2018


whatis("Version: 0.10.1")
whatis("Keywords: Distributed Automation on XNAT")
whatis("URL: https://github.com/VUIIS/dax/wiki")
whatis("Description: Distributed Automation on XNAT (DAX) is a python package developed at Vanderbilt University, Nashville, TN, USA. ")

depends_on("proxy")

prepend_path("PATH","/network/lustre/iss01/apps/lang/anaconda/2/2019.03/dax/bin")
setenv("XNAT_HOST","https://xnat.icm-institute.org")
setenv("PYTHONWARNINGS","ignore:Unverified HTTPS request")
