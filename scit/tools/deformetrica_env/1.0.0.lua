
whatis("Version: 3.5")
whatis("Keywords: Python")
whatis("URL: http://anaconda.org")
whatis("Description: Anaconda environment for deformetrica software")

execute {cmd="source deactivate deformetrica",modeA={"unload"}} 
prepend_path("PATH","/network/lustre/iss01/apps/lang/anaconda/3/deformetrica/bin")
execute {cmd="source activate deformetrica",modeA={"load"}} 
