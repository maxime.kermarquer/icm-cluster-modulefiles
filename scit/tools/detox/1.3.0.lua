-- -*- lua -*-
-- Date   : 2021-06-30
-- Author : Martial Bornet
--
--

-- Module description
whatis([[Name : detox]])
whatis([[Version : 1.3.0]])
whatis([[Short description : ]])
help([[https://github.com/dharple/detox]])


-- Set up environment variables
prepend_path("PATH", "/network/lustre/iss01/apps/tools/detox/1.3.0/bin", ":")
