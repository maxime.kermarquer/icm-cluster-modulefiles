-- -*- lua -*-
-- Date: 16/07/2021
-- Author : Maxime KERMARQUER
--

-- Module description
whatis([[Name : dos2unix ]])
whatis([[Version :6.0.3-7  ]])
whatis([[Short description : ]])


-- Set up environment variables
prepend_path("PATH", "/network/lustre/iss01/apps/tools/dos2unix/6.0.3-7/bin", ":")
prepend_path("MANPATH", "/network/lustre/iss01/apps/tools/dos2unix/6.0.3-7/share", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/tools/dos2unix/6.0.3-7", ":")

