-- -*- lua -*-
-- Date: 17/11/2021
-- Author : Maxime KERMARQUER
--
--

-- Module description
whatis([[Name : eeglab ]])
whatis([[Version : 2021 - commit b92796759f98c97373e9b2086be842a8cffbb329 ]])
whatis([[Short description : ]])
whatis([[Configure options : ]])
help([[ ]])


depends_on("MATLAB/R2020b")
depends_on("proxy")
depends_on("java/8u251")

-- Set up environment variables
prepend_path("MATLABPATH", "/network/lustre/iss01/apps/software/scit/eeglab/2021")

