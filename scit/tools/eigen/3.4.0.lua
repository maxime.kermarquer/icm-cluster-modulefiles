-- -*- lua -*-
--
-- Author      : Martial Bornet
-- Last update : 2022-07-05
--
--

-- Module description
whatis([[Name: eigen]])
whatis([[Version: 3.4.0]])
whatis([[Description: Eigen is a C++ template library for linear algebra: matrices, vectors, numerical solvers, and related algorithms.]])
help([[https://gitlab.com/libeigen/eigen]])

prepend_path("PATH", "/network/lustre/iss02/apps/tools/eigen/3.4.0", ":")
setenv("EIGEN_CFLAGS", "-isystem /network/lustre/iss02/apps/tools/eigen/3.4.0")
