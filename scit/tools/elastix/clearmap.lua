-- -*- lua -*-
-- Date: 23/10/2019
-- Author : Maxime KERMARQUER
--
--

-- Module description
whatis([[Name : Elastix]])
whatis([[Version : 5.0.0 / clearmap custom version]])
whatis([[Short description : Elastix consists of a collection of algorithms that are commonly used to solve medical image registration problems.]])
help([[URL : http://elastix.isi.uu.nl/index.php]])
help([[Main commands: elastix, transformix]])



-- Set up environment variables
prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/elastix/clearmap/bin", ":")
prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/elastix/clearmap/lib", ":")
prepend_path("LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/elastix/clearmap/lib", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/elastix/clearmap", ":")

