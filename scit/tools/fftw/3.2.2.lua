--

whatis([[Name : FFTW]])
whatis([[Version : 3.2.2]])
whatis([[Short description : FFTW is a free collection of fast C routines for computing the Discrete Fourier Transform in one or more dimensions.]])


prepend_path("PATH","/network/lustre/iss01/apps/software/scit/muse/external/installs/fftw-3.2.2/bin",":")
prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/muse/external/installs/fftw-3.2.2/lib", ":")
prepend_path("LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/muse/external/installs/fftw-3.2.2/lib", ":")
prepend_path("CPATH", "/network/lustre/iss01/apps/software/scit/muse/external/installs/fftw-3.2.2/include", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/muse/external/installs/fftw-3.2.2/", ":")

