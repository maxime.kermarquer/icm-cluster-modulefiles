-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2020-03-18 15:50:25.792599
--
-- fftw@3.3.8%gcc@6.4.0+double+float~fma+long_double~mpi~openmp~pfft_patches~quad simd=avx,avx2,sse2 arch=linux-centos7-x86_64 /g7jugkl
--

whatis([[Name : fftw]])
whatis([[Version : 3.3.8]])
whatis([[Short description : FFTW is a C subroutine library for computing the discrete Fourier transform (DFT) in one or more dimensions, of arbitrary input size, and of both real and complex data (as well as of even/odd data, i.e. the discrete cosine/sine transforms or DCT/DST). We believe that FFTW, which is free software, should become the FFT library of choice for most applications.]])

help([[FFTW is a C subroutine library for computing the discrete Fourier
transform (DFT) in one or more dimensions, of arbitrary input size, and
of both real and complex data (as well as of even/odd data, i.e. the
discrete cosine/sine transforms or DCT/DST). We believe that FFTW, which
is free software, should become the FFT library of choice for most
applications.]])



prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/fftw-3.3.8-g7jugklgzun65lpq3ygjbo3bag62zo6e/bin", ":")
prepend_path("MANPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/fftw-3.3.8-g7jugklgzun65lpq3ygjbo3bag62zo6e/share/man", ":")
prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/fftw-3.3.8-g7jugklgzun65lpq3ygjbo3bag62zo6e/lib", ":")
prepend_path("LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/fftw-3.3.8-g7jugklgzun65lpq3ygjbo3bag62zo6e/lib", ":")
prepend_path("CPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/fftw-3.3.8-g7jugklgzun65lpq3ygjbo3bag62zo6e/include", ":")
prepend_path("PKG_CONFIG_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/fftw-3.3.8-g7jugklgzun65lpq3ygjbo3bag62zo6e/lib/pkgconfig", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/fftw-3.3.8-g7jugklgzun65lpq3ygjbo3bag62zo6e/", ":")

