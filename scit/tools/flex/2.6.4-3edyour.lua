-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2021-11-30 16:30:55.576901
--
-- flex@2.6.4%gcc@6.4.0+lex patches=09c22e5c6fef327d3e48eb23f0d610dcd3a35ab9207f12e0f875701c677978d3 arch=linux-centos7-broadwell/3edyour
--

whatis([[Name : flex]])
whatis([[Version : 2.6.4]])
whatis([[Target : broadwell]])
whatis([[Short description : Flex is a tool for generating scanners.]])

help([[Flex is a tool for generating scanners.]])



prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/flex-2.6.4-3edyour2mqrif3cb3t5a4dh624x4o5qi/lib", ":")
prepend_path("LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/flex-2.6.4-3edyour2mqrif3cb3t5a4dh624x4o5qi/lib", ":")
prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/flex-2.6.4-3edyour2mqrif3cb3t5a4dh624x4o5qi/bin", ":")
prepend_path("MANPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/flex-2.6.4-3edyour2mqrif3cb3t5a4dh624x4o5qi/share/man", ":")
prepend_path("C_INCLUDE_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/flex-2.6.4-3edyour2mqrif3cb3t5a4dh624x4o5qi/include", ":")
prepend_path("CPLUS_INCLUDE_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/flex-2.6.4-3edyour2mqrif3cb3t5a4dh624x4o5qi/include", ":")
prepend_path("INCLUDE", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/flex-2.6.4-3edyour2mqrif3cb3t5a4dh624x4o5qi/include", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/flex-2.6.4-3edyour2mqrif3cb3t5a4dh624x4o5qi/", ":")

