-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2019-02-05 18:00:58.668584
--
-- flex@2.6.4%gcc@6.4.0+lex arch=linux-centos7-x86_64 /qbvejk6
--

whatis([[Name : flex]])
whatis([[Version : 2.6.4]])
whatis([[Short description : Flex is a tool for generating scanners.]])

help([[Flex is a tool for generating scanners.]])



prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/flex-2.6.4-qbvejk6e2novgizqjqun4cmn5oy5oqv6/bin", ":")
prepend_path("MANPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/flex-2.6.4-qbvejk6e2novgizqjqun4cmn5oy5oqv6/share/man", ":")
prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/flex-2.6.4-qbvejk6e2novgizqjqun4cmn5oy5oqv6/lib", ":")
prepend_path("LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/flex-2.6.4-qbvejk6e2novgizqjqun4cmn5oy5oqv6/lib", ":")
prepend_path("CPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/flex-2.6.4-qbvejk6e2novgizqjqun4cmn5oy5oqv6/include", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/flex-2.6.4-qbvejk6e2novgizqjqun4cmn5oy5oqv6/", ":")

