-- -*- lua -*-
--
-- Author : Martial Bornet
-- Date   : 2023-03-27
--

-- Module description
whatis([[Name: Free42]])
whatis([[Version: 1.4.77]])
whatis([[Description: An emulator of the HP-42S scientific calculator]])
whatis("URL: https://thomasokken.com/free42/")
help([[https://thomasokken.com/free42/]])

prepend_path("PATH", "/network/lustre/iss01/apps/tools/free42/1.4.77/bin", ":")
