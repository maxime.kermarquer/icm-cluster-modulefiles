-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2018-09-19 11:38:58.530322
--
-- freetype@2.7.1%gcc@6.4.0 arch=linux-centos7-x86_64 /fnguk6y
--

whatis([[Name : freetype]])
whatis([[Version : 2.7.1]])
whatis([[Short description : FreeType is a freely available software library to render fonts. It is written in C, designed to be small, efficient, highly customizable, and portable while capable of producing high-quality output (glyph images) of most vector and bitmap font formats.]])
whatis([[Configure options : --with-harfbuzz=no]])

help([[FreeType is a freely available software library to render fonts. It is
written in C, designed to be small, efficient, highly customizable, and
portable while capable of producing high-quality output (glyph images)
of most vector and bitmap font formats.]])



prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/freetype-2.7.1-fnguk6ypqcpy23j3j2n3l62sjvfd3sf2/bin", ":")
prepend_path("MANPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/freetype-2.7.1-fnguk6ypqcpy23j3j2n3l62sjvfd3sf2/share/man", ":")
prepend_path("ACLOCAL_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/freetype-2.7.1-fnguk6ypqcpy23j3j2n3l62sjvfd3sf2/share/aclocal", ":")
prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/freetype-2.7.1-fnguk6ypqcpy23j3j2n3l62sjvfd3sf2/lib", ":")
prepend_path("LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/freetype-2.7.1-fnguk6ypqcpy23j3j2n3l62sjvfd3sf2/lib", ":")
prepend_path("CPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/freetype-2.7.1-fnguk6ypqcpy23j3j2n3l62sjvfd3sf2/include", ":")
prepend_path("PKG_CONFIG_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/freetype-2.7.1-fnguk6ypqcpy23j3j2n3l62sjvfd3sf2/lib/pkgconfig", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/freetype-2.7.1-fnguk6ypqcpy23j3j2n3l62sjvfd3sf2/", ":")

