-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2022-05-27 15:02:19.976307
--
-- geos@3.8.1%gcc@6.4.0~python~ruby arch=linux-centos7-broadwell/y35hx5z
--

whatis([[Name : geos]])
whatis([[Version : 3.8.1]])
whatis([[Target : broadwell]])
whatis([[Short description : GEOS (Geometry Engine - Open Source) is a C++ port of the Java Topology Suite (JTS). As such, it aims to contain the complete functionality of JTS in C++. This includes all the OpenGIS Simple Features for SQL spatial predicate functions and spatial operators, as well as specific JTS enhanced topology functions.]])
whatis([[Configure options : --disable-ruby --disable-python]])

help([[GEOS (Geometry Engine - Open Source) is a C++ port of the Java Topology
Suite (JTS). As such, it aims to contain the complete functionality of
JTS in C++. This includes all the OpenGIS Simple Features for SQL
spatial predicate functions and spatial operators, as well as specific
JTS enhanced topology functions.]])



prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/geos-3.8.1-y35hx5z4u7dneyncqupzowrsm44t4izu/lib", ":")
prepend_path("LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/geos-3.8.1-y35hx5z4u7dneyncqupzowrsm44t4izu/lib", ":")
prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/geos-3.8.1-y35hx5z4u7dneyncqupzowrsm44t4izu/bin", ":")
prepend_path("C_INCLUDE_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/geos-3.8.1-y35hx5z4u7dneyncqupzowrsm44t4izu/include", ":")
prepend_path("CPLUS_INCLUDE_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/geos-3.8.1-y35hx5z4u7dneyncqupzowrsm44t4izu/include", ":")
prepend_path("INCLUDE", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/geos-3.8.1-y35hx5z4u7dneyncqupzowrsm44t4izu/include", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/geos-3.8.1-y35hx5z4u7dneyncqupzowrsm44t4izu/", ":")

