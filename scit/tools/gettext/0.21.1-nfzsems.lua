-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2023-05-04 16:19:26.206229
--
-- gettext@0.21.1%gcc@7.3.0+bzip2+curses+git~libunistring+libxml2+tar+xz build_system=autotools arch=linux-centos7-broadwell/nfzsems
--

whatis([[Name : gettext]])
whatis([[Version : 0.21.1]])
whatis([[Target : broadwell]])
whatis([[Short description : GNU internationalization (i18n) and localization (l10n) library.]])
whatis([[Configure options : --disable-java --disable-csharp --with-libiconv-prefix=/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/libiconv-1.17-afgvmbc7dt2refbap2qamy32yibwnrcr --with-included-glib --with-included-gettext --with-included-libcroco --without-emacs --with-lispdir=/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/gettext-0.21.1-nfzsemsi3me4hskjjbph4z5wbopxpwk7/share/emacs/site-lisp/gettext --without-cvs --with-ncurses-prefix=/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/ncurses-6.4-6n6u7qv2o4rszzmabo3ykcf6cwuetgzl --with-libxml2-prefix=/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/libxml2-2.10.3-23vb7uitljyf5ugtiduy3fo6qxcy6li6 --with-included-libunistring]])

help([[Name   : gettext]])
help([[Version: 0.21.1]])
help([[Target : broadwell]])
help()
help([[GNU internationalization (i18n) and localization (l10n) library.]])


depends_on("bzip2/1.0.8-zl4fo3v")
depends_on("libiconv/1.17-afgvmbc")
depends_on("libxml2/2.10.3-23vb7ui")
depends_on("ncurses/6.4-6n6u7qv")
depends_on("tar/1.34-hfygofk")
depends_on("xz/5.4.1-pthvj6a")

prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/gettext-0.21.1-nfzsemsi3me4hskjjbph4z5wbopxpwk7/bin", ":")
prepend_path("CPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/gettext-0.21.1-nfzsemsi3me4hskjjbph4z5wbopxpwk7/include", ":")
prepend_path("LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/gettext-0.21.1-nfzsemsi3me4hskjjbph4z5wbopxpwk7/lib", ":")
prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/gettext-0.21.1-nfzsemsi3me4hskjjbph4z5wbopxpwk7/lib", ":")
prepend_path("MANPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/gettext-0.21.1-nfzsemsi3me4hskjjbph4z5wbopxpwk7/share/man", ":")
prepend_path("ACLOCAL_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/gettext-0.21.1-nfzsemsi3me4hskjjbph4z5wbopxpwk7/share/aclocal", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/gettext-0.21.1-nfzsemsi3me4hskjjbph4z5wbopxpwk7/.", ":")

