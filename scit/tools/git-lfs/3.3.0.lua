--
-- Author	: Martial Bornet
-- Date		: 2023-05-24
--
whatis("Name: git-lfs")
whatis("Version : 3.3.0")
whatis("URL : https://github.com/git-lfs/git-lfs")

prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/git-lfs/3.3.0/bin")
