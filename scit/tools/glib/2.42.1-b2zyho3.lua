-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2019-02-06 19:13:33.349871
--
-- glib@2.42.1%gcc@6.4.0~libmount patches=3827277111596a181b4bbef989c36ee95cee6d814c1a99c5f243118605a78cf0,c325997b72a205ad1638bb3e3ba0e5b73e3d32ce63b2d0d3282f3e3a2ff4663c tracing= arch=linux-centos7-x86_64 /b2zyho3
--

whatis([[Name : glib]])
whatis([[Version : 2.42.1]])
whatis([[Short description : GLib provides the core application building blocks for libraries and applications written in C.]])
whatis([[Configure options : --disable-libmount --disable-dtrace --disable-systemtap]])

help([[GLib provides the core application building blocks for libraries and
applications written in C. The GLib package contains a low-level
libraries useful for providing data structure handling for C,
portability wrappers and interfaces for such runtime functionality as an
event loop, threads, dynamic loading and an object system.]])



prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/glib-2.42.1-b2zyho3whk2h7e2gmrllxq74mf4ztitx/bin", ":")
prepend_path("ACLOCAL_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/glib-2.42.1-b2zyho3whk2h7e2gmrllxq74mf4ztitx/share/aclocal", ":")
prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/glib-2.42.1-b2zyho3whk2h7e2gmrllxq74mf4ztitx/lib", ":")
prepend_path("LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/glib-2.42.1-b2zyho3whk2h7e2gmrllxq74mf4ztitx/lib", ":")
prepend_path("CPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/glib-2.42.1-b2zyho3whk2h7e2gmrllxq74mf4ztitx/include", ":")
prepend_path("PKG_CONFIG_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/glib-2.42.1-b2zyho3whk2h7e2gmrllxq74mf4ztitx/lib/pkgconfig", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/glib-2.42.1-b2zyho3whk2h7e2gmrllxq74mf4ztitx/", ":")
prepend_path("CPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/libxml2-2.9.8-m3rdnxm66wlzs5gqsm5pqlzqvideutnk/include/libxml2", ":")

