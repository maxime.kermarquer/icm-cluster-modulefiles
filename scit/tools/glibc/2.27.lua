-- -*- lua -*-
-- Date   : 2022-06-29
-- Author : Martial Bornet
--
--

-- Module description
whatis([[Name: glibc]])
whatis([[Version: 2.27]])
whatis([[Description: GNU C Library]])
help([[https://ftp.gnu.org/gnu/glibc/]])

prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/tools/glibc/glibc-2.27/build", ":")
