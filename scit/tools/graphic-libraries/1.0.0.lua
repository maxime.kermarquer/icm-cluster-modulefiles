-- -*- lua -*-
-- Date: 24/10/2019
-- Author : Maxime KERMARQUER
--

-- Module description
whatis([[Name : graphic-libraries]])
whatis([[Version : 1.0.0]])
whatis([[Short description : Meta module to load main graphic libraries.]])


depends_on("libxcomposite")
depends_on("libxscrnsaver")
--depends_on("mesa-glu")
--depends_on("mesa")
depends_on("libxcursor")
depends_on("libxrandr")
depends_on("mesa")
depends_on("libxi")
depends_on("libxtst")
depends_on("libxdamage")
depends_on("libxrender")
depends_on("libxinerama")
depends_on("libxrender")
