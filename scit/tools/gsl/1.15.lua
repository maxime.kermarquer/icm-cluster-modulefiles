--
--

whatis([[Name : gsl]])
whatis([[Version : 1.15]])
whatis([[Short description : The GNU Scientific Library (GSL) is a numerical library for C and C++ programmers. It is free software under the GNU General Public License. The library provides a wide range of mathematical routines such as random number generators, special functions and least-squares fitting. There are over 1000 functions in total with an extensive test suite.]])

help([[The GNU Scientific Library (GSL) is a numerical library for C and C++
programmers. It is free software under the GNU General Public License.
The library provides a wide range of mathematical routines such as
random number generators, special functions and least-squares fitting.
There are over 1000 functions in total with an extensive test suite.]])



prepend_path("PATH","/network/lustre/iss01/apps/software/scit/muse/external/installs/gsl-1.15/bin", ":")
prepend_path("MANPATH", "/network/lustre/iss01/apps/software/scit/muse/external/installs/gsl-1.15/man", ":")
prepend_path("ACLOCAL_PATH", "/network/lustre/iss01/apps/software/scit/muse/external/installs/gsl-1.15/share/aclocal", ":")
prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/muse/external/installs/gsl-1.15/lib", ":")
prepend_path("LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/muse/external/installs/gsl-1.15/lib", ":")
prepend_path("CPATH", "/network/lustre/iss01/apps/software/scit/muse/external/installs/gsl-1.15/include", ":")
prepend_path("PKG_CONFIG_PATH", "/network/lustre/iss01/apps/software/scit/muse/external/installs/gsl-1.15/lib/pkgconfig", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/muse/external/installs/gsl-1.15/", ":")

