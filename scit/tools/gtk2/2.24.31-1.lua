-- -*- lua -*-
-- Date: 04/03/2020
-- Author : Maxime KERMARQUER
--

-- Module description
whatis([[Name : gtk2 ]])
whatis([[Version : 2.24.31-1 ]])
whatis([[Short description : gtk2 - The GIMP ToolKit (GTK+), a library for creating GUIs for X ]])
whatis([[Configure options : https://centos.pkgs.org/7/centos-x86_64/gtk2-2.24.31-1.el7.x86_64.rpm.html ]])


-- Set up environment variables
prepend_path("PATH", "/network/lustre/iss01/apps/tools/additional_librairies/gtk2/2.24.31-1/bin", ":")
prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/tools/additional_librairies/gtk2/2.24.31-1/lib64", ":")
prepend_path("LIBRARY_PATH", "/network/lustre/iss01/apps/tools/additional_librairies/gtk2/2.24.31-1/lib64", ":")
prepend_path("MANPATH", "/network/lustre/iss01/apps/tools/additional_librairies/gtk2/2.24.31-1/share", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/tools/additional_librairies/gtk2/2.24.31-1", ":")

