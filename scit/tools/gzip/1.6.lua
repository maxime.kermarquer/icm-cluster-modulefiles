-- -*- lua -*-
-- Date: 29/06/2020
-- Author : Martial BORNET
--
--

-- Module description
whatis([[Name : gzip]])
whatis([[Version : 1.6]])
whatis([[Short description : Compression tools]])
-- help()


-- Set up environment variables
prepend_path("PATH", "/network/lustre/iss01/apps/tools/gzip/1.6/bin", ":")
prepend_path("MANPATH", "/network/lustre/iss01/apps/tools/gzip/1.6/src-1.6", ":")
if not string.find("$MANPATH", "/usr/share/man") then
	append_path("MANPATH", "/usr/share/man")
end
