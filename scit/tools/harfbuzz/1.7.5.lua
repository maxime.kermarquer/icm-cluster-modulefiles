-- -*- lua -*-
--
-- Author : Martial Bornet
-- Date   : 2023-05-13
--

whatis([[Name : harfbuzz-magick]])
whatis([[Version : 1.7.5]])
whatis([[Short description : HarfBuzz is a text-shaping engine.]])
whatis([[Configure options : configure / make / make install ]])

help([[HarfBuzz is a text-shaping engine.]])

prepend_path("PATH",            "/network/lustre/iss01/apps/software/scit/harfbuzz/1.7.5/install/bin", ":")
prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/harfbuzz/1.7.5/install/lib", ":")
prepend_path("LIBRARY_PATH",    "/network/lustre/iss01/apps/software/scit/harfbuzz/1.7.5/install/lib", ":")
prepend_path("CPATH",           "/network/lustre/iss01/apps/software/scit/harfbuzz/1.7.5/install/include", ":")

