-- -*- lua -*-
-- Date: 17/10/2018
-- Author : Maxime KERMARQUER
--

-- Module description
whatis([[Name : HDF5]])
whatis([[Version : 1.10.4]])
whatis([[Short description : HDF5 is a data model, library, and file format for storing and managing data. ]])
whatis([[Configure options : Linux 3.10 CentOS 7 x86_64  / https://www.hdfgroup.org/downloads/hdf5/]])


-- Set up environment variables
prepend_path("PATH", "/network/lustre/iss01/apps/tools/hdf5/1.10.4/bin", ":")
prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/tools/hdf5/1.10.4/lib", ":")
prepend_path("LIBRARY_PATH", "/network/lustre/iss01/apps/tools/hdf5/1.10.4/lib", ":")
prepend_path("CPATH", "/network/lustre/iss01/apps/tools/hdf5/1.10.4/include", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/tools/hdf5/1.10.4", ":")

