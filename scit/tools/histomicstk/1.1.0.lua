-- -*- lua -*-
-- Date: 14/10/2021
-- Author : Maxime KERMARQUER
--
--

-- Module description
whatis([[Name : histomicstk ]])
whatis([[Version :1.1.0 ]])
whatis([[Short description : HistomicsTK is a Python package for the analysis of digital pathology images.  ]])
whatis([[Short description : https://pypi.org/project/histomicstk/ https://github.com/DigitalSlideArchive/HistomicsTK ]])

depends_on("java")
depends_on("gdal")
depends_on("proj")

-- Set up environment variables
prepend_path("PATH", "/network/lustre/iss01/apps/lang/anaconda/3/2021.05/histomicstk/1.1.0/bin", ":")

