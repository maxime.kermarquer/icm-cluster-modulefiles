-- -*- lua -*-
-- Date   : 2021-05-10
-- Author : Martial Bornet
--
--

-- Module description
whatis([[Name: hl]])
whatis([[Version: 1.102]])
whatis([[Description: An automatic highlighter using regular expressions]])
help([[https://github.com/mbornet-hl]])

-- Set up environment variables
local own_cfg=pathJoin(os.getenv("HOME"), ".hl.cfg")

prepend_path("PATH", "/network/lustre/iss01/apps/tools/hl/1.102/bin", ":")
prepend_path("PATH", "/network/lustre/iss01/apps/tools/hl/1.102/src-1.102/hl_bin", ":")

prepend_path("MANPATH", "/network/lustre/iss01/apps/tools/hl/1.102/src-1.102", ":")
if not string.find("$MANPATH", "/usr/share/man") then
	append_path("MANPATH", "/usr/share/man")
end

prepend_path("HL_CONF", "/network/lustre/iss01/apps/tools/hl/1.102/src-1.102/config_files", ":")
-- prepend_path("HL_CONF", "/network/lustre/iss01/apps/tools/hl/1.102/src-1.102/config_files/.hl.cfg", ":")
prepend_path("HL_CONF", own_cfg, ":")
prepend_path("HL_CONF_GLOB", "hl_*.cfg:hl.cfg:.hl_*.cfg:.hl.cfg:hl", ":")

setenv("HL_A1", "2B")
setenv("HL_A2", "3c")

setenv("MANPAGER", "/network/lustre/iss01/apps/tools/hl/1.102/src-1.102/scripts/hl_man_pager")
