-- -*- lua -*-
-- Date: 11/02/2020
-- Author : Maxime KERMARQUER
--
--

-- Module description
whatis([[Name: hl]])
whatis([[Version: 1.85]])
whatis([[Description: An automatic highlighter using regular expressions]])
help([[https://github.com/mbornet-hl]])

-- Set up environment variables
prepend_path("PATH", "/network/lustre/iss01/apps/tools/hl/1.85/bin", ":")
--prepend_path("PATH", "/network/lustre/iss01/apps/tools/hl/1.85/bin/hl_bin", ":")

--prepend_path("MANPATH", "/network/lustre/iss01/apps/tools/hl/1.85/src-1.85/man1", ":")
--prepend_path("MANPATH", "/network/lustre/iss01/apps/tools/hl/1.85/src-1.85/man5", ":")
prepend_path("MANPATH", "/network/lustre/iss01/apps/tools/hl/1.85/src-1.85", ":")
if not string.find("$MANPATH", "/usr/share/man") then
	append_path("MANPATH", "/usr/share/man")
end
