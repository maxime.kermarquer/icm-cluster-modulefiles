-- -*- lua -*-
-- Date: 11/02/2020
-- Author : Maxime KERMARQUER
--
--

-- Module description
whatis([[Name: hl]])
whatis([[Version: 1.93]])
whatis([[Description: An automatic highlighter using regular expressions]])
help([[https://github.com/mbornet-hl]])

-- Set up environment variables
local own_cfg=pathJoin(os.getenv("HOME"), ".hl.cfg")

prepend_path("PATH", "/network/lustre/iss01/apps/tools/hl/1.93/bin", ":")
prepend_path("PATH", "/network/lustre/iss01/apps/tools/hl/1.93/src-1.93/hl_bin", ":")

prepend_path("MANPATH", "/network/lustre/iss01/apps/tools/hl/1.93/src-1.93", ":")
if not string.find("$MANPATH", "/usr/share/man") then
	append_path("MANPATH", "/usr/share/man")
end

prepend_path("HL_CONF", "/network/lustre/iss01/apps/tools/hl/1.93/src-1.93/config_files", ":")
-- prepend_path("HL_CONF", "/network/lustre/iss01/apps/tools/hl/1.93/src-1.93/config_files/.hl.cfg", ":")
prepend_path("HL_CONF", own_cfg, ":")
prepend_path("HL_CONF_GLOB", "hl_*.cfg:hl.cfg:.hl_*.cfg:.hl.cfg:hl", ":")

setenv("MANPAGER", "/network/lustre/iss01/apps/tools/hl/1.93/src-1.93/scripts/hl_man_pager")
