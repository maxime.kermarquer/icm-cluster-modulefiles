-- -*- lua -*-
-- Date: 30/11/2021
-- Author : Maxime KERMARQUER
--

-- Module description
whatis([[Name : hovernet inference ]])
whatis([[Version : Commit 19ff0af6d4683e786248ffaf1ff970fe6c7c7dcf - 2021-11-24 ]])
whatis([[Short description : HoVer-Net Tile and WSI processing code for simultaneous nuclear segmentation and classification in histology images.  ]])
help([[ https://github.com/simongraham/hovernet_inference ]])

depends_on("CUDA/9.0")

-- Execute a command
execute {cmd="source activate hovernet ",modeA={"load"}} 
execute {cmd="source deactivate hovernet",modeA={"unload"}} 

-- Set up environment variables
prepend_path("PATH", "/network/lustre/iss01/apps/lang/anaconda/3/2021.11/hovernet/bin", ":")

