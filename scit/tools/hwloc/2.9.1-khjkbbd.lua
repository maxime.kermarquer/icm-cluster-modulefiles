-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2023-05-10 09:30:11.282736
--
-- hwloc@2.9.1%gcc@7.3.0~cairo~cuda~gl~libudev+libxml2~netloc~nvml~oneapi-level-zero~opencl+pci~rocm build_system=autotools libs=shared,static arch=linux-centos7-broadwell/khjkbbd
--

whatis([[Name : hwloc]])
whatis([[Version : 2.9.1]])
whatis([[Target : broadwell]])
whatis([[Short description : The Hardware Locality (hwloc) software project.]])
whatis([[Configure options : --disable-opencl --disable-rsmi --disable-cairo --disable-nvml --disable-gl --disable-cuda --enable-libxml2 --disable-libudev --enable-pci --enable-shared --enable-static]])

help([[Name   : hwloc]])
help([[Version: 2.9.1]])
help([[Target : broadwell]])
help()
help([[The Hardware Locality (hwloc) software project. The Portable Hardware
Locality (hwloc) software package provides a portable abstraction
(across OS, versions, architectures, ...) of the hierarchical topology
of modern architectures, including NUMA memory nodes, sockets, shared
caches, cores and simultaneous multithreading. It also gathers various
system attributes such as cache and memory information as well as the
locality of I/O devices such as network interfaces, InfiniBand HCAs or
GPUs. It primarily aims at helping applications with gathering
information about modern computing hardware so as to exploit it
accordingly and efficiently.]])


depends_on("libpciaccess/0.17-3ypdv5x")
depends_on("libxml2/2.10.3-23vb7ui")
depends_on("ncurses/6.4-6n6u7qv")

prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/hwloc-2.9.1-khjkbbdzxifpffz5nngs6fomtw2cwiws/bin", ":")
prepend_path("MANPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/hwloc-2.9.1-khjkbbdzxifpffz5nngs6fomtw2cwiws/share/man", ":")
prepend_path("PKG_CONFIG_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/hwloc-2.9.1-khjkbbdzxifpffz5nngs6fomtw2cwiws/lib/pkgconfig", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/hwloc-2.9.1-khjkbbdzxifpffz5nngs6fomtw2cwiws/.", ":")

