-- -*- lua -*-
-- Date : 10/05/2023
-- Author : Maxime KERMARQUER
--

whatis([[Name : image-magick]])
whatis([[Version : 6.9.12]])
whatis([[Short description : ImageMagick is a software suite to create, edit, compose, or convert bitmap images.]])
whatis([[Configure options : git clone https://github.com/ImageMagick/ImageMagick6  - commit bc907f8755faa344db07fbfff4a8fdf1f54bf838 ]])
whatis([[Configure options : configure / make / make install ]])

help([[ImageMagick is a software suite to create, edit, compose, or convert
bitmap images.]])



prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/ImageMagick6/install/bin", ":")
prepend_path("MANPATH", "/network/lustre/iss01/apps/software/scit/ImageMagick6/install/share/man", ":")
prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/ImageMagick6/install/lib", ":")
prepend_path("LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/ImageMagick6/install/lib", ":")
prepend_path("CPATH", "/network/lustre/iss01/apps/software/scit/ImageMagick6/install/include", ":")
prepend_path("PKG_CONFIG_PATH", "/network/lustre/iss01/apps/software/scit/ImageMagick6/install/lib/pkgconfig", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/ImageMagick6/install/", ":")

