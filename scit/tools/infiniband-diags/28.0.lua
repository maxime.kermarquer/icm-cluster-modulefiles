-- -*- lua -*-
-- Date: 24/02/2020
-- Author : Martial BORNET
--
--

-- Module description
whatis([[Name : infiniband-diags]])
whatis([[Version : 28.0]])
whatis([[Short description : infiniband diagnostics]])
help([[https://altlinux.pkgs.org/sisyphus/classic-x86_64/infiniband-diags-28.0-alt1.x86_64.rpm.html]])


-- Set up environment variables
prepend_path("PATH", "/network/lustre/iss01/apps/tools/infiniband-diags/28.0/usr/sbin", ":")

prepend_path("MANPATH", "/network/lustre/iss01/apps/tools/infiniband-diags/28.0/usr/share/man/man8", ":")
