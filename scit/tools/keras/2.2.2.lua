help([[
For detailed instructions, go to:
       https://anaconda.org

]])
whatis("Version: 2.2.2 ")
whatis("Keywords: Python")
whatis("URL: http://anaconda.org")
whatis("Description: Keras is a high-level neural networks API, written in Python and capable of running on top of TensorFlow.")

prepend_path("PATH","/network/lustre/iss01/apps/lang/anaconda/3/5.2.0/keras/bin/")
