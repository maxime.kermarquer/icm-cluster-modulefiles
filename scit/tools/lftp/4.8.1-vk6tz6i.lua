-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2019-12-04 23:02:39.562310
--
-- lftp@4.8.1%gcc@6.4.0 arch=linux-centos7-x86_64 /vk6tz6i
--

whatis([[Name : lftp]])
whatis([[Version : 4.8.1]])
whatis([[Short description : LFTP is a sophisticated file transfer program supporting a number of network protocols (ftp, http, sftp, fish, torrent).]])
whatis([[Configure options : --with-expat=/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/expat-2.2.5-etozzgyvfjgsfm756gbcaonunhv4hqma --with-libiconv=/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/libiconv-1.15-hbsac4ys2y3uz6btgbnglxiuqkxkosxf --with-openssl=/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/openssl-1.0.2o-qxfzezxhp4blvxkj6vsnj43vpiqfjs3x --with-readline=/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/readline-7.0-56pj7kgchwfnsrilpn2khnokfqscfjsf --with-zlib=/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/zlib-1.2.11-ufwqfd4zrwuuu7fmguamaqokn5plxyns --disable-dependency-tracking]])

help([[LFTP is a sophisticated file transfer program supporting a number of
network protocols (ftp, http, sftp, fish, torrent).]])



prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/lftp-4.8.1-vk6tz6ieephmpobgzydzw2x7ikjelg4a/bin", ":")
prepend_path("MANPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/lftp-4.8.1-vk6tz6ieephmpobgzydzw2x7ikjelg4a/share/man", ":")
prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/lftp-4.8.1-vk6tz6ieephmpobgzydzw2x7ikjelg4a/lib", ":")
prepend_path("LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/lftp-4.8.1-vk6tz6ieephmpobgzydzw2x7ikjelg4a/lib", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/lftp-4.8.1-vk6tz6ieephmpobgzydzw2x7ikjelg4a/", ":")

