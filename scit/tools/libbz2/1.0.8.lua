-- -*- lua -*-
--
-- Date   : 2022-02-16
-- Author : Martial Bornet
--
--

whatis([[Name : libbz2]])
whatis([[Version : 1.0.8]])
whatis([[Short description : bzip2 is a freely available, patent free (see below), high-quality data compressor.]])

-- help([[]])

prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/libbz2/1.0.8/lib", ":")
prepend_path("LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/libbz2/1.0.8/lib", ":")
prepend_path("MANPATH", "/network/lustre/iss01/apps/software/scit/libbz2/1.0.8/man1", ":")
prepend_path("CPATH", "/network/lustre/iss01/apps/software/scit/libbz2/1.0.8/include", ":")
prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/libbz2/1.0.8/bin", ":")
