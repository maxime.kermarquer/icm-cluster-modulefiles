-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2023-05-15 14:53:10.273103
--
-- libevent@2.1.12%gcc@7.3.0+openssl build_system=autotools arch=linux-centos7-broadwell/de5q4gk
--

whatis([[Name : libevent]])
whatis([[Version : 2.1.12]])
whatis([[Target : broadwell]])
whatis([[Short description : The libevent API provides a mechanism to execute a callback function when a specific event occurs on a file descriptor or after a timeout has been reached. Furthermore, libevent also support callbacks due to signals or regular timeouts.]])
whatis([[Configure options : --enable-openssl]])

help([[Name   : libevent]])
help([[Version: 2.1.12]])
help([[Target : broadwell]])
help()
help([[The libevent API provides a mechanism to execute a callback function
when a specific event occurs on a file descriptor or after a timeout has
been reached. Furthermore, libevent also support callbacks due to
signals or regular timeouts.]])


depends_on("openssl/1.1.1t-f5klqzu")

prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/libevent-2.1.12-de5q4gk7vzmojm3aqz5bkfeijm6ljpeo/bin", ":")
prepend_path("PKG_CONFIG_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/libevent-2.1.12-de5q4gk7vzmojm3aqz5bkfeijm6ljpeo/lib/pkgconfig", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/libevent-2.1.12-de5q4gk7vzmojm3aqz5bkfeijm6ljpeo/.", ":")

