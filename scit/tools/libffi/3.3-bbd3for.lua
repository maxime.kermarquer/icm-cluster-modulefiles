-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2023-05-09 16:24:38.828016
--
-- libffi@3.3%gcc@7.3.0 build_system=autotools patches=26f26c6 arch=linux-centos7-broadwell/bbd3for
--

whatis([[Name : libffi]])
whatis([[Version : 3.3]])
whatis([[Target : broadwell]])
whatis([[Short description : The libffi library provides a portable, high level programming interface to various calling conventions. This allows a programmer to call any function specified by a call interface description at run time.]])
whatis([[Configure options : --without-gcc-arch]])

help([[Name   : libffi]])
help([[Version: 3.3]])
help([[Target : broadwell]])
help()
help([[The libffi library provides a portable, high level programming interface
to various calling conventions. This allows a programmer to call any
function specified by a call interface description at run time.]])



prepend_path("CPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/libffi-3.3-bbd3foradcc7oyrn3c2qntxnoq4imvvn/include", ":")
prepend_path("LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/libffi-3.3-bbd3foradcc7oyrn3c2qntxnoq4imvvn/lib64", ":")
prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/libffi-3.3-bbd3foradcc7oyrn3c2qntxnoq4imvvn/lib64", ":")
prepend_path("MANPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/libffi-3.3-bbd3foradcc7oyrn3c2qntxnoq4imvvn/share/man", ":")
prepend_path("PKG_CONFIG_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/libffi-3.3-bbd3foradcc7oyrn3c2qntxnoq4imvvn/lib/pkgconfig", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/libffi-3.3-bbd3foradcc7oyrn3c2qntxnoq4imvvn/.", ":")

