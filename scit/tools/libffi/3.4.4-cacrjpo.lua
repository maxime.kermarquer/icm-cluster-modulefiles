-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2023-05-04 15:46:11.932709
--
-- libffi@3.4.4%gcc@7.3.0 build_system=autotools arch=linux-centos7-broadwell/cacrjpo
--

whatis([[Name : libffi]])
whatis([[Version : 3.4.4]])
whatis([[Target : broadwell]])
whatis([[Short description : The libffi library provides a portable, high level programming interface to various calling conventions. This allows a programmer to call any function specified by a call interface description at run time.]])
whatis([[Configure options : --without-gcc-arch]])

help([[Name   : libffi]])
help([[Version: 3.4.4]])
help([[Target : broadwell]])
help()
help([[The libffi library provides a portable, high level programming interface
to various calling conventions. This allows a programmer to call any
function specified by a call interface description at run time.]])



prepend_path("CPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/libffi-3.4.4-cacrjpo6tmxilcbpni4frcdmvtoocx36/include", ":")
prepend_path("LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/libffi-3.4.4-cacrjpo6tmxilcbpni4frcdmvtoocx36/lib64", ":")
prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/libffi-3.4.4-cacrjpo6tmxilcbpni4frcdmvtoocx36/lib64", ":")
prepend_path("MANPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/libffi-3.4.4-cacrjpo6tmxilcbpni4frcdmvtoocx36/share/man", ":")
prepend_path("PKG_CONFIG_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/libffi-3.4.4-cacrjpo6tmxilcbpni4frcdmvtoocx36/lib/pkgconfig", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/libffi-3.4.4-cacrjpo6tmxilcbpni4frcdmvtoocx36/.", ":")

