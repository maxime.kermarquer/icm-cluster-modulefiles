-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2019-05-15 16:25:44.064335
--
-- libgd@2.2.4%gcc@6.4.0 arch=linux-centos7-x86_64 /uvcimer
--

whatis([[Name : libgd]])
whatis([[Version : 2.2.4]])
whatis([[Short description : GD is an open source code library for the dynamic creation of images by programmers. GD is written in C, and 'wrappers' are available for Perl, PHP and other languages. GD creates PNG, JPEG, GIF, WebP, XPM, BMP images, among other formats. GD is commonly used to generate charts, graphics, thumbnails, and most anything else, on the fly. While not restricted to use on the web, the most common applications of GD involve website development.]])

help([[GD is an open source code library for the dynamic creation of images by
programmers. GD is written in C, and "wrappers" are available for Perl,
PHP and other languages. GD creates PNG, JPEG, GIF, WebP, XPM, BMP
images, among other formats. GD is commonly used to generate charts,
graphics, thumbnails, and most anything else, on the fly. While not
restricted to use on the web, the most common applications of GD involve
website development.]])



prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/libgd-2.2.4-uvcimeravucybmsh3eyny6yxrfqoufvs/bin", ":")
prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/libgd-2.2.4-uvcimeravucybmsh3eyny6yxrfqoufvs/lib", ":")
prepend_path("LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/libgd-2.2.4-uvcimeravucybmsh3eyny6yxrfqoufvs/lib", ":")
prepend_path("CPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/libgd-2.2.4-uvcimeravucybmsh3eyny6yxrfqoufvs/include", ":")
prepend_path("PKG_CONFIG_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/libgd-2.2.4-uvcimeravucybmsh3eyny6yxrfqoufvs/lib/pkgconfig", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/libgd-2.2.4-uvcimeravucybmsh3eyny6yxrfqoufvs/", ":")
prepend_path("CPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/libxml2-2.9.8-m3rdnxm66wlzs5gqsm5pqlzqvideutnk/include/libxml2", ":")
prepend_path("CPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/freetype-2.7.1-fnguk6ypqcpy23j3j2n3l62sjvfd3sf2/include/freetype2", ":")

