-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2023-05-04 14:42:43.077488
--
-- libiconv@1.17%gcc@7.3.0 build_system=autotools libs=shared,static arch=linux-centos7-broadwell/afgvmbc
--

whatis([[Name : libiconv]])
whatis([[Version : 1.17]])
whatis([[Target : broadwell]])
whatis([[Short description : GNU libiconv provides an implementation of the iconv() function and the iconv program for character set conversion.]])
whatis([[Configure options : --enable-extra-encodings --enable-shared --enable-static --with-pic]])

help([[Name   : libiconv]])
help([[Version: 1.17]])
help([[Target : broadwell]])
help()
help([[GNU libiconv provides an implementation of the iconv() function and the
iconv program for character set conversion.]])



prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/libiconv-1.17-afgvmbc7dt2refbap2qamy32yibwnrcr/bin", ":")
prepend_path("CPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/libiconv-1.17-afgvmbc7dt2refbap2qamy32yibwnrcr/include", ":")
prepend_path("LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/libiconv-1.17-afgvmbc7dt2refbap2qamy32yibwnrcr/lib", ":")
prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/libiconv-1.17-afgvmbc7dt2refbap2qamy32yibwnrcr/lib", ":")
prepend_path("MANPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/libiconv-1.17-afgvmbc7dt2refbap2qamy32yibwnrcr/share/man", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/libiconv-1.17-afgvmbc7dt2refbap2qamy32yibwnrcr/.", ":")

