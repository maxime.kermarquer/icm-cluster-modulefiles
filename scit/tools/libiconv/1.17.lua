-- -*- lua -*-
--
-- libiconv 1.17
--
-- Author      : Martial Bornet
-- Last update : 2023-05-23
--
-- The library has been generated usign gcc 7.3.0.
--

whatis([[Name : libiconv]])
whatis([[Version : 1.17]])
whatis([[Target : broadwell]])
whatis([[Short description : GNU libiconv provides an iconv() implementation, for use on systems which don't have one, or whose implementation cannot convert from/to Unicode.
]])
whatis([[Configure options : --enable-extra-encodings]])

help([[GNU libiconv provides an iconv() implementation, for use on systems which don't have one, or whose implementation cannot convert from/to Unicode.]])



prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/libiconv/libiconv-1.17/lib", ":")
prepend_path("LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/libiconv/libiconv-1.17/lib", ":")
prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/libiconv/libiconv-1.17/bin", ":")
prepend_path("MANPATH", "/network/lustre/iss01/apps/software/scit/libiconv/libiconv-1.17/share/man", ":")
prepend_path("C_INCLUDE_PATH", "/network/lustre/iss01/apps/software/scit/libiconv/libiconv-1.17/include", ":")
prepend_path("CPLUS_INCLUDE_PATH", "/network/lustre/iss01/apps/software/scit/libiconv/libiconv-1.17/include", ":")
prepend_path("INCLUDE", "/network/lustre/iss01/apps/software/scit/libiconv/libiconv-1.17/include", ":")
-- prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/libiconv/libiconv-1.17/", ":")
