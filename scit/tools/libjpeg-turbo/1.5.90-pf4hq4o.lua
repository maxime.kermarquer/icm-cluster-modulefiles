-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2018-09-19 11:43:41.771468
--
-- libjpeg-turbo@1.5.90%gcc@6.4.0 arch=linux-centos7-x86_64 /pf4hq4o
--

whatis([[Name : libjpeg-turbo]])
whatis([[Version : 1.5.90]])
whatis([[Short description : libjpeg-turbo is a fork of the original IJG libjpeg which uses SIMD to accelerate baseline JPEG compression and decompression. libjpeg is a library that implements JPEG image encoding, decoding and transcoding.]])

help([[libjpeg-turbo is a fork of the original IJG libjpeg which uses SIMD to
accelerate baseline JPEG compression and decompression. libjpeg is a
library that implements JPEG image encoding, decoding and transcoding.]])



prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/libjpeg-turbo-1.5.90-pf4hq4ohh4p336waifios54z7txbhzy3/bin", ":")
prepend_path("MANPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/libjpeg-turbo-1.5.90-pf4hq4ohh4p336waifios54z7txbhzy3/share/man", ":")
prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/libjpeg-turbo-1.5.90-pf4hq4ohh4p336waifios54z7txbhzy3/lib64", ":")
prepend_path("LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/libjpeg-turbo-1.5.90-pf4hq4ohh4p336waifios54z7txbhzy3/lib64", ":")
prepend_path("CPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/libjpeg-turbo-1.5.90-pf4hq4ohh4p336waifios54z7txbhzy3/include", ":")
prepend_path("PKG_CONFIG_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/libjpeg-turbo-1.5.90-pf4hq4ohh4p336waifios54z7txbhzy3/lib64/pkgconfig", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/libjpeg-turbo-1.5.90-pf4hq4ohh4p336waifios54z7txbhzy3/", ":")

