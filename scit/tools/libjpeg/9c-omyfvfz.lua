-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2019-02-05 18:41:12.836264
--
-- libjpeg@9c%gcc@6.4.0 arch=linux-centos7-x86_64 /omyfvfz
--

whatis([[Name : libjpeg]])
whatis([[Version : 9c]])
whatis([[Short description : libjpeg is a widely used free library with functions for handling the JPEG image data format. It implements a JPEG codec (encoding and decoding) alongside various utilities for handling JPEG data.]])

help([[libjpeg is a widely used free library with functions for handling the
JPEG image data format. It implements a JPEG codec (encoding and
decoding) alongside various utilities for handling JPEG data.]])



prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/libjpeg-9c-omyfvfzzd6ge2lv4biciadhqg2in2hvg/bin", ":")
prepend_path("MANPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/libjpeg-9c-omyfvfzzd6ge2lv4biciadhqg2in2hvg/share/man", ":")
prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/libjpeg-9c-omyfvfzzd6ge2lv4biciadhqg2in2hvg/lib", ":")
prepend_path("LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/libjpeg-9c-omyfvfzzd6ge2lv4biciadhqg2in2hvg/lib", ":")
prepend_path("CPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/libjpeg-9c-omyfvfzzd6ge2lv4biciadhqg2in2hvg/include", ":")
prepend_path("PKG_CONFIG_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/libjpeg-9c-omyfvfzzd6ge2lv4biciadhqg2in2hvg/lib/pkgconfig", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/libjpeg-9c-omyfvfzzd6ge2lv4biciadhqg2in2hvg/", ":")

