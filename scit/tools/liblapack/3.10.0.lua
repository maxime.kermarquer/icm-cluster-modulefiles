-- -*- lua -*-
-- Date   : 2022-01-26
-- Author : Martial Bornet
--
--

-- Module description
whatis([[Name : liblapack]])
whatis([[Version : 3.10.0]])
whatis([[LAPACK is a library of Fortran subroutines for solving the most commonly occurring problems in numerical linear algebra.]])
help([[https://github.com/Reference-LAPACK/lapack]])


-- Set up environment variables

prepend_path("PATH", "/network/lustre/iss01/apps/tools/liblapack/3.10.0", ":")

