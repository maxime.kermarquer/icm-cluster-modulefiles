-- -*- lua -*-
--
-- Date   : 2022-02-18
-- Author : Martial Bornet
--
--

whatis([[Name : liblzma]])
whatis([[Version : 4.32.7]])
whatis([[Short description : LZMA Utils are legacy data compression software with high compression ratio]])

-- help([[]])

prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/liblzma/4.32.7/lib", ":")
prepend_path("LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/liblzma/4.32.7/lib", ":")
prepend_path("MANPATH", "/network/lustre/iss01/apps/software/scit/liblzma/4.32.7/man1", ":")
prepend_path("CPATH", "/network/lustre/iss01/apps/software/scit/liblzma/4.32.7/include", ":")
prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/liblzma/4.32.7/bin", ":")
