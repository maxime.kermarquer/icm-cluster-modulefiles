-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2018-09-19 11:44:36.069699
--
-- libmng@2.0.3%gcc@6.4.0 arch=linux-centos7-x86_64 /pi4jlow
--

whatis([[Name : libmng]])
whatis([[Version : 2.0.3]])
whatis([[Short description : libmng -THE reference library for reading, displaying, writing and examining Multiple-Image Network Graphics. MNG is the animation extension to the popular PNG image-format.]])

help([[libmng -THE reference library for reading, displaying, writing and
examining Multiple-Image Network Graphics. MNG is the animation
extension to the popular PNG image-format.]])



prepend_path("MANPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/libmng-2.0.3-pi4jlowerw3gne56nm5hxloclb623emc/share/man", ":")
prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/libmng-2.0.3-pi4jlowerw3gne56nm5hxloclb623emc/lib", ":")
prepend_path("LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/libmng-2.0.3-pi4jlowerw3gne56nm5hxloclb623emc/lib", ":")
prepend_path("CPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/libmng-2.0.3-pi4jlowerw3gne56nm5hxloclb623emc/include", ":")
prepend_path("PKG_CONFIG_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/libmng-2.0.3-pi4jlowerw3gne56nm5hxloclb623emc/lib/pkgconfig", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/libmng-2.0.3-pi4jlowerw3gne56nm5hxloclb623emc/", ":")

