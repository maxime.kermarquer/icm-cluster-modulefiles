-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2023-05-10 09:27:58.359132
--
-- libpciaccess@0.17%gcc@7.3.0 build_system=autotools arch=linux-centos7-broadwell/3ypdv5x
--

whatis([[Name : libpciaccess]])
whatis([[Version : 0.17]])
whatis([[Target : broadwell]])
whatis([[Short description : Generic PCI access library.]])

help([[Name   : libpciaccess]])
help([[Version: 0.17]])
help([[Target : broadwell]])
help()
help([[Generic PCI access library.]])



prepend_path("PKG_CONFIG_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/libpciaccess-0.17-3ypdv5xqfbeoeiz7t3ibfnajuxgtvuiz/lib/pkgconfig", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/libpciaccess-0.17-3ypdv5xqfbeoeiz7t3ibfnajuxgtvuiz/.", ":")

