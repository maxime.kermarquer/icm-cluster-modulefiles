-- -*- lua -*-
--
-- Author      : Martial Bornet
-- Last update : 2023-03-31
--


-- Module description
whatis([[Name : libpcre2-8]])
whatis([[Version : 2.8]])
whatis([[Short description : The PCRE2 library is a set of C functions that implement regular expression pattern matching using the same syntax and semantics as Perl 5]])
whatis([[URL: https://github.com/PCRE2Project/pcre2]])

-- Set up environment variables
prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/libpcre2-8", ":")
prepend_path("LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/libpcre2-8", ":")
