-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2018-09-19 10:56:11.970400
--
-- libpthread-stubs@0.4%gcc@6.4.0 arch=linux-centos7-x86_64 /gdly2jb
--

whatis([[Name : libpthread-stubs]])
whatis([[Version : 0.4]])
whatis([[Short description : The libpthread-stubs package provides weak aliases for pthread functions not provided in libc or otherwise available by default.]])

help([[The libpthread-stubs package provides weak aliases for pthread functions
not provided in libc or otherwise available by default.]])



prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/libpthread-stubs-0.4-gdly2jb23sls2pj36p7u7xaqv33typld/lib", ":")
prepend_path("LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/libpthread-stubs-0.4-gdly2jb23sls2pj36p7u7xaqv33typld/lib", ":")
prepend_path("PKG_CONFIG_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/libpthread-stubs-0.4-gdly2jb23sls2pj36p7u7xaqv33typld/lib/pkgconfig", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/libpthread-stubs-0.4-gdly2jb23sls2pj36p7u7xaqv33typld/", ":")

