-- -*- lua -*-
-- Date: 18/03/2020
-- Author : Maxime KERMARQUER
--

-- Module description
whatis([[Name : libsixel]])
whatis([[Version : 1.8.6]])
whatis([[Short description : This package provides encoder/decoder implementation for DEC SIXEL graphics, and some converter programs. SIXEL is one of image formats for printer and terminal imaging introduced by Digital Equipment Corp. (DEC). Its data scheme is represented as a terminal-friendly escape sequence. So if you want to view a SIXEL image file, all you have to do is “cat” it to your terminal.]])

-- Set up environment variables
prepend_path("PATH", "/network/lustre/iss01/apps/tools/additional_librairies/libsixel/1.8.6/bin", ":")
prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/tools/additional_librairies/libsixel/1.8.6/lib", ":")
prepend_path("LIBRARY_PATH", "/network/lustre/iss01/apps/tools/additional_librairies/libsixel/1.8.6/lib", ":")
prepend_path("CPATH", "/network/lustre/iss01/apps/tools/additional_librairies/libsixel/1.8.6/include", ":")
prepend_path("MANPATH", "/network/lustre/iss01/apps/tools/additional_librairies/libsixel/1.8.6/share", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/tools/additional_librairies/libsixel/1.8.6", ":")

