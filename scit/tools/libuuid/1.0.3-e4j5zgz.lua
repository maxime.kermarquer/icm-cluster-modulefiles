-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2023-04-28 15:57:23.826405
--
-- libuuid@1.0.3%gcc@6.4.0 build_system=autotools arch=linux-centos7-broadwell/e4j5zgz
--

whatis([[Name : libuuid]])
whatis([[Version : 1.0.3]])
whatis([[Target : broadwell]])
whatis([[Short description : Portable uuid C library]])

help([[Name   : libuuid]])
help([[Version: 1.0.3]])
help([[Target : broadwell]])
help()
help([[Portable uuid C library]])



prepend_path("PKG_CONFIG_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/libuuid-1.0.3-e4j5zgzeknmh5vi7tx62hzn7ca7l4336/lib/pkgconfig", ":")
prepend_path("CPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/libuuid-1.0.3-e4j5zgzeknmh5vi7tx62hzn7ca7l4336/include", ":")
prepend_path("LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/libuuid-1.0.3-e4j5zgzeknmh5vi7tx62hzn7ca7l4336/lib", ":")
prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/libuuid-1.0.3-e4j5zgzeknmh5vi7tx62hzn7ca7l4336/lib", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/libuuid-1.0.3-e4j5zgzeknmh5vi7tx62hzn7ca7l4336/.", ":")

