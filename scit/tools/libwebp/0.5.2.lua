-- WebP modulefile
-- 24/04/2018
whatis([[Name : WebP]])
whatis([[Version : 0.5.2]])
whatis([[Description : WebP is a modern image format that provides superior lossless and lossy compression for images on the web. ]])


prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/libwebp/0.5.2-linux-x86-64/bin")
prepend_path("LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/libwebp/0.5.2-linux-x86-64/lib")
prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/libwebp/0.5.2-linux-x86-64/lib")
prepend_path("CPATH", "/network/lustre/iss01/apps/software/scit/libwebp/0.5.2-linux-x86-64/include")
