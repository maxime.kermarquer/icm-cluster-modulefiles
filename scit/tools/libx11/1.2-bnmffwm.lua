-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2019-03-20 15:38:27.825180
--
-- libx11@1.2%gcc@6.4.0 arch=linux-centos7-x86_64 /bnmffwm
--

whatis([[Name : libx11]])
whatis([[Version : 1.2]])
whatis([[Short description : libX11 - Core X11 protocol client library.]])

help([[libX11 - Core X11 protocol client library.]])



prepend_path("MANPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/libx11-1.2-bnmffwmirmpwyuvddluv2fcbyeyqayza/share/man", ":")
prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/libx11-1.2-bnmffwmirmpwyuvddluv2fcbyeyqayza/lib", ":")
prepend_path("LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/libx11-1.2-bnmffwmirmpwyuvddluv2fcbyeyqayza/lib", ":")
prepend_path("CPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/libx11-1.2-bnmffwmirmpwyuvddluv2fcbyeyqayza/include", ":")
prepend_path("PKG_CONFIG_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/libx11-1.2-bnmffwmirmpwyuvddluv2fcbyeyqayza/lib/pkgconfig", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/libx11-1.2-bnmffwmirmpwyuvddluv2fcbyeyqayza/", ":")

