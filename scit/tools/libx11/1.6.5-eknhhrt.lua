-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2018-09-19 10:58:04.328642
--
-- libx11@1.6.5%gcc@6.4.0 arch=linux-centos7-x86_64 /eknhhrt
--

whatis([[Name : libx11]])
whatis([[Version : 1.6.5]])
whatis([[Short description : libX11 - Core X11 protocol client library.]])

help([[libX11 - Core X11 protocol client library.]])



prepend_path("MANPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/libx11-1.6.5-eknhhrtlelb6wgc4vf4z5rh4s6dzn4gx/share/man", ":")
prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/libx11-1.6.5-eknhhrtlelb6wgc4vf4z5rh4s6dzn4gx/lib", ":")
prepend_path("LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/libx11-1.6.5-eknhhrtlelb6wgc4vf4z5rh4s6dzn4gx/lib", ":")
prepend_path("CPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/libx11-1.6.5-eknhhrtlelb6wgc4vf4z5rh4s6dzn4gx/include", ":")
prepend_path("PKG_CONFIG_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/libx11-1.6.5-eknhhrtlelb6wgc4vf4z5rh4s6dzn4gx/lib/pkgconfig", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/libx11-1.6.5-eknhhrtlelb6wgc4vf4z5rh4s6dzn4gx/", ":")

