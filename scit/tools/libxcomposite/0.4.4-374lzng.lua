-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2019-09-27 09:37:40.430077
--
-- libxcomposite@0.4.4%gcc@6.4.0 arch=linux-centos7-x86_64 /374lzng
--

whatis([[Name : libxcomposite]])
whatis([[Version : 0.4.4]])
whatis([[Short description : libXcomposite - client library for the Composite extension to the X11 protocol.]])

help([[libXcomposite - client library for the Composite extension to the X11
protocol.]])



prepend_path("MANPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/libxcomposite-0.4.4-374lzng5i7nd2gxfw5lw4ljxnhr2qrfg/share/man", ":")
prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/libxcomposite-0.4.4-374lzng5i7nd2gxfw5lw4ljxnhr2qrfg/lib", ":")
prepend_path("LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/libxcomposite-0.4.4-374lzng5i7nd2gxfw5lw4ljxnhr2qrfg/lib", ":")
prepend_path("CPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/libxcomposite-0.4.4-374lzng5i7nd2gxfw5lw4ljxnhr2qrfg/include", ":")
prepend_path("PKG_CONFIG_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/libxcomposite-0.4.4-374lzng5i7nd2gxfw5lw4ljxnhr2qrfg/lib/pkgconfig", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/libxcomposite-0.4.4-374lzng5i7nd2gxfw5lw4ljxnhr2qrfg/", ":")

