-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2023-05-04 16:19:53.514518
--
-- libxcrypt@4.4.33%gcc@7.3.0~obsolete_api build_system=autotools arch=linux-centos7-broadwell/v3yo2l4
--

whatis([[Name : libxcrypt]])
whatis([[Version : 4.4.33]])
whatis([[Target : broadwell]])
whatis([[Short description : libxcrypt is a modern library for one-way hashing of passwords.]])
whatis([[Configure options : ac_cv_path_python3_passlib=not found --disable-werror --disable-obsolete-api]])

help([[Name   : libxcrypt]])
help([[Version: 4.4.33]])
help([[Target : broadwell]])
help()
help([[libxcrypt is a modern library for one-way hashing of passwords.]])



prepend_path("MANPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/libxcrypt-4.4.33-v3yo2l42q5mchoftxbjj3voisxmbapxf/share/man", ":")
prepend_path("CPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/libxcrypt-4.4.33-v3yo2l42q5mchoftxbjj3voisxmbapxf/include", ":")
prepend_path("LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/libxcrypt-4.4.33-v3yo2l42q5mchoftxbjj3voisxmbapxf/lib", ":")
prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/libxcrypt-4.4.33-v3yo2l42q5mchoftxbjj3voisxmbapxf/lib", ":")
prepend_path("PKG_CONFIG_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/libxcrypt-4.4.33-v3yo2l42q5mchoftxbjj3voisxmbapxf/lib/pkgconfig", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/libxcrypt-4.4.33-v3yo2l42q5mchoftxbjj3voisxmbapxf/.", ":")

