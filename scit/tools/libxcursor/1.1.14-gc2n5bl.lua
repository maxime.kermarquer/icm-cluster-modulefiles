-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2019-03-20 14:51:07.490816
--
-- libxcursor@1.1.14%gcc@6.4.0 arch=linux-centos7-x86_64 /gc2n5bl
--

whatis([[Name : libxcursor]])
whatis([[Version : 1.1.14]])
whatis([[Short description : libXcursor - X Window System Cursor management library.]])

help([[libXcursor - X Window System Cursor management library.]])



prepend_path("MANPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/libxcursor-1.1.14-gc2n5blg7m7hreyux6v4dahuzrwetrno/share/man", ":")
prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/libxcursor-1.1.14-gc2n5blg7m7hreyux6v4dahuzrwetrno/lib", ":")
prepend_path("LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/libxcursor-1.1.14-gc2n5blg7m7hreyux6v4dahuzrwetrno/lib", ":")
prepend_path("CPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/libxcursor-1.1.14-gc2n5blg7m7hreyux6v4dahuzrwetrno/include", ":")
prepend_path("PKG_CONFIG_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/libxcursor-1.1.14-gc2n5blg7m7hreyux6v4dahuzrwetrno/lib/pkgconfig", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/libxcursor-1.1.14-gc2n5blg7m7hreyux6v4dahuzrwetrno/", ":")

