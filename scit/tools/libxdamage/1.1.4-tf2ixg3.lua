-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2018-09-19 11:09:07.885320
--
-- libxdamage@1.1.4%gcc@6.4.0 arch=linux-centos7-x86_64 /tf2ixg3
--

whatis([[Name : libxdamage]])
whatis([[Version : 1.1.4]])
whatis([[Short description : This package contains the library for the X Damage extension.]])

help([[This package contains the library for the X Damage extension.]])



prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/libxdamage-1.1.4-tf2ixg37spar2mllqyzpke7iwaifmt2x/lib", ":")
prepend_path("LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/libxdamage-1.1.4-tf2ixg37spar2mllqyzpke7iwaifmt2x/lib", ":")
prepend_path("CPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/libxdamage-1.1.4-tf2ixg37spar2mllqyzpke7iwaifmt2x/include", ":")
prepend_path("PKG_CONFIG_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/libxdamage-1.1.4-tf2ixg37spar2mllqyzpke7iwaifmt2x/lib/pkgconfig", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/libxdamage-1.1.4-tf2ixg37spar2mllqyzpke7iwaifmt2x/", ":")

