-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2018-09-19 11:09:17.402318
--
-- libxext@1.3.3%gcc@6.4.0 arch=linux-centos7-x86_64 /vytz36j
--

whatis([[Name : libxext]])
whatis([[Version : 1.3.3]])
whatis([[Short description : libXext - library for common extensions to the X11 protocol.]])

help([[libXext - library for common extensions to the X11 protocol.]])



prepend_path("MANPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/libxext-1.3.3-vytz36j4etm5i7jstrzibm5bel3tg7be/share/man", ":")
prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/libxext-1.3.3-vytz36j4etm5i7jstrzibm5bel3tg7be/lib", ":")
prepend_path("LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/libxext-1.3.3-vytz36j4etm5i7jstrzibm5bel3tg7be/lib", ":")
prepend_path("CPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/libxext-1.3.3-vytz36j4etm5i7jstrzibm5bel3tg7be/include", ":")
prepend_path("PKG_CONFIG_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/libxext-1.3.3-vytz36j4etm5i7jstrzibm5bel3tg7be/lib/pkgconfig", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/libxext-1.3.3-vytz36j4etm5i7jstrzibm5bel3tg7be/", ":")

