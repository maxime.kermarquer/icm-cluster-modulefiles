-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2019-03-20 14:51:32.185903
--
-- libxi@1.7.6%gcc@6.4.0 arch=linux-centos7-x86_64 /oxc5xry
--

whatis([[Name : libxi]])
whatis([[Version : 1.7.6]])
whatis([[Short description : libXi - library for the X Input Extension.]])

help([[libXi - library for the X Input Extension.]])



prepend_path("MANPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/libxi-1.7.6-oxc5xryb5evzdbn2yfwjb2bl7kwbxmui/share/man", ":")
prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/libxi-1.7.6-oxc5xryb5evzdbn2yfwjb2bl7kwbxmui/lib", ":")
prepend_path("LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/libxi-1.7.6-oxc5xryb5evzdbn2yfwjb2bl7kwbxmui/lib", ":")
prepend_path("CPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/libxi-1.7.6-oxc5xryb5evzdbn2yfwjb2bl7kwbxmui/include", ":")
prepend_path("PKG_CONFIG_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/libxi-1.7.6-oxc5xryb5evzdbn2yfwjb2bl7kwbxmui/lib/pkgconfig", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/libxi-1.7.6-oxc5xryb5evzdbn2yfwjb2bl7kwbxmui/", ":")

