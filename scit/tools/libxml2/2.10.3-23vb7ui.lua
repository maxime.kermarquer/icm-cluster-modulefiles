-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2023-05-04 16:00:37.316652
--
-- libxml2@2.10.3%gcc@7.3.0~python build_system=autotools arch=linux-centos7-broadwell/23vb7ui
--

whatis([[Name : libxml2]])
whatis([[Version : 2.10.3]])
whatis([[Target : broadwell]])
whatis([[Short description : Libxml2 is the XML C parser and toolkit developed for the Gnome project (but usable outside of the Gnome platform), it is free software available under the MIT License.]])

help([[Name   : libxml2]])
help([[Version: 2.10.3]])
help([[Target : broadwell]])
help()
help([[Libxml2 is the XML C parser and toolkit developed for the Gnome project
(but usable outside of the Gnome platform), it is free software
available under the MIT License.]])


depends_on("libiconv/1.17-afgvmbc")
depends_on("xz/5.4.1-pthvj6a")
depends_on("zlib/1.2.13-7shounl")

prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/libxml2-2.10.3-23vb7uitljyf5ugtiduy3fo6qxcy6li6/bin", ":")
prepend_path("CPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/libxml2-2.10.3-23vb7uitljyf5ugtiduy3fo6qxcy6li6/include", ":")
prepend_path("LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/libxml2-2.10.3-23vb7uitljyf5ugtiduy3fo6qxcy6li6/lib", ":")
prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/libxml2-2.10.3-23vb7uitljyf5ugtiduy3fo6qxcy6li6/lib", ":")
prepend_path("MANPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/libxml2-2.10.3-23vb7uitljyf5ugtiduy3fo6qxcy6li6/share/man", ":")
prepend_path("ACLOCAL_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/libxml2-2.10.3-23vb7uitljyf5ugtiduy3fo6qxcy6li6/share/aclocal", ":")
prepend_path("PKG_CONFIG_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/libxml2-2.10.3-23vb7uitljyf5ugtiduy3fo6qxcy6li6/lib/pkgconfig", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/libxml2-2.10.3-23vb7uitljyf5ugtiduy3fo6qxcy6li6/.", ":")

