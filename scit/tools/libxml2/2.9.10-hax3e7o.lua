-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2021-01-13 17:08:25.521202
--
-- libxml2@2.9.10%gcc@6.4.0~python arch=linux-centos7-broadwell/hax3e7o
--

whatis([[Name : libxml2]])
whatis([[Version : 2.9.10]])
whatis([[Target : broadwell]])
whatis([[Short description : Libxml2 is the XML C parser and toolkit developed for the Gnome project (but usable outside of the Gnome platform), it is free software available under the MIT License.]])
whatis([[Configure options : --with-lzma=/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/xz-5.2.5-loqs5h3vpwh4r3izakh7y2hz6orm6hgm --with-iconv=/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/libiconv-1.16-ufp4dxx2bc4vtwju5qtgznbuy2zpzzbc --without-python]])

help([[Libxml2 is the XML C parser and toolkit developed for the Gnome project
(but usable outside of the Gnome platform), it is free software
available under the MIT License.]])



prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/libxml2-2.9.10-hax3e7opqsrnrfex7yyioo7rl4jpvcyb/lib", ":")
prepend_path("LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/libxml2-2.9.10-hax3e7opqsrnrfex7yyioo7rl4jpvcyb/lib", ":")
prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/libxml2-2.9.10-hax3e7opqsrnrfex7yyioo7rl4jpvcyb/bin", ":")
prepend_path("MANPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/libxml2-2.9.10-hax3e7opqsrnrfex7yyioo7rl4jpvcyb/share/man", ":")
prepend_path("ACLOCAL_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/libxml2-2.9.10-hax3e7opqsrnrfex7yyioo7rl4jpvcyb/share/aclocal", ":")
prepend_path("C_INCLUDE_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/libxml2-2.9.10-hax3e7opqsrnrfex7yyioo7rl4jpvcyb/include", ":")
prepend_path("CPLUS_INCLUDE_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/libxml2-2.9.10-hax3e7opqsrnrfex7yyioo7rl4jpvcyb/include", ":")
prepend_path("INCLUDE", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/libxml2-2.9.10-hax3e7opqsrnrfex7yyioo7rl4jpvcyb/include", ":")
prepend_path("PKG_CONFIG_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/libxml2-2.9.10-hax3e7opqsrnrfex7yyioo7rl4jpvcyb/lib/pkgconfig", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/libxml2-2.9.10-hax3e7opqsrnrfex7yyioo7rl4jpvcyb/", ":")

