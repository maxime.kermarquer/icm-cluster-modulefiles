-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2019-10-25 17:05:49.212871
--
-- libxpm@3.5.12%gcc@6.4.0 arch=linux-centos7-x86_64 /mj2dus4
--

whatis([[Name : libxpm]])
whatis([[Version : 3.5.12]])
whatis([[Short description : libXpm - X Pixmap (XPM) image file format library.]])

help([[libXpm - X Pixmap (XPM) image file format library.]])



prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/libxpm-3.5.12-mj2dus4srhxrzsdwypfn4wx5dxv3srng/bin", ":")
prepend_path("MANPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/libxpm-3.5.12-mj2dus4srhxrzsdwypfn4wx5dxv3srng/share/man", ":")
prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/libxpm-3.5.12-mj2dus4srhxrzsdwypfn4wx5dxv3srng/lib", ":")
prepend_path("LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/libxpm-3.5.12-mj2dus4srhxrzsdwypfn4wx5dxv3srng/lib", ":")
prepend_path("CPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/libxpm-3.5.12-mj2dus4srhxrzsdwypfn4wx5dxv3srng/include", ":")
prepend_path("PKG_CONFIG_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/libxpm-3.5.12-mj2dus4srhxrzsdwypfn4wx5dxv3srng/lib/pkgconfig", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/libxpm-3.5.12-mj2dus4srhxrzsdwypfn4wx5dxv3srng/", ":")
prepend_path("CPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/libxml2-2.9.8-m3rdnxm66wlzs5gqsm5pqlzqvideutnk/include/libxml2", ":")

