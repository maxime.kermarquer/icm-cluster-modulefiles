-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2018-11-02 10:57:22.618508
--
-- libxrender@0.9.10%gcc@6.4.0 arch=linux-centos7-x86_64 /dpxgslw
--

whatis([[Name : libxrender]])
whatis([[Version : 0.9.10]])
whatis([[Short description : libXrender - library for the Render Extension to the X11 protocol.]])

help([[libXrender - library for the Render Extension to the X11 protocol.]])



prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/libxrender-0.9.10-dpxgslw4dbomytmp5bjchdew3zvowrcr/lib", ":")
prepend_path("LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/libxrender-0.9.10-dpxgslw4dbomytmp5bjchdew3zvowrcr/lib", ":")
prepend_path("CPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/libxrender-0.9.10-dpxgslw4dbomytmp5bjchdew3zvowrcr/include", ":")
prepend_path("PKG_CONFIG_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/libxrender-0.9.10-dpxgslw4dbomytmp5bjchdew3zvowrcr/lib/pkgconfig", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/libxrender-0.9.10-dpxgslw4dbomytmp5bjchdew3zvowrcr/", ":")

