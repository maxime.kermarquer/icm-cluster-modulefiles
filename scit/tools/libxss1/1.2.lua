-- -*- lua -*-
-- Date: 04/10/2018
-- Author : Maxime KERMARQUER
--
--

-- Module description
whatis([[Name : libxss1]])
whatis([[Version : 1.2]])
whatis([[Short description : The X Window System provides support for changing the image on a display screen after a user-settable period of inactivity to avoid burning the cathode ray tube phosphors. However, no interfaces are provided for the user to control the image that is drawn. This extension allows an external "screen saver" client to detect when the alternate image is to be displayed and to provide the graphics.]])

-- Set up environment variables
prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/software/libxss1/lib/x86_64-linux-gnu", ":")
prepend_path("LIBRARY_PATH", "/network/lustre/iss01/apps/software/libxss1/lib/x86_64-linux-gnu", ":")
