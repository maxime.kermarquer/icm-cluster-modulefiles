-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2019-03-20 14:52:28.731265
--
-- libxt@1.1.5%gcc@6.4.0 arch=linux-centos7-x86_64 /hltokck
--

whatis([[Name : libxt]])
whatis([[Version : 1.1.5]])
whatis([[Short description : libXt - X Toolkit Intrinsics library.]])

help([[libXt - X Toolkit Intrinsics library.]])



prepend_path("MANPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/libxt-1.1.5-hltokck7ylfmu354soj3g2owvrpxh23p/share/man", ":")
prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/libxt-1.1.5-hltokck7ylfmu354soj3g2owvrpxh23p/lib", ":")
prepend_path("LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/libxt-1.1.5-hltokck7ylfmu354soj3g2owvrpxh23p/lib", ":")
prepend_path("CPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/libxt-1.1.5-hltokck7ylfmu354soj3g2owvrpxh23p/include", ":")
prepend_path("PKG_CONFIG_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/libxt-1.1.5-hltokck7ylfmu354soj3g2owvrpxh23p/lib/pkgconfig", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/libxt-1.1.5-hltokck7ylfmu354soj3g2owvrpxh23p/", ":")

