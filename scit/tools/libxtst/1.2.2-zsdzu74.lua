-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2019-09-16 16:31:00.771832
--
-- libxtst@1.2.2%gcc@6.4.0 arch=linux-centos7-x86_64 /zsdzu74
--

whatis([[Name : libxtst]])
whatis([[Version : 1.2.2]])
whatis([[Short description : libXtst provides the Xlib-based client API for the XTEST & RECORD extensions.]])

help([[libXtst provides the Xlib-based client API for the XTEST & RECORD
extensions. The XTEST extension is a minimal set of client and server
extensions required to completely test the X11 server with no user
intervention. This extension is not intended to support general
journaling and playback of user actions. The RECORD extension supports
the recording and reporting of all core X protocol and arbitrary X
extension protocol.]])



prepend_path("MANPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/libxtst-1.2.2-zsdzu74enw425jfdzekxbqk3266n3f4g/share/man", ":")
prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/libxtst-1.2.2-zsdzu74enw425jfdzekxbqk3266n3f4g/lib", ":")
prepend_path("LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/libxtst-1.2.2-zsdzu74enw425jfdzekxbqk3266n3f4g/lib", ":")
prepend_path("CPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/libxtst-1.2.2-zsdzu74enw425jfdzekxbqk3266n3f4g/include", ":")
prepend_path("PKG_CONFIG_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/libxtst-1.2.2-zsdzu74enw425jfdzekxbqk3266n3f4g/lib/pkgconfig", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/libxtst-1.2.2-zsdzu74enw425jfdzekxbqk3266n3f4g/", ":")

