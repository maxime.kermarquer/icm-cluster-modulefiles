-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2023-05-23 09:52:50.400030
--
-- lzma@4.32.7%gcc@7.3.0 build_system=autotools arch=linux-centos7-broadwell/l3psnyn
--

whatis([[Name : lzma]])
whatis([[Version : 4.32.7]])
whatis([[Target : broadwell]])
whatis([[Short description : LZMA Utils are legacy data compression software with high compression ratio. LZMA Utils are no longer developed, although critical bugs may be fixed as long as fixing them doesn't require huge changes to the code.]])

help([[Name   : lzma]])
help([[Version: 4.32.7]])
help([[Target : broadwell]])
help()
help([[LZMA Utils are legacy data compression software with high compression
ratio. LZMA Utils are no longer developed, although critical bugs may be
fixed as long as fixing them doesn't require huge changes to the code.
Users of LZMA Utils should move to XZ Utils. XZ Utils support the legacy
.lzma format used by LZMA Utils, and can also emulate the command line
tools of LZMA Utils. This should make transition from LZMA Utils to XZ
Utils relatively easy.]])



prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/lzma-4.32.7-l3psnynchx4egxcuthklllrqcq6u3377/bin", ":")
prepend_path("CPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/lzma-4.32.7-l3psnynchx4egxcuthklllrqcq6u3377/include", ":")
prepend_path("LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/lzma-4.32.7-l3psnynchx4egxcuthklllrqcq6u3377/lib", ":")
prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/lzma-4.32.7-l3psnynchx4egxcuthklllrqcq6u3377/lib", ":")
prepend_path("MANPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/lzma-4.32.7-l3psnynchx4egxcuthklllrqcq6u3377/share/man", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/lzma-4.32.7-l3psnynchx4egxcuthklllrqcq6u3377/.", ":")

