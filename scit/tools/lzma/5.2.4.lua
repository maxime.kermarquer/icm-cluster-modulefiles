-- Author : Maxime KERMARQUER
-- Date : 15/05/2019

whatis([[Name : lzma]])
whatis([[Version : 5.2.4]])
whatis([[Short description : LZMA Utils are legacy data compression software with high compression ratio. LZMA Utils are no longer developed, although critical bugs may be fixed as long as fixing them doesn't require huge changes to the code.]])

help([[LZMA Utils are legacy data compression software with high compression
ratio. LZMA Utils are no longer developed, although critical bugs may be
fixed as long as fixing them doesn't require huge changes to the code.
Users of LZMA Utils should move to XZ Utils. XZ Utils support the legacy
.lzma format used by LZMA Utils, and can also emulate the command line
tools of LZMA Utils. This should make transition from LZMA Utils to XZ
Utils relatively easy.]])



prepend_path("PATH", "/network/lustre/iss01/apps/tools/additional_librairies/xz/5.2.4/bin", ":")
prepend_path("MANPATH", "/network/lustre/iss01/apps/tools/additional_librairies/xz/5.2.4/share/man", ":")
prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/tools/additional_librairies/xz/5.2.4/lib", ":")
prepend_path("LIBRARY_PATH", "/network/lustre/iss01/apps/tools/additional_librairies/xz/5.2.4/lib", ":")
prepend_path("CPATH", "/network/lustre/iss01/apps/tools/additional_librairies/xz/5.2.4/include", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/tools/additional_librairies/xz/5.2.4/", ":")

