-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2018-09-20 15:09:58.184797
--
-- mariadb@10.2.8%gcc@5.4.0 build_type=RelWithDebInfo +nonblocking arch=linux-linuxmint18-x86_64 /54n6chw
--

whatis([[Name : mariadb]])
whatis([[Version : 10.2.8]])
whatis([[Short description : MariaDB turns data into structured information in a wide array of applications, ranging from banking to websites. It is an enhanced, drop-in replacement for MySQL. MariaDB is used because it is fast, scalable and robust, with a rich ecosystem of storage engines, plugins and many other tools make it very versatile for a wide variety of use cases.]])

help([[MariaDB turns data into structured information in a wide array of
applications, ranging from banking to websites. It is an enhanced, drop-
in replacement for MySQL. MariaDB is used because it is fast, scalable
and robust, with a rich ecosystem of storage engines, plugins and many
other tools make it very versatile for a wide variety of use cases.]])



prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/mariadb-10.2.8-54n6chwx5s7i7uoylygugblx4dtmllzr/bin", ":")
prepend_path("MANPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/mariadb-10.2.8-54n6chwx5s7i7uoylygugblx4dtmllzr/man", ":")
prepend_path("ACLOCAL_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/mariadb-10.2.8-54n6chwx5s7i7uoylygugblx4dtmllzr/share/aclocal", ":")
prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/mariadb-10.2.8-54n6chwx5s7i7uoylygugblx4dtmllzr/lib", ":")
prepend_path("LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/mariadb-10.2.8-54n6chwx5s7i7uoylygugblx4dtmllzr/lib", ":")
prepend_path("CPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/mariadb-10.2.8-54n6chwx5s7i7uoylygugblx4dtmllzr/include", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/mariadb-10.2.8-54n6chwx5s7i7uoylygugblx4dtmllzr/", ":")

