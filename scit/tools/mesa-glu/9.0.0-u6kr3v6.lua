-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2018-08-31 11:13:14.576832
--
-- mesa-glu@9.0.0%gcc@5.4.0+mesa arch=linux-linuxmint18-x86_64 /u6kr3v6
--

whatis([[Name : mesa-glu]])
whatis([[Version : 9.0.0]])
whatis([[Short description : This package provides the Mesa OpenGL Utility library.]])

help([[This package provides the Mesa OpenGL Utility library.]])



prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/mesa-glu-9.0.0-u6kr3v6pamsox7aerjl5qmrqlcidsckz/lib", ":")
prepend_path("LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/mesa-glu-9.0.0-u6kr3v6pamsox7aerjl5qmrqlcidsckz/lib", ":")
prepend_path("CPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/mesa-glu-9.0.0-u6kr3v6pamsox7aerjl5qmrqlcidsckz/include", ":")
prepend_path("PKG_CONFIG_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/mesa-glu-9.0.0-u6kr3v6pamsox7aerjl5qmrqlcidsckz/lib/pkgconfig", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/mesa-glu-9.0.0-u6kr3v6pamsox7aerjl5qmrqlcidsckz/", ":")
prepend_path("CPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/libxml2-2.9.8-55hho2ks47dasihj2dsz3xg67rbssfsr/include/libxml2", ":")

