-- -*- lua -*-
-- Date : 14/01/2021
-- Author : Maxime KERMARQUER
--
--

-- Module description
whatis([[Name : mpifileutils ]])
whatis([[Version : 0.10.1 ]])
whatis([[URL : https://hpc.github.io/mpifileutils/ https://mpifileutils.readthedocs.io/en/v0.10.1/ https://mpifileutils.readthedocs.io/_/downloads/en/latest/pdf/ ]])

depends_on("openmpi/4.0.0")

-- Set up environment variables
prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/mpifileutils-0.10.1-bh2msf3rute3ok3m4oisqnrhcxq5cdve/bin", ":")
prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/mpifileutils-0.10.1-bh2msf3rute3ok3m4oisqnrhcxq5cdve/lib", ":")
prepend_path("CPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/mpifileutils-0.10.1-bh2msf3rute3ok3m4oisqnrhcxq5cdve/include", ":")
prepend_path("MANPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/mpifileutils-0.10.1-bh2msf3rute3ok3m4oisqnrhcxq5cdve/share/man", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/mpifileutils-0.10.1-bh2msf3rute3ok3m4oisqnrhcxq5cdve", ":")

