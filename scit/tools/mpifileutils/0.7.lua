-- -*- lua -*-
-- Date : 27/12/2020
-- Author : Maxime KERMARQUER
--
--

-- Module description
whatis([[Name : mpifileutils ]])
whatis([[Version : 0.7 ]])



depends_on("openmpi")


-- Set up environment variables
prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/mpifileutils-0.7-wpzraficrm626snsyem5r55rj5jmgl63/bin", ":")
prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/mpifileutils-0.7-wpzraficrm626snsyem5r55rj5jmgl63/lib", ":")
prepend_path("CPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/mpifileutils-0.7-wpzraficrm626snsyem5r55rj5jmgl63/include", ":")
prepend_path("MANPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/mpifileutils-0.7-wpzraficrm626snsyem5r55rj5jmgl63/share", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/mpifileutils-0.7-wpzraficrm626snsyem5r55rj5jmgl63", ":")

