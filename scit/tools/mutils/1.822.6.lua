-- -*- lua -*-
-- Date: 24/04/2020
-- Author : Martial Bornet
--
--

-- Module description
whatis([[Name : mutils]])
whatis([[Version : 1.822.6]])
whatis([[Short description : Parallelization tools from NASA]])


-- Set up environment variables
prepend_path("PATH", "/network/lustre/iss01/apps/tools/mutils/1.822.6/bin", ":")
prepend_path("MANPATH", "/network/lustre/iss01/apps/tools/mutils/1.822.6/man", ":")

