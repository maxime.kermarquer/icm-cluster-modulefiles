-- -*- lua -*-
-- Date : 01/03/2023
-- Author : Maxime KERMARQUER
--

-- Module description
whatis([[Name : ncdu ]])
whatis([[Version : 2.2.1 ]])
whatis([[Short description : Ncdu is a disk usage analyzer with an ncurses interface. ]])

-- Set up environment variables
prepend_path("PATH", "/network/lustre/iss01/apps/tools/ncdu/bin", ":")

