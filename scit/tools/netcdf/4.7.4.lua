-- -*- lua -*-
-- Date: 27/05/2022
-- Author : Maxime KERMARQUER
--
--

-- Module description
whatis([[Name : netcdf-c  ]])
whatis([[Version : 4.7.4 ]])
whatis([[Short description   NetCDF (network Common Data Form) is a set of software libraries and
    machine-independent data formats that support the creation, access, and
        sharing of array-oriented scientific data. This is the C distribution.: ]])
help([[ spack install netcdf-c ]])


-- Set up environment variables
prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/netcdf-c-4.7.4-z64qyt7zaffhwkqxcbowvp2jo3ciykc3/bin", ":")
prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/netcdf-c-4.7.4-z64qyt7zaffhwkqxcbowvp2jo3ciykc3/lib", ":")
prepend_path("CPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/netcdf-c-4.7.4-z64qyt7zaffhwkqxcbowvp2jo3ciykc3/include", ":")
prepend_path("MANPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/netcdf-c-4.7.4-z64qyt7zaffhwkqxcbowvp2jo3ciykc3/share", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/netcdf-c-4.7.4-z64qyt7zaffhwkqxcbowvp2jo3ciykc3", ":")

