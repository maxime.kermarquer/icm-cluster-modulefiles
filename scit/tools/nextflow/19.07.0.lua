-- -*- lua -*-
-- Date: 15/10/2019
-- Author : Maxime KERMARQUER
--
--

-- Module description
whatis([[Name : nextflow]])
whatis([[Version : 19.07.0 ]])
whatis([[Short description : Nextflow is a bioinformatics workflow manager that enables the development of portable and reproducible workflows. It supports deploying workflows on a variety of execution platforms including local, HPC schedulers, AWS Batch, Google Genomics Pipelines, and Kubernetes. Additionally, it provides support for manage your workflow dependencies through built-in support for Conda, Docker, Singularity, and Modules.]])
whatis([[Configure options : conda install -c bioconda nextflow]])

depends_on("bzip2")
depends_on("curl")
depends_on("openssl")

-- Set up environment variables
prepend_path("PATH", "/network/lustre/iss01/apps/lang/anaconda/3/2019.07/nextflow/19.07.0/bin", ":")

