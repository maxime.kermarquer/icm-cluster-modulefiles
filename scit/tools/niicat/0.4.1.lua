-- -*- lua -*-
-- Date: 14/04/2020
-- Author : Maxime KERMARQUER
--

-- Module description
whatis([[Name : niicat ]])
whatis([[Version : 0.4.1 ]])
whatis([[Short description : This is a tool to quickly preview nifti images on the terminal. WORKS ONLY with iTerm2 terminal. ]])


execute {cmd='echo -e "\\n--- \\e[1;33m/\!\\ \\e[1;31mWorks only with \\e[1;33miTerm2\\e[1;31m terminal. \\e[1;33m/\!\\ \\e[0m---\\n"',modeA={"load"}}

-- Set up environment variables
prepend_path("PATH", "/network/lustre/iss01/apps/lang/anaconda/niicat/0.4.1/bin", ":")

-- Set aliases
set_alias("niicat","niicat -ic")
