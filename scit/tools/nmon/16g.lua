-- -*- lua -*-
-- Author      : Maxime Kermarquer
-- Last update : 2021-03-24
--
--

-- Module description
whatis([[Name : nmon]])
whatis([[Version : 16g ]])
whatis([[Short description : ]])

prepend_path("PATH", "/network/lustre/iss01/apps/tools/nmon/16g/bin",":")
prepend_path("MANPATH","/network/lustre/iss01/apps/tools/nmon/16g/share/man", ":")

