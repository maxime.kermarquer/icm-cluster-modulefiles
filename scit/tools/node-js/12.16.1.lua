-- -*- lua -*-
-- Date: 02/03/2020
-- Author : Maxime KERMARQUER
--

-- fmriprep dependency
--

-- Module description
whatis([[Name : node-js]])
whatis([[Version : 12.16.1]])
whatis([[Short description : Node.js is a JavaScript runtime built on Chrome's V8 JavaScript engine. ]])

-- Set up environment variables
prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/node-js/12.16.1/bin", ":")
prepend_path("MANPATH", "/network/lustre/iss01/apps/software/scit/node-js/12.16.1/share", ":")
prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/node-js/12.16.1/lib", ":")
prepend_path("LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/node-js/12.16.1/lib", ":")
prepend_path("CPATH", "/network/lustre/iss01/apps/software/scit/node-js/12.16.1/include", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/node-js/12.16.1", ":")

