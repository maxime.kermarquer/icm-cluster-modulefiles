-- -*- lua -*-
-- Date   : 2021-12-01
-- Author : Martial Bornet
--
--

-- Module description
whatis([[Name : nvtop]])
whatis([[Version : 1.2.2]])
whatis([[Short description : Nvtop stands for NVidia TOP, a (h)top like task monitor for NVIDIA GPUs. It can handle multiple GPUs and print information about them in a htop familiar way.]])


depends_on("ncurses")

-- Set up environment variables
prepend_path("PATH", "/network/lustre/iss01/apps/tools/nvtop/1.2.2/build/src", ":")
prepend_path("MANPATH", "/network/lustre/iss01/apps/tools/nvtop/1.2.2/share/man", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/tools/nvtop/1.2.2", ":")

