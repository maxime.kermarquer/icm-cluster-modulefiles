-- -*- lua -*-
-- Date        : 2020-06-22
-- Author      : Maxime KERMARQUER
--
-- Last update : 2022-03-08
-- Author      : Martial Bornet
--

-- Module description
whatis([[Name : OpenSlide]])
whatis([[Version :1.1.1 ]])
whatis([[Short description : https://openslide.org/]])
whatis([[Configure options : In conda environment]])

-- Set up environment variables
prepend_path("PATH", "/network/lustre/iss01/apps/lang/anaconda/3/2020.02/openslide/1.1.1/bin", ":")

-- Added by Martial Bornet - 2022-03-08
prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/lang/anaconda/3/2020.02/openslide/1.1.1/lib", ":")
prepend_path("LIBRARY_PATH",    "/network/lustre/iss01/apps/lang/anaconda/3/2020.02/openslide/1.1.1/lib", ":")

