-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2018-09-17 11:15:43.855336
--
-- openssl@1.0.2o%gcc@4.8.5+systemcerts arch=linux-centos7-x86_64 /seudoyo
--

whatis([[Name : openssl]])
whatis([[Version : 1.0.2o]])
whatis([[Short description : OpenSSL is an open source project that provides a robust, commercial-grade, and full-featured toolkit for the Transport Layer Security (TLS) and Secure Sockets Layer (SSL) protocols. It is also a general-purpose cryptography library.]])

help([[OpenSSL is an open source project that provides a robust, commercial-
grade, and full-featured toolkit for the Transport Layer Security (TLS)
and Secure Sockets Layer (SSL) protocols. It is also a general-purpose
cryptography library.]])



prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-4.8.5/openssl-1.0.2o-seudoyoiaa24xj2f3o4bl3bghurnqssc/bin", ":")
prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-4.8.5/openssl-1.0.2o-seudoyoiaa24xj2f3o4bl3bghurnqssc/lib", ":")
prepend_path("LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-4.8.5/openssl-1.0.2o-seudoyoiaa24xj2f3o4bl3bghurnqssc/lib", ":")
prepend_path("CPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-4.8.5/openssl-1.0.2o-seudoyoiaa24xj2f3o4bl3bghurnqssc/include", ":")
prepend_path("PKG_CONFIG_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-4.8.5/openssl-1.0.2o-seudoyoiaa24xj2f3o4bl3bghurnqssc/lib/pkgconfig", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-4.8.5/openssl-1.0.2o-seudoyoiaa24xj2f3o4bl3bghurnqssc/", ":")

