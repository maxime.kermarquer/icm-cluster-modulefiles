-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2023-05-04 14:55:42.283888
--
-- openssl@1.1.1t%gcc@7.3.0~docs~shared build_system=generic certs=mozilla arch=linux-centos7-broadwell/f5klqzu
--

whatis([[Name : openssl]])
whatis([[Version : 1.1.1t]])
whatis([[Target : broadwell]])
whatis([[Short description : OpenSSL is an open source project that provides a robust, commercial-grade, and full-featured toolkit for the Transport Layer Security (TLS) and Secure Sockets Layer (SSL) protocols. It is also a general-purpose cryptography library.]])

help([[Name   : openssl]])
help([[Version: 1.1.1t]])
help([[Target : broadwell]])
help()
help([[OpenSSL is an open source project that provides a robust, commercial-
grade, and full-featured toolkit for the Transport Layer Security (TLS)
and Secure Sockets Layer (SSL) protocols. It is also a general-purpose
cryptography library.]])


depends_on("zlib/1.2.13-7shounl")

prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/openssl-1.1.1t-f5klqzusu6fj4i4bdrxkxvvydvzj7ofa/bin", ":")
prepend_path("CPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/openssl-1.1.1t-f5klqzusu6fj4i4bdrxkxvvydvzj7ofa/include", ":")
prepend_path("LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/openssl-1.1.1t-f5klqzusu6fj4i4bdrxkxvvydvzj7ofa/lib", ":")
prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/openssl-1.1.1t-f5klqzusu6fj4i4bdrxkxvvydvzj7ofa/lib", ":")
prepend_path("PKG_CONFIG_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/openssl-1.1.1t-f5klqzusu6fj4i4bdrxkxvvydvzj7ofa/lib/pkgconfig", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/openssl-1.1.1t-f5klqzusu6fj4i4bdrxkxvvydvzj7ofa/.", ":")

