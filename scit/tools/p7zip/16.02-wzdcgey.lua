-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2022-03-08 15:23:12.286503
--
-- p7zip@16.02%gcc@6.4.0 arch=linux-centos7-broadwell/wzdcgey
--

whatis([[Name : p7zip]])
whatis([[Version : 16.02]])
whatis([[Target : broadwell]])
whatis([[Short description : A Unix port of the 7z file archiver]])

help([[A Unix port of the 7z file archiver]])



prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/p7zip-16.02-wzdcgeyxaxwgnj5zb7meqhvo4w2piuh2/lib", ":")
prepend_path("LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/p7zip-16.02-wzdcgeyxaxwgnj5zb7meqhvo4w2piuh2/lib", ":")
prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/p7zip-16.02-wzdcgeyxaxwgnj5zb7meqhvo4w2piuh2/bin", ":")
prepend_path("MANPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/p7zip-16.02-wzdcgeyxaxwgnj5zb7meqhvo4w2piuh2/man", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/p7zip-16.02-wzdcgeyxaxwgnj5zb7meqhvo4w2piuh2/", ":")

