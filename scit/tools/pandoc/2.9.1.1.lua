-- -*- lua -*-
-- Date: 09/01/2020
-- Author : Maxime KERMARQUER
--

-- Module description
whatis([[Name : pandoc]])
whatis([[Version : 2.9.1.1]])
whatis([[Short description : Pandoc is a Haskell library for converting from one markup format to another, and a command-line tool that uses this library.]])

-- Set up environment variables
prepend_path("PATH", "/network/lustre/iss01/apps/tools/pandoc/2.9.1.1/bin", ":")
prepend_path("MANPATH", "/network/lustre/iss01/apps/tools/pandoc/2.9.1.1/share/man", ":")

