-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2023-05-09 15:11:35.404235
--
-- pcre2@10.42%gcc@7.3.0~jit+multibyte build_system=autotools arch=linux-centos7-broadwell/g7g6n3a
--

whatis([[Name : pcre2]])
whatis([[Version : 10.42]])
whatis([[Target : broadwell]])
whatis([[Short description : The PCRE2 package contains Perl Compatible Regular Expression libraries. These are useful for implementing regular expression pattern matching using the same syntax and semantics as Perl 5.]])
whatis([[Configure options : --enable-pcre2-16 --enable-pcre2-32]])

help([[Name   : pcre2]])
help([[Version: 10.42]])
help([[Target : broadwell]])
help()
help([[The PCRE2 package contains Perl Compatible Regular Expression libraries.
These are useful for implementing regular expression pattern matching
using the same syntax and semantics as Perl 5.]])



prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/pcre2-10.42-g7g6n3abbu4gmm5tfthrs2hbwo7olrzz/bin", ":")
prepend_path("CPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/pcre2-10.42-g7g6n3abbu4gmm5tfthrs2hbwo7olrzz/include", ":")
prepend_path("LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/pcre2-10.42-g7g6n3abbu4gmm5tfthrs2hbwo7olrzz/lib", ":")
prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/pcre2-10.42-g7g6n3abbu4gmm5tfthrs2hbwo7olrzz/lib", ":")
prepend_path("MANPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/pcre2-10.42-g7g6n3abbu4gmm5tfthrs2hbwo7olrzz/share/man", ":")
prepend_path("PKG_CONFIG_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/pcre2-10.42-g7g6n3abbu4gmm5tfthrs2hbwo7olrzz/lib/pkgconfig", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/pcre2-10.42-g7g6n3abbu4gmm5tfthrs2hbwo7olrzz/.", ":")

