-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2018-09-18 11:34:04.586356
--
-- perl@5.26.2%gcc@6.4.0+cpanm patches=0eac10ed90aeb0459ad8851f88081d439a4e41978e586ec743069e8b059370ac +shared+threads arch=linux-centos7-x86_64 /lc5lzoi
--

whatis([[Name : perl]])
whatis([[Version : 5.26.2]])
whatis([[Short description : Perl 5 is a highly capable, feature-rich programming language with over 27 years of development.]])
whatis([[Configure options : -des -Dprefix=/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/perl-5.26.2-lc5lzoi2hqwuhitz677qx6s2wazzcrc5 -Dlocincpth=/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/gdbm-1.14.1-l3qnkor5t5xp7jeopgfi6umkwwvgz664/include -Dloclibpth=/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/gdbm-1.14.1-l3qnkor5t5xp7jeopgfi6umkwwvgz664/lib -Accflags=-DAPPLLIB_EXP=\"/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/perl-5.26.2-lc5lzoi2hqwuhitz677qx6s2wazzcrc5/lib/perl5\" -Duseshrplib -Dusethreads]])

help([[Perl 5 is a highly capable, feature-rich programming language with over
27 years of development.]])



prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/perl-5.26.2-lc5lzoi2hqwuhitz677qx6s2wazzcrc5/bin", ":")
prepend_path("MANPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/perl-5.26.2-lc5lzoi2hqwuhitz677qx6s2wazzcrc5/man", ":")
prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/perl-5.26.2-lc5lzoi2hqwuhitz677qx6s2wazzcrc5/lib", ":")
prepend_path("LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/perl-5.26.2-lc5lzoi2hqwuhitz677qx6s2wazzcrc5/lib", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/perl-5.26.2-lc5lzoi2hqwuhitz677qx6s2wazzcrc5/", ":")

depends_on("libpng/1.6.34-737hc7h")
depends_on("libgd/2.2.4-uvcimer")
depends_on("libjpeg/9c-omyfvfz")
depends_on("zlib/1.2.11-64vg6e4")
