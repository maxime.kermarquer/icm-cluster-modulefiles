-- -*- lua -*-
-- Date: 27/12/2020
-- Author : Maxime KERMARQUER
--

-- Module description
whatis([[Name : pmix ]])
whatis([[Version : 2.1.1 ]])




-- Set up environment variables
prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/pmix-2.1.1-y34grfmziecrjddujpwelsgbin347cnr/lib", ":")
prepend_path("CPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/pmix-2.1.1-y34grfmziecrjddujpwelsgbin347cnr/include", ":")
prepend_path("MANPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/pmix-2.1.1-y34grfmziecrjddujpwelsgbin347cnr/share", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/pmix-2.1.1-y34grfmziecrjddujpwelsgbin347cnr", ":")

