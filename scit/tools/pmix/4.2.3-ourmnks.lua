-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2023-05-15 14:59:42.112889
--
-- pmix@4.2.3%gcc@7.3.0~docs+pmi_backwards_compatibility~python~restful build_system=autotools arch=linux-centos7-broadwell/ourmnks
--

whatis([[Name : pmix]])
whatis([[Version : 4.2.3]])
whatis([[Target : broadwell]])
whatis([[Short description : The Process Management Interface (PMI) has been used for quite some time as a means of exchanging wireup information needed for interprocess communication. However, meeting the significant orchestration challenges presented by exascale systems requires that the process-to-system interface evolve to permit a tighter integration between the different components of the parallel application and existing and future SMS solutions.]])
whatis([[Configure options : --enable-shared --enable-static --with-libevent=/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/libevent-2.1.12-de5q4gk7vzmojm3aqz5bkfeijm6ljpeo --with-hwloc=/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/hwloc-2.9.1-khjkbbdzxifpffz5nngs6fomtw2cwiws --disable-python-bindings --enable-pmi-backward-compatibility --disable-man-pages]])

help([[Name   : pmix]])
help([[Version: 4.2.3]])
help([[Target : broadwell]])
help()
help([[The Process Management Interface (PMI) has been used for quite some time
as a means of exchanging wireup information needed for interprocess
communication. However, meeting the significant orchestration challenges
presented by exascale systems requires that the process-to-system
interface evolve to permit a tighter integration between the different
components of the parallel application and existing and future SMS
solutions. PMI Exascale (PMIx) addresses these needs by providing an
extended version of the PMI definitions specifically designed to support
exascale and beyond environments by: (a) adding flexibility to the
functionality expressed in the existing APIs, (b) augmenting the
interfaces with new APIs that provide extended capabilities, (c) forging
a collaboration between subsystem providers including resource manager,
fabric, file system, and programming library developers, (d)
establishing a standards-like body for maintaining the definitions, and
(e) providing a reference implementation of the PMIx standard that
demonstrates the desired level of scalability while maintaining strict
separation between it and the standard itself.]])


depends_on("hwloc/2.9.1-khjkbbd")
depends_on("libevent/2.1.12-de5q4gk")

prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/pmix-4.2.3-ourmnkswyplrtmptn6duobl3otnzxsgz/bin", ":")
prepend_path("MANPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/pmix-4.2.3-ourmnkswyplrtmptn6duobl3otnzxsgz/share/man", ":")
prepend_path("PKG_CONFIG_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/pmix-4.2.3-ourmnkswyplrtmptn6duobl3otnzxsgz/lib/pkgconfig", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/pmix-4.2.3-ourmnkswyplrtmptn6duobl3otnzxsgz/.", ":")

