-- -*- lua -*-
-- Date: 18/10/2018
-- Author : Maxime KERMARQUER
--
--

-- Module description
whatis([[Name : proxy]])
whatis([[Version : 1.0.0]])
whatis([[Short description : proxy to access to the web]])


-- Set up environment variables
setenv("http_proxy", "http://10.10.3.62:3128/")
setenv("https_proxy", "http://10.10.3.62:3128/")
