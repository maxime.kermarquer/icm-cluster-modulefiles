--
-- Author      : Martial.bornet
-- Last update : 2022-03-21
--
-- Version     : 1.11.0

help([[
For detailed instructions, go to:
       https://anaconda.org

]])
whatis("Version: 3.9")
whatis("Keywords: Python")
whatis("URL: http://anaconda.org")
whatis("Description: Python interpreter")

depends_on("CUDA/11.4")
prepend_path("PATH","/network/lustre/iss01/apps/lang/anaconda/3/2021.05/pytorch/1.11.0/bin")
