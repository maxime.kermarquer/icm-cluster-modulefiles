-- Author : Maxime KERMARQUER
-- Date : 15/09/2020

help([[
For detailed instructions, go to:
       https://anaconda.org

]])
whatis("Version: 3.8")
whatis("Keywords: Python")
whatis("URL: http://anaconda.org")
whatis("Description: Python interperter")

depends_on("CUDA/10.1")

-- The path indicates 1.6.0 version but it's 1.5.0 version that is installed.
-- The downgrade was due to an incompatibility with CUDA drivers installed at this time. (>= 440.33 needed and 418.39 were installed)
prepend_path("PATH","/network/lustre/iss01/apps/lang/anaconda/3/2020.07/pytorch/1.6.0/bin")
