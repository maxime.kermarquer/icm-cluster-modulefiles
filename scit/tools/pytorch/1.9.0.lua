-- Author : Maxime KERMARQUER
-- Date : 07/09/2021

help([[
For detailed instructions, go to:
       https://anaconda.org

]])
whatis("Version: 3.8")
whatis("Keywords: Python")
whatis("URL: http://anaconda.org")
whatis("Description: Python interperter")

depends_on("CUDA/10.2")
prepend_path("PATH","/network/lustre/iss01/apps/lang/anaconda/3/2021.05/pytorch/1.9.0/bin")
