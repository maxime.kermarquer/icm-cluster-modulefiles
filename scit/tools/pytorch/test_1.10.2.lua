--
-- Author      : Martial.bornet
-- Last update : 2022-03-24
--
-- Version     : 1.10.2

help([[
For detailed instructions, go to:
       https://pytorch.org/
]])
whatis("Version: 3.8")
whatis("Keywords: Pytorch")
whatis("URL: https://pytorch.org/")
whatis("Description: An open source machine learning framework that accelerates the path from research prototyping to production deployment.")

depends_on("CUDA/11.4")
prepend_path("PATH","/network/lustre/iss01/apps/lang/anaconda/3/2021.05/pytorch/test_1.10.2/bin")
prepend_path("LIBRARY_PATH","/network/lustre/iss01/apps/lang/anaconda/3/2021.05/pytorch/test_1.10.2/lib")
prepend_path("LD_LIBRARY_PATH","/network/lustre/iss01/apps/lang/anaconda/3/2021.05/pytorch/test_1.10.2/lib")
