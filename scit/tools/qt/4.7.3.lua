-- -*- lua -*-
-- Author : Maxime KERMARQUER
-- Date : 02/11/2018

whatis([[Name : qt]])
whatis([[Version : 4.7.3]])
whatis([[Short description : Qt is a comprehensive cross-platform C++ application framework.]])

help([[Qt is a comprehensive cross-platform C++ application framework.]])



prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/muse/external/installs/qt-everywhere-opensource-4.7.3/bin", ":")
prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/muse/external/installs/qt-everywhere-opensource-4.7.3/lib", ":")
prepend_path("LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/muse/external/installs/qt-everywhere-opensource-4.7.3/lib", ":")
prepend_path("CPATH", "/network/lustre/iss01/apps/software/scit/muse/external/installs/qt-everywhere-opensource-4.7.3/include", ":")
prepend_path("PKG_CONFIG_PATH", "/network/lustre/iss01/apps/software/scit/muse/external/installs/qt-everywhere-opensource-4.7.3/lib/pkgconfig", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/muse/external/installs/qt-everywhere-opensource-4.7.3", ":")
setenv("QTDIR", "/network/lustre/iss01/apps/software/scit/muse/external/installs/qt-everywhere-opensource-4.7.3")
