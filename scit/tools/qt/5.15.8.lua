-- -*- lua -*-
--
-- Author : Martial Bornet
-- Date   : 2023-03-02
--

whatis([[Name : qt]])
whatis([[Version : 5.15.8]])
whatis([[Short description : Qt is a comprehensive cross-platform C++ application framework.]])

help([[Qt is a comprehensive cross-platform C++ application framework.]])

prepend_path("PATH",              "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/qt-5.15.8/build/qt-everywhere-src-5.15.8/qtbase/bin", ":")
prepend_path("LD_LIBRARY_PATH",   "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/qt-5.15.8/build/qt-everywhere-src-5.15.8/qtbase/lib", ":")
prepend_path("LIBRARY_PATH",      "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/qt-5.15.8/build/qt-everywhere-src-5.15.8/qtbase/lib", ":")
prepend_path("CPATH",             "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/qt-5.15.8/build/qt-everywhere-src-5.15.8/qtbase/include", ":")
prepend_path("PKG_CONFIG_PATH",   "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/qt-5.15.8/build/qt-everywhere-src-5.15.8/qtbase/lib/pkgconfig", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/qt-5.15.8/build/qt-everywhere-src-5.15.8/qtbase", ":")
setenv("QTDIR",                   "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/qt-5.15.8/build/qt-everywhere-src-5.15.8/qtbase")
