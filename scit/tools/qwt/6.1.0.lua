-- -*- lua -*-
-- Date : 05/11/2018
-- Author : Maxime KERMARQUER
--

whatis([[Name : qwt]])
whatis([[Version : 6.1.0]])
whatis([[Short description : The Qwt library contains GUI Components and utility classes which are primarily useful for programs with a technical background. Beside a framework for 2D plots it provides scales, sliders, dials, compasses, thermometers, wheels and knobs to control or display values, arrays, or ranges of type double. ]])

help([[The Qwt library contains GUI Components and utility classes which are
primarily useful for programs with a technical background. Beside a
framework for 2D plots it provides scales, sliders, dials, compasses,
thermometers, wheels and knobs to control or display values, arrays, or
ranges of type double.]])

depends_on("mesa")
depends_on("mesa-glu")

prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/muse/external/installs/qwt-6.1.0/lib", ":")
prepend_path("LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/muse/external/installs/qwt-6.1.0/lib", ":")
prepend_path("CPATH", "/network/lustre/iss01/apps/software/scit/muse/external/installs/qwt-6.1.0/include", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/muse/external/installs/qwt-6.1.0", ":")
--prepend_path("CPATH", "/include/libxml2", ":")
--prepend_path("CPATH", "/freetype2", ":")

