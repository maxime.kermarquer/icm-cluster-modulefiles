-- -*- lua -*-
-- Date: 16/11/2021
-- Author : Maxime KERMARQUER
--

-- Module description
whatis([[Name : RAR ]])
whatis([[Version : 5.5.0 ]])
whatis([[Short description : RAR provides functionality for creating a 'solid' archive, which can raise the compression ratio by 10% - 50% over more common  methods, particularly when packing large numbers of small files.]])
whatis([[Configure options : https://www.rarlab.com/rar/rarlinux-x64-5.5.0.tar.gz ]])

-- Set up environment variables
prepend_path("PATH", "/network/lustre/iss01/apps/tools/rar/bin", ":")

