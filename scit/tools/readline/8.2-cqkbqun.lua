-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2023-05-04 14:47:34.776725
--
-- readline@8.2%gcc@7.3.0 build_system=autotools patches=bbf97f1 arch=linux-centos7-broadwell/cqkbqun
--

whatis([[Name : readline]])
whatis([[Version : 8.2]])
whatis([[Target : broadwell]])
whatis([[Short description : The GNU Readline library provides a set of functions for use by applications that allow users to edit command lines as they are typed in. Both Emacs and vi editing modes are available. The Readline library includes additional functions to maintain a list of previously-entered command lines, to recall and perhaps reedit those lines, and perform csh-like history expansion on previous commands.]])

help([[Name   : readline]])
help([[Version: 8.2]])
help([[Target : broadwell]])
help()
help([[The GNU Readline library provides a set of functions for use by
applications that allow users to edit command lines as they are typed
in. Both Emacs and vi editing modes are available. The Readline library
includes additional functions to maintain a list of previously-entered
command lines, to recall and perhaps reedit those lines, and perform
csh-like history expansion on previous commands.]])


depends_on("ncurses/6.4-6n6u7qv")

prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/readline-8.2-cqkbqunz7kg34rc6w4sf6ehx36qn2ldt/bin", ":")
prepend_path("CPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/readline-8.2-cqkbqunz7kg34rc6w4sf6ehx36qn2ldt/include", ":")
prepend_path("LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/readline-8.2-cqkbqunz7kg34rc6w4sf6ehx36qn2ldt/lib", ":")
prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/readline-8.2-cqkbqunz7kg34rc6w4sf6ehx36qn2ldt/lib", ":")
prepend_path("MANPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/readline-8.2-cqkbqunz7kg34rc6w4sf6ehx36qn2ldt/share/man", ":")
prepend_path("PKG_CONFIG_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/readline-8.2-cqkbqunz7kg34rc6w4sf6ehx36qn2ldt/lib/pkgconfig", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/readline-8.2-cqkbqunz7kg34rc6w4sf6ehx36qn2ldt/.", ":")

