-- -*- lua -*-
-- Date   : 2022-09-13
-- Author : Martial Bornet
--
--

-- Module description
whatis([[Name: rpn]])
whatis([[Version: 2022-09-13]])
whatis([[Description: Multi-applications Reverse Polish Notation interpreter]])
help([[https://github.com/mbornet-hl/rpn]])

depends_on("hl/1.157")

prepend_path("PATH", "/network/lustre/iss01/apps/tools/rpn/2022-09-13/rpn/v2.0/scripts", ":")
prepend_path("PATH", "/network/lustre/iss01/apps/tools/rpn/2022-09-13/rpn/v2.0/cy", ":")

setenv("RPN_LIBPATH", "/network/lustre/iss01/apps/tools/rpn/2022-09-13/rpn/v2.0/rpn_modules")
