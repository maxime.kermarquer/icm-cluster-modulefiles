-- -*- lua -*-
--
-- Author : Martial Bornet
-- Date   : 2023-03-16
--

-- Module description
whatis([[Name: rpn]])
whatis([[Version: 2023-03-16]])
whatis([[Description: Multi-applications Reverse Polish Notation interpreter]])
help([[https://github.com/mbornet-hl/rpn]])

depends_on("hl/1.158")

prepend_path("PATH", "/network/lustre/iss01/apps/tools/rpn/2023-03-16/rpn/v2.0/scripts", ":")
prepend_path("PATH", "/network/lustre/iss01/apps/tools/rpn/2023-03-16/rpn/v2.0/cy", ":")

setenv("RPN_LIBPATH", "/network/lustre/iss01/apps/tools/rpn/2023-03-16/rpn/v2.0/rpn_modules")
