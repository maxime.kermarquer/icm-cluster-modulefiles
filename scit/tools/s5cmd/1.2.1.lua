-- -*- lua -*-
-- Author      : Martial Bornet
-- Last update : 2021-03-16
--
--

-- Module description
whatis([[Name : s5cmd]])
whatis([[Version : 1.2.1]])
whatis([[Short description : Blazing fast S3 and local filesystem execution tool]])
help([[/network/lustre/iss01/apps/modules/scit/tools/s5cmd/1.2.1.lua]])

prepend_path("PATH", "/network/lustre/iss01/apps/tools/s5cmd/1.2.1/bin", ":")

