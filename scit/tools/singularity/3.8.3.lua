-- -*- lua -*-
-- Date: 16/06/2023
-- Author : Maxime KERMARQUER
--
--

-- Module description
whatis([[Name : singularity]])
whatis([[Version : ce 3.8.3 ]])
whatis([[Short description : SingularityCE is a container platform. It allows you to create and run containers that package up pieces of software in a way that is portable and reproducible.  ]])
whatis([[URL : https://github.com/sylabs/singularity/releases/tag/v3.8.3 ]])
help([[ https://docs.sylabs.io/guides/3.11/user-guide/ ]])


-- Set up environment variables
prepend_path("PATH", "/network/lustre/iss01/apps/tools/singularity/3.8.3/bin", ":")

