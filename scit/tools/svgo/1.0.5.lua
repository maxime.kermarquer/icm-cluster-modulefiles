-- svgo modulefile
-- 24/04/2018

whatis("Version: 1.0.5")
whatis("URL: https://github.com/svg/svgo")
whatis("Description: SVG Optimizer is a Nodejs-based tool for optimizing SVG vector graphics files.")
prepend_path("PATH","/network/lustre/iss01/apps/software/scit/npm/bin/")
