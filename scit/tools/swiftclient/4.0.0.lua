-- -*- lua -*-
-- Date: 31/05/2022
-- Author : Maxime KERMARQUER
--

-- Module description
whatis([[Name : swiftclient ]])
whatis([[Version : 4.0.0 ]])
whatis([[Short description : This is a python client for the Swift API. There’s a Python API (the swiftclient module), and a command-line script (swift). ]])

-- Set up environment variables
prepend_path("PATH", "/network/lustre/iss01/apps/tools/swiftclient/4.0.0/bin", ":")

