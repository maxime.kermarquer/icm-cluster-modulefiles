-- -*- lua -*-
-- Date: 22/04/2020
-- Author : Maxime KERMARQUER
--
--

-- Module description
whatis([[Name : tensorflow ]])
whatis([[Version : 1.15.2 ]])
whatis([[Short description : ]])

-- Dependencies
depends_on("CUDA/10.0")

-- Execute a command
execute {cmd="source activate tensorflow_1.15.2",modeA={"load"}} 
execute {cmd="source deactivate",modeA={"unload"}} 

-- Set up environment variables
prepend_path("PATH", "/network/lustre/iss01/apps/lang/anaconda/3/2019.3/tensorflow/bin", ":")

