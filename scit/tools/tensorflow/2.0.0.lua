help([[
	For detailed instructions, go to:
	https://anaconda.org

	]])

whatis("Version: 3.7")
whatis("Keywords: Python")
whatis("URL: http://anaconda.org")
whatis("Description: Python interperter")
depends_on("CUDA/10.0")
prepend_path("PATH","/network/lustre/iss01/apps/lang/anaconda/3/2018.12/tensorflow/2.0.0/bin")
