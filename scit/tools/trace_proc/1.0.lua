-- -*- lua -*-
--
-- Author : Martial Bornet
-- Date   : 2023-05-03
--
--

-- Module description
whatis([[Name: trace_proc]])
whatis([[Version: 1.0]])
whatis([[Description: Trace processes]])

prepend_path("PATH", "/network/lustre/iss01/apps/tools/trace_proc/1.0/bin", ":")
setenv("TRACE_DIR", "/network/lustre/iss01/apps/tools/trace_proc/1.0/bin")
