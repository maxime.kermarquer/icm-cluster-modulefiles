-- -*- lua -*-
-- Author      : Maxime Kermarquer
-- Last update : 2021-03-24
--
--

-- Module description
whatis([[Name : traceroute]])
whatis([[Version : 2.0.22-2 ]])
whatis([[Short description : ]])


prepend_path("PATH", "/network/lustre/iss01/apps/tools/traceroute/2.0.22-2/bin",":")
prepend_path("MANPATH","/network/lustre/iss01/apps/tools/traceroute/2.0.22-2/man", ":")

