-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2019-09-17 16:53:12.344654
--
-- tree@1.7.0%gcc@6.4.0 arch=linux-centos7-x86_64 /yuxlwnd
--

whatis([[Name : tree]])
whatis([[Version : 1.7.0]])
whatis([[Short description : Tree is a recursive directory listing command that produces a depth indented listing of files, which is colorized ala dircolors if the LS_COLORS environment variable is set and output is to tty. Tree has been ported and reported to work under the following operating systems: Linux, FreeBSD, OS X, Solaris, HP/UX, Cygwin, HP Nonstop and OS/2.]])

help([[Tree is a recursive directory listing command that produces a depth
indented listing of files, which is colorized ala dircolors if the
LS_COLORS environment variable is set and output is to tty. Tree has
been ported and reported to work under the following operating systems:
Linux, FreeBSD, OS X, Solaris, HP/UX, Cygwin, HP Nonstop and OS/2.]])



prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/tree-1.7.0-yuxlwnd5a6ebwgmrgzasekjovp7se7o5/bin", ":")
prepend_path("MANPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/tree-1.7.0-yuxlwnd5a6ebwgmrgzasekjovp7se7o5/man", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/tree-1.7.0-yuxlwnd5a6ebwgmrgzasekjovp7se7o5/", ":")

