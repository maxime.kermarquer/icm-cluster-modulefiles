-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2022-05-27 15:47:32.821219
--
-- udunits@2.2.24%gcc@6.4.0 arch=linux-centos7-broadwell/kqfz6mc
--

whatis([[Name : udunits]])
whatis([[Version : 2.2.24]])
whatis([[Target : broadwell]])
whatis([[Short description : Automated units conversion]])

help([[Automated units conversion]])



prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/udunits-2.2.24-kqfz6mcxin6ctojflsn3ghij2dpry7rz/lib", ":")
prepend_path("LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/udunits-2.2.24-kqfz6mcxin6ctojflsn3ghij2dpry7rz/lib", ":")
prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/udunits-2.2.24-kqfz6mcxin6ctojflsn3ghij2dpry7rz/bin", ":")
prepend_path("C_INCLUDE_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/udunits-2.2.24-kqfz6mcxin6ctojflsn3ghij2dpry7rz/include", ":")
prepend_path("CPLUS_INCLUDE_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/udunits-2.2.24-kqfz6mcxin6ctojflsn3ghij2dpry7rz/include", ":")
prepend_path("INCLUDE", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/udunits-2.2.24-kqfz6mcxin6ctojflsn3ghij2dpry7rz/include", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/udunits-2.2.24-kqfz6mcxin6ctojflsn3ghij2dpry7rz/", ":")

