-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2018-10-16 10:42:58.942907
--
-- unzip@6.0%gcc@6.4.0 arch=linux-centos7-x86_64 /usr5aek
--

whatis([[Name : unzip]])
whatis([[Version : 6.0]])
whatis([[Short description : Unzip is a compression and file packaging/archive utility.]])

help([[Unzip is a compression and file packaging/archive utility.]])



prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/unzip-6.0-usr5aek2hs3phpwcqulzctvq6xzjs6cw/bin", ":")
prepend_path("MANPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/unzip-6.0-usr5aek2hs3phpwcqulzctvq6xzjs6cw/man", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/unzip-6.0-usr5aek2hs3phpwcqulzctvq6xzjs6cw/", ":")

