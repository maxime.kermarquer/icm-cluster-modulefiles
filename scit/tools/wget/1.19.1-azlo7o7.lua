-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2018-09-18 11:34:59.018341
--
-- wget@1.19.1%gcc@6.4.0~libpsl~pcre~python ssl=openssl +zlib arch=linux-centos7-x86_64 /azlo7o7
--

whatis([[Name : wget]])
whatis([[Version : 1.19.1]])
whatis([[Short description : GNU Wget is a free software package for retrieving files using HTTP, HTTPS and FTP, the most widely-used Internet protocols. It is a non-interactive commandline tool, so it may easily be called from scripts, cron jobs, terminals without X-Windows support, etc.]])
whatis([[Configure options : --with-ssl=openssl --with-zlib --without-libpsl --disable-pcre --disable-valgrind-tests]])

help([[GNU Wget is a free software package for retrieving files using HTTP,
HTTPS and FTP, the most widely-used Internet protocols. It is a non-
interactive commandline tool, so it may easily be called from scripts,
cron jobs, terminals without X-Windows support, etc.]])



prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/wget-1.19.1-azlo7o7coblr577nlxzo2rcsn3lfo3cw/bin", ":")
prepend_path("MANPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/wget-1.19.1-azlo7o7coblr577nlxzo2rcsn3lfo3cw/share/man", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/wget-1.19.1-azlo7o7coblr577nlxzo2rcsn3lfo3cw/", ":")

