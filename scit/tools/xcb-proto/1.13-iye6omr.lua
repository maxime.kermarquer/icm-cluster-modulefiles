-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2018-09-17 11:43:03.413892
--
-- xcb-proto@1.13%gcc@4.8.5 arch=linux-centos7-x86_64 /iye6omr
--

whatis([[Name : xcb-proto]])
whatis([[Version : 1.13]])
whatis([[Short description : xcb-proto provides the XML-XCB protocol descriptions that libxcb uses to generate the majority of its code and API.]])

help([[xcb-proto provides the XML-XCB protocol descriptions that libxcb uses to
generate the majority of its code and API.]])



prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-4.8.5/xcb-proto-1.13-iye6omrs3qrxanc3wwhpldi64bsvcbed/lib", ":")
prepend_path("LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-4.8.5/xcb-proto-1.13-iye6omrs3qrxanc3wwhpldi64bsvcbed/lib", ":")
prepend_path("PKG_CONFIG_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-4.8.5/xcb-proto-1.13-iye6omrs3qrxanc3wwhpldi64bsvcbed/lib/pkgconfig", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-4.8.5/xcb-proto-1.13-iye6omrs3qrxanc3wwhpldi64bsvcbed/", ":")

