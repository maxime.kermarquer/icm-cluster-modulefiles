-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2018-09-19 10:56:17.679397
--
-- xproto@7.0.31%gcc@6.4.0 arch=linux-centos7-x86_64 /tny2g5b
--

whatis([[Name : xproto]])
whatis([[Version : 7.0.31]])
whatis([[Short description : X Window System Core Protocol.]])

help([[X Window System Core Protocol. This package provides the headers and
specification documents defining the X Window System Core Protocol,
Version 11. It also includes a number of headers that aren't purely
protocol related, but are depended upon by many other X Window System
packages to provide common definitions and porting layer.]])



prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/xproto-7.0.31-tny2g5bizmgy5ckzvelvrmiqds42wroq/lib", ":")
prepend_path("LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/xproto-7.0.31-tny2g5bizmgy5ckzvelvrmiqds42wroq/lib", ":")
prepend_path("CPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/xproto-7.0.31-tny2g5bizmgy5ckzvelvrmiqds42wroq/include", ":")
prepend_path("PKG_CONFIG_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/xproto-7.0.31-tny2g5bizmgy5ckzvelvrmiqds42wroq/lib/pkgconfig", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/xproto-7.0.31-tny2g5bizmgy5ckzvelvrmiqds42wroq/", ":")

