-- -*- lua -*-
--
-- Date   : 2022-02-21
-- Author : Martial Bornet
--
--

whatis([[Name : xz]])
whatis([[Version : 5.2.5]])
whatis([[Short description : XZ Utils is free general-purpose data compression software with a high compression ratio]])

-- help([[]])

prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/xz/5.2.5/lib", ":")
prepend_path("LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/xz/5.2.5/lib", ":")
prepend_path("MANPATH", "/network/lustre/iss01/apps/software/scit/xz/5.2.5/man1", ":")
prepend_path("CPATH", "/network/lustre/iss01/apps/software/scit/xz/5.2.5/include", ":")
prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/xz/5.2.5/bin", ":")
