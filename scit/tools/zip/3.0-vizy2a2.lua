-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2018-10-16 10:42:53.453709
--
-- zip@3.0%gcc@6.4.0 arch=linux-centos7-x86_64 /vizy2a2
--

whatis([[Name : zip]])
whatis([[Version : 3.0]])
whatis([[Short description : Zip is a compression and file packaging/archive utility.]])

help([[Zip is a compression and file packaging/archive utility.]])



prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/zip-3.0-vizy2a267mixib2zn5c4qrhmt6xqauoq/bin", ":")
prepend_path("MANPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/zip-3.0-vizy2a267mixib2zn5c4qrhmt6xqauoq/man", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/zip-3.0-vizy2a267mixib2zn5c4qrhmt6xqauoq/", ":")

