-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2023-05-04 15:43:30.538313
--
-- zstd@1.5.5%gcc@7.3.0+programs build_system=makefile compression=none libs=shared,static arch=linux-centos7-broadwell/nwuxakc
--

whatis([[Name : zstd]])
whatis([[Version : 1.5.5]])
whatis([[Target : broadwell]])
whatis([[Short description : Zstandard, or zstd as short version, is a fast lossless compression algorithm, targeting real-time compression scenarios at zlib-level and better compression ratios.]])

help([[Name   : zstd]])
help([[Version: 1.5.5]])
help([[Target : broadwell]])
help()
help([[Zstandard, or zstd as short version, is a fast lossless compression
algorithm, targeting real-time compression scenarios at zlib-level and
better compression ratios.]])



prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/zstd-1.5.5-nwuxakczlignlp3a3etw5rzf6qpetd6g/bin", ":")
prepend_path("CPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/zstd-1.5.5-nwuxakczlignlp3a3etw5rzf6qpetd6g/include", ":")
prepend_path("LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/zstd-1.5.5-nwuxakczlignlp3a3etw5rzf6qpetd6g/lib", ":")
prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/zstd-1.5.5-nwuxakczlignlp3a3etw5rzf6qpetd6g/lib", ":")
prepend_path("MANPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/zstd-1.5.5-nwuxakczlignlp3a3etw5rzf6qpetd6g/share/man", ":")
prepend_path("PKG_CONFIG_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/zstd-1.5.5-nwuxakczlignlp3a3etw5rzf6qpetd6g/lib/pkgconfig", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/zstd-1.5.5-nwuxakczlignlp3a3etw5rzf6qpetd6g/.", ":")

