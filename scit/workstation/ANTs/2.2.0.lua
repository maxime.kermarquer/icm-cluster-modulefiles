-- ANTs modulefile
-- 19/03/2018
whatis([[Name : ANTs]])
whatis([[Version : 2.2.0]])
whatis([[Description : ANTs computes high-dimensional mappings to capture the statistics of brain structure and function. ]])

setenv("ANTSPATH", "/network/lustre/iss01/apps/software/scit/ANTs/ANTs_2.1.0/bin")
prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/ANTs/ANTs_2.1.0/bin")
prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/ANTs/ANTs_src_19-03-2018/Scripts")
prepend_path("LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/ANTs/ANTs_2.1.0/lib")
prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/ANTs/ANTs_2.1.0/lib")
