-- -*- lua -*-
-- Date: 02/11/2021
-- Author : Maxime KERMARQUER
--

-- Module description
whatis([[Name : Biological parametric mapping (BPM) ]])
whatis([[Version : BPM_Extended 3.1]])
whatis([[Short description : Biological parametric mapping (BPM) has extended the widely popular statistical parametric approach to enable application of the general linear model to multiple image modalities (both for regressors and regressands) along with scalar valued observations.]])
whatis([[Configure options : https://www.nitrc.org/frs/?group_id=433]])
help([[ ]])


depends_on("SPM/8")

prepend_path("MATLABPATH","/network/lustre/iss01/apps/tools/BPM/BPMe_3.1/BPM_Extended")

