-- -*- lua -*-
-- Date: 03/03/2021
-- Author : Maxime KERMARQUER
--
--

-- Module description
whatis([[Name : CONN]])
whatis([[Version : 20b]])
whatis([[Short description : CONN is an open-source Matlab/SPM-based cross-platform software for the computation, display, and analysis of functional connectivity Magnetic Resonance Imaging (fcMRI). CONN is used to analyze resting state data (rsfMRI) as well as task-related designs. ]])
whatis([[Configure options : https://www.nitrc.org/frs/download.php/10562/conn20b.zip]])
help([[ ]])


-- Set up environment variables
depends_on("MATLAB")
depends_on("SPM")
prepend_path("MATLABPATH", "/network/lustre/iss01/apps/software/scit/conn/20b", ":")
