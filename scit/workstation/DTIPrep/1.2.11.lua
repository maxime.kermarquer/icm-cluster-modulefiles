-- -*- lua -*-
-- Date: 02/11/2021
-- Author : Maxime KERMARQUER
--

-- Module description
whatis([[Name : DTIPrep ]])
whatis([[Version : 1.2.11 ]])
whatis([[Short description :  DTIPrep performs a "Study-specific Protocol" based automatic pipeline for DWI/DTI quality control and preparation. This is both a GUI and command line tool. The configurable pipeline includes image/diffusion information check, padding/Cropping of data, slice-wise, interlace-wise and gradient-wise intensity and motion check, head motion and Eddy current artifact correction, and DTI computing.]])
whatis([[Configure options :  https://www.nitrc.org/projects/dtiprep/]])



-- Set up environment variables
prepend_path("PATH", "/network/lustre/iss01/apps/tools/DTIPrep/1.2.11/bin", ":")
prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/tools/DTIPrep/1.2.11/lib", ":")

