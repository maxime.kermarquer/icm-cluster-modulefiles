--#%Module1.0
--## Module file created by spack (https://github.com/spack/spack) on 2018-03-14 22:27:35.251224
--##
--## fsl@5.0.10%gcc@4.8.5 arch=linux-centos7-x86_64 /rvai3es
--##
--
--
--module-whatis "FSL is a comprehensive library of analysis tools for FMRI, MRI and DTI brain imaging data."
--
--proc ModulesHelp { } {
--puts stderr "FSL is a comprehensive library of analysis tools for FMRI, MRI and DTI"
--puts stderr "brain imaging data. Note: A manual download is required for FSL. Spack"
--puts stderr "will search your current directory for the download file. Alternatively,"
--puts stderr "add this file to a mirror so that Spack can find it. For instructions on"
--puts stderr "how to set up a mirror, see"
--puts stderr "http://spack.readthedocs.io/en/latest/mirrors.html"
--}
--
--
--prepend-path PATH "/network/lustre/iss01/apps/software/linux-centos7-x86_64/gcc/4.8.5/fsl/5.0.10/rvai3esi63xxhv47yu3dkcvbou7ok7rl/bin"
--prepend-path LIBRARY_PATH "/network/lustre/iss01/apps/software/linux-centos7-x86_64/gcc/4.8.5/fsl/5.0.10/rvai3esi63xxhv47yu3dkcvbou7ok7rl/lib"
--prepend-path LD_LIBRARY_PATH "/network/lustre/iss01/apps/software/linux-centos7-x86_64/gcc/4.8.5/fsl/5.0.10/rvai3esi63xxhv47yu3dkcvbou7ok7rl/lib"
--prepend-path CPATH "/network/lustre/iss01/apps/software/linux-centos7-x86_64/gcc/4.8.5/fsl/5.0.10/rvai3esi63xxhv47yu3dkcvbou7ok7rl/include"
--prepend-path CMAKE_PREFIX_PATH "/network/lustre/iss01/apps/software/linux-centos7-x86_64/gcc/4.8.5/fsl/5.0.10/rvai3esi63xxhv47yu3dkcvbou7ok7rl/"
--setenv FSLDIR "/network/lustre/iss01/apps/software/linux-centos7-x86_64/gcc/4.8.5/fsl/5.0.10/rvai3esi63xxhv47yu3dkcvbou7ok7rl"
--setenv FSLOUTPUTTYPE "NIFTI_GZ"
--setenv FSLMULTIFILEQUIT "TRUE"
--setenv FSLTCLSH "/network/lustre/iss01/apps/software/linux-centos7-x86_64/gcc/4.8.5/fsl/5.0.10/rvai3esi63xxhv47yu3dkcvbou7ok7rl/bin/fsltclsh"
--setenv FSLWISH "/network/lustre/iss01/apps/software/linux-centos7-x86_64/gcc/4.8.5/fsl/5.0.10/rvai3esi63xxhv47yu3dkcvbou7ok7rl/bin/fslwish"
--setenv FSLLOCKDIR ""
--setenv FSLMACHINELIST ""
--setenv FSLREMOTECALL ""
--setenv FSLGECUDAQ "cuda.q"
--prepend-path PATH "/network/lustre/iss01/apps/software/linux-centos7-x86_64/gcc/4.8.5/fsl/5.0.10/rvai3esi63xxhv47yu3dkcvbou7ok7rl"


-- FSL modulefile
-- 19/03/2018
whatis([[Name : FSL]])
whatis([[Version : 5.0.10]])
whatis([[Short description : FSL is a comprehensive library of analysis tools for FMRI, MRI and DTI brain imaging data.]])

prepend_path("PATH", "/network/lustre/iss01/cenir/software/irm/fsl5/bin")
prepend_path("LIBRARY_PATH", "/network/lustre/iss01/cenir/software/irm/fsl5/lib")
prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/cenir/software/irm/fsl5/lib")
setenv("FSLDIR","/network/lustre/iss01/cenir/software/irm/fsl5/")
setenv("FSLGECUDAQ","cuda.q")
setenv("FSLLOCKDIR","")
setenv("FSLMACHINELIST","")
setenv("FSLMULTIFILEQUIT","TRUE")
setenv("FSLOUTPUTTYPE","NIFTI_GZ")
setenv("FSLREMOTECALL","")
setenv("FSLTCLSH","/network/lustre/iss01/cenir/software/irm/fsl5/bin/fsltclsh")
setenv("FSLWISH","/network/lustre/iss01/cenir/software/irm/fsl5/bin/fslwish")

