-- MATLAB modulefile
-- 26/11/2018
--

whatis("Version: R2015b")
whatis("Keywords: MATLAB")
whatis("URL: https://fr.mathworks.com/products/matlab.html")
whatis("Description: MATLAB combines a desktop environment tuned for iterative analysis and design processes with a programming language that expresses matrix and array mathematics directly.")

setenv("LM_LICENSE_FILE","27000@192.168.90.47")
setenv("MATLAB_HOME","/network/lustre/iss01/apps/lang/matlab/R2015b/")
prepend_path("PATH","/network/lustre/iss01/apps/lang/matlab/R2015b/bin/")
