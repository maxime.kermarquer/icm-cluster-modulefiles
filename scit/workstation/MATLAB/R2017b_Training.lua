-- MATLAB modulefile
-- 19/03/2018
--

whatis("Version: R2017b")
whatis("Keywords: MATLAB")
whatis("URL: https://fr.mathworks.com/products/matlab.html")
whatis("Description: MATLAB combines a desktop environment tuned for iterative analysis and design processes with a programming language that expresses matrix and array mathematics directly.")

setenv("LM_LICENSE_FILE","27000@192.168.90.47")
setenv("MATLAB_HOME","/network/lustre/iss01/apps/lang/matlab/R2017b/")
prepend_path("PATH","/network/lustre/iss01/apps/lang/matlab/R2017b/bin/")


--execute {cmd="source /network/lustre/iss01/apps/training/copy-data-matlab-essentials.sh",modeA={"load"}} 
execute {cmd="source /network/lustre/iss01/apps/training/copy-data-matlab-intermediate.sh",modeA={"load"}} 
