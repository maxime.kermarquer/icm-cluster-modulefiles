help([[
For detailed instructions, go to:
       https://anaconda.org
       https://martinos.org/mne/stable/documentation.html
]])
whatis("Version: Python 2.7, MNE 0.16.2")
whatis("URL: http://anaconda.org")
whatis("Description: Python interperter for MNE")

local f = io.open("/etc/os-release", "r")
local operating_system = f:read("*all")
f:close()

local f2 = io.open("/etc/hostname", "r")
local hostname = f2:read("*all")
f2:close()



if string.find(operating_system, "CentOS") then
    -- centOS
    if string.find(hostname, "meg") then
        -- MEG workstation
        --LmodError ("This module is not supported for MEG workstation running on CentOS 7. Please use this one for MNE :\nmodule load MNE/0.16.1_py36")
        prepend_path("PATH","/network/lustre/iss01/apps/lang/anaconda/2/5.3.0/mne_workstation/0.16.2/bin")
    else    
        -- cluster node
        prepend_path("PATH","/network/lustre/iss01/apps/lang/anaconda/2/5.3.0/mne/bin")
    end
else
    -- linux mint
    prepend_path("PATH","/network/lustre/iss01/apps/lang/anaconda/2/5.3.0/mne/bin")
end

