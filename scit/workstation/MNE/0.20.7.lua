-- 07/10/2019
-- Maxime KERMARQUER

help([[
For detailed instructions, go to:
       https://anaconda.org
       https://martinos.org/mne/stable/documentation.html
]])
whatis("Version: Python 3.6, MNE 0.20.0")
whatis("URL: http://anaconda.org")
whatis("Description: Python interperter for MNE")

-- mesa
prepend_path("LD_LIBRARY_PATH","/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/mesa-18.1.2-ysz5dekfnpkjnr3q6xd2obnifbdvttjq/lib")
prepend_path("LIBRARY_PATH","/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/mesa-18.1.2-ysz5dekfnpkjnr3q6xd2obnifbdvttjq/lib")
prepend_path("CPATH","/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/mesa-18.1.2-ysz5dekfnpkjnr3q6xd2obnifbdvttjq/include")
prepend_path("PKG_CONFIG_PATH","/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/mesa-18.1.2-ysz5dekfnpkjnr3q6xd2obnifbdvttjq/lib/pkgconfig")
prepend_path("CMAKE_PREFIX_PATH","/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/mesa-18.1.2-ysz5dekfnpkjnr3q6xd2obnifbdvttjq/")
prepend_path("CPATH","/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/libxml2-2.9.8-55hho2ks47dasihj2dsz3xg67rbssfsr/include/libxml2")

-- mesa-glu
prepend_path("LD_LIBRARY_PATH","/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/mesa-glu-9.0.0-u6kr3v6pamsox7aerjl5qmrqlcidsckz/lib")
prepend_path("LIBRARY_PATH","/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/mesa-glu-9.0.0-u6kr3v6pamsox7aerjl5qmrqlcidsckz/lib")
prepend_path("CPATH","/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/mesa-glu-9.0.0-u6kr3v6pamsox7aerjl5qmrqlcidsckz/include")
prepend_path("PKG_CONFIG_PATH","/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/mesa-glu-9.0.0-u6kr3v6pamsox7aerjl5qmrqlcidsckz/lib/pkgconfig")
prepend_path("CMAKE_PREFIX_PATH","/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/mesa-glu-9.0.0-u6kr3v6pamsox7aerjl5qmrqlcidsckz/")
prepend_path("CPATH","/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/libxml2-2.9.8-55hho2ks47dasihj2dsz3xg67rbssfsr/include/libxml2")

-- xcb-proto
prepend_path("LD_LIBRARY_PATH","/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-4.8.5/xcb-proto-1.13-iye6omrs3qrxanc3wwhpldi64bsvcbed/lib")
prepend_path("LIBRARY_PATH","/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-4.8.5/xcb-proto-1.13-iye6omrs3qrxanc3wwhpldi64bsvcbed/lib")
prepend_path("PKG_CONFIG_PATH","/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-4.8.5/xcb-proto-1.13-iye6omrs3qrxanc3wwhpldi64bsvcbed/lib/pkgconfig")
prepend_path("CMAKE_PREFIX_PATH","/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-4.8.5/xcb-proto-1.13-iye6omrs3qrxanc3wwhpldi64bsvcbed/")

-- libxcb
prepend_path("MANPATH","/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/libxcb-1.13-exuaj64lecbzyteamkinzkdkw5ij5n34/share/man")
prepend_path("LD_LIBRARY_PATH","/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/libxcb-1.13-exuaj64lecbzyteamkinzkdkw5ij5n34/lib")
prepend_path("LIBRARY_PATH","/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/libxcb-1.13-exuaj64lecbzyteamkinzkdkw5ij5n34/lib")
prepend_path("CPATH","/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/libxcb-1.13-exuaj64lecbzyteamkinzkdkw5ij5n34/include")
prepend_path("PKG_CONFIG_PATH","/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/libxcb-1.13-exuaj64lecbzyteamkinzkdkw5ij5n34/lib/pkgconfig")
prepend_path("CMAKE_PREFIX_PATH","/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/libxcb-1.13-exuaj64lecbzyteamkinzkdkw5ij5n34/")

--libxi
prepend_path("MANPATH","/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/libxi-1.7.6-oxc5xryb5evzdbn2yfwjb2bl7kwbxmui/share/man")
prepend_path("LD_LIBRARY_PATH","/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/libxi-1.7.6-oxc5xryb5evzdbn2yfwjb2bl7kwbxmui/lib")
prepend_path("LIBRARY_PATH","/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/libxi-1.7.6-oxc5xryb5evzdbn2yfwjb2bl7kwbxmui/lib")
prepend_path("CPATH","/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/libxi-1.7.6-oxc5xryb5evzdbn2yfwjb2bl7kwbxmui/include")
prepend_path("PKG_CONFIG_PATH","/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/libxi-1.7.6-oxc5xryb5evzdbn2yfwjb2bl7kwbxmui/lib/pkgconfig")
prepend_path("CMAKE_PREFIX_PATH","/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/libxi-1.7.6-oxc5xryb5evzdbn2yfwjb2bl7kwbxmui/")

-- utili-macros
prepend_path("ACLOCAL_PATH","/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-4.8.5/util-macros-1.19.1-p66qdrtaslf2cq4wyc6mitrxzhnyujhc/share/aclocal")
prepend_path("CMAKE_PREFIX_PATH","/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-4.8.5/util-macros-1.19.1-p66qdrtaslf2cq4wyc6mitrxzhnyujhc/")


-- python path
prepend_path("PATH","/network/lustre/iss01/apps/lang/anaconda/3/2020.02/meg_workstations/bin")
