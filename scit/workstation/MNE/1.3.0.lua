-- 10/05/2023
-- Maxime KERMARQUER

help([[
For detailed instructions, go to:
       https://anaconda.org
       https://mne.tools/stable/overview/index.html
]])
whatis("Version: Python 3.10.8, MNE 1.3.0")
whatis("Description: Open-source Python package for exploring, visualizing, and analyzing human neurophysiological data: MEG, EEG, sEEG, ECoG, NIRS, and more.")

prepend_path("PATH","/network/lustre/iss01/apps/lang/anaconda/3/2023.03/mne/1.3.0/bin")
