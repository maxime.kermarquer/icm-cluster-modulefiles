-- Mrtrix3 modulefile
-- 31/05/2021
whatis([[Name : mrtrix3]])
whatis([[Version : 3.0.2]])
whatis([[Short description : MRtrix provides a set of tools to perform various advanced diffusion MRI analyses, including constrained spherical deconvolution (CSD), probabilistic tractography, track-density imaging, and apparent fibre density.]])

help([[MRtrix provides a set of tools to perform various advanced diffusion MRI
analyses, including constrained spherical deconvolution (CSD),
probabilistic tractography, track-density imaging, and apparent fibre
density.]])


prepend_path("PATH", "/network/lustre/iss01/apps/lang/miniconda/mrtrix/3.0.2/bin")
-- prepend_path("PATH", "")
-- prepend_path("PERL5LIB", "")
-- prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/mrtrix3/mrtrix3")

