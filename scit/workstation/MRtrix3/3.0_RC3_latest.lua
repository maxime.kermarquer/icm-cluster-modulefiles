-- Mrtrix3 modulefile
-- Author : Maxime KERMARQUER
-- Date : 06/03/2020
whatis([[Name : mrtrix3]])
whatis([[Version : 3.0_RC3_latest]])
whatis([[Short description : MRtrix provides a set of tools to perform various advanced diffusion MRI analyses, including constrained spherical deconvolution (CSD), probabilistic tractography, track-density imaging, and apparent fibre density.]])

help([[MRtrix provides a set of tools to perform various advanced diffusion MRI
analyses, including constrained spherical deconvolution (CSD),
probabilistic tractography, track-density imaging, and apparent fibre
density.]])

--- https://github.com/MRtrix3/mrtrix3.git
--/network/lustre/iss01/apps/software/scit/mrtrix3/3.0_RC3_latest/linuxmint/18.3/share

-- apt-get install libomp5


prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/mrtrix3/3.0_RC3_latest/linuxmint/18.3/bin")
prepend_path("LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/mrtrix3/3.0_RC3_latest/linuxmint/18.3/lib")
prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/mrtrix3/3.0_RC3_latest/linuxmint/18.3/lib")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/mrtrix3/3.0_RC3_latest/linuxmint/18.3")

