-- Slicer modulefile
-- Date : 20/02/2020
-- Author : Maxime Kermarquer
whatis([[Name : Slicer]])
whatis([[Version : 4.10.2]])


prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/slicer/4.10.2")
