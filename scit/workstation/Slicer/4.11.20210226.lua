-- Slicer modulefile
-- Date : 02/11/2021
-- Author : Maxime Kermarquer
whatis([[Name : Slicer]])
whatis([[Version : 4.11.20210226]])


prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/slicer/4.11.20210226")
prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/slicer/4.11.20210226/bin")
prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/slicer/4.11.20210226/lib")
prepend_path("CPATH", "/network/lustre/iss01/apps/software/scit/slicer/4.11.20210226/include")
prepend_path("MANPATH", "/network/lustre/iss01/apps/software/scit/slicer/4.11.20210226/share")

if (mode() == "load") then LmodMessage ('\27[1mThe package libxcb-xinerama0 is necessary to run Slicer.') end
