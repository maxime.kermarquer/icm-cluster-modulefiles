-- -*- lua -*-
-- Date: 12/05/2020
-- Author : Maxime KERMARQUER
--

-- Module description
whatis([[Name : fsleyes]])
whatis([[Version : 0.32.3]])
whatis([[Short description : FSLeyes (pronounced fossilise) is the FSL image viewer.]])


-- Set up environment variables
prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/miniconda-fsleyes/0.32.3/bin", ":")
setenv("FSLDIR","/network/lustre/iss01/apps/software/linux-centos7-x86_64/gcc/4.8.5/fsl/5.0.10/rvai3esi63xxhv47yu3dkcvbou7ok7rl/")
