--#%Module1.0
--##

-- radatools modulefile
-- 13/03/2020
whatis([[Name : graphpype]])
whatis([[Version : 0.0.9]])
whatis([[Short description : radatools is an open-source multi-modal brain data analysis kit which provides Python-based pipelines .]])

depends_on("radatools/5.0")
prepend_path("PATH", "/network/lustre/iss01/apps/lang/anaconda/3/workstations/graphpype/0.0.9/bin")

