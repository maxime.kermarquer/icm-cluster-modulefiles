-- -*- lua -*-
-- Date: 11/02/2020
-- Author : Maxime KERMARQUER
--
--

-- Module description
whatis([[Name : hl]])
whatis([[Version : 1.91]])
whatis([[Short description : ]])
help([[https://github.com/mbornet-hl]])


-- Set up environment variables
prepend_path("PATH", "/network/lustre/iss01/apps/tools/hl/1.91/bin", ":")
prepend_path("PATH", "/network/lustre/iss01/apps/tools/hl/1.91/src-1.91/hl_bin", ":")

prepend_path("MANPATH", "/network/lustre/iss01/apps/tools/hl/1.91/src-1.91", ":")

prepend_path("HL_CONF", "/network/lustre/iss01/apps/tools/hl/1.91/src-1.91/config_files/hl", ":")
prepend_path("HL_CONF", "/network/lustre/iss01/apps/tools/hl/1.91/src-1.91/config_files/.hl.cfg", ":")
-- prepend_path("HL_CONF_GLOB", "hl_*.cfg:hl.cfg:.hl_*.cfg:.hl.cfg", ":")

setenv("MANPAGER", "/network/lustre/iss01/apps/tools/hl/1.91/src-1.91/scripts/hl_man_pager")
