-- -*- lua -*-
-- Date: 12/11/2018
-- Author : Maxime KERMARQUER
--
--

-- Module description
whatis([[Name : muse]])
whatis([[Version : 1.9.30]])
whatis([[Short description : ]])


-- Built with the following modules
--  gcc/6.4.0
--  mesa/18.1.2-rwyp6rj 
--  mesa-glu/9.0.0-5jxiuvx

-- For module dependencies
depends_on("mesa-glu")
depends_on("mesa")
depends_on("zlib")


-- Set up environment variables
--prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/muse/workstation/linux_mint/18.3/bin64", ":")
prepend_path("LD_LIBRARY_PATH","/network/lustre/iss01/apps/software/scit/muse/OLD/workstation/linux_mint/18.3/lib64/",":")
prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/muse/OLD/workstation/linux_mint/18.3/bin64", ":")
