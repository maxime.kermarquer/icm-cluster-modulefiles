-- -*- lua -*-
-- Date: 20/06/2018
-- Author : Maxime KERMARQUER
--
--

-- Module description
whatis([[Name : muse]])
whatis([[Version : 1.9.30]])
whatis([[Short description : ]])

-- Set up environment variables
prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/muse/muse/le41/install/1.9.34/bin64", ":")
