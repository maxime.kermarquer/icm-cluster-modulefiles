--#%Module1.0
--##

-- radatools modulefile
-- 13/03/2020
whatis([[Name : radatools]])
whatis([[Version : 5.0]])
whatis([[Short description : radatools is a set of freely distributed programs to analyze Complex Networks .]])

--depends_on("graphpype/0.0.9")
prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/radatools/5.0/Network_Tools")
setenv("RADA_PATH","/network/lustre/iss01/apps/software/scit/radatools/5.0/")

