
-- Author : Maxime KERMARQUER
-- Date : 31/05/2021

whatis("Version: 1.0.8")
whatis("Keywords: SpyKING CIRCUS")
whatis("URL: https://spyking-circus.readthedocs.io")
whatis("Description: SpyKING CIRCUS is a free, open-source, spike sorting software written entirely in python. In a nutshell, this is a fast and efficient way to perform spike sorting using a template-matching based algorithm.")


-- Installation
--- Anaconda installation - Anaconda3-2020.11-Linux-x86_64.sh
--
-- TODO
--- conda install -c conda-forge -c intel -c spyking-circus spyking-circus
--- pip install colorcet pyopengl qtconsole requests traitlets tqdm joblib click mkdocs dask toolz mtscomp
--- pip install --upgrade https://github.com/cortex-lab/phy/archive/master.zip
--- pip install --upgrade https://github.com/cortex-lab/phylib/archive/master.zip

prepend_path("PATH","/network/lustre/iss01/apps/lang/anaconda/3/2020.11/spyking-circus/1.0.8/bin")

