-- -*- lua -*-
-- Date: 22/11/2018
-- Author : Maxime KERMARQUER
--
--

-- Module description
whatis([[Name : GIFT]])
whatis([[Version : 4.0b]])
whatis([[Short description : GIFT is a MATLAB toolbox which implements multiple algorithms for independent component analysis and blind source separation of group (and single subject) functional magnetic resonance imaging data. GIFT works on MATLAB R2008a and higher. ]])
whatis([[Configure options : GroupICATv4.0b_standalone_Linux_x86_64]])
help([[ ]])


-- Set up environment variables
depends_on("MATLAB")
prepend_path("MATLABPATH", "/network/lustre/iss01/apps/software/scit/GIFT/GroupICATv2.0d_complex/icatb", ":")
