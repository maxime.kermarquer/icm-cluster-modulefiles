-- MATLAB modulefile
-- 19/03/2018
--

whatis("Version: R2017b")
whatis("Keywords: MATLAB")
whatis("URL: https://fr.mathworks.com/products/matlab.html")
whatis("Description: MATLAB combines a desktop environment tuned for iterative analysis and design processes with a programming language that expresses matrix and array mathematics directly.")


--execute {cmd="source /network/lustre/iss01/scratch/copy_data.sh",modeA={"unload"}} 

setenv("LM_LICENSE_FILE","27000@SRVFLEXLM01")
setenv("MATLAB_HOME","/network/lustre/iss01/apps/lang/matlab/R2017b/")
prepend_path("PATH","/network/lustre/iss01/apps/lang/matlab/R2017b/bin/")
