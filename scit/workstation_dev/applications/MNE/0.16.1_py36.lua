
help([[
For detailed instructions, go to:
       https://anaconda.org
       https://martinos.org/mne/stable/documentation.html
]])
whatis("Version: Python 3.6, MNE 0.16.1")
whatis("URL: http://anaconda.org")
whatis("Description: Python interperter for MNE")


-- Get OS
local f = io.open("/etc/os-release", "r")
local operating_system = f:read("*all")
f:close()

-- Get Hostname
local f2 = io.open("/etc/hostname", "r")
local hostname = f2:read("*all")
f2:close()

if string.find(operating_system, "CentOS") then
    -- centOS
    if string.find(hostname, "meg") then
        -- MEG workstation        
        prepend_path("PATH","/network/lustre/iss01/apps/lang/anaconda/3/5.3.0/mne_workstation/0.16.1/bin")
    else    
        -- cluster node
        prepend_path("PATH","/network/lustre/iss01/apps/lang/anaconda/mne/envs/tensorflow_cpu/bin")
    end
else
    -- linux mint
    prepend_path("PATH","/network/lustre/iss01/apps/lang/anaconda/mne/envs/tensorflow_cpu/bin")
end
