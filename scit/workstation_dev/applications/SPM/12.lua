-- SPM modulefile
-- 23/03/2018
whatis([[Name : SPM]])
whatis([[Version : 12]])
whatis([[Description : Statistical Parametric Mapping refers to the construction and assessment of spatially extended statistical processes used to test hypotheses about functional imaging data.]])

depends_on("MATLAB")
prepend_path("MATLABPATH", "/network/lustre/iss01/apps/software/scit/SPM/spm12")
prepend_path("MATLABPATH", "/network/lustre/iss01/apps/software/scit/SPM/spm12/toolbox/marsbar")
--prepend_path("LIBRARY_PATH", "")
--prepend_path("LD_LIBRARY_PATH", "")
