
whatis([[Name : MEG-EEG]])
whatis([[Version : 1.0.0]])
whatis([[Short description : This meta-module load the utility for MEG-EEG platform users.]])


load("muse")
load("GIFT")
load("CONN")
load("MATLAB")
load("MNE")
load("FreeSurfer")
