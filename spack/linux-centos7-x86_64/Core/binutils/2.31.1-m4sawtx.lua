-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2018-09-17 11:40:40.319743
--
-- binutils@2.31.1%gcc@4.8.5+gold~libiberty~plugins arch=linux-centos7-x86_64 /m4sawtx
--

whatis([[Name : binutils]])
whatis([[Version : 2.31.1]])
whatis([[Short description : GNU binutils, which contain the linker, assembler, objdump and others]])
whatis([[Configure options : --with-system-zlib --disable-dependency-tracking --disable-werror --enable-interwork --enable-multilib --enable-shared --enable-64-bit-bfd --enable-targets=all --with-sysroot=/ --enable-gold]])

help([[GNU binutils, which contain the linker, assembler, objdump and others]])



prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-4.8.5/binutils-2.31.1-m4sawtxber4mn4g4gopyn3kfuear6tej/bin", ":")
prepend_path("MANPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-4.8.5/binutils-2.31.1-m4sawtxber4mn4g4gopyn3kfuear6tej/share/man", ":")
prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-4.8.5/binutils-2.31.1-m4sawtxber4mn4g4gopyn3kfuear6tej/lib", ":")
prepend_path("LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-4.8.5/binutils-2.31.1-m4sawtxber4mn4g4gopyn3kfuear6tej/lib", ":")
prepend_path("CPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-4.8.5/binutils-2.31.1-m4sawtxber4mn4g4gopyn3kfuear6tej/include", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-4.8.5/binutils-2.31.1-m4sawtxber4mn4g4gopyn3kfuear6tej/", ":")
prepend_path("CPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-4.8.5/libxml2-2.9.8-okofegeulrhnfyzu6sldwacn3gbpfjj4/include/libxml2", ":")

