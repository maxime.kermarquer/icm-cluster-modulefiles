-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2018-08-31 10:01:34.124729
--
-- bison@3.0.5%gcc@4.8.5 arch=linux-centos7-x86_64 /6gcnlvb
--

whatis([[Name : bison]])
whatis([[Version : 3.0.5]])
whatis([[Short description : Bison is a general-purpose parser generator that converts an annotated context-free grammar into a deterministic LR or generalized LR (GLR) parser employing LALR(1) parser tables.]])

help([[Bison is a general-purpose parser generator that converts an annotated
context-free grammar into a deterministic LR or generalized LR (GLR)
parser employing LALR(1) parser tables.]])



prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-4.8.5/bison-3.0.5-6gcnlvbuzmj7cd6c6behxpzpdrlyrjoe/bin", ":")
prepend_path("MANPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-4.8.5/bison-3.0.5-6gcnlvbuzmj7cd6c6behxpzpdrlyrjoe/share/man", ":")
prepend_path("ACLOCAL_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-4.8.5/bison-3.0.5-6gcnlvbuzmj7cd6c6behxpzpdrlyrjoe/share/aclocal", ":")
prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-4.8.5/bison-3.0.5-6gcnlvbuzmj7cd6c6behxpzpdrlyrjoe/lib", ":")
prepend_path("LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-4.8.5/bison-3.0.5-6gcnlvbuzmj7cd6c6behxpzpdrlyrjoe/lib", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-4.8.5/bison-3.0.5-6gcnlvbuzmj7cd6c6behxpzpdrlyrjoe/", ":")

