-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2018-08-31 10:01:36.847471
--
-- bzip2@1.0.6%gcc@4.8.5+shared arch=linux-centos7-x86_64 /4vcfqu3
--

whatis([[Name : bzip2]])
whatis([[Version : 1.0.6]])
whatis([[Short description : bzip2 is a freely available, patent free high-quality data compressor. It typically compresses files to within 10% to 15% of the best available techniques (the PPM family of statistical compressors), whilst being around twice as fast at compression and six times faster at decompression.]])

help([[bzip2 is a freely available, patent free high-quality data compressor.
It typically compresses files to within 10% to 15% of the best available
techniques (the PPM family of statistical compressors), whilst being
around twice as fast at compression and six times faster at
decompression.]])



prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-4.8.5/bzip2-1.0.6-4vcfqu3netbuizhbtflvsrliinx7jogd/bin", ":")
prepend_path("MANPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-4.8.5/bzip2-1.0.6-4vcfqu3netbuizhbtflvsrliinx7jogd/man", ":")
prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-4.8.5/bzip2-1.0.6-4vcfqu3netbuizhbtflvsrliinx7jogd/lib", ":")
prepend_path("LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-4.8.5/bzip2-1.0.6-4vcfqu3netbuizhbtflvsrliinx7jogd/lib", ":")
prepend_path("CPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-4.8.5/bzip2-1.0.6-4vcfqu3netbuizhbtflvsrliinx7jogd/include", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-4.8.5/bzip2-1.0.6-4vcfqu3netbuizhbtflvsrliinx7jogd/", ":")

