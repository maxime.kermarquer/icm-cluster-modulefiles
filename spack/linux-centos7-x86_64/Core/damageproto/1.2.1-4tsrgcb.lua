-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2018-09-17 11:47:59.545098
--
-- damageproto@1.2.1%gcc@4.8.5 arch=linux-centos7-x86_64 /4tsrgcb
--

whatis([[Name : damageproto]])
whatis([[Version : 1.2.1]])
whatis([[Short description : X Damage Extension.]])

help([[X Damage Extension. This package contains header files and documentation
for the X Damage extension. Library and server implementations are
separate.]])



prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-4.8.5/damageproto-1.2.1-4tsrgcb5aqiu36isr2ccghwqnbfnld52/lib", ":")
prepend_path("LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-4.8.5/damageproto-1.2.1-4tsrgcb5aqiu36isr2ccghwqnbfnld52/lib", ":")
prepend_path("CPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-4.8.5/damageproto-1.2.1-4tsrgcb5aqiu36isr2ccghwqnbfnld52/include", ":")
prepend_path("PKG_CONFIG_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-4.8.5/damageproto-1.2.1-4tsrgcb5aqiu36isr2ccghwqnbfnld52/lib/pkgconfig", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-4.8.5/damageproto-1.2.1-4tsrgcb5aqiu36isr2ccghwqnbfnld52/", ":")

