-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2018-09-17 11:41:01.950432
--
-- expat@2.2.5%gcc@4.8.5+libbsd arch=linux-centos7-x86_64 /k3piyaw
--

whatis([[Name : expat]])
whatis([[Version : 2.2.5]])
whatis([[Short description : Expat is an XML parser library written in C.]])
whatis([[Configure options : --with-libbsd]])

help([[Expat is an XML parser library written in C.]])



prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-4.8.5/expat-2.2.5-k3piyaw2ui3rucxjlktmfh6zxyovlqxf/bin", ":")
prepend_path("MANPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-4.8.5/expat-2.2.5-k3piyaw2ui3rucxjlktmfh6zxyovlqxf/share/man", ":")
prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-4.8.5/expat-2.2.5-k3piyaw2ui3rucxjlktmfh6zxyovlqxf/lib", ":")
prepend_path("LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-4.8.5/expat-2.2.5-k3piyaw2ui3rucxjlktmfh6zxyovlqxf/lib", ":")
prepend_path("CPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-4.8.5/expat-2.2.5-k3piyaw2ui3rucxjlktmfh6zxyovlqxf/include", ":")
prepend_path("PKG_CONFIG_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-4.8.5/expat-2.2.5-k3piyaw2ui3rucxjlktmfh6zxyovlqxf/lib/pkgconfig", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-4.8.5/expat-2.2.5-k3piyaw2ui3rucxjlktmfh6zxyovlqxf/", ":")

