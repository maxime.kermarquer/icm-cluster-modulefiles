-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2018-09-17 11:48:02.498905
--
-- fixesproto@5.0%gcc@4.8.5 arch=linux-centos7-x86_64 /fwl7t4d
--

whatis([[Name : fixesproto]])
whatis([[Version : 5.0]])
whatis([[Short description : X Fixes Extension.]])

help([[X Fixes Extension. The extension makes changes to many areas of the
protocol to resolve issues raised by application interaction with core
protocol mechanisms that cannot be adequately worked around on the
client side of the wire.]])



prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-4.8.5/fixesproto-5.0-fwl7t4d62kwvbsdkngsks5p6po7oapyz/lib", ":")
prepend_path("LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-4.8.5/fixesproto-5.0-fwl7t4d62kwvbsdkngsks5p6po7oapyz/lib", ":")
prepend_path("CPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-4.8.5/fixesproto-5.0-fwl7t4d62kwvbsdkngsks5p6po7oapyz/include", ":")
prepend_path("PKG_CONFIG_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-4.8.5/fixesproto-5.0-fwl7t4d62kwvbsdkngsks5p6po7oapyz/lib/pkgconfig", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-4.8.5/fixesproto-5.0-fwl7t4d62kwvbsdkngsks5p6po7oapyz/", ":")

