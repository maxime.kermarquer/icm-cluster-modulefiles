-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2018-08-31 10:45:06.029430
--
-- flex@2.6.3%gcc@4.8.5+lex arch=linux-centos7-x86_64 /fmg4vap
--

whatis([[Name : flex]])
whatis([[Version : 2.6.3]])
whatis([[Short description : Flex is a tool for generating scanners.]])

help([[Flex is a tool for generating scanners.]])



prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-4.8.5/flex-2.6.3-fmg4vapnznmtb3pm367kzrnryeoldcl7/bin", ":")
prepend_path("MANPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-4.8.5/flex-2.6.3-fmg4vapnznmtb3pm367kzrnryeoldcl7/share/man", ":")
prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-4.8.5/flex-2.6.3-fmg4vapnznmtb3pm367kzrnryeoldcl7/lib", ":")
prepend_path("LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-4.8.5/flex-2.6.3-fmg4vapnznmtb3pm367kzrnryeoldcl7/lib", ":")
prepend_path("CPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-4.8.5/flex-2.6.3-fmg4vapnznmtb3pm367kzrnryeoldcl7/include", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-4.8.5/flex-2.6.3-fmg4vapnznmtb3pm367kzrnryeoldcl7/", ":")

