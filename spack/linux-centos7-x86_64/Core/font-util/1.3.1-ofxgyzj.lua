-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2018-09-17 14:54:39.174867
--
-- font-util@1.3.1%gcc@4.8.5 arch=linux-centos7-x86_64 /ofxgyzj
--

whatis([[Name : font-util]])
whatis([[Version : 1.3.1]])
whatis([[Short description : X.Org font package creation/installation utilities.]])

help([[X.Org font package creation/installation utilities.]])



prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-4.8.5/font-util-1.3.1-ofxgyzj54cnhf2zkje7px3pzxkyrbgv4/bin", ":")
prepend_path("MANPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-4.8.5/font-util-1.3.1-ofxgyzj54cnhf2zkje7px3pzxkyrbgv4/share/man", ":")
prepend_path("ACLOCAL_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-4.8.5/font-util-1.3.1-ofxgyzj54cnhf2zkje7px3pzxkyrbgv4/share/aclocal", ":")
prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-4.8.5/font-util-1.3.1-ofxgyzj54cnhf2zkje7px3pzxkyrbgv4/lib", ":")
prepend_path("LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-4.8.5/font-util-1.3.1-ofxgyzj54cnhf2zkje7px3pzxkyrbgv4/lib", ":")
prepend_path("PKG_CONFIG_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-4.8.5/font-util-1.3.1-ofxgyzj54cnhf2zkje7px3pzxkyrbgv4/lib/pkgconfig", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-4.8.5/font-util-1.3.1-ofxgyzj54cnhf2zkje7px3pzxkyrbgv4/", ":")

