-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2018-09-17 14:55:40.268260
--
-- fontconfig@2.12.3%gcc@4.8.5 arch=linux-centos7-x86_64 /usxqq63
--

whatis([[Name : fontconfig]])
whatis([[Version : 2.12.3]])
whatis([[Short description : Fontconfig is a library for configuring/customizing font access]])
whatis([[Configure options : --enable-libxml2 --disable-docs --with-default-fonts=/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-4.8.5/font-util-1.3.1-ofxgyzj54cnhf2zkje7px3pzxkyrbgv4/share/fonts]])

help([[Fontconfig is a library for configuring/customizing font access]])



prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-4.8.5/fontconfig-2.12.3-usxqq63g5qzsqenobrjz45idhau7un3c/bin", ":")
prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-4.8.5/fontconfig-2.12.3-usxqq63g5qzsqenobrjz45idhau7un3c/lib", ":")
prepend_path("LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-4.8.5/fontconfig-2.12.3-usxqq63g5qzsqenobrjz45idhau7un3c/lib", ":")
prepend_path("CPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-4.8.5/fontconfig-2.12.3-usxqq63g5qzsqenobrjz45idhau7un3c/include", ":")
prepend_path("PKG_CONFIG_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-4.8.5/fontconfig-2.12.3-usxqq63g5qzsqenobrjz45idhau7un3c/lib/pkgconfig", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-4.8.5/fontconfig-2.12.3-usxqq63g5qzsqenobrjz45idhau7un3c/", ":")
prepend_path("CPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-4.8.5/libxml2-2.9.8-okofegeulrhnfyzu6sldwacn3gbpfjj4/include/libxml2", ":")
prepend_path("CPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-4.8.5/freetype-2.7.1-mzguxvn36w57ru7lh25ikcoeqraqldmu/include/freetype2", ":")

