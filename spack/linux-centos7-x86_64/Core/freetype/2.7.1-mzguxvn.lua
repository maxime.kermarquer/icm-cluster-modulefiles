-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2018-09-17 14:55:09.106476
--
-- freetype@2.7.1%gcc@4.8.5 arch=linux-centos7-x86_64 /mzguxvn
--

whatis([[Name : freetype]])
whatis([[Version : 2.7.1]])
whatis([[Short description : FreeType is a freely available software library to render fonts. It is written in C, designed to be small, efficient, highly customizable, and portable while capable of producing high-quality output (glyph images) of most vector and bitmap font formats.]])
whatis([[Configure options : --with-harfbuzz=no]])

help([[FreeType is a freely available software library to render fonts. It is
written in C, designed to be small, efficient, highly customizable, and
portable while capable of producing high-quality output (glyph images)
of most vector and bitmap font formats.]])



prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-4.8.5/freetype-2.7.1-mzguxvn36w57ru7lh25ikcoeqraqldmu/bin", ":")
prepend_path("MANPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-4.8.5/freetype-2.7.1-mzguxvn36w57ru7lh25ikcoeqraqldmu/share/man", ":")
prepend_path("ACLOCAL_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-4.8.5/freetype-2.7.1-mzguxvn36w57ru7lh25ikcoeqraqldmu/share/aclocal", ":")
prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-4.8.5/freetype-2.7.1-mzguxvn36w57ru7lh25ikcoeqraqldmu/lib", ":")
prepend_path("LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-4.8.5/freetype-2.7.1-mzguxvn36w57ru7lh25ikcoeqraqldmu/lib", ":")
prepend_path("CPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-4.8.5/freetype-2.7.1-mzguxvn36w57ru7lh25ikcoeqraqldmu/include", ":")
prepend_path("PKG_CONFIG_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-4.8.5/freetype-2.7.1-mzguxvn36w57ru7lh25ikcoeqraqldmu/lib/pkgconfig", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-4.8.5/freetype-2.7.1-mzguxvn36w57ru7lh25ikcoeqraqldmu/", ":")

