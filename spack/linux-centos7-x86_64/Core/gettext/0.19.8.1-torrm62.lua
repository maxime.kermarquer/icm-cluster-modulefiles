-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2018-08-31 10:10:13.112687
--
-- gettext@0.19.8.1%gcc@4.8.5+bzip2+curses+git~libunistring+libxml2 patches=9acdb4e73f67c241b5ef32505c9ddf7cf6884ca8ea661692f21dca28483b04b8 +tar+xz arch=linux-centos7-x86_64 /torrm62
--

whatis([[Name : gettext]])
whatis([[Version : 0.19.8.1]])
whatis([[Short description : GNU internationalization (i18n) and localization (l10n) library.]])
whatis([[Configure options : --disable-java --disable-csharp --with-included-glib --with-included-gettext --with-included-libcroco --without-emacs --with-lispdir=/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-4.8.5/gettext-0.19.8.1-torrm62ocl2chzcjxkwffvwxdv5wr23o/share/emacs/site-lisp/gettext --without-cvs --with-ncurses-prefix=/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-4.8.5/ncurses-6.1-rqw3cmcwvd76h4rjgtaxpvrqg47prwkg CPPFLAGS=-I/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-4.8.5/libxml2-2.9.8-okofegeulrhnfyzu6sldwacn3gbpfjj4/include LDFLAGS=-L/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-4.8.5/libxml2-2.9.8-okofegeulrhnfyzu6sldwacn3gbpfjj4/lib -Wl,-rpath,/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-4.8.5/libxml2-2.9.8-okofegeulrhnfyzu6sldwacn3gbpfjj4/lib --with-included-libunistring]])

help([[GNU internationalization (i18n) and localization (l10n) library.]])



prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-4.8.5/gettext-0.19.8.1-torrm62ocl2chzcjxkwffvwxdv5wr23o/bin", ":")
prepend_path("MANPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-4.8.5/gettext-0.19.8.1-torrm62ocl2chzcjxkwffvwxdv5wr23o/share/man", ":")
prepend_path("ACLOCAL_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-4.8.5/gettext-0.19.8.1-torrm62ocl2chzcjxkwffvwxdv5wr23o/share/aclocal", ":")
prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-4.8.5/gettext-0.19.8.1-torrm62ocl2chzcjxkwffvwxdv5wr23o/lib", ":")
prepend_path("LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-4.8.5/gettext-0.19.8.1-torrm62ocl2chzcjxkwffvwxdv5wr23o/lib", ":")
prepend_path("CPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-4.8.5/gettext-0.19.8.1-torrm62ocl2chzcjxkwffvwxdv5wr23o/include", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-4.8.5/gettext-0.19.8.1-torrm62ocl2chzcjxkwffvwxdv5wr23o/", ":")
prepend_path("CPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-4.8.5/libxml2-2.9.8-okofegeulrhnfyzu6sldwacn3gbpfjj4/include/libxml2", ":")

