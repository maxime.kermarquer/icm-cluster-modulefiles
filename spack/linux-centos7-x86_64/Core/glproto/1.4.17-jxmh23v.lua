-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2018-09-17 11:41:07.613837
--
-- glproto@1.4.17%gcc@4.8.5 arch=linux-centos7-x86_64 /jxmh23v
--

whatis([[Name : glproto]])
whatis([[Version : 1.4.17]])
whatis([[Short description : OpenGL Extension to the X Window System.]])

help([[OpenGL Extension to the X Window System. This extension defines a
protocol for the client to send 3D rendering commands to the X server.]])



prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-4.8.5/glproto-1.4.17-jxmh23vmxykjo6nwlhs3xj7ybfhsx2jv/lib", ":")
prepend_path("LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-4.8.5/glproto-1.4.17-jxmh23vmxykjo6nwlhs3xj7ybfhsx2jv/lib", ":")
prepend_path("CPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-4.8.5/glproto-1.4.17-jxmh23vmxykjo6nwlhs3xj7ybfhsx2jv/include", ":")
prepend_path("PKG_CONFIG_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-4.8.5/glproto-1.4.17-jxmh23vmxykjo6nwlhs3xj7ybfhsx2jv/lib/pkgconfig", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-4.8.5/glproto-1.4.17-jxmh23vmxykjo6nwlhs3xj7ybfhsx2jv/", ":")

