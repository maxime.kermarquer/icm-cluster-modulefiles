-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2018-09-17 14:55:17.605379
--
-- gperf@3.0.4%gcc@4.8.5 arch=linux-centos7-x86_64 /r2j762c
--

whatis([[Name : gperf]])
whatis([[Version : 3.0.4]])
whatis([[Short description : GNU gperf is a perfect hash function generator. For a given list of strings, it produces a hash function and hash table, in form of C or C++ code, for looking up a value depending on the input string. The hash function is perfect, which means that the hash table has no collisions, and the hash table lookup needs a single string comparison only.]])

help([[GNU gperf is a perfect hash function generator. For a given list of
strings, it produces a hash function and hash table, in form of C or C++
code, for looking up a value depending on the input string. The hash
function is perfect, which means that the hash table has no collisions,
and the hash table lookup needs a single string comparison only.]])



prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-4.8.5/gperf-3.0.4-r2j762cppsosmmk6hfknjinxia6riidy/bin", ":")
prepend_path("MANPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-4.8.5/gperf-3.0.4-r2j762cppsosmmk6hfknjinxia6riidy/share/man", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-4.8.5/gperf-3.0.4-r2j762cppsosmmk6hfknjinxia6riidy/", ":")

