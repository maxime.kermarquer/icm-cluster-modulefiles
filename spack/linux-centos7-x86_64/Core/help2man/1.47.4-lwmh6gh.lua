-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2018-08-31 10:44:07.100191
--
-- help2man@1.47.4%gcc@4.8.5 arch=linux-centos7-x86_64 /lwmh6gh
--

whatis([[Name : help2man]])
whatis([[Version : 1.47.4]])
whatis([[Short description : help2man produces simple manual pages from the '--help' and '--version' output of other commands.]])

help([[help2man produces simple manual pages from the '--help' and '--version'
output of other commands.]])



prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-4.8.5/help2man-1.47.4-lwmh6ghlmfy73mxo3fe4ldenj2ebjk3v/bin", ":")
prepend_path("MANPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-4.8.5/help2man-1.47.4-lwmh6ghlmfy73mxo3fe4ldenj2ebjk3v/share/man", ":")
prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-4.8.5/help2man-1.47.4-lwmh6ghlmfy73mxo3fe4ldenj2ebjk3v/lib", ":")
prepend_path("LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-4.8.5/help2man-1.47.4-lwmh6ghlmfy73mxo3fe4ldenj2ebjk3v/lib", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-4.8.5/help2man-1.47.4-lwmh6ghlmfy73mxo3fe4ldenj2ebjk3v/", ":")

