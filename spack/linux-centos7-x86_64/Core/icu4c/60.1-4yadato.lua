-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2018-09-17 11:42:33.042488
--
-- icu4c@60.1%gcc@4.8.5 arch=linux-centos7-x86_64 /4yadato
--

whatis([[Name : icu4c]])
whatis([[Version : 60.1]])
whatis([[Short description : ICU is a mature, widely used set of C/C++ and Java libraries providing Unicode and Globalization support for software applications. ICU4C is the C/C++ interface.]])
whatis([[Configure options : --enable-rpath]])

help([[ICU is a mature, widely used set of C/C++ and Java libraries providing
Unicode and Globalization support for software applications. ICU4C is
the C/C++ interface.]])



prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-4.8.5/icu4c-60.1-4yadatobymglukfhlkasskcetbbwgqxv/bin", ":")
prepend_path("MANPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-4.8.5/icu4c-60.1-4yadatobymglukfhlkasskcetbbwgqxv/share/man", ":")
prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-4.8.5/icu4c-60.1-4yadatobymglukfhlkasskcetbbwgqxv/lib", ":")
prepend_path("LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-4.8.5/icu4c-60.1-4yadatobymglukfhlkasskcetbbwgqxv/lib", ":")
prepend_path("CPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-4.8.5/icu4c-60.1-4yadatobymglukfhlkasskcetbbwgqxv/include", ":")
prepend_path("PKG_CONFIG_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-4.8.5/icu4c-60.1-4yadatobymglukfhlkasskcetbbwgqxv/lib/pkgconfig", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-4.8.5/icu4c-60.1-4yadatobymglukfhlkasskcetbbwgqxv/", ":")

