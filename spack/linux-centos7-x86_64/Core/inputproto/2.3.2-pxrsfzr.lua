-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2018-09-17 11:42:39.438350
--
-- inputproto@2.3.2%gcc@4.8.5 arch=linux-centos7-x86_64 /pxrsfzr
--

whatis([[Name : inputproto]])
whatis([[Version : 2.3.2]])
whatis([[Short description : X Input Extension.]])

help([[X Input Extension. This extension defines a protocol to provide
additional input devices management such as graphic tablets.]])



prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-4.8.5/inputproto-2.3.2-pxrsfzrdl6dspsp46wj6og6m3nyzjbdo/lib", ":")
prepend_path("LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-4.8.5/inputproto-2.3.2-pxrsfzrdl6dspsp46wj6og6m3nyzjbdo/lib", ":")
prepend_path("CPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-4.8.5/inputproto-2.3.2-pxrsfzrdl6dspsp46wj6og6m3nyzjbdo/include", ":")
prepend_path("PKG_CONFIG_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-4.8.5/inputproto-2.3.2-pxrsfzrdl6dspsp46wj6og6m3nyzjbdo/lib/pkgconfig", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-4.8.5/inputproto-2.3.2-pxrsfzrdl6dspsp46wj6og6m3nyzjbdo/", ":")

