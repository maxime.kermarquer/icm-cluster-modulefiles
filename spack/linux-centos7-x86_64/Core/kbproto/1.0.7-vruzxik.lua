-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2018-09-17 11:42:43.432013
--
-- kbproto@1.0.7%gcc@4.8.5 arch=linux-centos7-x86_64 /vruzxik
--

whatis([[Name : kbproto]])
whatis([[Version : 1.0.7]])
whatis([[Short description : X Keyboard Extension.]])

help([[X Keyboard Extension. This extension defines a protcol to provide a
number of new capabilities and controls for text keyboards.]])



prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-4.8.5/kbproto-1.0.7-vruzxik3xlpqwriupuhz5qfyu6sjsdxk/lib", ":")
prepend_path("LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-4.8.5/kbproto-1.0.7-vruzxik3xlpqwriupuhz5qfyu6sjsdxk/lib", ":")
prepend_path("CPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-4.8.5/kbproto-1.0.7-vruzxik3xlpqwriupuhz5qfyu6sjsdxk/include", ":")
prepend_path("PKG_CONFIG_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-4.8.5/kbproto-1.0.7-vruzxik3xlpqwriupuhz5qfyu6sjsdxk/lib/pkgconfig", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-4.8.5/kbproto-1.0.7-vruzxik3xlpqwriupuhz5qfyu6sjsdxk/", ":")

