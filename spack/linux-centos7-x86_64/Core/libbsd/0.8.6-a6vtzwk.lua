-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2018-09-17 11:40:51.471109
--
-- libbsd@0.8.6%gcc@4.8.5 patches=aaae81b3edc29362a94770cc235f1eb6d6f84bb6168c4084a7376a09a2ae93c2 arch=linux-centos7-x86_64 /a6vtzwk
--

whatis([[Name : libbsd]])
whatis([[Version : 0.8.6]])
whatis([[Short description : This library provides useful functions commonly found on BSD systems, and lacking on others like GNU systems, thus making it easier to port projects with strong BSD origins, without needing to embed the same code over and over again on each project. ]])

help([[This library provides useful functions commonly found on BSD systems,
and lacking on others like GNU systems, thus making it easier to port
projects with strong BSD origins, without needing to embed the same code
over and over again on each project.]])



prepend_path("MANPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-4.8.5/libbsd-0.8.6-a6vtzwkji7snncp7qbag6rasor72evgy/share/man", ":")
prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-4.8.5/libbsd-0.8.6-a6vtzwkji7snncp7qbag6rasor72evgy/lib", ":")
prepend_path("LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-4.8.5/libbsd-0.8.6-a6vtzwkji7snncp7qbag6rasor72evgy/lib", ":")
prepend_path("CPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-4.8.5/libbsd-0.8.6-a6vtzwkji7snncp7qbag6rasor72evgy/include", ":")
prepend_path("PKG_CONFIG_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-4.8.5/libbsd-0.8.6-a6vtzwkji7snncp7qbag6rasor72evgy/lib/pkgconfig", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-4.8.5/libbsd-0.8.6-a6vtzwkji7snncp7qbag6rasor72evgy/", ":")

