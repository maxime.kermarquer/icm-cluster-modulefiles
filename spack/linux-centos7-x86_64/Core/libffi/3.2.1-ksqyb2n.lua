-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2018-09-17 14:55:47.991732
--
-- libffi@3.2.1%gcc@4.8.5 arch=linux-centos7-x86_64 /ksqyb2n
--

whatis([[Name : libffi]])
whatis([[Version : 3.2.1]])
whatis([[Short description : The libffi library provides a portable, high level programming interface to various calling conventions. This allows a programmer to call any function specified by a call interface description at run time.]])

help([[The libffi library provides a portable, high level programming interface
to various calling conventions. This allows a programmer to call any
function specified by a call interface description at run time.]])



prepend_path("MANPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-4.8.5/libffi-3.2.1-ksqyb2nne56f3f5z5cfsjwqyjfpnbejl/share/man", ":")
prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-4.8.5/libffi-3.2.1-ksqyb2nne56f3f5z5cfsjwqyjfpnbejl/lib", ":")
prepend_path("LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-4.8.5/libffi-3.2.1-ksqyb2nne56f3f5z5cfsjwqyjfpnbejl/lib", ":")
prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-4.8.5/libffi-3.2.1-ksqyb2nne56f3f5z5cfsjwqyjfpnbejl/lib64", ":")
prepend_path("LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-4.8.5/libffi-3.2.1-ksqyb2nne56f3f5z5cfsjwqyjfpnbejl/lib64", ":")
prepend_path("PKG_CONFIG_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-4.8.5/libffi-3.2.1-ksqyb2nne56f3f5z5cfsjwqyjfpnbejl/lib/pkgconfig", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-4.8.5/libffi-3.2.1-ksqyb2nne56f3f5z5cfsjwqyjfpnbejl/", ":")

