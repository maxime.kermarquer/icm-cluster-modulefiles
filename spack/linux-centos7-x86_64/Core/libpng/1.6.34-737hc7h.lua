-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2018-09-17 14:54:51.430081
--
-- libpng@1.6.34%gcc@4.8.5 arch=linux-centos7-x86_64 /737hc7h
--

whatis([[Name : libpng]])
whatis([[Version : 1.6.34]])
whatis([[Short description : libpng is the official PNG reference library.]])
whatis([[Configure options : CPPFLAGS=-I/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-4.8.5/zlib-1.2.11-64vg6e4evdrlqgx7iicwhu2hs7lv6gpz/include LDFLAGS=-L/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-4.8.5/zlib-1.2.11-64vg6e4evdrlqgx7iicwhu2hs7lv6gpz/lib]])

help([[libpng is the official PNG reference library.]])



prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-4.8.5/libpng-1.6.34-737hc7hnrfzzecjak3ziopsm37bp5li4/bin", ":")
prepend_path("MANPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-4.8.5/libpng-1.6.34-737hc7hnrfzzecjak3ziopsm37bp5li4/share/man", ":")
prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-4.8.5/libpng-1.6.34-737hc7hnrfzzecjak3ziopsm37bp5li4/lib", ":")
prepend_path("LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-4.8.5/libpng-1.6.34-737hc7hnrfzzecjak3ziopsm37bp5li4/lib", ":")
prepend_path("CPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-4.8.5/libpng-1.6.34-737hc7hnrfzzecjak3ziopsm37bp5li4/include", ":")
prepend_path("PKG_CONFIG_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-4.8.5/libpng-1.6.34-737hc7hnrfzzecjak3ziopsm37bp5li4/lib/pkgconfig", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-4.8.5/libpng-1.6.34-737hc7hnrfzzecjak3ziopsm37bp5li4/", ":")

