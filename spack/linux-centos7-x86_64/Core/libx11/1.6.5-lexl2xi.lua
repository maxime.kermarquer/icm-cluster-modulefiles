-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2018-09-17 11:47:56.431224
--
-- libx11@1.6.5%gcc@4.8.5 arch=linux-centos7-x86_64 /lexl2xi
--

whatis([[Name : libx11]])
whatis([[Version : 1.6.5]])
whatis([[Short description : libX11 - Core X11 protocol client library.]])

help([[libX11 - Core X11 protocol client library.]])



prepend_path("MANPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-4.8.5/libx11-1.6.5-lexl2xijcqvkdfhde2wdbabcw5wath55/share/man", ":")
prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-4.8.5/libx11-1.6.5-lexl2xijcqvkdfhde2wdbabcw5wath55/lib", ":")
prepend_path("LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-4.8.5/libx11-1.6.5-lexl2xijcqvkdfhde2wdbabcw5wath55/lib", ":")
prepend_path("CPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-4.8.5/libx11-1.6.5-lexl2xijcqvkdfhde2wdbabcw5wath55/include", ":")
prepend_path("PKG_CONFIG_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-4.8.5/libx11-1.6.5-lexl2xijcqvkdfhde2wdbabcw5wath55/lib/pkgconfig", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-4.8.5/libx11-1.6.5-lexl2xijcqvkdfhde2wdbabcw5wath55/", ":")

