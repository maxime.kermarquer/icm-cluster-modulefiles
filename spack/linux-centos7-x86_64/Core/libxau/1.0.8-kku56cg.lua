-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2018-09-17 11:42:53.816849
--
-- libxau@1.0.8%gcc@4.8.5 arch=linux-centos7-x86_64 /kku56cg
--

whatis([[Name : libxau]])
whatis([[Version : 1.0.8]])
whatis([[Short description : The libXau package contains a library implementing the X11 Authorization Protocol. This is useful for restricting client access to the display.]])

help([[The libXau package contains a library implementing the X11 Authorization
Protocol. This is useful for restricting client access to the display.]])



prepend_path("MANPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-4.8.5/libxau-1.0.8-kku56cgglvfdwf363fa65en25dbcesso/share/man", ":")
prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-4.8.5/libxau-1.0.8-kku56cgglvfdwf363fa65en25dbcesso/lib", ":")
prepend_path("LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-4.8.5/libxau-1.0.8-kku56cgglvfdwf363fa65en25dbcesso/lib", ":")
prepend_path("CPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-4.8.5/libxau-1.0.8-kku56cgglvfdwf363fa65en25dbcesso/include", ":")
prepend_path("PKG_CONFIG_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-4.8.5/libxau-1.0.8-kku56cgglvfdwf363fa65en25dbcesso/lib/pkgconfig", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-4.8.5/libxau-1.0.8-kku56cgglvfdwf363fa65en25dbcesso/", ":")

