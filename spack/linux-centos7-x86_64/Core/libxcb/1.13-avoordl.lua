-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2018-09-17 11:43:30.635440
--
-- libxcb@1.13%gcc@4.8.5 arch=linux-centos7-x86_64 /avoordl
--

whatis([[Name : libxcb]])
whatis([[Version : 1.13]])
whatis([[Short description : The X protocol C-language Binding (XCB) is a replacement for Xlib featuring a small footprint, latency hiding, direct access to the protocol, improved threading support, and extensibility.]])

help([[The X protocol C-language Binding (XCB) is a replacement for Xlib
featuring a small footprint, latency hiding, direct access to the
protocol, improved threading support, and extensibility.]])



prepend_path("MANPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-4.8.5/libxcb-1.13-avoordl7lxb3wdyttvg6j4o2egmx2dap/share/man", ":")
prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-4.8.5/libxcb-1.13-avoordl7lxb3wdyttvg6j4o2egmx2dap/lib", ":")
prepend_path("LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-4.8.5/libxcb-1.13-avoordl7lxb3wdyttvg6j4o2egmx2dap/lib", ":")
prepend_path("CPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-4.8.5/libxcb-1.13-avoordl7lxb3wdyttvg6j4o2egmx2dap/include", ":")
prepend_path("PKG_CONFIG_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-4.8.5/libxcb-1.13-avoordl7lxb3wdyttvg6j4o2egmx2dap/lib/pkgconfig", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-4.8.5/libxcb-1.13-avoordl7lxb3wdyttvg6j4o2egmx2dap/", ":")

