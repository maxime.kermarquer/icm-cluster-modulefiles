-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2018-09-17 11:48:17.357209
--
-- libxdamage@1.1.4%gcc@4.8.5 arch=linux-centos7-x86_64 /5uovbs4
--

whatis([[Name : libxdamage]])
whatis([[Version : 1.1.4]])
whatis([[Short description : This package contains the library for the X Damage extension.]])

help([[This package contains the library for the X Damage extension.]])



prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-4.8.5/libxdamage-1.1.4-5uovbs4cv3jippoavhnkudgp5vqcw6dc/lib", ":")
prepend_path("LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-4.8.5/libxdamage-1.1.4-5uovbs4cv3jippoavhnkudgp5vqcw6dc/lib", ":")
prepend_path("CPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-4.8.5/libxdamage-1.1.4-5uovbs4cv3jippoavhnkudgp5vqcw6dc/include", ":")
prepend_path("PKG_CONFIG_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-4.8.5/libxdamage-1.1.4-5uovbs4cv3jippoavhnkudgp5vqcw6dc/lib/pkgconfig", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-4.8.5/libxdamage-1.1.4-5uovbs4cv3jippoavhnkudgp5vqcw6dc/", ":")

