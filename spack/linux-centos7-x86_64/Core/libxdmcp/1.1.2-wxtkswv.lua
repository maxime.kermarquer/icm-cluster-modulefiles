-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2018-09-17 11:43:00.334234
--
-- libxdmcp@1.1.2%gcc@4.8.5 arch=linux-centos7-x86_64 /wxtkswv
--

whatis([[Name : libxdmcp]])
whatis([[Version : 1.1.2]])
whatis([[Short description : libXdmcp - X Display Manager Control Protocol library.]])

help([[libXdmcp - X Display Manager Control Protocol library.]])



prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-4.8.5/libxdmcp-1.1.2-wxtkswvdxjpoe4igsc7t4ul6h4rhxaac/lib", ":")
prepend_path("LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-4.8.5/libxdmcp-1.1.2-wxtkswvdxjpoe4igsc7t4ul6h4rhxaac/lib", ":")
prepend_path("CPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-4.8.5/libxdmcp-1.1.2-wxtkswvdxjpoe4igsc7t4ul6h4rhxaac/include", ":")
prepend_path("PKG_CONFIG_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-4.8.5/libxdmcp-1.1.2-wxtkswvdxjpoe4igsc7t4ul6h4rhxaac/lib/pkgconfig", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-4.8.5/libxdmcp-1.1.2-wxtkswvdxjpoe4igsc7t4ul6h4rhxaac/", ":")

