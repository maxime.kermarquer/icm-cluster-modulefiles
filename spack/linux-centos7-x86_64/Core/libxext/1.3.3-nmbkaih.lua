-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2018-09-17 11:48:26.149259
--
-- libxext@1.3.3%gcc@4.8.5 arch=linux-centos7-x86_64 /nmbkaih
--

whatis([[Name : libxext]])
whatis([[Version : 1.3.3]])
whatis([[Short description : libXext - library for common extensions to the X11 protocol.]])

help([[libXext - library for common extensions to the X11 protocol.]])



prepend_path("MANPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-4.8.5/libxext-1.3.3-nmbkaihmei56gt5gxx6y7omqcnc7coxk/share/man", ":")
prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-4.8.5/libxext-1.3.3-nmbkaihmei56gt5gxx6y7omqcnc7coxk/lib", ":")
prepend_path("LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-4.8.5/libxext-1.3.3-nmbkaihmei56gt5gxx6y7omqcnc7coxk/lib", ":")
prepend_path("CPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-4.8.5/libxext-1.3.3-nmbkaihmei56gt5gxx6y7omqcnc7coxk/include", ":")
prepend_path("PKG_CONFIG_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-4.8.5/libxext-1.3.3-nmbkaihmei56gt5gxx6y7omqcnc7coxk/lib/pkgconfig", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-4.8.5/libxext-1.3.3-nmbkaihmei56gt5gxx6y7omqcnc7coxk/", ":")

