-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2018-09-17 11:48:09.617396
--
-- libxfixes@5.0.2%gcc@4.8.5 arch=linux-centos7-x86_64 /wy6nxjl
--

whatis([[Name : libxfixes]])
whatis([[Version : 5.0.2]])
whatis([[Short description : This package contains header files and documentation for the XFIXES extension. Library and server implementations are separate.]])

help([[This package contains header files and documentation for the XFIXES
extension. Library and server implementations are separate.]])



prepend_path("MANPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-4.8.5/libxfixes-5.0.2-wy6nxjldqeoa3vvqv3tgsnkfp7lb6z36/share/man", ":")
prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-4.8.5/libxfixes-5.0.2-wy6nxjldqeoa3vvqv3tgsnkfp7lb6z36/lib", ":")
prepend_path("LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-4.8.5/libxfixes-5.0.2-wy6nxjldqeoa3vvqv3tgsnkfp7lb6z36/lib", ":")
prepend_path("CPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-4.8.5/libxfixes-5.0.2-wy6nxjldqeoa3vvqv3tgsnkfp7lb6z36/include", ":")
prepend_path("PKG_CONFIG_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-4.8.5/libxfixes-5.0.2-wy6nxjldqeoa3vvqv3tgsnkfp7lb6z36/lib/pkgconfig", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-4.8.5/libxfixes-5.0.2-wy6nxjldqeoa3vvqv3tgsnkfp7lb6z36/", ":")

