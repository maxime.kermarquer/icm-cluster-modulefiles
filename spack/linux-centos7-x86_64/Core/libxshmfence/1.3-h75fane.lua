-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2018-09-17 11:48:32.481324
--
-- libxshmfence@1.3%gcc@4.8.5 arch=linux-centos7-x86_64 /h75fane
--

whatis([[Name : libxshmfence]])
whatis([[Version : 1.3]])
whatis([[Short description : libxshmfence - Shared memory 'SyncFence' synchronization primitive.]])

help([[libxshmfence - Shared memory 'SyncFence' synchronization primitive. This
library offers a CPU-based synchronization primitive compatible with the
X SyncFence objects that can be shared between processes using file
descriptor passing.]])



prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-4.8.5/libxshmfence-1.3-h75faneiznn76y2y373gvdihx6jpi7i3/lib", ":")
prepend_path("LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-4.8.5/libxshmfence-1.3-h75faneiznn76y2y373gvdihx6jpi7i3/lib", ":")
prepend_path("CPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-4.8.5/libxshmfence-1.3-h75faneiznn76y2y373gvdihx6jpi7i3/include", ":")
prepend_path("PKG_CONFIG_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-4.8.5/libxshmfence-1.3-h75faneiznn76y2y373gvdihx6jpi7i3/lib/pkgconfig", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-4.8.5/libxshmfence-1.3-h75faneiznn76y2y373gvdihx6jpi7i3/", ":")

