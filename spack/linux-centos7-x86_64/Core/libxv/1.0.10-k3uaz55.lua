-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2018-09-17 11:48:43.974570
--
-- libxv@1.0.10%gcc@4.8.5 arch=linux-centos7-x86_64 /k3uaz55
--

whatis([[Name : libxv]])
whatis([[Version : 1.0.10]])
whatis([[Short description : libXv - library for the X Video (Xv) extension to the X Window System.]])

help([[libXv - library for the X Video (Xv) extension to the X Window System.]])



prepend_path("MANPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-4.8.5/libxv-1.0.10-k3uaz55w5gbqkfzb6a3shjeab3hhntge/share/man", ":")
prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-4.8.5/libxv-1.0.10-k3uaz55w5gbqkfzb6a3shjeab3hhntge/lib", ":")
prepend_path("LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-4.8.5/libxv-1.0.10-k3uaz55w5gbqkfzb6a3shjeab3hhntge/lib", ":")
prepend_path("CPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-4.8.5/libxv-1.0.10-k3uaz55w5gbqkfzb6a3shjeab3hhntge/include", ":")
prepend_path("PKG_CONFIG_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-4.8.5/libxv-1.0.10-k3uaz55w5gbqkfzb6a3shjeab3hhntge/lib/pkgconfig", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-4.8.5/libxv-1.0.10-k3uaz55w5gbqkfzb6a3shjeab3hhntge/", ":")

