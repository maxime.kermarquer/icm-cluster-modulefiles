-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2018-09-17 11:48:52.326622
--
-- libxvmc@1.0.9%gcc@4.8.5 arch=linux-centos7-x86_64 /4plfpel
--

whatis([[Name : libxvmc]])
whatis([[Version : 1.0.9]])
whatis([[Short description : X.org libXvMC library.]])

help([[X.org libXvMC library.]])



prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-4.8.5/libxvmc-1.0.9-4plfpelddqdkmuccwhx5e3cqqprubi5a/lib", ":")
prepend_path("LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-4.8.5/libxvmc-1.0.9-4plfpelddqdkmuccwhx5e3cqqprubi5a/lib", ":")
prepend_path("CPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-4.8.5/libxvmc-1.0.9-4plfpelddqdkmuccwhx5e3cqqprubi5a/include", ":")
prepend_path("PKG_CONFIG_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-4.8.5/libxvmc-1.0.9-4plfpelddqdkmuccwhx5e3cqqprubi5a/lib/pkgconfig", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-4.8.5/libxvmc-1.0.9-4plfpelddqdkmuccwhx5e3cqqprubi5a/", ":")

