-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2018-09-17 14:56:05.704001
--
-- pcre@8.42%gcc@4.8.5~jit+utf arch=linux-centos7-x86_64 /fx4feox
--

whatis([[Name : pcre]])
whatis([[Version : 8.42]])
whatis([[Short description : The PCRE package contains Perl Compatible Regular Expression libraries. These are useful for implementing regular expression pattern matching using the same syntax and semantics as Perl 5.]])
whatis([[Configure options : --enable-utf --enable-unicode-properties]])

help([[The PCRE package contains Perl Compatible Regular Expression libraries.
These are useful for implementing regular expression pattern matching
using the same syntax and semantics as Perl 5.]])



prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-4.8.5/pcre-8.42-fx4feox5fku33c76hgp4dzzcvleyc2mp/bin", ":")
prepend_path("MANPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-4.8.5/pcre-8.42-fx4feox5fku33c76hgp4dzzcvleyc2mp/share/man", ":")
prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-4.8.5/pcre-8.42-fx4feox5fku33c76hgp4dzzcvleyc2mp/lib", ":")
prepend_path("LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-4.8.5/pcre-8.42-fx4feox5fku33c76hgp4dzzcvleyc2mp/lib", ":")
prepend_path("CPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-4.8.5/pcre-8.42-fx4feox5fku33c76hgp4dzzcvleyc2mp/include", ":")
prepend_path("PKG_CONFIG_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-4.8.5/pcre-8.42-fx4feox5fku33c76hgp4dzzcvleyc2mp/lib/pkgconfig", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-4.8.5/pcre-8.42-fx4feox5fku33c76hgp4dzzcvleyc2mp/", ":")

