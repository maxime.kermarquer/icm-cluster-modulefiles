-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2018-09-17 11:46:14.415842
--
-- perl@5.26.2%gcc@4.8.5+cpanm patches=0eac10ed90aeb0459ad8851f88081d439a4e41978e586ec743069e8b059370ac +shared+threads arch=linux-centos7-x86_64 /hnx5wqj
--

whatis([[Name : perl]])
whatis([[Version : 5.26.2]])
whatis([[Short description : Perl 5 is a highly capable, feature-rich programming language with over 27 years of development.]])
whatis([[Configure options : -des -Dprefix=/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-4.8.5/perl-5.26.2-hnx5wqjxdsqb2l3g3zjebpdul4wuurov -Dlocincpth=/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-4.8.5/gdbm-1.14.1-xdrlyqkflqshxl2so56p5qhwjf77dxbt/include -Dloclibpth=/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-4.8.5/gdbm-1.14.1-xdrlyqkflqshxl2so56p5qhwjf77dxbt/lib -Accflags=-DAPPLLIB_EXP=\"/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-4.8.5/perl-5.26.2-hnx5wqjxdsqb2l3g3zjebpdul4wuurov/lib/perl5\" -Duseshrplib -Dusethreads]])

help([[Perl 5 is a highly capable, feature-rich programming language with over
27 years of development.]])



prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-4.8.5/perl-5.26.2-hnx5wqjxdsqb2l3g3zjebpdul4wuurov/bin", ":")
prepend_path("MANPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-4.8.5/perl-5.26.2-hnx5wqjxdsqb2l3g3zjebpdul4wuurov/man", ":")
prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-4.8.5/perl-5.26.2-hnx5wqjxdsqb2l3g3zjebpdul4wuurov/lib", ":")
prepend_path("LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-4.8.5/perl-5.26.2-hnx5wqjxdsqb2l3g3zjebpdul4wuurov/lib", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-4.8.5/perl-5.26.2-hnx5wqjxdsqb2l3g3zjebpdul4wuurov/", ":")

