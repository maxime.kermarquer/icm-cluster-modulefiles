-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2018-08-28 11:45:42.578624
--
-- pkgconf@1.4.2%gcc@4.8.5 arch=linux-centos7-x86_64 /lq2llfa
--

whatis([[Name : pkgconf]])
whatis([[Version : 1.4.2]])
whatis([[Short description : pkgconf is a program which helps to configure compiler and linker flags for development frameworks. It is similar to pkg-config from freedesktop.org, providing additional functionality while also maintaining compatibility.]])

help([[pkgconf is a program which helps to configure compiler and linker flags
for development frameworks. It is similar to pkg-config from
freedesktop.org, providing additional functionality while also
maintaining compatibility.]])



prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-4.8.5/pkgconf-1.4.2-lq2llfax3ikzxy7fel7kvlrxogtolr7w/bin", ":")
prepend_path("MANPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-4.8.5/pkgconf-1.4.2-lq2llfax3ikzxy7fel7kvlrxogtolr7w/share/man", ":")
prepend_path("ACLOCAL_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-4.8.5/pkgconf-1.4.2-lq2llfax3ikzxy7fel7kvlrxogtolr7w/share/aclocal", ":")
prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-4.8.5/pkgconf-1.4.2-lq2llfax3ikzxy7fel7kvlrxogtolr7w/lib", ":")
prepend_path("LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-4.8.5/pkgconf-1.4.2-lq2llfax3ikzxy7fel7kvlrxogtolr7w/lib", ":")
prepend_path("CPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-4.8.5/pkgconf-1.4.2-lq2llfax3ikzxy7fel7kvlrxogtolr7w/include", ":")
prepend_path("PKG_CONFIG_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-4.8.5/pkgconf-1.4.2-lq2llfax3ikzxy7fel7kvlrxogtolr7w/lib/pkgconfig", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-4.8.5/pkgconf-1.4.2-lq2llfax3ikzxy7fel7kvlrxogtolr7w/", ":")

