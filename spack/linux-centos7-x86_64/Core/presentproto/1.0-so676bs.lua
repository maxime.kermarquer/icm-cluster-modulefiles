-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2018-09-17 11:48:56.080972
--
-- presentproto@1.0%gcc@4.8.5 arch=linux-centos7-x86_64 /so676bs
--

whatis([[Name : presentproto]])
whatis([[Version : 1.0]])
whatis([[Short description : Present protocol specification and Xlib/Xserver headers.]])

help([[Present protocol specification and Xlib/Xserver headers.]])



prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-4.8.5/presentproto-1.0-so676bshkzyhoythbttkgjf7bnmuq3js/lib", ":")
prepend_path("LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-4.8.5/presentproto-1.0-so676bshkzyhoythbttkgjf7bnmuq3js/lib", ":")
prepend_path("CPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-4.8.5/presentproto-1.0-so676bshkzyhoythbttkgjf7bnmuq3js/include", ":")
prepend_path("PKG_CONFIG_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-4.8.5/presentproto-1.0-so676bshkzyhoythbttkgjf7bnmuq3js/lib/pkgconfig", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-4.8.5/presentproto-1.0-so676bshkzyhoythbttkgjf7bnmuq3js/", ":")

