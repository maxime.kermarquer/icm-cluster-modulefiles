-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2018-09-17 11:49:51.639813
--
-- sqlite@3.23.1%gcc@4.8.5~functions arch=linux-centos7-x86_64 /4qpziz4
--

whatis([[Name : sqlite]])
whatis([[Version : 3.23.1]])
whatis([[Short description : SQLite3 is an SQL database engine in a C library. Programs that link the SQLite3 library can have SQL database access without running a separate RDBMS process. ]])

help([[SQLite3 is an SQL database engine in a C library. Programs that link the
SQLite3 library can have SQL database access without running a separate
RDBMS process.]])



prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-4.8.5/sqlite-3.23.1-4qpziz4rfanmsqwqjfakeyks2mavtrll/bin", ":")
prepend_path("MANPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-4.8.5/sqlite-3.23.1-4qpziz4rfanmsqwqjfakeyks2mavtrll/share/man", ":")
prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-4.8.5/sqlite-3.23.1-4qpziz4rfanmsqwqjfakeyks2mavtrll/lib", ":")
prepend_path("LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-4.8.5/sqlite-3.23.1-4qpziz4rfanmsqwqjfakeyks2mavtrll/lib", ":")
prepend_path("CPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-4.8.5/sqlite-3.23.1-4qpziz4rfanmsqwqjfakeyks2mavtrll/include", ":")
prepend_path("PKG_CONFIG_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-4.8.5/sqlite-3.23.1-4qpziz4rfanmsqwqjfakeyks2mavtrll/lib/pkgconfig", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-4.8.5/sqlite-3.23.1-4qpziz4rfanmsqwqjfakeyks2mavtrll/", ":")

