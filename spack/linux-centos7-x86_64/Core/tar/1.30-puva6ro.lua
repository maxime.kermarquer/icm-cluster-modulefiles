-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2018-08-31 10:06:59.817276
--
-- tar@1.30%gcc@4.8.5 arch=linux-centos7-x86_64 /puva6ro
--

whatis([[Name : tar]])
whatis([[Version : 1.30]])
whatis([[Short description : GNU Tar provides the ability to create tar archives, as well as various other kinds of manipulation.]])

help([[GNU Tar provides the ability to create tar archives, as well as various
other kinds of manipulation.]])



prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-4.8.5/tar-1.30-puva6rockyblvc3emnjgqfalwd4yt5xo/bin", ":")
prepend_path("MANPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-4.8.5/tar-1.30-puva6rockyblvc3emnjgqfalwd4yt5xo/share/man", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-4.8.5/tar-1.30-puva6rockyblvc3emnjgqfalwd4yt5xo/", ":")

