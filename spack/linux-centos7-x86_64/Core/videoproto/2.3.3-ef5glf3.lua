-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2018-09-17 11:48:36.232066
--
-- videoproto@2.3.3%gcc@4.8.5 arch=linux-centos7-x86_64 /ef5glf3
--

whatis([[Name : videoproto]])
whatis([[Version : 2.3.3]])
whatis([[Short description : X Video Extension.]])

help([[X Video Extension. This extension provides a protocol for a video output
mechanism, mainly to rescale video playback in the video controller
hardware.]])



prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-4.8.5/videoproto-2.3.3-ef5glf3ukeqjtfu7xnuaeoqtbp42dwjt/lib", ":")
prepend_path("LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-4.8.5/videoproto-2.3.3-ef5glf3ukeqjtfu7xnuaeoqtbp42dwjt/lib", ":")
prepend_path("CPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-4.8.5/videoproto-2.3.3-ef5glf3ukeqjtfu7xnuaeoqtbp42dwjt/include", ":")
prepend_path("PKG_CONFIG_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-4.8.5/videoproto-2.3.3-ef5glf3ukeqjtfu7xnuaeoqtbp42dwjt/lib/pkgconfig", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-4.8.5/videoproto-2.3.3-ef5glf3ukeqjtfu7xnuaeoqtbp42dwjt/", ":")

