-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2018-09-17 11:46:18.627985
--
-- xextproto@7.3.0%gcc@4.8.5 arch=linux-centos7-x86_64 /v5vttf7
--

whatis([[Name : xextproto]])
whatis([[Version : 7.3.0]])
whatis([[Short description : X Protocol Extensions.]])

help([[X Protocol Extensions.]])



prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-4.8.5/xextproto-7.3.0-v5vttf7jlxd4lkqhrtc4x5zflbudgpov/lib", ":")
prepend_path("LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-4.8.5/xextproto-7.3.0-v5vttf7jlxd4lkqhrtc4x5zflbudgpov/lib", ":")
prepend_path("CPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-4.8.5/xextproto-7.3.0-v5vttf7jlxd4lkqhrtc4x5zflbudgpov/include", ":")
prepend_path("PKG_CONFIG_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-4.8.5/xextproto-7.3.0-v5vttf7jlxd4lkqhrtc4x5zflbudgpov/lib/pkgconfig", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-4.8.5/xextproto-7.3.0-v5vttf7jlxd4lkqhrtc4x5zflbudgpov/", ":")

