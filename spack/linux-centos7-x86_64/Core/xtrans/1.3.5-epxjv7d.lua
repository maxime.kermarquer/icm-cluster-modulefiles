-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2018-09-17 11:46:22.619782
--
-- xtrans@1.3.5%gcc@4.8.5 arch=linux-centos7-x86_64 /epxjv7d
--

whatis([[Name : xtrans]])
whatis([[Version : 1.3.5]])
whatis([[Short description : xtrans is a library of code that is shared among various X packages to handle network protocol transport in a modular fashion, allowing a single place to add new transport types. It is used by the X server, libX11, libICE, the X font server, and related components.]])

help([[xtrans is a library of code that is shared among various X packages to
handle network protocol transport in a modular fashion, allowing a
single place to add new transport types. It is used by the X server,
libX11, libICE, the X font server, and related components.]])



prepend_path("ACLOCAL_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-4.8.5/xtrans-1.3.5-epxjv7dn6fmunguuyr6olmpgkrpkkoxv/share/aclocal", ":")
prepend_path("CPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-4.8.5/xtrans-1.3.5-epxjv7dn6fmunguuyr6olmpgkrpkkoxv/include", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-4.8.5/xtrans-1.3.5-epxjv7dn6fmunguuyr6olmpgkrpkkoxv/", ":")

