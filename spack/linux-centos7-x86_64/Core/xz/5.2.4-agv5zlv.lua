-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2018-08-31 10:04:27.670722
--
-- xz@5.2.4%gcc@4.8.5 arch=linux-centos7-x86_64 /agv5zlv
--

whatis([[Name : xz]])
whatis([[Version : 5.2.4]])
whatis([[Short description : XZ Utils is free general-purpose data compression software with high compression ratio. XZ Utils were written for POSIX-like systems, but also work on some not-so-POSIX systems. XZ Utils are the successor to LZMA Utils.]])

help([[XZ Utils is free general-purpose data compression software with high
compression ratio. XZ Utils were written for POSIX-like systems, but
also work on some not-so-POSIX systems. XZ Utils are the successor to
LZMA Utils.]])



prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-4.8.5/xz-5.2.4-agv5zlv7phi4ye3evlpv3t4ypgve4bpe/bin", ":")
prepend_path("MANPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-4.8.5/xz-5.2.4-agv5zlv7phi4ye3evlpv3t4ypgve4bpe/share/man", ":")
prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-4.8.5/xz-5.2.4-agv5zlv7phi4ye3evlpv3t4ypgve4bpe/lib", ":")
prepend_path("LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-4.8.5/xz-5.2.4-agv5zlv7phi4ye3evlpv3t4ypgve4bpe/lib", ":")
prepend_path("CPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-4.8.5/xz-5.2.4-agv5zlv7phi4ye3evlpv3t4ypgve4bpe/include", ":")
prepend_path("PKG_CONFIG_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-4.8.5/xz-5.2.4-agv5zlv7phi4ye3evlpv3t4ypgve4bpe/lib/pkgconfig", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-4.8.5/xz-5.2.4-agv5zlv7phi4ye3evlpv3t4ypgve4bpe/", ":")

