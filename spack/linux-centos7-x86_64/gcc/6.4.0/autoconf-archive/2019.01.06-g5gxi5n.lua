-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2021-01-13 16:56:24.271162
--
-- autoconf-archive@2019.01.06%gcc@6.4.0 arch=linux-centos7-broadwell/g5gxi5n
--

whatis([[Name : autoconf-archive]])
whatis([[Version : 2019.01.06]])
whatis([[Target : broadwell]])
whatis([[Short description : The GNU Autoconf Archive is a collection of more than 500 macros for GNU Autoconf.]])

help([[The GNU Autoconf Archive is a collection of more than 500 macros for GNU
Autoconf.]])



prepend_path("ACLOCAL_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/autoconf-archive-2019.01.06-g5gxi5nnm5sm7udcrttlobzeqhcogu56/share/aclocal", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/autoconf-archive-2019.01.06-g5gxi5nnm5sm7udcrttlobzeqhcogu56/", ":")

