-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2018-09-18 16:53:29.676615
--
-- autoconf@2.69%gcc@6.4.0 arch=linux-centos7-x86_64 /6yqcpou
--

whatis([[Name : autoconf]])
whatis([[Version : 2.69]])
whatis([[Short description : Autoconf -- system configuration part of autotools]])

help([[Autoconf -- system configuration part of autotools]])



prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/autoconf-2.69-6yqcpouu45ynoa3ia2n3elqibantilca/bin", ":")
prepend_path("MANPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/autoconf-2.69-6yqcpouu45ynoa3ia2n3elqibantilca/share/man", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/autoconf-2.69-6yqcpouu45ynoa3ia2n3elqibantilca/", ":")

