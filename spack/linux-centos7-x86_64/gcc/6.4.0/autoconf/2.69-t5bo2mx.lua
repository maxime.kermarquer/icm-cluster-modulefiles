-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2021-01-13 17:30:25.247465
--
-- autoconf@2.69%gcc@6.4.0 arch=linux-centos7-broadwell/t5bo2mx
--

whatis([[Name : autoconf]])
whatis([[Version : 2.69]])
whatis([[Target : broadwell]])
whatis([[Short description : Autoconf -- system configuration part of autotools]])

help([[Autoconf -- system configuration part of autotools]])



prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/autoconf-2.69-t5bo2mxuzlicgbtk575k5hqdz7k5s2rx/bin", ":")
prepend_path("MANPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/autoconf-2.69-t5bo2mxuzlicgbtk575k5hqdz7k5s2rx/share/man", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/autoconf-2.69-t5bo2mxuzlicgbtk575k5hqdz7k5s2rx/", ":")

