-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2018-09-18 16:53:36.562466
--
-- automake@1.16.1%gcc@6.4.0 arch=linux-centos7-x86_64 /yctt74f
--

whatis([[Name : automake]])
whatis([[Version : 1.16.1]])
whatis([[Short description : Automake -- make file builder part of autotools]])

help([[Automake -- make file builder part of autotools]])



prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/automake-1.16.1-yctt74fsm6xnlfw3gv2rde6wqfuaobrl/bin", ":")
prepend_path("MANPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/automake-1.16.1-yctt74fsm6xnlfw3gv2rde6wqfuaobrl/share/man", ":")
prepend_path("ACLOCAL_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/automake-1.16.1-yctt74fsm6xnlfw3gv2rde6wqfuaobrl/share/aclocal", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/automake-1.16.1-yctt74fsm6xnlfw3gv2rde6wqfuaobrl/", ":")

