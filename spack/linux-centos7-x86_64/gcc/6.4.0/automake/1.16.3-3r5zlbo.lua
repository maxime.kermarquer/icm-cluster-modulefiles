-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2021-01-13 17:43:30.799668
--
-- automake@1.16.3%gcc@6.4.0 arch=linux-centos7-broadwell/3r5zlbo
--

whatis([[Name : automake]])
whatis([[Version : 1.16.3]])
whatis([[Target : broadwell]])
whatis([[Short description : Automake -- make file builder part of autotools]])

help([[Automake -- make file builder part of autotools]])



prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/automake-1.16.3-3r5zlbonkxud74iq3oa4etivkfxajpwi/bin", ":")
prepend_path("MANPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/automake-1.16.3-3r5zlbonkxud74iq3oa4etivkfxajpwi/share/man", ":")
prepend_path("ACLOCAL_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/automake-1.16.3-3r5zlbonkxud74iq3oa4etivkfxajpwi/share/aclocal", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/automake-1.16.3-3r5zlbonkxud74iq3oa4etivkfxajpwi/", ":")

