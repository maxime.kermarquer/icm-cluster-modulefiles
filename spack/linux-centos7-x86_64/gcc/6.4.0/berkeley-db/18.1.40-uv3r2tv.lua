-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2021-01-13 16:47:53.600722
--
-- berkeley-db@18.1.40%gcc@6.4.0 arch=linux-centos7-broadwell/uv3r2tv
--

whatis([[Name : berkeley-db]])
whatis([[Version : 18.1.40]])
whatis([[Target : broadwell]])
whatis([[Short description : Oracle Berkeley DB]])
whatis([[Configure options : --disable-static --enable-cxx --enable-dbm --enable-stl --enable-compat185 --with-repmgr-ssl=no]])

help([[Oracle Berkeley DB]])



prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/berkeley-db-18.1.40-uv3r2tvfk242lf2ckigfyzw4aaic74j3/lib", ":")
prepend_path("LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/berkeley-db-18.1.40-uv3r2tvfk242lf2ckigfyzw4aaic74j3/lib", ":")
prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/berkeley-db-18.1.40-uv3r2tvfk242lf2ckigfyzw4aaic74j3/bin", ":")
prepend_path("C_INCLUDE_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/berkeley-db-18.1.40-uv3r2tvfk242lf2ckigfyzw4aaic74j3/include", ":")
prepend_path("CPLUS_INCLUDE_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/berkeley-db-18.1.40-uv3r2tvfk242lf2ckigfyzw4aaic74j3/include", ":")
prepend_path("INCLUDE", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/berkeley-db-18.1.40-uv3r2tvfk242lf2ckigfyzw4aaic74j3/include", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/berkeley-db-18.1.40-uv3r2tvfk242lf2ckigfyzw4aaic74j3/", ":")

