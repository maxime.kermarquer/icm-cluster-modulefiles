-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2018-09-19 11:07:47.654946
--
-- binutils@2.31.1%gcc@6.4.0+gold~libiberty~plugins arch=linux-centos7-x86_64 /qnvgwri
--

whatis([[Name : binutils]])
whatis([[Version : 2.31.1]])
whatis([[Short description : GNU binutils, which contain the linker, assembler, objdump and others]])
whatis([[Configure options : --with-system-zlib --disable-dependency-tracking --disable-werror --enable-interwork --enable-multilib --enable-shared --enable-64-bit-bfd --enable-targets=all --with-sysroot=/ --enable-gold]])

help([[GNU binutils, which contain the linker, assembler, objdump and others]])



prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/binutils-2.31.1-qnvgwrix4n4wo7hpcoclx5kpxjrlvvqz/bin", ":")
prepend_path("MANPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/binutils-2.31.1-qnvgwrix4n4wo7hpcoclx5kpxjrlvvqz/share/man", ":")
prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/binutils-2.31.1-qnvgwrix4n4wo7hpcoclx5kpxjrlvvqz/lib", ":")
prepend_path("LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/binutils-2.31.1-qnvgwrix4n4wo7hpcoclx5kpxjrlvvqz/lib", ":")
prepend_path("CPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/binutils-2.31.1-qnvgwrix4n4wo7hpcoclx5kpxjrlvvqz/include", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/binutils-2.31.1-qnvgwrix4n4wo7hpcoclx5kpxjrlvvqz/", ":")
prepend_path("CPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/libxml2-2.9.8-m3rdnxm66wlzs5gqsm5pqlzqvideutnk/include/libxml2", ":")

