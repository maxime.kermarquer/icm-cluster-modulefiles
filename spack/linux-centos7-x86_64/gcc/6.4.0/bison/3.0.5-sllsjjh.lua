-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2018-09-19 11:03:31.579666
--
-- bison@3.0.5%gcc@6.4.0 arch=linux-centos7-x86_64 /sllsjjh
--

whatis([[Name : bison]])
whatis([[Version : 3.0.5]])
whatis([[Short description : Bison is a general-purpose parser generator that converts an annotated context-free grammar into a deterministic LR or generalized LR (GLR) parser employing LALR(1) parser tables.]])

help([[Bison is a general-purpose parser generator that converts an annotated
context-free grammar into a deterministic LR or generalized LR (GLR)
parser employing LALR(1) parser tables.]])



prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/bison-3.0.5-sllsjjhwtr3f6j4lwwn2hxfskcev4dmy/bin", ":")
prepend_path("MANPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/bison-3.0.5-sllsjjhwtr3f6j4lwwn2hxfskcev4dmy/share/man", ":")
prepend_path("ACLOCAL_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/bison-3.0.5-sllsjjhwtr3f6j4lwwn2hxfskcev4dmy/share/aclocal", ":")
prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/bison-3.0.5-sllsjjhwtr3f6j4lwwn2hxfskcev4dmy/lib", ":")
prepend_path("LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/bison-3.0.5-sllsjjhwtr3f6j4lwwn2hxfskcev4dmy/lib", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/bison-3.0.5-sllsjjhwtr3f6j4lwwn2hxfskcev4dmy/", ":")

