-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2021-11-30 16:26:53.744559
--
-- bison@3.7.4%gcc@6.4.0 arch=linux-centos7-broadwell/q6wcjcf
--

whatis([[Name : bison]])
whatis([[Version : 3.7.4]])
whatis([[Target : broadwell]])
whatis([[Short description : Bison is a general-purpose parser generator that converts an annotated context-free grammar into a deterministic LR or generalized LR (GLR) parser employing LALR(1) parser tables.]])

help([[Bison is a general-purpose parser generator that converts an annotated
context-free grammar into a deterministic LR or generalized LR (GLR)
parser employing LALR(1) parser tables.]])



prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/bison-3.7.4-q6wcjcfhs2jlnfgscn56x5msy2zfmeqn/lib", ":")
prepend_path("LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/bison-3.7.4-q6wcjcfhs2jlnfgscn56x5msy2zfmeqn/lib", ":")
prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/bison-3.7.4-q6wcjcfhs2jlnfgscn56x5msy2zfmeqn/bin", ":")
prepend_path("MANPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/bison-3.7.4-q6wcjcfhs2jlnfgscn56x5msy2zfmeqn/share/man", ":")
prepend_path("ACLOCAL_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/bison-3.7.4-q6wcjcfhs2jlnfgscn56x5msy2zfmeqn/share/aclocal", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/bison-3.7.4-q6wcjcfhs2jlnfgscn56x5msy2zfmeqn/", ":")

