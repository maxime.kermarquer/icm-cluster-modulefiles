-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2023-05-04 10:46:25.740662
--
-- boost@1.77.0%gcc@6.4.0+atomic+chrono~clanglibcpp~container~context~contract~coroutine+date_time~debug+exception~fiber+filesystem+graph~graph_parallel~icu+iostreams~json+locale+log+math~mpi+multithreaded~nowide~numpy~pic+program_options~python+random+regex+serialization+shared+signals~singlethreaded~stacktrace+system~taggedlayout+test+thread+timer~type_erasure~versionedlayout+wave build_system=generic cxxstd=14 patches=4849671,a440f96 visibility=hidden arch=linux-centos7-broadwell/uybo3zg
--

whatis([[Name : boost]])
whatis([[Version : 1.77.0]])
whatis([[Target : broadwell]])
whatis([[Short description : Boost provides free peer-reviewed portable C++ source libraries, emphasizing libraries that work well with the C++ Standard Library.]])

help([[Name   : boost]])
help([[Version: 1.77.0]])
help([[Target : broadwell]])
help()
help([[Boost provides free peer-reviewed portable C++ source libraries,
emphasizing libraries that work well with the C++ Standard Library.
Boost libraries are intended to be widely useful, and usable across a
broad spectrum of applications. The Boost license encourages both
commercial and non-commercial use.]])


depends_on("bzip2/1.0.8-hoiufcd")
depends_on("xz/5.2.5-loqs5h3")
depends_on("zlib/1.2.11-wp5dasp")
depends_on("zstd/1.5.5-kw2nhqz")

prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/boost-1.77.0-uybo3zgj5zlefxsnglayc5xgru7fp4l6/.", ":")
setenv("BOOST_ROOT", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/boost-1.77.0-uybo3zgj5zlefxsnglayc5xgru7fp4l6")

