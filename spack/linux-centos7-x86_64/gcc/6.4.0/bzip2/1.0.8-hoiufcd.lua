-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2021-01-13 17:11:00.262415
--
-- bzip2@1.0.8%gcc@6.4.0+shared arch=linux-centos7-broadwell/hoiufcd
--

whatis([[Name : bzip2]])
whatis([[Version : 1.0.8]])
whatis([[Target : broadwell]])
whatis([[Short description : bzip2 is a freely available, patent free high-quality data compressor. It typically compresses files to within 10% to 15% of the best available techniques (the PPM family of statistical compressors), whilst being around twice as fast at compression and six times faster at decompression.]])

help([[bzip2 is a freely available, patent free high-quality data compressor.
It typically compresses files to within 10% to 15% of the best available
techniques (the PPM family of statistical compressors), whilst being
around twice as fast at compression and six times faster at
decompression.]])



prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/bzip2-1.0.8-hoiufcd7sjxd2ruqcbriem6a4cvinl7b/lib", ":")
prepend_path("LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/bzip2-1.0.8-hoiufcd7sjxd2ruqcbriem6a4cvinl7b/lib", ":")
prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/bzip2-1.0.8-hoiufcd7sjxd2ruqcbriem6a4cvinl7b/bin", ":")
prepend_path("MANPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/bzip2-1.0.8-hoiufcd7sjxd2ruqcbriem6a4cvinl7b/man", ":")
prepend_path("C_INCLUDE_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/bzip2-1.0.8-hoiufcd7sjxd2ruqcbriem6a4cvinl7b/include", ":")
prepend_path("CPLUS_INCLUDE_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/bzip2-1.0.8-hoiufcd7sjxd2ruqcbriem6a4cvinl7b/include", ":")
prepend_path("INCLUDE", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/bzip2-1.0.8-hoiufcd7sjxd2ruqcbriem6a4cvinl7b/include", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/bzip2-1.0.8-hoiufcd7sjxd2ruqcbriem6a4cvinl7b/", ":")

