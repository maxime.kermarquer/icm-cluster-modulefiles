-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2019-02-06 20:03:55.747246
--
-- cairo@1.14.6%gcc@6.4.0+X arch=linux-centos7-x86_64 /hyzkrwo
--

whatis([[Name : cairo]])
whatis([[Version : 1.14.6]])
whatis([[Short description : Cairo is a 2D graphics library with support for multiple output devices.]])
whatis([[Configure options : --disable-trace --enable-tee --enable-xlib --enable-xcb]])

help([[Cairo is a 2D graphics library with support for multiple output devices.]])



prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/cairo-1.14.6-hyzkrwoy75wu7n56ejrsaqrvpz6eu4wj/bin", ":")
prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/cairo-1.14.6-hyzkrwoy75wu7n56ejrsaqrvpz6eu4wj/lib", ":")
prepend_path("LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/cairo-1.14.6-hyzkrwoy75wu7n56ejrsaqrvpz6eu4wj/lib", ":")
prepend_path("CPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/cairo-1.14.6-hyzkrwoy75wu7n56ejrsaqrvpz6eu4wj/include", ":")
prepend_path("PKG_CONFIG_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/cairo-1.14.6-hyzkrwoy75wu7n56ejrsaqrvpz6eu4wj/lib/pkgconfig", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/cairo-1.14.6-hyzkrwoy75wu7n56ejrsaqrvpz6eu4wj/", ":")
prepend_path("CPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/libxml2-2.9.8-m3rdnxm66wlzs5gqsm5pqlzqvideutnk/include/libxml2", ":")
prepend_path("CPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/freetype-2.7.1-fnguk6ypqcpy23j3j2n3l62sjvfd3sf2/include/freetype2", ":")

