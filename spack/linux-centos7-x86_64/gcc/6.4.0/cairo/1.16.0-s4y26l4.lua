-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2021-11-30 16:33:17.761351
--
-- cairo@1.16.0%gcc@6.4.0~X~fc~ft~gobject+pdf~png~svg patches=7c4da77767fe9feb03f8051def0832f0c67f99162913275cfa127a88df19cf51 arch=linux-centos7-broadwell/s4y26l4
--

whatis([[Name : cairo]])
whatis([[Version : 1.16.0]])
whatis([[Target : broadwell]])
whatis([[Short description : Cairo is a 2D graphics library with support for multiple output devices.]])
whatis([[Configure options : --disable-trace --enable-tee --disable-xlib --disable-xcb --enable-pdf --disable-gobject --disable-ft --disable-fc]])

help([[Cairo is a 2D graphics library with support for multiple output devices.]])



prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/cairo-1.16.0-s4y26l4irhvdm5ncngmi5wzlim374cfx/lib", ":")
prepend_path("LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/cairo-1.16.0-s4y26l4irhvdm5ncngmi5wzlim374cfx/lib", ":")
prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/cairo-1.16.0-s4y26l4irhvdm5ncngmi5wzlim374cfx/bin", ":")
prepend_path("C_INCLUDE_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/cairo-1.16.0-s4y26l4irhvdm5ncngmi5wzlim374cfx/include", ":")
prepend_path("CPLUS_INCLUDE_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/cairo-1.16.0-s4y26l4irhvdm5ncngmi5wzlim374cfx/include", ":")
prepend_path("INCLUDE", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/cairo-1.16.0-s4y26l4irhvdm5ncngmi5wzlim374cfx/include", ":")
prepend_path("PKG_CONFIG_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/cairo-1.16.0-s4y26l4irhvdm5ncngmi5wzlim374cfx/lib/pkgconfig", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/cairo-1.16.0-s4y26l4irhvdm5ncngmi5wzlim374cfx/", ":")
prepend_path("PYTHONPATH", "", ":")

