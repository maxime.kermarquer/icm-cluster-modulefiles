-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2023-05-09 16:14:55.825836
--
-- cairo@1.16.0%gcc@6.4.0~X+fc+ft+gobject+pdf~png~svg build_system=autotools patches=7c4da77 arch=linux-centos7-broadwell/tbkrl52
--

whatis([[Name : cairo]])
whatis([[Version : 1.16.0]])
whatis([[Target : broadwell]])
whatis([[Short description : Cairo is a 2D graphics library with support for multiple output devices.]])
whatis([[Configure options : --disable-trace --enable-tee --disable-xlib --disable-xcb --enable-pdf --enable-gobject --enable-ft --enable-fc]])

help([[Name   : cairo]])
help([[Version: 1.16.0]])
help([[Target : broadwell]])
help()
help([[Cairo is a 2D graphics library with support for multiple output devices.]])


depends_on("linux-centos7-x86_64/gcc/6.4.0/fontconfig/2.14.2-nuwul4i")
depends_on("linux-centos7-x86_64/gcc/6.4.0/freetype/2.11.1-e77ncz4")
depends_on("linux-centos7-x86_64/gcc/6.4.0/glib/2.66.2-fbdjtsd")
depends_on("linux-centos7-x86_64/gcc/6.4.0/pixman/0.40.0-cdhyk76")

prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/cairo-1.16.0-tbkrl52lkugdosrpsa3b62gessmzvhzw/bin", ":")
prepend_path("CPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/cairo-1.16.0-tbkrl52lkugdosrpsa3b62gessmzvhzw/include/cairo", ":")
prepend_path("LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/cairo-1.16.0-tbkrl52lkugdosrpsa3b62gessmzvhzw/lib", ":")
prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/cairo-1.16.0-tbkrl52lkugdosrpsa3b62gessmzvhzw/lib", ":")
prepend_path("PKG_CONFIG_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/cairo-1.16.0-tbkrl52lkugdosrpsa3b62gessmzvhzw/lib/pkgconfig", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/cairo-1.16.0-tbkrl52lkugdosrpsa3b62gessmzvhzw/.", ":")

