-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2019-02-05 18:38:35.151042
--
-- ccache@3.3.4%gcc@6.4.0 arch=linux-centos7-x86_64 /76eib4q
--

whatis([[Name : ccache]])
whatis([[Version : 3.3.4]])
whatis([[Short description : ccache is a compiler cache. It speeds up recompilation by caching previous compilations and detecting when the same compilation is being done again.]])

help([[ccache is a compiler cache. It speeds up recompilation by caching
previous compilations and detecting when the same compilation is being
done again.]])



prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/ccache-3.3.4-76eib4q2rymu7dbjvpijppcnmyyq47mc/bin", ":")
prepend_path("MANPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/ccache-3.3.4-76eib4q2rymu7dbjvpijppcnmyyq47mc/share/man", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/ccache-3.3.4-76eib4q2rymu7dbjvpijppcnmyyq47mc/", ":")
prepend_path("CPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/libxml2-2.9.8-m3rdnxm66wlzs5gqsm5pqlzqvideutnk/include/libxml2", ":")

