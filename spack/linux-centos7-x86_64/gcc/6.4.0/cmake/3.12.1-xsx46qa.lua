-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2018-09-19 11:43:15.825905
--
-- cmake@3.12.1%gcc@6.4.0~doc+ncurses+openssl+ownlibs patches=dd3a40d4d92f6b2158b87d6fb354c277947c776424aa03f6dc8096cf3135f5d0 ~qt arch=linux-centos7-x86_64 /xsx46qa
--

whatis([[Name : cmake]])
whatis([[Version : 3.12.1]])
whatis([[Short description : A cross-platform, open-source build system. CMake is a family of tools designed to build, test and package software.]])

help([[A cross-platform, open-source build system. CMake is a family of tools
designed to build, test and package software.]])



prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/cmake-3.12.1-xsx46qaqtcddtdnhlbwdnxxltjgt4cya/bin", ":")
prepend_path("ACLOCAL_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/cmake-3.12.1-xsx46qaqtcddtdnhlbwdnxxltjgt4cya/share/aclocal", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/cmake-3.12.1-xsx46qaqtcddtdnhlbwdnxxltjgt4cya/", ":")

