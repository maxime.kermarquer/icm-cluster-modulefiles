-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2021-01-13 17:38:52.740107
--
-- cmake@3.19.2%gcc@6.4.0~doc+ncurses+openssl+ownlibs~qt arch=linux-centos7-broadwell/ofa6nkx
--

whatis([[Name : cmake]])
whatis([[Version : 3.19.2]])
whatis([[Target : broadwell]])
whatis([[Short description : A cross-platform, open-source build system. CMake is a family of tools designed to build, test and package software. ]])

help([[A cross-platform, open-source build system. CMake is a family of tools
designed to build, test and package software.]])



prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/cmake-3.19.2-ofa6nkxee66gvdt2ju5b76lnpwh2wmde/bin", ":")
prepend_path("ACLOCAL_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/cmake-3.19.2-ofa6nkxee66gvdt2ju5b76lnpwh2wmde/share/aclocal", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/cmake-3.19.2-ofa6nkxee66gvdt2ju5b76lnpwh2wmde/", ":")

