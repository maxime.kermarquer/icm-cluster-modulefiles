-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2019-09-27 09:37:25.375483
--
-- compositeproto@0.4.2%gcc@6.4.0 arch=linux-centos7-x86_64 /datuccm
--

whatis([[Name : compositeproto]])
whatis([[Version : 0.4.2]])
whatis([[Short description : Composite Extension.]])

help([[Composite Extension. This package contains header files and
documentation for the composite extension. Library and server
implementations are separate.]])



prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/compositeproto-0.4.2-datuccmvkh3zqirb3neewwbmh2utwnbb/lib", ":")
prepend_path("LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/compositeproto-0.4.2-datuccmvkh3zqirb3neewwbmh2utwnbb/lib", ":")
prepend_path("CPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/compositeproto-0.4.2-datuccmvkh3zqirb3neewwbmh2utwnbb/include", ":")
prepend_path("PKG_CONFIG_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/compositeproto-0.4.2-datuccmvkh3zqirb3neewwbmh2utwnbb/lib/pkgconfig", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/compositeproto-0.4.2-datuccmvkh3zqirb3neewwbmh2utwnbb/", ":")

