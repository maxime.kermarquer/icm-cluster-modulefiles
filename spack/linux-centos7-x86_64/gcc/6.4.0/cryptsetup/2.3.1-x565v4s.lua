-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2021-01-13 17:49:38.443371
--
-- cryptsetup@2.3.1%gcc@6.4.0 arch=linux-centos7-broadwell/x565v4s
--

whatis([[Name : cryptsetup]])
whatis([[Version : 2.3.1]])
whatis([[Target : broadwell]])
whatis([[Short description : Cryptsetup and LUKS - open-source disk encryption.]])
whatis([[Configure options : systemd_tmpfilesdir=/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/cryptsetup-2.3.1-x565v4sukge47vdftnipds7iejlndrhu/tmpfiles.d --with-crypto_backend=openssl]])

help([[Cryptsetup and LUKS - open-source disk encryption.]])



prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/cryptsetup-2.3.1-x565v4sukge47vdftnipds7iejlndrhu/lib", ":")
prepend_path("LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/cryptsetup-2.3.1-x565v4sukge47vdftnipds7iejlndrhu/lib", ":")
prepend_path("MANPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/cryptsetup-2.3.1-x565v4sukge47vdftnipds7iejlndrhu/share/man", ":")
prepend_path("C_INCLUDE_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/cryptsetup-2.3.1-x565v4sukge47vdftnipds7iejlndrhu/include", ":")
prepend_path("CPLUS_INCLUDE_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/cryptsetup-2.3.1-x565v4sukge47vdftnipds7iejlndrhu/include", ":")
prepend_path("INCLUDE", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/cryptsetup-2.3.1-x565v4sukge47vdftnipds7iejlndrhu/include", ":")
prepend_path("PKG_CONFIG_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/cryptsetup-2.3.1-x565v4sukge47vdftnipds7iejlndrhu/lib/pkgconfig", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/cryptsetup-2.3.1-x565v4sukge47vdftnipds7iejlndrhu/", ":")

