-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2019-03-20 16:04:46.128479
--
-- cups@2.2.3%gcc@6.4.0 arch=linux-centos7-x86_64 /wpzhyqw
--

whatis([[Name : cups]])
whatis([[Version : 2.2.3]])
whatis([[Short description : CUPS is the standards-based, open source printing system developed by Apple Inc. for macOS and other UNIX-like operating systems. CUPS uses the Internet Printing Protocol (IPP) to support printing to local and network printers. This provides the core CUPS libraries, not a complete CUPS install.]])
whatis([[Configure options : --enable-gnutls --with-components=core]])

help([[CUPS is the standards-based, open source printing system developed by
Apple Inc. for macOS and other UNIX-like operating systems. CUPS uses
the Internet Printing Protocol (IPP) to support printing to local and
network printers. This provides the core CUPS libraries, not a complete
CUPS install.]])



prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/cups-2.2.3-wpzhyqwi3yxcclptso6lbvujf75zy453/bin", ":")
prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/cups-2.2.3-wpzhyqwi3yxcclptso6lbvujf75zy453/lib64", ":")
prepend_path("LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/cups-2.2.3-wpzhyqwi3yxcclptso6lbvujf75zy453/lib64", ":")
prepend_path("CPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/cups-2.2.3-wpzhyqwi3yxcclptso6lbvujf75zy453/include", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/cups-2.2.3-wpzhyqwi3yxcclptso6lbvujf75zy453/", ":")
prepend_path("CPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/libxml2-2.9.8-m3rdnxm66wlzs5gqsm5pqlzqvideutnk/include/libxml2", ":")

