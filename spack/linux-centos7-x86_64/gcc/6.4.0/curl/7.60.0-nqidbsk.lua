-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2018-09-18 17:04:17.886623
--
-- curl@7.60.0%gcc@6.4.0~darwinssl~libssh~libssh2~nghttp2 arch=linux-centos7-x86_64 /nqidbsk
--

whatis([[Name : curl]])
whatis([[Version : 7.60.0]])
whatis([[Short description : cURL is an open source command line tool and library for transferring data with URL syntax]])
whatis([[Configure options : --with-zlib=/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/zlib-1.2.11-ufwqfd4zrwuuu7fmguamaqokn5plxyns --with-ssl=/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/openssl-1.0.2o-qxfzezxhp4blvxkj6vsnj43vpiqfjs3x --without-nghttp2 --without-libssh2 --without-libssh]])

help([[cURL is an open source command line tool and library for transferring
data with URL syntax]])



prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/curl-7.60.0-nqidbsk4n4he3jls4s7y3i4befhyaduc/bin", ":")
prepend_path("MANPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/curl-7.60.0-nqidbsk4n4he3jls4s7y3i4befhyaduc/share/man", ":")
prepend_path("ACLOCAL_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/curl-7.60.0-nqidbsk4n4he3jls4s7y3i4befhyaduc/share/aclocal", ":")
prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/curl-7.60.0-nqidbsk4n4he3jls4s7y3i4befhyaduc/lib", ":")
prepend_path("LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/curl-7.60.0-nqidbsk4n4he3jls4s7y3i4befhyaduc/lib", ":")
prepend_path("CPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/curl-7.60.0-nqidbsk4n4he3jls4s7y3i4befhyaduc/include", ":")
prepend_path("PKG_CONFIG_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/curl-7.60.0-nqidbsk4n4he3jls4s7y3i4befhyaduc/lib/pkgconfig", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/curl-7.60.0-nqidbsk4n4he3jls4s7y3i4befhyaduc/", ":")

