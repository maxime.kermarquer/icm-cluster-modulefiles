-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2021-01-13 17:42:44.352767
--
-- curl@7.74.0%gcc@6.4.0~darwinssl~gssapi~libssh~libssh2~nghttp2 arch=linux-centos7-broadwell/uspov5c
--

whatis([[Name : curl]])
whatis([[Version : 7.74.0]])
whatis([[Target : broadwell]])
whatis([[Short description : cURL is an open source command line tool and library for transferring data with URL syntax]])
whatis([[Configure options : --with-zlib=/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/zlib-1.2.11-wp5daspjexwgd5rft64ekrbjk4uarol2 --with-libidn2=/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/libidn2-2.3.0-tkwd4vytzm66bbqk37sim6akijtojkm4 --with-ssl=/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/openssl-1.1.1i-kphel3rter2r2c5g4trtfsrzvjajvy5k --without-nghttp2 --without-libssh2 --without-libssh]])

help([[cURL is an open source command line tool and library for transferring
data with URL syntax]])



prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/curl-7.74.0-uspov5cgjhwhmwedyzn66britqnfe6ce/lib", ":")
prepend_path("LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/curl-7.74.0-uspov5cgjhwhmwedyzn66britqnfe6ce/lib", ":")
prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/curl-7.74.0-uspov5cgjhwhmwedyzn66britqnfe6ce/bin", ":")
prepend_path("MANPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/curl-7.74.0-uspov5cgjhwhmwedyzn66britqnfe6ce/share/man", ":")
prepend_path("ACLOCAL_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/curl-7.74.0-uspov5cgjhwhmwedyzn66britqnfe6ce/share/aclocal", ":")
prepend_path("C_INCLUDE_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/curl-7.74.0-uspov5cgjhwhmwedyzn66britqnfe6ce/include", ":")
prepend_path("CPLUS_INCLUDE_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/curl-7.74.0-uspov5cgjhwhmwedyzn66britqnfe6ce/include", ":")
prepend_path("INCLUDE", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/curl-7.74.0-uspov5cgjhwhmwedyzn66britqnfe6ce/include", ":")
prepend_path("PKG_CONFIG_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/curl-7.74.0-uspov5cgjhwhmwedyzn66britqnfe6ce/lib/pkgconfig", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/curl-7.74.0-uspov5cgjhwhmwedyzn66britqnfe6ce/", ":")

