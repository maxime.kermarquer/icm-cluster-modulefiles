-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2023-05-17 16:16:38.610390
--
-- curl@7.88.1%gcc@6.4.0~gssapi~ldap+libidn2~librtmp~libssh~libssh2~nghttp2 build_system=autotools libs=shared,static tls=openssl arch=linux-centos7-broadwell/n4o7puq
--

whatis([[Name : curl]])
whatis([[Version : 7.88.1]])
whatis([[Target : broadwell]])
whatis([[Short description : cURL is an open source command line tool and library for transferring data with URL syntax]])

help([[Name   : curl]])
help([[Version: 7.88.1]])
help([[Target : broadwell]])
help()
help([[cURL is an open source command line tool and library for transferring
data with URL syntax]])


depends_on("libidn2/2.3.0-tkwd4vy")
depends_on("openssl/1.1.1i-kphel3r")
depends_on("zlib/1.2.11-wp5dasp")

prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/curl-7.88.1-n4o7puqrpmgkzgimxy5bmuywgjqt456w/bin", ":")
prepend_path("MANPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/curl-7.88.1-n4o7puqrpmgkzgimxy5bmuywgjqt456w/share/man", ":")
prepend_path("ACLOCAL_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/curl-7.88.1-n4o7puqrpmgkzgimxy5bmuywgjqt456w/share/aclocal", ":")
prepend_path("PKG_CONFIG_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/curl-7.88.1-n4o7puqrpmgkzgimxy5bmuywgjqt456w/lib/pkgconfig", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/curl-7.88.1-n4o7puqrpmgkzgimxy5bmuywgjqt456w/.", ":")

