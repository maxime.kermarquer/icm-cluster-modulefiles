-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2018-09-19 11:08:46.228488
--
-- damageproto@1.2.1%gcc@6.4.0 arch=linux-centos7-x86_64 /uiceygp
--

whatis([[Name : damageproto]])
whatis([[Version : 1.2.1]])
whatis([[Short description : X Damage Extension.]])

help([[X Damage Extension. This package contains header files and documentation
for the X Damage extension. Library and server implementations are
separate.]])



prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/damageproto-1.2.1-uiceygp2f45lj3e77dc6gosea6pdi6t3/lib", ":")
prepend_path("LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/damageproto-1.2.1-uiceygp2f45lj3e77dc6gosea6pdi6t3/lib", ":")
prepend_path("CPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/damageproto-1.2.1-uiceygp2f45lj3e77dc6gosea6pdi6t3/include", ":")
prepend_path("PKG_CONFIG_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/damageproto-1.2.1-uiceygp2f45lj3e77dc6gosea6pdi6t3/lib/pkgconfig", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/damageproto-1.2.1-uiceygp2f45lj3e77dc6gosea6pdi6t3/", ":")

