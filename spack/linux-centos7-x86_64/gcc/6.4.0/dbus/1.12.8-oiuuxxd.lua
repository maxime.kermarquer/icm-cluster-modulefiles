-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2019-03-20 15:46:05.245676
--
-- dbus@1.12.8%gcc@6.4.0 arch=linux-centos7-x86_64 /oiuuxxd
--

whatis([[Name : dbus]])
whatis([[Version : 1.12.8]])
whatis([[Short description : D-Bus is a message bus system, a simple way for applications to talk to one another. D-Bus supplies both a system daemon (for events such new hardware device printer queue ) and a per-user-login-session daemon (for general IPC needs among user applications). Also, the message bus is built on top of a general one-to-one message passing framework, which can be used by any two applications to communicate directly (without going through the message bus daemon).]])

help([[D-Bus is a message bus system, a simple way for applications to talk to
one another. D-Bus supplies both a system daemon (for events such new
hardware device printer queue ) and a per-user-login-session daemon (for
general IPC needs among user applications). Also, the message bus is
built on top of a general one-to-one message passing framework, which
can be used by any two applications to communicate directly (without
going through the message bus daemon).]])



prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/dbus-1.12.8-oiuuxxdctkfgerzolm5kqxepkzyrpvho/bin", ":")
prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/dbus-1.12.8-oiuuxxdctkfgerzolm5kqxepkzyrpvho/lib", ":")
prepend_path("LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/dbus-1.12.8-oiuuxxdctkfgerzolm5kqxepkzyrpvho/lib", ":")
prepend_path("CPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/dbus-1.12.8-oiuuxxdctkfgerzolm5kqxepkzyrpvho/include", ":")
prepend_path("PKG_CONFIG_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/dbus-1.12.8-oiuuxxdctkfgerzolm5kqxepkzyrpvho/lib/pkgconfig", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/dbus-1.12.8-oiuuxxdctkfgerzolm5kqxepkzyrpvho/", ":")

