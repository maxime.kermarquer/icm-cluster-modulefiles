-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2021-01-13 17:02:57.262745
--
-- diffutils@3.7%gcc@6.4.0 arch=linux-centos7-broadwell/4lcp4gp
--

whatis([[Name : diffutils]])
whatis([[Version : 3.7]])
whatis([[Target : broadwell]])
whatis([[Short description : GNU Diffutils is a package of several programs related to finding differences between files.]])

help([[GNU Diffutils is a package of several programs related to finding
differences between files.]])



prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/diffutils-3.7-4lcp4gp3guxjp6hjsawmklunlaraa2u7/bin", ":")
prepend_path("MANPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/diffutils-3.7-4lcp4gp3guxjp6hjsawmklunlaraa2u7/share/man", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/diffutils-3.7-4lcp4gp3guxjp6hjsawmklunlaraa2u7/", ":")

