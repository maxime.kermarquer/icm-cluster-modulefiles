-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2021-11-30 16:22:50.360379
--
-- docbook-xml@4.5%gcc@6.4.0 arch=linux-centos7-broadwell/fqta4kz
--

whatis([[Name : docbook-xml]])
whatis([[Version : 4.5]])
whatis([[Target : broadwell]])
whatis([[Short description : Docbook DTD XML files.]])

help([[Docbook DTD XML files.]])



prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/docbook-xml-4.5-fqta4kz2yecxaw6jly6ki3oed43exowt/", ":")
setenv("XML_CATALOG_FILES", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/docbook-xml-4.5-fqta4kz2yecxaw6jly6ki3oed43exowt/catalog.xml")

