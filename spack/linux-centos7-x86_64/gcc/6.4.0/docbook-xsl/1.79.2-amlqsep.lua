-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2021-11-30 16:23:22.243322
--
-- docbook-xsl@1.79.2%gcc@6.4.0 patches=a92c39715c54949ba9369add1809527b8f155b7e2a2b2e30cb4b39ee715f2e30 arch=linux-centos7-broadwell/amlqsep
--

whatis([[Name : docbook-xsl]])
whatis([[Version : 1.79.2]])
whatis([[Target : broadwell]])
whatis([[Short description : DocBook XSLT 1.0 Stylesheets.]])

help([[DocBook XSLT 1.0 Stylesheets.]])



prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/docbook-xsl-1.79.2-amlqsepngejphvxcxuwyo5e3zoud5tf7/lib", ":")
prepend_path("LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/docbook-xsl-1.79.2-amlqsepngejphvxcxuwyo5e3zoud5tf7/lib", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/docbook-xsl-1.79.2-amlqsepngejphvxcxuwyo5e3zoud5tf7/", ":")
prepend_path("XML_CATALOG_FILES", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/docbook-xsl-1.79.2-amlqsepngejphvxcxuwyo5e3zoud5tf7/catalog.xml", " ")

