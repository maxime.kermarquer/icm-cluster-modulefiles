-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2023-05-04 11:55:04.571235
--
-- doxygen@1.9.0%gcc@6.4.0~graphviz~ipo~mscgen build_system=cmake build_type=RelWithDebInfo generator=make arch=linux-centos7-broadwell/pyi5dyi
--

whatis([[Name : doxygen]])
whatis([[Version : 1.9.0]])
whatis([[Target : broadwell]])
whatis([[Short description : Doxygen is the de facto standard tool for generating documentation from annotated C++ sources, but it also supports other popular programming languages such as C, Objective-C, C#, PHP, Java, Python, IDL (Corba, Microsoft, and UNO/OpenOffice flavors), Fortran, VHDL, Tcl, and to some extent D..]])
whatis([[Configure options : -DPYTHON_EXECUTABLE:STRING=/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/python-3.8.7-eoi5k2niuf2tyzvjxcwt6qwaxtwm32ri/bin/python3.8]])

help([[Name   : doxygen]])
help([[Version: 1.9.0]])
help([[Target : broadwell]])
help()
help([[Doxygen is the de facto standard tool for generating documentation from
annotated C++ sources, but it also supports other popular programming
languages such as C, Objective-C, C#, PHP, Java, Python, IDL (Corba,
Microsoft, and UNO/OpenOffice flavors), Fortran, VHDL, Tcl, and to some
extent D..]])


depends_on("libiconv/1.16-ufp4dxx")

prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/doxygen-1.9.0-pyi5dyismnlpgcf5tehkmswwgxbtcclo/bin", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/doxygen-1.9.0-pyi5dyismnlpgcf5tehkmswwgxbtcclo/.", ":")

