-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2021-01-13 17:09:37.986171
--
-- expat@2.2.10%gcc@6.4.0+libbsd arch=linux-centos7-broadwell/ibk667v
--

whatis([[Name : expat]])
whatis([[Version : 2.2.10]])
whatis([[Target : broadwell]])
whatis([[Short description : Expat is an XML parser library written in C.]])
whatis([[Configure options : --without-docbook --enable-static --with-libbsd]])

help([[Expat is an XML parser library written in C.]])



prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/expat-2.2.10-ibk667vyy4bqx2vthuj4wgefuadbttjm/lib", ":")
prepend_path("LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/expat-2.2.10-ibk667vyy4bqx2vthuj4wgefuadbttjm/lib", ":")
prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/expat-2.2.10-ibk667vyy4bqx2vthuj4wgefuadbttjm/bin", ":")
prepend_path("C_INCLUDE_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/expat-2.2.10-ibk667vyy4bqx2vthuj4wgefuadbttjm/include", ":")
prepend_path("CPLUS_INCLUDE_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/expat-2.2.10-ibk667vyy4bqx2vthuj4wgefuadbttjm/include", ":")
prepend_path("INCLUDE", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/expat-2.2.10-ibk667vyy4bqx2vthuj4wgefuadbttjm/include", ":")
prepend_path("PKG_CONFIG_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/expat-2.2.10-ibk667vyy4bqx2vthuj4wgefuadbttjm/lib/pkgconfig", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/expat-2.2.10-ibk667vyy4bqx2vthuj4wgefuadbttjm/", ":")

