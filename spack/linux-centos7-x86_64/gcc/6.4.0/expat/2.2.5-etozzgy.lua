-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2018-09-18 17:04:40.101433
--
-- expat@2.2.5%gcc@6.4.0+libbsd arch=linux-centos7-x86_64 /etozzgy
--

whatis([[Name : expat]])
whatis([[Version : 2.2.5]])
whatis([[Short description : Expat is an XML parser library written in C.]])
whatis([[Configure options : --with-libbsd]])

help([[Expat is an XML parser library written in C.]])



prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/expat-2.2.5-etozzgyvfjgsfm756gbcaonunhv4hqma/bin", ":")
prepend_path("MANPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/expat-2.2.5-etozzgyvfjgsfm756gbcaonunhv4hqma/share/man", ":")
prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/expat-2.2.5-etozzgyvfjgsfm756gbcaonunhv4hqma/lib", ":")
prepend_path("LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/expat-2.2.5-etozzgyvfjgsfm756gbcaonunhv4hqma/lib", ":")
prepend_path("CPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/expat-2.2.5-etozzgyvfjgsfm756gbcaonunhv4hqma/include", ":")
prepend_path("PKG_CONFIG_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/expat-2.2.5-etozzgyvfjgsfm756gbcaonunhv4hqma/lib/pkgconfig", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/expat-2.2.5-etozzgyvfjgsfm756gbcaonunhv4hqma/", ":")

