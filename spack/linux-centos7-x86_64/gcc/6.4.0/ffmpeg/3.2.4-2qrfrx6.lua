-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2019-02-13 20:52:52.728077
--
-- ffmpeg@3.2.4%gcc@6.4.0+shared arch=linux-centos7-x86_64 /2qrfrx6
--

whatis([[Name : ffmpeg]])
whatis([[Version : 3.2.4]])
whatis([[Short description : FFmpeg is a complete, cross-platform solution to record, convert and stream audio and video.]])
whatis([[Configure options : --enable-pic --enable-shared]])

help([[FFmpeg is a complete, cross-platform solution to record, convert and
stream audio and video.]])



prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/ffmpeg-3.2.4-2qrfrx6dosmrnpxzoevfhjqdxkfuyti3/bin", ":")
prepend_path("MANPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/ffmpeg-3.2.4-2qrfrx6dosmrnpxzoevfhjqdxkfuyti3/share/man", ":")
prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/ffmpeg-3.2.4-2qrfrx6dosmrnpxzoevfhjqdxkfuyti3/lib", ":")
prepend_path("LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/ffmpeg-3.2.4-2qrfrx6dosmrnpxzoevfhjqdxkfuyti3/lib", ":")
prepend_path("CPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/ffmpeg-3.2.4-2qrfrx6dosmrnpxzoevfhjqdxkfuyti3/include", ":")
prepend_path("PKG_CONFIG_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/ffmpeg-3.2.4-2qrfrx6dosmrnpxzoevfhjqdxkfuyti3/lib/pkgconfig", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/ffmpeg-3.2.4-2qrfrx6dosmrnpxzoevfhjqdxkfuyti3/", ":")

