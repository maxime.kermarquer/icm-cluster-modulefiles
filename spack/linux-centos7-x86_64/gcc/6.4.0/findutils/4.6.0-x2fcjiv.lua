-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2021-11-30 16:29:30.786750
--
-- findutils@4.6.0%gcc@6.4.0 patches=84b916c0bf8c51b7e7b28417692f0ad3e7030d1f3c248ba77c42ede5c1c5d11e,bd9e4e5cc280f9753ae14956c4e4aa17fe7a210f55dd6c84aa60b12d106d47a2 arch=linux-centos7-broadwell/x2fcjiv
--

whatis([[Name : findutils]])
whatis([[Version : 4.6.0]])
whatis([[Target : broadwell]])
whatis([[Short description : The GNU Find Utilities are the basic directory searching utilities of the GNU operating system.]])

help([[The GNU Find Utilities are the basic directory searching utilities of
the GNU operating system.]])



prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/findutils-4.6.0-x2fcjivsicn2qvbcwstiebgeze2zuepk/bin", ":")
prepend_path("MANPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/findutils-4.6.0-x2fcjivsicn2qvbcwstiebgeze2zuepk/share/man", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/findutils-4.6.0-x2fcjivsicn2qvbcwstiebgeze2zuepk/", ":")

