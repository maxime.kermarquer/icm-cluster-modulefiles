-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2018-09-19 11:08:49.744295
--
-- fixesproto@5.0%gcc@6.4.0 arch=linux-centos7-x86_64 /q4zbunc
--

whatis([[Name : fixesproto]])
whatis([[Version : 5.0]])
whatis([[Short description : X Fixes Extension.]])

help([[X Fixes Extension. The extension makes changes to many areas of the
protocol to resolve issues raised by application interaction with core
protocol mechanisms that cannot be adequately worked around on the
client side of the wire.]])



prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/fixesproto-5.0-q4zbunclf5suvsuwfs5gbdmjpydiijfs/lib", ":")
prepend_path("LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/fixesproto-5.0-q4zbunclf5suvsuwfs5gbdmjpydiijfs/lib", ":")
prepend_path("CPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/fixesproto-5.0-q4zbunclf5suvsuwfs5gbdmjpydiijfs/include", ":")
prepend_path("PKG_CONFIG_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/fixesproto-5.0-q4zbunclf5suvsuwfs5gbdmjpydiijfs/lib/pkgconfig", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/fixesproto-5.0-q4zbunclf5suvsuwfs5gbdmjpydiijfs/", ":")

