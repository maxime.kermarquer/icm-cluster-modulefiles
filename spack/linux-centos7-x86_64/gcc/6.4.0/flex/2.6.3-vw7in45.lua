-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2018-09-19 11:03:49.769755
--
-- flex@2.6.3%gcc@6.4.0+lex arch=linux-centos7-x86_64 /vw7in45
--

whatis([[Name : flex]])
whatis([[Version : 2.6.3]])
whatis([[Short description : Flex is a tool for generating scanners.]])

help([[Flex is a tool for generating scanners.]])



prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/flex-2.6.3-vw7in45hacpfvcuwdnfa7jq224eli3vn/bin", ":")
prepend_path("MANPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/flex-2.6.3-vw7in45hacpfvcuwdnfa7jq224eli3vn/share/man", ":")
prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/flex-2.6.3-vw7in45hacpfvcuwdnfa7jq224eli3vn/lib", ":")
prepend_path("LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/flex-2.6.3-vw7in45hacpfvcuwdnfa7jq224eli3vn/lib", ":")
prepend_path("CPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/flex-2.6.3-vw7in45hacpfvcuwdnfa7jq224eli3vn/include", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/flex-2.6.3-vw7in45hacpfvcuwdnfa7jq224eli3vn/", ":")

