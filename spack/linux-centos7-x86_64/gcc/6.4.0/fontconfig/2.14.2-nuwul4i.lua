-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2023-05-09 16:11:17.819066
--
-- fontconfig@2.14.2%gcc@6.4.0 build_system=autotools arch=linux-centos7-broadwell/nuwul4i
--

whatis([[Name : fontconfig]])
whatis([[Version : 2.14.2]])
whatis([[Target : broadwell]])
whatis([[Short description : Fontconfig is a library for configuring/customizing font access]])
whatis([[Configure options : --enable-libxml2 --disable-docs --with-default-fonts=/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/font-util-1.3.1-5rq6eqjj5g6jxatp4vnpbzmw4od6qzs5/share/fonts]])

help([[Name   : fontconfig]])
help([[Version: 2.14.2]])
help([[Target : broadwell]])
help()
help([[Fontconfig is a library for configuring/customizing font access]])


depends_on("linux-centos7-x86_64/gcc/6.4.0/font-util/1.3.1-5rq6eqj")
depends_on("linux-centos7-x86_64/gcc/6.4.0/freetype/2.11.1-e77ncz4")
depends_on("linux-centos7-x86_64/gcc/6.4.0/libxml2/2.9.10-hax3e7o")
depends_on("linux-centos7-x86_64/gcc/6.4.0/util-linux-uuid/2.36-nlh67vl")

prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/fontconfig-2.14.2-nuwul4izqv66beu3s3ffam7w7e53fc7d/bin", ":")
prepend_path("PKG_CONFIG_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/fontconfig-2.14.2-nuwul4izqv66beu3s3ffam7w7e53fc7d/lib/pkgconfig", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/fontconfig-2.14.2-nuwul4izqv66beu3s3ffam7w7e53fc7d/.", ":")

