-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2020-07-01 14:25:53.300339
--
-- freetype@2.10.2%gcc@6.4.0 arch=linux-centos7-x86_64 /twph56r
--

whatis([[Name : freetype]])
whatis([[Version : 2.10.2]])
whatis([[Short description : FreeType is a freely available software library to render fonts. It is written in C, designed to be small, efficient, highly customizable, and portable while capable of producing high-quality output (glyph images) of most vector and bitmap font formats.]])
whatis([[Configure options : --with-harfbuzz=no]])

help([[FreeType is a freely available software library to render fonts. It is
written in C, designed to be small, efficient, highly customizable, and
portable while capable of producing high-quality output (glyph images)
of most vector and bitmap font formats.]])



prepend_path("ACLOCAL_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/freetype-2.10.2-twph56rm2q2dh2sgnzj6nqbr4xoh3id2/share/aclocal", ":")
prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/freetype-2.10.2-twph56rm2q2dh2sgnzj6nqbr4xoh3id2/lib", ":")
prepend_path("LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/freetype-2.10.2-twph56rm2q2dh2sgnzj6nqbr4xoh3id2/lib", ":")
prepend_path("CPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/freetype-2.10.2-twph56rm2q2dh2sgnzj6nqbr4xoh3id2/include", ":")
prepend_path("PKG_CONFIG_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/freetype-2.10.2-twph56rm2q2dh2sgnzj6nqbr4xoh3id2/lib/pkgconfig", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/freetype-2.10.2-twph56rm2q2dh2sgnzj6nqbr4xoh3id2/", ":")

