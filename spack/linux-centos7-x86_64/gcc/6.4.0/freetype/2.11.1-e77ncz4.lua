-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2023-05-09 16:10:40.617733
--
-- freetype@2.11.1%gcc@6.4.0 build_system=autotools arch=linux-centos7-broadwell/e77ncz4
--

whatis([[Name : freetype]])
whatis([[Version : 2.11.1]])
whatis([[Target : broadwell]])
whatis([[Short description : FreeType is a freely available software library to render fonts. It is written in C, designed to be small, efficient, highly customizable, and portable while capable of producing high-quality output (glyph images) of most vector and bitmap font formats.]])

help([[Name   : freetype]])
help([[Version: 2.11.1]])
help([[Target : broadwell]])
help()
help([[FreeType is a freely available software library to render fonts. It is
written in C, designed to be small, efficient, highly customizable, and
portable while capable of producing high-quality output (glyph images)
of most vector and bitmap font formats.]])


depends_on("linux-centos7-x86_64/gcc/6.4.0/bzip2/1.0.8-hoiufcd")
depends_on("linux-centos7-x86_64/gcc/6.4.0/libpng/1.6.37-qxiycq3")

prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/freetype-2.11.1-e77ncz47vvbcvfcz3graphfqcwwswsrc/bin", ":")
prepend_path("CPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/freetype-2.11.1-e77ncz47vvbcvfcz3graphfqcwwswsrc/include", ":")
prepend_path("LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/freetype-2.11.1-e77ncz47vvbcvfcz3graphfqcwwswsrc/lib", ":")
prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/freetype-2.11.1-e77ncz47vvbcvfcz3graphfqcwwswsrc/lib", ":")
prepend_path("MANPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/freetype-2.11.1-e77ncz47vvbcvfcz3graphfqcwwswsrc/share/man", ":")
prepend_path("ACLOCAL_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/freetype-2.11.1-e77ncz47vvbcvfcz3graphfqcwwswsrc/share/aclocal", ":")
prepend_path("PKG_CONFIG_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/freetype-2.11.1-e77ncz47vvbcvfcz3graphfqcwwswsrc/lib/pkgconfig", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/freetype-2.11.1-e77ncz47vvbcvfcz3graphfqcwwswsrc/.", ":")

