-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2023-05-09 16:11:32.195531
--
-- fribidi@1.0.12%gcc@6.4.0 build_system=autotools arch=linux-centos7-broadwell/5fp7pkr
--

whatis([[Name : fribidi]])
whatis([[Version : 1.0.12]])
whatis([[Target : broadwell]])
whatis([[Short description : GNU FriBidi: The Free Implementation of the Unicode Bidirectional Algorithm.]])

help([[Name   : fribidi]])
help([[Version: 1.0.12]])
help([[Target : broadwell]])
help()
help([[GNU FriBidi: The Free Implementation of the Unicode Bidirectional
Algorithm.]])



prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/fribidi-1.0.12-5fp7pkrskocn6kucfa6x53n5xac77vhv/bin", ":")
prepend_path("MANPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/fribidi-1.0.12-5fp7pkrskocn6kucfa6x53n5xac77vhv/share/man", ":")
prepend_path("PKG_CONFIG_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/fribidi-1.0.12-5fp7pkrskocn6kucfa6x53n5xac77vhv/lib/pkgconfig", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/fribidi-1.0.12-5fp7pkrskocn6kucfa6x53n5xac77vhv/.", ":")

