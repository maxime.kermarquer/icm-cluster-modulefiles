-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2021-01-13 17:57:03.172428
--
-- gawk@5.0.1%gcc@6.4.0 arch=linux-centos7-broadwell/fex2hdi
--

whatis([[Name : gawk]])
whatis([[Version : 5.0.1]])
whatis([[Target : broadwell]])
whatis([[Short description : If you are like many computer users, you would frequently like to make changes in various text files wherever certain patterns appear, or extract data from parts of certain lines while discarding the rest. To write a program to do this in a language such as C or Pascal is a time-consuming inconvenience that may take many lines of code. The job is easy with awk, especially the GNU implementation: gawk.]])

help([[If you are like many computer users, you would frequently like to make
changes in various text files wherever certain patterns appear, or
extract data from parts of certain lines while discarding the rest. To
write a program to do this in a language such as C or Pascal is a time-
consuming inconvenience that may take many lines of code. The job is
easy with awk, especially the GNU implementation: gawk. The awk utility
interprets a special-purpose programming language that makes it possible
to handle simple data-reformatting jobs with just a few lines of code.]])



prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/gawk-5.0.1-fex2hdid7iybsthkyfcqtif6wfubah2o/lib", ":")
prepend_path("LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/gawk-5.0.1-fex2hdid7iybsthkyfcqtif6wfubah2o/lib", ":")
prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/gawk-5.0.1-fex2hdid7iybsthkyfcqtif6wfubah2o/bin", ":")
prepend_path("MANPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/gawk-5.0.1-fex2hdid7iybsthkyfcqtif6wfubah2o/share/man", ":")
prepend_path("C_INCLUDE_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/gawk-5.0.1-fex2hdid7iybsthkyfcqtif6wfubah2o/include", ":")
prepend_path("CPLUS_INCLUDE_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/gawk-5.0.1-fex2hdid7iybsthkyfcqtif6wfubah2o/include", ":")
prepend_path("INCLUDE", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/gawk-5.0.1-fex2hdid7iybsthkyfcqtif6wfubah2o/include", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/gawk-5.0.1-fex2hdid7iybsthkyfcqtif6wfubah2o/", ":")

