-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2023-05-04 12:03:15.263421
--
-- gawk@5.2.1%gcc@6.4.0~nls build_system=autotools arch=linux-centos7-broadwell/tftq34l
--

whatis([[Name : gawk]])
whatis([[Version : 5.2.1]])
whatis([[Target : broadwell]])
whatis([[Short description : If you are like many computer users, you would frequently like to make changes in various text files wherever certain patterns appear, or extract data from parts of certain lines while discarding the rest. To write a program to do this in a language such as C or Pascal is a time-consuming inconvenience that may take many lines of code. The job is easy with awk, especially the GNU implementation: gawk.]])
whatis([[Configure options : --disable-nls]])

help([[Name   : gawk]])
help([[Version: 5.2.1]])
help([[Target : broadwell]])
help()
help([[If you are like many computer users, you would frequently like to make
changes in various text files wherever certain patterns appear, or
extract data from parts of certain lines while discarding the rest. To
write a program to do this in a language such as C or Pascal is a time-
consuming inconvenience that may take many lines of code. The job is
easy with awk, especially the GNU implementation: gawk. The awk utility
interprets a special-purpose programming language that makes it possible
to handle simple data-reformatting jobs with just a few lines of code.]])


depends_on("gmp/6.1.2-twibrqn")
depends_on("libsigsegv/2.12-7m3tio7")
depends_on("mpfr/3.1.6-qmjno7t")
depends_on("readline/8.0-i5obceg")

prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/gawk-5.2.1-tftq34llbtstwel2ug344pyzxqoc3e7n/bin", ":")
prepend_path("MANPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/gawk-5.2.1-tftq34llbtstwel2ug344pyzxqoc3e7n/share/man", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/gawk-5.2.1-tftq34llbtstwel2ug344pyzxqoc3e7n/.", ":")

