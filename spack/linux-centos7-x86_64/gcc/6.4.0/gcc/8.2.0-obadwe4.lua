-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2023-05-04 13:20:55.064363
--
-- gcc@8.2.0%gcc@6.4.0~binutils+bootstrap~graphite~nvptx~piclibs~profiled~strip build_system=autotools build_type=RelWithDebInfo languages=c,c++,fortran patches=49341f7,98a9c96,d4919d6,dc1ca24 arch=linux-centos7-broadwell/obadwe4
--

whatis([[Name : gcc]])
whatis([[Version : 8.2.0]])
whatis([[Target : broadwell]])
whatis([[Short description : The GNU Compiler Collection includes front ends for C, C++, Objective-C, Fortran, Ada, and Go, as well as libraries for these languages.]])
whatis([[Configure options : --with-pkgversion=Spack GCC --with-bugurl=https://github.com/spack/spack/issues --disable-multilib --enable-languages=c,c++,fortran --disable-nls --disable-canonical-system-headers --with-system-zlib --enable-bootstrap --with-mpfr-include=/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/mpfr-3.1.6-qmjno7t5g4kmdenkxyoohtw6uma3ptze/include --with-mpfr-lib=/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/mpfr-3.1.6-qmjno7t5g4kmdenkxyoohtw6uma3ptze/lib --with-gmp-include=/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/gmp-6.1.2-twibrqnq6p2jn54n2npl2ebv5p7aetro/include --with-gmp-lib=/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/gmp-6.1.2-twibrqnq6p2jn54n2npl2ebv5p7aetro/lib --with-mpc-include=/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/mpc-1.1.0-wslgvbqvayynzlxha73cemtfibqhkwu5/include --with-mpc-lib=/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/mpc-1.1.0-wslgvbqvayynzlxha73cemtfibqhkwu5/lib --without-isl --with-stage1-ldflags=-Wl,-rpath,/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/gcc-8.2.0-obadwe4g7jlpeg7r4ulxqy3xxy2vef4d/lib -Wl,-rpath,/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/gcc-8.2.0-obadwe4g7jlpeg7r4ulxqy3xxy2vef4d/lib64 -Wl,-rpath,/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/gmp-6.1.2-twibrqnq6p2jn54n2npl2ebv5p7aetro/lib -Wl,-rpath,/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/mpc-1.1.0-wslgvbqvayynzlxha73cemtfibqhkwu5/lib -Wl,-rpath,/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/mpfr-3.1.6-qmjno7t5g4kmdenkxyoohtw6uma3ptze/lib -Wl,-rpath,/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/zlib-1.2.11-wp5daspjexwgd5rft64ekrbjk4uarol2/lib --with-boot-ldflags=-Wl,-rpath,/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/gcc-8.2.0-obadwe4g7jlpeg7r4ulxqy3xxy2vef4d/lib -Wl,-rpath,/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/gcc-8.2.0-obadwe4g7jlpeg7r4ulxqy3xxy2vef4d/lib64 -Wl,-rpath,/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/gmp-6.1.2-twibrqnq6p2jn54n2npl2ebv5p7aetro/lib -Wl,-rpath,/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/mpc-1.1.0-wslgvbqvayynzlxha73cemtfibqhkwu5/lib -Wl,-rpath,/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/mpfr-3.1.6-qmjno7t5g4kmdenkxyoohtw6uma3ptze/lib -Wl,-rpath,/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/zlib-1.2.11-wp5daspjexwgd5rft64ekrbjk4uarol2/lib -static-libstdc++ -static-libgcc --with-build-config=spack]])

help([[Name   : gcc]])
help([[Version: 8.2.0]])
help([[Target : broadwell]])
help()
help([[The GNU Compiler Collection includes front ends for C, C++, Objective-C,
Fortran, Ada, and Go, as well as libraries for these languages.]])

-- Services provided by the package
family("compiler")

-- Loading this module unlocks the path below unconditionally
prepend_path("MODULEPATH", "/network/lustre/iss01/apps/modules/spack/linux-centos7-x86_64/gcc/8.2.0")


depends_on("gmp/6.1.2-twibrqn")
depends_on("mpc/1.1.0-wslgvbq")
depends_on("mpfr/3.1.6-qmjno7t")
depends_on("zlib/1.2.11-wp5dasp")

prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/gcc-8.2.0-obadwe4g7jlpeg7r4ulxqy3xxy2vef4d/bin", ":")
prepend_path("MANPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/gcc-8.2.0-obadwe4g7jlpeg7r4ulxqy3xxy2vef4d/share/man", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/gcc-8.2.0-obadwe4g7jlpeg7r4ulxqy3xxy2vef4d/.", ":")
setenv("CC", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/gcc-8.2.0-obadwe4g7jlpeg7r4ulxqy3xxy2vef4d/bin/gcc")
setenv("CXX", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/gcc-8.2.0-obadwe4g7jlpeg7r4ulxqy3xxy2vef4d/bin/g++")
setenv("FC", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/gcc-8.2.0-obadwe4g7jlpeg7r4ulxqy3xxy2vef4d/bin/gfortran")
setenv("F77", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/gcc-8.2.0-obadwe4g7jlpeg7r4ulxqy3xxy2vef4d/bin/gfortran")

