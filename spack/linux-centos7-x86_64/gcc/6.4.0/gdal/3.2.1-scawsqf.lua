-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2021-10-14 18:55:36.365111
--
-- gdal@3.2.1%gcc@6.4.0~armadillo~cfitsio~crypto~cryptopp~curl~expat~geos~gif~grib~hdf4~hdf5~jasper~java+jpeg~kea~libiconv~libkml+liblzma+libtool+libz~mdb~netcdf~odbc~opencl~openjpeg~pcre~perl~pg~png~poppler+proj~python~qhull~sosi~sqlite3~xerces~xml2~zstd arch=linux-centos7-broadwell/scawsqf
--

whatis([[Name : gdal]])
whatis([[Version : 3.2.1]])
whatis([[Target : broadwell]])
whatis([[Short description : GDAL (Geospatial Data Abstraction Library) is a translator library for raster and vector geospatial data formats that is released under an X/MIT style Open Source license by the Open Source Geospatial Foundation. As a library, it presents a single raster abstract data model and vector abstract data model to the calling application for all supported formats. It also comes with a variety of useful command line utilities for data translation and processing. ]])
whatis([[Configure options : --with-libtiff=/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/libtiff-4.1.0-n6rdcwtr4a76cfjvcln4ahdlnyojyihd --with-geotiff=/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/libgeotiff-1.6.0-vqzhghizhghyeqh2xjqrpprgwpvsrmjf --with-libjson-c=/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/json-c-0.13.1-fii7ums6rqhxlrrwmx3c3tokj7e2seje --disable-driver-bsb --disable-driver-mrf --disable-driver-grib --with-zstd=no --with-proj=/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/proj-7.1.0-ksbdx7icgjlzhqrylhbcpi3t3p5mj4wr --with-crypto=no --with-qhull=no --with-cryptopp=no --with-kea=no --with-libtool=yes --with-libz=/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/zlib-1.2.11-wp5daspjexwgd5rft64ekrbjk4uarol2 --with-libiconv-prefix=no --with-liblzma=yes --with-pg=no --with-cfitsio=no --with-png=no --with-jpeg=/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/libjpeg-turbo-2.0.6-muo6zfe37yacs7mdbonx6r37evue2k45 --with-gif=no --with-sosi=no --with-hdf4=no --with-hdf5=no --with-netcdf=no --with-jasper=no --with-openjpeg=no --with-xerces=no --with-expat=no --with-libkml=no --with-odbc=no --with-curl=no --with-xml2=no --with-sqlite3=no --with-pcre=no --with-geos=no --with-opencl=no --with-poppler=no --with-perl=no --with-python=no --with-java=no --with-mdb=no --with-armadillo=no --with-grass=no --with-libgrass=no --with-pcraster=no --with-dds=no --with-gta=no --with-pcidsk=no --with-ogdi=no --with-fme=no --with-fgdb=no --with-ecw=no --with-kakadu=no --with-mrsid=no --with-jp2mrsid=no --with-mrsid_lidar=no --with-msg=no --with-oci=no --with-mysql=no --with-ingres=no --with-dods-root=no --with-spatialite=no --with-idb=no --with-epsilon=no --with-webp=no --with-freexl=no --with-pam=no --with-podofo=no --with-rasdaman=no --with-heif=no --with-exr=no --with-rdb=no --with-tiledb=no --with-mongocxxv3=no --with-jp2lura=no --with-rasterlite2=no --with-teigha=no --with-sfcgal=no --with-mongocxx=no --with-gnm=no --with-pdfium=no]])

help([[GDAL (Geospatial Data Abstraction Library) is a translator library for
raster and vector geospatial data formats that is released under an
X/MIT style Open Source license by the Open Source Geospatial
Foundation. As a library, it presents a single raster abstract data
model and vector abstract data model to the calling application for all
supported formats. It also comes with a variety of useful command line
utilities for data translation and processing.]])



prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/gdal-3.2.1-scawsqf45r2q7bk6atos3mq6dkiyufnn/lib", ":")
prepend_path("LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/gdal-3.2.1-scawsqf45r2q7bk6atos3mq6dkiyufnn/lib", ":")
prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/gdal-3.2.1-scawsqf45r2q7bk6atos3mq6dkiyufnn/bin", ":")
prepend_path("C_INCLUDE_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/gdal-3.2.1-scawsqf45r2q7bk6atos3mq6dkiyufnn/include", ":")
prepend_path("CPLUS_INCLUDE_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/gdal-3.2.1-scawsqf45r2q7bk6atos3mq6dkiyufnn/include", ":")
prepend_path("INCLUDE", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/gdal-3.2.1-scawsqf45r2q7bk6atos3mq6dkiyufnn/include", ":")
prepend_path("PKG_CONFIG_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/gdal-3.2.1-scawsqf45r2q7bk6atos3mq6dkiyufnn/lib/pkgconfig", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/gdal-3.2.1-scawsqf45r2q7bk6atos3mq6dkiyufnn/", ":")

