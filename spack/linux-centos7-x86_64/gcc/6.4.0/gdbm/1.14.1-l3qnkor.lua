-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2018-09-18 11:31:49.584840
--
-- gdbm@1.14.1%gcc@6.4.0 arch=linux-centos7-x86_64 /l3qnkor
--

whatis([[Name : gdbm]])
whatis([[Version : 1.14.1]])
whatis([[Short description : GNU dbm (or GDBM, for short) is a library of database functions that use extensible hashing and work similar to the standard UNIX dbm. These routines are provided to a programmer needing to create and manipulate a hashed database.]])
whatis([[Configure options : --enable-libgdbm-compat CPPFLAGS=-D_GNU_SOURCE]])

help([[GNU dbm (or GDBM, for short) is a library of database functions that use
extensible hashing and work similar to the standard UNIX dbm. These
routines are provided to a programmer needing to create and manipulate a
hashed database.]])



prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/gdbm-1.14.1-l3qnkor5t5xp7jeopgfi6umkwwvgz664/bin", ":")
prepend_path("MANPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/gdbm-1.14.1-l3qnkor5t5xp7jeopgfi6umkwwvgz664/share/man", ":")
prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/gdbm-1.14.1-l3qnkor5t5xp7jeopgfi6umkwwvgz664/lib", ":")
prepend_path("LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/gdbm-1.14.1-l3qnkor5t5xp7jeopgfi6umkwwvgz664/lib", ":")
prepend_path("CPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/gdbm-1.14.1-l3qnkor5t5xp7jeopgfi6umkwwvgz664/include", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/gdbm-1.14.1-l3qnkor5t5xp7jeopgfi6umkwwvgz664/", ":")

