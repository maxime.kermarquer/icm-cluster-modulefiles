-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2021-01-13 17:14:05.866345
--
-- gdbm@1.18.1%gcc@6.4.0 arch=linux-centos7-broadwell/boj5sgi
--

whatis([[Name : gdbm]])
whatis([[Version : 1.18.1]])
whatis([[Target : broadwell]])
whatis([[Short description : GNU dbm (or GDBM, for short) is a library of database functions that use extensible hashing and work similar to the standard UNIX dbm. These routines are provided to a programmer needing to create and manipulate a hashed database.]])
whatis([[Configure options : --enable-libgdbm-compat CPPFLAGS=-D_GNU_SOURCE]])

help([[GNU dbm (or GDBM, for short) is a library of database functions that use
extensible hashing and work similar to the standard UNIX dbm. These
routines are provided to a programmer needing to create and manipulate a
hashed database.]])



prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/gdbm-1.18.1-boj5sgi36mmh7jskcuamd2zi6pf3zcpa/lib", ":")
prepend_path("LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/gdbm-1.18.1-boj5sgi36mmh7jskcuamd2zi6pf3zcpa/lib", ":")
prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/gdbm-1.18.1-boj5sgi36mmh7jskcuamd2zi6pf3zcpa/bin", ":")
prepend_path("MANPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/gdbm-1.18.1-boj5sgi36mmh7jskcuamd2zi6pf3zcpa/share/man", ":")
prepend_path("C_INCLUDE_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/gdbm-1.18.1-boj5sgi36mmh7jskcuamd2zi6pf3zcpa/include", ":")
prepend_path("CPLUS_INCLUDE_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/gdbm-1.18.1-boj5sgi36mmh7jskcuamd2zi6pf3zcpa/include", ":")
prepend_path("INCLUDE", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/gdbm-1.18.1-boj5sgi36mmh7jskcuamd2zi6pf3zcpa/include", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/gdbm-1.18.1-boj5sgi36mmh7jskcuamd2zi6pf3zcpa/", ":")

