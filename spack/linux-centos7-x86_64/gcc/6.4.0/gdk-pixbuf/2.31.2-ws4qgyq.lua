-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2019-02-06 18:54:58.293492
--
-- gdk-pixbuf@2.31.2%gcc@6.4.0 arch=linux-centos7-x86_64 /ws4qgyq
--

whatis([[Name : gdk-pixbuf]])
whatis([[Version : 2.31.2]])
whatis([[Short description : The Gdk Pixbuf is a toolkit for image loading and pixel buffer manipulation. It is used by GTK+ 2 and GTK+ 3 to load and manipulate images. In the past it was distributed as part of GTK+ 2 but it was split off into a separate package in preparation for the change to GTK+ 3.]])

help([[The Gdk Pixbuf is a toolkit for image loading and pixel buffer
manipulation. It is used by GTK+ 2 and GTK+ 3 to load and manipulate
images. In the past it was distributed as part of GTK+ 2 but it was
split off into a separate package in preparation for the change to GTK+
3.]])



prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/gdk-pixbuf-2.31.2-ws4qgyqmkb4hlgv4ps3ktdxjrkmwueit/bin", ":")
prepend_path("MANPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/gdk-pixbuf-2.31.2-ws4qgyqmkb4hlgv4ps3ktdxjrkmwueit/share/man", ":")
prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/gdk-pixbuf-2.31.2-ws4qgyqmkb4hlgv4ps3ktdxjrkmwueit/lib", ":")
prepend_path("LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/gdk-pixbuf-2.31.2-ws4qgyqmkb4hlgv4ps3ktdxjrkmwueit/lib", ":")
prepend_path("CPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/gdk-pixbuf-2.31.2-ws4qgyqmkb4hlgv4ps3ktdxjrkmwueit/include", ":")
prepend_path("PKG_CONFIG_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/gdk-pixbuf-2.31.2-ws4qgyqmkb4hlgv4ps3ktdxjrkmwueit/lib/pkgconfig", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/gdk-pixbuf-2.31.2-ws4qgyqmkb4hlgv4ps3ktdxjrkmwueit/", ":")
prepend_path("CPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/libxml2-2.9.8-m3rdnxm66wlzs5gqsm5pqlzqvideutnk/include/libxml2", ":")
prepend_path("CPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/freetype-2.7.1-fnguk6ypqcpy23j3j2n3l62sjvfd3sf2/include/freetype2", ":")

