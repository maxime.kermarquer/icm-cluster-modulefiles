-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2021-11-30 16:36:30.385874
--
-- gdk-pixbuf@2.40.0%gcc@6.4.0~x11 patches=6f6e6411051846f0e99e5f46e07c78428c4a2a88f51c202b3122972d91452f4c arch=linux-centos7-broadwell/6cbgcfr
--

whatis([[Name : gdk-pixbuf]])
whatis([[Version : 2.40.0]])
whatis([[Target : broadwell]])
whatis([[Short description : The Gdk Pixbuf is a toolkit for image loading and pixel buffer manipulation. It is used by GTK+ 2 and GTK+ 3 to load and manipulate images. In the past it was distributed as part of GTK+ 2 but it was split off into a separate package in preparation for the change to GTK+ 3.]])
whatis([[Configure options : --disable-gtk-doc-html GTKDOC_CHECK=/bin/true GTKDOC_CHECK_PATH=/bin/true GTKDOC_MKPDF=/bin/true GTKDOC_REBASE=/bin/true]])

help([[The Gdk Pixbuf is a toolkit for image loading and pixel buffer
manipulation. It is used by GTK+ 2 and GTK+ 3 to load and manipulate
images. In the past it was distributed as part of GTK+ 2 but it was
split off into a separate package in preparation for the change to GTK+
3.]])



prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/gdk-pixbuf-2.40.0-6cbgcfrqaq2xe4gxfcddx4u4haldscld/lib", ":")
prepend_path("LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/gdk-pixbuf-2.40.0-6cbgcfrqaq2xe4gxfcddx4u4haldscld/lib", ":")
prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/gdk-pixbuf-2.40.0-6cbgcfrqaq2xe4gxfcddx4u4haldscld/bin", ":")
prepend_path("MANPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/gdk-pixbuf-2.40.0-6cbgcfrqaq2xe4gxfcddx4u4haldscld/share/man", ":")
prepend_path("C_INCLUDE_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/gdk-pixbuf-2.40.0-6cbgcfrqaq2xe4gxfcddx4u4haldscld/include", ":")
prepend_path("CPLUS_INCLUDE_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/gdk-pixbuf-2.40.0-6cbgcfrqaq2xe4gxfcddx4u4haldscld/include", ":")
prepend_path("INCLUDE", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/gdk-pixbuf-2.40.0-6cbgcfrqaq2xe4gxfcddx4u4haldscld/include", ":")
prepend_path("PKG_CONFIG_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/gdk-pixbuf-2.40.0-6cbgcfrqaq2xe4gxfcddx4u4haldscld/lib/pkgconfig", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/gdk-pixbuf-2.40.0-6cbgcfrqaq2xe4gxfcddx4u4haldscld/", ":")
prepend_path("PYTHONPATH", "", ":")
prepend_path("XDG_DATA_DIRS", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/gobject-introspection-1.56.1-ecxzmbozibkmxuomxizc2msi55zzaq7p/share", ":")
prepend_path("GI_TYPELIB_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/gobject-introspection-1.56.1-ecxzmbozibkmxuomxizc2msi55zzaq7p/lib/girepository-1.0", ":")
prepend_path("GI_TYPELIB_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/gdk-pixbuf-2.40.0-6cbgcfrqaq2xe4gxfcddx4u4haldscld/lib/girepository-1.0", ":")

