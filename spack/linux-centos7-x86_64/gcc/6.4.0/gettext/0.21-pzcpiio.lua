-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2021-01-13 17:22:36.893594
--
-- gettext@0.21%gcc@6.4.0+bzip2+curses+git~libunistring+libxml2+tar+xz arch=linux-centos7-broadwell/pzcpiio
--

whatis([[Name : gettext]])
whatis([[Version : 0.21]])
whatis([[Target : broadwell]])
whatis([[Short description : GNU internationalization (i18n) and localization (l10n) library.]])
whatis([[Configure options : --disable-java --disable-csharp --with-libiconv-prefix=/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/libiconv-1.16-ufp4dxx2bc4vtwju5qtgznbuy2zpzzbc --with-included-glib --with-included-gettext --with-included-libcroco --without-emacs --with-lispdir=/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/gettext-0.21-pzcpiio4lazjkflwc4qckzpv7p4r5tx5/share/emacs/site-lisp/gettext --without-cvs --with-ncurses-prefix=/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/ncurses-6.2-qc3gtqzwi6fytsfj75gykwnl7iexhxxv CPPFLAGS=-I/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/libxml2-2.9.10-hax3e7opqsrnrfex7yyioo7rl4jpvcyb/include LDFLAGS=-L/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/libxml2-2.9.10-hax3e7opqsrnrfex7yyioo7rl4jpvcyb/lib -Wl,-rpath,/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/libxml2-2.9.10-hax3e7opqsrnrfex7yyioo7rl4jpvcyb/lib --with-included-libunistring]])

help([[GNU internationalization (i18n) and localization (l10n) library.]])



prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/gettext-0.21-pzcpiio4lazjkflwc4qckzpv7p4r5tx5/lib", ":")
prepend_path("LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/gettext-0.21-pzcpiio4lazjkflwc4qckzpv7p4r5tx5/lib", ":")
prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/gettext-0.21-pzcpiio4lazjkflwc4qckzpv7p4r5tx5/bin", ":")
prepend_path("MANPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/gettext-0.21-pzcpiio4lazjkflwc4qckzpv7p4r5tx5/share/man", ":")
prepend_path("ACLOCAL_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/gettext-0.21-pzcpiio4lazjkflwc4qckzpv7p4r5tx5/share/aclocal", ":")
prepend_path("C_INCLUDE_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/gettext-0.21-pzcpiio4lazjkflwc4qckzpv7p4r5tx5/include", ":")
prepend_path("CPLUS_INCLUDE_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/gettext-0.21-pzcpiio4lazjkflwc4qckzpv7p4r5tx5/include", ":")
prepend_path("INCLUDE", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/gettext-0.21-pzcpiio4lazjkflwc4qckzpv7p4r5tx5/include", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/gettext-0.21-pzcpiio4lazjkflwc4qckzpv7p4r5tx5/", ":")

