-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2018-09-18 17:06:16.225516
--
-- git@2.18.0%gcc@6.4.0~tcltk arch=linux-centos7-x86_64 /hxsmyen
--

whatis([[Name : git]])
whatis([[Version : 2.18.0]])
whatis([[Short description : Git is a free and open source distributed version control system designed to handle everything from small to very large projects with speed and efficiency. ]])
whatis([[Configure options : --with-curl=/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/curl-7.60.0-nqidbsk4n4he3jls4s7y3i4befhyaduc --with-expat=/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/expat-2.2.5-etozzgyvfjgsfm756gbcaonunhv4hqma --with-iconv=/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/libiconv-1.15-hbsac4ys2y3uz6btgbnglxiuqkxkosxf --with-libpcre=/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/pcre-8.42-iyitiwcrxmrd7zhxpxt5mzgr5o2inwok --with-openssl=/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/openssl-1.0.2o-qxfzezxhp4blvxkj6vsnj43vpiqfjs3x --with-perl=/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/perl-5.26.2-lc5lzoi2hqwuhitz677qx6s2wazzcrc5/bin/perl --with-zlib=/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/zlib-1.2.11-ufwqfd4zrwuuu7fmguamaqokn5plxyns --without-tcltk]])

help([[Git is a free and open source distributed version control system
designed to handle everything from small to very large projects with
speed and efficiency.]])



prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/git-2.18.0-hxsmyenhdz2ciubes6nkcfgqv66h3otq/bin", ":")
prepend_path("MANPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/git-2.18.0-hxsmyenhdz2ciubes6nkcfgqv66h3otq/share/man", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/git-2.18.0-hxsmyenhdz2ciubes6nkcfgqv66h3otq/", ":")
prepend_path("CPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/libxml2-2.9.8-m3rdnxm66wlzs5gqsm5pqlzqvideutnk/include/libxml2", ":")

