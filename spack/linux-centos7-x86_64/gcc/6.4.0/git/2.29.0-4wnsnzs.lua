-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2021-01-13 17:48:16.981775
--
-- git@2.29.0%gcc@6.4.0~tcltk arch=linux-centos7-broadwell/4wnsnzs
--

whatis([[Name : git]])
whatis([[Version : 2.29.0]])
whatis([[Target : broadwell]])
whatis([[Short description : Git is a free and open source distributed version control system designed to handle everything from small to very large projects with speed and efficiency. ]])
whatis([[Configure options : --with-curl=/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/curl-7.74.0-uspov5cgjhwhmwedyzn66britqnfe6ce --with-expat=/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/expat-2.2.10-ibk667vyy4bqx2vthuj4wgefuadbttjm --with-iconv=/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/libiconv-1.16-ufp4dxx2bc4vtwju5qtgznbuy2zpzzbc --with-openssl=/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/openssl-1.1.1i-kphel3rter2r2c5g4trtfsrzvjajvy5k --with-perl=/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/perl-5.32.0-cnlqxeflbhegnsxbwlallv5k3n6nwjoy/bin/perl --with-zlib=/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/zlib-1.2.11-wp5daspjexwgd5rft64ekrbjk4uarol2 --with-libpcre2=/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/pcre2-10.35-xeq74dy5d2ed6ax4zgzzfmwpjka2v2e3 --without-tcltk]])

help([[Git is a free and open source distributed version control system
designed to handle everything from small to very large projects with
speed and efficiency.]])



prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/git-2.29.0-4wnsnzsx73amnxlk3bij47fgi7iyj6xk/bin", ":")
prepend_path("MANPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/git-2.29.0-4wnsnzsx73amnxlk3bij47fgi7iyj6xk/share/man", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/git-2.29.0-4wnsnzsx73amnxlk3bij47fgi7iyj6xk/", ":")
depends_on("tig")
