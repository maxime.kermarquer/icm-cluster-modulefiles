-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2021-01-13 17:55:12.884882
--
-- glib@2.66.2%gcc@6.4.0~libmount patches=b3fd45063a19c871048aa1f28692293ab8971a871bdcbe65f06f17fdd79db9e2 tracing=none arch=linux-centos7-broadwell/fbdjtsd
--

whatis([[Name : glib]])
whatis([[Version : 2.66.2]])
whatis([[Target : broadwell]])
whatis([[Short description : GLib provides the core application building blocks for libraries and applications written in C.]])
whatis([[Configure options : --disable-libmount --with-python=python3.8 --with-libiconv=gnu --disable-dtrace --disable-systemtap --disable-selinux --disable-gtk-doc-html GTKDOC_CHECK=/bin/true GTKDOC_CHECK_PATH=/bin/true GTKDOC_MKPDF=/bin/true GTKDOC_REBASE=/bin/true]])

help([[GLib provides the core application building blocks for libraries and
applications written in C. The GLib package contains a low-level
libraries useful for providing data structure handling for C,
portability wrappers and interfaces for such runtime functionality as an
event loop, threads, dynamic loading and an object system.]])



prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/glib-2.66.2-fbdjtsd343km2ycm3maozbmpcretb434/lib", ":")
prepend_path("LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/glib-2.66.2-fbdjtsd343km2ycm3maozbmpcretb434/lib", ":")
prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/glib-2.66.2-fbdjtsd343km2ycm3maozbmpcretb434/bin", ":")
prepend_path("ACLOCAL_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/glib-2.66.2-fbdjtsd343km2ycm3maozbmpcretb434/share/aclocal", ":")
prepend_path("C_INCLUDE_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/glib-2.66.2-fbdjtsd343km2ycm3maozbmpcretb434/include", ":")
prepend_path("CPLUS_INCLUDE_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/glib-2.66.2-fbdjtsd343km2ycm3maozbmpcretb434/include", ":")
prepend_path("INCLUDE", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/glib-2.66.2-fbdjtsd343km2ycm3maozbmpcretb434/include", ":")
prepend_path("PKG_CONFIG_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/glib-2.66.2-fbdjtsd343km2ycm3maozbmpcretb434/lib/pkgconfig", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/glib-2.66.2-fbdjtsd343km2ycm3maozbmpcretb434/", ":")
prepend_path("PYTHONPATH", "", ":")

