-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2022-03-31 10:42:25.497215
--
-- glpk@4.65%gcc@6.4.0~gmp arch=linux-centos7-broadwell/gzd5wxf
--

whatis([[Name : glpk]])
whatis([[Version : 4.65]])
whatis([[Target : broadwell]])
whatis([[Short description : The GLPK (GNU Linear Programming Kit) package is intended for solving large-scale linear programming (LP), mixed integer programming (MIP), and other related problems. It is a set of routines written in ANSI C and organized in the form of a callable library. ]])

help([[The GLPK (GNU Linear Programming Kit) package is intended for solving
large-scale linear programming (LP), mixed integer programming (MIP),
and other related problems. It is a set of routines written in ANSI C
and organized in the form of a callable library.]])



prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/glpk-4.65-gzd5wxfyn2oh3qcvvj4j7in4c2dxvnm4/lib", ":")
prepend_path("LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/glpk-4.65-gzd5wxfyn2oh3qcvvj4j7in4c2dxvnm4/lib", ":")
prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/glpk-4.65-gzd5wxfyn2oh3qcvvj4j7in4c2dxvnm4/bin", ":")
prepend_path("C_INCLUDE_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/glpk-4.65-gzd5wxfyn2oh3qcvvj4j7in4c2dxvnm4/include", ":")
prepend_path("CPLUS_INCLUDE_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/glpk-4.65-gzd5wxfyn2oh3qcvvj4j7in4c2dxvnm4/include", ":")
prepend_path("INCLUDE", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/glpk-4.65-gzd5wxfyn2oh3qcvvj4j7in4c2dxvnm4/include", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/glpk-4.65-gzd5wxfyn2oh3qcvvj4j7in4c2dxvnm4/", ":")

