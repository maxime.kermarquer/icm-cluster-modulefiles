-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2018-09-19 11:07:53.844272
--
-- glproto@1.4.17%gcc@6.4.0 arch=linux-centos7-x86_64 /ko72prh
--

whatis([[Name : glproto]])
whatis([[Version : 1.4.17]])
whatis([[Short description : OpenGL Extension to the X Window System.]])

help([[OpenGL Extension to the X Window System. This extension defines a
protocol for the client to send 3D rendering commands to the X server.]])



prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/glproto-1.4.17-ko72prhsskoykf4x6zqo4vexto3uy4dw/lib", ":")
prepend_path("LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/glproto-1.4.17-ko72prhsskoykf4x6zqo4vexto3uy4dw/lib", ":")
prepend_path("CPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/glproto-1.4.17-ko72prhsskoykf4x6zqo4vexto3uy4dw/include", ":")
prepend_path("PKG_CONFIG_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/glproto-1.4.17-ko72prhsskoykf4x6zqo4vexto3uy4dw/lib/pkgconfig", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/glproto-1.4.17-ko72prhsskoykf4x6zqo4vexto3uy4dw/", ":")

