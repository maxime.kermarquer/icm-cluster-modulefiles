-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2021-10-14 18:45:41.048582
--
-- gmake@4.2.1%gcc@6.4.0~guile+nls patches=ca60bd9c1a1b35bc0dc58b6a4a19d5c2651f7a94a4b22b2c5ea001a1ca7a8a7f,fe5b60d091c33f169740df8cb718bf4259f84528b42435194ffe0dd5b79cd125 arch=linux-centos7-broadwell/evrhn74
--

whatis([[Name : gmake]])
whatis([[Version : 4.2.1]])
whatis([[Target : broadwell]])
whatis([[Short description : GNU Make is a tool which controls the generation of executables and other non-source files of a program from the program's source files.]])
whatis([[Configure options : --without-guile --enable-nls]])

help([[GNU Make is a tool which controls the generation of executables and
other non-source files of a program from the program's source files.]])



prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/gmake-4.2.1-evrhn747jpthb63iktwmtb3mp7mvuq44/bin", ":")
prepend_path("MANPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/gmake-4.2.1-evrhn747jpthb63iktwmtb3mp7mvuq44/share/man", ":")
prepend_path("C_INCLUDE_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/gmake-4.2.1-evrhn747jpthb63iktwmtb3mp7mvuq44/include", ":")
prepend_path("CPLUS_INCLUDE_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/gmake-4.2.1-evrhn747jpthb63iktwmtb3mp7mvuq44/include", ":")
prepend_path("INCLUDE", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/gmake-4.2.1-evrhn747jpthb63iktwmtb3mp7mvuq44/include", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/gmake-4.2.1-evrhn747jpthb63iktwmtb3mp7mvuq44/", ":")

