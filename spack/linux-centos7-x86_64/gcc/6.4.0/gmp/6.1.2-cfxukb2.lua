-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2019-03-20 16:00:49.197657
--
-- gmp@6.1.2%gcc@6.4.0 arch=linux-centos7-x86_64 /cfxukb2
--

whatis([[Name : gmp]])
whatis([[Version : 6.1.2]])
whatis([[Short description : GMP is a free library for arbitrary precision arithmetic, operating on signed integers, rational numbers, and floating-point numbers.]])
whatis([[Configure options : --enable-cxx]])

help([[GMP is a free library for arbitrary precision arithmetic, operating on
signed integers, rational numbers, and floating-point numbers.]])



prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/gmp-6.1.2-cfxukb2wvqxtsepm4nawhp2qq6rupxmi/lib", ":")
prepend_path("LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/gmp-6.1.2-cfxukb2wvqxtsepm4nawhp2qq6rupxmi/lib", ":")
prepend_path("CPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/gmp-6.1.2-cfxukb2wvqxtsepm4nawhp2qq6rupxmi/include", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/gmp-6.1.2-cfxukb2wvqxtsepm4nawhp2qq6rupxmi/", ":")

