-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2021-01-13 17:47:24.061557
--
-- gmp@6.1.2%gcc@6.4.0 arch=linux-centos7-broadwell/twibrqn
--

whatis([[Name : gmp]])
whatis([[Version : 6.1.2]])
whatis([[Target : broadwell]])
whatis([[Short description : GMP is a free library for arbitrary precision arithmetic, operating on signed integers, rational numbers, and floating-point numbers.]])
whatis([[Configure options : --enable-cxx]])

help([[GMP is a free library for arbitrary precision arithmetic, operating on
signed integers, rational numbers, and floating-point numbers.]])



prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/gmp-6.1.2-twibrqnq6p2jn54n2npl2ebv5p7aetro/lib", ":")
prepend_path("LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/gmp-6.1.2-twibrqnq6p2jn54n2npl2ebv5p7aetro/lib", ":")
prepend_path("C_INCLUDE_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/gmp-6.1.2-twibrqnq6p2jn54n2npl2ebv5p7aetro/include", ":")
prepend_path("CPLUS_INCLUDE_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/gmp-6.1.2-twibrqnq6p2jn54n2npl2ebv5p7aetro/include", ":")
prepend_path("INCLUDE", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/gmp-6.1.2-twibrqnq6p2jn54n2npl2ebv5p7aetro/include", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/gmp-6.1.2-twibrqnq6p2jn54n2npl2ebv5p7aetro/", ":")

