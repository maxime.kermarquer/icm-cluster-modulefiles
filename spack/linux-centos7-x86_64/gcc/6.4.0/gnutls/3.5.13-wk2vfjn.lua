-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2019-03-20 16:04:23.906635
--
-- gnutls@3.5.13%gcc@6.4.0+zlib arch=linux-centos7-x86_64 /wk2vfjn
--

whatis([[Name : gnutls]])
whatis([[Version : 3.5.13]])
whatis([[Short description : GnuTLS is a secure communications library implementing the SSL, TLS and DTLS protocols and technologies around them. It provides a simple C language application programming interface (API) to access the secure communications protocols as well as APIs to parse and write X.509, PKCS #12, OpenPGP and other required structures. It is aimed to be portable and efficient with focus on security and interoperability.]])
whatis([[Configure options : --enable-static --with-included-libtasn1 --with-included-unistring --without-p11-kit --with-zlib --disable-tests --disable-valgrind-tests --disable-full-test-suite]])

help([[GnuTLS is a secure communications library implementing the SSL, TLS and
DTLS protocols and technologies around them. It provides a simple C
language application programming interface (API) to access the secure
communications protocols as well as APIs to parse and write X.509, PKCS
#12, OpenPGP and other required structures. It is aimed to be portable
and efficient with focus on security and interoperability.]])



prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/gnutls-3.5.13-wk2vfjnhjoaclwexzybdyj33hc3paopt/bin", ":")
prepend_path("MANPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/gnutls-3.5.13-wk2vfjnhjoaclwexzybdyj33hc3paopt/share/man", ":")
prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/gnutls-3.5.13-wk2vfjnhjoaclwexzybdyj33hc3paopt/lib", ":")
prepend_path("LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/gnutls-3.5.13-wk2vfjnhjoaclwexzybdyj33hc3paopt/lib", ":")
prepend_path("CPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/gnutls-3.5.13-wk2vfjnhjoaclwexzybdyj33hc3paopt/include", ":")
prepend_path("PKG_CONFIG_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/gnutls-3.5.13-wk2vfjnhjoaclwexzybdyj33hc3paopt/lib/pkgconfig", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/gnutls-3.5.13-wk2vfjnhjoaclwexzybdyj33hc3paopt/", ":")
prepend_path("CPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/libxml2-2.9.8-m3rdnxm66wlzs5gqsm5pqlzqvideutnk/include/libxml2", ":")

