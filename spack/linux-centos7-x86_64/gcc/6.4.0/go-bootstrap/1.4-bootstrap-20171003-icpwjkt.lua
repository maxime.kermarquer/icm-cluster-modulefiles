-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2021-01-13 17:53:54.135642
--
-- go-bootstrap@1.4-bootstrap-20171003%gcc@6.4.0 arch=linux-centos7-broadwell/icpwjkt
--

whatis([[Name : go-bootstrap]])
whatis([[Version : 1.4-bootstrap-20171003]])
whatis([[Target : broadwell]])
whatis([[Short description : Old C-bootstrapped go to bootstrap real go]])

help([[Old C-bootstrapped go to bootstrap real go]])



prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/go-bootstrap-1.4-bootstrap-20171003-icpwjktxep5zxjpu3utk26ftzrnutmgi/lib", ":")
prepend_path("LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/go-bootstrap-1.4-bootstrap-20171003-icpwjktxep5zxjpu3utk26ftzrnutmgi/lib", ":")
prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/go-bootstrap-1.4-bootstrap-20171003-icpwjktxep5zxjpu3utk26ftzrnutmgi/bin", ":")
prepend_path("C_INCLUDE_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/go-bootstrap-1.4-bootstrap-20171003-icpwjktxep5zxjpu3utk26ftzrnutmgi/include", ":")
prepend_path("CPLUS_INCLUDE_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/go-bootstrap-1.4-bootstrap-20171003-icpwjktxep5zxjpu3utk26ftzrnutmgi/include", ":")
prepend_path("INCLUDE", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/go-bootstrap-1.4-bootstrap-20171003-icpwjktxep5zxjpu3utk26ftzrnutmgi/include", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/go-bootstrap-1.4-bootstrap-20171003-icpwjktxep5zxjpu3utk26ftzrnutmgi/", ":")

