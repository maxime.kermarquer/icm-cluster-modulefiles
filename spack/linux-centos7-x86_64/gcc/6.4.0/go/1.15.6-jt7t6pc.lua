-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2021-01-13 18:46:36.225788
--
-- go@1.15.6%gcc@6.4.0 arch=linux-centos7-broadwell/jt7t6pc
--

whatis([[Name : go]])
whatis([[Version : 1.15.6]])
whatis([[Target : broadwell]])
whatis([[Short description : The golang compiler and build environment]])

help([[The golang compiler and build environment]])



prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/go-1.15.6-jt7t6pc7wmis2tuqulhkjgz35yebbvlt/lib", ":")
prepend_path("LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/go-1.15.6-jt7t6pc7wmis2tuqulhkjgz35yebbvlt/lib", ":")
prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/go-1.15.6-jt7t6pc7wmis2tuqulhkjgz35yebbvlt/bin", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/go-1.15.6-jt7t6pc7wmis2tuqulhkjgz35yebbvlt/", ":")

