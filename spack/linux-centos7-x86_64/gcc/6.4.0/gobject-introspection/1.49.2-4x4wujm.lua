-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2019-02-06 20:18:41.495646
--
-- gobject-introspection@1.49.2%gcc@6.4.0 patches=6f90bb267efa043ed70b900b4b8e2faf9e8133afae311893b01060356ea81bba arch=linux-centos7-x86_64 /4x4wujm
--

whatis([[Name : gobject-introspection]])
whatis([[Version : 1.49.2]])
whatis([[Short description : The GObject Introspection is used to describe the program APIs and collect them in a uniform, machine readable format.Cairo is a 2D graphics library with support for multiple output]])

help([[The GObject Introspection is used to describe the program APIs and
collect them in a uniform, machine readable format.Cairo is a 2D
graphics library with support for multiple output]])



prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/gobject-introspection-1.49.2-4x4wujmeyjtho24zgwmc6yehhfgvki7w/bin", ":")
prepend_path("MANPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/gobject-introspection-1.49.2-4x4wujmeyjtho24zgwmc6yehhfgvki7w/share/man", ":")
prepend_path("ACLOCAL_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/gobject-introspection-1.49.2-4x4wujmeyjtho24zgwmc6yehhfgvki7w/share/aclocal", ":")
prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/gobject-introspection-1.49.2-4x4wujmeyjtho24zgwmc6yehhfgvki7w/lib", ":")
prepend_path("LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/gobject-introspection-1.49.2-4x4wujmeyjtho24zgwmc6yehhfgvki7w/lib", ":")
prepend_path("CPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/gobject-introspection-1.49.2-4x4wujmeyjtho24zgwmc6yehhfgvki7w/include", ":")
prepend_path("PKG_CONFIG_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/gobject-introspection-1.49.2-4x4wujmeyjtho24zgwmc6yehhfgvki7w/lib/pkgconfig", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/gobject-introspection-1.49.2-4x4wujmeyjtho24zgwmc6yehhfgvki7w/", ":")
prepend_path("CPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/libxml2-2.9.8-m3rdnxm66wlzs5gqsm5pqlzqvideutnk/include/libxml2", ":")
prepend_path("CPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/freetype-2.7.1-fnguk6ypqcpy23j3j2n3l62sjvfd3sf2/include/freetype2", ":")

