-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2021-11-30 16:34:26.218864
--
-- gobject-introspection@1.56.1%gcc@6.4.0 patches=6f90bb267efa043ed70b900b4b8e2faf9e8133afae311893b01060356ea81bba arch=linux-centos7-broadwell/ecxzmbo
--

whatis([[Name : gobject-introspection]])
whatis([[Version : 1.56.1]])
whatis([[Target : broadwell]])
whatis([[Short description : The GObject Introspection is used to describe the program APIs and collect them in a uniform, machine readable format.Cairo is a 2D graphics library with support for multiple output]])

help([[The GObject Introspection is used to describe the program APIs and
collect them in a uniform, machine readable format.Cairo is a 2D
graphics library with support for multiple output]])



prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/gobject-introspection-1.56.1-ecxzmbozibkmxuomxizc2msi55zzaq7p/lib", ":")
prepend_path("LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/gobject-introspection-1.56.1-ecxzmbozibkmxuomxizc2msi55zzaq7p/lib", ":")
prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/gobject-introspection-1.56.1-ecxzmbozibkmxuomxizc2msi55zzaq7p/bin", ":")
prepend_path("MANPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/gobject-introspection-1.56.1-ecxzmbozibkmxuomxizc2msi55zzaq7p/share/man", ":")
prepend_path("ACLOCAL_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/gobject-introspection-1.56.1-ecxzmbozibkmxuomxizc2msi55zzaq7p/share/aclocal", ":")
prepend_path("C_INCLUDE_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/gobject-introspection-1.56.1-ecxzmbozibkmxuomxizc2msi55zzaq7p/include", ":")
prepend_path("CPLUS_INCLUDE_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/gobject-introspection-1.56.1-ecxzmbozibkmxuomxizc2msi55zzaq7p/include", ":")
prepend_path("INCLUDE", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/gobject-introspection-1.56.1-ecxzmbozibkmxuomxizc2msi55zzaq7p/include", ":")
prepend_path("PKG_CONFIG_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/gobject-introspection-1.56.1-ecxzmbozibkmxuomxizc2msi55zzaq7p/lib/pkgconfig", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/gobject-introspection-1.56.1-ecxzmbozibkmxuomxizc2msi55zzaq7p/", ":")
prepend_path("PYTHONPATH", "", ":")
prepend_path("GI_TYPELIB_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/gobject-introspection-1.56.1-ecxzmbozibkmxuomxizc2msi55zzaq7p/lib/girepository-1.0", ":")

