-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2021-10-14 18:46:29.833679
--
-- googletest@1.10.0%gcc@6.4.0~gmock~ipo+pthreads+shared build_type=RelWithDebInfo arch=linux-centos7-broadwell/5jruwht
--

whatis([[Name : googletest]])
whatis([[Version : 1.10.0]])
whatis([[Target : broadwell]])
whatis([[Short description : Google test framework for C++. Also called gtest.]])
whatis([[Configure options : -DBUILD_GTEST=ON -DBUILD_GMOCK=OFF -Dgtest_disable_pthreads=OFF -DBUILD_SHARED_LIBS=ON]])

help([[Google test framework for C++. Also called gtest.]])



prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/googletest-1.10.0-5jruwhtxpa2mi2uttexyq55go5ke5arb/lib64", ":")
prepend_path("LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/googletest-1.10.0-5jruwhtxpa2mi2uttexyq55go5ke5arb/lib64", ":")
prepend_path("C_INCLUDE_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/googletest-1.10.0-5jruwhtxpa2mi2uttexyq55go5ke5arb/include", ":")
prepend_path("CPLUS_INCLUDE_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/googletest-1.10.0-5jruwhtxpa2mi2uttexyq55go5ke5arb/include", ":")
prepend_path("INCLUDE", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/googletest-1.10.0-5jruwhtxpa2mi2uttexyq55go5ke5arb/include", ":")
prepend_path("PKG_CONFIG_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/googletest-1.10.0-5jruwhtxpa2mi2uttexyq55go5ke5arb/lib64/pkgconfig", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/googletest-1.10.0-5jruwhtxpa2mi2uttexyq55go5ke5arb/", ":")

