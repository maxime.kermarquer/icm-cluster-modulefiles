-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2018-09-19 11:35:06.517912
--
-- gsl@1.8%gcc@6.4.0 arch=linux-centos7-x86_64 /v5btgkk
--

whatis([[Name : gsl]])
whatis([[Version : 1.8]])
whatis([[Short description : The GNU Scientific Library (GSL) is a numerical library for C and C++ programmers. It is free software under the GNU General Public License. The library provides a wide range of mathematical routines such as random number generators, special functions and least-squares fitting. There are over 1000 functions in total with an extensive test suite.]])

help([[The GNU Scientific Library (GSL) is a numerical library for C and C++
programmers. It is free software under the GNU General Public License.
The library provides a wide range of mathematical routines such as
random number generators, special functions and least-squares fitting.
There are over 1000 functions in total with an extensive test suite.]])



prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/gsl-1.8-v5btgkksrmnlg2thzrnhgg3lod374575/bin", ":")
prepend_path("MANPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/gsl-1.8-v5btgkksrmnlg2thzrnhgg3lod374575/man", ":")
prepend_path("ACLOCAL_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/gsl-1.8-v5btgkksrmnlg2thzrnhgg3lod374575/share/aclocal", ":")
prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/gsl-1.8-v5btgkksrmnlg2thzrnhgg3lod374575/lib", ":")
prepend_path("LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/gsl-1.8-v5btgkksrmnlg2thzrnhgg3lod374575/lib", ":")
prepend_path("CPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/gsl-1.8-v5btgkksrmnlg2thzrnhgg3lod374575/include", ":")
prepend_path("PKG_CONFIG_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/gsl-1.8-v5btgkksrmnlg2thzrnhgg3lod374575/lib/pkgconfig", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/gsl-1.8-v5btgkksrmnlg2thzrnhgg3lod374575/", ":")

