-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2019-02-06 20:19:07.111778
--
-- harfbuzz@1.4.6%gcc@6.4.0 arch=linux-centos7-x86_64 /pkqkf7y
--

whatis([[Name : harfbuzz]])
whatis([[Version : 1.4.6]])
whatis([[Short description : The Harfbuzz package contains an OpenType text shaping engine.]])

help([[The Harfbuzz package contains an OpenType text shaping engine.]])



prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/harfbuzz-1.4.6-pkqkf7ylqse3sbxqrj2kfahd6k3rbaw6/bin", ":")
prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/harfbuzz-1.4.6-pkqkf7ylqse3sbxqrj2kfahd6k3rbaw6/lib", ":")
prepend_path("LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/harfbuzz-1.4.6-pkqkf7ylqse3sbxqrj2kfahd6k3rbaw6/lib", ":")
prepend_path("CPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/harfbuzz-1.4.6-pkqkf7ylqse3sbxqrj2kfahd6k3rbaw6/include", ":")
prepend_path("PKG_CONFIG_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/harfbuzz-1.4.6-pkqkf7ylqse3sbxqrj2kfahd6k3rbaw6/lib/pkgconfig", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/harfbuzz-1.4.6-pkqkf7ylqse3sbxqrj2kfahd6k3rbaw6/", ":")
prepend_path("CPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/libxml2-2.9.8-m3rdnxm66wlzs5gqsm5pqlzqvideutnk/include/libxml2", ":")
prepend_path("CPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/freetype-2.7.1-fnguk6ypqcpy23j3j2n3l62sjvfd3sf2/include/freetype2", ":")

