-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2021-11-30 16:25:13.918141
--
-- help2man@1.47.16%gcc@6.4.0 arch=linux-centos7-broadwell/e6f2wer
--

whatis([[Name : help2man]])
whatis([[Version : 1.47.16]])
whatis([[Target : broadwell]])
whatis([[Short description : help2man produces simple manual pages from the '--help' and '--version' output of other commands.]])

help([[help2man produces simple manual pages from the '--help' and '--version'
output of other commands.]])



prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/help2man-1.47.16-e6f2wert4fhg7cm3e2tasckbeprzn4ft/bin", ":")
prepend_path("MANPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/help2man-1.47.16-e6f2wert4fhg7cm3e2tasckbeprzn4ft/share/man", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/help2man-1.47.16-e6f2wert4fhg7cm3e2tasckbeprzn4ft/", ":")

