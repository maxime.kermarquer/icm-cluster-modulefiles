-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2018-09-19 11:03:35.280211
--
-- help2man@1.47.4%gcc@6.4.0 arch=linux-centos7-x86_64 /jde5qz5
--

whatis([[Name : help2man]])
whatis([[Version : 1.47.4]])
whatis([[Short description : help2man produces simple manual pages from the '--help' and '--version' output of other commands.]])

help([[help2man produces simple manual pages from the '--help' and '--version'
output of other commands.]])



prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/help2man-1.47.4-jde5qz5h4qjeejdz7zs5niv2r3wibngj/bin", ":")
prepend_path("MANPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/help2man-1.47.4-jde5qz5h4qjeejdz7zs5niv2r3wibngj/share/man", ":")
prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/help2man-1.47.4-jde5qz5h4qjeejdz7zs5niv2r3wibngj/lib", ":")
prepend_path("LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/help2man-1.47.4-jde5qz5h4qjeejdz7zs5niv2r3wibngj/lib", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/help2man-1.47.4-jde5qz5h4qjeejdz7zs5niv2r3wibngj/", ":")

