-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2021-03-17 15:27:35.148384
--
-- htop@2.2.0%gcc@6.4.0 arch=linux-centos7-broadwell/w6p25fx
--

whatis([[Name : htop]])
whatis([[Version : 2.2.0]])
whatis([[Target : broadwell]])
whatis([[Short description : htop is an interactive text-mode process viewer for Unix systems.]])
whatis([[Configure options : --enable-shared]])

help([[htop is an interactive text-mode process viewer for Unix systems.]])



prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/htop-2.2.0-w6p25fxbsvwxypvroc654nqfjwhshm26/bin", ":")
prepend_path("MANPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/htop-2.2.0-w6p25fxbsvwxypvroc654nqfjwhshm26/share/man", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/htop-2.2.0-w6p25fxbsvwxypvroc654nqfjwhshm26/", ":")

