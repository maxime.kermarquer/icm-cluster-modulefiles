-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2022-03-08 16:40:32.625663
--
-- htslib@1.10.2%gcc@6.4.0+libcurl arch=linux-centos7-broadwell/salaoke
--

whatis([[Name : htslib]])
whatis([[Version : 1.10.2]])
whatis([[Target : broadwell]])
whatis([[Short description : C library for high-throughput sequencing data formats.]])
whatis([[Configure options : --enable-libcurl]])

help([[C library for high-throughput sequencing data formats.]])



prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/htslib-1.10.2-salaokepumaslycpym2mqrldt7ygyene/lib", ":")
prepend_path("LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/htslib-1.10.2-salaokepumaslycpym2mqrldt7ygyene/lib", ":")
prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/htslib-1.10.2-salaokepumaslycpym2mqrldt7ygyene/bin", ":")
prepend_path("MANPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/htslib-1.10.2-salaokepumaslycpym2mqrldt7ygyene/share/man", ":")
prepend_path("C_INCLUDE_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/htslib-1.10.2-salaokepumaslycpym2mqrldt7ygyene/include", ":")
prepend_path("CPLUS_INCLUDE_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/htslib-1.10.2-salaokepumaslycpym2mqrldt7ygyene/include", ":")
prepend_path("INCLUDE", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/htslib-1.10.2-salaokepumaslycpym2mqrldt7ygyene/include", ":")
prepend_path("PKG_CONFIG_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/htslib-1.10.2-salaokepumaslycpym2mqrldt7ygyene/lib/pkgconfig", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/htslib-1.10.2-salaokepumaslycpym2mqrldt7ygyene/", ":")

