-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2018-10-02 15:04:34.237187
--
-- hwloc@1.11.9%gcc@6.4.0~cairo~cuda+libxml2+pci+shared arch=linux-centos7-x86_64 /lmbkym4
--

whatis([[Name : hwloc]])
whatis([[Version : 1.11.9]])
whatis([[Short description : The Hardware Locality (hwloc) software project.]])
whatis([[Configure options : --disable-opencl --disable-cairo --disable-cuda --enable-libxml2 --enable-pci --enable-shared]])

help([[The Hardware Locality (hwloc) software project. The Portable Hardware
Locality (hwloc) software package provides a portable abstraction
(across OS, versions, architectures, ...) of the hierarchical topology
of modern architectures, including NUMA memory nodes, sockets, shared
caches, cores and simultaneous multithreading. It also gathers various
system attributes such as cache and memory information as well as the
locality of I/O devices such as network interfaces, InfiniBand HCAs or
GPUs. It primarily aims at helping applications with gathering
information about modern computing hardware so as to exploit it
accordingly and efficiently.]])



prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/hwloc-1.11.9-lmbkym4knppvajnov3iag6fwf364stnx/bin", ":")
prepend_path("MANPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/hwloc-1.11.9-lmbkym4knppvajnov3iag6fwf364stnx/share/man", ":")
prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/hwloc-1.11.9-lmbkym4knppvajnov3iag6fwf364stnx/lib", ":")
prepend_path("LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/hwloc-1.11.9-lmbkym4knppvajnov3iag6fwf364stnx/lib", ":")
prepend_path("CPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/hwloc-1.11.9-lmbkym4knppvajnov3iag6fwf364stnx/include", ":")
prepend_path("PKG_CONFIG_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/hwloc-1.11.9-lmbkym4knppvajnov3iag6fwf364stnx/lib/pkgconfig", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/hwloc-1.11.9-lmbkym4knppvajnov3iag6fwf364stnx/", ":")
prepend_path("CPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/libxml2-2.9.8-m3rdnxm66wlzs5gqsm5pqlzqvideutnk/include/libxml2", ":")

