-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2021-01-13 17:23:37.521240
--
-- hwloc@2.4.0%gcc@6.4.0~cairo~cuda~gl~libudev+libxml2~netloc~nvml+pci+shared arch=linux-centos7-broadwell/ekhsw53
--

whatis([[Name : hwloc]])
whatis([[Version : 2.4.0]])
whatis([[Target : broadwell]])
whatis([[Short description : The Hardware Locality (hwloc) software project.]])
whatis([[Configure options : --disable-opencl --disable-cairo --disable-nvml --disable-gl --disable-cuda --enable-libxml2 --disable-libudev --enable-pci --enable-shared]])

help([[The Hardware Locality (hwloc) software project. The Portable Hardware
Locality (hwloc) software package provides a portable abstraction
(across OS, versions, architectures, ...) of the hierarchical topology
of modern architectures, including NUMA memory nodes, sockets, shared
caches, cores and simultaneous multithreading. It also gathers various
system attributes such as cache and memory information as well as the
locality of I/O devices such as network interfaces, InfiniBand HCAs or
GPUs. It primarily aims at helping applications with gathering
information about modern computing hardware so as to exploit it
accordingly and efficiently.]])



prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/hwloc-2.4.0-ekhsw53rov2jgyq4b6oqszcxgu2zu5sn/lib", ":")
prepend_path("LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/hwloc-2.4.0-ekhsw53rov2jgyq4b6oqszcxgu2zu5sn/lib", ":")
prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/hwloc-2.4.0-ekhsw53rov2jgyq4b6oqszcxgu2zu5sn/bin", ":")
prepend_path("MANPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/hwloc-2.4.0-ekhsw53rov2jgyq4b6oqszcxgu2zu5sn/share/man", ":")
prepend_path("C_INCLUDE_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/hwloc-2.4.0-ekhsw53rov2jgyq4b6oqszcxgu2zu5sn/include", ":")
prepend_path("CPLUS_INCLUDE_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/hwloc-2.4.0-ekhsw53rov2jgyq4b6oqszcxgu2zu5sn/include", ":")
prepend_path("INCLUDE", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/hwloc-2.4.0-ekhsw53rov2jgyq4b6oqszcxgu2zu5sn/include", ":")
prepend_path("PKG_CONFIG_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/hwloc-2.4.0-ekhsw53rov2jgyq4b6oqszcxgu2zu5sn/lib/pkgconfig", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/hwloc-2.4.0-ekhsw53rov2jgyq4b6oqszcxgu2zu5sn/", ":")

