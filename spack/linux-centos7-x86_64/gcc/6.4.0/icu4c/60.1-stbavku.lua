-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2018-09-19 11:08:41.756374
--
-- icu4c@60.1%gcc@6.4.0 arch=linux-centos7-x86_64 /stbavku
--

whatis([[Name : icu4c]])
whatis([[Version : 60.1]])
whatis([[Short description : ICU is a mature, widely used set of C/C++ and Java libraries providing Unicode and Globalization support for software applications. ICU4C is the C/C++ interface.]])
whatis([[Configure options : --enable-rpath]])

help([[ICU is a mature, widely used set of C/C++ and Java libraries providing
Unicode and Globalization support for software applications. ICU4C is
the C/C++ interface.]])



prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/icu4c-60.1-stbavkucvni53lux3j2keswytkxmzwqp/bin", ":")
prepend_path("MANPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/icu4c-60.1-stbavkucvni53lux3j2keswytkxmzwqp/share/man", ":")
prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/icu4c-60.1-stbavkucvni53lux3j2keswytkxmzwqp/lib", ":")
prepend_path("LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/icu4c-60.1-stbavkucvni53lux3j2keswytkxmzwqp/lib", ":")
prepend_path("CPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/icu4c-60.1-stbavkucvni53lux3j2keswytkxmzwqp/include", ":")
prepend_path("PKG_CONFIG_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/icu4c-60.1-stbavkucvni53lux3j2keswytkxmzwqp/lib/pkgconfig", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/icu4c-60.1-stbavkucvni53lux3j2keswytkxmzwqp/", ":")

