-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2023-05-17 16:18:34.681449
--
-- icu4c@67.1%gcc@6.4.0 build_system=autotools cxxstd=11 arch=linux-centos7-broadwell/63z4q7x
--

whatis([[Name : icu4c]])
whatis([[Version : 67.1]])
whatis([[Target : broadwell]])
whatis([[Short description : ICU is a mature, widely used set of C/C++ and Java libraries providing Unicode and Globalization support for software applications. ICU4C is the C/C++ interface.]])
whatis([[Configure options : PYTHON=/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/python-3.8.7-eoi5k2niuf2tyzvjxcwt6qwaxtwm32ri/bin/python3.8]])

help([[Name   : icu4c]])
help([[Version: 67.1]])
help([[Target : broadwell]])
help()
help([[ICU is a mature, widely used set of C/C++ and Java libraries providing
Unicode and Globalization support for software applications. ICU4C is
the C/C++ interface.]])



prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/icu4c-67.1-63z4q7xrhus2qkuco5ysfarxcy67xzul/bin", ":")
prepend_path("MANPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/icu4c-67.1-63z4q7xrhus2qkuco5ysfarxcy67xzul/share/man", ":")
prepend_path("PKG_CONFIG_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/icu4c-67.1-63z4q7xrhus2qkuco5ysfarxcy67xzul/lib/pkgconfig", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/icu4c-67.1-63z4q7xrhus2qkuco5ysfarxcy67xzul/.", ":")

