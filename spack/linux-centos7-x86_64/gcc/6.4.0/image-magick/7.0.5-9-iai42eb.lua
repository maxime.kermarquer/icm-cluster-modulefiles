-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2020-02-24 11:20:45.478058
--
-- image-magick@7.0.5-9%gcc@6.4.0 arch=linux-centos7-x86_64 /iai42eb
--

whatis([[Name : image-magick]])
whatis([[Version : 7.0.5-9]])
whatis([[Short description : ImageMagick is a software suite to create, edit, compose, or convert bitmap images.]])
whatis([[Configure options : --with-gs-font-dir=/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/ghostscript-fonts-8.11-3pl2rj44ixdift7tsupcvw4or3ac2q5u/share/font]])

help([[ImageMagick is a software suite to create, edit, compose, or convert
bitmap images.]])



prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/image-magick-7.0.5-9-iai42eb72lj34hxcvrmfodpvxqp4cu2n/bin", ":")
prepend_path("MANPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/image-magick-7.0.5-9-iai42eb72lj34hxcvrmfodpvxqp4cu2n/share/man", ":")
prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/image-magick-7.0.5-9-iai42eb72lj34hxcvrmfodpvxqp4cu2n/lib", ":")
prepend_path("LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/image-magick-7.0.5-9-iai42eb72lj34hxcvrmfodpvxqp4cu2n/lib", ":")
prepend_path("CPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/image-magick-7.0.5-9-iai42eb72lj34hxcvrmfodpvxqp4cu2n/include", ":")
prepend_path("PKG_CONFIG_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/image-magick-7.0.5-9-iai42eb72lj34hxcvrmfodpvxqp4cu2n/lib/pkgconfig", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/image-magick-7.0.5-9-iai42eb72lj34hxcvrmfodpvxqp4cu2n/", ":")
prepend_path("XDG_DATA_DIRS", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/pango-1.41.0-6g6sapxd2n6qkomhzorm2h4fmo4mu3qg/share", ":")
prepend_path("CPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/libxml2-2.9.8-m3rdnxm66wlzs5gqsm5pqlzqvideutnk/include/libxml2", ":")
prepend_path("CPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/freetype-2.7.1-fnguk6ypqcpy23j3j2n3l62sjvfd3sf2/include/freetype2", ":")

