-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2023-05-17 16:14:37.529169
--
-- inputproto@2.3.2%gcc@6.4.0 build_system=autotools arch=linux-centos7-broadwell/3n5ebij
--

whatis([[Name : inputproto]])
whatis([[Version : 2.3.2]])
whatis([[Target : broadwell]])
whatis([[Short description : X Input Extension.]])

help([[Name   : inputproto]])
help([[Version: 2.3.2]])
help([[Target : broadwell]])
help()
help([[X Input Extension. This extension defines a protocol to provide
additional input devices management such as graphic tablets.]])



prepend_path("PKG_CONFIG_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/inputproto-2.3.2-3n5ebijogccrexovqurpjs55ckao7ftx/lib/pkgconfig", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/inputproto-2.3.2-3n5ebijogccrexovqurpjs55ckao7ftx/.", ":")

