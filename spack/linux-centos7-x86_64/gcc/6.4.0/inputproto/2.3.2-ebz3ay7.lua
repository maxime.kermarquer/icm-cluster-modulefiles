-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2018-09-19 10:56:04.278065
--
-- inputproto@2.3.2%gcc@6.4.0 arch=linux-centos7-x86_64 /ebz3ay7
--

whatis([[Name : inputproto]])
whatis([[Version : 2.3.2]])
whatis([[Short description : X Input Extension.]])

help([[X Input Extension. This extension defines a protocol to provide
additional input devices management such as graphic tablets.]])



prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/inputproto-2.3.2-ebz3ay7ggmejebwu2v7ounmtln3rme67/lib", ":")
prepend_path("LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/inputproto-2.3.2-ebz3ay7ggmejebwu2v7ounmtln3rme67/lib", ":")
prepend_path("CPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/inputproto-2.3.2-ebz3ay7ggmejebwu2v7ounmtln3rme67/include", ":")
prepend_path("PKG_CONFIG_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/inputproto-2.3.2-ebz3ay7ggmejebwu2v7ounmtln3rme67/lib/pkgconfig", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/inputproto-2.3.2-ebz3ay7ggmejebwu2v7ounmtln3rme67/", ":")

