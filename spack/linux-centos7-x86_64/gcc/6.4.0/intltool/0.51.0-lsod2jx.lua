-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2019-02-06 19:30:37.467218
--
-- intltool@0.51.0%gcc@6.4.0 patches=ca9d6562f29f06c64150f50369a24402b7aa01a3a0dc73dce55106f3224330a1 arch=linux-centos7-x86_64 /lsod2jx
--

whatis([[Name : intltool]])
whatis([[Version : 0.51.0]])
whatis([[Short description : intltool is a set of tools to centralize translation of many different file formats using GNU gettext-compatible PO files.]])

help([[intltool is a set of tools to centralize translation of many different
file formats using GNU gettext-compatible PO files.]])



prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/intltool-0.51.0-lsod2jx2rxtpqxl75imq7ihapxe6e252/bin", ":")
prepend_path("MANPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/intltool-0.51.0-lsod2jx2rxtpqxl75imq7ihapxe6e252/share/man", ":")
prepend_path("ACLOCAL_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/intltool-0.51.0-lsod2jx2rxtpqxl75imq7ihapxe6e252/share/aclocal", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/intltool-0.51.0-lsod2jx2rxtpqxl75imq7ihapxe6e252/", ":")
prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/perl-xml-parser-2.44-5ynzrni47wk6ar6wkemzhuekzbxprn3f/bin", ":")
prepend_path("PERL5LIB", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/perl-xml-parser-2.44-5ynzrni47wk6ar6wkemzhuekzbxprn3f/lib/perl5", ":")

