-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2019-02-05 18:41:24.169173
--
-- jasper@2.0.14%gcc@6.4.0 build_type=Release +jpeg~opengl+shared arch=linux-centos7-x86_64 /7kb335f
--

whatis([[Name : jasper]])
whatis([[Version : 2.0.14]])
whatis([[Short description : Library for manipulating JPEG-2000 images]])
whatis([[Configure options : --prefix=/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/jasper-2.0.14-7kb335fugi4hrccgkycip4akvifyb4nj --enable-libjpeg --disable-opengl --enable-shared --disable-debug]])

help([[Library for manipulating JPEG-2000 images]])



prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/jasper-2.0.14-7kb335fugi4hrccgkycip4akvifyb4nj/bin", ":")
prepend_path("MANPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/jasper-2.0.14-7kb335fugi4hrccgkycip4akvifyb4nj/share/man", ":")
prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/jasper-2.0.14-7kb335fugi4hrccgkycip4akvifyb4nj/lib64", ":")
prepend_path("LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/jasper-2.0.14-7kb335fugi4hrccgkycip4akvifyb4nj/lib64", ":")
prepend_path("CPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/jasper-2.0.14-7kb335fugi4hrccgkycip4akvifyb4nj/include", ":")
prepend_path("PKG_CONFIG_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/jasper-2.0.14-7kb335fugi4hrccgkycip4akvifyb4nj/lib64/pkgconfig", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/jasper-2.0.14-7kb335fugi4hrccgkycip4akvifyb4nj/", ":")

