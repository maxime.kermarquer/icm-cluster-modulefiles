-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2018-09-20 11:32:40.714537
--
-- jemalloc@4.5.0%gcc@6.4.0~prof~stats arch=linux-centos7-x86_64 /dzimwu7
--

whatis([[Name : jemalloc]])
whatis([[Version : 4.5.0]])
whatis([[Short description : jemalloc is a general purpose malloc(3) implementation that emphasizes fragmentation avoidance and scalable concurrency support.]])

help([[jemalloc is a general purpose malloc(3) implementation that emphasizes
fragmentation avoidance and scalable concurrency support.]])



prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/jemalloc-4.5.0-dzimwu7s2w6fzk6bm7yum63acb5f3zra/bin", ":")
prepend_path("MANPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/jemalloc-4.5.0-dzimwu7s2w6fzk6bm7yum63acb5f3zra/share/man", ":")
prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/jemalloc-4.5.0-dzimwu7s2w6fzk6bm7yum63acb5f3zra/lib", ":")
prepend_path("LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/jemalloc-4.5.0-dzimwu7s2w6fzk6bm7yum63acb5f3zra/lib", ":")
prepend_path("CPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/jemalloc-4.5.0-dzimwu7s2w6fzk6bm7yum63acb5f3zra/include", ":")
prepend_path("PKG_CONFIG_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/jemalloc-4.5.0-dzimwu7s2w6fzk6bm7yum63acb5f3zra/lib/pkgconfig", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/jemalloc-4.5.0-dzimwu7s2w6fzk6bm7yum63acb5f3zra/", ":")

