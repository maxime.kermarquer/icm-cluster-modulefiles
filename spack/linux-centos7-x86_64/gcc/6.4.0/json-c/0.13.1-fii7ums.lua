-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2021-01-13 17:43:15.568845
--
-- json-c@0.13.1%gcc@6.4.0 arch=linux-centos7-broadwell/fii7ums
--

whatis([[Name : json-c]])
whatis([[Version : 0.13.1]])
whatis([[Target : broadwell]])
whatis([[Short description : A JSON implementation in C.]])

help([[A JSON implementation in C.]])



prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/json-c-0.13.1-fii7ums6rqhxlrrwmx3c3tokj7e2seje/lib", ":")
prepend_path("LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/json-c-0.13.1-fii7ums6rqhxlrrwmx3c3tokj7e2seje/lib", ":")
prepend_path("C_INCLUDE_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/json-c-0.13.1-fii7ums6rqhxlrrwmx3c3tokj7e2seje/include", ":")
prepend_path("CPLUS_INCLUDE_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/json-c-0.13.1-fii7ums6rqhxlrrwmx3c3tokj7e2seje/include", ":")
prepend_path("INCLUDE", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/json-c-0.13.1-fii7ums6rqhxlrrwmx3c3tokj7e2seje/include", ":")
prepend_path("PKG_CONFIG_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/json-c-0.13.1-fii7ums6rqhxlrrwmx3c3tokj7e2seje/lib/pkgconfig", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/json-c-0.13.1-fii7ums6rqhxlrrwmx3c3tokj7e2seje/", ":")

