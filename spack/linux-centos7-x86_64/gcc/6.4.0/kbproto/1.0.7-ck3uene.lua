-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2018-09-19 10:56:09.461462
--
-- kbproto@1.0.7%gcc@6.4.0 arch=linux-centos7-x86_64 /ck3uene
--

whatis([[Name : kbproto]])
whatis([[Version : 1.0.7]])
whatis([[Short description : X Keyboard Extension.]])

help([[X Keyboard Extension. This extension defines a protcol to provide a
number of new capabilities and controls for text keyboards.]])



prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/kbproto-1.0.7-ck3uene2z3ihb4hhqeuagtubtne2sltx/lib", ":")
prepend_path("LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/kbproto-1.0.7-ck3uene2z3ihb4hhqeuagtubtne2sltx/lib", ":")
prepend_path("CPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/kbproto-1.0.7-ck3uene2z3ihb4hhqeuagtubtne2sltx/include", ":")
prepend_path("PKG_CONFIG_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/kbproto-1.0.7-ck3uene2z3ihb4hhqeuagtubtne2sltx/lib/pkgconfig", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/kbproto-1.0.7-ck3uene2z3ihb4hhqeuagtubtne2sltx/", ":")

