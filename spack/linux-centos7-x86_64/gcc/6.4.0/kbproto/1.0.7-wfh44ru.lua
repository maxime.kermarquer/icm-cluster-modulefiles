-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2023-05-17 16:14:10.756396
--
-- kbproto@1.0.7%gcc@6.4.0 build_system=autotools arch=linux-centos7-broadwell/wfh44ru
--

whatis([[Name : kbproto]])
whatis([[Version : 1.0.7]])
whatis([[Target : broadwell]])
whatis([[Short description : X Keyboard Extension.]])

help([[Name   : kbproto]])
help([[Version: 1.0.7]])
help([[Target : broadwell]])
help()
help([[X Keyboard Extension. This extension defines a protcol to provide a
number of new capabilities and controls for text keyboards.]])



prepend_path("PKG_CONFIG_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/kbproto-1.0.7-wfh44rujfylyfdamo5riivwtc4sw3hm2/lib/pkgconfig", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/kbproto-1.0.7-wfh44rujfylyfdamo5riivwtc4sw3hm2/.", ":")

