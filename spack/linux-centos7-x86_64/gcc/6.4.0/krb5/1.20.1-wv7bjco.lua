-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2023-05-04 10:49:39.067275
--
-- krb5@1.20.1%gcc@6.4.0+shared build_system=autotools arch=linux-centos7-broadwell/wv7bjco
--

whatis([[Name : krb5]])
whatis([[Version : 1.20.1]])
whatis([[Target : broadwell]])
whatis([[Short description : Network authentication protocol]])
whatis([[Configure options : --without-system-verto --disable-static]])

help([[Name   : krb5]])
help([[Version: 1.20.1]])
help([[Target : broadwell]])
help()
help([[Network authentication protocol]])


depends_on("linux-centos7-x86_64/gcc/6.4.0/gettext/0.21-pzcpiio")
depends_on("linux-centos7-x86_64/gcc/6.4.0/openssl/1.1.1i-kphel3r")

prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/krb5-1.20.1-wv7bjcoap5dj57deopbq4vk7kvmnx44h/bin", ":")
prepend_path("MANPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/krb5-1.20.1-wv7bjcoap5dj57deopbq4vk7kvmnx44h/share/man", ":")
prepend_path("PKG_CONFIG_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/krb5-1.20.1-wv7bjcoap5dj57deopbq4vk7kvmnx44h/lib/pkgconfig", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/krb5-1.20.1-wv7bjcoap5dj57deopbq4vk7kvmnx44h/.", ":")

