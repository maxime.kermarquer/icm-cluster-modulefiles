-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2018-09-19 11:44:19.134109
--
-- lcms@2.9%gcc@6.4.0 arch=linux-centos7-x86_64 /iqcomfx
--

whatis([[Name : lcms]])
whatis([[Version : 2.9]])
whatis([[Short description : Little cms is a color management library. Implements fast transforms between ICC profiles. It is focused on speed, and is portable across several platforms (MIT license).]])

help([[Little cms is a color management library. Implements fast transforms
between ICC profiles. It is focused on speed, and is portable across
several platforms (MIT license).]])



prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/lcms-2.9-iqcomfxhnhq6zarimhe4so56negefgke/bin", ":")
prepend_path("MANPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/lcms-2.9-iqcomfxhnhq6zarimhe4so56negefgke/share/man", ":")
prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/lcms-2.9-iqcomfxhnhq6zarimhe4so56negefgke/lib", ":")
prepend_path("LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/lcms-2.9-iqcomfxhnhq6zarimhe4so56negefgke/lib", ":")
prepend_path("CPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/lcms-2.9-iqcomfxhnhq6zarimhe4so56negefgke/include", ":")
prepend_path("PKG_CONFIG_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/lcms-2.9-iqcomfxhnhq6zarimhe4so56negefgke/lib/pkgconfig", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/lcms-2.9-iqcomfxhnhq6zarimhe4so56negefgke/", ":")

