-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2018-09-20 11:32:41.970153
--
-- libaio@0.3.110%gcc@6.4.0 arch=linux-centos7-x86_64 /4txx243
--

whatis([[Name : libaio]])
whatis([[Version : 0.3.110]])
whatis([[Short description : This is the linux native Asynchronous I/O interface library.]])

help([[This is the linux native Asynchronous I/O interface library.]])



prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/libaio-0.3.110-4txx243if25niaw4oeive2rbrjqxurr6/lib", ":")
prepend_path("LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/libaio-0.3.110-4txx243if25niaw4oeive2rbrjqxurr6/lib", ":")
prepend_path("CPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/libaio-0.3.110-4txx243if25niaw4oeive2rbrjqxurr6/include", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/libaio-0.3.110-4txx243if25niaw4oeive2rbrjqxurr6/", ":")

