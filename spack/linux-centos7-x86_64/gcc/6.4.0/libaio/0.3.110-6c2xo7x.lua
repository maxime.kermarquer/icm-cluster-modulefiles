-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2021-01-13 16:55:07.220901
--
-- libaio@0.3.110%gcc@6.4.0 arch=linux-centos7-broadwell/6c2xo7x
--

whatis([[Name : libaio]])
whatis([[Version : 0.3.110]])
whatis([[Target : broadwell]])
whatis([[Short description : Linux native Asynchronous I/O interface library.]])

help([[Linux native Asynchronous I/O interface library. AIO enables even a
single application thread to overlap I/O operations with other
processing, by providing an interface for submitting one or more I/O
requests in one system call (io_submit()) without waiting for
completion, and a separate interface (io_getevents()) to reap completed
I/O operations associated with a given completion group.]])



prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/libaio-0.3.110-6c2xo7xgcddlo4b2apj5cbx2ofglbdtr/lib", ":")
prepend_path("LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/libaio-0.3.110-6c2xo7xgcddlo4b2apj5cbx2ofglbdtr/lib", ":")
prepend_path("C_INCLUDE_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/libaio-0.3.110-6c2xo7xgcddlo4b2apj5cbx2ofglbdtr/include", ":")
prepend_path("CPLUS_INCLUDE_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/libaio-0.3.110-6c2xo7xgcddlo4b2apj5cbx2ofglbdtr/include", ":")
prepend_path("INCLUDE", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/libaio-0.3.110-6c2xo7xgcddlo4b2apj5cbx2ofglbdtr/include", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/libaio-0.3.110-6c2xo7xgcddlo4b2apj5cbx2ofglbdtr/", ":")

