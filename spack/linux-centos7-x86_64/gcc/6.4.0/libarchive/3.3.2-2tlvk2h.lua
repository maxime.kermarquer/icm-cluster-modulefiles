-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2020-12-12 16:41:11.468399
--
-- libarchive@3.3.2%gcc@6.4.0 arch=linux-centos7-x86_64 /2tlvk2h
--

whatis([[Name : libarchive]])
whatis([[Version : 3.3.2]])
whatis([[Short description : libarchive: C library and command-line tools for reading and writing tar, cpio, zip, ISO, and other archive formats.]])

help([[libarchive: C library and command-line tools for reading and writing
tar, cpio, zip, ISO, and other archive formats.]])



prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/libarchive-3.3.2-2tlvk2hyjbj5jb7r4q2zfa3oycffoejg/bin", ":")
prepend_path("MANPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/libarchive-3.3.2-2tlvk2hyjbj5jb7r4q2zfa3oycffoejg/share/man", ":")
prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/libarchive-3.3.2-2tlvk2hyjbj5jb7r4q2zfa3oycffoejg/lib", ":")
prepend_path("LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/libarchive-3.3.2-2tlvk2hyjbj5jb7r4q2zfa3oycffoejg/lib", ":")
prepend_path("CPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/libarchive-3.3.2-2tlvk2hyjbj5jb7r4q2zfa3oycffoejg/include", ":")
prepend_path("PKG_CONFIG_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/libarchive-3.3.2-2tlvk2hyjbj5jb7r4q2zfa3oycffoejg/lib/pkgconfig", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/libarchive-3.3.2-2tlvk2hyjbj5jb7r4q2zfa3oycffoejg/", ":")
prepend_path("CPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/libxml2-2.9.8-m3rdnxm66wlzs5gqsm5pqlzqvideutnk/include/libxml2", ":")

