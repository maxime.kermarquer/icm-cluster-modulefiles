-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2021-01-13 19:28:15.331695
--
-- libarchive@3.5.1%gcc@6.4.0 arch=linux-centos7-broadwell/ehwfqol
--

whatis([[Name : libarchive]])
whatis([[Version : 3.5.1]])
whatis([[Target : broadwell]])
whatis([[Short description : libarchive: C library and command-line tools for reading and writing tar, cpio, zip, ISO, and other archive formats.]])

help([[libarchive: C library and command-line tools for reading and writing
tar, cpio, zip, ISO, and other archive formats.]])



prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/libarchive-3.5.1-ehwfqolp7siafzodfcuwqq6mzk7pswew/lib", ":")
prepend_path("LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/libarchive-3.5.1-ehwfqolp7siafzodfcuwqq6mzk7pswew/lib", ":")
prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/libarchive-3.5.1-ehwfqolp7siafzodfcuwqq6mzk7pswew/bin", ":")
prepend_path("MANPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/libarchive-3.5.1-ehwfqolp7siafzodfcuwqq6mzk7pswew/share/man", ":")
prepend_path("C_INCLUDE_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/libarchive-3.5.1-ehwfqolp7siafzodfcuwqq6mzk7pswew/include", ":")
prepend_path("CPLUS_INCLUDE_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/libarchive-3.5.1-ehwfqolp7siafzodfcuwqq6mzk7pswew/include", ":")
prepend_path("INCLUDE", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/libarchive-3.5.1-ehwfqolp7siafzodfcuwqq6mzk7pswew/include", ":")
prepend_path("PKG_CONFIG_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/libarchive-3.5.1-ehwfqolp7siafzodfcuwqq6mzk7pswew/lib/pkgconfig", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/libarchive-3.5.1-ehwfqolp7siafzodfcuwqq6mzk7pswew/", ":")

