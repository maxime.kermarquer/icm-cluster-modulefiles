-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2021-01-13 16:55:25.918845
--
-- libbsd@0.10.0%gcc@6.4.0 arch=linux-centos7-broadwell/wog2nt6
--

whatis([[Name : libbsd]])
whatis([[Version : 0.10.0]])
whatis([[Target : broadwell]])
whatis([[Short description : This library provides useful functions commonly found on BSD systems, and lacking on others like GNU systems, thus making it easier to port projects with strong BSD origins, without needing to embed the same code over and over again on each project. ]])

help([[This library provides useful functions commonly found on BSD systems,
and lacking on others like GNU systems, thus making it easier to port
projects with strong BSD origins, without needing to embed the same code
over and over again on each project.]])



prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/libbsd-0.10.0-wog2nt6zgmv57ca63skkdcysxjns4u3z/lib", ":")
prepend_path("LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/libbsd-0.10.0-wog2nt6zgmv57ca63skkdcysxjns4u3z/lib", ":")
prepend_path("MANPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/libbsd-0.10.0-wog2nt6zgmv57ca63skkdcysxjns4u3z/share/man", ":")
prepend_path("C_INCLUDE_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/libbsd-0.10.0-wog2nt6zgmv57ca63skkdcysxjns4u3z/include", ":")
prepend_path("CPLUS_INCLUDE_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/libbsd-0.10.0-wog2nt6zgmv57ca63skkdcysxjns4u3z/include", ":")
prepend_path("INCLUDE", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/libbsd-0.10.0-wog2nt6zgmv57ca63skkdcysxjns4u3z/include", ":")
prepend_path("PKG_CONFIG_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/libbsd-0.10.0-wog2nt6zgmv57ca63skkdcysxjns4u3z/lib/pkgconfig", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/libbsd-0.10.0-wog2nt6zgmv57ca63skkdcysxjns4u3z/", ":")

