-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2018-09-18 17:04:27.618664
--
-- libbsd@0.8.6%gcc@6.4.0 arch=linux-centos7-x86_64 /nqdwd3q
--

whatis([[Name : libbsd]])
whatis([[Version : 0.8.6]])
whatis([[Short description : This library provides useful functions commonly found on BSD systems, and lacking on others like GNU systems, thus making it easier to port projects with strong BSD origins, without needing to embed the same code over and over again on each project. ]])

help([[This library provides useful functions commonly found on BSD systems,
and lacking on others like GNU systems, thus making it easier to port
projects with strong BSD origins, without needing to embed the same code
over and over again on each project.]])



prepend_path("MANPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/libbsd-0.8.6-nqdwd3q3kcrejjg3p5dcsgte55xwov7e/share/man", ":")
prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/libbsd-0.8.6-nqdwd3q3kcrejjg3p5dcsgte55xwov7e/lib", ":")
prepend_path("LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/libbsd-0.8.6-nqdwd3q3kcrejjg3p5dcsgte55xwov7e/lib", ":")
prepend_path("CPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/libbsd-0.8.6-nqdwd3q3kcrejjg3p5dcsgte55xwov7e/include", ":")
prepend_path("PKG_CONFIG_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/libbsd-0.8.6-nqdwd3q3kcrejjg3p5dcsgte55xwov7e/lib/pkgconfig", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/libbsd-0.8.6-nqdwd3q3kcrejjg3p5dcsgte55xwov7e/", ":")

