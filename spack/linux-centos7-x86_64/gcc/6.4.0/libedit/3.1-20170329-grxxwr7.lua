-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2018-09-20 11:32:54.768211
--
-- libedit@3.1-20170329%gcc@6.4.0 arch=linux-centos7-x86_64 /grxxwr7
--

whatis([[Name : libedit]])
whatis([[Version : 3.1-20170329]])
whatis([[Short description : An autotools compatible port of the NetBSD editline library]])

help([[An autotools compatible port of the NetBSD editline library]])



prepend_path("MANPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/libedit-3.1-20170329-grxxwr7mmikwx7jfopimjugtlp7y2bux/share/man", ":")
prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/libedit-3.1-20170329-grxxwr7mmikwx7jfopimjugtlp7y2bux/lib", ":")
prepend_path("LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/libedit-3.1-20170329-grxxwr7mmikwx7jfopimjugtlp7y2bux/lib", ":")
prepend_path("CPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/libedit-3.1-20170329-grxxwr7mmikwx7jfopimjugtlp7y2bux/include", ":")
prepend_path("PKG_CONFIG_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/libedit-3.1-20170329-grxxwr7mmikwx7jfopimjugtlp7y2bux/lib/pkgconfig", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/libedit-3.1-20170329-grxxwr7mmikwx7jfopimjugtlp7y2bux/", ":")

