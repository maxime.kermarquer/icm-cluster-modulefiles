-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2021-01-13 17:10:53.164482
--
-- libedit@3.1-20191231%gcc@6.4.0 arch=linux-centos7-broadwell/excu5kp
--

whatis([[Name : libedit]])
whatis([[Version : 3.1-20191231]])
whatis([[Target : broadwell]])
whatis([[Short description : An autotools compatible port of the NetBSD editline library]])

help([[An autotools compatible port of the NetBSD editline library]])



prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/libedit-3.1-20191231-excu5kpaqhgeafimiejbljofau3afgj3/lib", ":")
prepend_path("LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/libedit-3.1-20191231-excu5kpaqhgeafimiejbljofau3afgj3/lib", ":")
prepend_path("MANPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/libedit-3.1-20191231-excu5kpaqhgeafimiejbljofau3afgj3/share/man", ":")
prepend_path("C_INCLUDE_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/libedit-3.1-20191231-excu5kpaqhgeafimiejbljofau3afgj3/include", ":")
prepend_path("CPLUS_INCLUDE_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/libedit-3.1-20191231-excu5kpaqhgeafimiejbljofau3afgj3/include", ":")
prepend_path("INCLUDE", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/libedit-3.1-20191231-excu5kpaqhgeafimiejbljofau3afgj3/include", ":")
prepend_path("PKG_CONFIG_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/libedit-3.1-20191231-excu5kpaqhgeafimiejbljofau3afgj3/lib/pkgconfig", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/libedit-3.1-20191231-excu5kpaqhgeafimiejbljofau3afgj3/", ":")

