-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2018-09-20 11:33:21.316519
--
-- libevent@2.0.21%gcc@6.4.0+openssl arch=linux-centos7-x86_64 /35iviu6
--

whatis([[Name : libevent]])
whatis([[Version : 2.0.21]])
whatis([[Short description : The libevent API provides a mechanism to execute a callback function when a specific event occurs on a file descriptor or after a timeout has been reached. Furthermore, libevent also support callbacks due to signals or regular timeouts.]])
whatis([[Configure options : --enable-openssl]])

help([[The libevent API provides a mechanism to execute a callback function
when a specific event occurs on a file descriptor or after a timeout has
been reached. Furthermore, libevent also support callbacks due to
signals or regular timeouts.]])



prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/libevent-2.0.21-35iviu66vp4uym7xsqlnvuc6rweat7vk/bin", ":")
prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/libevent-2.0.21-35iviu66vp4uym7xsqlnvuc6rweat7vk/lib", ":")
prepend_path("LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/libevent-2.0.21-35iviu66vp4uym7xsqlnvuc6rweat7vk/lib", ":")
prepend_path("CPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/libevent-2.0.21-35iviu66vp4uym7xsqlnvuc6rweat7vk/include", ":")
prepend_path("PKG_CONFIG_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/libevent-2.0.21-35iviu66vp4uym7xsqlnvuc6rweat7vk/lib/pkgconfig", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/libevent-2.0.21-35iviu66vp4uym7xsqlnvuc6rweat7vk/", ":")

