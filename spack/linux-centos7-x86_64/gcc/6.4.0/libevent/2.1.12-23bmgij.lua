-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2021-01-13 17:31:07.927506
--
-- libevent@2.1.12%gcc@6.4.0+openssl arch=linux-centos7-broadwell/23bmgij
--

whatis([[Name : libevent]])
whatis([[Version : 2.1.12]])
whatis([[Target : broadwell]])
whatis([[Short description : The libevent API provides a mechanism to execute a callback function when a specific event occurs on a file descriptor or after a timeout has been reached. Furthermore, libevent also support callbacks due to signals or regular timeouts.]])
whatis([[Configure options : --enable-openssl]])

help([[The libevent API provides a mechanism to execute a callback function
when a specific event occurs on a file descriptor or after a timeout has
been reached. Furthermore, libevent also support callbacks due to
signals or regular timeouts.]])



prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/libevent-2.1.12-23bmgijbukon3rbzbnwrnsgktvqiyblr/lib", ":")
prepend_path("LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/libevent-2.1.12-23bmgijbukon3rbzbnwrnsgktvqiyblr/lib", ":")
prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/libevent-2.1.12-23bmgijbukon3rbzbnwrnsgktvqiyblr/bin", ":")
prepend_path("C_INCLUDE_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/libevent-2.1.12-23bmgijbukon3rbzbnwrnsgktvqiyblr/include", ":")
prepend_path("CPLUS_INCLUDE_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/libevent-2.1.12-23bmgijbukon3rbzbnwrnsgktvqiyblr/include", ":")
prepend_path("INCLUDE", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/libevent-2.1.12-23bmgijbukon3rbzbnwrnsgktvqiyblr/include", ":")
prepend_path("PKG_CONFIG_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/libevent-2.1.12-23bmgijbukon3rbzbnwrnsgktvqiyblr/lib/pkgconfig", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/libevent-2.1.12-23bmgijbukon3rbzbnwrnsgktvqiyblr/", ":")

