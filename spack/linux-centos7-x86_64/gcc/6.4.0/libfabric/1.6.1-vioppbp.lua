-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2018-10-18 15:24:41.303129
--
-- libfabric@1.6.1%gcc@6.4.0 fabrics=sockets arch=linux-centos7-x86_64 /vioppbp
--

whatis([[Name : libfabric]])
whatis([[Version : 1.6.1]])
whatis([[Short description : The Open Fabrics Interfaces (OFI) is a framework focused on exporting fabric communication services to applications.]])
whatis([[Configure options : --enable-psm=no --enable-psm2=no --enable-sockets=yes --enable-verbs=no --enable-usnic=no --enable-gni=no --enable-xpmem=no --enable-udp=no --enable-rxm=no --enable-rxd=no --enable-mlx=no]])

help([[The Open Fabrics Interfaces (OFI) is a framework focused on exporting
fabric communication services to applications.]])



prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/libfabric-1.6.1-vioppbp5bornbhp5ngop5woxyckso224/bin", ":")
prepend_path("MANPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/libfabric-1.6.1-vioppbp5bornbhp5ngop5woxyckso224/share/man", ":")
prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/libfabric-1.6.1-vioppbp5bornbhp5ngop5woxyckso224/lib", ":")
prepend_path("LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/libfabric-1.6.1-vioppbp5bornbhp5ngop5woxyckso224/lib", ":")
prepend_path("CPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/libfabric-1.6.1-vioppbp5bornbhp5ngop5woxyckso224/include", ":")
prepend_path("PKG_CONFIG_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/libfabric-1.6.1-vioppbp5bornbhp5ngop5woxyckso224/lib/pkgconfig", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/libfabric-1.6.1-vioppbp5bornbhp5ngop5woxyckso224/", ":")

