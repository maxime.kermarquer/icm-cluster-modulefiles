-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2018-09-19 11:39:33.539149
--
-- libffi@3.2.1%gcc@6.4.0 arch=linux-centos7-x86_64 /mivldt7
--

whatis([[Name : libffi]])
whatis([[Version : 3.2.1]])
whatis([[Short description : The libffi library provides a portable, high level programming interface to various calling conventions. This allows a programmer to call any function specified by a call interface description at run time.]])

help([[The libffi library provides a portable, high level programming interface
to various calling conventions. This allows a programmer to call any
function specified by a call interface description at run time.]])



prepend_path("MANPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/libffi-3.2.1-mivldt7hi4cckincgnahgaf5lt3oplbf/share/man", ":")
prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/libffi-3.2.1-mivldt7hi4cckincgnahgaf5lt3oplbf/lib", ":")
prepend_path("LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/libffi-3.2.1-mivldt7hi4cckincgnahgaf5lt3oplbf/lib", ":")
prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/libffi-3.2.1-mivldt7hi4cckincgnahgaf5lt3oplbf/lib64", ":")
prepend_path("LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/libffi-3.2.1-mivldt7hi4cckincgnahgaf5lt3oplbf/lib64", ":")
prepend_path("PKG_CONFIG_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/libffi-3.2.1-mivldt7hi4cckincgnahgaf5lt3oplbf/lib/pkgconfig", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/libffi-3.2.1-mivldt7hi4cckincgnahgaf5lt3oplbf/", ":")

