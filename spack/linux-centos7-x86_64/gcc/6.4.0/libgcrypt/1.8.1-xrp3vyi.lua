-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2019-02-05 18:38:03.667556
--
-- libgcrypt@1.8.1%gcc@6.4.0 arch=linux-centos7-x86_64 /xrp3vyi
--

whatis([[Name : libgcrypt]])
whatis([[Version : 1.8.1]])
whatis([[Short description : Libgcrypt is a general purpose cryptographic library based on the code from GnuPG. It provides functions for all cryptographic building blocks: symmetric ciphers, hash algorithms, MACs, public key algorithms, large integer functions, random numbers and a lot of supporting functions. ]])

help([[Libgcrypt is a general purpose cryptographic library based on the code
from GnuPG. It provides functions for all cryptographic building blocks:
symmetric ciphers, hash algorithms, MACs, public key algorithms, large
integer functions, random numbers and a lot of supporting functions.]])



prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/libgcrypt-1.8.1-xrp3vyit22p5bynj7t4p4i3ndoupt7zk/bin", ":")
prepend_path("MANPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/libgcrypt-1.8.1-xrp3vyit22p5bynj7t4p4i3ndoupt7zk/share/man", ":")
prepend_path("ACLOCAL_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/libgcrypt-1.8.1-xrp3vyit22p5bynj7t4p4i3ndoupt7zk/share/aclocal", ":")
prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/libgcrypt-1.8.1-xrp3vyit22p5bynj7t4p4i3ndoupt7zk/lib", ":")
prepend_path("LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/libgcrypt-1.8.1-xrp3vyit22p5bynj7t4p4i3ndoupt7zk/lib", ":")
prepend_path("CPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/libgcrypt-1.8.1-xrp3vyit22p5bynj7t4p4i3ndoupt7zk/include", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/libgcrypt-1.8.1-xrp3vyit22p5bynj7t4p4i3ndoupt7zk/", ":")

