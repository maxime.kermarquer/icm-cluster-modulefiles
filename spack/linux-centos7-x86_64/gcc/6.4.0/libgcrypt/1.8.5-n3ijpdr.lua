-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2021-01-13 17:59:52.643806
--
-- libgcrypt@1.8.5%gcc@6.4.0 arch=linux-centos7-broadwell/n3ijpdr
--

whatis([[Name : libgcrypt]])
whatis([[Version : 1.8.5]])
whatis([[Target : broadwell]])
whatis([[Short description : Cryptographic library based on the code from GnuPG.]])

help([[Cryptographic library based on the code from GnuPG.]])



prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/libgcrypt-1.8.5-n3ijpdr23foxgp4jkt7bq5efqevvfiek/lib", ":")
prepend_path("LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/libgcrypt-1.8.5-n3ijpdr23foxgp4jkt7bq5efqevvfiek/lib", ":")
prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/libgcrypt-1.8.5-n3ijpdr23foxgp4jkt7bq5efqevvfiek/bin", ":")
prepend_path("MANPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/libgcrypt-1.8.5-n3ijpdr23foxgp4jkt7bq5efqevvfiek/share/man", ":")
prepend_path("ACLOCAL_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/libgcrypt-1.8.5-n3ijpdr23foxgp4jkt7bq5efqevvfiek/share/aclocal", ":")
prepend_path("C_INCLUDE_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/libgcrypt-1.8.5-n3ijpdr23foxgp4jkt7bq5efqevvfiek/include", ":")
prepend_path("CPLUS_INCLUDE_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/libgcrypt-1.8.5-n3ijpdr23foxgp4jkt7bq5efqevvfiek/include", ":")
prepend_path("INCLUDE", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/libgcrypt-1.8.5-n3ijpdr23foxgp4jkt7bq5efqevvfiek/include", ":")
prepend_path("PKG_CONFIG_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/libgcrypt-1.8.5-n3ijpdr23foxgp4jkt7bq5efqevvfiek/lib/pkgconfig", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/libgcrypt-1.8.5-n3ijpdr23foxgp4jkt7bq5efqevvfiek/", ":")

