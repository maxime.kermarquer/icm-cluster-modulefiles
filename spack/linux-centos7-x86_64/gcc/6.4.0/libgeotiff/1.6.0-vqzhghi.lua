-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2021-10-14 18:50:42.057401
--
-- libgeotiff@1.6.0%gcc@6.4.0+jpeg+proj+zlib arch=linux-centos7-broadwell/vqzhghi
--

whatis([[Name : libgeotiff]])
whatis([[Version : 1.6.0]])
whatis([[Target : broadwell]])
whatis([[Short description : GeoTIFF represents an effort by over 160 different remote sensing, GIS, cartographic, and surveying related companies and organizations to establish a TIFF based interchange format for georeferenced raster imagery. ]])
whatis([[Configure options : --with-libtiff=/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/libtiff-4.1.0-n6rdcwtr4a76cfjvcln4ahdlnyojyihd --with-zlib=/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/zlib-1.2.11-wp5daspjexwgd5rft64ekrbjk4uarol2 --with-jpeg=/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/libjpeg-turbo-2.0.6-muo6zfe37yacs7mdbonx6r37evue2k45 --with-proj=/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/proj-7.1.0-ksbdx7icgjlzhqrylhbcpi3t3p5mj4wr]])

help([[GeoTIFF represents an effort by over 160 different remote sensing, GIS,
cartographic, and surveying related companies and organizations to
establish a TIFF based interchange format for georeferenced raster
imagery.]])



prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/libgeotiff-1.6.0-vqzhghizhghyeqh2xjqrpprgwpvsrmjf/lib", ":")
prepend_path("LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/libgeotiff-1.6.0-vqzhghizhghyeqh2xjqrpprgwpvsrmjf/lib", ":")
prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/libgeotiff-1.6.0-vqzhghizhghyeqh2xjqrpprgwpvsrmjf/bin", ":")
prepend_path("MANPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/libgeotiff-1.6.0-vqzhghizhghyeqh2xjqrpprgwpvsrmjf/share/man", ":")
prepend_path("C_INCLUDE_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/libgeotiff-1.6.0-vqzhghizhghyeqh2xjqrpprgwpvsrmjf/include", ":")
prepend_path("CPLUS_INCLUDE_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/libgeotiff-1.6.0-vqzhghizhghyeqh2xjqrpprgwpvsrmjf/include", ":")
prepend_path("INCLUDE", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/libgeotiff-1.6.0-vqzhghizhghyeqh2xjqrpprgwpvsrmjf/include", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/libgeotiff-1.6.0-vqzhghizhghyeqh2xjqrpprgwpvsrmjf/", ":")

