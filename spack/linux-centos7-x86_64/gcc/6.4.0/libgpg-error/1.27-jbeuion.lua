-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2019-02-05 18:37:36.437531
--
-- libgpg-error@1.27%gcc@6.4.0 arch=linux-centos7-x86_64 /jbeuion
--

whatis([[Name : libgpg-error]])
whatis([[Version : 1.27]])
whatis([[Short description : Libgpg-error is a small library that defines common error values for all GnuPG components. Among these are GPG, GPGSM, GPGME, GPG-Agent, libgcrypt, Libksba, DirMngr, Pinentry, SmartCard Daemon and possibly more in the future. ]])

help([[Libgpg-error is a small library that defines common error values for all
GnuPG components. Among these are GPG, GPGSM, GPGME, GPG-Agent,
libgcrypt, Libksba, DirMngr, Pinentry, SmartCard Daemon and possibly
more in the future.]])



prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/libgpg-error-1.27-jbeuiontsrvbmsrjfzp44mv4lk6lfhzg/bin", ":")
prepend_path("MANPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/libgpg-error-1.27-jbeuiontsrvbmsrjfzp44mv4lk6lfhzg/share/man", ":")
prepend_path("ACLOCAL_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/libgpg-error-1.27-jbeuiontsrvbmsrjfzp44mv4lk6lfhzg/share/aclocal", ":")
prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/libgpg-error-1.27-jbeuiontsrvbmsrjfzp44mv4lk6lfhzg/lib", ":")
prepend_path("LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/libgpg-error-1.27-jbeuiontsrvbmsrjfzp44mv4lk6lfhzg/lib", ":")
prepend_path("CPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/libgpg-error-1.27-jbeuiontsrvbmsrjfzp44mv4lk6lfhzg/include", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/libgpg-error-1.27-jbeuiontsrvbmsrjfzp44mv4lk6lfhzg/", ":")

