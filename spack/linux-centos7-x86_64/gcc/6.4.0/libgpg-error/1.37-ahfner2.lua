-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2021-01-13 17:59:05.282837
--
-- libgpg-error@1.37%gcc@6.4.0 arch=linux-centos7-broadwell/ahfner2
--

whatis([[Name : libgpg-error]])
whatis([[Version : 1.37]])
whatis([[Target : broadwell]])
whatis([[Short description : Common error values for all GnuPG components.]])
whatis([[Configure options : --enable-static --disable-tests]])

help([[Common error values for all GnuPG components.]])



prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/libgpg-error-1.37-ahfner25kbzgyv6v7ivyzsemzxlsppyt/lib", ":")
prepend_path("LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/libgpg-error-1.37-ahfner25kbzgyv6v7ivyzsemzxlsppyt/lib", ":")
prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/libgpg-error-1.37-ahfner25kbzgyv6v7ivyzsemzxlsppyt/bin", ":")
prepend_path("MANPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/libgpg-error-1.37-ahfner25kbzgyv6v7ivyzsemzxlsppyt/share/man", ":")
prepend_path("ACLOCAL_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/libgpg-error-1.37-ahfner25kbzgyv6v7ivyzsemzxlsppyt/share/aclocal", ":")
prepend_path("C_INCLUDE_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/libgpg-error-1.37-ahfner25kbzgyv6v7ivyzsemzxlsppyt/include", ":")
prepend_path("CPLUS_INCLUDE_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/libgpg-error-1.37-ahfner25kbzgyv6v7ivyzsemzxlsppyt/include", ":")
prepend_path("INCLUDE", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/libgpg-error-1.37-ahfner25kbzgyv6v7ivyzsemzxlsppyt/include", ":")
prepend_path("PKG_CONFIG_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/libgpg-error-1.37-ahfner25kbzgyv6v7ivyzsemzxlsppyt/lib/pkgconfig", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/libgpg-error-1.37-ahfner25kbzgyv6v7ivyzsemzxlsppyt/", ":")

