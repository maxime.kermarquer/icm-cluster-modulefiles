-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2019-03-20 14:51:47.580604
--
-- libice@1.0.9%gcc@6.4.0 arch=linux-centos7-x86_64 /il7as4v
--

whatis([[Name : libice]])
whatis([[Version : 1.0.9]])
whatis([[Short description : libICE - Inter-Client Exchange Library.]])

help([[libICE - Inter-Client Exchange Library.]])



prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/libice-1.0.9-il7as4vrzfryhlj3zqbuzxtvton42dmk/lib", ":")
prepend_path("LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/libice-1.0.9-il7as4vrzfryhlj3zqbuzxtvton42dmk/lib", ":")
prepend_path("CPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/libice-1.0.9-il7as4vrzfryhlj3zqbuzxtvton42dmk/include", ":")
prepend_path("PKG_CONFIG_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/libice-1.0.9-il7as4vrzfryhlj3zqbuzxtvton42dmk/lib/pkgconfig", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/libice-1.0.9-il7as4vrzfryhlj3zqbuzxtvton42dmk/", ":")

