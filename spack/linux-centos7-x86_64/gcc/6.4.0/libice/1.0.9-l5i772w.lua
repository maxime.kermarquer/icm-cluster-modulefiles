-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2023-05-17 16:15:28.902738
--
-- libice@1.0.9%gcc@6.4.0 build_system=autotools arch=linux-centos7-broadwell/l5i772w
--

whatis([[Name : libice]])
whatis([[Version : 1.0.9]])
whatis([[Target : broadwell]])
whatis([[Short description : libICE - Inter-Client Exchange Library.]])

help([[Name   : libice]])
help([[Version: 1.0.9]])
help([[Target : broadwell]])
help()
help([[libICE - Inter-Client Exchange Library.]])


depends_on("xproto/7.0.31-2wcbwe7")
depends_on("xtrans/1.4.0-czrtbqp")

prepend_path("PKG_CONFIG_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/libice-1.0.9-l5i772wj2dws4jywtmgypohbasq46z65/lib/pkgconfig", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/libice-1.0.9-l5i772wj2dws4jywtmgypohbasq46z65/.", ":")

