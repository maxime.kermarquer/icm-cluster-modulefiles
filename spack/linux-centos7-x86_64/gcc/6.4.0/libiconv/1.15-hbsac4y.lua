-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2018-09-18 17:05:21.738886
--
-- libiconv@1.15%gcc@6.4.0 arch=linux-centos7-x86_64 /hbsac4y
--

whatis([[Name : libiconv]])
whatis([[Version : 1.15]])
whatis([[Short description : GNU libiconv provides an implementation of the iconv() function and the iconv program for character set conversion.]])

help([[GNU libiconv provides an implementation of the iconv() function and the
iconv program for character set conversion.]])



prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/libiconv-1.15-hbsac4ys2y3uz6btgbnglxiuqkxkosxf/bin", ":")
prepend_path("MANPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/libiconv-1.15-hbsac4ys2y3uz6btgbnglxiuqkxkosxf/share/man", ":")
prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/libiconv-1.15-hbsac4ys2y3uz6btgbnglxiuqkxkosxf/lib", ":")
prepend_path("LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/libiconv-1.15-hbsac4ys2y3uz6btgbnglxiuqkxkosxf/lib", ":")
prepend_path("CPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/libiconv-1.15-hbsac4ys2y3uz6btgbnglxiuqkxkosxf/include", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/libiconv-1.15-hbsac4ys2y3uz6btgbnglxiuqkxkosxf/", ":")

