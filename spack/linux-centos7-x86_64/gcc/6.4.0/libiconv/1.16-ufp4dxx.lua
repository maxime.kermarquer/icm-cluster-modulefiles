-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2021-01-13 16:54:35.314441
--
-- libiconv@1.16%gcc@6.4.0 arch=linux-centos7-broadwell/ufp4dxx
--

whatis([[Name : libiconv]])
whatis([[Version : 1.16]])
whatis([[Target : broadwell]])
whatis([[Short description : GNU libiconv provides an implementation of the iconv() function and the iconv program for character set conversion.]])
whatis([[Configure options : --enable-extra-encodings]])

help([[GNU libiconv provides an implementation of the iconv() function and the
iconv program for character set conversion.]])



prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/libiconv-1.16-ufp4dxx2bc4vtwju5qtgznbuy2zpzzbc/lib", ":")
prepend_path("LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/libiconv-1.16-ufp4dxx2bc4vtwju5qtgznbuy2zpzzbc/lib", ":")
prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/libiconv-1.16-ufp4dxx2bc4vtwju5qtgznbuy2zpzzbc/bin", ":")
prepend_path("MANPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/libiconv-1.16-ufp4dxx2bc4vtwju5qtgznbuy2zpzzbc/share/man", ":")
prepend_path("C_INCLUDE_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/libiconv-1.16-ufp4dxx2bc4vtwju5qtgznbuy2zpzzbc/include", ":")
prepend_path("CPLUS_INCLUDE_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/libiconv-1.16-ufp4dxx2bc4vtwju5qtgznbuy2zpzzbc/include", ":")
prepend_path("INCLUDE", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/libiconv-1.16-ufp4dxx2bc4vtwju5qtgznbuy2zpzzbc/include", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/libiconv-1.16-ufp4dxx2bc4vtwju5qtgznbuy2zpzzbc/", ":")

