-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2021-01-13 17:11:52.478125
--
-- libidn2@2.3.0%gcc@6.4.0 arch=linux-centos7-broadwell/tkwd4vy
--

whatis([[Name : libidn2]])
whatis([[Version : 2.3.0]])
whatis([[Target : broadwell]])
whatis([[Short description : Libidn2 is a free software implementation of IDNA2008, Punycode and TR46. Its purpose is to encode and decode internationalized domain names.]])

help([[Libidn2 is a free software implementation of IDNA2008, Punycode and
TR46. Its purpose is to encode and decode internationalized domain
names.]])



prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/libidn2-2.3.0-tkwd4vytzm66bbqk37sim6akijtojkm4/lib", ":")
prepend_path("LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/libidn2-2.3.0-tkwd4vytzm66bbqk37sim6akijtojkm4/lib", ":")
prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/libidn2-2.3.0-tkwd4vytzm66bbqk37sim6akijtojkm4/bin", ":")
prepend_path("MANPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/libidn2-2.3.0-tkwd4vytzm66bbqk37sim6akijtojkm4/share/man", ":")
prepend_path("C_INCLUDE_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/libidn2-2.3.0-tkwd4vytzm66bbqk37sim6akijtojkm4/include", ":")
prepend_path("CPLUS_INCLUDE_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/libidn2-2.3.0-tkwd4vytzm66bbqk37sim6akijtojkm4/include", ":")
prepend_path("INCLUDE", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/libidn2-2.3.0-tkwd4vytzm66bbqk37sim6akijtojkm4/include", ":")
prepend_path("PKG_CONFIG_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/libidn2-2.3.0-tkwd4vytzm66bbqk37sim6akijtojkm4/lib/pkgconfig", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/libidn2-2.3.0-tkwd4vytzm66bbqk37sim6akijtojkm4/", ":")

