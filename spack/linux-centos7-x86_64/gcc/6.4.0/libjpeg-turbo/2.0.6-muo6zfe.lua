-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2021-10-14 18:46:05.395808
--
-- libjpeg-turbo@2.0.6%gcc@6.4.0 arch=linux-centos7-broadwell/muo6zfe
--

whatis([[Name : libjpeg-turbo]])
whatis([[Version : 2.0.6]])
whatis([[Target : broadwell]])
whatis([[Short description : libjpeg-turbo is a fork of the original IJG libjpeg which uses SIMD to accelerate baseline JPEG compression and decompression.]])

help([[libjpeg-turbo is a fork of the original IJG libjpeg which uses SIMD to
accelerate baseline JPEG compression and decompression. libjpeg is a
library that implements JPEG image encoding, decoding and transcoding.]])



prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/libjpeg-turbo-2.0.6-muo6zfe37yacs7mdbonx6r37evue2k45/lib64", ":")
prepend_path("LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/libjpeg-turbo-2.0.6-muo6zfe37yacs7mdbonx6r37evue2k45/lib64", ":")
prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/libjpeg-turbo-2.0.6-muo6zfe37yacs7mdbonx6r37evue2k45/bin", ":")
prepend_path("MANPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/libjpeg-turbo-2.0.6-muo6zfe37yacs7mdbonx6r37evue2k45/share/man", ":")
prepend_path("C_INCLUDE_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/libjpeg-turbo-2.0.6-muo6zfe37yacs7mdbonx6r37evue2k45/include", ":")
prepend_path("CPLUS_INCLUDE_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/libjpeg-turbo-2.0.6-muo6zfe37yacs7mdbonx6r37evue2k45/include", ":")
prepend_path("INCLUDE", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/libjpeg-turbo-2.0.6-muo6zfe37yacs7mdbonx6r37evue2k45/include", ":")
prepend_path("PKG_CONFIG_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/libjpeg-turbo-2.0.6-muo6zfe37yacs7mdbonx6r37evue2k45/lib64/pkgconfig", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/libjpeg-turbo-2.0.6-muo6zfe37yacs7mdbonx6r37evue2k45/", ":")

