-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2018-10-18 14:31:22.284893
--
-- libnl@3.3.0%gcc@6.4.0 arch=linux-centos7-x86_64 /jin3ntt
--

whatis([[Name : libnl]])
whatis([[Version : 3.3.0]])
whatis([[Short description : libnl - Netlink Protocol Library Suite]])

help([[libnl - Netlink Protocol Library Suite]])



prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/libnl-3.3.0-jin3nttotl5zlf6xiomjlb2rieyf6xyv/bin", ":")
prepend_path("MANPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/libnl-3.3.0-jin3nttotl5zlf6xiomjlb2rieyf6xyv/share/man", ":")
prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/libnl-3.3.0-jin3nttotl5zlf6xiomjlb2rieyf6xyv/lib", ":")
prepend_path("LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/libnl-3.3.0-jin3nttotl5zlf6xiomjlb2rieyf6xyv/lib", ":")
prepend_path("CPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/libnl-3.3.0-jin3nttotl5zlf6xiomjlb2rieyf6xyv/include", ":")
prepend_path("PKG_CONFIG_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/libnl-3.3.0-jin3nttotl5zlf6xiomjlb2rieyf6xyv/lib/pkgconfig", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/libnl-3.3.0-jin3nttotl5zlf6xiomjlb2rieyf6xyv/", ":")

