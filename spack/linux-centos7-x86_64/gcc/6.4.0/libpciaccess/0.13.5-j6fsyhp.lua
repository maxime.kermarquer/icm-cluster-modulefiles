-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2018-10-02 15:02:40.681390
--
-- libpciaccess@0.13.5%gcc@6.4.0 arch=linux-centos7-x86_64 /j6fsyhp
--

whatis([[Name : libpciaccess]])
whatis([[Version : 0.13.5]])
whatis([[Short description : Generic PCI access library.]])

help([[Generic PCI access library.]])



prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/libpciaccess-0.13.5-j6fsyhpv7edgisytx4vyvimj6rvpmdav/lib", ":")
prepend_path("LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/libpciaccess-0.13.5-j6fsyhpv7edgisytx4vyvimj6rvpmdav/lib", ":")
prepend_path("CPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/libpciaccess-0.13.5-j6fsyhpv7edgisytx4vyvimj6rvpmdav/include", ":")
prepend_path("PKG_CONFIG_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/libpciaccess-0.13.5-j6fsyhpv7edgisytx4vyvimj6rvpmdav/lib/pkgconfig", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/libpciaccess-0.13.5-j6fsyhpv7edgisytx4vyvimj6rvpmdav/", ":")

