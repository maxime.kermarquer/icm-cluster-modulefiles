-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2021-01-13 17:12:11.596705
--
-- libpciaccess@0.16%gcc@6.4.0 arch=linux-centos7-broadwell/ifyycdk
--

whatis([[Name : libpciaccess]])
whatis([[Version : 0.16]])
whatis([[Target : broadwell]])
whatis([[Short description : Generic PCI access library.]])

help([[Generic PCI access library.]])



prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/libpciaccess-0.16-ifyycdk6gmzsksedolznfsy6y2wo7gh5/lib", ":")
prepend_path("LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/libpciaccess-0.16-ifyycdk6gmzsksedolznfsy6y2wo7gh5/lib", ":")
prepend_path("C_INCLUDE_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/libpciaccess-0.16-ifyycdk6gmzsksedolznfsy6y2wo7gh5/include", ":")
prepend_path("CPLUS_INCLUDE_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/libpciaccess-0.16-ifyycdk6gmzsksedolznfsy6y2wo7gh5/include", ":")
prepend_path("INCLUDE", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/libpciaccess-0.16-ifyycdk6gmzsksedolznfsy6y2wo7gh5/include", ":")
prepend_path("PKG_CONFIG_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/libpciaccess-0.16-ifyycdk6gmzsksedolznfsy6y2wo7gh5/lib/pkgconfig", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/libpciaccess-0.16-ifyycdk6gmzsksedolznfsy6y2wo7gh5/", ":")

