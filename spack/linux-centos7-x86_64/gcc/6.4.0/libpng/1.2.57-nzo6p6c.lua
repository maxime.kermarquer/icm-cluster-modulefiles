-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2023-04-28 15:39:25.225934
--
-- libpng@1.2.57%gcc@6.4.0~ipo build_system=cmake build_type=RelWithDebInfo generator=make libs=shared,static arch=linux-centos7-broadwell/nzo6p6c
--

whatis([[Name : libpng]])
whatis([[Version : 1.2.57]])
whatis([[Target : broadwell]])
whatis([[Short description : libpng is the official PNG reference library.]])

help([[Name   : libpng]])
help([[Version: 1.2.57]])
help([[Target : broadwell]])
help()
help([[libpng is the official PNG reference library.]])


depends_on("zlib/1.2.11-wp5dasp")

prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/libpng-1.2.57-nzo6p6cosvrpadjbg5cnbuqo5yt6ayr4/bin", ":")
prepend_path("MANPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/libpng-1.2.57-nzo6p6cosvrpadjbg5cnbuqo5yt6ayr4/man", ":")
prepend_path("PKG_CONFIG_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/libpng-1.2.57-nzo6p6cosvrpadjbg5cnbuqo5yt6ayr4/lib/pkgconfig", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/libpng-1.2.57-nzo6p6cosvrpadjbg5cnbuqo5yt6ayr4/.", ":")

