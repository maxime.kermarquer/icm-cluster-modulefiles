-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2019-10-25 17:08:08.809818
--
-- libpng@1.2.57%gcc@6.4.0 arch=linux-centos7-x86_64 /zgnlc7c
--

whatis([[Name : libpng]])
whatis([[Version : 1.2.57]])
whatis([[Short description : libpng is the official PNG reference library.]])
whatis([[Configure options : CPPFLAGS=-I/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/zlib-1.2.11-ufwqfd4zrwuuu7fmguamaqokn5plxyns/include LDFLAGS=-L/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/zlib-1.2.11-ufwqfd4zrwuuu7fmguamaqokn5plxyns/lib]])

help([[libpng is the official PNG reference library.]])



prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/libpng-1.2.57-zgnlc7cqwox64c3e4zfy7pfre36ofcpf/bin", ":")
prepend_path("MANPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/libpng-1.2.57-zgnlc7cqwox64c3e4zfy7pfre36ofcpf/share/man", ":")
prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/libpng-1.2.57-zgnlc7cqwox64c3e4zfy7pfre36ofcpf/lib", ":")
prepend_path("LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/libpng-1.2.57-zgnlc7cqwox64c3e4zfy7pfre36ofcpf/lib", ":")
prepend_path("CPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/libpng-1.2.57-zgnlc7cqwox64c3e4zfy7pfre36ofcpf/include", ":")
prepend_path("PKG_CONFIG_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/libpng-1.2.57-zgnlc7cqwox64c3e4zfy7pfre36ofcpf/lib/pkgconfig", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/libpng-1.2.57-zgnlc7cqwox64c3e4zfy7pfre36ofcpf/", ":")

