-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2021-03-30 21:12:56.859746
--
-- libpng@1.6.37%gcc@6.4.0 arch=linux-centos7-broadwell/qxiycq3
--

whatis([[Name : libpng]])
whatis([[Version : 1.6.37]])
whatis([[Target : broadwell]])
whatis([[Short description : libpng is the official PNG reference library.]])
whatis([[Configure options : CPPFLAGS=-I/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/zlib-1.2.11-wp5daspjexwgd5rft64ekrbjk4uarol2/include LDFLAGS=-L/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/zlib-1.2.11-wp5daspjexwgd5rft64ekrbjk4uarol2/lib]])

help([[libpng is the official PNG reference library.]])



prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/libpng-1.6.37-qxiycq3kmiiuczaaz77uv7mwx7erpvbl/lib", ":")
prepend_path("LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/libpng-1.6.37-qxiycq3kmiiuczaaz77uv7mwx7erpvbl/lib", ":")
prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/libpng-1.6.37-qxiycq3kmiiuczaaz77uv7mwx7erpvbl/bin", ":")
prepend_path("MANPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/libpng-1.6.37-qxiycq3kmiiuczaaz77uv7mwx7erpvbl/share/man", ":")
prepend_path("C_INCLUDE_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/libpng-1.6.37-qxiycq3kmiiuczaaz77uv7mwx7erpvbl/include", ":")
prepend_path("CPLUS_INCLUDE_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/libpng-1.6.37-qxiycq3kmiiuczaaz77uv7mwx7erpvbl/include", ":")
prepend_path("INCLUDE", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/libpng-1.6.37-qxiycq3kmiiuczaaz77uv7mwx7erpvbl/include", ":")
prepend_path("PKG_CONFIG_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/libpng-1.6.37-qxiycq3kmiiuczaaz77uv7mwx7erpvbl/lib/pkgconfig", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/libpng-1.6.37-qxiycq3kmiiuczaaz77uv7mwx7erpvbl/", ":")

