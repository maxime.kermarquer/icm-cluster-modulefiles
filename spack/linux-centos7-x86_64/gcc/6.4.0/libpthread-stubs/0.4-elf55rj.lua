-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2023-05-17 16:13:32.789929
--
-- libpthread-stubs@0.4%gcc@6.4.0 build_system=autotools arch=linux-centos7-broadwell/elf55rj
--

whatis([[Name : libpthread-stubs]])
whatis([[Version : 0.4]])
whatis([[Target : broadwell]])
whatis([[Short description : The libpthread-stubs package provides weak aliases for pthread functions not provided in libc or otherwise available by default.]])

help([[Name   : libpthread-stubs]])
help([[Version: 0.4]])
help([[Target : broadwell]])
help()
help([[The libpthread-stubs package provides weak aliases for pthread functions
not provided in libc or otherwise available by default.]])



prepend_path("PKG_CONFIG_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/libpthread-stubs-0.4-elf55rjvfsyf4n7yawooewjnxsye5xsr/lib/pkgconfig", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/libpthread-stubs-0.4-elf55rjvfsyf4n7yawooewjnxsye5xsr/.", ":")

