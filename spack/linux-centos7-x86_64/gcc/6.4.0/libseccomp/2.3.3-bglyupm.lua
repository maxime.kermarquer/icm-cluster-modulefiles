-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2021-01-13 17:55:56.886681
--
-- libseccomp@2.3.3%gcc@6.4.0+python arch=linux-centos7-broadwell/bglyupm
--

whatis([[Name : libseccomp]])
whatis([[Version : 2.3.3]])
whatis([[Target : broadwell]])
whatis([[Short description : The main libseccomp repository]])
whatis([[Configure options : --enable-python]])

help([[The main libseccomp repository]])



prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/libseccomp-2.3.3-bglyupmcw4vjtjfoxlbkbuffy4uhdnp3/lib", ":")
prepend_path("LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/libseccomp-2.3.3-bglyupmcw4vjtjfoxlbkbuffy4uhdnp3/lib", ":")
prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/libseccomp-2.3.3-bglyupmcw4vjtjfoxlbkbuffy4uhdnp3/bin", ":")
prepend_path("MANPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/libseccomp-2.3.3-bglyupmcw4vjtjfoxlbkbuffy4uhdnp3/share/man", ":")
prepend_path("C_INCLUDE_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/libseccomp-2.3.3-bglyupmcw4vjtjfoxlbkbuffy4uhdnp3/include", ":")
prepend_path("CPLUS_INCLUDE_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/libseccomp-2.3.3-bglyupmcw4vjtjfoxlbkbuffy4uhdnp3/include", ":")
prepend_path("INCLUDE", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/libseccomp-2.3.3-bglyupmcw4vjtjfoxlbkbuffy4uhdnp3/include", ":")
prepend_path("PKG_CONFIG_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/libseccomp-2.3.3-bglyupmcw4vjtjfoxlbkbuffy4uhdnp3/lib/pkgconfig", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/libseccomp-2.3.3-bglyupmcw4vjtjfoxlbkbuffy4uhdnp3/", ":")

