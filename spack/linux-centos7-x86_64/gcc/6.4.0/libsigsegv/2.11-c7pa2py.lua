-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2018-09-18 16:52:39.856615
--
-- libsigsegv@2.11%gcc@6.4.0 arch=linux-centos7-x86_64 /c7pa2py
--

whatis([[Name : libsigsegv]])
whatis([[Version : 2.11]])
whatis([[Short description : GNU libsigsegv is a library for handling page faults in user mode.]])
whatis([[Configure options : --enable-shared]])

help([[GNU libsigsegv is a library for handling page faults in user mode.]])



prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/libsigsegv-2.11-c7pa2pydcb5hlo6ok3ermvmtybifjqe2/lib", ":")
prepend_path("LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/libsigsegv-2.11-c7pa2pydcb5hlo6ok3ermvmtybifjqe2/lib", ":")
prepend_path("CPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/libsigsegv-2.11-c7pa2pydcb5hlo6ok3ermvmtybifjqe2/include", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/libsigsegv-2.11-c7pa2pydcb5hlo6ok3ermvmtybifjqe2/", ":")

