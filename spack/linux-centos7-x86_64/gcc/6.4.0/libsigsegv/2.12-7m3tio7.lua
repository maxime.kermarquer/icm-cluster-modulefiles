-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2021-01-13 16:48:30.577790
--
-- libsigsegv@2.12%gcc@6.4.0 arch=linux-centos7-broadwell/7m3tio7
--

whatis([[Name : libsigsegv]])
whatis([[Version : 2.12]])
whatis([[Target : broadwell]])
whatis([[Short description : GNU libsigsegv is a library for handling page faults in user mode.]])
whatis([[Configure options : --enable-shared]])

help([[GNU libsigsegv is a library for handling page faults in user mode.]])



prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/libsigsegv-2.12-7m3tio72mk3mnwef66ytv6f6uudnnhfj/lib", ":")
prepend_path("LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/libsigsegv-2.12-7m3tio72mk3mnwef66ytv6f6uudnnhfj/lib", ":")
prepend_path("C_INCLUDE_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/libsigsegv-2.12-7m3tio72mk3mnwef66ytv6f6uudnnhfj/include", ":")
prepend_path("CPLUS_INCLUDE_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/libsigsegv-2.12-7m3tio72mk3mnwef66ytv6f6uudnnhfj/include", ":")
prepend_path("INCLUDE", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/libsigsegv-2.12-7m3tio72mk3mnwef66ytv6f6uudnnhfj/include", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/libsigsegv-2.12-7m3tio72mk3mnwef66ytv6f6uudnnhfj/", ":")

