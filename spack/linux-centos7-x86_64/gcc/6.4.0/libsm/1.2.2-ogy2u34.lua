-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2019-03-20 14:51:58.886289
--
-- libsm@1.2.2%gcc@6.4.0 arch=linux-centos7-x86_64 /ogy2u34
--

whatis([[Name : libsm]])
whatis([[Version : 1.2.2]])
whatis([[Short description : libSM - X Session Management Library.]])

help([[libSM - X Session Management Library.]])



prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/libsm-1.2.2-ogy2u342oxxvt7xbpjra3fjzczm2h6ky/lib", ":")
prepend_path("LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/libsm-1.2.2-ogy2u342oxxvt7xbpjra3fjzczm2h6ky/lib", ":")
prepend_path("CPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/libsm-1.2.2-ogy2u342oxxvt7xbpjra3fjzczm2h6ky/include", ":")
prepend_path("PKG_CONFIG_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/libsm-1.2.2-ogy2u342oxxvt7xbpjra3fjzczm2h6ky/lib/pkgconfig", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/libsm-1.2.2-ogy2u342oxxvt7xbpjra3fjzczm2h6ky/", ":")

