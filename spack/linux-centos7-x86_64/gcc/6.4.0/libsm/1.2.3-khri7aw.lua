-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2023-05-17 16:17:30.857352
--
-- libsm@1.2.3%gcc@6.4.0 build_system=autotools arch=linux-centos7-broadwell/khri7aw
--

whatis([[Name : libsm]])
whatis([[Version : 1.2.3]])
whatis([[Target : broadwell]])
whatis([[Short description : libSM - X Session Management Library.]])

help([[Name   : libsm]])
help([[Version: 1.2.3]])
help([[Target : broadwell]])
help()
help([[libSM - X Session Management Library.]])


depends_on("libice/1.0.9-l5i772w")
depends_on("util-linux-uuid/2.36-nlh67vl")
depends_on("xproto/7.0.31-2wcbwe7")
depends_on("xtrans/1.4.0-czrtbqp")

prepend_path("PKG_CONFIG_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/libsm-1.2.3-khri7aw4rpw43qgdjcf4g3liqlalykiq/lib/pkgconfig", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/libsm-1.2.3-khri7aw4rpw43qgdjcf4g3liqlalykiq/.", ":")

