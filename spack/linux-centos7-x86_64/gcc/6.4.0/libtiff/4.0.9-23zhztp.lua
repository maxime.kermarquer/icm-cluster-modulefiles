-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2018-09-19 11:44:04.734234
--
-- libtiff@4.0.9%gcc@6.4.0 arch=linux-centos7-x86_64 /23zhztp
--

whatis([[Name : libtiff]])
whatis([[Version : 4.0.9]])
whatis([[Short description : LibTIFF - Tag Image File Format (TIFF) Library and Utilities.]])

help([[LibTIFF - Tag Image File Format (TIFF) Library and Utilities.]])



prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/libtiff-4.0.9-23zhztpam6tknjjdbkwldxn62tindbxf/bin", ":")
prepend_path("MANPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/libtiff-4.0.9-23zhztpam6tknjjdbkwldxn62tindbxf/share/man", ":")
prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/libtiff-4.0.9-23zhztpam6tknjjdbkwldxn62tindbxf/lib", ":")
prepend_path("LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/libtiff-4.0.9-23zhztpam6tknjjdbkwldxn62tindbxf/lib", ":")
prepend_path("CPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/libtiff-4.0.9-23zhztpam6tknjjdbkwldxn62tindbxf/include", ":")
prepend_path("PKG_CONFIG_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/libtiff-4.0.9-23zhztpam6tknjjdbkwldxn62tindbxf/lib/pkgconfig", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/libtiff-4.0.9-23zhztpam6tknjjdbkwldxn62tindbxf/", ":")

