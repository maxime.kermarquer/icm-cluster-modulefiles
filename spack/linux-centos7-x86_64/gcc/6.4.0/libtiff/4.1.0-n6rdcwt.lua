-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2021-10-14 18:47:18.488162
--
-- libtiff@4.1.0%gcc@6.4.0 arch=linux-centos7-broadwell/n6rdcwt
--

whatis([[Name : libtiff]])
whatis([[Version : 4.1.0]])
whatis([[Target : broadwell]])
whatis([[Short description : LibTIFF - Tag Image File Format (TIFF) Library and Utilities.]])

help([[LibTIFF - Tag Image File Format (TIFF) Library and Utilities.]])



prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/libtiff-4.1.0-n6rdcwtr4a76cfjvcln4ahdlnyojyihd/lib", ":")
prepend_path("LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/libtiff-4.1.0-n6rdcwtr4a76cfjvcln4ahdlnyojyihd/lib", ":")
prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/libtiff-4.1.0-n6rdcwtr4a76cfjvcln4ahdlnyojyihd/bin", ":")
prepend_path("MANPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/libtiff-4.1.0-n6rdcwtr4a76cfjvcln4ahdlnyojyihd/share/man", ":")
prepend_path("C_INCLUDE_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/libtiff-4.1.0-n6rdcwtr4a76cfjvcln4ahdlnyojyihd/include", ":")
prepend_path("CPLUS_INCLUDE_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/libtiff-4.1.0-n6rdcwtr4a76cfjvcln4ahdlnyojyihd/include", ":")
prepend_path("INCLUDE", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/libtiff-4.1.0-n6rdcwtr4a76cfjvcln4ahdlnyojyihd/include", ":")
prepend_path("PKG_CONFIG_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/libtiff-4.1.0-n6rdcwtr4a76cfjvcln4ahdlnyojyihd/lib/pkgconfig", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/libtiff-4.1.0-n6rdcwtr4a76cfjvcln4ahdlnyojyihd/", ":")

