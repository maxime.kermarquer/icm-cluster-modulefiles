-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2023-05-04 10:50:19.579980
--
-- libtirpc@1.2.6%gcc@6.4.0 build_system=autotools arch=linux-centos7-broadwell/uqn3frc
--

whatis([[Name : libtirpc]])
whatis([[Version : 1.2.6]])
whatis([[Target : broadwell]])
whatis([[Short description : Libtirpc is a port of Suns Transport-Independent RPC library to Linux.]])

help([[Name   : libtirpc]])
help([[Version: 1.2.6]])
help([[Target : broadwell]])
help()
help([[Libtirpc is a port of Suns Transport-Independent RPC library to Linux.]])


depends_on("linux-centos7-x86_64/gcc/6.4.0/krb5/1.20.1-wv7bjco")

prepend_path("MANPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/libtirpc-1.2.6-uqn3frcen4bywpdbr57klztniiplx5ri/share/man", ":")
prepend_path("PKG_CONFIG_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/libtirpc-1.2.6-uqn3frcen4bywpdbr57klztniiplx5ri/lib/pkgconfig", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/libtirpc-1.2.6-uqn3frcen4bywpdbr57klztniiplx5ri/.", ":")

