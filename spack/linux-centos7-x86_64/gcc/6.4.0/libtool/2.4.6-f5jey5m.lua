-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2018-09-18 17:05:33.174583
--
-- libtool@2.4.6%gcc@6.4.0 arch=linux-centos7-x86_64 /f5jey5m
--

whatis([[Name : libtool]])
whatis([[Version : 2.4.6]])
whatis([[Short description : libtool -- library building part of autotools.]])

help([[libtool -- library building part of autotools.]])



prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/libtool-2.4.6-f5jey5mczcnuppxedvdkwdpplubkh2op/bin", ":")
prepend_path("MANPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/libtool-2.4.6-f5jey5mczcnuppxedvdkwdpplubkh2op/share/man", ":")
prepend_path("ACLOCAL_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/libtool-2.4.6-f5jey5mczcnuppxedvdkwdpplubkh2op/share/aclocal", ":")
prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/libtool-2.4.6-f5jey5mczcnuppxedvdkwdpplubkh2op/lib", ":")
prepend_path("LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/libtool-2.4.6-f5jey5mczcnuppxedvdkwdpplubkh2op/lib", ":")
prepend_path("CPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/libtool-2.4.6-f5jey5mczcnuppxedvdkwdpplubkh2op/include", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/libtool-2.4.6-f5jey5mczcnuppxedvdkwdpplubkh2op/", ":")

