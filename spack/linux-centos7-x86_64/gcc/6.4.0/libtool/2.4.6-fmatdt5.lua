-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2021-01-13 17:10:07.305888
--
-- libtool@2.4.6%gcc@6.4.0 arch=linux-centos7-broadwell/fmatdt5
--

whatis([[Name : libtool]])
whatis([[Version : 2.4.6]])
whatis([[Target : broadwell]])
whatis([[Short description : libtool -- library building part of autotools.]])

help([[libtool -- library building part of autotools.]])



prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/libtool-2.4.6-fmatdt55a3aaz56oh7aqmmjdpuoegxux/lib", ":")
prepend_path("LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/libtool-2.4.6-fmatdt55a3aaz56oh7aqmmjdpuoegxux/lib", ":")
prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/libtool-2.4.6-fmatdt55a3aaz56oh7aqmmjdpuoegxux/bin", ":")
prepend_path("MANPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/libtool-2.4.6-fmatdt55a3aaz56oh7aqmmjdpuoegxux/share/man", ":")
prepend_path("ACLOCAL_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/libtool-2.4.6-fmatdt55a3aaz56oh7aqmmjdpuoegxux/share/aclocal", ":")
prepend_path("C_INCLUDE_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/libtool-2.4.6-fmatdt55a3aaz56oh7aqmmjdpuoegxux/include", ":")
prepend_path("CPLUS_INCLUDE_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/libtool-2.4.6-fmatdt55a3aaz56oh7aqmmjdpuoegxux/include", ":")
prepend_path("INCLUDE", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/libtool-2.4.6-fmatdt55a3aaz56oh7aqmmjdpuoegxux/include", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/libtool-2.4.6-fmatdt55a3aaz56oh7aqmmjdpuoegxux/", ":")

