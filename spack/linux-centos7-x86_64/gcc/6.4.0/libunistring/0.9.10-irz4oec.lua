-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2021-01-13 17:07:12.073923
--
-- libunistring@0.9.10%gcc@6.4.0 arch=linux-centos7-broadwell/irz4oec
--

whatis([[Name : libunistring]])
whatis([[Version : 0.9.10]])
whatis([[Target : broadwell]])
whatis([[Short description : This library provides functions for manipulating Unicode strings and for manipulating C strings according to the Unicode standard.]])

help([[This library provides functions for manipulating Unicode strings and for
manipulating C strings according to the Unicode standard.]])



prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/libunistring-0.9.10-irz4oec5d3vcogrtsq4f4whu6dxkydrg/lib", ":")
prepend_path("LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/libunistring-0.9.10-irz4oec5d3vcogrtsq4f4whu6dxkydrg/lib", ":")
prepend_path("C_INCLUDE_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/libunistring-0.9.10-irz4oec5d3vcogrtsq4f4whu6dxkydrg/include", ":")
prepend_path("CPLUS_INCLUDE_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/libunistring-0.9.10-irz4oec5d3vcogrtsq4f4whu6dxkydrg/include", ":")
prepend_path("INCLUDE", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/libunistring-0.9.10-irz4oec5d3vcogrtsq4f4whu6dxkydrg/include", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/libunistring-0.9.10-irz4oec5d3vcogrtsq4f4whu6dxkydrg/", ":")

