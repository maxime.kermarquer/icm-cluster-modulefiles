-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2023-05-17 16:19:42.468592
--
-- libx11@1.8.4%gcc@6.4.0 build_system=autotools arch=linux-centos7-broadwell/cxeh2gx
--

whatis([[Name : libx11]])
whatis([[Version : 1.8.4]])
whatis([[Target : broadwell]])
whatis([[Short description : libX11 - Core X11 protocol client library.]])

help([[Name   : libx11]])
help([[Version: 1.8.4]])
help([[Target : broadwell]])
help()
help([[libX11 - Core X11 protocol client library.]])


depends_on("inputproto/2.3.2-3n5ebij")
depends_on("kbproto/1.0.7-wfh44ru")
depends_on("libxcb/1.14-ukkb4rv")
depends_on("xextproto/7.3.0-q4gk4gv")
depends_on("xproto/7.0.31-2wcbwe7")
depends_on("xtrans/1.4.0-czrtbqp")

prepend_path("MANPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/libx11-1.8.4-cxeh2gxzvgcmjafrazgnhglr76lbsvin/share/man", ":")
prepend_path("PKG_CONFIG_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/libx11-1.8.4-cxeh2gxzvgcmjafrazgnhglr76lbsvin/lib/pkgconfig", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/libx11-1.8.4-cxeh2gxzvgcmjafrazgnhglr76lbsvin/.", ":")

