-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2023-05-17 16:15:12.594194
--
-- libxau@1.0.8%gcc@6.4.0 build_system=autotools arch=linux-centos7-broadwell/5jmzqqf
--

whatis([[Name : libxau]])
whatis([[Version : 1.0.8]])
whatis([[Target : broadwell]])
whatis([[Short description : The libXau package contains a library implementing the X11 Authorization Protocol. This is useful for restricting client access to the display.]])

help([[Name   : libxau]])
help([[Version: 1.0.8]])
help([[Target : broadwell]])
help()
help([[The libXau package contains a library implementing the X11 Authorization
Protocol. This is useful for restricting client access to the display.]])


depends_on("xproto/7.0.31-2wcbwe7")

prepend_path("MANPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/libxau-1.0.8-5jmzqqf7t2hqxi4fdd4faqqlnv3vqwsj/share/man", ":")
prepend_path("PKG_CONFIG_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/libxau-1.0.8-5jmzqqf7t2hqxi4fdd4faqqlnv3vqwsj/lib/pkgconfig", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/libxau-1.0.8-5jmzqqf7t2hqxi4fdd4faqqlnv3vqwsj/.", ":")

