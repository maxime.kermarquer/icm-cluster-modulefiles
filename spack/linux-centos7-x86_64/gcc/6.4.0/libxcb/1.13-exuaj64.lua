-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2018-09-19 10:56:59.578876
--
-- libxcb@1.13%gcc@6.4.0 arch=linux-centos7-x86_64 /exuaj64
--

whatis([[Name : libxcb]])
whatis([[Version : 1.13]])
whatis([[Short description : The X protocol C-language Binding (XCB) is a replacement for Xlib featuring a small footprint, latency hiding, direct access to the protocol, improved threading support, and extensibility.]])

help([[The X protocol C-language Binding (XCB) is a replacement for Xlib
featuring a small footprint, latency hiding, direct access to the
protocol, improved threading support, and extensibility.]])



prepend_path("MANPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/libxcb-1.13-exuaj64lecbzyteamkinzkdkw5ij5n34/share/man", ":")
prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/libxcb-1.13-exuaj64lecbzyteamkinzkdkw5ij5n34/lib", ":")
prepend_path("LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/libxcb-1.13-exuaj64lecbzyteamkinzkdkw5ij5n34/lib", ":")
prepend_path("CPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/libxcb-1.13-exuaj64lecbzyteamkinzkdkw5ij5n34/include", ":")
prepend_path("PKG_CONFIG_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/libxcb-1.13-exuaj64lecbzyteamkinzkdkw5ij5n34/lib/pkgconfig", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/libxcb-1.13-exuaj64lecbzyteamkinzkdkw5ij5n34/", ":")

