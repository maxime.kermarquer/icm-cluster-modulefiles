-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2023-05-17 16:17:13.336873
--
-- libxcb@1.14%gcc@6.4.0 build_system=autotools arch=linux-centos7-broadwell/ukkb4rv
--

whatis([[Name : libxcb]])
whatis([[Version : 1.14]])
whatis([[Target : broadwell]])
whatis([[Short description : The X protocol C-language Binding (XCB) is a replacement for Xlib featuring a small footprint, latency hiding, direct access to the protocol, improved threading support, and extensibility.]])

help([[Name   : libxcb]])
help([[Version: 1.14]])
help([[Target : broadwell]])
help()
help([[The X protocol C-language Binding (XCB) is a replacement for Xlib
featuring a small footprint, latency hiding, direct access to the
protocol, improved threading support, and extensibility.]])


depends_on("libpthread-stubs/0.4-elf55rj")
depends_on("libxau/1.0.8-5jmzqqf")
depends_on("libxdmcp/1.1.4-wd4cqvd")
depends_on("xcb-proto/1.15.2-5mhobw4")

prepend_path("MANPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/libxcb-1.14-ukkb4rvh5go26tuukhyvzx4earenljmm/share/man", ":")
prepend_path("PKG_CONFIG_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/libxcb-1.14-ukkb4rvh5go26tuukhyvzx4earenljmm/lib/pkgconfig", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/libxcb-1.14-ukkb4rvh5go26tuukhyvzx4earenljmm/.", ":")

