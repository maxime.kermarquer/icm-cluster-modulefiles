-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2018-09-19 10:56:32.962857
--
-- libxdmcp@1.1.2%gcc@6.4.0 arch=linux-centos7-x86_64 /3u4fvtg
--

whatis([[Name : libxdmcp]])
whatis([[Version : 1.1.2]])
whatis([[Short description : libXdmcp - X Display Manager Control Protocol library.]])

help([[libXdmcp - X Display Manager Control Protocol library.]])



prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/libxdmcp-1.1.2-3u4fvtgtmvksznpnrnyva2cmmbqpd6nj/lib", ":")
prepend_path("LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/libxdmcp-1.1.2-3u4fvtgtmvksznpnrnyva2cmmbqpd6nj/lib", ":")
prepend_path("CPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/libxdmcp-1.1.2-3u4fvtgtmvksznpnrnyva2cmmbqpd6nj/include", ":")
prepend_path("PKG_CONFIG_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/libxdmcp-1.1.2-3u4fvtgtmvksznpnrnyva2cmmbqpd6nj/lib/pkgconfig", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/libxdmcp-1.1.2-3u4fvtgtmvksznpnrnyva2cmmbqpd6nj/", ":")

