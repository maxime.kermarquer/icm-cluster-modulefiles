-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2023-05-17 16:15:00.324431
--
-- libxdmcp@1.1.4%gcc@6.4.0 build_system=autotools arch=linux-centos7-broadwell/wd4cqvd
--

whatis([[Name : libxdmcp]])
whatis([[Version : 1.1.4]])
whatis([[Target : broadwell]])
whatis([[Short description : libXdmcp - X Display Manager Control Protocol library.]])

help([[Name   : libxdmcp]])
help([[Version: 1.1.4]])
help([[Target : broadwell]])
help()
help([[libXdmcp - X Display Manager Control Protocol library.]])


depends_on("libbsd/0.10.0-wog2nt6")
depends_on("xproto/7.0.31-2wcbwe7")

prepend_path("PKG_CONFIG_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/libxdmcp-1.1.4-wd4cqvd6dzkkjhgtekv3whixczsrmh7c/lib/pkgconfig", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/libxdmcp-1.1.4-wd4cqvd6dzkkjhgtekv3whixczsrmh7c/.", ":")

