-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2023-05-17 16:20:00.391025
--
-- libxext@1.3.3%gcc@6.4.0 build_system=autotools arch=linux-centos7-broadwell/fefhmmd
--

whatis([[Name : libxext]])
whatis([[Version : 1.3.3]])
whatis([[Target : broadwell]])
whatis([[Short description : libXext - library for common extensions to the X11 protocol.]])

help([[Name   : libxext]])
help([[Version: 1.3.3]])
help([[Target : broadwell]])
help()
help([[libXext - library for common extensions to the X11 protocol.]])


depends_on("libx11/1.8.4-cxeh2gx")
depends_on("xextproto/7.3.0-q4gk4gv")
depends_on("xproto/7.0.31-2wcbwe7")

prepend_path("MANPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/libxext-1.3.3-fefhmmdjh5tzqr2iyybwawhkygyireym/share/man", ":")
prepend_path("PKG_CONFIG_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/libxext-1.3.3-fefhmmdjh5tzqr2iyybwawhkygyireym/lib/pkgconfig", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/libxext-1.3.3-fefhmmdjh5tzqr2iyybwawhkygyireym/.", ":")
prepend_path("XLOCALEDIR", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/libx11-1.8.4-cxeh2gxzvgcmjafrazgnhglr76lbsvin/share/X11/locale", ":")

