-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2018-09-19 11:08:58.467606
--
-- libxfixes@5.0.2%gcc@6.4.0 arch=linux-centos7-x86_64 /dvlv4rn
--

whatis([[Name : libxfixes]])
whatis([[Version : 5.0.2]])
whatis([[Short description : This package contains header files and documentation for the XFIXES extension. Library and server implementations are separate.]])

help([[This package contains header files and documentation for the XFIXES
extension. Library and server implementations are separate.]])



prepend_path("MANPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/libxfixes-5.0.2-dvlv4rnmz47gnqglyc56yqz72vid7qwf/share/man", ":")
prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/libxfixes-5.0.2-dvlv4rnmz47gnqglyc56yqz72vid7qwf/lib", ":")
prepend_path("LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/libxfixes-5.0.2-dvlv4rnmz47gnqglyc56yqz72vid7qwf/lib", ":")
prepend_path("CPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/libxfixes-5.0.2-dvlv4rnmz47gnqglyc56yqz72vid7qwf/include", ":")
prepend_path("PKG_CONFIG_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/libxfixes-5.0.2-dvlv4rnmz47gnqglyc56yqz72vid7qwf/lib/pkgconfig", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/libxfixes-5.0.2-dvlv4rnmz47gnqglyc56yqz72vid7qwf/", ":")

