-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2023-05-17 16:21:14.814833
--
-- libxft@2.3.2%gcc@6.4.0 build_system=autotools arch=linux-centos7-broadwell/rvgjmvo
--

whatis([[Name : libxft]])
whatis([[Version : 2.3.2]])
whatis([[Target : broadwell]])
whatis([[Short description : X FreeType library.]])

help([[Name   : libxft]])
help([[Version: 2.3.2]])
help([[Target : broadwell]])
help()
help([[X FreeType library. Xft version 2.1 was the first stand alone release of
Xft, a library that connects X applications with the FreeType font
rasterization library. Xft uses fontconfig to locate fonts so it has no
configuration files.]])


depends_on("fontconfig/2.14.2-nuwul4i")
depends_on("freetype/2.11.1-e77ncz4")
depends_on("libx11/1.8.4-cxeh2gx")
depends_on("libxrender/0.9.10-7l6gzpd")

prepend_path("MANPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/libxft-2.3.2-rvgjmvo4znb2pwr4ii4jptmhxgl5bmai/share/man", ":")
prepend_path("PKG_CONFIG_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/libxft-2.3.2-rvgjmvo4znb2pwr4ii4jptmhxgl5bmai/lib/pkgconfig", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/libxft-2.3.2-rvgjmvo4znb2pwr4ii4jptmhxgl5bmai/.", ":")
prepend_path("XLOCALEDIR", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/libx11-1.8.4-cxeh2gxzvgcmjafrazgnhglr76lbsvin/share/X11/locale", ":")

