-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2019-02-06 20:25:02.909455
--
-- libxinerama@1.1.3%gcc@6.4.0 arch=linux-centos7-x86_64 /quyii3n
--

whatis([[Name : libxinerama]])
whatis([[Version : 1.1.3]])
whatis([[Short description : libXinerama - API for Xinerama extension to X11 Protocol.]])

help([[libXinerama - API for Xinerama extension to X11 Protocol.]])



prepend_path("MANPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/libxinerama-1.1.3-quyii3nl54nvlvsqx64fjsahrj3w5fkk/share/man", ":")
prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/libxinerama-1.1.3-quyii3nl54nvlvsqx64fjsahrj3w5fkk/lib", ":")
prepend_path("LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/libxinerama-1.1.3-quyii3nl54nvlvsqx64fjsahrj3w5fkk/lib", ":")
prepend_path("CPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/libxinerama-1.1.3-quyii3nl54nvlvsqx64fjsahrj3w5fkk/include", ":")
prepend_path("PKG_CONFIG_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/libxinerama-1.1.3-quyii3nl54nvlvsqx64fjsahrj3w5fkk/lib/pkgconfig", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/libxinerama-1.1.3-quyii3nl54nvlvsqx64fjsahrj3w5fkk/", ":")

