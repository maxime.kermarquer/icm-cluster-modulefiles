-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2019-10-07 10:52:34.216974
--
-- libxkbfile@1.0.9%gcc@6.4.0 arch=linux-centos7-x86_64 /j252udb
--

whatis([[Name : libxkbfile]])
whatis([[Version : 1.0.9]])
whatis([[Short description : XKB file handling routines.]])

help([[XKB file handling routines.]])



prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/libxkbfile-1.0.9-j252udbkjgk6bbd2zj7z2pkk222nv6cn/lib", ":")
prepend_path("LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/libxkbfile-1.0.9-j252udbkjgk6bbd2zj7z2pkk222nv6cn/lib", ":")
prepend_path("CPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/libxkbfile-1.0.9-j252udbkjgk6bbd2zj7z2pkk222nv6cn/include", ":")
prepend_path("PKG_CONFIG_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/libxkbfile-1.0.9-j252udbkjgk6bbd2zj7z2pkk222nv6cn/lib/pkgconfig", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/libxkbfile-1.0.9-j252udbkjgk6bbd2zj7z2pkk222nv6cn/", ":")

