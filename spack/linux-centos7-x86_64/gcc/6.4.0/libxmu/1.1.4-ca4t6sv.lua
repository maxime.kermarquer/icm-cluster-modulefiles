-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2023-05-17 16:21:41.264319
--
-- libxmu@1.1.4%gcc@6.4.0 build_system=autotools arch=linux-centos7-broadwell/ca4t6sv
--

whatis([[Name : libxmu]])
whatis([[Version : 1.1.4]])
whatis([[Target : broadwell]])
whatis([[Short description : This library contains miscellaneous utilities and is not part of the Xlib standard. It contains routines which only use public interfaces so that it may be layered on top of any proprietary implementation of Xlib or Xt.]])

help([[Name   : libxmu]])
help([[Version: 1.1.4]])
help([[Target : broadwell]])
help()
help([[This library contains miscellaneous utilities and is not part of the
Xlib standard. It contains routines which only use public interfaces so
that it may be layered on top of any proprietary implementation of Xlib
or Xt.]])


depends_on("libx11/1.8.4-cxeh2gx")
depends_on("libxext/1.3.3-fefhmmd")
depends_on("libxt/1.1.5-3vk6bb7")
depends_on("xextproto/7.3.0-q4gk4gv")

prepend_path("PKG_CONFIG_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/libxmu-1.1.4-ca4t6svgwaqk35m5vmjqjhw7hjnif4ls/lib/pkgconfig", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/libxmu-1.1.4-ca4t6svgwaqk35m5vmjqjhw7hjnif4ls/.", ":")
prepend_path("XLOCALEDIR", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/libx11-1.8.4-cxeh2gxzvgcmjafrazgnhglr76lbsvin/share/X11/locale", ":")

