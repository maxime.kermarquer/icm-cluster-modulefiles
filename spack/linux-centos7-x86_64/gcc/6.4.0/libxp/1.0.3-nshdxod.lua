-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2019-10-25 17:05:07.368980
--
-- libxp@1.0.3%gcc@6.4.0 arch=linux-centos7-x86_64 /nshdxod
--

whatis([[Name : libxp]])
whatis([[Version : 1.0.3]])
whatis([[Short description : libXp - X Print Client Library.]])

help([[libXp - X Print Client Library.]])



prepend_path("MANPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/libxp-1.0.3-nshdxodp4gyb735bcvpb4yzemrrd3m2e/share/man", ":")
prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/libxp-1.0.3-nshdxodp4gyb735bcvpb4yzemrrd3m2e/lib", ":")
prepend_path("LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/libxp-1.0.3-nshdxodp4gyb735bcvpb4yzemrrd3m2e/lib", ":")
prepend_path("PKG_CONFIG_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/libxp-1.0.3-nshdxodp4gyb735bcvpb4yzemrrd3m2e/lib/pkgconfig", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/libxp-1.0.3-nshdxodp4gyb735bcvpb4yzemrrd3m2e/", ":")

