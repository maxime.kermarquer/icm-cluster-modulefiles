-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2019-03-20 14:50:53.815353
--
-- libxrandr@1.5.0%gcc@6.4.0 arch=linux-centos7-x86_64 /h3w7kww
--

whatis([[Name : libxrandr]])
whatis([[Version : 1.5.0]])
whatis([[Short description : libXrandr - X Resize, Rotate and Reflection extension library.]])

help([[libXrandr - X Resize, Rotate and Reflection extension library.]])



prepend_path("MANPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/libxrandr-1.5.0-h3w7kwwbjvvwlmzhjwnfjixja3rxotau/share/man", ":")
prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/libxrandr-1.5.0-h3w7kwwbjvvwlmzhjwnfjixja3rxotau/lib", ":")
prepend_path("LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/libxrandr-1.5.0-h3w7kwwbjvvwlmzhjwnfjixja3rxotau/lib", ":")
prepend_path("CPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/libxrandr-1.5.0-h3w7kwwbjvvwlmzhjwnfjixja3rxotau/include", ":")
prepend_path("PKG_CONFIG_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/libxrandr-1.5.0-h3w7kwwbjvvwlmzhjwnfjixja3rxotau/lib/pkgconfig", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/libxrandr-1.5.0-h3w7kwwbjvvwlmzhjwnfjixja3rxotau/", ":")

