-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2023-05-17 16:20:16.557801
--
-- libxrender@0.9.10%gcc@6.4.0 build_system=autotools arch=linux-centos7-broadwell/7l6gzpd
--

whatis([[Name : libxrender]])
whatis([[Version : 0.9.10]])
whatis([[Target : broadwell]])
whatis([[Short description : libXrender - library for the Render Extension to the X11 protocol.]])

help([[Name   : libxrender]])
help([[Version: 0.9.10]])
help([[Target : broadwell]])
help()
help([[libXrender - library for the Render Extension to the X11 protocol.]])


depends_on("libx11/1.8.4-cxeh2gx")
depends_on("renderproto/0.11.1-neuwjvx")

prepend_path("PKG_CONFIG_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/libxrender-0.9.10-7l6gzpdoiqehyg2k74no2zekkim57jkr/lib/pkgconfig", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/libxrender-0.9.10-7l6gzpdoiqehyg2k74no2zekkim57jkr/.", ":")
prepend_path("XLOCALEDIR", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/libx11-1.8.4-cxeh2gxzvgcmjafrazgnhglr76lbsvin/share/X11/locale", ":")

