-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2023-05-17 16:20:55.551409
--
-- libxscrnsaver@1.2.2%gcc@6.4.0 build_system=autotools arch=linux-centos7-broadwell/3xitosg
--

whatis([[Name : libxscrnsaver]])
whatis([[Version : 1.2.2]])
whatis([[Target : broadwell]])
whatis([[Short description : XScreenSaver - X11 Screen Saver extension client library]])

help([[Name   : libxscrnsaver]])
help([[Version: 1.2.2]])
help([[Target : broadwell]])
help()
help([[XScreenSaver - X11 Screen Saver extension client library]])


depends_on("libx11/1.8.4-cxeh2gx")
depends_on("libxext/1.3.3-fefhmmd")
depends_on("scrnsaverproto/1.2.2-24z6w7s")
depends_on("xextproto/7.3.0-q4gk4gv")

prepend_path("MANPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/libxscrnsaver-1.2.2-3xitosgnbekopy7miokwl537dfpnvlrj/share/man", ":")
prepend_path("PKG_CONFIG_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/libxscrnsaver-1.2.2-3xitosgnbekopy7miokwl537dfpnvlrj/lib/pkgconfig", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/libxscrnsaver-1.2.2-3xitosgnbekopy7miokwl537dfpnvlrj/.", ":")
prepend_path("XLOCALEDIR", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/libx11-1.8.4-cxeh2gxzvgcmjafrazgnhglr76lbsvin/share/X11/locale", ":")

