-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2019-10-24 09:13:51.818138
--
-- libxscrnsaver@1.2.2%gcc@6.4.0 arch=linux-centos7-x86_64 /tktn3ij
--

whatis([[Name : libxscrnsaver]])
whatis([[Version : 1.2.2]])
whatis([[Short description : XScreenSaver - X11 Screen Saver extension client library]])

help([[XScreenSaver - X11 Screen Saver extension client library]])



prepend_path("MANPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/libxscrnsaver-1.2.2-tktn3ijo4vq5touspuf7hzzn2jylkpb5/share/man", ":")
prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/libxscrnsaver-1.2.2-tktn3ijo4vq5touspuf7hzzn2jylkpb5/lib", ":")
prepend_path("LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/libxscrnsaver-1.2.2-tktn3ijo4vq5touspuf7hzzn2jylkpb5/lib", ":")
prepend_path("CPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/libxscrnsaver-1.2.2-tktn3ijo4vq5touspuf7hzzn2jylkpb5/include", ":")
prepend_path("PKG_CONFIG_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/libxscrnsaver-1.2.2-tktn3ijo4vq5touspuf7hzzn2jylkpb5/lib/pkgconfig", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/libxscrnsaver-1.2.2-tktn3ijo4vq5touspuf7hzzn2jylkpb5/", ":")

