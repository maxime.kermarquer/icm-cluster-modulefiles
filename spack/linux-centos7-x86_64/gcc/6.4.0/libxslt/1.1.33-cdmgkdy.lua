-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2021-11-30 16:35:04.747788
--
-- libxslt@1.1.33%gcc@6.4.0+crypto~python arch=linux-centos7-broadwell/cdmgkdy
--

whatis([[Name : libxslt]])
whatis([[Version : 1.1.33]])
whatis([[Target : broadwell]])
whatis([[Short description : Libxslt is the XSLT C library developed for the GNOME project. XSLT itself is a an XML language to define transformation for XML. Libxslt is based on libxml2 the XML C library developed for the GNOME project. It also implements most of the EXSLT set of processor-portable extensions functions and some of Saxon's evaluate and expressions extensions.]])
whatis([[Configure options : --with-crypto --without-python]])

help([[Libxslt is the XSLT C library developed for the GNOME project. XSLT
itself is a an XML language to define transformation for XML. Libxslt is
based on libxml2 the XML C library developed for the GNOME project. It
also implements most of the EXSLT set of processor-portable extensions
functions and some of Saxon's evaluate and expressions extensions.]])



prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/libxslt-1.1.33-cdmgkdyanlpokxhes53sjw7ehscmvh5k/lib", ":")
prepend_path("LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/libxslt-1.1.33-cdmgkdyanlpokxhes53sjw7ehscmvh5k/lib", ":")
prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/libxslt-1.1.33-cdmgkdyanlpokxhes53sjw7ehscmvh5k/bin", ":")
prepend_path("MANPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/libxslt-1.1.33-cdmgkdyanlpokxhes53sjw7ehscmvh5k/share/man", ":")
prepend_path("ACLOCAL_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/libxslt-1.1.33-cdmgkdyanlpokxhes53sjw7ehscmvh5k/share/aclocal", ":")
prepend_path("C_INCLUDE_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/libxslt-1.1.33-cdmgkdyanlpokxhes53sjw7ehscmvh5k/include", ":")
prepend_path("CPLUS_INCLUDE_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/libxslt-1.1.33-cdmgkdyanlpokxhes53sjw7ehscmvh5k/include", ":")
prepend_path("INCLUDE", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/libxslt-1.1.33-cdmgkdyanlpokxhes53sjw7ehscmvh5k/include", ":")
prepend_path("PKG_CONFIG_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/libxslt-1.1.33-cdmgkdyanlpokxhes53sjw7ehscmvh5k/lib/pkgconfig", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/libxslt-1.1.33-cdmgkdyanlpokxhes53sjw7ehscmvh5k/", ":")

