-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2023-05-17 16:20:38.388767
--
-- libxt@1.1.5%gcc@6.4.0 build_system=autotools arch=linux-centos7-broadwell/3vk6bb7
--

whatis([[Name : libxt]])
whatis([[Version : 1.1.5]])
whatis([[Target : broadwell]])
whatis([[Short description : libXt - X Toolkit Intrinsics library.]])

help([[Name   : libxt]])
help([[Version: 1.1.5]])
help([[Target : broadwell]])
help()
help([[libXt - X Toolkit Intrinsics library.]])


depends_on("kbproto/1.0.7-wfh44ru")
depends_on("libice/1.0.9-l5i772w")
depends_on("libsm/1.2.3-khri7aw")
depends_on("libx11/1.8.4-cxeh2gx")
depends_on("xproto/7.0.31-2wcbwe7")

prepend_path("MANPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/libxt-1.1.5-3vk6bb7au3pnipiqfkitda5djyhd5k3c/share/man", ":")
prepend_path("PKG_CONFIG_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/libxt-1.1.5-3vk6bb7au3pnipiqfkitda5djyhd5k3c/lib/pkgconfig", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/libxt-1.1.5-3vk6bb7au3pnipiqfkitda5djyhd5k3c/.", ":")
prepend_path("XLOCALEDIR", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/libx11-1.8.4-cxeh2gxzvgcmjafrazgnhglr76lbsvin/share/X11/locale", ":")

