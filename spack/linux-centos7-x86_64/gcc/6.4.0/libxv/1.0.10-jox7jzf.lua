-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2018-09-19 11:09:39.417383
--
-- libxv@1.0.10%gcc@6.4.0 arch=linux-centos7-x86_64 /jox7jzf
--

whatis([[Name : libxv]])
whatis([[Version : 1.0.10]])
whatis([[Short description : libXv - library for the X Video (Xv) extension to the X Window System.]])

help([[libXv - library for the X Video (Xv) extension to the X Window System.]])



prepend_path("MANPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/libxv-1.0.10-jox7jzf3kni3kjhypfrphp366x7grwhs/share/man", ":")
prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/libxv-1.0.10-jox7jzf3kni3kjhypfrphp366x7grwhs/lib", ":")
prepend_path("LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/libxv-1.0.10-jox7jzf3kni3kjhypfrphp366x7grwhs/lib", ":")
prepend_path("CPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/libxv-1.0.10-jox7jzf3kni3kjhypfrphp366x7grwhs/include", ":")
prepend_path("PKG_CONFIG_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/libxv-1.0.10-jox7jzf3kni3kjhypfrphp366x7grwhs/lib/pkgconfig", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/libxv-1.0.10-jox7jzf3kni3kjhypfrphp366x7grwhs/", ":")

