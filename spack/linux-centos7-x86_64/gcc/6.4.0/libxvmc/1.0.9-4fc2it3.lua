-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2018-09-19 11:09:49.763358
--
-- libxvmc@1.0.9%gcc@6.4.0 arch=linux-centos7-x86_64 /4fc2it3
--

whatis([[Name : libxvmc]])
whatis([[Version : 1.0.9]])
whatis([[Short description : X.org libXvMC library.]])

help([[X.org libXvMC library.]])



prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/libxvmc-1.0.9-4fc2it37pfmsmlgxvmmenv4ejewx2rrc/lib", ":")
prepend_path("LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/libxvmc-1.0.9-4fc2it37pfmsmlgxvmmenv4ejewx2rrc/lib", ":")
prepend_path("CPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/libxvmc-1.0.9-4fc2it37pfmsmlgxvmmenv4ejewx2rrc/include", ":")
prepend_path("PKG_CONFIG_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/libxvmc-1.0.9-4fc2it37pfmsmlgxvmmenv4ejewx2rrc/lib/pkgconfig", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/libxvmc-1.0.9-4fc2it37pfmsmlgxvmmenv4ejewx2rrc/", ":")

