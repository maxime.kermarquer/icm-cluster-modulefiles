-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2021-01-13 17:09:13.392670
--
-- lvm2@2.03.05%gcc@6.4.0+pkgconfig arch=linux-centos7-broadwell/l3nqqjo
--

whatis([[Name : lvm2]])
whatis([[Version : 2.03.05]])
whatis([[Target : broadwell]])
whatis([[Short description : LVM2 is the userspace toolset that provides logical volume management facilities on linux.]])
whatis([[Configure options : --with-confdir=/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/lvm2-2.03.05-l3nqqjofirtprccnconiecjd73rc46mt/etc --with-default-system-dir=/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/lvm2-2.03.05-l3nqqjofirtprccnconiecjd73rc46mt/etc/lvm --enable-pkgconfig]])

help([[LVM2 is the userspace toolset that provides logical volume management
facilities on linux. To use it you need 3 things: device-mapper in your
kernel, the userspace device-mapper support library (libdevmapper) and
the userspace LVM2 tools (dmsetup). These userspace components, and
associated header files, are provided by this package. See
http://sources.redhat.com/dm/ for additional information about the
device-mapper kernel and userspace components.]])



prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/lvm2-2.03.05-l3nqqjofirtprccnconiecjd73rc46mt/lib", ":")
prepend_path("LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/lvm2-2.03.05-l3nqqjofirtprccnconiecjd73rc46mt/lib", ":")
prepend_path("MANPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/lvm2-2.03.05-l3nqqjofirtprccnconiecjd73rc46mt/share/man", ":")
prepend_path("C_INCLUDE_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/lvm2-2.03.05-l3nqqjofirtprccnconiecjd73rc46mt/include", ":")
prepend_path("CPLUS_INCLUDE_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/lvm2-2.03.05-l3nqqjofirtprccnconiecjd73rc46mt/include", ":")
prepend_path("INCLUDE", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/lvm2-2.03.05-l3nqqjofirtprccnconiecjd73rc46mt/include", ":")
prepend_path("PKG_CONFIG_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/lvm2-2.03.05-l3nqqjofirtprccnconiecjd73rc46mt/lib/pkgconfig", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/lvm2-2.03.05-l3nqqjofirtprccnconiecjd73rc46mt/", ":")

