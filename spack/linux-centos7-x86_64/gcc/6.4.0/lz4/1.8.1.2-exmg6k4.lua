-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2020-12-12 16:39:42.490088
--
-- lz4@1.8.1.2%gcc@6.4.0 arch=linux-centos7-x86_64 /exmg6k4
--

whatis([[Name : lz4]])
whatis([[Version : 1.8.1.2]])
whatis([[Short description : LZ4 is lossless compression algorithm, providing compression speed at 400 MB/s per core, scalable with multi-cores CPU. It also features an extremely fast decoder, with speed in multiple GB/s per core, typically reaching RAM speed limits on multi-core systems.]])

help([[LZ4 is lossless compression algorithm, providing compression speed at
400 MB/s per core, scalable with multi-cores CPU. It also features an
extremely fast decoder, with speed in multiple GB/s per core, typically
reaching RAM speed limits on multi-core systems.]])



prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/lz4-1.8.1.2-exmg6k46yolwv3gytch26q7o3u75jplo/bin", ":")
prepend_path("MANPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/lz4-1.8.1.2-exmg6k46yolwv3gytch26q7o3u75jplo/share/man", ":")
prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/lz4-1.8.1.2-exmg6k46yolwv3gytch26q7o3u75jplo/lib", ":")
prepend_path("LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/lz4-1.8.1.2-exmg6k46yolwv3gytch26q7o3u75jplo/lib", ":")
prepend_path("CPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/lz4-1.8.1.2-exmg6k46yolwv3gytch26q7o3u75jplo/include", ":")
prepend_path("PKG_CONFIG_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/lz4-1.8.1.2-exmg6k46yolwv3gytch26q7o3u75jplo/lib/pkgconfig", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/lz4-1.8.1.2-exmg6k46yolwv3gytch26q7o3u75jplo/", ":")

