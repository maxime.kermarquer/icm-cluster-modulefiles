-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2021-01-13 16:57:49.159956
--
-- lz4@1.9.2%gcc@6.4.0 arch=linux-centos7-broadwell/pumyq77
--

whatis([[Name : lz4]])
whatis([[Version : 1.9.2]])
whatis([[Target : broadwell]])
whatis([[Short description : LZ4 is lossless compression algorithm, providing compression speed at 400 MB/s per core, scalable with multi-cores CPU. It also features an extremely fast decoder, with speed in multiple GB/s per core, typically reaching RAM speed limits on multi-core systems.]])

help([[LZ4 is lossless compression algorithm, providing compression speed at
400 MB/s per core, scalable with multi-cores CPU. It also features an
extremely fast decoder, with speed in multiple GB/s per core, typically
reaching RAM speed limits on multi-core systems.]])



prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/lz4-1.9.2-pumyq77l5og3xr3bouw3bdngk2gjc4jo/lib", ":")
prepend_path("LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/lz4-1.9.2-pumyq77l5og3xr3bouw3bdngk2gjc4jo/lib", ":")
prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/lz4-1.9.2-pumyq77l5og3xr3bouw3bdngk2gjc4jo/bin", ":")
prepend_path("MANPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/lz4-1.9.2-pumyq77l5og3xr3bouw3bdngk2gjc4jo/share/man", ":")
prepend_path("C_INCLUDE_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/lz4-1.9.2-pumyq77l5og3xr3bouw3bdngk2gjc4jo/include", ":")
prepend_path("CPLUS_INCLUDE_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/lz4-1.9.2-pumyq77l5og3xr3bouw3bdngk2gjc4jo/include", ":")
prepend_path("INCLUDE", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/lz4-1.9.2-pumyq77l5og3xr3bouw3bdngk2gjc4jo/include", ":")
prepend_path("PKG_CONFIG_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/lz4-1.9.2-pumyq77l5og3xr3bouw3bdngk2gjc4jo/lib/pkgconfig", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/lz4-1.9.2-pumyq77l5og3xr3bouw3bdngk2gjc4jo/", ":")

