-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2019-05-15 14:23:47.318446
--
-- lzma@4.32.7%gcc@6.4.0 arch=linux-centos7-x86_64 /w225jl2
--

whatis([[Name : lzma]])
whatis([[Version : 4.32.7]])
whatis([[Short description : LZMA Utils are legacy data compression software with high compression ratio. LZMA Utils are no longer developed, although critical bugs may be fixed as long as fixing them doesn't require huge changes to the code.]])

help([[LZMA Utils are legacy data compression software with high compression
ratio. LZMA Utils are no longer developed, although critical bugs may be
fixed as long as fixing them doesn't require huge changes to the code.
Users of LZMA Utils should move to XZ Utils. XZ Utils support the legacy
.lzma format used by LZMA Utils, and can also emulate the command line
tools of LZMA Utils. This should make transition from LZMA Utils to XZ
Utils relatively easy.]])



prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/lzma-4.32.7-w225jl2sc5bi4ukayrhgrlkoe4abdbl4/bin", ":")
prepend_path("MANPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/lzma-4.32.7-w225jl2sc5bi4ukayrhgrlkoe4abdbl4/share/man", ":")
prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/lzma-4.32.7-w225jl2sc5bi4ukayrhgrlkoe4abdbl4/lib", ":")
prepend_path("LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/lzma-4.32.7-w225jl2sc5bi4ukayrhgrlkoe4abdbl4/lib", ":")
prepend_path("CPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/lzma-4.32.7-w225jl2sc5bi4ukayrhgrlkoe4abdbl4/include", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/lzma-4.32.7-w225jl2sc5bi4ukayrhgrlkoe4abdbl4/", ":")

