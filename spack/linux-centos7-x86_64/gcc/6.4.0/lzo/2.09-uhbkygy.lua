-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2020-12-12 16:40:07.312194
--
-- lzo@2.09%gcc@6.4.0 arch=linux-centos7-x86_64 /uhbkygy
--

whatis([[Name : lzo]])
whatis([[Version : 2.09]])
whatis([[Short description : Real-time data compression library]])
whatis([[Configure options : --disable-dependency-tracking --enable-shared]])

help([[Real-time data compression library]])



prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/lzo-2.09-uhbkygytrdda75utxdn2aghbdueol3ep/lib", ":")
prepend_path("LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/lzo-2.09-uhbkygytrdda75utxdn2aghbdueol3ep/lib", ":")
prepend_path("CPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/lzo-2.09-uhbkygytrdda75utxdn2aghbdueol3ep/include", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/lzo-2.09-uhbkygytrdda75utxdn2aghbdueol3ep/", ":")

