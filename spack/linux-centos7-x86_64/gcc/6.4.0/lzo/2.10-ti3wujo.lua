-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2021-01-13 19:11:32.296055
--
-- lzo@2.10%gcc@6.4.0 arch=linux-centos7-broadwell/ti3wujo
--

whatis([[Name : lzo]])
whatis([[Version : 2.10]])
whatis([[Target : broadwell]])
whatis([[Short description : Real-time data compression library]])
whatis([[Configure options : --disable-dependency-tracking --enable-shared]])

help([[Real-time data compression library]])



prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/lzo-2.10-ti3wujorunztok6i762sow74hatedpse/lib", ":")
prepend_path("LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/lzo-2.10-ti3wujorunztok6i762sow74hatedpse/lib", ":")
prepend_path("C_INCLUDE_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/lzo-2.10-ti3wujorunztok6i762sow74hatedpse/include", ":")
prepend_path("CPLUS_INCLUDE_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/lzo-2.10-ti3wujorunztok6i762sow74hatedpse/include", ":")
prepend_path("INCLUDE", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/lzo-2.10-ti3wujorunztok6i762sow74hatedpse/include", ":")
prepend_path("PKG_CONFIG_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/lzo-2.10-ti3wujorunztok6i762sow74hatedpse/lib/pkgconfig", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/lzo-2.10-ti3wujorunztok6i762sow74hatedpse/", ":")

