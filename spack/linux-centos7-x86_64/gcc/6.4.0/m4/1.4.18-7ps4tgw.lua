-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2018-09-18 16:53:24.152772
--
-- m4@1.4.18%gcc@6.4.0 patches=3877ab548f88597ab2327a2230ee048d2d07ace1062efe81fc92e91b7f39cd00,c0a408fbffb7255fcc75e26bd8edab116fc81d216bfd18b473668b7739a4158e +sigsegv arch=linux-centos7-x86_64 /7ps4tgw
--

whatis([[Name : m4]])
whatis([[Version : 1.4.18]])
whatis([[Short description : GNU M4 is an implementation of the traditional Unix macro processor.]])
whatis([[Configure options : --enable-c++ --with-libsigsegv-prefix=/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/libsigsegv-2.11-c7pa2pydcb5hlo6ok3ermvmtybifjqe2]])

help([[GNU M4 is an implementation of the traditional Unix macro processor.]])



prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/m4-1.4.18-7ps4tgwzzlvveg4w43jhgrztcttxjrrm/bin", ":")
prepend_path("MANPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/m4-1.4.18-7ps4tgwzzlvveg4w43jhgrztcttxjrrm/share/man", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/m4-1.4.18-7ps4tgwzzlvveg4w43jhgrztcttxjrrm/", ":")

