-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2021-01-13 16:58:53.683514
--
-- m4@1.4.18%gcc@6.4.0+sigsegv patches=3877ab548f88597ab2327a2230ee048d2d07ace1062efe81fc92e91b7f39cd00,fc9b61654a3ba1a8d6cd78ce087e7c96366c290bc8d2c299f09828d793b853c8 arch=linux-centos7-broadwell/cwecvas
--

whatis([[Name : m4]])
whatis([[Version : 1.4.18]])
whatis([[Target : broadwell]])
whatis([[Short description : GNU M4 is an implementation of the traditional Unix macro processor.]])
whatis([[Configure options : --enable-c++ --with-libsigsegv-prefix=/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/libsigsegv-2.12-7m3tio72mk3mnwef66ytv6f6uudnnhfj]])

help([[GNU M4 is an implementation of the traditional Unix macro processor.]])



prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/m4-1.4.18-cwecvasekua767d2t457xe43lvbw2nss/bin", ":")
prepend_path("MANPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/m4-1.4.18-cwecvasekua767d2t457xe43lvbw2nss/share/man", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/m4-1.4.18-cwecvasekua767d2t457xe43lvbw2nss/", ":")

