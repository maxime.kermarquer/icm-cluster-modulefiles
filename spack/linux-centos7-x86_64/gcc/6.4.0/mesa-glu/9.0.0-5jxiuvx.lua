-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2018-09-19 11:16:17.009238
--
-- mesa-glu@9.0.0%gcc@6.4.0+mesa arch=linux-centos7-x86_64 /5jxiuvx
--

whatis([[Name : mesa-glu]])
whatis([[Version : 9.0.0]])
whatis([[Short description : This package provides the Mesa OpenGL Utility library.]])

help([[This package provides the Mesa OpenGL Utility library.]])



prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/mesa-glu-9.0.0-5jxiuvx3sxefprh4bzspdzujq547bd7u/lib", ":")
prepend_path("LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/mesa-glu-9.0.0-5jxiuvx3sxefprh4bzspdzujq547bd7u/lib", ":")
prepend_path("CPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/mesa-glu-9.0.0-5jxiuvx3sxefprh4bzspdzujq547bd7u/include", ":")
prepend_path("PKG_CONFIG_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/mesa-glu-9.0.0-5jxiuvx3sxefprh4bzspdzujq547bd7u/lib/pkgconfig", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/mesa-glu-9.0.0-5jxiuvx3sxefprh4bzspdzujq547bd7u/", ":")
prepend_path("CPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/libxml2-2.9.8-m3rdnxm66wlzs5gqsm5pqlzqvideutnk/include/libxml2", ":")

