-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2018-09-19 11:15:58.139731
--
-- mesa@18.1.2%gcc@6.4.0~hwrender~llvm+swrender arch=linux-centos7-x86_64 /rwyp6rj
--

whatis([[Name : mesa]])
whatis([[Version : 18.1.2]])
whatis([[Short description : Mesa is an open-source implementation of the OpenGL specification - a system for rendering interactive 3D graphics.]])
whatis([[Configure options : --enable-glx --enable-glx-tls --disable-osmesa --enable-gallium-osmesa --enable-texture-float --disable-xa --disable-dri --disable-dri3 --disable-egl --disable-gbm --disable-xvmc --with-platforms=x11 --with-gallium-drivers=swrast LIBS=-lrt]])

help([[Mesa is an open-source implementation of the OpenGL specification - a
system for rendering interactive 3D graphics.]])



prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/mesa-18.1.2-rwyp6rjpfmwejhoakn4wue4ewj3vx4hr/lib", ":")
prepend_path("LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/mesa-18.1.2-rwyp6rjpfmwejhoakn4wue4ewj3vx4hr/lib", ":")
prepend_path("CPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/mesa-18.1.2-rwyp6rjpfmwejhoakn4wue4ewj3vx4hr/include", ":")
prepend_path("PKG_CONFIG_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/mesa-18.1.2-rwyp6rjpfmwejhoakn4wue4ewj3vx4hr/lib/pkgconfig", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/mesa-18.1.2-rwyp6rjpfmwejhoakn4wue4ewj3vx4hr/", ":")
prepend_path("CPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/libxml2-2.9.8-m3rdnxm66wlzs5gqsm5pqlzqvideutnk/include/libxml2", ":")

