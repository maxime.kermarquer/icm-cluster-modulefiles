-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2018-12-05 18:47:39.434739
--
-- meson@0.42.0%gcc@6.4.0+ninjabuild arch=linux-centos7-x86_64 /54idvms
--

whatis([[Name : meson]])
whatis([[Version : 0.42.0]])
whatis([[Short description : Meson is a portable open source build system meant to be both extremely fast, and as user friendly as possible.]])

help([[Meson is a portable open source build system meant to be both extremely
fast, and as user friendly as possible.]])



prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/meson-0.42.0-54idvmsb332e3hkyja3kdn7lnmopsrgi/bin", ":")
prepend_path("MANPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/meson-0.42.0-54idvmsb332e3hkyja3kdn7lnmopsrgi/share/man", ":")
prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/meson-0.42.0-54idvmsb332e3hkyja3kdn7lnmopsrgi/lib", ":")
prepend_path("LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/meson-0.42.0-54idvmsb332e3hkyja3kdn7lnmopsrgi/lib", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/meson-0.42.0-54idvmsb332e3hkyja3kdn7lnmopsrgi/", ":")
prepend_path("PYTHONPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/meson-0.42.0-54idvmsb332e3hkyja3kdn7lnmopsrgi/lib/python3.7/site-packages", ":")

