-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2019-02-05 18:29:59.044739
--
-- meson@0.46.0%gcc@6.4.0+ninjabuild arch=linux-centos7-x86_64 /5tad346
--

whatis([[Name : meson]])
whatis([[Version : 0.46.0]])
whatis([[Short description : Meson is a portable open source build system meant to be both extremely fast, and as user friendly as possible.]])

help([[Meson is a portable open source build system meant to be both extremely
fast, and as user friendly as possible.]])



prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/meson-0.46.0-5tad3464mf4ctkmadeec2zdgn4tbixr6/bin", ":")
prepend_path("MANPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/meson-0.46.0-5tad3464mf4ctkmadeec2zdgn4tbixr6/share/man", ":")
prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/meson-0.46.0-5tad3464mf4ctkmadeec2zdgn4tbixr6/lib", ":")
prepend_path("LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/meson-0.46.0-5tad3464mf4ctkmadeec2zdgn4tbixr6/lib", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/meson-0.46.0-5tad3464mf4ctkmadeec2zdgn4tbixr6/", ":")
prepend_path("PYTHONPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/meson-0.46.0-5tad3464mf4ctkmadeec2zdgn4tbixr6/lib/python3.7/site-packages", ":")

