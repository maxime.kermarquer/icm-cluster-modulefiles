-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2021-01-13 17:49:56.533847
--
-- meson@0.56.0%gcc@6.4.0 patches=7a7ff3bffb5c1996faf178da6765d578394624edb96fe9297bf21f2e3cd9ebda arch=linux-centos7-broadwell/mryb4g4
--

whatis([[Name : meson]])
whatis([[Version : 0.56.0]])
whatis([[Target : broadwell]])
whatis([[Short description : Meson is a portable open source build system meant to be both extremely fast, and as user friendly as possible.]])

help([[Meson is a portable open source build system meant to be both extremely
fast, and as user friendly as possible.]])



prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/meson-0.56.0-mryb4g4rkot4ueruwsedlze7s7fhjcer/lib", ":")
prepend_path("LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/meson-0.56.0-mryb4g4rkot4ueruwsedlze7s7fhjcer/lib", ":")
prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/meson-0.56.0-mryb4g4rkot4ueruwsedlze7s7fhjcer/bin", ":")
prepend_path("MANPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/meson-0.56.0-mryb4g4rkot4ueruwsedlze7s7fhjcer/share/man", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/meson-0.56.0-mryb4g4rkot4ueruwsedlze7s7fhjcer/", ":")
prepend_path("PYTHONPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/meson-0.56.0-mryb4g4rkot4ueruwsedlze7s7fhjcer/lib/python3.8/site-packages:/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/py-setuptools-50.3.2-ynbx5if5cmwdxitjjakrje4m4d3ni2zz/lib/python3.8/site-packages", ":")

