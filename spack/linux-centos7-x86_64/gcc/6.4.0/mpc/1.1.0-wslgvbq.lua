-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2023-05-04 12:03:38.443376
--
-- mpc@1.1.0%gcc@6.4.0 build_system=autotools libs=shared,static arch=linux-centos7-broadwell/wslgvbq
--

whatis([[Name : mpc]])
whatis([[Version : 1.1.0]])
whatis([[Target : broadwell]])
whatis([[Short description : Gnu Mpc is a C library for the arithmetic of complex numbers with arbitrarily high precision and correct rounding of the result.]])
whatis([[Configure options : --with-mpfr=/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/mpfr-3.1.6-qmjno7t5g4kmdenkxyoohtw6uma3ptze --with-gmp=/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/gmp-6.1.2-twibrqnq6p2jn54n2npl2ebv5p7aetro --enable-shared --enable-static]])

help([[Name   : mpc]])
help([[Version: 1.1.0]])
help([[Target : broadwell]])
help()
help([[Gnu Mpc is a C library for the arithmetic of complex numbers with
arbitrarily high precision and correct rounding of the result.]])


depends_on("gmp/6.1.2-twibrqn")
depends_on("mpfr/3.1.6-qmjno7t")

prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/mpc-1.1.0-wslgvbqvayynzlxha73cemtfibqhkwu5/.", ":")

