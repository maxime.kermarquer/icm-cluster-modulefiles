-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2023-05-04 12:02:19.946446
--
-- mpfr@3.1.6%gcc@6.4.0 build_system=autotools libs=shared,static patches=7a6dd71 arch=linux-centos7-broadwell/qmjno7t
--

whatis([[Name : mpfr]])
whatis([[Version : 3.1.6]])
whatis([[Target : broadwell]])
whatis([[Short description : The MPFR library is a C library for multiple-precision floating-point computations with correct rounding.]])
whatis([[Configure options : --with-gmp=/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/gmp-6.1.2-twibrqnq6p2jn54n2npl2ebv5p7aetro --enable-shared --enable-static --with-pic]])

help([[Name   : mpfr]])
help([[Version: 3.1.6]])
help([[Target : broadwell]])
help()
help([[The MPFR library is a C library for multiple-precision floating-point
computations with correct rounding.]])


depends_on("gmp/6.1.2-twibrqn")

prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/mpfr-3.1.6-qmjno7t5g4kmdenkxyoohtw6uma3ptze/.", ":")

