-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2021-01-13 17:52:40.585770
--
-- mpfr@4.0.2%gcc@6.4.0 patches=3f80b836948aa96f8d1cb9cc7f3f55973f19285482a96f9a4e1623d460bcccf0 arch=linux-centos7-broadwell/pk3b4ab
--

whatis([[Name : mpfr]])
whatis([[Version : 4.0.2]])
whatis([[Target : broadwell]])
whatis([[Short description : The MPFR library is a C library for multiple-precision floating-point computations with correct rounding.]])
whatis([[Configure options : --with-gmp=/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/gmp-6.1.2-twibrqnq6p2jn54n2npl2ebv5p7aetro]])

help([[The MPFR library is a C library for multiple-precision floating-point
computations with correct rounding.]])



prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/mpfr-4.0.2-pk3b4ab5xspubpigqfpk46uvckwdusuc/lib", ":")
prepend_path("LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/mpfr-4.0.2-pk3b4ab5xspubpigqfpk46uvckwdusuc/lib", ":")
prepend_path("C_INCLUDE_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/mpfr-4.0.2-pk3b4ab5xspubpigqfpk46uvckwdusuc/include", ":")
prepend_path("CPLUS_INCLUDE_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/mpfr-4.0.2-pk3b4ab5xspubpigqfpk46uvckwdusuc/include", ":")
prepend_path("INCLUDE", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/mpfr-4.0.2-pk3b4ab5xspubpigqfpk46uvckwdusuc/include", ":")
prepend_path("PKG_CONFIG_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/mpfr-4.0.2-pk3b4ab5xspubpigqfpk46uvckwdusuc/lib/pkgconfig", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/mpfr-4.0.2-pk3b4ab5xspubpigqfpk46uvckwdusuc/", ":")

