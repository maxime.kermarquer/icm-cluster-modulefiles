-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2021-01-13 18:00:38.291390
--
-- munge@0.5.14%gcc@6.4.0 localstatedir=PREFIX/var arch=linux-centos7-broadwell/yjzgbxa
--

whatis([[Name : munge]])
whatis([[Version : 0.5.14]])
whatis([[Target : broadwell]])
whatis([[Short description :  MUNGE Uid 'N' Gid Emporium ]])

help([[ MUNGE Uid 'N' Gid Emporium]])



prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/munge-0.5.14-yjzgbxaxfqcmeguqet64ftzk6mjeiy5a/lib", ":")
prepend_path("LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/munge-0.5.14-yjzgbxaxfqcmeguqet64ftzk6mjeiy5a/lib", ":")
prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/munge-0.5.14-yjzgbxaxfqcmeguqet64ftzk6mjeiy5a/bin", ":")
prepend_path("MANPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/munge-0.5.14-yjzgbxaxfqcmeguqet64ftzk6mjeiy5a/share/man", ":")
prepend_path("C_INCLUDE_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/munge-0.5.14-yjzgbxaxfqcmeguqet64ftzk6mjeiy5a/include", ":")
prepend_path("CPLUS_INCLUDE_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/munge-0.5.14-yjzgbxaxfqcmeguqet64ftzk6mjeiy5a/include", ":")
prepend_path("INCLUDE", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/munge-0.5.14-yjzgbxaxfqcmeguqet64ftzk6mjeiy5a/include", ":")
prepend_path("PKG_CONFIG_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/munge-0.5.14-yjzgbxaxfqcmeguqet64ftzk6mjeiy5a/lib/pkgconfig", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/munge-0.5.14-yjzgbxaxfqcmeguqet64ftzk6mjeiy5a/", ":")

