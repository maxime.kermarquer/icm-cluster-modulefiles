-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2018-09-19 11:43:29.048994
--
-- nasm@2.13.03%gcc@6.4.0 arch=linux-centos7-x86_64 /gxoad6w
--

whatis([[Name : nasm]])
whatis([[Version : 2.13.03]])
whatis([[Short description : NASM (Netwide Assembler) is an 80x86 assembler designed for portability and modularity. It includes a disassembler as well.]])

help([[NASM (Netwide Assembler) is an 80x86 assembler designed for portability
and modularity. It includes a disassembler as well.]])



prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/nasm-2.13.03-gxoad6wfqy2l6zqubfjgiii2llytbyzx/bin", ":")
prepend_path("MANPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/nasm-2.13.03-gxoad6wfqy2l6zqubfjgiii2llytbyzx/share/man", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/nasm-2.13.03-gxoad6wfqy2l6zqubfjgiii2llytbyzx/", ":")

