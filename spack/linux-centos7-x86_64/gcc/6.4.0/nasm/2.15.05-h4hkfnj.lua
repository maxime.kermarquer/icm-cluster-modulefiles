-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2021-10-14 18:43:03.310263
--
-- nasm@2.15.05%gcc@6.4.0 arch=linux-centos7-broadwell/h4hkfnj
--

whatis([[Name : nasm]])
whatis([[Version : 2.15.05]])
whatis([[Target : broadwell]])
whatis([[Short description : NASM (Netwide Assembler) is an 80x86 assembler designed for portability and modularity. It includes a disassembler as well.]])

help([[NASM (Netwide Assembler) is an 80x86 assembler designed for portability
and modularity. It includes a disassembler as well.]])



prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/nasm-2.15.05-h4hkfnjklejxyvndu3jqogwubmrj6opj/bin", ":")
prepend_path("MANPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/nasm-2.15.05-h4hkfnjklejxyvndu3jqogwubmrj6opj/share/man", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/nasm-2.15.05-h4hkfnjklejxyvndu3jqogwubmrj6opj/", ":")

