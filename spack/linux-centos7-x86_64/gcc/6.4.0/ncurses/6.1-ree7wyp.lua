-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2018-09-18 10:58:41.485237
--
-- ncurses@6.1%gcc@6.4.0~symlinks~termlib arch=linux-centos7-x86_64 /ree7wyp
--

whatis([[Name : ncurses]])
whatis([[Version : 6.1]])
whatis([[Short description : The ncurses (new curses) library is a free software emulation of curses in System V Release 4.0, and more. It uses terminfo format, supports pads and color and multiple highlights and forms characters and function-key mapping, and has all the other SYSV-curses enhancements over BSD curses.]])

help([[The ncurses (new curses) library is a free software emulation of curses
in System V Release 4.0, and more. It uses terminfo format, supports
pads and color and multiple highlights and forms characters and
function-key mapping, and has all the other SYSV-curses enhancements
over BSD curses.]])



prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/ncurses-6.1-ree7wypg73ztyvthpzod4cy26sojrfpw/bin", ":")
prepend_path("MANPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/ncurses-6.1-ree7wypg73ztyvthpzod4cy26sojrfpw/share/man", ":")
prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/ncurses-6.1-ree7wypg73ztyvthpzod4cy26sojrfpw/lib", ":")
prepend_path("LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/ncurses-6.1-ree7wypg73ztyvthpzod4cy26sojrfpw/lib", ":")
prepend_path("CPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/ncurses-6.1-ree7wypg73ztyvthpzod4cy26sojrfpw/include", ":")
prepend_path("PKG_CONFIG_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/ncurses-6.1-ree7wypg73ztyvthpzod4cy26sojrfpw/lib/pkgconfig", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/ncurses-6.1-ree7wypg73ztyvthpzod4cy26sojrfpw/", ":")

