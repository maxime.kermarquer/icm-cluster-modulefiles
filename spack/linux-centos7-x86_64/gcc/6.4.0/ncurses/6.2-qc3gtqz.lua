-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2021-01-13 17:00:39.336600
--
-- ncurses@6.2%gcc@6.4.0~symlinks+termlib arch=linux-centos7-broadwell/qc3gtqz
--

whatis([[Name : ncurses]])
whatis([[Version : 6.2]])
whatis([[Target : broadwell]])
whatis([[Short description : The ncurses (new curses) library is a free software emulation of curses in System V Release 4.0, and more. It uses terminfo format, supports pads and color and multiple highlights and forms characters and function-key mapping, and has all the other SYSV-curses enhancements over BSD curses.]])

help([[The ncurses (new curses) library is a free software emulation of curses
in System V Release 4.0, and more. It uses terminfo format, supports
pads and color and multiple highlights and forms characters and
function-key mapping, and has all the other SYSV-curses enhancements
over BSD curses.]])



prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/ncurses-6.2-qc3gtqzwi6fytsfj75gykwnl7iexhxxv/lib", ":")
prepend_path("LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/ncurses-6.2-qc3gtqzwi6fytsfj75gykwnl7iexhxxv/lib", ":")
prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/ncurses-6.2-qc3gtqzwi6fytsfj75gykwnl7iexhxxv/bin", ":")
prepend_path("C_INCLUDE_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/ncurses-6.2-qc3gtqzwi6fytsfj75gykwnl7iexhxxv/include", ":")
prepend_path("CPLUS_INCLUDE_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/ncurses-6.2-qc3gtqzwi6fytsfj75gykwnl7iexhxxv/include", ":")
prepend_path("INCLUDE", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/ncurses-6.2-qc3gtqzwi6fytsfj75gykwnl7iexhxxv/include", ":")
prepend_path("PKG_CONFIG_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/ncurses-6.2-qc3gtqzwi6fytsfj75gykwnl7iexhxxv/lib/pkgconfig", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/ncurses-6.2-qc3gtqzwi6fytsfj75gykwnl7iexhxxv/", ":")

