-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2019-03-20 16:01:15.415884
--
-- nettle@3.4%gcc@6.4.0 arch=linux-centos7-x86_64 /ex6upnh
--

whatis([[Name : nettle]])
whatis([[Version : 3.4]])
whatis([[Short description : The Nettle package contains the low-level cryptographic library that is designed to fit easily in many contexts.]])

help([[The Nettle package contains the low-level cryptographic library that is
designed to fit easily in many contexts.]])



prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/nettle-3.4-ex6upnhqprb7twiu3iqyts5smtowmvyk/bin", ":")
prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/nettle-3.4-ex6upnhqprb7twiu3iqyts5smtowmvyk/lib64", ":")
prepend_path("LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/nettle-3.4-ex6upnhqprb7twiu3iqyts5smtowmvyk/lib64", ":")
prepend_path("CPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/nettle-3.4-ex6upnhqprb7twiu3iqyts5smtowmvyk/include", ":")
prepend_path("PKG_CONFIG_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/nettle-3.4-ex6upnhqprb7twiu3iqyts5smtowmvyk/lib64/pkgconfig", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/nettle-3.4-ex6upnhqprb7twiu3iqyts5smtowmvyk/", ":")

