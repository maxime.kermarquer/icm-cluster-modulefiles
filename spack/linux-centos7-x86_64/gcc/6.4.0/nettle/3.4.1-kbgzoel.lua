-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2021-01-13 19:12:02.104071
--
-- nettle@3.4.1%gcc@6.4.0 arch=linux-centos7-broadwell/kbgzoel
--

whatis([[Name : nettle]])
whatis([[Version : 3.4.1]])
whatis([[Target : broadwell]])
whatis([[Short description : The Nettle package contains the low-level cryptographic library that is designed to fit easily in many contexts.]])
whatis([[Configure options : CFLAGS=-std=c99]])

help([[The Nettle package contains the low-level cryptographic library that is
designed to fit easily in many contexts.]])



prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/nettle-3.4.1-kbgzoelvkzgzdjjlk6wz26e6pzj2cjsm/lib64", ":")
prepend_path("LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/nettle-3.4.1-kbgzoelvkzgzdjjlk6wz26e6pzj2cjsm/lib64", ":")
prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/nettle-3.4.1-kbgzoelvkzgzdjjlk6wz26e6pzj2cjsm/bin", ":")
prepend_path("C_INCLUDE_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/nettle-3.4.1-kbgzoelvkzgzdjjlk6wz26e6pzj2cjsm/include", ":")
prepend_path("CPLUS_INCLUDE_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/nettle-3.4.1-kbgzoelvkzgzdjjlk6wz26e6pzj2cjsm/include", ":")
prepend_path("INCLUDE", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/nettle-3.4.1-kbgzoelvkzgzdjjlk6wz26e6pzj2cjsm/include", ":")
prepend_path("PKG_CONFIG_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/nettle-3.4.1-kbgzoelvkzgzdjjlk6wz26e6pzj2cjsm/lib64/pkgconfig", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/nettle-3.4.1-kbgzoelvkzgzdjjlk6wz26e6pzj2cjsm/", ":")

