-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2018-12-05 18:47:35.291058
--
-- ninja@1.8.2%gcc@6.4.0 arch=linux-centos7-x86_64 /ybyq6p4
--

whatis([[Name : ninja]])
whatis([[Version : 1.8.2]])
whatis([[Short description : Ninja is a small build system with a focus on speed. It differs from other build systems in two major respects: it is designed to have its input files generated by a higher-level build system, and it is designed to run builds as fast as possible.]])

help([[Ninja is a small build system with a focus on speed. It differs from
other build systems in two major respects: it is designed to have its
input files generated by a higher-level build system, and it is designed
to run builds as fast as possible.]])



prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/ninja-1.8.2-ybyq6p4x2hxqnem7nobrrol2ttzgnl42/bin", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/ninja-1.8.2-ybyq6p4x2hxqnem7nobrrol2ttzgnl42/", ":")

