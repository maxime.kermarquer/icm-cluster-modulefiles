-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2018-10-02 15:02:58.332162
--
-- numactl@2.0.11%gcc@6.4.0 patches=592f30f7f5f757dfc239ad0ffd39a9a048487ad803c26b419e0f96b8cda08c1a arch=linux-centos7-x86_64 /k2i6r24
--

whatis([[Name : numactl]])
whatis([[Version : 2.0.11]])
whatis([[Short description : NUMA support for Linux]])

help([[NUMA support for Linux]])



prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/numactl-2.0.11-k2i6r24t4cu3tkjhg7ikpi5yp73rh4yo/bin", ":")
prepend_path("MANPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/numactl-2.0.11-k2i6r24t4cu3tkjhg7ikpi5yp73rh4yo/share/man", ":")
prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/numactl-2.0.11-k2i6r24t4cu3tkjhg7ikpi5yp73rh4yo/lib", ":")
prepend_path("LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/numactl-2.0.11-k2i6r24t4cu3tkjhg7ikpi5yp73rh4yo/lib", ":")
prepend_path("CPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/numactl-2.0.11-k2i6r24t4cu3tkjhg7ikpi5yp73rh4yo/include", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/numactl-2.0.11-k2i6r24t4cu3tkjhg7ikpi5yp73rh4yo/", ":")

