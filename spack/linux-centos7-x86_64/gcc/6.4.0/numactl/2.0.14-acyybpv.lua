-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2021-01-13 17:48:46.745580
--
-- numactl@2.0.14%gcc@6.4.0 patches=4e1d78cbbb85de625bad28705e748856033eaafab92a66dffd383a3d7e00cc94 arch=linux-centos7-broadwell/acyybpv
--

whatis([[Name : numactl]])
whatis([[Version : 2.0.14]])
whatis([[Target : broadwell]])
whatis([[Short description : NUMA support for Linux]])

help([[NUMA support for Linux]])



prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/numactl-2.0.14-acyybpvidt2okruh7qnyhdpxcruzqfxf/lib", ":")
prepend_path("LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/numactl-2.0.14-acyybpvidt2okruh7qnyhdpxcruzqfxf/lib", ":")
prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/numactl-2.0.14-acyybpvidt2okruh7qnyhdpxcruzqfxf/bin", ":")
prepend_path("MANPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/numactl-2.0.14-acyybpvidt2okruh7qnyhdpxcruzqfxf/share/man", ":")
prepend_path("C_INCLUDE_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/numactl-2.0.14-acyybpvidt2okruh7qnyhdpxcruzqfxf/include", ":")
prepend_path("CPLUS_INCLUDE_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/numactl-2.0.14-acyybpvidt2okruh7qnyhdpxcruzqfxf/include", ":")
prepend_path("INCLUDE", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/numactl-2.0.14-acyybpvidt2okruh7qnyhdpxcruzqfxf/include", ":")
prepend_path("PKG_CONFIG_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/numactl-2.0.14-acyybpvidt2okruh7qnyhdpxcruzqfxf/lib/pkgconfig", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/numactl-2.0.14-acyybpvidt2okruh7qnyhdpxcruzqfxf/", ":")

