-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2019-01-07 15:46:22.391055
--
-- openblas@0.3.2%gcc@6.4.0 cpu_target= ~ilp64 patches=47cfa7a952ac7b2e4632c73ae199d69fb54490627b66a62c681e21019c4ddc9d +pic+shared threads=none ~virtual_machine arch=linux-centos7-x86_64 /kjtrgn7
--

whatis([[Name : openblas]])
whatis([[Version : 0.3.2]])
whatis([[Short description : OpenBLAS: An optimized BLAS library]])

help([[OpenBLAS: An optimized BLAS library]])



prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/openblas-0.3.2-kjtrgn7bwz3vow6aajdsbnti4gtvhuwa/bin", ":")
prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/openblas-0.3.2-kjtrgn7bwz3vow6aajdsbnti4gtvhuwa/lib", ":")
prepend_path("LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/openblas-0.3.2-kjtrgn7bwz3vow6aajdsbnti4gtvhuwa/lib", ":")
prepend_path("CPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/openblas-0.3.2-kjtrgn7bwz3vow6aajdsbnti4gtvhuwa/include", ":")
prepend_path("PKG_CONFIG_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/openblas-0.3.2-kjtrgn7bwz3vow6aajdsbnti4gtvhuwa/lib/pkgconfig", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/openblas-0.3.2-kjtrgn7bwz3vow6aajdsbnti4gtvhuwa/", ":")

