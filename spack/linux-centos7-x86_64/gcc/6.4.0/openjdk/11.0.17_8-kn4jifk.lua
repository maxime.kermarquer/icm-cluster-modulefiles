-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2023-05-17 16:13:47.481837
--
-- openjdk@11.0.17_8%gcc@6.4.0 build_system=generic arch=linux-centos7-broadwell/kn4jifk
--

whatis([[Name : openjdk]])
whatis([[Version : 11.0.17_8]])
whatis([[Target : broadwell]])
whatis([[Short description : The free and opensource java implementation]])

help([[Name   : openjdk]])
help([[Version: 11.0.17_8]])
help([[Target : broadwell]])
help()
help([[The free and opensource java implementation]])



prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/openjdk-11.0.17_8-kn4jifkds6hd5vvg2e6fgleejwfzq2in/bin", ":")
prepend_path("MANPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/openjdk-11.0.17_8-kn4jifkds6hd5vvg2e6fgleejwfzq2in/man", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/openjdk-11.0.17_8-kn4jifkds6hd5vvg2e6fgleejwfzq2in/.", ":")
setenv("JAVA_HOME", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/openjdk-11.0.17_8-kn4jifkds6hd5vvg2e6fgleejwfzq2in")

