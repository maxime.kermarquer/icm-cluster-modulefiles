-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2021-11-30 16:30:13.811937
--
-- openjpeg@2.3.1%gcc@6.4.0~ipo build_type=RelWithDebInfo arch=linux-centos7-broadwell/pwhdh5f
--

whatis([[Name : openjpeg]])
whatis([[Version : 2.3.1]])
whatis([[Target : broadwell]])
whatis([[Short description : OpenJPEG is an open-source JPEG 2000 codec written in C language.]])

help([[OpenJPEG is an open-source JPEG 2000 codec written in C language. It has
been developed in order to promote the use of JPEG 2000, a still-image
compression standard from the Joint Photographic Experts Group (JPEG).
Since April 2015, it is officially recognized by ISO/IEC and ITU-T as a
JPEG 2000 Reference Software.]])



prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/openjpeg-2.3.1-pwhdh5fn4hl524sg7qjeip5g4lwhx4ga/lib", ":")
prepend_path("LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/openjpeg-2.3.1-pwhdh5fn4hl524sg7qjeip5g4lwhx4ga/lib", ":")
prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/openjpeg-2.3.1-pwhdh5fn4hl524sg7qjeip5g4lwhx4ga/bin", ":")
prepend_path("C_INCLUDE_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/openjpeg-2.3.1-pwhdh5fn4hl524sg7qjeip5g4lwhx4ga/include", ":")
prepend_path("CPLUS_INCLUDE_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/openjpeg-2.3.1-pwhdh5fn4hl524sg7qjeip5g4lwhx4ga/include", ":")
prepend_path("INCLUDE", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/openjpeg-2.3.1-pwhdh5fn4hl524sg7qjeip5g4lwhx4ga/include", ":")
prepend_path("PKG_CONFIG_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/openjpeg-2.3.1-pwhdh5fn4hl524sg7qjeip5g4lwhx4ga/lib/pkgconfig", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/openjpeg-2.3.1-pwhdh5fn4hl524sg7qjeip5g4lwhx4ga/", ":")

