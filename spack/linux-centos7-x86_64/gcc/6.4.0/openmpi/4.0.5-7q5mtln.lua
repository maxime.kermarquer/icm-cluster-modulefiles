-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2021-01-13 19:26:41.989364
--
-- openmpi@4.0.5%gcc@6.4.0~atomics~cuda~cxx~cxx_exceptions+gpfs~java~legacylaunchers~lustre~memchecker~pmi~singularity~sqlite3+static~thread_multiple+vt+wrapper-rpath fabrics=none schedulers=none arch=linux-centos7-broadwell/7q5mtln
--

whatis([[Name : openmpi]])
whatis([[Version : 4.0.5]])
whatis([[Target : broadwell]])
whatis([[Short description : An open source Message Passing Interface implementation.]])
whatis([[Configure options : --enable-shared --disable-silent-rules --disable-builtin-atomics --enable-static --without-pmi --with-zlib=/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/zlib-1.2.11-wp5daspjexwgd5rft64ekrbjk4uarol2 --enable-mpi1-compatibility --without-knem --without-hcoll --without-psm --without-ofi --without-cma --without-ucx --without-fca --without-mxm --without-verbs --without-xpmem --without-psm2 --without-alps --without-lsf --without-sge --without-slurm --without-tm --without-loadleveler --disable-memchecker --with-libevent=/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/libevent-2.1.12-23bmgijbukon3rbzbnwrnsgktvqiyblr --with-hwloc=/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/hwloc-2.4.0-ekhsw53rov2jgyq4b6oqszcxgu2zu5sn --disable-java --disable-mpi-java --without-cuda --enable-wrapper-rpath --disable-wrapper-runpath --disable-mpi-cxx --disable-cxx-exceptions --with-wrapper-ldflags=-Wl,-rpath,/network/lustre/iss01/apps/software/linux-centos7-x86_64/gcc/4.8.5/gcc/6.4.0/gbpzos7apffhqzcgxeyau3u6dfdikzth/lib/gcc/x86_64-pc-linux-gnu/6.4.0 -Wl,-rpath,/network/lustre/iss01/apps/software/linux-centos7-x86_64/gcc/4.8.5/gcc/6.4.0/gbpzos7apffhqzcgxeyau3u6dfdikzth/lib64]])

help([[An open source Message Passing Interface implementation. The Open MPI
Project is an open source Message Passing Interface implementation that
is developed and maintained by a consortium of academic, research, and
industry partners. Open MPI is therefore able to combine the expertise,
technologies, and resources from all across the High Performance
Computing community in order to build the best MPI library available.
Open MPI offers advantages for system and software vendors, application
developers and computer science researchers.]])

-- Services provided by the package
family("mpi")

-- Loading this module unlocks the path below unconditionally
prepend_path("MODULEPATH", "/network/lustre/iss01/home/maxime.kermarquer/modules-TODO/linux-centos7-x86_64/openmpi/4.0.5-7q5mtln/gcc/6.4.0")

-- Try to load variables into path to see if providers are there

-- Change MODULEPATH based on the result of the tests above

-- Set variables to notify the provider of the new services
setenv("LMOD_MPI_NAME", "openmpi")
setenv("LMOD_MPI_VERSION", "4.0.5-7q5mtln")


prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/openmpi-4.0.5-7q5mtlntifhl2ttzzw4uy4kyo32o3pjk/lib", ":")
prepend_path("LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/openmpi-4.0.5-7q5mtlntifhl2ttzzw4uy4kyo32o3pjk/lib", ":")
prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/openmpi-4.0.5-7q5mtlntifhl2ttzzw4uy4kyo32o3pjk/bin", ":")
prepend_path("MANPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/openmpi-4.0.5-7q5mtlntifhl2ttzzw4uy4kyo32o3pjk/share/man", ":")
prepend_path("C_INCLUDE_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/openmpi-4.0.5-7q5mtlntifhl2ttzzw4uy4kyo32o3pjk/include", ":")
prepend_path("CPLUS_INCLUDE_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/openmpi-4.0.5-7q5mtlntifhl2ttzzw4uy4kyo32o3pjk/include", ":")
prepend_path("INCLUDE", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/openmpi-4.0.5-7q5mtlntifhl2ttzzw4uy4kyo32o3pjk/include", ":")
prepend_path("PKG_CONFIG_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/openmpi-4.0.5-7q5mtlntifhl2ttzzw4uy4kyo32o3pjk/lib/pkgconfig", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/openmpi-4.0.5-7q5mtlntifhl2ttzzw4uy4kyo32o3pjk/", ":")
setenv("MPICC", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/openmpi-4.0.5-7q5mtlntifhl2ttzzw4uy4kyo32o3pjk/bin/mpicc")
setenv("MPICXX", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/openmpi-4.0.5-7q5mtlntifhl2ttzzw4uy4kyo32o3pjk/bin/mpic++")
setenv("MPIF77", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/openmpi-4.0.5-7q5mtlntifhl2ttzzw4uy4kyo32o3pjk/bin/mpif77")
setenv("MPIF90", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/openmpi-4.0.5-7q5mtlntifhl2ttzzw4uy4kyo32o3pjk/bin/mpif90")

