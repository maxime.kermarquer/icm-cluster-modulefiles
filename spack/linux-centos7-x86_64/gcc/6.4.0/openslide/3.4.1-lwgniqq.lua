-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2021-11-30 16:36:54.424036
--
-- openslide@3.4.1%gcc@6.4.0 arch=linux-centos7-broadwell/lwgniqq
--

whatis([[Name : openslide]])
whatis([[Version : 3.4.1]])
whatis([[Target : broadwell]])
whatis([[Short description : OpenSlide reads whole slide image files.]])

help([[OpenSlide reads whole slide image files.]])



prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/openslide-3.4.1-lwgniqqfpngv247vzdskvya6v4yjwool/lib", ":")
prepend_path("LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/openslide-3.4.1-lwgniqqfpngv247vzdskvya6v4yjwool/lib", ":")
prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/openslide-3.4.1-lwgniqqfpngv247vzdskvya6v4yjwool/bin", ":")
prepend_path("MANPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/openslide-3.4.1-lwgniqqfpngv247vzdskvya6v4yjwool/share/man", ":")
prepend_path("C_INCLUDE_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/openslide-3.4.1-lwgniqqfpngv247vzdskvya6v4yjwool/include", ":")
prepend_path("CPLUS_INCLUDE_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/openslide-3.4.1-lwgniqqfpngv247vzdskvya6v4yjwool/include", ":")
prepend_path("INCLUDE", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/openslide-3.4.1-lwgniqqfpngv247vzdskvya6v4yjwool/include", ":")
prepend_path("PKG_CONFIG_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/openslide-3.4.1-lwgniqqfpngv247vzdskvya6v4yjwool/lib/pkgconfig", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/openslide-3.4.1-lwgniqqfpngv247vzdskvya6v4yjwool/", ":")
prepend_path("PYTHONPATH", "", ":")
prepend_path("XDG_DATA_DIRS", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/gobject-introspection-1.56.1-ecxzmbozibkmxuomxizc2msi55zzaq7p/share", ":")
prepend_path("GI_TYPELIB_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/gobject-introspection-1.56.1-ecxzmbozibkmxuomxizc2msi55zzaq7p/lib/girepository-1.0", ":")
prepend_path("XDG_DATA_DIRS", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/gdk-pixbuf-2.40.0-6cbgcfrqaq2xe4gxfcddx4u4haldscld/share", ":")
prepend_path("GI_TYPELIB_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/gdk-pixbuf-2.40.0-6cbgcfrqaq2xe4gxfcddx4u4haldscld/lib/girepository-1.0", ":")

