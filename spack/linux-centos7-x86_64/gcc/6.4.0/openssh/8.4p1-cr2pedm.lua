-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2021-01-13 17:40:43.304892
--
-- openssh@8.4p1%gcc@6.4.0 arch=linux-centos7-broadwell/cr2pedm
--

whatis([[Name : openssh]])
whatis([[Version : 8.4p1]])
whatis([[Target : broadwell]])
whatis([[Short description : OpenSSH is the premier connectivity tool for remote login with the SSH protocol. It encrypts all traffic to eliminate eavesdropping, connection hijacking, and other attacks. In addition, OpenSSH provides a large suite of secure tunneling capabilities, several authentication methods, and sophisticated configuration options. ]])
whatis([[Configure options : --with-privsep-path=/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/openssh-8.4p1-cr2pedmycm4daec7vbxozf542jdckwhg/var/empty]])

help([[OpenSSH is the premier connectivity tool for remote login with the SSH
protocol. It encrypts all traffic to eliminate eavesdropping, connection
hijacking, and other attacks. In addition, OpenSSH provides a large
suite of secure tunneling capabilities, several authentication methods,
and sophisticated configuration options.]])



prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/openssh-8.4p1-cr2pedmycm4daec7vbxozf542jdckwhg/bin", ":")
prepend_path("MANPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/openssh-8.4p1-cr2pedmycm4daec7vbxozf542jdckwhg/share/man", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/openssh-8.4p1-cr2pedmycm4daec7vbxozf542jdckwhg/", ":")

