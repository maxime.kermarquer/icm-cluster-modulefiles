-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2018-09-18 11:31:28.548872
--
-- openssl@1.0.2o%gcc@6.4.0+systemcerts arch=linux-centos7-x86_64 /qxfzezx
--

whatis([[Name : openssl]])
whatis([[Version : 1.0.2o]])
whatis([[Short description : OpenSSL is an open source project that provides a robust, commercial-grade, and full-featured toolkit for the Transport Layer Security (TLS) and Secure Sockets Layer (SSL) protocols. It is also a general-purpose cryptography library.]])

help([[OpenSSL is an open source project that provides a robust, commercial-
grade, and full-featured toolkit for the Transport Layer Security (TLS)
and Secure Sockets Layer (SSL) protocols. It is also a general-purpose
cryptography library.]])



prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/openssl-1.0.2o-qxfzezxhp4blvxkj6vsnj43vpiqfjs3x/bin", ":")
prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/openssl-1.0.2o-qxfzezxhp4blvxkj6vsnj43vpiqfjs3x/lib", ":")
prepend_path("LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/openssl-1.0.2o-qxfzezxhp4blvxkj6vsnj43vpiqfjs3x/lib", ":")
prepend_path("CPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/openssl-1.0.2o-qxfzezxhp4blvxkj6vsnj43vpiqfjs3x/include", ":")
prepend_path("PKG_CONFIG_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/openssl-1.0.2o-qxfzezxhp4blvxkj6vsnj43vpiqfjs3x/lib/pkgconfig", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/openssl-1.0.2o-qxfzezxhp4blvxkj6vsnj43vpiqfjs3x/", ":")

