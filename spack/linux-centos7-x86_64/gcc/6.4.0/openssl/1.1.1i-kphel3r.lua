-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2021-01-13 17:30:10.112672
--
-- openssl@1.1.1i%gcc@6.4.0+systemcerts arch=linux-centos7-broadwell/kphel3r
--

whatis([[Name : openssl]])
whatis([[Version : 1.1.1i]])
whatis([[Target : broadwell]])
whatis([[Short description : OpenSSL is an open source project that provides a robust, commercial-grade, and full-featured toolkit for the Transport Layer Security (TLS) and Secure Sockets Layer (SSL) protocols. It is also a general-purpose cryptography library.]])

help([[OpenSSL is an open source project that provides a robust, commercial-
grade, and full-featured toolkit for the Transport Layer Security (TLS)
and Secure Sockets Layer (SSL) protocols. It is also a general-purpose
cryptography library.]])



prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/openssl-1.1.1i-kphel3rter2r2c5g4trtfsrzvjajvy5k/lib", ":")
prepend_path("LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/openssl-1.1.1i-kphel3rter2r2c5g4trtfsrzvjajvy5k/lib", ":")
prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/openssl-1.1.1i-kphel3rter2r2c5g4trtfsrzvjajvy5k/bin", ":")
prepend_path("MANPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/openssl-1.1.1i-kphel3rter2r2c5g4trtfsrzvjajvy5k/share/man", ":")
prepend_path("C_INCLUDE_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/openssl-1.1.1i-kphel3rter2r2c5g4trtfsrzvjajvy5k/include", ":")
prepend_path("CPLUS_INCLUDE_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/openssl-1.1.1i-kphel3rter2r2c5g4trtfsrzvjajvy5k/include", ":")
prepend_path("INCLUDE", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/openssl-1.1.1i-kphel3rter2r2c5g4trtfsrzvjajvy5k/include", ":")
prepend_path("PKG_CONFIG_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/openssl-1.1.1i-kphel3rter2r2c5g4trtfsrzvjajvy5k/lib/pkgconfig", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/openssl-1.1.1i-kphel3rter2r2c5g4trtfsrzvjajvy5k/", ":")

