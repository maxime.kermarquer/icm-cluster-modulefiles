-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2019-02-06 20:21:17.220327
--
-- pango@1.38.1%gcc@6.4.0+X arch=linux-centos7-x86_64 /tl7vjmr
--

whatis([[Name : pango]])
whatis([[Version : 1.38.1]])
whatis([[Short description : Pango is a library for laying out and rendering of text, with an emphasis on internationalization. It can be used anywhere that text layout is needed, though most of the work on Pango so far has been done in the context of the GTK+ widget toolkit.]])
whatis([[Configure options : --with-xft]])

help([[Pango is a library for laying out and rendering of text, with an
emphasis on internationalization. It can be used anywhere that text
layout is needed, though most of the work on Pango so far has been done
in the context of the GTK+ widget toolkit.]])



prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/pango-1.38.1-tl7vjmro5ibdzbor2xqf5lj46xpwjrqj/bin", ":")
prepend_path("MANPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/pango-1.38.1-tl7vjmro5ibdzbor2xqf5lj46xpwjrqj/share/man", ":")
prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/pango-1.38.1-tl7vjmro5ibdzbor2xqf5lj46xpwjrqj/lib", ":")
prepend_path("LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/pango-1.38.1-tl7vjmro5ibdzbor2xqf5lj46xpwjrqj/lib", ":")
prepend_path("CPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/pango-1.38.1-tl7vjmro5ibdzbor2xqf5lj46xpwjrqj/include", ":")
prepend_path("PKG_CONFIG_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/pango-1.38.1-tl7vjmro5ibdzbor2xqf5lj46xpwjrqj/lib/pkgconfig", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/pango-1.38.1-tl7vjmro5ibdzbor2xqf5lj46xpwjrqj/", ":")
prepend_path("CPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/libxml2-2.9.8-m3rdnxm66wlzs5gqsm5pqlzqvideutnk/include/libxml2", ":")
prepend_path("CPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/freetype-2.7.1-fnguk6ypqcpy23j3j2n3l62sjvfd3sf2/include/freetype2", ":")

