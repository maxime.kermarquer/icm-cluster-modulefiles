-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2018-09-18 11:01:32.042803
--
-- pcre@8.42%gcc@6.4.0~jit+utf arch=linux-centos7-x86_64 /lewbfks
--

whatis([[Name : pcre]])
whatis([[Version : 8.42]])
whatis([[Short description : The PCRE package contains Perl Compatible Regular Expression libraries. These are useful for implementing regular expression pattern matching using the same syntax and semantics as Perl 5.]])
whatis([[Configure options : --enable-utf --enable-unicode-properties]])

help([[The PCRE package contains Perl Compatible Regular Expression libraries.
These are useful for implementing regular expression pattern matching
using the same syntax and semantics as Perl 5.]])



prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/pcre-8.42-lewbfks5ck2g2nya2mk5dpxoobowgmpv/bin", ":")
prepend_path("MANPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/pcre-8.42-lewbfks5ck2g2nya2mk5dpxoobowgmpv/share/man", ":")
prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/pcre-8.42-lewbfks5ck2g2nya2mk5dpxoobowgmpv/lib", ":")
prepend_path("LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/pcre-8.42-lewbfks5ck2g2nya2mk5dpxoobowgmpv/lib", ":")
prepend_path("CPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/pcre-8.42-lewbfks5ck2g2nya2mk5dpxoobowgmpv/include", ":")
prepend_path("PKG_CONFIG_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/pcre-8.42-lewbfks5ck2g2nya2mk5dpxoobowgmpv/lib/pkgconfig", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/pcre-8.42-lewbfks5ck2g2nya2mk5dpxoobowgmpv/", ":")

