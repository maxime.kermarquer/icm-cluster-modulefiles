-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2021-01-13 16:57:35.095977
--
-- pcre@8.44%gcc@6.4.0~jit+multibyte+utf arch=linux-centos7-broadwell/p5mug7j
--

whatis([[Name : pcre]])
whatis([[Version : 8.44]])
whatis([[Target : broadwell]])
whatis([[Short description : The PCRE package contains Perl Compatible Regular Expression libraries. These are useful for implementing regular expression pattern matching using the same syntax and semantics as Perl 5.]])
whatis([[Configure options : --enable-pcre16 --enable-pcre32 --enable-utf --enable-unicode-properties]])

help([[The PCRE package contains Perl Compatible Regular Expression libraries.
These are useful for implementing regular expression pattern matching
using the same syntax and semantics as Perl 5.]])



prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/pcre-8.44-p5mug7jc2k2c6v76hsvcskao4tgt3ekr/lib", ":")
prepend_path("LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/pcre-8.44-p5mug7jc2k2c6v76hsvcskao4tgt3ekr/lib", ":")
prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/pcre-8.44-p5mug7jc2k2c6v76hsvcskao4tgt3ekr/bin", ":")
prepend_path("MANPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/pcre-8.44-p5mug7jc2k2c6v76hsvcskao4tgt3ekr/share/man", ":")
prepend_path("C_INCLUDE_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/pcre-8.44-p5mug7jc2k2c6v76hsvcskao4tgt3ekr/include", ":")
prepend_path("CPLUS_INCLUDE_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/pcre-8.44-p5mug7jc2k2c6v76hsvcskao4tgt3ekr/include", ":")
prepend_path("INCLUDE", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/pcre-8.44-p5mug7jc2k2c6v76hsvcskao4tgt3ekr/include", ":")
prepend_path("PKG_CONFIG_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/pcre-8.44-p5mug7jc2k2c6v76hsvcskao4tgt3ekr/lib/pkgconfig", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/pcre-8.44-p5mug7jc2k2c6v76hsvcskao4tgt3ekr/", ":")

