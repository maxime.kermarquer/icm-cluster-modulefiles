-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2021-01-13 16:56:13.273105
--
-- pcre2@10.35%gcc@6.4.0~jit+multibyte arch=linux-centos7-broadwell/xeq74dy
--

whatis([[Name : pcre2]])
whatis([[Version : 10.35]])
whatis([[Target : broadwell]])
whatis([[Short description : The PCRE2 package contains Perl Compatible Regular Expression libraries. These are useful for implementing regular expression pattern matching using the same syntax and semantics as Perl 5.]])
whatis([[Configure options : --enable-pcre2-16 --enable-pcre2-32]])

help([[The PCRE2 package contains Perl Compatible Regular Expression libraries.
These are useful for implementing regular expression pattern matching
using the same syntax and semantics as Perl 5.]])



prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/pcre2-10.35-xeq74dy5d2ed6ax4zgzzfmwpjka2v2e3/lib", ":")
prepend_path("LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/pcre2-10.35-xeq74dy5d2ed6ax4zgzzfmwpjka2v2e3/lib", ":")
prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/pcre2-10.35-xeq74dy5d2ed6ax4zgzzfmwpjka2v2e3/bin", ":")
prepend_path("MANPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/pcre2-10.35-xeq74dy5d2ed6ax4zgzzfmwpjka2v2e3/share/man", ":")
prepend_path("C_INCLUDE_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/pcre2-10.35-xeq74dy5d2ed6ax4zgzzfmwpjka2v2e3/include", ":")
prepend_path("CPLUS_INCLUDE_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/pcre2-10.35-xeq74dy5d2ed6ax4zgzzfmwpjka2v2e3/include", ":")
prepend_path("INCLUDE", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/pcre2-10.35-xeq74dy5d2ed6ax4zgzzfmwpjka2v2e3/include", ":")
prepend_path("PKG_CONFIG_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/pcre2-10.35-xeq74dy5d2ed6ax4zgzzfmwpjka2v2e3/lib/pkgconfig", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/pcre2-10.35-xeq74dy5d2ed6ax4zgzzfmwpjka2v2e3/", ":")

