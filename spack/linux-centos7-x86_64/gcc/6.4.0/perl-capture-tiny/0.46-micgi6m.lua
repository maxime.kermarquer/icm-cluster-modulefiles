-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2019-05-15 16:25:19.624106
--
-- perl-capture-tiny@0.46%gcc@6.4.0 arch=linux-centos7-x86_64 /micgi6m
--

whatis([[Name : perl-capture-tiny]])
whatis([[Version : 0.46]])
whatis([[Short description : Capture STDOUT and STDERR from Perl, XS or external programs]])

help([[Capture STDOUT and STDERR from Perl, XS or external programs]])



prepend_path("MANPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/perl-capture-tiny-0.46-micgi6mbjr7sjtl56snd56iu5of4umhi/man", ":")
prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/perl-capture-tiny-0.46-micgi6mbjr7sjtl56snd56iu5of4umhi/lib", ":")
prepend_path("LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/perl-capture-tiny-0.46-micgi6mbjr7sjtl56snd56iu5of4umhi/lib", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/perl-capture-tiny-0.46-micgi6mbjr7sjtl56snd56iu5of4umhi/", ":")
prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/perl-capture-tiny-0.46-micgi6mbjr7sjtl56snd56iu5of4umhi/bin", ":")
prepend_path("PERL5LIB", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/perl-capture-tiny-0.46-micgi6mbjr7sjtl56snd56iu5of4umhi/lib/perl5", ":")

