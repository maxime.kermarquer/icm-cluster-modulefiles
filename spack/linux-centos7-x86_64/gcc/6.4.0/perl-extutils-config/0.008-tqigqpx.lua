-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2021-11-30 16:24:44.588332
--
-- perl-extutils-config@0.008%gcc@6.4.0 arch=linux-centos7-broadwell/tqigqpx
--

whatis([[Name : perl-extutils-config]])
whatis([[Version : 0.008]])
whatis([[Target : broadwell]])
whatis([[Short description : ExtUtils::Config - A wrapper for perl's configuration]])

help([[ExtUtils::Config - A wrapper for perl's configuration]])



prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/perl-extutils-config-0.008-tqigqpxfjijkocchg6oasntqcus42g5h/lib", ":")
prepend_path("LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/perl-extutils-config-0.008-tqigqpxfjijkocchg6oasntqcus42g5h/lib", ":")
prepend_path("MANPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/perl-extutils-config-0.008-tqigqpxfjijkocchg6oasntqcus42g5h/man", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/perl-extutils-config-0.008-tqigqpxfjijkocchg6oasntqcus42g5h/", ":")
prepend_path("PERL5LIB", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/perl-extutils-config-0.008-tqigqpxfjijkocchg6oasntqcus42g5h/lib/perl5", ":")

