-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2021-11-30 16:25:06.794532
--
-- perl-extutils-helpers@0.026%gcc@6.4.0 arch=linux-centos7-broadwell/wkwcsp7
--

whatis([[Name : perl-extutils-helpers]])
whatis([[Version : 0.026]])
whatis([[Target : broadwell]])
whatis([[Short description : ExtUtils::Helpers - Various portability utilities for module builders]])

help([[ExtUtils::Helpers - Various portability utilities for module builders]])



prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/perl-extutils-helpers-0.026-wkwcsp7vmg6s3fs54cme4j2xxtovhszw/lib", ":")
prepend_path("LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/perl-extutils-helpers-0.026-wkwcsp7vmg6s3fs54cme4j2xxtovhszw/lib", ":")
prepend_path("MANPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/perl-extutils-helpers-0.026-wkwcsp7vmg6s3fs54cme4j2xxtovhszw/man", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/perl-extutils-helpers-0.026-wkwcsp7vmg6s3fs54cme4j2xxtovhszw/", ":")
prepend_path("PERL5LIB", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/perl-extutils-helpers-0.026-wkwcsp7vmg6s3fs54cme4j2xxtovhszw/lib/perl5", ":")

