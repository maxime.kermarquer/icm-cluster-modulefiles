-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2021-11-30 16:24:27.270745
--
-- perl-extutils-installpaths@0.012%gcc@6.4.0 arch=linux-centos7-broadwell/dc77rzf
--

whatis([[Name : perl-extutils-installpaths]])
whatis([[Version : 0.012]])
whatis([[Target : broadwell]])
whatis([[Short description : ExtUtils::InstallPaths - Build.PL install path logic made easy]])

help([[ExtUtils::InstallPaths - Build.PL install path logic made easy]])



prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/perl-extutils-installpaths-0.012-dc77rzfdl3l6vejkbfub6aqoifhdtedg/lib", ":")
prepend_path("LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/perl-extutils-installpaths-0.012-dc77rzfdl3l6vejkbfub6aqoifhdtedg/lib", ":")
prepend_path("MANPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/perl-extutils-installpaths-0.012-dc77rzfdl3l6vejkbfub6aqoifhdtedg/man", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/perl-extutils-installpaths-0.012-dc77rzfdl3l6vejkbfub6aqoifhdtedg/", ":")
prepend_path("PERL5LIB", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/perl-extutils-installpaths-0.012-dc77rzfdl3l6vejkbfub6aqoifhdtedg/lib/perl5", ":")

