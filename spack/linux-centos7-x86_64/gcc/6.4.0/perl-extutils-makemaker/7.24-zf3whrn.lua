-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2019-05-15 16:25:48.632861
--
-- perl-extutils-makemaker@7.24%gcc@6.4.0 arch=linux-centos7-x86_64 /zf3whrn
--

whatis([[Name : perl-extutils-makemaker]])
whatis([[Version : 7.24]])
whatis([[Short description : ExtUtils::MakeMaker - Create a module Makefile. This utility is designed to write a Makefile for an extension module from a Makefile.PL. It is based on the Makefile.SH model provided by Andy Dougherty and the perl5-porters. ]])

help([[ExtUtils::MakeMaker - Create a module Makefile. This utility is designed
to write a Makefile for an extension module from a Makefile.PL. It is
based on the Makefile.SH model provided by Andy Dougherty and the
perl5-porters.]])



prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/perl-extutils-makemaker-7.24-zf3whrnroksdzisiurt323vovkhi6nha/bin", ":")
prepend_path("MANPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/perl-extutils-makemaker-7.24-zf3whrnroksdzisiurt323vovkhi6nha/man", ":")
prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/perl-extutils-makemaker-7.24-zf3whrnroksdzisiurt323vovkhi6nha/lib", ":")
prepend_path("LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/perl-extutils-makemaker-7.24-zf3whrnroksdzisiurt323vovkhi6nha/lib", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/perl-extutils-makemaker-7.24-zf3whrnroksdzisiurt323vovkhi6nha/", ":")
prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/perl-extutils-makemaker-7.24-zf3whrnroksdzisiurt323vovkhi6nha/bin", ":")
prepend_path("PERL5LIB", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/perl-extutils-makemaker-7.24-zf3whrnroksdzisiurt323vovkhi6nha/lib/perl5", ":")

