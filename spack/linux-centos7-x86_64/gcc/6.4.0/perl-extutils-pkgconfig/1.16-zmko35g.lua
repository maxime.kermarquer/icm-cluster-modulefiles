-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2019-05-15 16:25:50.824827
--
-- perl-extutils-pkgconfig@1.16%gcc@6.4.0 arch=linux-centos7-x86_64 /zmko35g
--

whatis([[Name : perl-extutils-pkgconfig]])
whatis([[Version : 1.16]])
whatis([[Short description : simplistic interface to pkg-config]])

help([[simplistic interface to pkg-config]])



prepend_path("MANPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/perl-extutils-pkgconfig-1.16-zmko35gwizpotbyamkbseomh2ebeunyt/man", ":")
prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/perl-extutils-pkgconfig-1.16-zmko35gwizpotbyamkbseomh2ebeunyt/lib", ":")
prepend_path("LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/perl-extutils-pkgconfig-1.16-zmko35gwizpotbyamkbseomh2ebeunyt/lib", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/perl-extutils-pkgconfig-1.16-zmko35gwizpotbyamkbseomh2ebeunyt/", ":")
prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/perl-extutils-pkgconfig-1.16-zmko35gwizpotbyamkbseomh2ebeunyt/bin", ":")
prepend_path("PERL5LIB", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/perl-extutils-pkgconfig-1.16-zmko35gwizpotbyamkbseomh2ebeunyt/lib/perl5", ":")

