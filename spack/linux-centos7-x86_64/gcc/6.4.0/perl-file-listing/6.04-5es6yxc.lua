-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2021-11-30 16:25:26.618593
--
-- perl-file-listing@6.04%gcc@6.4.0 arch=linux-centos7-broadwell/5es6yxc
--

whatis([[Name : perl-file-listing]])
whatis([[Version : 6.04]])
whatis([[Target : broadwell]])
whatis([[Short description : Parse directory listing]])

help([[Parse directory listing]])



prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/perl-file-listing-6.04-5es6yxcjhhiqmyet7kbacxjr7ko5nd2x/lib", ":")
prepend_path("LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/perl-file-listing-6.04-5es6yxcjhhiqmyet7kbacxjr7ko5nd2x/lib", ":")
prepend_path("MANPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/perl-file-listing-6.04-5es6yxcjhhiqmyet7kbacxjr7ko5nd2x/man", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/perl-file-listing-6.04-5es6yxcjhhiqmyet7kbacxjr7ko5nd2x/", ":")
prepend_path("PERL5LIB", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/perl-file-listing-6.04-5es6yxcjhhiqmyet7kbacxjr7ko5nd2x/lib/perl5:/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/perl-http-date-6.02-jquqfhutkxpbehrzabjoqdnusurylewc/lib/perl5", ":")

