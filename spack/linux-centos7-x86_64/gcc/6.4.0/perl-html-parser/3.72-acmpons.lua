-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2021-11-30 16:25:22.102211
--
-- perl-html-parser@3.72%gcc@6.4.0 arch=linux-centos7-broadwell/acmpons
--

whatis([[Name : perl-html-parser]])
whatis([[Version : 3.72]])
whatis([[Target : broadwell]])
whatis([[Short description : HTML parser class]])

help([[HTML parser class]])



prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/perl-html-parser-3.72-acmponsc66a227wxpmsfphylcpgoz35d/lib", ":")
prepend_path("LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/perl-html-parser-3.72-acmponsc66a227wxpmsfphylcpgoz35d/lib", ":")
prepend_path("MANPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/perl-html-parser-3.72-acmponsc66a227wxpmsfphylcpgoz35d/man", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/perl-html-parser-3.72-acmponsc66a227wxpmsfphylcpgoz35d/", ":")
prepend_path("PERL5LIB", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/perl-html-parser-3.72-acmponsc66a227wxpmsfphylcpgoz35d/lib/perl5:/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/perl-html-tagset-3.20-a6d5in5qcf5vrgq3zufqupkbp7bgtin3/lib/perl5", ":")

