-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2021-11-30 16:24:22.378642
--
-- perl-html-tagset@3.20%gcc@6.4.0 arch=linux-centos7-broadwell/a6d5in5
--

whatis([[Name : perl-html-tagset]])
whatis([[Version : 3.20]])
whatis([[Target : broadwell]])
whatis([[Short description : Data tables useful in parsing HTML]])

help([[Data tables useful in parsing HTML]])



prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/perl-html-tagset-3.20-a6d5in5qcf5vrgq3zufqupkbp7bgtin3/lib", ":")
prepend_path("LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/perl-html-tagset-3.20-a6d5in5qcf5vrgq3zufqupkbp7bgtin3/lib", ":")
prepend_path("MANPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/perl-html-tagset-3.20-a6d5in5qcf5vrgq3zufqupkbp7bgtin3/man", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/perl-html-tagset-3.20-a6d5in5qcf5vrgq3zufqupkbp7bgtin3/", ":")
prepend_path("PERL5LIB", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/perl-html-tagset-3.20-a6d5in5qcf5vrgq3zufqupkbp7bgtin3/lib/perl5", ":")

