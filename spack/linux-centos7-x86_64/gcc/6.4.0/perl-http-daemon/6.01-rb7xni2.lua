-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2021-11-30 16:31:05.896289
--
-- perl-http-daemon@6.01%gcc@6.4.0 arch=linux-centos7-broadwell/rb7xni2
--

whatis([[Name : perl-http-daemon]])
whatis([[Version : 6.01]])
whatis([[Target : broadwell]])
whatis([[Short description : A simple http server class]])

help([[A simple http server class]])



prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/perl-http-daemon-6.01-rb7xni2a7ogqdbhmz57l4bpmf26j6yhy/lib", ":")
prepend_path("LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/perl-http-daemon-6.01-rb7xni2a7ogqdbhmz57l4bpmf26j6yhy/lib", ":")
prepend_path("MANPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/perl-http-daemon-6.01-rb7xni2a7ogqdbhmz57l4bpmf26j6yhy/man", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/perl-http-daemon-6.01-rb7xni2a7ogqdbhmz57l4bpmf26j6yhy/", ":")
prepend_path("PERL5LIB", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/perl-http-daemon-6.01-rb7xni2a7ogqdbhmz57l4bpmf26j6yhy/lib/perl5:/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/perl-http-date-6.02-jquqfhutkxpbehrzabjoqdnusurylewc/lib/perl5:/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/perl-http-message-6.13-pm2wd5oashzro6xbg5udhye62w7xkefj/lib/perl5:/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/perl-encode-locale-1.05-47niauog2234wufnijxkrbhlebai3u2n/lib/perl5:/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/perl-io-html-1.001-vnsdto7jxmpxfckgxzrcfsvhoq767mm2/lib/perl5:/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/perl-lwp-mediatypes-6.02-n3lkntb2yh4e52pqd3oqom462zk75snx/lib/perl5:/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/perl-try-tiny-0.28-trqua4bralfptukov6hzpr2wfmzt6j7z/lib/perl5:/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/perl-uri-1.72-mmg5pkyojvf6bapceys7aqt4gw2d7z5d/lib/perl5", ":")

