-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2021-11-30 16:24:53.599185
--
-- perl-http-date@6.02%gcc@6.4.0 arch=linux-centos7-broadwell/jquqfhu
--

whatis([[Name : perl-http-date]])
whatis([[Version : 6.02]])
whatis([[Target : broadwell]])
whatis([[Short description : Date conversion routines]])

help([[Date conversion routines]])



prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/perl-http-date-6.02-jquqfhutkxpbehrzabjoqdnusurylewc/lib", ":")
prepend_path("LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/perl-http-date-6.02-jquqfhutkxpbehrzabjoqdnusurylewc/lib", ":")
prepend_path("MANPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/perl-http-date-6.02-jquqfhutkxpbehrzabjoqdnusurylewc/man", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/perl-http-date-6.02-jquqfhutkxpbehrzabjoqdnusurylewc/", ":")
prepend_path("PERL5LIB", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/perl-http-date-6.02-jquqfhutkxpbehrzabjoqdnusurylewc/lib/perl5", ":")

