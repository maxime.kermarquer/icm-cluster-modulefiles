-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2021-11-30 16:25:02.421243
--
-- perl-io-html@1.001%gcc@6.4.0 arch=linux-centos7-broadwell/vnsdto7
--

whatis([[Name : perl-io-html]])
whatis([[Version : 1.001]])
whatis([[Target : broadwell]])
whatis([[Short description : Open an HTML file with automatic charset detection.]])

help([[Open an HTML file with automatic charset detection.]])



prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/perl-io-html-1.001-vnsdto7jxmpxfckgxzrcfsvhoq767mm2/lib", ":")
prepend_path("LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/perl-io-html-1.001-vnsdto7jxmpxfckgxzrcfsvhoq767mm2/lib", ":")
prepend_path("MANPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/perl-io-html-1.001-vnsdto7jxmpxfckgxzrcfsvhoq767mm2/man", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/perl-io-html-1.001-vnsdto7jxmpxfckgxzrcfsvhoq767mm2/", ":")
prepend_path("PERL5LIB", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/perl-io-html-1.001-vnsdto7jxmpxfckgxzrcfsvhoq767mm2/lib/perl5", ":")

