-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2021-11-30 16:24:32.151328
--
-- perl-lwp-mediatypes@6.02%gcc@6.4.0 arch=linux-centos7-broadwell/n3lkntb
--

whatis([[Name : perl-lwp-mediatypes]])
whatis([[Version : 6.02]])
whatis([[Target : broadwell]])
whatis([[Short description : Guess media type for a file or a URL]])

help([[Guess media type for a file or a URL]])



prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/perl-lwp-mediatypes-6.02-n3lkntb2yh4e52pqd3oqom462zk75snx/lib", ":")
prepend_path("LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/perl-lwp-mediatypes-6.02-n3lkntb2yh4e52pqd3oqom462zk75snx/lib", ":")
prepend_path("MANPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/perl-lwp-mediatypes-6.02-n3lkntb2yh4e52pqd3oqom462zk75snx/man", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/perl-lwp-mediatypes-6.02-n3lkntb2yh4e52pqd3oqom462zk75snx/", ":")
prepend_path("PERL5LIB", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/perl-lwp-mediatypes-6.02-n3lkntb2yh4e52pqd3oqom462zk75snx/lib/perl5", ":")

