-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2021-11-30 16:25:38.693596
--
-- perl-module-build-tiny@0.039%gcc@6.4.0 arch=linux-centos7-broadwell/cftud5m
--

whatis([[Name : perl-module-build-tiny]])
whatis([[Version : 0.039]])
whatis([[Target : broadwell]])
whatis([[Short description : Module::Build::Tiny - A tiny replacement for Module::Build]])

help([[Module::Build::Tiny - A tiny replacement for Module::Build]])



prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/perl-module-build-tiny-0.039-cftud5mmvyzndhi3ftwfbdrkua453snk/lib", ":")
prepend_path("LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/perl-module-build-tiny-0.039-cftud5mmvyzndhi3ftwfbdrkua453snk/lib", ":")
prepend_path("MANPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/perl-module-build-tiny-0.039-cftud5mmvyzndhi3ftwfbdrkua453snk/man", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/perl-module-build-tiny-0.039-cftud5mmvyzndhi3ftwfbdrkua453snk/", ":")
prepend_path("PERL5LIB", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/perl-module-build-tiny-0.039-cftud5mmvyzndhi3ftwfbdrkua453snk/lib/perl5:/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/perl-extutils-config-0.008-tqigqpxfjijkocchg6oasntqcus42g5h/lib/perl5:/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/perl-extutils-helpers-0.026-wkwcsp7vmg6s3fs54cme4j2xxtovhszw/lib/perl5:/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/perl-extutils-installpaths-0.012-dc77rzfdl3l6vejkbfub6aqoifhdtedg/lib/perl5", ":")

