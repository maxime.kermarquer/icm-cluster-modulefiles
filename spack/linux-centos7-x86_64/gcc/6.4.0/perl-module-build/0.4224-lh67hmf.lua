-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2021-11-30 16:24:39.759499
--
-- perl-module-build@0.4224%gcc@6.4.0 arch=linux-centos7-broadwell/lh67hmf
--

whatis([[Name : perl-module-build]])
whatis([[Version : 0.4224]])
whatis([[Target : broadwell]])
whatis([[Short description : Module::Build is a system for building, testing, and installing Perl modules. It is meant to be an alternative to ExtUtils::MakeMaker. Developers may alter the behavior of the module through subclassing in a much more straightforward way than with MakeMaker. It also does not require a make on your system - most of the Module::Build code is pure-perl and written in a very cross-platform way. ]])

help([[Module::Build is a system for building, testing, and installing Perl
modules. It is meant to be an alternative to ExtUtils::MakeMaker.
Developers may alter the behavior of the module through subclassing in a
much more straightforward way than with MakeMaker. It also does not
require a make on your system - most of the Module::Build code is pure-
perl and written in a very cross-platform way.]])



prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/perl-module-build-0.4224-lh67hmfyuvacg23rlyjxurnmtccwuhq5/lib", ":")
prepend_path("LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/perl-module-build-0.4224-lh67hmfyuvacg23rlyjxurnmtccwuhq5/lib", ":")
prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/perl-module-build-0.4224-lh67hmfyuvacg23rlyjxurnmtccwuhq5/bin", ":")
prepend_path("MANPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/perl-module-build-0.4224-lh67hmfyuvacg23rlyjxurnmtccwuhq5/man", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/perl-module-build-0.4224-lh67hmfyuvacg23rlyjxurnmtccwuhq5/", ":")
prepend_path("PERL5LIB", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/perl-module-build-0.4224-lh67hmfyuvacg23rlyjxurnmtccwuhq5/lib/perl5", ":")

