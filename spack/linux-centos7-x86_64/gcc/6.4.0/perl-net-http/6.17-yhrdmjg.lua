-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2021-11-30 16:29:36.993696
--
-- perl-net-http@6.17%gcc@6.4.0 arch=linux-centos7-broadwell/yhrdmjg
--

whatis([[Name : perl-net-http]])
whatis([[Version : 6.17]])
whatis([[Target : broadwell]])
whatis([[Short description : Low-level HTTP connection (client)]])

help([[Low-level HTTP connection (client)]])



prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/perl-net-http-6.17-yhrdmjg4blx6xg2khvhkigpcrqyu32or/lib", ":")
prepend_path("LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/perl-net-http-6.17-yhrdmjg4blx6xg2khvhkigpcrqyu32or/lib", ":")
prepend_path("MANPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/perl-net-http-6.17-yhrdmjg4blx6xg2khvhkigpcrqyu32or/man", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/perl-net-http-6.17-yhrdmjg4blx6xg2khvhkigpcrqyu32or/", ":")
prepend_path("PERL5LIB", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/perl-net-http-6.17-yhrdmjg4blx6xg2khvhkigpcrqyu32or/lib/perl5:/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/perl-uri-1.72-mmg5pkyojvf6bapceys7aqt4gw2d7z5d/lib/perl5", ":")

