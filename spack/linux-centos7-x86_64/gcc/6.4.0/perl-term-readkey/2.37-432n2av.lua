-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2019-05-15 17:00:00.777488
--
-- perl-term-readkey@2.37%gcc@6.4.0 arch=linux-centos7-x86_64 /432n2av
--

whatis([[Name : perl-term-readkey]])
whatis([[Version : 2.37]])
whatis([[Short description : Term::ReadKey is a compiled perl module dedicated to providing simple control over terminal driver modes (cbreak, raw, cooked, etc.,) support for non-blocking reads, if the architecture allows, and some generalized handy functions for working with terminals. One of the main goals is to have the functions as portable as possible, so you can just plug in 'use Term::ReadKey' on any architecture and have a good likelihood of it working.]])

help([[Term::ReadKey is a compiled perl module dedicated to providing simple
control over terminal driver modes (cbreak, raw, cooked, etc.,) support
for non-blocking reads, if the architecture allows, and some generalized
handy functions for working with terminals. One of the main goals is to
have the functions as portable as possible, so you can just plug in "use
Term::ReadKey" on any architecture and have a good likelihood of it
working.]])



prepend_path("MANPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/perl-term-readkey-2.37-432n2avirkb2vne2hxskaemzba5vy5zl/man", ":")
prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/perl-term-readkey-2.37-432n2avirkb2vne2hxskaemzba5vy5zl/lib", ":")
prepend_path("LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/perl-term-readkey-2.37-432n2avirkb2vne2hxskaemzba5vy5zl/lib", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/perl-term-readkey-2.37-432n2avirkb2vne2hxskaemzba5vy5zl/", ":")
prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/perl-term-readkey-2.37-432n2avirkb2vne2hxskaemzba5vy5zl/bin", ":")
prepend_path("PERL5LIB", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/perl-term-readkey-2.37-432n2avirkb2vne2hxskaemzba5vy5zl/lib/perl5", ":")

