-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2021-11-30 16:24:57.922817
--
-- perl-test-needs@0.002005%gcc@6.4.0 arch=linux-centos7-broadwell/gbpmocl
--

whatis([[Name : perl-test-needs]])
whatis([[Version : 0.002005]])
whatis([[Target : broadwell]])
whatis([[Short description : Skip tests when modules not available.]])

help([[Skip tests when modules not available.]])



prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/perl-test-needs-0.002005-gbpmocl2y3b4u4fwedbgfrwhptot52rl/lib", ":")
prepend_path("LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/perl-test-needs-0.002005-gbpmocl2y3b4u4fwedbgfrwhptot52rl/lib", ":")
prepend_path("MANPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/perl-test-needs-0.002005-gbpmocl2y3b4u4fwedbgfrwhptot52rl/man", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/perl-test-needs-0.002005-gbpmocl2y3b4u4fwedbgfrwhptot52rl/", ":")
prepend_path("PERL5LIB", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/perl-test-needs-0.002005-gbpmocl2y3b4u4fwedbgfrwhptot52rl/lib/perl5", ":")

