-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2021-11-30 16:25:33.495355
--
-- perl-uri@1.72%gcc@6.4.0 arch=linux-centos7-broadwell/mmg5pky
--

whatis([[Name : perl-uri]])
whatis([[Version : 1.72]])
whatis([[Target : broadwell]])
whatis([[Short description : Uniform Resource Identifiers (absolute and relative)]])

help([[Uniform Resource Identifiers (absolute and relative)]])



prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/perl-uri-1.72-mmg5pkyojvf6bapceys7aqt4gw2d7z5d/lib", ":")
prepend_path("LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/perl-uri-1.72-mmg5pkyojvf6bapceys7aqt4gw2d7z5d/lib", ":")
prepend_path("MANPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/perl-uri-1.72-mmg5pkyojvf6bapceys7aqt4gw2d7z5d/man", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/perl-uri-1.72-mmg5pkyojvf6bapceys7aqt4gw2d7z5d/", ":")
prepend_path("PERL5LIB", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/perl-uri-1.72-mmg5pkyojvf6bapceys7aqt4gw2d7z5d/lib/perl5", ":")

