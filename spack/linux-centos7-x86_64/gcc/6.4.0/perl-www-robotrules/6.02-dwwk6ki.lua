-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2021-11-30 16:29:47.437376
--
-- perl-www-robotrules@6.02%gcc@6.4.0 arch=linux-centos7-broadwell/dwwk6ki
--

whatis([[Name : perl-www-robotrules]])
whatis([[Version : 6.02]])
whatis([[Target : broadwell]])
whatis([[Short description : Database of robots.txt-derived permissions]])

help([[Database of robots.txt-derived permissions]])



prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/perl-www-robotrules-6.02-dwwk6kikmvlinz2esg5pxk4bf3xfteqi/lib", ":")
prepend_path("LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/perl-www-robotrules-6.02-dwwk6kikmvlinz2esg5pxk4bf3xfteqi/lib", ":")
prepend_path("MANPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/perl-www-robotrules-6.02-dwwk6kikmvlinz2esg5pxk4bf3xfteqi/man", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/perl-www-robotrules-6.02-dwwk6kikmvlinz2esg5pxk4bf3xfteqi/", ":")
prepend_path("PERL5LIB", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/perl-www-robotrules-6.02-dwwk6kikmvlinz2esg5pxk4bf3xfteqi/lib/perl5:/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/perl-uri-1.72-mmg5pkyojvf6bapceys7aqt4gw2d7z5d/lib/perl5", ":")

