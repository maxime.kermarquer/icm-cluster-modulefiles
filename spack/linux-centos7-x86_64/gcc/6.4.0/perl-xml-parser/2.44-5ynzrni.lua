-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2019-02-06 19:30:34.205268
--
-- perl-xml-parser@2.44%gcc@6.4.0 arch=linux-centos7-x86_64 /5ynzrni
--

whatis([[Name : perl-xml-parser]])
whatis([[Version : 2.44]])
whatis([[Short description : XML::Parser - A perl module for parsing XML documents]])
whatis([[Configure options : EXPATLIBPATH=/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/expat-2.2.5-etozzgyvfjgsfm756gbcaonunhv4hqma/lib EXPATINCPATH=/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/expat-2.2.5-etozzgyvfjgsfm756gbcaonunhv4hqma/include]])

help([[XML::Parser - A perl module for parsing XML documents]])



prepend_path("MANPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/perl-xml-parser-2.44-5ynzrni47wk6ar6wkemzhuekzbxprn3f/man", ":")
prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/perl-xml-parser-2.44-5ynzrni47wk6ar6wkemzhuekzbxprn3f/lib", ":")
prepend_path("LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/perl-xml-parser-2.44-5ynzrni47wk6ar6wkemzhuekzbxprn3f/lib", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/perl-xml-parser-2.44-5ynzrni47wk6ar6wkemzhuekzbxprn3f/", ":")
prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/perl-xml-parser-2.44-5ynzrni47wk6ar6wkemzhuekzbxprn3f/bin", ":")
prepend_path("PERL5LIB", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/perl-xml-parser-2.44-5ynzrni47wk6ar6wkemzhuekzbxprn3f/lib/perl5", ":")

