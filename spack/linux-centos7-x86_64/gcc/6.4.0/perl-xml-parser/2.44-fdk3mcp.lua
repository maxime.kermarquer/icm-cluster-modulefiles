-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2021-11-30 16:31:28.419953
--
-- perl-xml-parser@2.44%gcc@6.4.0 arch=linux-centos7-broadwell/fdk3mcp
--

whatis([[Name : perl-xml-parser]])
whatis([[Version : 2.44]])
whatis([[Target : broadwell]])
whatis([[Short description : XML::Parser - A perl module for parsing XML documents]])
whatis([[Configure options : EXPATLIBPATH=/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/expat-2.2.10-ibk667vyy4bqx2vthuj4wgefuadbttjm/lib EXPATINCPATH=/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/expat-2.2.10-ibk667vyy4bqx2vthuj4wgefuadbttjm/include]])

help([[XML::Parser - A perl module for parsing XML documents]])



prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/perl-xml-parser-2.44-fdk3mcp4dukzgot4jr3jytlutgxouldt/lib", ":")
prepend_path("LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/perl-xml-parser-2.44-fdk3mcp4dukzgot4jr3jytlutgxouldt/lib", ":")
prepend_path("MANPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/perl-xml-parser-2.44-fdk3mcp4dukzgot4jr3jytlutgxouldt/man", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/perl-xml-parser-2.44-fdk3mcp4dukzgot4jr3jytlutgxouldt/", ":")
prepend_path("PERL5LIB", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/perl-xml-parser-2.44-fdk3mcp4dukzgot4jr3jytlutgxouldt/lib/perl5:/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/perl-libwww-perl-6.33-vnsg7isrnbepflz3ct5uaartn2jcrp2t/lib/perl5:/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/perl-encode-locale-1.05-47niauog2234wufnijxkrbhlebai3u2n/lib/perl5:/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/perl-file-listing-6.04-5es6yxcjhhiqmyet7kbacxjr7ko5nd2x/lib/perl5:/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/perl-http-date-6.02-jquqfhutkxpbehrzabjoqdnusurylewc/lib/perl5:/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/perl-html-parser-3.72-acmponsc66a227wxpmsfphylcpgoz35d/lib/perl5:/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/perl-html-tagset-3.20-a6d5in5qcf5vrgq3zufqupkbp7bgtin3/lib/perl5:/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/perl-http-cookies-6.04-5az24nc7yhmsjlkl5vfeb6z2i7l2lyvd/lib/perl5:/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/perl-http-message-6.13-pm2wd5oashzro6xbg5udhye62w7xkefj/lib/perl5:/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/perl-io-html-1.001-vnsdto7jxmpxfckgxzrcfsvhoq767mm2/lib/perl5:/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/perl-lwp-mediatypes-6.02-n3lkntb2yh4e52pqd3oqom462zk75snx/lib/perl5:/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/perl-try-tiny-0.28-trqua4bralfptukov6hzpr2wfmzt6j7z/lib/perl5:/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/perl-uri-1.72-mmg5pkyojvf6bapceys7aqt4gw2d7z5d/lib/perl5:/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/perl-http-daemon-6.01-rb7xni2a7ogqdbhmz57l4bpmf26j6yhy/lib/perl5:/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/perl-http-negotiate-6.01-imflso4kbbcnsknm3sspeldkw53m6eov/lib/perl5:/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/perl-net-http-6.17-yhrdmjg4blx6xg2khvhkigpcrqyu32or/lib/perl5:/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/perl-www-robotrules-6.02-dwwk6kikmvlinz2esg5pxk4bf3xfteqi/lib/perl5", ":")

