-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2021-01-13 17:27:19.631741
--
-- perl@5.32.0%gcc@6.4.0+cpanm+shared+threads arch=linux-centos7-broadwell/cnlqxef
--

whatis([[Name : perl]])
whatis([[Version : 5.32.0]])
whatis([[Target : broadwell]])
whatis([[Short description : Perl 5 is a highly capable, feature-rich programming language with over 27 years of development.]])
whatis([[Configure options : -des -Dprefix=/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/perl-5.32.0-cnlqxeflbhegnsxbwlallv5k3n6nwjoy -Dlocincpth=/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/gdbm-1.18.1-boj5sgi36mmh7jskcuamd2zi6pf3zcpa/include -Dloclibpth=/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/gdbm-1.18.1-boj5sgi36mmh7jskcuamd2zi6pf3zcpa/lib -Accflags=-DAPPLLIB_EXP=\"/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/perl-5.32.0-cnlqxeflbhegnsxbwlallv5k3n6nwjoy/lib/perl5\" -Duseshrplib -Dusethreads]])

help([[Perl 5 is a highly capable, feature-rich programming language with over
27 years of development.]])



prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/perl-5.32.0-cnlqxeflbhegnsxbwlallv5k3n6nwjoy/lib", ":")
prepend_path("LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/perl-5.32.0-cnlqxeflbhegnsxbwlallv5k3n6nwjoy/lib", ":")
prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/perl-5.32.0-cnlqxeflbhegnsxbwlallv5k3n6nwjoy/bin", ":")
prepend_path("MANPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/perl-5.32.0-cnlqxeflbhegnsxbwlallv5k3n6nwjoy/man", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/perl-5.32.0-cnlqxeflbhegnsxbwlallv5k3n6nwjoy/", ":")

