-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2019-02-06 18:48:48.684227
--
-- pixman@0.34.0%gcc@6.4.0 arch=linux-centos7-x86_64 /y63f2v4
--

whatis([[Name : pixman]])
whatis([[Version : 0.34.0]])
whatis([[Short description : The Pixman package contains a library that provides low-level pixel manipulation features such as image compositing and trapezoid rasterization.]])
whatis([[Configure options : --enable-libpng --disable-gtk]])

help([[The Pixman package contains a library that provides low-level pixel
manipulation features such as image compositing and trapezoid
rasterization.]])



prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/pixman-0.34.0-y63f2v4ltx3jc3wjnll7xmpr7j7tjqhu/lib", ":")
prepend_path("LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/pixman-0.34.0-y63f2v4ltx3jc3wjnll7xmpr7j7tjqhu/lib", ":")
prepend_path("CPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/pixman-0.34.0-y63f2v4ltx3jc3wjnll7xmpr7j7tjqhu/include", ":")
prepend_path("PKG_CONFIG_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/pixman-0.34.0-y63f2v4ltx3jc3wjnll7xmpr7j7tjqhu/lib/pkgconfig", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/pixman-0.34.0-y63f2v4ltx3jc3wjnll7xmpr7j7tjqhu/", ":")

