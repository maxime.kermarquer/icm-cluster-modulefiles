-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2021-11-30 16:24:07.859556
--
-- pixman@0.40.0%gcc@6.4.0 arch=linux-centos7-broadwell/cdhyk76
--

whatis([[Name : pixman]])
whatis([[Version : 0.40.0]])
whatis([[Target : broadwell]])
whatis([[Short description : The Pixman package contains a library that provides low-level pixel manipulation features such as image compositing and trapezoid rasterization.]])
whatis([[Configure options : --enable-libpng --disable-gtk]])

help([[The Pixman package contains a library that provides low-level pixel
manipulation features such as image compositing and trapezoid
rasterization.]])



prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/pixman-0.40.0-cdhyk76lss3bv2cylwabp4fonegthnb4/lib", ":")
prepend_path("LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/pixman-0.40.0-cdhyk76lss3bv2cylwabp4fonegthnb4/lib", ":")
prepend_path("C_INCLUDE_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/pixman-0.40.0-cdhyk76lss3bv2cylwabp4fonegthnb4/include", ":")
prepend_path("CPLUS_INCLUDE_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/pixman-0.40.0-cdhyk76lss3bv2cylwabp4fonegthnb4/include", ":")
prepend_path("INCLUDE", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/pixman-0.40.0-cdhyk76lss3bv2cylwabp4fonegthnb4/include", ":")
prepend_path("PKG_CONFIG_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/pixman-0.40.0-cdhyk76lss3bv2cylwabp4fonegthnb4/lib/pkgconfig", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/pixman-0.40.0-cdhyk76lss3bv2cylwabp4fonegthnb4/", ":")

