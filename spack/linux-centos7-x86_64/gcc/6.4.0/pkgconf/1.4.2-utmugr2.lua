-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2018-09-17 15:52:05.683860
--
-- pkgconf@1.4.2%gcc@6.4.0 arch=linux-centos7-x86_64 /utmugr2
--

whatis([[Name : pkgconf]])
whatis([[Version : 1.4.2]])
whatis([[Short description : pkgconf is a program which helps to configure compiler and linker flags for development frameworks. It is similar to pkg-config from freedesktop.org, providing additional functionality while also maintaining compatibility.]])

help([[pkgconf is a program which helps to configure compiler and linker flags
for development frameworks. It is similar to pkg-config from
freedesktop.org, providing additional functionality while also
maintaining compatibility.]])



prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/pkgconf-1.4.2-utmugr24kuv55uw26zm7gjfhblpkqzqv/bin", ":")
prepend_path("MANPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/pkgconf-1.4.2-utmugr24kuv55uw26zm7gjfhblpkqzqv/share/man", ":")
prepend_path("ACLOCAL_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/pkgconf-1.4.2-utmugr24kuv55uw26zm7gjfhblpkqzqv/share/aclocal", ":")
prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/pkgconf-1.4.2-utmugr24kuv55uw26zm7gjfhblpkqzqv/lib", ":")
prepend_path("LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/pkgconf-1.4.2-utmugr24kuv55uw26zm7gjfhblpkqzqv/lib", ":")
prepend_path("CPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/pkgconf-1.4.2-utmugr24kuv55uw26zm7gjfhblpkqzqv/include", ":")
prepend_path("PKG_CONFIG_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/pkgconf-1.4.2-utmugr24kuv55uw26zm7gjfhblpkqzqv/lib/pkgconfig", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/pkgconf-1.4.2-utmugr24kuv55uw26zm7gjfhblpkqzqv/", ":")

