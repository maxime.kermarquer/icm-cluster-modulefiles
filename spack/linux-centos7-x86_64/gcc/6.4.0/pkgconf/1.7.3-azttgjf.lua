-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2021-01-13 16:46:27.473900
--
-- pkgconf@1.7.3%gcc@6.4.0 arch=linux-centos7-broadwell/azttgjf
--

whatis([[Name : pkgconf]])
whatis([[Version : 1.7.3]])
whatis([[Target : broadwell]])
whatis([[Short description : pkgconf is a program which helps to configure compiler and linker flags for development frameworks. It is similar to pkg-config from freedesktop.org, providing additional functionality while also maintaining compatibility.]])

help([[pkgconf is a program which helps to configure compiler and linker flags
for development frameworks. It is similar to pkg-config from
freedesktop.org, providing additional functionality while also
maintaining compatibility.]])



prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/pkgconf-1.7.3-azttgjfcavq5oarzm6u57b3spixtoteh/lib", ":")
prepend_path("LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/pkgconf-1.7.3-azttgjfcavq5oarzm6u57b3spixtoteh/lib", ":")
prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/pkgconf-1.7.3-azttgjfcavq5oarzm6u57b3spixtoteh/bin", ":")
prepend_path("MANPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/pkgconf-1.7.3-azttgjfcavq5oarzm6u57b3spixtoteh/share/man", ":")
prepend_path("ACLOCAL_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/pkgconf-1.7.3-azttgjfcavq5oarzm6u57b3spixtoteh/share/aclocal", ":")
prepend_path("C_INCLUDE_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/pkgconf-1.7.3-azttgjfcavq5oarzm6u57b3spixtoteh/include", ":")
prepend_path("CPLUS_INCLUDE_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/pkgconf-1.7.3-azttgjfcavq5oarzm6u57b3spixtoteh/include", ":")
prepend_path("INCLUDE", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/pkgconf-1.7.3-azttgjfcavq5oarzm6u57b3spixtoteh/include", ":")
prepend_path("PKG_CONFIG_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/pkgconf-1.7.3-azttgjfcavq5oarzm6u57b3spixtoteh/lib/pkgconfig", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/pkgconf-1.7.3-azttgjfcavq5oarzm6u57b3spixtoteh/", ":")

