-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2020-12-27 23:18:16.032720
--
-- pmix@2.1.1%gcc@6.4.0 arch=linux-centos7-x86_64 /y34grfm
--

whatis([[Name : pmix]])
whatis([[Version : 2.1.1]])
whatis([[Short description : The Process Management Interface (PMI) has been used for quite some time as a means of exchanging wireup information needed for interprocess communication. Two versions (PMI-1 and PMI-2) have been released as part of the MPICH effort. While PMI-2 demonstrates better scaling properties than its PMI-1 predecessor, attaining rapid launch and wireup of the roughly 1M processes executing across 100k nodes expected for exascale operations remains challenging. PMI Exascale (PMIx) represents an attempt to resolve these questions by providing an extended version of the PMI definitions specifically designed to support clusters up to and including exascale sizes. The overall objective of the project is not to branch the existing definitions - in fact, PMIx fully supports both of the existing PMI-1 and PMI-2 APIs - but rather to (a) augment and extend those APIs to eliminate some current restrictions that impact scalability, (b) establish a standards-like body for maintaining the definitions, and (c) provide a reference implementation of the PMIx standard that demonstrates the desired level of scalability.]])
whatis([[Configure options : --enable-shared --enable-static --with-libevent=/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/libevent-2.0.21-35iviu66vp4uym7xsqlnvuc6rweat7vk]])

help([[The Process Management Interface (PMI) has been used for quite some time
as a means of exchanging wireup information needed for interprocess
communication. Two versions (PMI-1 and PMI-2) have been released as part
of the MPICH effort. While PMI-2 demonstrates better scaling properties
than its PMI-1 predecessor, attaining rapid launch and wireup of the
roughly 1M processes executing across 100k nodes expected for exascale
operations remains challenging. PMI Exascale (PMIx) represents an
attempt to resolve these questions by providing an extended version of
the PMI definitions specifically designed to support clusters up to and
including exascale sizes. The overall objective of the project is not to
branch the existing definitions - in fact, PMIx fully supports both of
the existing PMI-1 and PMI-2 APIs - but rather to (a) augment and extend
those APIs to eliminate some current restrictions that impact
scalability, (b) establish a standards-like body for maintaining the
definitions, and (c) provide a reference implementation of the PMIx
standard that demonstrates the desired level of scalability.]])



prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/pmix-2.1.1-y34grfmziecrjddujpwelsgbin347cnr/lib", ":")
prepend_path("LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/pmix-2.1.1-y34grfmziecrjddujpwelsgbin347cnr/lib", ":")
prepend_path("CPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/pmix-2.1.1-y34grfmziecrjddujpwelsgbin347cnr/include", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/pmix-2.1.1-y34grfmziecrjddujpwelsgbin347cnr/", ":")

