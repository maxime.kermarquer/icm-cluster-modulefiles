-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2021-01-13 17:01:58.134793
--
-- popt@1.16%gcc@6.4.0 arch=linux-centos7-broadwell/utedvwk
--

whatis([[Name : popt]])
whatis([[Version : 1.16]])
whatis([[Target : broadwell]])
whatis([[Short description : The popt library parses command line options.]])

help([[The popt library parses command line options.]])



prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/popt-1.16-utedvwkewjcd7dexc6tsfcsw54pwdx26/lib", ":")
prepend_path("LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/popt-1.16-utedvwkewjcd7dexc6tsfcsw54pwdx26/lib", ":")
prepend_path("MANPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/popt-1.16-utedvwkewjcd7dexc6tsfcsw54pwdx26/share/man", ":")
prepend_path("C_INCLUDE_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/popt-1.16-utedvwkewjcd7dexc6tsfcsw54pwdx26/include", ":")
prepend_path("CPLUS_INCLUDE_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/popt-1.16-utedvwkewjcd7dexc6tsfcsw54pwdx26/include", ":")
prepend_path("INCLUDE", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/popt-1.16-utedvwkewjcd7dexc6tsfcsw54pwdx26/include", ":")
prepend_path("PKG_CONFIG_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/popt-1.16-utedvwkewjcd7dexc6tsfcsw54pwdx26/lib/pkgconfig", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/popt-1.16-utedvwkewjcd7dexc6tsfcsw54pwdx26/", ":")

