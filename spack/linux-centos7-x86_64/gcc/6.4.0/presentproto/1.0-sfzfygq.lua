-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2018-09-19 11:09:54.858580
--
-- presentproto@1.0%gcc@6.4.0 arch=linux-centos7-x86_64 /sfzfygq
--

whatis([[Name : presentproto]])
whatis([[Version : 1.0]])
whatis([[Short description : Present protocol specification and Xlib/Xserver headers.]])

help([[Present protocol specification and Xlib/Xserver headers.]])



prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/presentproto-1.0-sfzfygqz2gtp6r7xzw5jtallpxjda423/lib", ":")
prepend_path("LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/presentproto-1.0-sfzfygqz2gtp6r7xzw5jtallpxjda423/lib", ":")
prepend_path("CPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/presentproto-1.0-sfzfygqz2gtp6r7xzw5jtallpxjda423/include", ":")
prepend_path("PKG_CONFIG_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/presentproto-1.0-sfzfygqz2gtp6r7xzw5jtallpxjda423/lib/pkgconfig", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/presentproto-1.0-sfzfygqz2gtp6r7xzw5jtallpxjda423/", ":")

