-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2019-10-25 17:04:51.749039
--
-- printproto@1.0.5%gcc@6.4.0 arch=linux-centos7-x86_64 /tp6nce5
--

whatis([[Name : printproto]])
whatis([[Version : 1.0.5]])
whatis([[Short description : Xprint extension to the X11 protocol - a portable, network-transparent printing system.]])

help([[Xprint extension to the X11 protocol - a portable, network-transparent
printing system.]])



prepend_path("MANPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/printproto-1.0.5-tp6nce5vvf7iwssttiaqlyxgo5ql3mra/share/man", ":")
prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/printproto-1.0.5-tp6nce5vvf7iwssttiaqlyxgo5ql3mra/lib", ":")
prepend_path("LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/printproto-1.0.5-tp6nce5vvf7iwssttiaqlyxgo5ql3mra/lib", ":")
prepend_path("CPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/printproto-1.0.5-tp6nce5vvf7iwssttiaqlyxgo5ql3mra/include", ":")
prepend_path("PKG_CONFIG_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/printproto-1.0.5-tp6nce5vvf7iwssttiaqlyxgo5ql3mra/lib/pkgconfig", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/printproto-1.0.5-tp6nce5vvf7iwssttiaqlyxgo5ql3mra/", ":")

