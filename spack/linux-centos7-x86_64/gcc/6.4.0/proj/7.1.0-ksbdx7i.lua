-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2021-10-14 18:50:20.901062
--
-- proj@7.1.0%gcc@6.4.0+curl+tiff arch=linux-centos7-broadwell/ksbdx7i
--

whatis([[Name : proj]])
whatis([[Version : 7.1.0]])
whatis([[Target : broadwell]])
whatis([[Short description : PROJ is a generic coordinate transformation software, that transforms geospatial coordinates from one coordinate reference system (CRS) to another. This includes cartographic projections as well as geodetic transformations.]])
whatis([[Configure options : PROJ_LIB=/network/lustre/iss01/home/maxime.kermarquer/old-scratch/maxime.kermarquer/tmp/spack-stage-proj-7.1.0-ksbdx7icgjlzhqrylhbcpi3t3p5mj4wr/spack-src/nad --with-external-gtest --enable-tiff --with-curl=/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/curl-7.74.0-uspov5cgjhwhmwedyzn66britqnfe6ce/bin/curl-config]])

help([[PROJ is a generic coordinate transformation software, that transforms
geospatial coordinates from one coordinate reference system (CRS) to
another. This includes cartographic projections as well as geodetic
transformations.]])



prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/proj-7.1.0-ksbdx7icgjlzhqrylhbcpi3t3p5mj4wr/lib", ":")
prepend_path("LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/proj-7.1.0-ksbdx7icgjlzhqrylhbcpi3t3p5mj4wr/lib", ":")
prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/proj-7.1.0-ksbdx7icgjlzhqrylhbcpi3t3p5mj4wr/bin", ":")
prepend_path("MANPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/proj-7.1.0-ksbdx7icgjlzhqrylhbcpi3t3p5mj4wr/share/man", ":")
prepend_path("C_INCLUDE_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/proj-7.1.0-ksbdx7icgjlzhqrylhbcpi3t3p5mj4wr/include", ":")
prepend_path("CPLUS_INCLUDE_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/proj-7.1.0-ksbdx7icgjlzhqrylhbcpi3t3p5mj4wr/include", ":")
prepend_path("INCLUDE", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/proj-7.1.0-ksbdx7icgjlzhqrylhbcpi3t3p5mj4wr/include", ":")
prepend_path("PKG_CONFIG_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/proj-7.1.0-ksbdx7icgjlzhqrylhbcpi3t3p5mj4wr/lib/pkgconfig", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/proj-7.1.0-ksbdx7icgjlzhqrylhbcpi3t3p5mj4wr/", ":")

