-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2021-01-13 17:51:26.036532
--
-- py-cython@0.29.21%gcc@6.4.0 arch=linux-centos7-broadwell/yivvs7d
--

whatis([[Name : py-cython]])
whatis([[Version : 0.29.21]])
whatis([[Target : broadwell]])
whatis([[Short description : The Cython compiler for writing C extensions for the Python language.]])

help([[The Cython compiler for writing C extensions for the Python language.]])



prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/py-cython-0.29.21-yivvs7dqshqutht644skla6a2dr7nkk6/lib", ":")
prepend_path("LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/py-cython-0.29.21-yivvs7dqshqutht644skla6a2dr7nkk6/lib", ":")
prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/py-cython-0.29.21-yivvs7dqshqutht644skla6a2dr7nkk6/bin", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/py-cython-0.29.21-yivvs7dqshqutht644skla6a2dr7nkk6/", ":")
prepend_path("PYTHONPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/py-cython-0.29.21-yivvs7dqshqutht644skla6a2dr7nkk6/lib/python3.8/site-packages:/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/py-setuptools-50.3.2-ynbx5if5cmwdxitjjakrje4m4d3ni2zz/lib/python3.8/site-packages", ":")

