-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2018-09-19 11:13:25.410006
--
-- py-mako@1.0.4%gcc@6.4.0 arch=linux-centos7-x86_64 /bnwxh3g
--

whatis([[Name : py-mako]])
whatis([[Version : 1.0.4]])
whatis([[Short description : A super-fast templating language that borrows the best ideas from the existing templating languages.]])

help([[A super-fast templating language that borrows the best ideas from the
existing templating languages.]])



prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/py-mako-1.0.4-bnwxh3gyqmjg4nviqdnds4qvrel2bn7d/bin", ":")
prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/py-mako-1.0.4-bnwxh3gyqmjg4nviqdnds4qvrel2bn7d/lib", ":")
prepend_path("LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/py-mako-1.0.4-bnwxh3gyqmjg4nviqdnds4qvrel2bn7d/lib", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/py-mako-1.0.4-bnwxh3gyqmjg4nviqdnds4qvrel2bn7d/", ":")
prepend_path("PYTHONPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/py-mako-1.0.4-bnwxh3gyqmjg4nviqdnds4qvrel2bn7d/lib/python2.7/site-packages", ":")

