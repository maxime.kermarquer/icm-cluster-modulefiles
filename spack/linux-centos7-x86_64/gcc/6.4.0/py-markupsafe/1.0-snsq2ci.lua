-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2018-09-19 11:13:22.817040
--
-- py-markupsafe@1.0%gcc@6.4.0 arch=linux-centos7-x86_64 /snsq2ci
--

whatis([[Name : py-markupsafe]])
whatis([[Version : 1.0]])
whatis([[Short description : MarkupSafe is a library for Python that implements a unicode string that is aware of HTML escaping rules and can be used to implement automatic string escaping. It is used by Jinja 2, the Mako templating engine, the Pylons web framework and many more.]])

help([[MarkupSafe is a library for Python that implements a unicode string that
is aware of HTML escaping rules and can be used to implement automatic
string escaping. It is used by Jinja 2, the Mako templating engine, the
Pylons web framework and many more.]])



prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/py-markupsafe-1.0-snsq2cil54xmz3unrzzmpuopzygccxr2/lib", ":")
prepend_path("LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/py-markupsafe-1.0-snsq2cil54xmz3unrzzmpuopzygccxr2/lib", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/py-markupsafe-1.0-snsq2cil54xmz3unrzzmpuopzygccxr2/", ":")
prepend_path("PYTHONPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/py-markupsafe-1.0-snsq2cil54xmz3unrzzmpuopzygccxr2/lib/python2.7/site-packages", ":")

