-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2018-09-19 11:13:20.319687
--
-- py-setuptools@40.2.0%gcc@6.4.0 arch=linux-centos7-x86_64 /5dhwuun
--

whatis([[Name : py-setuptools]])
whatis([[Version : 40.2.0]])
whatis([[Short description : A Python utility that aids in the process of downloading, building, upgrading, installing, and uninstalling Python packages.]])

help([[A Python utility that aids in the process of downloading, building,
upgrading, installing, and uninstalling Python packages.]])



prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/py-setuptools-40.2.0-5dhwuunn2pjyimrpfi54k23cvxbkyqb4/bin", ":")
prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/py-setuptools-40.2.0-5dhwuunn2pjyimrpfi54k23cvxbkyqb4/lib", ":")
prepend_path("LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/py-setuptools-40.2.0-5dhwuunn2pjyimrpfi54k23cvxbkyqb4/lib", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/py-setuptools-40.2.0-5dhwuunn2pjyimrpfi54k23cvxbkyqb4/", ":")
prepend_path("PYTHONPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/py-setuptools-40.2.0-5dhwuunn2pjyimrpfi54k23cvxbkyqb4/lib/python2.7/site-packages", ":")

