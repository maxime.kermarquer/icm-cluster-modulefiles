-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2021-01-13 17:45:44.118054
--
-- py-setuptools@50.3.2%gcc@6.4.0 arch=linux-centos7-broadwell/ynbx5if
--

whatis([[Name : py-setuptools]])
whatis([[Version : 50.3.2]])
whatis([[Target : broadwell]])
whatis([[Short description : A Python utility that aids in the process of downloading, building, upgrading, installing, and uninstalling Python packages.]])

help([[A Python utility that aids in the process of downloading, building,
upgrading, installing, and uninstalling Python packages.]])



prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/py-setuptools-50.3.2-ynbx5if5cmwdxitjjakrje4m4d3ni2zz/lib", ":")
prepend_path("LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/py-setuptools-50.3.2-ynbx5if5cmwdxitjjakrje4m4d3ni2zz/lib", ":")
prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/py-setuptools-50.3.2-ynbx5if5cmwdxitjjakrje4m4d3ni2zz/bin", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/py-setuptools-50.3.2-ynbx5if5cmwdxitjjakrje4m4d3ni2zz/", ":")
prepend_path("PYTHONPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/py-setuptools-50.3.2-ynbx5if5cmwdxitjjakrje4m4d3ni2zz/lib/python3.8/site-packages", ":")

