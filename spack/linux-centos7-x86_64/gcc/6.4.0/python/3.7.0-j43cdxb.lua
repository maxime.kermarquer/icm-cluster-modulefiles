-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2018-12-05 18:47:08.458068
--
-- python@3.7.0%gcc@6.4.0+dbm~optimizations patches=123082ab3483ded78e86d7c809e98a804b3465b4683c96bd79a2fd799f572244 +pic+pythoncmd+shared~tk~ucs4 arch=linux-centos7-x86_64 /j43cdxb
--

whatis([[Name : python]])
whatis([[Version : 3.7.0]])
whatis([[Short description : The Python programming language.]])
whatis([[Configure options : --with-threads CPPFLAGS=-I/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/bzip2-1.0.6-zfy3jckgmrl5yq44f5hscczcw7k2iihk/include -I/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/ncurses-6.1-ree7wypg73ztyvthpzod4cy26sojrfpw/include -I/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/zlib-1.2.11-ufwqfd4zrwuuu7fmguamaqokn5plxyns/include -I/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/openssl-1.0.2o-qxfzezxhp4blvxkj6vsnj43vpiqfjs3x/include -I/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/sqlite-3.23.1-fyylzue7vrp4z2f2bqq33celllfezgti/include -I/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/readline-7.0-56pj7kgchwfnsrilpn2khnokfqscfjsf/include -I/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/gdbm-1.14.1-l3qnkor5t5xp7jeopgfi6umkwwvgz664/include LDFLAGS=-L/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/bzip2-1.0.6-zfy3jckgmrl5yq44f5hscczcw7k2iihk/lib -L/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/ncurses-6.1-ree7wypg73ztyvthpzod4cy26sojrfpw/lib -L/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/zlib-1.2.11-ufwqfd4zrwuuu7fmguamaqokn5plxyns/lib -L/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/openssl-1.0.2o-qxfzezxhp4blvxkj6vsnj43vpiqfjs3x/lib -L/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/sqlite-3.23.1-fyylzue7vrp4z2f2bqq33celllfezgti/lib -L/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/readline-7.0-56pj7kgchwfnsrilpn2khnokfqscfjsf/lib -L/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/gdbm-1.14.1-l3qnkor5t5xp7jeopgfi6umkwwvgz664/lib --enable-shared --without-ensurepip CFLAGS=-fPIC]])

help([[The Python programming language.]])



prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/python-3.7.0-j43cdxboelcgdwol25j63mqwdkwt2qne/bin", ":")
prepend_path("MANPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/python-3.7.0-j43cdxboelcgdwol25j63mqwdkwt2qne/share/man", ":")
prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/python-3.7.0-j43cdxboelcgdwol25j63mqwdkwt2qne/lib", ":")
prepend_path("LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/python-3.7.0-j43cdxboelcgdwol25j63mqwdkwt2qne/lib", ":")
prepend_path("CPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/python-3.7.0-j43cdxboelcgdwol25j63mqwdkwt2qne/include", ":")
prepend_path("PKG_CONFIG_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/python-3.7.0-j43cdxboelcgdwol25j63mqwdkwt2qne/lib/pkgconfig", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/python-3.7.0-j43cdxboelcgdwol25j63mqwdkwt2qne/", ":")

