-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2018-09-19 12:00:30.817806
--
-- qt@5.10.0%gcc@6.4.0~dbus~examples~gtk~krellpatch+opengl patches=7f34d48d2faaa108dc3fcc47187af1ccd1d37ee0f931b42597b820f03a99864c,c52f72dac7fdff5a296467536cc9ea024d78f94b49903286395f53fd0eb66e5e ~phonon~webkit arch=linux-centos7-x86_64 /3rgzboc
--

whatis([[Name : qt]])
whatis([[Version : 5.10.0]])
whatis([[Short description : Qt is a comprehensive cross-platform C++ application framework.]])

help([[Qt is a comprehensive cross-platform C++ application framework.]])



prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/qt-5.10.0-3rgzboc3nzqymso7t3mvrtj7wmuiwkmq/bin", ":")
prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/qt-5.10.0-3rgzboc3nzqymso7t3mvrtj7wmuiwkmq/lib", ":")
prepend_path("LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/qt-5.10.0-3rgzboc3nzqymso7t3mvrtj7wmuiwkmq/lib", ":")
prepend_path("CPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/qt-5.10.0-3rgzboc3nzqymso7t3mvrtj7wmuiwkmq/include", ":")
prepend_path("PKG_CONFIG_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/qt-5.10.0-3rgzboc3nzqymso7t3mvrtj7wmuiwkmq/lib/pkgconfig", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/qt-5.10.0-3rgzboc3nzqymso7t3mvrtj7wmuiwkmq/", ":")
prepend_path("CPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/libxml2-2.9.8-m3rdnxm66wlzs5gqsm5pqlzqvideutnk/include/libxml2", ":")
prepend_path("CPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/freetype-2.7.1-fnguk6ypqcpy23j3j2n3l62sjvfd3sf2/include/freetype2", ":")
setenv("QTDIR", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/qt-5.10.0-3rgzboc3nzqymso7t3mvrtj7wmuiwkmq")

