-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2018-09-19 12:01:09.623492
--
-- qwt@6.1.3%gcc@6.4.0 arch=linux-centos7-x86_64 /qutmscq
--

whatis([[Name : qwt]])
whatis([[Version : 6.1.3]])
whatis([[Short description : The Qwt library contains GUI Components and utility classes which are primarily useful for programs with a technical background. Beside a framework for 2D plots it provides scales, sliders, dials, compasses, thermometers, wheels and knobs to control or display values, arrays, or ranges of type double. ]])

help([[The Qwt library contains GUI Components and utility classes which are
primarily useful for programs with a technical background. Beside a
framework for 2D plots it provides scales, sliders, dials, compasses,
thermometers, wheels and knobs to control or display values, arrays, or
ranges of type double.]])



prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/qwt-6.1.3-qutmscqjraesfwwgnc62ss3w7vz2jbgr/lib", ":")
prepend_path("LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/qwt-6.1.3-qutmscqjraesfwwgnc62ss3w7vz2jbgr/lib", ":")
prepend_path("CPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/qwt-6.1.3-qutmscqjraesfwwgnc62ss3w7vz2jbgr/include", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/qwt-6.1.3-qutmscqjraesfwwgnc62ss3w7vz2jbgr/", ":")
prepend_path("CPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/libxml2-2.9.8-m3rdnxm66wlzs5gqsm5pqlzqvideutnk/include/libxml2", ":")
prepend_path("CPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/freetype-2.7.1-fnguk6ypqcpy23j3j2n3l62sjvfd3sf2/include/freetype2", ":")

