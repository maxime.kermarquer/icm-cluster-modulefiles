-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2019-03-20 14:50:35.899243
--
-- randrproto@1.5.0%gcc@6.4.0 arch=linux-centos7-x86_64 /sheu4b5
--

whatis([[Name : randrproto]])
whatis([[Version : 1.5.0]])
whatis([[Short description : X Resize and Rotate Extension (RandR).]])

help([[X Resize and Rotate Extension (RandR). This extension defines a protocol
for clients to dynamically change X screens, so as to resize, rotate and
reflect the root window of a screen.]])



prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/randrproto-1.5.0-sheu4b5eunbp2mnkdmpzmqvvy6uu7m32/lib", ":")
prepend_path("LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/randrproto-1.5.0-sheu4b5eunbp2mnkdmpzmqvvy6uu7m32/lib", ":")
prepend_path("CPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/randrproto-1.5.0-sheu4b5eunbp2mnkdmpzmqvvy6uu7m32/include", ":")
prepend_path("PKG_CONFIG_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/randrproto-1.5.0-sheu4b5eunbp2mnkdmpzmqvvy6uu7m32/lib/pkgconfig", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/randrproto-1.5.0-sheu4b5eunbp2mnkdmpzmqvvy6uu7m32/", ":")

