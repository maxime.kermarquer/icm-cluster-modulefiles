-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2018-09-18 11:31:39.755443
--
-- readline@7.0%gcc@6.4.0 arch=linux-centos7-x86_64 /56pj7kg
--

whatis([[Name : readline]])
whatis([[Version : 7.0]])
whatis([[Short description : The GNU Readline library provides a set of functions for use by applications that allow users to edit command lines as they are typed in. Both Emacs and vi editing modes are available. The Readline library includes additional functions to maintain a list of previously-entered command lines, to recall and perhaps reedit those lines, and perform csh-like history expansion on previous commands.]])

help([[The GNU Readline library provides a set of functions for use by
applications that allow users to edit command lines as they are typed
in. Both Emacs and vi editing modes are available. The Readline library
includes additional functions to maintain a list of previously-entered
command lines, to recall and perhaps reedit those lines, and perform
csh-like history expansion on previous commands.]])



prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/readline-7.0-56pj7kgchwfnsrilpn2khnokfqscfjsf/bin", ":")
prepend_path("MANPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/readline-7.0-56pj7kgchwfnsrilpn2khnokfqscfjsf/share/man", ":")
prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/readline-7.0-56pj7kgchwfnsrilpn2khnokfqscfjsf/lib", ":")
prepend_path("LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/readline-7.0-56pj7kgchwfnsrilpn2khnokfqscfjsf/lib", ":")
prepend_path("CPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/readline-7.0-56pj7kgchwfnsrilpn2khnokfqscfjsf/include", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/readline-7.0-56pj7kgchwfnsrilpn2khnokfqscfjsf/", ":")

