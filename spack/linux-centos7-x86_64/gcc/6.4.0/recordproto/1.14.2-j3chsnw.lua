-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2019-09-16 16:30:46.187352
--
-- recordproto@1.14.2%gcc@6.4.0 arch=linux-centos7-x86_64 /j3chsnw
--

whatis([[Name : recordproto]])
whatis([[Version : 1.14.2]])
whatis([[Short description : X Record Extension.]])

help([[X Record Extension. This extension defines a protocol for the recording
and playback of user actions in the X Window System.]])



prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/recordproto-1.14.2-j3chsnwrr4npye6r45em5zyivog4aqv6/lib", ":")
prepend_path("LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/recordproto-1.14.2-j3chsnwrr4npye6r45em5zyivog4aqv6/lib", ":")
prepend_path("CPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/recordproto-1.14.2-j3chsnwrr4npye6r45em5zyivog4aqv6/include", ":")
prepend_path("PKG_CONFIG_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/recordproto-1.14.2-j3chsnwrr4npye6r45em5zyivog4aqv6/lib/pkgconfig", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/recordproto-1.14.2-j3chsnwrr4npye6r45em5zyivog4aqv6/", ":")

