-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2023-05-17 16:13:54.627235
--
-- renderproto@0.11.1%gcc@6.4.0 build_system=autotools arch=linux-centos7-broadwell/neuwjvx
--

whatis([[Name : renderproto]])
whatis([[Version : 0.11.1]])
whatis([[Target : broadwell]])
whatis([[Short description : X Rendering Extension.]])

help([[Name   : renderproto]])
help([[Version: 0.11.1]])
help([[Target : broadwell]])
help()
help([[X Rendering Extension. This extension defines the protcol for a digital
image composition as the foundation of a new rendering model within the
X Window System.]])



prepend_path("PKG_CONFIG_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/renderproto-0.11.1-neuwjvx6h4qc3pvqt7wzb6kcf3sq43up/lib/pkgconfig", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/renderproto-0.11.1-neuwjvx6h4qc3pvqt7wzb6kcf3sq43up/.", ":")

