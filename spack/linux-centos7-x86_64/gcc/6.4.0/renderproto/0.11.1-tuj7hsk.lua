-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2018-11-02 10:57:10.322509
--
-- renderproto@0.11.1%gcc@6.4.0 arch=linux-centos7-x86_64 /tuj7hsk
--

whatis([[Name : renderproto]])
whatis([[Version : 0.11.1]])
whatis([[Short description : X Rendering Extension.]])

help([[X Rendering Extension. This extension defines the protcol for a digital
image composition as the foundation of a new rendering model within the
X Window System.]])



prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/renderproto-0.11.1-tuj7hski3m3rjjod63i53l2axaouokvu/lib", ":")
prepend_path("LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/renderproto-0.11.1-tuj7hski3m3rjjod63i53l2axaouokvu/lib", ":")
prepend_path("CPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/renderproto-0.11.1-tuj7hski3m3rjjod63i53l2axaouokvu/include", ":")
prepend_path("PKG_CONFIG_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/renderproto-0.11.1-tuj7hski3m3rjjod63i53l2axaouokvu/lib/pkgconfig", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/renderproto-0.11.1-tuj7hski3m3rjjod63i53l2axaouokvu/", ":")

