-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2023-05-04 10:49:50.409080
--
-- rpcsvc-proto@1.4.3%gcc@6.4.0 build_system=autotools arch=linux-centos7-broadwell/jy7pfzw
--

whatis([[Name : rpcsvc-proto]])
whatis([[Version : 1.4.3]])
whatis([[Target : broadwell]])
whatis([[Short description : rpcsvc protocol definitions from glibc.]])
whatis([[Configure options : LIBS=-lintl]])

help([[Name   : rpcsvc-proto]])
help([[Version: 1.4.3]])
help([[Target : broadwell]])
help()
help([[rpcsvc protocol definitions from glibc.]])


depends_on("gettext/0.21-pzcpiio")

prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/rpcsvc-proto-1.4.3-jy7pfzwzeok5jflpxldqrusf2t3kscwo/bin", ":")
prepend_path("MANPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/rpcsvc-proto-1.4.3-jy7pfzwzeok5jflpxldqrusf2t3kscwo/share/man", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/rpcsvc-proto-1.4.3-jy7pfzwzeok5jflpxldqrusf2t3kscwo/.", ":")

