-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2018-09-18 17:12:43.076687
--
-- rsync@3.1.3%gcc@6.4.0 arch=linux-centos7-x86_64 /lc3ulv2
--

whatis([[Name : rsync]])
whatis([[Version : 3.1.3]])
whatis([[Short description : An open source utility that provides fast incremental file transfer.]])

help([[An open source utility that provides fast incremental file transfer.]])



prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/rsync-3.1.3-lc3ulv2kzaco56hx7xyi4yyopq3xjr5l/bin", ":")
prepend_path("MANPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/rsync-3.1.3-lc3ulv2kzaco56hx7xyi4yyopq3xjr5l/share/man", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/rsync-3.1.3-lc3ulv2kzaco56hx7xyi4yyopq3xjr5l/", ":")

