-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2022-03-31 10:48:17.181084
--
-- samtools@1.2%gcc@6.4.0 arch=linux-centos7-broadwell/adxykao
--

whatis([[Name : samtools]])
whatis([[Version : 1.2]])
whatis([[Target : broadwell]])
whatis([[Short description : SAM Tools provide various utilities for manipulating alignments in the SAM format, including sorting, merging, indexing and generating alignments in a per-position format]])

help([[SAM Tools provide various utilities for manipulating alignments in the
SAM format, including sorting, merging, indexing and generating
alignments in a per-position format]])



prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/samtools-1.2-adxykaovkngfn7u2ux64jb42bvrltovw/lib", ":")
prepend_path("LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/samtools-1.2-adxykaovkngfn7u2ux64jb42bvrltovw/lib", ":")
prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/samtools-1.2-adxykaovkngfn7u2ux64jb42bvrltovw/bin", ":")
prepend_path("MANPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/samtools-1.2-adxykaovkngfn7u2ux64jb42bvrltovw/share/man", ":")
prepend_path("C_INCLUDE_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/samtools-1.2-adxykaovkngfn7u2ux64jb42bvrltovw/include", ":")
prepend_path("CPLUS_INCLUDE_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/samtools-1.2-adxykaovkngfn7u2ux64jb42bvrltovw/include", ":")
prepend_path("INCLUDE", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/samtools-1.2-adxykaovkngfn7u2ux64jb42bvrltovw/include", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/samtools-1.2-adxykaovkngfn7u2ux64jb42bvrltovw/", ":")
prepend_path("PYTHONPATH", "", ":")

