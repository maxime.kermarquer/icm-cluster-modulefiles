-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2023-05-17 16:14:47.030007
--
-- scrnsaverproto@1.2.2%gcc@6.4.0 build_system=autotools arch=linux-centos7-broadwell/24z6w7s
--

whatis([[Name : scrnsaverproto]])
whatis([[Version : 1.2.2]])
whatis([[Target : broadwell]])
whatis([[Short description : MIT Screen Saver Extension.]])

help([[Name   : scrnsaverproto]])
help([[Version: 1.2.2]])
help([[Target : broadwell]])
help()
help([[MIT Screen Saver Extension. This extension defines a protocol to control
screensaver features and also to query screensaver info on specific
windows.]])



prepend_path("PKG_CONFIG_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/scrnsaverproto-1.2.2-24z6w7sq2u4vecnjxbhhckhbpinap4ka/lib/pkgconfig", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/scrnsaverproto-1.2.2-24z6w7sq2u4vecnjxbhhckhbpinap4ka/.", ":")

