-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2019-10-24 09:13:38.558754
--
-- scrnsaverproto@1.2.2%gcc@6.4.0 arch=linux-centos7-x86_64 /bexfefq
--

whatis([[Name : scrnsaverproto]])
whatis([[Version : 1.2.2]])
whatis([[Short description : MIT Screen Saver Extension.]])

help([[MIT Screen Saver Extension. This extension defines a protocol to control
screensaver features and also to query screensaver info on specific
windows.]])



prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/scrnsaverproto-1.2.2-bexfefq3nfzynhz6sswhk4lewa4ywauh/lib", ":")
prepend_path("LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/scrnsaverproto-1.2.2-bexfefq3nfzynhz6sswhk4lewa4ywauh/lib", ":")
prepend_path("CPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/scrnsaverproto-1.2.2-bexfefq3nfzynhz6sswhk4lewa4ywauh/include", ":")
prepend_path("PKG_CONFIG_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/scrnsaverproto-1.2.2-bexfefq3nfzynhz6sswhk4lewa4ywauh/lib/pkgconfig", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/scrnsaverproto-1.2.2-bexfefq3nfzynhz6sswhk4lewa4ywauh/", ":")

