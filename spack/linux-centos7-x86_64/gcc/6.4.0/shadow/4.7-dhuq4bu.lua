-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2021-01-13 16:57:03.148365
--
-- shadow@4.7%gcc@6.4.0 arch=linux-centos7-broadwell/dhuq4bu
--

whatis([[Name : shadow]])
whatis([[Version : 4.7]])
whatis([[Target : broadwell]])
whatis([[Short description : Tools to help unprivileged users create uid and gid mappings in user namespaces.]])

help([[Tools to help unprivileged users create uid and gid mappings in user
namespaces.]])



prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/shadow-4.7-dhuq4buxuibyoez5mibvp73vyz7me4xl/bin", ":")
prepend_path("MANPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/shadow-4.7-dhuq4buxuibyoez5mibvp73vyz7me4xl/share/man", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/shadow-4.7-dhuq4buxuibyoez5mibvp73vyz7me4xl/", ":")

