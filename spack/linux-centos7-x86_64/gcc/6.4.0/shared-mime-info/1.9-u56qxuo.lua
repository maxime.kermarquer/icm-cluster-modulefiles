-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2021-11-30 16:35:51.818317
--
-- shared-mime-info@1.9%gcc@6.4.0 arch=linux-centos7-broadwell/u56qxuo
--

whatis([[Name : shared-mime-info]])
whatis([[Version : 1.9]])
whatis([[Target : broadwell]])
whatis([[Short description : Database of common MIME types.]])

help([[Database of common MIME types.]])



prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/shared-mime-info-1.9-u56qxuov4fk3svrri3an7cbvyfemqvmf/bin", ":")
prepend_path("MANPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/shared-mime-info-1.9-u56qxuov4fk3svrri3an7cbvyfemqvmf/share/man", ":")
prepend_path("PKG_CONFIG_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/shared-mime-info-1.9-u56qxuov4fk3svrri3an7cbvyfemqvmf/share/pkgconfig", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/shared-mime-info-1.9-u56qxuov4fk3svrri3an7cbvyfemqvmf/", ":")
prepend_path("PYTHONPATH", "", ":")

