-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2021-01-13 18:50:14.485282
--
-- singularity@3.7.0%gcc@6.4.0+network+suid arch=linux-centos7-broadwell/owiobrw
--

whatis([[Name : singularity]])
whatis([[Version : 3.7.0]])
whatis([[Target : broadwell]])
whatis([[Short description : Singularity is a container technology focused on building portable encapsulated environments to support 'Mobility of Compute' For older versions of Singularity (pre 3.0) you should use singularity-legacy, which has a different install base (Autotools).]])

help([[Singularity is a container technology focused on building portable
encapsulated environments to support "Mobility of Compute" For older
versions of Singularity (pre 3.0) you should use singularity-legacy,
which has a different install base (Autotools). Needs post-install
chmod/chown steps to enable full functionality. See package definition
or `spack-build-out.txt` build log for details, e.g. tail -15 $(spack
location -i singularity)/.spack/spack-build-out.txt]])



prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/singularity-3.7.0-owiobrww2ndnyvofzcj2k4ew3x7r4uwv/bin", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/singularity-3.7.0-owiobrww2ndnyvofzcj2k4ew3x7r4uwv/", ":")
prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/cryptsetup-2.3.1-x565v4sukge47vdftnipds7iejlndrhu/sbin", ":")
prepend_path("GOPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/singularity-3.7.0-owiobrww2ndnyvofzcj2k4ew3x7r4uwv", ":")

