-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2021-01-13 18:06:36.482561
--
-- slurm@20-11-0-1%gcc@6.4.0~gtk~hdf5~hwloc~mariadb~pmix+readline~restd sysconfdir=PREFIX/etc arch=linux-centos7-broadwell/szvz7mr
--

whatis([[Name : slurm]])
whatis([[Version : 20-11-0-1]])
whatis([[Target : broadwell]])
whatis([[Short description : Slurm is an open source, fault-tolerant, and highly scalable cluster management and job scheduling system for large and small Linux clusters.]])
whatis([[Configure options : --with-libcurl=/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/curl-7.74.0-uspov5cgjhwhmwedyzn66britqnfe6ce --with-json=/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/json-c-0.13.1-fii7ums6rqhxlrrwmx3c3tokj7e2seje --with-lz4=/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/lz4-1.9.2-pumyq77l5og3xr3bouw3bdngk2gjc4jo --with-munge=/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/munge-0.5.14-yjzgbxaxfqcmeguqet64ftzk6mjeiy5a --with-ssl=/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/openssl-1.1.1i-kphel3rter2r2c5g4trtfsrzvjajvy5k --with-zlib=/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/zlib-1.2.11-wp5daspjexwgd5rft64ekrbjk4uarol2 --disable-gtktest --without-hdf5 --disable-slurmrestd --without-hwloc --without-pmix]])

help([[Slurm is an open source, fault-tolerant, and highly scalable cluster
management and job scheduling system for large and small Linux clusters.
Slurm requires no kernel modifications for its operation and is
relatively self-contained. As a cluster workload manager, Slurm has
three key functions. First, it allocates exclusive and/or non-exclusive
access to resources (compute nodes) to users for some duration of time
so they can perform work. Second, it provides a framework for starting,
executing, and monitoring work (normally a parallel job) on the set of
allocated nodes. Finally, it arbitrates contention for resources by
managing a queue of pending work.]])



prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/slurm-20-11-0-1-szvz7mrqymtwzerr7xgm3vsmzjvb7gja/lib", ":")
prepend_path("LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/slurm-20-11-0-1-szvz7mrqymtwzerr7xgm3vsmzjvb7gja/lib", ":")
prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/slurm-20-11-0-1-szvz7mrqymtwzerr7xgm3vsmzjvb7gja/bin", ":")
prepend_path("MANPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/slurm-20-11-0-1-szvz7mrqymtwzerr7xgm3vsmzjvb7gja/share/man", ":")
prepend_path("C_INCLUDE_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/slurm-20-11-0-1-szvz7mrqymtwzerr7xgm3vsmzjvb7gja/include", ":")
prepend_path("CPLUS_INCLUDE_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/slurm-20-11-0-1-szvz7mrqymtwzerr7xgm3vsmzjvb7gja/include", ":")
prepend_path("INCLUDE", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/slurm-20-11-0-1-szvz7mrqymtwzerr7xgm3vsmzjvb7gja/include", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/slurm-20-11-0-1-szvz7mrqymtwzerr7xgm3vsmzjvb7gja/", ":")
prepend_path("PYTHONPATH", "", ":")

