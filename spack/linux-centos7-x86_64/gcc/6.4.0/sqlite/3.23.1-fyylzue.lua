-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2018-09-19 11:10:59.678009
--
-- sqlite@3.23.1%gcc@6.4.0~functions arch=linux-centos7-x86_64 /fyylzue
--

whatis([[Name : sqlite]])
whatis([[Version : 3.23.1]])
whatis([[Short description : SQLite3 is an SQL database engine in a C library. Programs that link the SQLite3 library can have SQL database access without running a separate RDBMS process. ]])

help([[SQLite3 is an SQL database engine in a C library. Programs that link the
SQLite3 library can have SQL database access without running a separate
RDBMS process.]])



prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/sqlite-3.23.1-fyylzue7vrp4z2f2bqq33celllfezgti/bin", ":")
prepend_path("MANPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/sqlite-3.23.1-fyylzue7vrp4z2f2bqq33celllfezgti/share/man", ":")
prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/sqlite-3.23.1-fyylzue7vrp4z2f2bqq33celllfezgti/lib", ":")
prepend_path("LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/sqlite-3.23.1-fyylzue7vrp4z2f2bqq33celllfezgti/lib", ":")
prepend_path("CPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/sqlite-3.23.1-fyylzue7vrp4z2f2bqq33celllfezgti/include", ":")
prepend_path("PKG_CONFIG_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/sqlite-3.23.1-fyylzue7vrp4z2f2bqq33celllfezgti/lib/pkgconfig", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/sqlite-3.23.1-fyylzue7vrp4z2f2bqq33celllfezgti/", ":")

