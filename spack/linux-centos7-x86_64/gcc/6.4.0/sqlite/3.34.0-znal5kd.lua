-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2021-01-13 17:13:44.662865
--
-- sqlite@3.34.0%gcc@6.4.0+column_metadata+fts~functions~rtree arch=linux-centos7-broadwell/znal5kd
--

whatis([[Name : sqlite]])
whatis([[Version : 3.34.0]])
whatis([[Target : broadwell]])
whatis([[Short description : SQLite3 is an SQL database engine in a C library. Programs that link the SQLite3 library can have SQL database access without running a separate RDBMS process. ]])
whatis([[Configure options : CPPFLAGS=-DSQLITE_ENABLE_COLUMN_METADATA=1]])

help([[SQLite3 is an SQL database engine in a C library. Programs that link the
SQLite3 library can have SQL database access without running a separate
RDBMS process.]])



prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/sqlite-3.34.0-znal5kdhjdvntwp63lcvcituvvjblqv6/lib", ":")
prepend_path("LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/sqlite-3.34.0-znal5kdhjdvntwp63lcvcituvvjblqv6/lib", ":")
prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/sqlite-3.34.0-znal5kdhjdvntwp63lcvcituvvjblqv6/bin", ":")
prepend_path("MANPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/sqlite-3.34.0-znal5kdhjdvntwp63lcvcituvvjblqv6/share/man", ":")
prepend_path("C_INCLUDE_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/sqlite-3.34.0-znal5kdhjdvntwp63lcvcituvvjblqv6/include", ":")
prepend_path("CPLUS_INCLUDE_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/sqlite-3.34.0-znal5kdhjdvntwp63lcvcituvvjblqv6/include", ":")
prepend_path("INCLUDE", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/sqlite-3.34.0-znal5kdhjdvntwp63lcvcituvvjblqv6/include", ":")
prepend_path("PKG_CONFIG_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/sqlite-3.34.0-znal5kdhjdvntwp63lcvcituvvjblqv6/lib/pkgconfig", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/sqlite-3.34.0-znal5kdhjdvntwp63lcvcituvvjblqv6/", ":")

