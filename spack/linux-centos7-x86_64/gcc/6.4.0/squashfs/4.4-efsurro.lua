-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2021-01-13 17:48:59.742645
--
-- squashfs@4.4%gcc@6.4.0+gzip~lz4~lzo~xz~zstd default_compression=gzip arch=linux-centos7-broadwell/efsurro
--

whatis([[Name : squashfs]])
whatis([[Version : 4.4]])
whatis([[Target : broadwell]])
whatis([[Short description : Squashfs - read only compressed filesystem]])

help([[Squashfs - read only compressed filesystem]])



prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/squashfs-4.4-efsurrotvucqt6vgz5hkuwevwsm7c52m/bin", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/squashfs-4.4-efsurrotvucqt6vgz5hkuwevwsm7c52m/", ":")

