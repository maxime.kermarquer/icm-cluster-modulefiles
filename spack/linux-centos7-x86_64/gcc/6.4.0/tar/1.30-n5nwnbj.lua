-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2018-09-18 11:25:19.633840
--
-- tar@1.30%gcc@6.4.0 arch=linux-centos7-x86_64 /n5nwnbj
--

whatis([[Name : tar]])
whatis([[Version : 1.30]])
whatis([[Short description : GNU Tar provides the ability to create tar archives, as well as various other kinds of manipulation.]])

help([[GNU Tar provides the ability to create tar archives, as well as various
other kinds of manipulation.]])



prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/tar-1.30-n5nwnbjg652tekncrzrxi5zmjwnct7jv/bin", ":")
prepend_path("MANPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/tar-1.30-n5nwnbjg652tekncrzrxi5zmjwnct7jv/share/man", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/tar-1.30-n5nwnbjg652tekncrzrxi5zmjwnct7jv/", ":")

