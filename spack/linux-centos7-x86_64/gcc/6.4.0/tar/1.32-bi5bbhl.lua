-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2021-01-13 17:04:18.703672
--
-- tar@1.32%gcc@6.4.0 arch=linux-centos7-broadwell/bi5bbhl
--

whatis([[Name : tar]])
whatis([[Version : 1.32]])
whatis([[Target : broadwell]])
whatis([[Short description : GNU Tar provides the ability to create tar archives, as well as various other kinds of manipulation.]])
whatis([[Configure options : --with-libiconv-prefix=/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/libiconv-1.16-ufp4dxx2bc4vtwju5qtgznbuy2zpzzbc]])

help([[GNU Tar provides the ability to create tar archives, as well as various
other kinds of manipulation.]])



prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/tar-1.32-bi5bbhlxwh6tvaklf7frbhdvvmy327sm/bin", ":")
prepend_path("MANPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/tar-1.32-bi5bbhlxwh6tvaklf7frbhdvvmy327sm/share/man", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/tar-1.32-bi5bbhlxwh6tvaklf7frbhdvvmy327sm/", ":")

