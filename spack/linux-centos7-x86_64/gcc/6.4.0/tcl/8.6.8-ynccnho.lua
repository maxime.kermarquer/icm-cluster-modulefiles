-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2020-02-24 11:29:18.008937
--
-- tcl@8.6.8%gcc@6.4.0 arch=linux-centos7-x86_64 /ynccnho
--

whatis([[Name : tcl]])
whatis([[Version : 8.6.8]])
whatis([[Short description : Tcl (Tool Command Language) is a very powerful but easy to learn dynamic programming language, suitable for a very wide range of uses, including web and desktop applications, networking, administration, testing and many more. Open source and business-friendly, Tcl is a mature yet evolving language that is truly cross platform, easily deployed and highly extensible.]])

help([[Tcl (Tool Command Language) is a very powerful but easy to learn dynamic
programming language, suitable for a very wide range of uses, including
web and desktop applications, networking, administration, testing and
many more. Open source and business-friendly, Tcl is a mature yet
evolving language that is truly cross platform, easily deployed and
highly extensible.]])



prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/tcl-8.6.8-ynccnhok2k2u2gzjjtygmkq2pmzdjqbd/bin", ":")
prepend_path("MANPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/tcl-8.6.8-ynccnhok2k2u2gzjjtygmkq2pmzdjqbd/man", ":")
prepend_path("MANPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/tcl-8.6.8-ynccnhok2k2u2gzjjtygmkq2pmzdjqbd/share/man", ":")
prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/tcl-8.6.8-ynccnhok2k2u2gzjjtygmkq2pmzdjqbd/lib", ":")
prepend_path("LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/tcl-8.6.8-ynccnhok2k2u2gzjjtygmkq2pmzdjqbd/lib", ":")
prepend_path("CPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/tcl-8.6.8-ynccnhok2k2u2gzjjtygmkq2pmzdjqbd/include", ":")
prepend_path("PKG_CONFIG_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/tcl-8.6.8-ynccnhok2k2u2gzjjtygmkq2pmzdjqbd/lib/pkgconfig", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/tcl-8.6.8-ynccnhok2k2u2gzjjtygmkq2pmzdjqbd/", ":")
setenv("TCL_LIBRARY", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/tcl-8.6.8-ynccnhok2k2u2gzjjtygmkq2pmzdjqbd/lib")

