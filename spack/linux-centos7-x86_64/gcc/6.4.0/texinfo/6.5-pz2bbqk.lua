-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2021-10-14 18:45:05.255089
--
-- texinfo@6.5%gcc@6.4.0 patches=12f6edb0c6b270b8c8dba2ce17998c580db01182d871ee32b7b6e4129bd1d23a,1732115f651cff98989cb0215d8f64da5e0f7911ebf0c13b064920f088f2ffe1 arch=linux-centos7-broadwell/pz2bbqk
--

whatis([[Name : texinfo]])
whatis([[Version : 6.5]])
whatis([[Target : broadwell]])
whatis([[Short description : Texinfo is the official documentation format of the GNU project.]])

help([[Texinfo is the official documentation format of the GNU project. It was
invented by Richard Stallman and Bob Chassell many years ago, loosely
based on Brian Reid's Scribe and other formatting languages of the time.
It is used by many non-GNU projects as well.]])



prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/texinfo-6.5-pz2bbqkuavqm6lrmopslkbl4k5nm2ndy/lib", ":")
prepend_path("LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/texinfo-6.5-pz2bbqkuavqm6lrmopslkbl4k5nm2ndy/lib", ":")
prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/texinfo-6.5-pz2bbqkuavqm6lrmopslkbl4k5nm2ndy/bin", ":")
prepend_path("MANPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/texinfo-6.5-pz2bbqkuavqm6lrmopslkbl4k5nm2ndy/share/man", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/texinfo-6.5-pz2bbqkuavqm6lrmopslkbl4k5nm2ndy/", ":")

