-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2018-09-19 14:28:38.684664
--
-- tig@2.2.2%gcc@6.4.0 arch=linux-centos7-x86_64 /b6kmhrv
--

whatis([[Name : tig]])
whatis([[Version : 2.2.2]])
whatis([[Short description : Text-mode interface for git]])

help([[Text-mode interface for git]])



prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/tig-2.2.2-b6kmhrvlnjqrabyvvly526jpati3lxro/bin", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/tig-2.2.2-b6kmhrvlnjqrabyvvly526jpati3lxro/", ":")

