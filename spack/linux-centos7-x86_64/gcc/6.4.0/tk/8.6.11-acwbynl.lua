-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2023-05-17 16:22:05.166989
--
-- tk@8.6.11%gcc@6.4.0+xft+xss build_system=autotools arch=linux-centos7-broadwell/acwbynl
--

whatis([[Name : tk]])
whatis([[Version : 8.6.11]])
whatis([[Target : broadwell]])
whatis([[Short description : Tk is a graphical user interface toolkit that takes developing desktop applications to a higher level than conventional approaches. Tk is the standard GUI not only for Tcl, but for many other dynamic languages, and can produce rich, native applications that run unchanged across Windows, Mac OS X, Linux and more.]])
whatis([[Configure options : --with-tcl=/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/tcl-8.6.12-cep652v7kipbainbxmb6rubuf2toghxk/lib --x-includes=/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/libx11-1.8.4-cxeh2gxzvgcmjafrazgnhglr76lbsvin/include --x-libraries=/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/libx11-1.8.4-cxeh2gxzvgcmjafrazgnhglr76lbsvin/lib --enable-xft --enable-xss]])

help([[Name   : tk]])
help([[Version: 8.6.11]])
help([[Target : broadwell]])
help()
help([[Tk is a graphical user interface toolkit that takes developing desktop
applications to a higher level than conventional approaches. Tk is the
standard GUI not only for Tcl, but for many other dynamic languages, and
can produce rich, native applications that run unchanged across Windows,
Mac OS X, Linux and more.]])


depends_on("libx11/1.8.4-cxeh2gx")
depends_on("libxft/2.3.2-rvgjmvo")
depends_on("libxscrnsaver/1.2.2-3xitosg")
depends_on("tcl/8.6.12-cep652v")

prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/tk-8.6.11-acwbynlqkfpwqsp4al4p45shtrp76xuo/bin", ":")
prepend_path("MANPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/tk-8.6.11-acwbynlqkfpwqsp4al4p45shtrp76xuo/man", ":")
prepend_path("PKG_CONFIG_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/tk-8.6.11-acwbynlqkfpwqsp4al4p45shtrp76xuo/lib/pkgconfig", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/tk-8.6.11-acwbynlqkfpwqsp4al4p45shtrp76xuo/.", ":")
prepend_path("XLOCALEDIR", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/libx11-1.8.4-cxeh2gxzvgcmjafrazgnhglr76lbsvin/share/X11/locale", ":")
prepend_path("TCLLIBPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/tk-8.6.11-acwbynlqkfpwqsp4al4p45shtrp76xuo/lib", " ")
setenv("TK_LIBRARY", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/tk-8.6.11-acwbynlqkfpwqsp4al4p45shtrp76xuo/lib/tk8.6")

