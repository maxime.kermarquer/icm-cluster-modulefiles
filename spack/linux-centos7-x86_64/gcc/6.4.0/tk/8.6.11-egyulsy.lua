-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2023-05-16 16:57:15.991647
--
-- tk@8.6.11%gcc@6.4.0+xft+xss build_system=autotools arch=linux-centos7-x86_64/egyulsy
--

whatis([[Name : tk]])
whatis([[Version : 8.6.11]])
whatis([[Target : x86_64]])
whatis([[Short description : Tk is a graphical user interface toolkit that takes developing desktop applications to a higher level than conventional approaches. Tk is the standard GUI not only for Tcl, but for many other dynamic languages, and can produce rich, native applications that run unchanged across Windows, Mac OS X, Linux and more.]])
whatis([[Configure options : --with-tcl=/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/tcl-8.6.8-ynccnhok2k2u2gzjjtygmkq2pmzdjqbd/lib --x-includes=/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/libx11-1.6.5-eknhhrtlelb6wgc4vf4z5rh4s6dzn4gx/include --x-libraries=/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/libx11-1.6.5-eknhhrtlelb6wgc4vf4z5rh4s6dzn4gx/lib --enable-xft --enable-xss]])

help([[Name   : tk]])
help([[Version: 8.6.11]])
help([[Target : x86_64]])
help()
help([[Tk is a graphical user interface toolkit that takes developing desktop
applications to a higher level than conventional approaches. Tk is the
standard GUI not only for Tcl, but for many other dynamic languages, and
can produce rich, native applications that run unchanged across Windows,
Mac OS X, Linux and more.]])


depends_on("linux-centos7-x86_64/gcc/6.4.0/libx11/1.6.5-eknhhrt")
depends_on("linux-centos7-x86_64/gcc/6.4.0/libxft/2.3.2-x4ptnvj")
depends_on("linux-centos7-x86_64/gcc/6.4.0/libxscrnsaver/1.2.2-tktn3ij")
depends_on("linux-centos7-x86_64/gcc/6.4.0/tcl/8.6.8-ynccnho")

prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/tk-8.6.11-egyulsywsb45xhrcukhfl5uwiwbqw2vy/bin", ":")
prepend_path("CPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/tk-8.6.11-egyulsywsb45xhrcukhfl5uwiwbqw2vy/include", ":")
prepend_path("LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/tk-8.6.11-egyulsywsb45xhrcukhfl5uwiwbqw2vy/lib", ":")
prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/tk-8.6.11-egyulsywsb45xhrcukhfl5uwiwbqw2vy/lib", ":")
prepend_path("MANPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/tk-8.6.11-egyulsywsb45xhrcukhfl5uwiwbqw2vy/man", ":")
prepend_path("PKG_CONFIG_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/tk-8.6.11-egyulsywsb45xhrcukhfl5uwiwbqw2vy/lib/pkgconfig", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/tk-8.6.11-egyulsywsb45xhrcukhfl5uwiwbqw2vy/.", ":")
prepend_path("XLOCALEDIR", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/libx11-1.6.5-eknhhrtlelb6wgc4vf4z5rh4s6dzn4gx/share/X11/locale", ":")
prepend_path("TCLLIBPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/tk-8.6.11-egyulsywsb45xhrcukhfl5uwiwbqw2vy/lib", " ")
setenv("TK_LIBRARY", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/tk-8.6.11-egyulsywsb45xhrcukhfl5uwiwbqw2vy/lib/tk8.6")

