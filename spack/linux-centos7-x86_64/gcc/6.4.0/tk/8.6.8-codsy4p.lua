-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2020-02-24 11:29:43.143326
--
-- tk@8.6.8%gcc@6.4.0 arch=linux-centos7-x86_64 /codsy4p
--

whatis([[Name : tk]])
whatis([[Version : 8.6.8]])
whatis([[Short description : Tk is a graphical user interface toolkit that takes developing desktop applications to a higher level than conventional approaches. Tk is the standard GUI not only for Tcl, but for many other dynamic languages, and can produce rich, native applications that run unchanged across Windows, Mac OS X, Linux and more.]])
whatis([[Configure options : --with-tcl=/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/tcl-8.6.8-ynccnhok2k2u2gzjjtygmkq2pmzdjqbd/lib]])

help([[Tk is a graphical user interface toolkit that takes developing desktop
applications to a higher level than conventional approaches. Tk is the
standard GUI not only for Tcl, but for many other dynamic languages, and
can produce rich, native applications that run unchanged across Windows,
Mac OS X, Linux and more.]])



prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/tk-8.6.8-codsy4pp2bwdo4kx4amrmbayz7ntn3tt/bin", ":")
prepend_path("MANPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/tk-8.6.8-codsy4pp2bwdo4kx4amrmbayz7ntn3tt/man", ":")
prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/tk-8.6.8-codsy4pp2bwdo4kx4amrmbayz7ntn3tt/lib", ":")
prepend_path("LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/tk-8.6.8-codsy4pp2bwdo4kx4amrmbayz7ntn3tt/lib", ":")
prepend_path("CPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/tk-8.6.8-codsy4pp2bwdo4kx4amrmbayz7ntn3tt/include", ":")
prepend_path("PKG_CONFIG_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/tk-8.6.8-codsy4pp2bwdo4kx4amrmbayz7ntn3tt/lib/pkgconfig", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/tk-8.6.8-codsy4pp2bwdo4kx4amrmbayz7ntn3tt/", ":")
prepend_path("TCLLIBPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/tk-8.6.8-codsy4pp2bwdo4kx4amrmbayz7ntn3tt/lib", " ")
setenv("TK_LIBRARY", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/tk-8.6.8-codsy4pp2bwdo4kx4amrmbayz7ntn3tt/lib/tk8.6")

