-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2022-03-08 15:23:22.287050
--
-- unzip@6.0%gcc@6.4.0 arch=linux-centos7-broadwell/sbkuy6b
--

whatis([[Name : unzip]])
whatis([[Version : 6.0]])
whatis([[Target : broadwell]])
whatis([[Short description : Unzip is a compression and file packaging/archive utility.]])

help([[Unzip is a compression and file packaging/archive utility.]])



prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/unzip-6.0-sbkuy6b5fvprdrysshmt6b737f6rx5z3/bin", ":")
prepend_path("MANPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/unzip-6.0-sbkuy6b5fvprdrysshmt6b737f6rx5z3/man", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/unzip-6.0-sbkuy6b5fvprdrysshmt6b737f6rx5z3/", ":")

