-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2021-01-13 17:45:31.954986
--
-- util-linux@2.36%gcc@6.4.0~bash arch=linux-centos7-broadwell/5kdqua5
--

whatis([[Name : util-linux]])
whatis([[Version : 2.36]])
whatis([[Target : broadwell]])
whatis([[Short description : Util-linux is a suite of essential utilities for any Linux system.]])
whatis([[Configure options : --disable-use-tty-group --disable-makeinstall-chown --without-systemd --disable-libuuid --disable-bash-completion]])

help([[Util-linux is a suite of essential utilities for any Linux system.]])



prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/util-linux-2.36-5kdqua5rxtuxnyyjsykhbwqvu2rdla4x/lib", ":")
prepend_path("LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/util-linux-2.36-5kdqua5rxtuxnyyjsykhbwqvu2rdla4x/lib", ":")
prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/util-linux-2.36-5kdqua5rxtuxnyyjsykhbwqvu2rdla4x/bin", ":")
prepend_path("MANPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/util-linux-2.36-5kdqua5rxtuxnyyjsykhbwqvu2rdla4x/share/man", ":")
prepend_path("C_INCLUDE_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/util-linux-2.36-5kdqua5rxtuxnyyjsykhbwqvu2rdla4x/include", ":")
prepend_path("CPLUS_INCLUDE_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/util-linux-2.36-5kdqua5rxtuxnyyjsykhbwqvu2rdla4x/include", ":")
prepend_path("INCLUDE", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/util-linux-2.36-5kdqua5rxtuxnyyjsykhbwqvu2rdla4x/include", ":")
prepend_path("PKG_CONFIG_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/util-linux-2.36-5kdqua5rxtuxnyyjsykhbwqvu2rdla4x/lib/pkgconfig", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/util-linux-2.36-5kdqua5rxtuxnyyjsykhbwqvu2rdla4x/", ":")

