-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2021-01-13 16:48:37.252377
--
-- util-macros@1.19.1%gcc@6.4.0 arch=linux-centos7-broadwell/5gc2vaa
--

whatis([[Name : util-macros]])
whatis([[Version : 1.19.1]])
whatis([[Target : broadwell]])
whatis([[Short description : This is a set of autoconf macros used by the configure.ac scripts in other Xorg modular packages, and is needed to generate new versions of their configure scripts with autoconf.]])

help([[This is a set of autoconf macros used by the configure.ac scripts in
other Xorg modular packages, and is needed to generate new versions of
their configure scripts with autoconf.]])



prepend_path("ACLOCAL_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/util-macros-1.19.1-5gc2vaaflc2y4e57zesigxw3uwgbs2ib/share/aclocal", ":")
prepend_path("PKG_CONFIG_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/util-macros-1.19.1-5gc2vaaflc2y4e57zesigxw3uwgbs2ib/share/pkgconfig", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/util-macros-1.19.1-5gc2vaaflc2y4e57zesigxw3uwgbs2ib/", ":")

