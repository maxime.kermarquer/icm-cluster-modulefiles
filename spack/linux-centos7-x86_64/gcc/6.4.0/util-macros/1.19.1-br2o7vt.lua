-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2018-09-19 10:55:59.073077
--
-- util-macros@1.19.1%gcc@6.4.0 arch=linux-centos7-x86_64 /br2o7vt
--

whatis([[Name : util-macros]])
whatis([[Version : 1.19.1]])
whatis([[Short description : This is a set of autoconf macros used by the configure.ac scripts in other Xorg modular packages, and is needed to generate new versions of their configure scripts with autoconf.]])

help([[This is a set of autoconf macros used by the configure.ac scripts in
other Xorg modular packages, and is needed to generate new versions of
their configure scripts with autoconf.]])



prepend_path("ACLOCAL_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/util-macros-1.19.1-br2o7vt364vjeq3osmev6e6kgxekzh2f/share/aclocal", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/util-macros-1.19.1-br2o7vt364vjeq3osmev6e6kgxekzh2f/", ":")

