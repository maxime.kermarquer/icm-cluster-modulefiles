-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2018-09-19 11:09:30.397506
--
-- videoproto@2.3.3%gcc@6.4.0 arch=linux-centos7-x86_64 /igh4amd
--

whatis([[Name : videoproto]])
whatis([[Version : 2.3.3]])
whatis([[Short description : X Video Extension.]])

help([[X Video Extension. This extension provides a protocol for a video output
mechanism, mainly to rescale video playback in the video controller
hardware.]])



prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/videoproto-2.3.3-igh4amdzxpooxhddaicyozbgetaifiq7/lib", ":")
prepend_path("LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/videoproto-2.3.3-igh4amdzxpooxhddaicyozbgetaifiq7/lib", ":")
prepend_path("CPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/videoproto-2.3.3-igh4amdzxpooxhddaicyozbgetaifiq7/include", ":")
prepend_path("PKG_CONFIG_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/videoproto-2.3.3-igh4amdzxpooxhddaicyozbgetaifiq7/lib/pkgconfig", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/videoproto-2.3.3-igh4amdzxpooxhddaicyozbgetaifiq7/", ":")

