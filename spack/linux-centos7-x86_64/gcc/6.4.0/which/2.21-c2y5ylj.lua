-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2023-05-09 16:10:00.958672
--
-- which@2.21%gcc@6.4.0 build_system=autotools arch=linux-centos7-broadwell/c2y5ylj
--

whatis([[Name : which]])
whatis([[Version : 2.21]])
whatis([[Target : broadwell]])
whatis([[Short description : GNU which - is a utility that is used to find which executable (or alias or shell function) is executed when entered on the shell prompt.]])

help([[Name   : which]])
help([[Version: 2.21]])
help([[Target : broadwell]])
help()
help([[GNU which - is a utility that is used to find which executable (or alias
or shell function) is executed when entered on the shell prompt.]])



prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/which-2.21-c2y5ylj5b44uf2dslxgowybnscxmulmq/bin", ":")
prepend_path("MANPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/which-2.21-c2y5ylj5b44uf2dslxgowybnscxmulmq/share/man", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/which-2.21-c2y5ylj5b44uf2dslxgowybnscxmulmq/.", ":")

