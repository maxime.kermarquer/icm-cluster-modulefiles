-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2023-05-17 16:13:37.189534
--
-- xcb-proto@1.15.2%gcc@6.4.0 build_system=autotools arch=linux-centos7-broadwell/5mhobw4
--

whatis([[Name : xcb-proto]])
whatis([[Version : 1.15.2]])
whatis([[Target : broadwell]])
whatis([[Short description : xcb-proto provides the XML-XCB protocol descriptions that libxcb uses to generate the majority of its code and API.]])

help([[Name   : xcb-proto]])
help([[Version: 1.15.2]])
help([[Target : broadwell]])
help()
help([[xcb-proto provides the XML-XCB protocol descriptions that libxcb uses to
generate the majority of its code and API.]])



prepend_path("PKG_CONFIG_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/xcb-proto-1.15.2-5mhobw4fapmai2w7pdscvu4jkgjzznj5/share/pkgconfig", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/xcb-proto-1.15.2-5mhobw4fapmai2w7pdscvu4jkgjzznj5/.", ":")

