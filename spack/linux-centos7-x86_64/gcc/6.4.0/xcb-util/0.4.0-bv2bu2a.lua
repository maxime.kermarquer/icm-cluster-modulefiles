-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2019-02-06 20:08:31.076323
--
-- xcb-util@0.4.0%gcc@6.4.0 arch=linux-centos7-x86_64 /bv2bu2a
--

whatis([[Name : xcb-util]])
whatis([[Version : 0.4.0]])
whatis([[Short description : The XCB util modules provides a number of libraries which sit on top of libxcb, the core X protocol library, and some of the extension libraries. These experimental libraries provide convenience functions and interfaces which make the raw X protocol more usable. Some of the libraries also provide client-side code which is not strictly part of the X protocol but which have traditionally been provided by Xlib.]])

help([[The XCB util modules provides a number of libraries which sit on top of
libxcb, the core X protocol library, and some of the extension
libraries. These experimental libraries provide convenience functions
and interfaces which make the raw X protocol more usable. Some of the
libraries also provide client-side code which is not strictly part of
the X protocol but which have traditionally been provided by Xlib.]])



prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/xcb-util-0.4.0-bv2bu2am6hxpmybmnco5eqmjtnuwwxgk/lib", ":")
prepend_path("LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/xcb-util-0.4.0-bv2bu2am6hxpmybmnco5eqmjtnuwwxgk/lib", ":")
prepend_path("CPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/xcb-util-0.4.0-bv2bu2am6hxpmybmnco5eqmjtnuwwxgk/include", ":")
prepend_path("PKG_CONFIG_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/xcb-util-0.4.0-bv2bu2am6hxpmybmnco5eqmjtnuwwxgk/lib/pkgconfig", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/xcb-util-0.4.0-bv2bu2am6hxpmybmnco5eqmjtnuwwxgk/", ":")

