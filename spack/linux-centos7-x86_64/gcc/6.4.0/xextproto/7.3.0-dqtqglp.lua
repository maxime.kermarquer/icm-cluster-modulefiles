-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2018-09-19 10:57:04.977184
--
-- xextproto@7.3.0%gcc@6.4.0 arch=linux-centos7-x86_64 /dqtqglp
--

whatis([[Name : xextproto]])
whatis([[Version : 7.3.0]])
whatis([[Short description : X Protocol Extensions.]])

help([[X Protocol Extensions.]])



prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/xextproto-7.3.0-dqtqglp5qdjicm7owrnaqd4v65o3cro2/lib", ":")
prepend_path("LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/xextproto-7.3.0-dqtqglp5qdjicm7owrnaqd4v65o3cro2/lib", ":")
prepend_path("CPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/xextproto-7.3.0-dqtqglp5qdjicm7owrnaqd4v65o3cro2/include", ":")
prepend_path("PKG_CONFIG_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/xextproto-7.3.0-dqtqglp5qdjicm7owrnaqd4v65o3cro2/lib/pkgconfig", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/xextproto-7.3.0-dqtqglp5qdjicm7owrnaqd4v65o3cro2/", ":")

