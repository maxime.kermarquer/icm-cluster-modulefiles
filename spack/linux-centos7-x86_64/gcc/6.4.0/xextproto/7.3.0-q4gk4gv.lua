-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2023-05-17 16:14:02.610703
--
-- xextproto@7.3.0%gcc@6.4.0 build_system=autotools arch=linux-centos7-broadwell/q4gk4gv
--

whatis([[Name : xextproto]])
whatis([[Version : 7.3.0]])
whatis([[Target : broadwell]])
whatis([[Short description : X Protocol Extensions.]])

help([[Name   : xextproto]])
help([[Version: 7.3.0]])
help([[Target : broadwell]])
help()
help([[X Protocol Extensions.]])



prepend_path("PKG_CONFIG_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/xextproto-7.3.0-q4gk4gvbbbw6s44bxpyyx5h4ym4y3dxv/lib/pkgconfig", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/xextproto-7.3.0-q4gk4gvbbbw6s44bxpyyx5h4ym4y3dxv/.", ":")

