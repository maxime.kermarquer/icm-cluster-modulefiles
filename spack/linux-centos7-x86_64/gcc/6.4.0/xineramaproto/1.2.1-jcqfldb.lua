-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2019-02-06 20:24:51.168591
--
-- xineramaproto@1.2.1%gcc@6.4.0 arch=linux-centos7-x86_64 /jcqfldb
--

whatis([[Name : xineramaproto]])
whatis([[Version : 1.2.1]])
whatis([[Short description : X Xinerama Extension.]])

help([[X Xinerama Extension. This is an X extension that allows multiple
physical screens controlled by a single X server to appear as a single
screen.]])



prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/xineramaproto-1.2.1-jcqfldb2oqbur6nusag6hujb3znrt7ub/lib", ":")
prepend_path("LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/xineramaproto-1.2.1-jcqfldb2oqbur6nusag6hujb3znrt7ub/lib", ":")
prepend_path("CPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/xineramaproto-1.2.1-jcqfldb2oqbur6nusag6hujb3znrt7ub/include", ":")
prepend_path("PKG_CONFIG_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/xineramaproto-1.2.1-jcqfldb2oqbur6nusag6hujb3znrt7ub/lib/pkgconfig", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/xineramaproto-1.2.1-jcqfldb2oqbur6nusag6hujb3znrt7ub/", ":")

