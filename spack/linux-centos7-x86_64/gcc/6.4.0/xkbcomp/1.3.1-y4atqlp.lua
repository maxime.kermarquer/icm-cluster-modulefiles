-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2019-10-07 10:52:46.813298
--
-- xkbcomp@1.3.1%gcc@6.4.0 arch=linux-centos7-x86_64 /y4atqlp
--

whatis([[Name : xkbcomp]])
whatis([[Version : 1.3.1]])
whatis([[Short description : The X Keyboard (XKB) Extension essentially replaces the core protocol definition of a keyboard. The extension makes it possible to specify clearly and explicitly most aspects of keyboard behaviour on a per-key basis, and to track more closely the logical and physical state of a keyboard. It also includes a number of keyboard controls designed to make keyboards more accessible to people with physical impairments.]])

help([[The X Keyboard (XKB) Extension essentially replaces the core protocol
definition of a keyboard. The extension makes it possible to specify
clearly and explicitly most aspects of keyboard behaviour on a per-key
basis, and to track more closely the logical and physical state of a
keyboard. It also includes a number of keyboard controls designed to
make keyboards more accessible to people with physical impairments.]])



prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/xkbcomp-1.3.1-y4atqlpgbr3kvz4bcqn2yyexjatlxa4n/bin", ":")
prepend_path("MANPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/xkbcomp-1.3.1-y4atqlpgbr3kvz4bcqn2yyexjatlxa4n/share/man", ":")
prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/xkbcomp-1.3.1-y4atqlpgbr3kvz4bcqn2yyexjatlxa4n/lib", ":")
prepend_path("LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/xkbcomp-1.3.1-y4atqlpgbr3kvz4bcqn2yyexjatlxa4n/lib", ":")
prepend_path("PKG_CONFIG_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/xkbcomp-1.3.1-y4atqlpgbr3kvz4bcqn2yyexjatlxa4n/lib/pkgconfig", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/xkbcomp-1.3.1-y4atqlpgbr3kvz4bcqn2yyexjatlxa4n/", ":")

