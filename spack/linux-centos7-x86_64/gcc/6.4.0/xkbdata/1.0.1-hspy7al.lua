-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2019-10-07 10:52:56.040011
--
-- xkbdata@1.0.1%gcc@6.4.0 arch=linux-centos7-x86_64 /hspy7al
--

whatis([[Name : xkbdata]])
whatis([[Version : 1.0.1]])
whatis([[Short description : The XKB data files for the various keyboard models, layouts, and locales.]])

help([[The XKB data files for the various keyboard models, layouts, and
locales.]])



prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/xkbdata-1.0.1-hspy7albk5hn43qq6t4gymzficairdm6/", ":")

