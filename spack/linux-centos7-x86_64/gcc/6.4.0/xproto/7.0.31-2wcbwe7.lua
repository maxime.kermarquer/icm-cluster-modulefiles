-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2023-05-17 16:14:20.236041
--
-- xproto@7.0.31%gcc@6.4.0 build_system=autotools arch=linux-centos7-broadwell/2wcbwe7
--

whatis([[Name : xproto]])
whatis([[Version : 7.0.31]])
whatis([[Target : broadwell]])
whatis([[Short description : X Window System Core Protocol.]])

help([[Name   : xproto]])
help([[Version: 7.0.31]])
help([[Target : broadwell]])
help()
help([[X Window System Core Protocol. This package provides the headers and
specification documents defining the X Window System Core Protocol,
Version 11. It also includes a number of headers that aren't purely
protocol related, but are depended upon by many other X Window System
packages to provide common definitions and porting layer.]])



prepend_path("PKG_CONFIG_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/xproto-7.0.31-2wcbwe7zbifmawigjy5x33bxlk7yddim/lib/pkgconfig", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/xproto-7.0.31-2wcbwe7zbifmawigjy5x33bxlk7yddim/.", ":")

