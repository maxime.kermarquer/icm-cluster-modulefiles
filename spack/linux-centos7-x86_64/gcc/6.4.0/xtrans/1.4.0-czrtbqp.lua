-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2023-05-17 16:14:29.090349
--
-- xtrans@1.4.0%gcc@6.4.0 build_system=autotools arch=linux-centos7-broadwell/czrtbqp
--

whatis([[Name : xtrans]])
whatis([[Version : 1.4.0]])
whatis([[Target : broadwell]])
whatis([[Short description : xtrans is a library of code that is shared among various X packages to handle network protocol transport in a modular fashion, allowing a single place to add new transport types. It is used by the X server, libX11, libICE, the X font server, and related components.]])

help([[Name   : xtrans]])
help([[Version: 1.4.0]])
help([[Target : broadwell]])
help()
help([[xtrans is a library of code that is shared among various X packages to
handle network protocol transport in a modular fashion, allowing a
single place to add new transport types. It is used by the X server,
libX11, libICE, the X font server, and related components.]])



prepend_path("ACLOCAL_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/xtrans-1.4.0-czrtbqpbez3wpiylpc5v6e3z4irovxwt/share/aclocal", ":")
prepend_path("PKG_CONFIG_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/xtrans-1.4.0-czrtbqpbez3wpiylpc5v6e3z4irovxwt/share/pkgconfig", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/xtrans-1.4.0-czrtbqpbez3wpiylpc5v6e3z4irovxwt/.", ":")

