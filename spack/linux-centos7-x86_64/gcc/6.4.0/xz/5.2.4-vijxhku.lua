-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2018-09-18 11:23:51.843368
--
-- xz@5.2.4%gcc@6.4.0 arch=linux-centos7-x86_64 /vijxhku
--

whatis([[Name : xz]])
whatis([[Version : 5.2.4]])
whatis([[Short description : XZ Utils is free general-purpose data compression software with high compression ratio. XZ Utils were written for POSIX-like systems, but also work on some not-so-POSIX systems. XZ Utils are the successor to LZMA Utils.]])

help([[XZ Utils is free general-purpose data compression software with high
compression ratio. XZ Utils were written for POSIX-like systems, but
also work on some not-so-POSIX systems. XZ Utils are the successor to
LZMA Utils.]])



prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/xz-5.2.4-vijxhkua7fzbd7dvww7w4tvyyp2u2jd2/bin", ":")
prepend_path("MANPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/xz-5.2.4-vijxhkua7fzbd7dvww7w4tvyyp2u2jd2/share/man", ":")
prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/xz-5.2.4-vijxhkua7fzbd7dvww7w4tvyyp2u2jd2/lib", ":")
prepend_path("LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/xz-5.2.4-vijxhkua7fzbd7dvww7w4tvyyp2u2jd2/lib", ":")
prepend_path("CPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/xz-5.2.4-vijxhkua7fzbd7dvww7w4tvyyp2u2jd2/include", ":")
prepend_path("PKG_CONFIG_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/xz-5.2.4-vijxhkua7fzbd7dvww7w4tvyyp2u2jd2/lib/pkgconfig", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/xz-5.2.4-vijxhkua7fzbd7dvww7w4tvyyp2u2jd2/", ":")

