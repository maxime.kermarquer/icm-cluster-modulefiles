-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2021-01-13 16:55:02.880238
--
-- xz@5.2.5%gcc@6.4.0~pic arch=linux-centos7-broadwell/loqs5h3
--

whatis([[Name : xz]])
whatis([[Version : 5.2.5]])
whatis([[Target : broadwell]])
whatis([[Short description : XZ Utils is free general-purpose data compression software with high compression ratio. XZ Utils were written for POSIX-like systems, but also work on some not-so-POSIX systems. XZ Utils are the successor to LZMA Utils.]])

help([[XZ Utils is free general-purpose data compression software with high
compression ratio. XZ Utils were written for POSIX-like systems, but
also work on some not-so-POSIX systems. XZ Utils are the successor to
LZMA Utils.]])



prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/xz-5.2.5-loqs5h3vpwh4r3izakh7y2hz6orm6hgm/lib", ":")
prepend_path("LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/xz-5.2.5-loqs5h3vpwh4r3izakh7y2hz6orm6hgm/lib", ":")
prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/xz-5.2.5-loqs5h3vpwh4r3izakh7y2hz6orm6hgm/bin", ":")
prepend_path("MANPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/xz-5.2.5-loqs5h3vpwh4r3izakh7y2hz6orm6hgm/share/man", ":")
prepend_path("C_INCLUDE_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/xz-5.2.5-loqs5h3vpwh4r3izakh7y2hz6orm6hgm/include", ":")
prepend_path("CPLUS_INCLUDE_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/xz-5.2.5-loqs5h3vpwh4r3izakh7y2hz6orm6hgm/include", ":")
prepend_path("INCLUDE", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/xz-5.2.5-loqs5h3vpwh4r3izakh7y2hz6orm6hgm/include", ":")
prepend_path("PKG_CONFIG_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/xz-5.2.5-loqs5h3vpwh4r3izakh7y2hz6orm6hgm/lib/pkgconfig", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/xz-5.2.5-loqs5h3vpwh4r3izakh7y2hz6orm6hgm/", ":")

