-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2019-02-13 20:48:31.928071
--
-- yasm@1.3.0%gcc@6.4.0 arch=linux-centos7-x86_64 /tb37js6
--

whatis([[Name : yasm]])
whatis([[Version : 1.3.0]])
whatis([[Short description : Yasm is a complete rewrite of the NASM-2.11.06 assembler. It supports the x86 and AMD64 instruction sets, accepts NASM and GAS assembler syntaxes and outputs binary, ELF32 and ELF64 object formats.]])

help([[Yasm is a complete rewrite of the NASM-2.11.06 assembler. It supports
the x86 and AMD64 instruction sets, accepts NASM and GAS assembler
syntaxes and outputs binary, ELF32 and ELF64 object formats.]])



prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/yasm-1.3.0-tb37js6sh4wy24ckpj7yumv63mk5bymm/bin", ":")
prepend_path("MANPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/yasm-1.3.0-tb37js6sh4wy24ckpj7yumv63mk5bymm/share/man", ":")
prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/yasm-1.3.0-tb37js6sh4wy24ckpj7yumv63mk5bymm/lib", ":")
prepend_path("LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/yasm-1.3.0-tb37js6sh4wy24ckpj7yumv63mk5bymm/lib", ":")
prepend_path("CPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/yasm-1.3.0-tb37js6sh4wy24ckpj7yumv63mk5bymm/include", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/yasm-1.3.0-tb37js6sh4wy24ckpj7yumv63mk5bymm/", ":")

