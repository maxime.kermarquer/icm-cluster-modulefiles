-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2022-03-08 15:23:30.909164
--
-- zip@3.0%gcc@6.4.0 patches=14dc88014812a896e5697553025f451435f69b1b541743f853b8dcae6b4dfe27,3bc30bafdfd29f1d26d1e0c4c7217def2ff5b37a715e482f83c5fefa2aa0ecb6,5068e7cb188e520da3cced095292ace13ad5e8321419127bf2ca095e148ab65a,51f48db588b17790aaf6b901d6ca737abdb20124210cf8def17d81841c013d13,66ab4ce03f342c6624aa14be5fa43b90e5608a4f6babcc9c3680828f2c246a74,a92fc4e4f59aa2ca3e8059c6b355ecce8c8c7802cb4f118756bf06eb51455549,a95ed93de9284ff68e835387ddb9ff62414712f9b95ec8e120f02cef8f26faca,eb83fc886ded7101ad8ddf10c025b1bd4d33b594766816cce65397d4f5c6a7b9,f7d0bc42e1db9564732ae0aa1ad238b7278a3e68eb0b05e16f1cb6507df2668d,fa8312c722a1cf774af588d5534745f2231130da0f40a44bba5b394eb13a5c64 arch=linux-centos7-broadwell/oboe3y5
--

whatis([[Name : zip]])
whatis([[Version : 3.0]])
whatis([[Target : broadwell]])
whatis([[Short description : Zip is a compression and file packaging/archive utility.]])

help([[Zip is a compression and file packaging/archive utility.]])



prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/zip-3.0-oboe3y55e6uvmct3sqspfca6gkfyre6h/bin", ":")
prepend_path("MANPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/zip-3.0-oboe3y55e6uvmct3sqspfca6gkfyre6h/man", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/zip-3.0-oboe3y55e6uvmct3sqspfca6gkfyre6h/", ":")

