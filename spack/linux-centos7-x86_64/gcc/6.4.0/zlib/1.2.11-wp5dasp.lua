-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2021-01-13 16:48:14.115447
--
-- zlib@1.2.11%gcc@6.4.0+optimize+pic+shared arch=linux-centos7-broadwell/wp5dasp
--

whatis([[Name : zlib]])
whatis([[Version : 1.2.11]])
whatis([[Target : broadwell]])
whatis([[Short description : A free, general-purpose, legally unencumbered lossless data-compression library. ]])

help([[A free, general-purpose, legally unencumbered lossless data-compression
library.]])



prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/zlib-1.2.11-wp5daspjexwgd5rft64ekrbjk4uarol2/lib", ":")
prepend_path("LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/zlib-1.2.11-wp5daspjexwgd5rft64ekrbjk4uarol2/lib", ":")
prepend_path("MANPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/zlib-1.2.11-wp5daspjexwgd5rft64ekrbjk4uarol2/share/man", ":")
prepend_path("C_INCLUDE_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/zlib-1.2.11-wp5daspjexwgd5rft64ekrbjk4uarol2/include", ":")
prepend_path("CPLUS_INCLUDE_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/zlib-1.2.11-wp5daspjexwgd5rft64ekrbjk4uarol2/include", ":")
prepend_path("INCLUDE", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/zlib-1.2.11-wp5daspjexwgd5rft64ekrbjk4uarol2/include", ":")
prepend_path("PKG_CONFIG_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/zlib-1.2.11-wp5daspjexwgd5rft64ekrbjk4uarol2/lib/pkgconfig", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/zlib-1.2.11-wp5daspjexwgd5rft64ekrbjk4uarol2/", ":")

