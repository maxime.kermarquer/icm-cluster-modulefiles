-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2018-09-18 11:03:05.116716
--
-- zsh@5.4.2%gcc@6.4.0+skip-tcsetpgrp-test arch=linux-centos7-x86_64 /iqlioim
--

whatis([[Name : zsh]])
whatis([[Version : 5.4.2]])
whatis([[Short description : Zsh is a shell designed for interactive use, although it is also a powerful scripting language. Many of the useful features of bash, ksh, and tcsh were incorporated into zsh; many original features were added. ]])
whatis([[Configure options : --with-tcsetpgrp]])

help([[Zsh is a shell designed for interactive use, although it is also a
powerful scripting language. Many of the useful features of bash, ksh,
and tcsh were incorporated into zsh; many original features were added.]])



prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/zsh-5.4.2-iqlioimqe3xceagtea6vrizdfkxe7qbx/bin", ":")
prepend_path("MANPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/zsh-5.4.2-iqlioimqe3xceagtea6vrizdfkxe7qbx/share/man", ":")
prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/zsh-5.4.2-iqlioimqe3xceagtea6vrizdfkxe7qbx/lib", ":")
prepend_path("LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/zsh-5.4.2-iqlioimqe3xceagtea6vrizdfkxe7qbx/lib", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/zsh-5.4.2-iqlioimqe3xceagtea6vrizdfkxe7qbx/", ":")

