-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2023-05-04 10:32:10.894025
--
-- zstd@1.5.5%gcc@6.4.0~programs build_system=makefile libs=shared,static arch=linux-centos7-broadwell/kw2nhqz
--

whatis([[Name : zstd]])
whatis([[Version : 1.5.5]])
whatis([[Target : broadwell]])
whatis([[Short description : Zstandard, or zstd as short version, is a fast lossless compression algorithm, targeting real-time compression scenarios at zlib-level and better compression ratios.]])

help([[Name   : zstd]])
help([[Version: 1.5.5]])
help([[Target : broadwell]])
help()
help([[Zstandard, or zstd as short version, is a fast lossless compression
algorithm, targeting real-time compression scenarios at zlib-level and
better compression ratios.]])



prepend_path("PKG_CONFIG_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/zstd-1.5.5-kw2nhqzti6sgb24zbpt6dzoy4yfquny4/lib/pkgconfig", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/zstd-1.5.5-kw2nhqzti6sgb24zbpt6dzoy4yfquny4/.", ":")

