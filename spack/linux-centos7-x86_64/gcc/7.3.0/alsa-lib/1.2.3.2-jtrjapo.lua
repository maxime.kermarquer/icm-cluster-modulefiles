-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2023-05-10 15:57:03.215511
--
-- alsa-lib@1.2.3.2%gcc@7.3.0~python build_system=autotools arch=linux-centos7-broadwell/jtrjapo
--

whatis([[Name : alsa-lib]])
whatis([[Version : 1.2.3.2]])
whatis([[Target : broadwell]])
whatis([[Short description : The Advanced Linux Sound Architecture (ALSA) provides audio and MIDI functionality to the Linux operating system. alsa-lib contains the user space library that developers compile ALSA applications against.]])
whatis([[Configure options : --disable-python]])

help([[Name   : alsa-lib]])
help([[Version: 1.2.3.2]])
help([[Target : broadwell]])
help()
help([[The Advanced Linux Sound Architecture (ALSA) provides audio and MIDI
functionality to the Linux operating system. alsa-lib contains the user
space library that developers compile ALSA applications against.]])



prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/alsa-lib-1.2.3.2-jtrjapowp6w227fbtsh3c6py6bemixhl/bin", ":")
prepend_path("ACLOCAL_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/alsa-lib-1.2.3.2-jtrjapowp6w227fbtsh3c6py6bemixhl/share/aclocal", ":")
prepend_path("PKG_CONFIG_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/alsa-lib-1.2.3.2-jtrjapowp6w227fbtsh3c6py6bemixhl/lib/pkgconfig", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/alsa-lib-1.2.3.2-jtrjapowp6w227fbtsh3c6py6bemixhl/.", ":")

