-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2023-05-10 09:23:25.082486
--
-- autoconf-archive@2023.02.20%gcc@7.3.0 build_system=autotools arch=linux-centos7-broadwell/ktgnxto
--

whatis([[Name : autoconf-archive]])
whatis([[Version : 2023.02.20]])
whatis([[Target : broadwell]])
whatis([[Short description : The GNU Autoconf Archive is a collection of more than 500 macros for GNU Autoconf.]])

help([[Name   : autoconf-archive]])
help([[Version: 2023.02.20]])
help([[Target : broadwell]])
help()
help([[The GNU Autoconf Archive is a collection of more than 500 macros for GNU
Autoconf.]])



prepend_path("ACLOCAL_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/autoconf-archive-2023.02.20-ktgnxto6q5w2k3xcoieyn2wrheriebeu/share/aclocal", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/autoconf-archive-2023.02.20-ktgnxto6q5w2k3xcoieyn2wrheriebeu/.", ":")

