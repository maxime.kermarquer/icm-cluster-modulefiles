-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2023-05-09 15:13:05.461791
--
-- autoconf@2.69%gcc@7.3.0 build_system=autotools patches=35c4492,7793209,a49dd5b arch=linux-centos7-broadwell/3zwqxru
--

whatis([[Name : autoconf]])
whatis([[Version : 2.69]])
whatis([[Target : broadwell]])
whatis([[Short description : Autoconf -- system configuration part of autotools]])

help([[Name   : autoconf]])
help([[Version: 2.69]])
help([[Target : broadwell]])
help()
help([[Autoconf -- system configuration part of autotools]])


depends_on("m4/1.4.19-qurp35e")
depends_on("perl/5.36.0-3zk6frf")

prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/autoconf-2.69-3zwqxruc2svlmjvvxkfryj5xnb7j3t32/bin", ":")
prepend_path("MANPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/autoconf-2.69-3zwqxruc2svlmjvvxkfryj5xnb7j3t32/share/man", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/autoconf-2.69-3zwqxruc2svlmjvvxkfryj5xnb7j3t32/.", ":")

