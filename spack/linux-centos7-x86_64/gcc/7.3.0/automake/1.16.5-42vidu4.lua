-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2023-05-09 15:15:17.692956
--
-- automake@1.16.5%gcc@7.3.0 build_system=autotools arch=linux-centos7-broadwell/42vidu4
--

whatis([[Name : automake]])
whatis([[Version : 1.16.5]])
whatis([[Target : broadwell]])
whatis([[Short description : Automake -- make file builder part of autotools]])

help([[Name   : automake]])
help([[Version: 1.16.5]])
help([[Target : broadwell]])
help()
help([[Automake -- make file builder part of autotools]])


depends_on("linux-centos7-x86_64/gcc/7.3.0/perl/5.36.0-3zk6frf")

prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/automake-1.16.5-42vidu4zdktw55g75qcyqyupkmmstrml/bin", ":")
prepend_path("MANPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/automake-1.16.5-42vidu4zdktw55g75qcyqyupkmmstrml/share/man", ":")
prepend_path("ACLOCAL_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/automake-1.16.5-42vidu4zdktw55g75qcyqyupkmmstrml/share/aclocal", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/automake-1.16.5-42vidu4zdktw55g75qcyqyupkmmstrml/.", ":")

