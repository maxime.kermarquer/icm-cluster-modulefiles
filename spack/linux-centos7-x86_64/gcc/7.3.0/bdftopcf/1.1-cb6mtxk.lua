-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2023-05-09 15:52:21.997550
--
-- bdftopcf@1.1%gcc@7.3.0 build_system=autotools arch=linux-centos7-broadwell/cb6mtxk
--

whatis([[Name : bdftopcf]])
whatis([[Version : 1.1]])
whatis([[Target : broadwell]])
whatis([[Short description : bdftopcf is a font compiler for the X server and font server. Fonts in Portable Compiled Format can be read by any architecture, although the file is structured to allow one particular architecture to read them directly without reformatting. This allows fast reading on the appropriate machine, but the files are still portable (but read more slowly) on other machines.]])

help([[Name   : bdftopcf]])
help([[Version: 1.1]])
help([[Target : broadwell]])
help()
help([[bdftopcf is a font compiler for the X server and font server. Fonts in
Portable Compiled Format can be read by any architecture, although the
file is structured to allow one particular architecture to read them
directly without reformatting. This allows fast reading on the
appropriate machine, but the files are still portable (but read more
slowly) on other machines.]])


depends_on("fontsproto/2.1.3-245mshc")
depends_on("libxfont/1.5.4-7e2j3bm")
depends_on("xproto/7.0.31-2iu6dcc")

prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/bdftopcf-1.1-cb6mtxktway5pyjchmzutuxhsflpyrq4/bin", ":")
prepend_path("MANPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/bdftopcf-1.1-cb6mtxktway5pyjchmzutuxhsflpyrq4/share/man", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/bdftopcf-1.1-cb6mtxktway5pyjchmzutuxhsflpyrq4/.", ":")

