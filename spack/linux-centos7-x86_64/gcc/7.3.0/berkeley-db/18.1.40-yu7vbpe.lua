-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2023-05-04 14:41:56.062028
--
-- berkeley-db@18.1.40%gcc@7.3.0+cxx~docs+stl build_system=autotools patches=26090f4,b231fcc arch=linux-centos7-broadwell/yu7vbpe
--

whatis([[Name : berkeley-db]])
whatis([[Version : 18.1.40]])
whatis([[Target : broadwell]])
whatis([[Short description : Oracle Berkeley DB]])
whatis([[Configure options : --disable-static --enable-dbm --enable-compat185 --with-repmgr-ssl=no --enable-cxx --enable-stl]])

help([[Name   : berkeley-db]])
help([[Version: 18.1.40]])
help([[Target : broadwell]])
help()
help([[Oracle Berkeley DB]])



prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/berkeley-db-18.1.40-yu7vbpezergbbu3iuvk7qictdu7ucker/bin", ":")
prepend_path("CPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/berkeley-db-18.1.40-yu7vbpezergbbu3iuvk7qictdu7ucker/include", ":")
prepend_path("LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/berkeley-db-18.1.40-yu7vbpezergbbu3iuvk7qictdu7ucker/lib", ":")
prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/berkeley-db-18.1.40-yu7vbpezergbbu3iuvk7qictdu7ucker/lib", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/berkeley-db-18.1.40-yu7vbpezergbbu3iuvk7qictdu7ucker/.", ":")

