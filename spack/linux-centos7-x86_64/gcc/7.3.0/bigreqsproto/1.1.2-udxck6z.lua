-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2023-05-11 10:57:54.365527
--
-- bigreqsproto@1.1.2%gcc@7.3.0 build_system=autotools arch=linux-centos7-broadwell/udxck6z
--

whatis([[Name : bigreqsproto]])
whatis([[Version : 1.1.2]])
whatis([[Target : broadwell]])
whatis([[Short description : Big Requests Extension.]])

help([[Name   : bigreqsproto]])
help([[Version: 1.1.2]])
help([[Target : broadwell]])
help()
help([[Big Requests Extension. This extension defines a protocol to enable the
use of requests that exceed 262140 bytes in length.]])



prepend_path("PKG_CONFIG_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/bigreqsproto-1.1.2-udxck6zjyucqmag4q537xmido6pff665/lib/pkgconfig", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/bigreqsproto-1.1.2-udxck6zjyucqmag4q537xmido6pff665/.", ":")

