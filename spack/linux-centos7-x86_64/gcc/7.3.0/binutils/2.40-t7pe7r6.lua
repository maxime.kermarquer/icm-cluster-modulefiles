-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2023-05-10 09:39:23.961661
--
-- binutils@2.40%gcc@7.3.0~gas+gold~gprofng+headers~interwork+ld~libiberty~lto~nls~pgo+plugins build_system=autotools libs=shared,static arch=linux-centos7-broadwell/t7pe7r6
--

whatis([[Name : binutils]])
whatis([[Version : 2.40]])
whatis([[Target : broadwell]])
whatis([[Short description : GNU binutils, which contain the linker, assembler, objdump and others]])

help([[Name   : binutils]])
help([[Version: 2.40]])
help([[Target : broadwell]])
help()
help([[GNU binutils, which contain the linker, assembler, objdump and others]])


depends_on("zlib/1.2.13-7shounl")
depends_on("zstd/1.5.5-nwuxakc")

prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/binutils-2.40-t7pe7r6mzhmoeb4ymcfwrullhideqqn2/bin", ":")
prepend_path("MANPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/binutils-2.40-t7pe7r6mzhmoeb4ymcfwrullhideqqn2/share/man", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/binutils-2.40-t7pe7r6mzhmoeb4ymcfwrullhideqqn2/.", ":")

