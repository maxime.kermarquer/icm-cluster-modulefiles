-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2023-05-04 16:21:13.483496
--
-- bison@3.8.2%gcc@7.3.0 build_system=autotools arch=linux-centos7-broadwell/wmipuj3
--

whatis([[Name : bison]])
whatis([[Version : 3.8.2]])
whatis([[Target : broadwell]])
whatis([[Short description : Bison is a general-purpose parser generator that converts an annotated context-free grammar into a deterministic LR or generalized LR (GLR) parser employing LALR(1) parser tables.]])

help([[Name   : bison]])
help([[Version: 3.8.2]])
help([[Target : broadwell]])
help()
help([[Bison is a general-purpose parser generator that converts an annotated
context-free grammar into a deterministic LR or generalized LR (GLR)
parser employing LALR(1) parser tables.]])


depends_on("linux-centos7-x86_64/gcc/7.3.0/m4/1.4.19-qurp35e")

prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/bison-3.8.2-wmipuj3zmsaqjuubkhehfyj2cbuez5vr/bin", ":")
prepend_path("MANPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/bison-3.8.2-wmipuj3zmsaqjuubkhehfyj2cbuez5vr/share/man", ":")
prepend_path("ACLOCAL_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/bison-3.8.2-wmipuj3zmsaqjuubkhehfyj2cbuez5vr/share/aclocal", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/bison-3.8.2-wmipuj3zmsaqjuubkhehfyj2cbuez5vr/.", ":")

