-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2023-05-30 12:28:38.355234
--
-- boost@1.82.0%gcc@7.3.0~atomic~chrono~clanglibcpp~container~context~contract~coroutine~date_time~debug~exception~fiber~filesystem~graph~graph_parallel~icu~iostreams~json~locale~log~math~mpi+multithreaded~nowide~numpy~pic~program_options~python~random~regex~serialization+shared~signals~singlethreaded~stacktrace~system~taggedlayout~test~thread~timer~type_erasure~versionedlayout~wave build_system=generic cxxstd=98 patches=a440f96,a7c807f visibility=hidden arch=linux-centos7-broadwell/jqhuaak
--

whatis([[Name : boost]])
whatis([[Version : 1.82.0]])
whatis([[Target : broadwell]])
whatis([[Short description : Boost provides free peer-reviewed portable C++ source libraries, emphasizing libraries that work well with the C++ Standard Library.]])

help([[Name   : boost]])
help([[Version: 1.82.0]])
help([[Target : broadwell]])
help()
help([[Boost provides free peer-reviewed portable C++ source libraries,
emphasizing libraries that work well with the C++ Standard Library.
Boost libraries are intended to be widely useful, and usable across a
broad spectrum of applications. The Boost license encourages both
commercial and non-commercial use.]])



prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/boost-1.82.0-jqhuaak65mhtvm4ngjkywto27bbrolaf/.", ":")
setenv("BOOST_ROOT", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/boost-1.82.0-jqhuaak65mhtvm4ngjkywto27bbrolaf")

