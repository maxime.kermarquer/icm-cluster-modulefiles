-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2023-05-04 14:40:20.361116
--
-- ca-certificates-mozilla@2023-01-10%gcc@7.3.0 build_system=generic arch=linux-centos7-broadwell/zpg6icp
--

whatis([[Name : ca-certificates-mozilla]])
whatis([[Version : 2023-01-10]])
whatis([[Target : broadwell]])
whatis([[Short description : The Mozilla CA certificate store in PEM format]])

help([[Name   : ca-certificates-mozilla]])
help([[Version: 2023-01-10]])
help([[Target : broadwell]])
help()
help([[The Mozilla CA certificate store in PEM format]])



prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/ca-certificates-mozilla-2023-01-10-zpg6icpjkdrluoyrf5s5sjifrc3k5jen/.", ":")

