-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2023-05-23 16:08:39.971598
--
-- cairo@1.14.0%gcc@7.3.0~X~fc~ft~gobject~pdf~png~svg build_system=autotools arch=linux-centos7-broadwell/zcbaxxq
--

whatis([[Name : cairo]])
whatis([[Version : 1.14.0]])
whatis([[Target : broadwell]])
whatis([[Short description : Cairo is a 2D graphics library with support for multiple output devices.]])
whatis([[Configure options : --disable-trace --enable-tee --disable-xlib --disable-xcb --disable-pdf --disable-gobject --disable-ft --disable-fc]])

help([[Name   : cairo]])
help([[Version: 1.14.0]])
help([[Target : broadwell]])
help()
help([[Cairo is a 2D graphics library with support for multiple output devices.]])


depends_on("linux-centos7-x86_64/gcc/7.3.0/glib/2.76.1-oc4a7h7")
depends_on("linux-centos7-x86_64/gcc/7.3.0/pixman/0.42.2-vfkphva")

prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/cairo-1.14.0-zcbaxxq4kgy4hl22bmwirlqngdkmtdnm/bin", ":")
prepend_path("CPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/cairo-1.14.0-zcbaxxq4kgy4hl22bmwirlqngdkmtdnm/include", ":")
prepend_path("LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/cairo-1.14.0-zcbaxxq4kgy4hl22bmwirlqngdkmtdnm/lib", ":")
prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/cairo-1.14.0-zcbaxxq4kgy4hl22bmwirlqngdkmtdnm/lib", ":")
prepend_path("PKG_CONFIG_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/cairo-1.14.0-zcbaxxq4kgy4hl22bmwirlqngdkmtdnm/lib/pkgconfig", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/cairo-1.14.0-zcbaxxq4kgy4hl22bmwirlqngdkmtdnm/.", ":")

