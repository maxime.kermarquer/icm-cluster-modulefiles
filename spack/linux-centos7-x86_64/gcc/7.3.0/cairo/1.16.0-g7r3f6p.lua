-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2023-05-09 16:41:59.321430
--
-- cairo@1.16.0%gcc@7.3.0~X+fc+ft+gobject+pdf~png~svg build_system=autotools patches=7c4da77 arch=linux-centos7-broadwell/g7r3f6p
--

whatis([[Name : cairo]])
whatis([[Version : 1.16.0]])
whatis([[Target : broadwell]])
whatis([[Short description : Cairo is a 2D graphics library with support for multiple output devices.]])
whatis([[Configure options : --disable-trace --enable-tee --disable-xlib --disable-xcb --enable-pdf --enable-gobject --enable-ft --enable-fc]])

help([[Name   : cairo]])
help([[Version: 1.16.0]])
help([[Target : broadwell]])
help()
help([[Cairo is a 2D graphics library with support for multiple output devices.]])


depends_on("linux-centos7-x86_64/gcc/7.3.0/fontconfig/2.14.2-pnelxju")
depends_on("linux-centos7-x86_64/gcc/7.3.0/freetype/2.11.1-5stkwjm")
depends_on("linux-centos7-x86_64/gcc/7.3.0/glib/2.76.1-webav7s")
depends_on("linux-centos7-x86_64/gcc/7.3.0/pixman/0.42.2-vfkphva")

prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/cairo-1.16.0-g7r3f6p365ewqv2uxsirjha64itag4e6/bin", ":")
prepend_path("CPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/cairo-1.16.0-g7r3f6p365ewqv2uxsirjha64itag4e6/include", ":")
prepend_path("LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/cairo-1.16.0-g7r3f6p365ewqv2uxsirjha64itag4e6/lib", ":")
prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/cairo-1.16.0-g7r3f6p365ewqv2uxsirjha64itag4e6/lib", ":")
prepend_path("PKG_CONFIG_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/cairo-1.16.0-g7r3f6p365ewqv2uxsirjha64itag4e6/lib/pkgconfig", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/cairo-1.16.0-g7r3f6p365ewqv2uxsirjha64itag4e6/.", ":")

