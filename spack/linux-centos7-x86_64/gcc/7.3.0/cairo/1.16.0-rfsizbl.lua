-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2023-05-15 14:45:45.336409
--
-- cairo@1.16.0%gcc@7.3.0+X+fc+ft+gobject+pdf~png~svg build_system=autotools patches=7c4da77 arch=linux-centos7-broadwell/rfsizbl
--

whatis([[Name : cairo]])
whatis([[Version : 1.16.0]])
whatis([[Target : broadwell]])
whatis([[Short description : Cairo is a 2D graphics library with support for multiple output devices.]])
whatis([[Configure options : --disable-trace --enable-tee --enable-xlib --enable-xcb --enable-pdf --enable-gobject --enable-ft --enable-fc]])

help([[Name   : cairo]])
help([[Version: 1.16.0]])
help([[Target : broadwell]])
help()
help([[Cairo is a 2D graphics library with support for multiple output devices.]])


depends_on("linux-centos7-x86_64/gcc/7.3.0/fontconfig/2.14.2-pnelxju")
depends_on("linux-centos7-x86_64/gcc/7.3.0/freetype/2.11.1-5stkwjm")
depends_on("linux-centos7-x86_64/gcc/7.3.0/glib/2.76.1-webav7s")
depends_on("linux-centos7-x86_64/gcc/7.3.0/libx11/1.8.4-mygw7c4")
depends_on("linux-centos7-x86_64/gcc/7.3.0/libxcb/1.14-34nijb6")
depends_on("linux-centos7-x86_64/gcc/7.3.0/libxext/1.3.3-dpmqypk")
depends_on("linux-centos7-x86_64/gcc/7.3.0/libxrender/0.9.10-perzwwz")
depends_on("linux-centos7-x86_64/gcc/7.3.0/pixman/0.42.2-vfkphva")

prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/cairo-1.16.0-rfsizblqht7omvcvvokj6l4dql2baish/bin", ":")
prepend_path("CPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/cairo-1.16.0-rfsizblqht7omvcvvokj6l4dql2baish/include", ":")
prepend_path("LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/cairo-1.16.0-rfsizblqht7omvcvvokj6l4dql2baish/lib", ":")
prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/cairo-1.16.0-rfsizblqht7omvcvvokj6l4dql2baish/lib", ":")
prepend_path("PKG_CONFIG_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/cairo-1.16.0-rfsizblqht7omvcvvokj6l4dql2baish/lib/pkgconfig", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/cairo-1.16.0-rfsizblqht7omvcvvokj6l4dql2baish/.", ":")
prepend_path("XLOCALEDIR", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/libx11-1.8.4-mygw7c45hwjfojavmnf2f3qnjb3bugq4/share/X11/locale", ":")

