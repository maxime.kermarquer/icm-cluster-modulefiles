-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2023-05-04 15:12:15.457085
--
-- cmake@3.26.3%gcc@7.3.0~doc+ncurses+ownlibs~qt build_system=generic build_type=Release arch=linux-centos7-broadwell/sm45u2h
--

whatis([[Name : cmake]])
whatis([[Version : 3.26.3]])
whatis([[Target : broadwell]])
whatis([[Short description : A cross-platform, open-source build system. CMake is a family of tools designed to build, test and package software. ]])

help([[Name   : cmake]])
help([[Version: 3.26.3]])
help([[Target : broadwell]])
help()
help([[A cross-platform, open-source build system. CMake is a family of tools
designed to build, test and package software.]])


depends_on("linux-centos7-x86_64/gcc/7.3.0/ncurses/6.4-6n6u7qv")
depends_on("linux-centos7-x86_64/gcc/7.3.0/openssl/1.1.1t-f5klqzu")

prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/cmake-3.26.3-sm45u2hqntshizt3rhnm5gkausujn47o/bin", ":")
prepend_path("ACLOCAL_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/cmake-3.26.3-sm45u2hqntshizt3rhnm5gkausujn47o/share/aclocal", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/cmake-3.26.3-sm45u2hqntshizt3rhnm5gkausujn47o/.", ":")

