-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2023-05-11 10:57:22.172150
--
-- compositeproto@0.4.2%gcc@7.3.0 build_system=autotools arch=linux-centos7-broadwell/7zokwqk
--

whatis([[Name : compositeproto]])
whatis([[Version : 0.4.2]])
whatis([[Target : broadwell]])
whatis([[Short description : Composite Extension.]])

help([[Name   : compositeproto]])
help([[Version: 0.4.2]])
help([[Target : broadwell]])
help()
help([[Composite Extension. This package contains header files and
documentation for the composite extension. Library and server
implementations are separate.]])



prepend_path("PKG_CONFIG_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/compositeproto-0.4.2-7zokwqkmyff7dxex5a36nzfc3qa5czv2/lib/pkgconfig", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/compositeproto-0.4.2-7zokwqkmyff7dxex5a36nzfc3qa5czv2/.", ":")

