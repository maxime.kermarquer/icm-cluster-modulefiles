-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2023-06-05 18:13:01.202678
--
-- conmon@2.1.5%gcc@7.3.0 build_system=makefile arch=linux-centos7-broadwell/lnyllfz
--

whatis([[Name : conmon]])
whatis([[Version : 2.1.5]])
whatis([[Target : broadwell]])
whatis([[Short description : An OCI container runtime monitor]])

help([[Name   : conmon]])
help([[Version: 2.1.5]])
help([[Target : broadwell]])
help()
help([[An OCI container runtime monitor]])


depends_on("glib/2.76.1-oc4a7h7")
depends_on("libseccomp/2.5.4-jajgsv5")

prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/conmon-2.1.5-lnyllfzlmuptpgf5vc3edilazvv66wlo/bin", ":")
prepend_path("MANPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/conmon-2.1.5-lnyllfzlmuptpgf5vc3edilazvv66wlo/share/man", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/conmon-2.1.5-lnyllfzlmuptpgf5vc3edilazvv66wlo/.", ":")

