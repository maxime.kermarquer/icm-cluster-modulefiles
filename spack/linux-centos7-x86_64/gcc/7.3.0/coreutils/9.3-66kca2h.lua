-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2023-05-10 09:26:41.421626
--
-- coreutils@9.3%gcc@7.3.0~gprefix build_system=autotools arch=linux-centos7-broadwell/66kca2h
--

whatis([[Name : coreutils]])
whatis([[Version : 9.3]])
whatis([[Target : broadwell]])
whatis([[Short description : The GNU Core Utilities are the basic file, shell and text manipulation utilities of the GNU operating system. These are the core utilities which are expected to exist on every operating system. ]])

help([[Name   : coreutils]])
help([[Version: 9.3]])
help([[Target : broadwell]])
help()
help([[The GNU Core Utilities are the basic file, shell and text manipulation
utilities of the GNU operating system. These are the core utilities
which are expected to exist on every operating system.]])



prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/coreutils-9.3-66kca2h2sftqiq2tjgupbdq7famtlmhy/bin", ":")
prepend_path("MANPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/coreutils-9.3-66kca2h2sftqiq2tjgupbdq7famtlmhy/share/man", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/coreutils-9.3-66kca2h2sftqiq2tjgupbdq7famtlmhy/.", ":")

