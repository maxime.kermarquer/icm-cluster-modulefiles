-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2023-06-05 18:02:57.550030
--
-- cryptsetup@2.3.5%gcc@7.3.0 build_system=autotools arch=linux-centos7-broadwell/gdfcuey
--

whatis([[Name : cryptsetup]])
whatis([[Version : 2.3.5]])
whatis([[Target : broadwell]])
whatis([[Short description : Cryptsetup and LUKS - open-source disk encryption.]])
whatis([[Configure options : systemd_tmpfilesdir=/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/cryptsetup-2.3.5-gdfcueywqooln6eizxxr5ryn6pfvt5cq/tmpfiles.d --with-crypto_backend=openssl]])

help([[Name   : cryptsetup]])
help([[Version: 2.3.5]])
help([[Target : broadwell]])
help()
help([[Cryptsetup and LUKS - open-source disk encryption.]])


depends_on("gettext/0.21.1-nfzsems")
depends_on("json-c/0.16-r67yc2a")
depends_on("lvm2/2.03.14-kpqdsus")
depends_on("openssl/1.1.1t-f5klqzu")
depends_on("popt/1.16-6au2chr")
depends_on("util-linux/2.38.1-j6mejf3")
depends_on("util-linux-uuid/2.38.1-2oluzsj")

prepend_path("MANPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/cryptsetup-2.3.5-gdfcueywqooln6eizxxr5ryn6pfvt5cq/share/man", ":")
prepend_path("PKG_CONFIG_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/cryptsetup-2.3.5-gdfcueywqooln6eizxxr5ryn6pfvt5cq/lib/pkgconfig", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/cryptsetup-2.3.5-gdfcueywqooln6eizxxr5ryn6pfvt5cq/.", ":")

