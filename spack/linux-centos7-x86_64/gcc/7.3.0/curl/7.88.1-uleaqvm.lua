-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2023-05-15 14:40:39.070749
--
-- curl@7.88.1%gcc@7.3.0~gssapi~ldap+libidn2~librtmp~libssh~libssh2~nghttp2 build_system=autotools libs=shared,static tls=openssl arch=linux-centos7-broadwell/uleaqvm
--

whatis([[Name : curl]])
whatis([[Version : 7.88.1]])
whatis([[Target : broadwell]])
whatis([[Short description : cURL is an open source command line tool and library for transferring data with URL syntax]])

help([[Name   : curl]])
help([[Version: 7.88.1]])
help([[Target : broadwell]])
help()
help([[cURL is an open source command line tool and library for transferring
data with URL syntax]])


depends_on("linux-centos7-x86_64/gcc/7.3.0/libidn2/2.3.4-qtseck2")
depends_on("linux-centos7-x86_64/gcc/7.3.0/openssl/1.1.1t-f5klqzu")
depends_on("linux-centos7-x86_64/gcc/7.3.0/zlib/1.2.13-7shounl")

prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/curl-7.88.1-uleaqvmnd43n2wacjgqbexgkgy3sa5dy/bin", ":")
prepend_path("MANPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/curl-7.88.1-uleaqvmnd43n2wacjgqbexgkgy3sa5dy/share/man", ":")
prepend_path("ACLOCAL_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/curl-7.88.1-uleaqvmnd43n2wacjgqbexgkgy3sa5dy/share/aclocal", ":")
prepend_path("PKG_CONFIG_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/curl-7.88.1-uleaqvmnd43n2wacjgqbexgkgy3sa5dy/lib/pkgconfig", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/curl-7.88.1-uleaqvmnd43n2wacjgqbexgkgy3sa5dy/.", ":")

