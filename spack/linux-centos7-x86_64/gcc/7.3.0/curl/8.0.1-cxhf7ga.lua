-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2023-05-10 09:29:21.233189
--
-- curl@8.0.1%gcc@7.3.0~gssapi~ldap~libidn2~librtmp~libssh~libssh2+nghttp2 build_system=autotools libs=shared,static tls=openssl arch=linux-centos7-broadwell/cxhf7ga
--

whatis([[Name : curl]])
whatis([[Version : 8.0.1]])
whatis([[Target : broadwell]])
whatis([[Short description : cURL is an open source command line tool and library for transferring data with URL syntax]])

help([[Name   : curl]])
help([[Version: 8.0.1]])
help([[Target : broadwell]])
help()
help([[cURL is an open source command line tool and library for transferring
data with URL syntax]])


depends_on("linux-centos7-x86_64/gcc/7.3.0/nghttp2/1.52.0-dy5lr64")
depends_on("linux-centos7-x86_64/gcc/7.3.0/openssl/1.1.1t-f5klqzu")
depends_on("linux-centos7-x86_64/gcc/7.3.0/zlib/1.2.13-7shounl")

prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/curl-8.0.1-cxhf7gaowzqaldtjtpjrhaz7p2fhjczk/bin", ":")
prepend_path("CPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/curl-8.0.1-cxhf7gaowzqaldtjtpjrhaz7p2fhjczk/include", ":")
prepend_path("LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/curl-8.0.1-cxhf7gaowzqaldtjtpjrhaz7p2fhjczk/lib", ":")
prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/curl-8.0.1-cxhf7gaowzqaldtjtpjrhaz7p2fhjczk/lib", ":")
prepend_path("MANPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/curl-8.0.1-cxhf7gaowzqaldtjtpjrhaz7p2fhjczk/share/man", ":")
prepend_path("ACLOCAL_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/curl-8.0.1-cxhf7gaowzqaldtjtpjrhaz7p2fhjczk/share/aclocal", ":")
prepend_path("PKG_CONFIG_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/curl-8.0.1-cxhf7gaowzqaldtjtpjrhaz7p2fhjczk/lib/pkgconfig", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/curl-8.0.1-cxhf7gaowzqaldtjtpjrhaz7p2fhjczk/.", ":")

