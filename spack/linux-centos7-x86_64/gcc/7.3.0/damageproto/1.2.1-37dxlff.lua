-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2023-05-11 10:59:29.261465
--
-- damageproto@1.2.1%gcc@7.3.0 build_system=autotools arch=linux-centos7-broadwell/37dxlff
--

whatis([[Name : damageproto]])
whatis([[Version : 1.2.1]])
whatis([[Target : broadwell]])
whatis([[Short description : X Damage Extension.]])

help([[Name   : damageproto]])
help([[Version: 1.2.1]])
help([[Target : broadwell]])
help()
help([[X Damage Extension. This package contains header files and documentation
for the X Damage extension. Library and server implementations are
separate.]])



prepend_path("PKG_CONFIG_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/damageproto-1.2.1-37dxlffucl6lt3vufxebdflgutitngjv/lib/pkgconfig", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/damageproto-1.2.1-37dxlffucl6lt3vufxebdflgutitngjv/.", ":")

