-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2023-05-04 14:47:11.985446
--
-- diffutils@3.9%gcc@7.3.0 build_system=autotools arch=linux-centos7-broadwell/ijr3g2f
--

whatis([[Name : diffutils]])
whatis([[Version : 3.9]])
whatis([[Target : broadwell]])
whatis([[Short description : GNU Diffutils is a package of several programs related to finding differences between files.]])

help([[Name   : diffutils]])
help([[Version: 3.9]])
help([[Target : broadwell]])
help()
help([[GNU Diffutils is a package of several programs related to finding
differences between files.]])


depends_on("libiconv/1.17-afgvmbc")

prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/diffutils-3.9-ijr3g2flfxa5hqccnk4vrkb7xp32fw3x/bin", ":")
prepend_path("MANPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/diffutils-3.9-ijr3g2flfxa5hqccnk4vrkb7xp32fw3x/share/man", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/diffutils-3.9-ijr3g2flfxa5hqccnk4vrkb7xp32fw3x/.", ":")

