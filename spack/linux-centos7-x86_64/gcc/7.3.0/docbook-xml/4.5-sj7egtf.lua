-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2023-05-10 09:29:27.337909
--
-- docbook-xml@4.5%gcc@7.3.0 build_system=generic arch=linux-centos7-broadwell/sj7egtf
--

whatis([[Name : docbook-xml]])
whatis([[Version : 4.5]])
whatis([[Target : broadwell]])
whatis([[Short description : Docbook DTD XML files.]])

help([[Name   : docbook-xml]])
help([[Version: 4.5]])
help([[Target : broadwell]])
help()
help([[Docbook DTD XML files.]])



prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/docbook-xml-4.5-sj7egtfoi6v7k7pkgp3wtojmwpfacy4b/.", ":")
prepend_path("XML_CATALOG_FILES", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/docbook-xml-4.5-sj7egtfoi6v7k7pkgp3wtojmwpfacy4b/xml-catalog", " ")

