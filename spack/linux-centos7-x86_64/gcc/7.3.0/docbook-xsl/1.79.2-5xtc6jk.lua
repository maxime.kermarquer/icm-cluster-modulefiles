-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2023-05-10 09:31:50.349288
--
-- docbook-xsl@1.79.2%gcc@7.3.0 build_system=generic patches=a92c397 arch=linux-centos7-broadwell/5xtc6jk
--

whatis([[Name : docbook-xsl]])
whatis([[Version : 1.79.2]])
whatis([[Target : broadwell]])
whatis([[Short description : DocBook XSLT 1.0 Stylesheets.]])

help([[Name   : docbook-xsl]])
help([[Version: 1.79.2]])
help([[Target : broadwell]])
help()
help([[DocBook XSLT 1.0 Stylesheets.]])


depends_on("docbook-xml/4.5-sj7egtf")

prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/docbook-xsl-1.79.2-5xtc6jkplzgxqxuptwomcjut23q42pit/.", ":")
prepend_path("XML_CATALOG_FILES", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/docbook-xsl-1.79.2-5xtc6jkplzgxqxuptwomcjut23q42pit/xsl-catalog", " ")

