-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2023-05-04 16:36:58.013371
--
-- doxygen@1.8.20%gcc@7.3.0~graphviz~ipo~mscgen build_system=cmake build_type=RelWithDebInfo generator=make patches=3355c80 arch=linux-centos7-broadwell/nznngbj
--

whatis([[Name : doxygen]])
whatis([[Version : 1.8.20]])
whatis([[Target : broadwell]])
whatis([[Short description : Doxygen is the de facto standard tool for generating documentation from annotated C++ sources, but it also supports other popular programming languages such as C, Objective-C, C#, PHP, Java, Python, IDL (Corba, Microsoft, and UNO/OpenOffice flavors), Fortran, VHDL, Tcl, and to some extent D..]])
whatis([[Configure options : -DPYTHON_EXECUTABLE:STRING=/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/python-3.10.10-3muxe4qnheiapjqxmhqtqzjr7gdml3oi/bin/python3.10]])

help([[Name   : doxygen]])
help([[Version: 1.8.20]])
help([[Target : broadwell]])
help()
help([[Doxygen is the de facto standard tool for generating documentation from
annotated C++ sources, but it also supports other popular programming
languages such as C, Objective-C, C#, PHP, Java, Python, IDL (Corba,
Microsoft, and UNO/OpenOffice flavors), Fortran, VHDL, Tcl, and to some
extent D..]])


depends_on("libiconv/1.17-afgvmbc")

prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/doxygen-1.8.20-nznngbjofxiolesujzijg4yyfwl2bmsm/bin", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/doxygen-1.8.20-nznngbjofxiolesujzijg4yyfwl2bmsm/.", ":")

