-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2023-05-11 10:58:22.330910
--
-- dri2proto@2.8%gcc@7.3.0 build_system=autotools arch=linux-centos7-broadwell/g4clu3s
--

whatis([[Name : dri2proto]])
whatis([[Version : 2.8]])
whatis([[Target : broadwell]])
whatis([[Short description : Direct Rendering Infrastructure 2 Extension.]])

help([[Name   : dri2proto]])
help([[Version: 2.8]])
help([[Target : broadwell]])
help()
help([[Direct Rendering Infrastructure 2 Extension. This extension defines a
protocol to securely allow user applications to access the video
hardware without requiring data to be passed through the X server.]])



prepend_path("PKG_CONFIG_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/dri2proto-2.8-g4clu3s6e36pi75xh3777ujafbopgy6r/lib/pkgconfig", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/dri2proto-2.8-g4clu3s6e36pi75xh3777ujafbopgy6r/.", ":")

