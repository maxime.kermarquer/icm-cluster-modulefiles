-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2023-05-11 10:59:51.140408
--
-- dri3proto@1.0%gcc@7.3.0 build_system=autotools arch=linux-centos7-broadwell/5nnqysd
--

whatis([[Name : dri3proto]])
whatis([[Version : 1.0]])
whatis([[Target : broadwell]])
whatis([[Short description : Direct Rendering Infrastructure 3 Extension.]])

help([[Name   : dri3proto]])
help([[Version: 1.0]])
help([[Target : broadwell]])
help()
help([[Direct Rendering Infrastructure 3 Extension. This extension defines a
protocol to securely allow user applications to access the video
hardware without requiring data to be passed through the X server.]])



prepend_path("PKG_CONFIG_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/dri3proto-1.0-5nnqysdvvnmsgohriehm75olqzxob6yu/lib/pkgconfig", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/dri3proto-1.0-5nnqysdvvnmsgohriehm75olqzxob6yu/.", ":")

