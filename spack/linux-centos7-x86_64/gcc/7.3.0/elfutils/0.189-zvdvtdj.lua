-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2023-05-09 15:15:07.607585
--
-- elfutils@0.189%gcc@7.3.0~bzip2~debuginfod+nls~xz~zstd build_system=autotools arch=linux-centos7-broadwell/zvdvtdj
--

whatis([[Name : elfutils]])
whatis([[Version : 0.189]])
whatis([[Target : broadwell]])
whatis([[Short description : elfutils is a collection of various binary tools such as eu-objdump, eu-readelf, and other utilities that allow you to inspect and manipulate ELF files. Refer to Table 5.Tools Included in elfutils for Red Hat Developer for a complete list of binary tools that are distributed with the Red Hat Developer Toolset version of elfutils.]])
whatis([[Configure options : --without-bzlib --without-lzma --without-zstd --with-zlib=/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/zlib-1.2.13-7shounlfxhfmsqyucuh5nfp447lvnxn4 LDFLAGS=-Wl,--no-as-needed -L/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/gettext-0.21.1-nfzsemsi3me4hskjjbph4z5wbopxpwk7/lib -lintl --disable-debuginfod --disable-libdebuginfod]])

help([[Name   : elfutils]])
help([[Version: 0.189]])
help([[Target : broadwell]])
help()
help([[elfutils is a collection of various binary tools such as eu-objdump, eu-
readelf, and other utilities that allow you to inspect and manipulate
ELF files. Refer to Table 5.Tools Included in elfutils for Red Hat
Developer for a complete list of binary tools that are distributed with
the Red Hat Developer Toolset version of elfutils.]])


depends_on("linux-centos7-x86_64/gcc/7.3.0/gettext/0.21.1-nfzsems")
depends_on("linux-centos7-x86_64/gcc/7.3.0/pkgconf/1.8.0-haasdgy")
depends_on("linux-centos7-x86_64/gcc/7.3.0/zlib/1.2.13-7shounl")

prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/elfutils-0.189-zvdvtdjyzkbq5ypkrddiw6jupxitdhzm/bin", ":")
prepend_path("CPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/elfutils-0.189-zvdvtdjyzkbq5ypkrddiw6jupxitdhzm/include", ":")
prepend_path("LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/elfutils-0.189-zvdvtdjyzkbq5ypkrddiw6jupxitdhzm/lib", ":")
prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/elfutils-0.189-zvdvtdjyzkbq5ypkrddiw6jupxitdhzm/lib", ":")
prepend_path("MANPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/elfutils-0.189-zvdvtdjyzkbq5ypkrddiw6jupxitdhzm/share/man", ":")
prepend_path("PKG_CONFIG_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/elfutils-0.189-zvdvtdjyzkbq5ypkrddiw6jupxitdhzm/lib/pkgconfig", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/elfutils-0.189-zvdvtdjyzkbq5ypkrddiw6jupxitdhzm/.", ":")

