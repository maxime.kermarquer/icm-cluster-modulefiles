-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2023-05-04 16:11:35.185257
--
-- expat@2.5.0%gcc@7.3.0+libbsd build_system=autotools arch=linux-centos7-broadwell/yfs4wfv
--

whatis([[Name : expat]])
whatis([[Version : 2.5.0]])
whatis([[Target : broadwell]])
whatis([[Short description : Expat is an XML parser library written in C.]])

help([[Name   : expat]])
help([[Version: 2.5.0]])
help([[Target : broadwell]])
help()
help([[Expat is an XML parser library written in C.]])


depends_on("linux-centos7-x86_64/gcc/7.3.0/libbsd/0.11.7-ka2ajwy")

prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/expat-2.5.0-yfs4wfvmxzjltd3f7bkdwnu3egav3evt/bin", ":")
prepend_path("CPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/expat-2.5.0-yfs4wfvmxzjltd3f7bkdwnu3egav3evt/include", ":")
prepend_path("LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/expat-2.5.0-yfs4wfvmxzjltd3f7bkdwnu3egav3evt/lib", ":")
prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/expat-2.5.0-yfs4wfvmxzjltd3f7bkdwnu3egav3evt/lib", ":")
prepend_path("PKG_CONFIG_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/expat-2.5.0-yfs4wfvmxzjltd3f7bkdwnu3egav3evt/lib/pkgconfig", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/expat-2.5.0-yfs4wfvmxzjltd3f7bkdwnu3egav3evt/.", ":")

