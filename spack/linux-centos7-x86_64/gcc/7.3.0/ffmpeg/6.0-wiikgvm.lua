-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2023-05-10 16:04:11.594074
--
-- ffmpeg@6.0%gcc@7.3.0~X+bzlib~drawtext+gpl~libaom~libmp3lame~libopenjpeg~libopus~libsnappy~libspeex~libssh~libvorbis~libvpx~libwebp~libx264~libxml2~libzmq~lzma~nonfree~openssl~sdl2+shared+version3 build_system=autotools arch=linux-centos7-broadwell/wiikgvm
--

whatis([[Name : ffmpeg]])
whatis([[Version : 6.0]])
whatis([[Target : broadwell]])
whatis([[Short description : FFmpeg is a complete, cross-platform solution to record, convert and stream audio and video.]])
whatis([[Configure options : --enable-pic --cc=/network/lustre/iss01/apps/software/spack/0.20.0.dev0/lib/spack/env/gcc/gcc --cxx=/network/lustre/iss01/apps/software/spack/0.20.0.dev0/lib/spack/env/gcc/g++ --disable-libxcb --disable-libxcb-shape --disable-libxcb-shm --disable-libxcb-xfixes --disable-xlib --disable-libfontconfig --disable-libfreetype --disable-libfribidi --enable-bzlib --enable-gpl --disable-libmp3lame --disable-libopenjpeg --disable-libopus --disable-libspeex --disable-libvorbis --disable-libvpx --disable-libx264 --disable-nonfree --disable-openssl --enable-shared --enable-version3 --disable-libzmq --disable-libssh --disable-libwebp --disable-lzma --disable-libsnappy --disable-sdl2 --disable-libaom --disable-libxml2]])

help([[Name   : ffmpeg]])
help([[Version: 6.0]])
help([[Target : broadwell]])
help()
help([[FFmpeg is a complete, cross-platform solution to record, convert and
stream audio and video.]])


depends_on("linux-centos7-x86_64/gcc/7.3.0/alsa-lib/1.2.3.2-jtrjapo")
depends_on("linux-centos7-x86_64/gcc/7.3.0/bzip2/1.0.8-zl4fo3v")
depends_on("linux-centos7-x86_64/gcc/7.3.0/libiconv/1.17-afgvmbc")
depends_on("linux-centos7-x86_64/gcc/7.3.0/yasm/1.3.0-hpdxvqz")
depends_on("linux-centos7-x86_64/gcc/7.3.0/zlib/1.2.13-7shounl")

prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/ffmpeg-6.0-wiikgvm5iqerzz2jj3xlcoushyitbl7f/bin", ":")
prepend_path("CPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/ffmpeg-6.0-wiikgvm5iqerzz2jj3xlcoushyitbl7f/include", ":")
prepend_path("LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/ffmpeg-6.0-wiikgvm5iqerzz2jj3xlcoushyitbl7f/lib", ":")
prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/ffmpeg-6.0-wiikgvm5iqerzz2jj3xlcoushyitbl7f/lib", ":")
prepend_path("MANPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/ffmpeg-6.0-wiikgvm5iqerzz2jj3xlcoushyitbl7f/share/man", ":")
prepend_path("PKG_CONFIG_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/ffmpeg-6.0-wiikgvm5iqerzz2jj3xlcoushyitbl7f/lib/pkgconfig", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/ffmpeg-6.0-wiikgvm5iqerzz2jj3xlcoushyitbl7f/.", ":")

