-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2023-05-04 15:45:09.965470
--
-- findutils@4.9.0%gcc@7.3.0 build_system=autotools patches=440b954 arch=linux-centos7-broadwell/m4nb7lv
--

whatis([[Name : findutils]])
whatis([[Version : 4.9.0]])
whatis([[Target : broadwell]])
whatis([[Short description : The GNU Find Utilities are the basic directory searching utilities of the GNU operating system.]])

help([[Name   : findutils]])
help([[Version: 4.9.0]])
help([[Target : broadwell]])
help()
help([[The GNU Find Utilities are the basic directory searching utilities of
the GNU operating system.]])



prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/findutils-4.9.0-m4nb7lvmoyf7p5vamhndut6wev275g7t/bin", ":")
prepend_path("MANPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/findutils-4.9.0-m4nb7lvmoyf7p5vamhndut6wev275g7t/share/man", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/findutils-4.9.0-m4nb7lvmoyf7p5vamhndut6wev275g7t/.", ":")

