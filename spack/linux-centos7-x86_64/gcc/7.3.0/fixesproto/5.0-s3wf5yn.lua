-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2023-05-10 09:27:42.909976
--
-- fixesproto@5.0%gcc@7.3.0 build_system=autotools arch=linux-centos7-broadwell/s3wf5yn
--

whatis([[Name : fixesproto]])
whatis([[Version : 5.0]])
whatis([[Target : broadwell]])
whatis([[Short description : X Fixes Extension.]])

help([[Name   : fixesproto]])
help([[Version: 5.0]])
help([[Target : broadwell]])
help()
help([[X Fixes Extension. The extension makes changes to many areas of the
protocol to resolve issues raised by application interaction with core
protocol mechanisms that cannot be adequately worked around on the
client side of the wire.]])



prepend_path("PKG_CONFIG_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/fixesproto-5.0-s3wf5yn2zfun24gzxx3e3hzscvxobrv4/lib/pkgconfig", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/fixesproto-5.0-s3wf5yn2zfun24gzxx3e3hzscvxobrv4/.", ":")

