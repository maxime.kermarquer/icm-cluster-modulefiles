-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2023-05-04 16:29:12.309588
--
-- flex@2.6.3%gcc@7.3.0+lex~nls build_system=autotools arch=linux-centos7-broadwell/hn657he
--

whatis([[Name : flex]])
whatis([[Version : 2.6.3]])
whatis([[Target : broadwell]])
whatis([[Short description : Flex is a tool for generating scanners.]])
whatis([[Configure options : --disable-nls]])

help([[Name   : flex]])
help([[Version: 2.6.3]])
help([[Target : broadwell]])
help()
help([[Flex is a tool for generating scanners.]])



prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/flex-2.6.3-hn657he5tawokrzyie47eveh5oeh7dbr/bin", ":")
prepend_path("MANPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/flex-2.6.3-hn657he5tawokrzyie47eveh5oeh7dbr/share/man", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/flex-2.6.3-hn657he5tawokrzyie47eveh5oeh7dbr/.", ":")

