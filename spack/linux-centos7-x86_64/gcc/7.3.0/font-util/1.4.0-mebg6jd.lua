-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2023-05-09 16:03:41.476044
--
-- font-util@1.4.0%gcc@7.3.0 build_system=autotools fonts=encodings,font-adobe-100dpi,font-adobe-75dpi,font-adobe-utopia-100dpi,font-adobe-utopia-75dpi,font-adobe-utopia-type1,font-alias,font-arabic-misc,font-bh-100dpi,font-bh-75dpi,font-bh-lucidatypewriter-100dpi,font-bh-lucidatypewriter-75dpi,font-bh-type1,font-bitstream-100dpi,font-bitstream-75dpi,font-bitstream-speedo,font-bitstream-type1,font-cronyx-cyrillic,font-cursor-misc,font-daewoo-misc,font-dec-misc,font-ibm-type1,font-isas-misc,font-jis-misc,font-micro-misc,font-misc-cyrillic,font-misc-ethiopic,font-misc-meltho,font-misc-misc,font-mutt-misc,font-schumacher-misc,font-screen-cyrillic,font-sun-misc,font-winitzki-cyrillic,font-xfree86-type1 arch=linux-centos7-broadwell/mebg6jd
--

whatis([[Name : font-util]])
whatis([[Version : 1.4.0]])
whatis([[Target : broadwell]])
whatis([[Short description : X.Org font package creation/installation utilities and fonts.]])

help([[Name   : font-util]])
help([[Version: 1.4.0]])
help([[Target : broadwell]])
help()
help([[X.Org font package creation/installation utilities and fonts.]])



prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/font-util-1.4.0-mebg6jdflpo27flcpsmvm7ux2q3kjkcs/bin", ":")
prepend_path("LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/font-util-1.4.0-mebg6jdflpo27flcpsmvm7ux2q3kjkcs/lib", ":")
prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/font-util-1.4.0-mebg6jdflpo27flcpsmvm7ux2q3kjkcs/lib", ":")
prepend_path("MANPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/font-util-1.4.0-mebg6jdflpo27flcpsmvm7ux2q3kjkcs/share/man", ":")
prepend_path("ACLOCAL_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/font-util-1.4.0-mebg6jdflpo27flcpsmvm7ux2q3kjkcs/share/aclocal", ":")
prepend_path("PKG_CONFIG_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/font-util-1.4.0-mebg6jdflpo27flcpsmvm7ux2q3kjkcs/lib/pkgconfig", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/font-util-1.4.0-mebg6jdflpo27flcpsmvm7ux2q3kjkcs/.", ":")

