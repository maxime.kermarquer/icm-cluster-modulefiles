-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2023-05-09 16:04:48.426679
--
-- fontconfig@2.14.2%gcc@7.3.0 build_system=autotools arch=linux-centos7-broadwell/pnelxju
--

whatis([[Name : fontconfig]])
whatis([[Version : 2.14.2]])
whatis([[Target : broadwell]])
whatis([[Short description : Fontconfig is a library for configuring/customizing font access]])
whatis([[Configure options : --enable-libxml2 --disable-docs --with-default-fonts=/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/font-util-1.4.0-mebg6jdflpo27flcpsmvm7ux2q3kjkcs/share/fonts]])

help([[Name   : fontconfig]])
help([[Version: 2.14.2]])
help([[Target : broadwell]])
help()
help([[Fontconfig is a library for configuring/customizing font access]])


depends_on("linux-centos7-x86_64/gcc/7.3.0/font-util/1.4.0-mebg6jd")
depends_on("linux-centos7-x86_64/gcc/7.3.0/freetype/2.11.1-5stkwjm")
depends_on("linux-centos7-x86_64/gcc/7.3.0/libxml2/2.10.3-23vb7ui")
depends_on("linux-centos7-x86_64/gcc/7.3.0/util-linux-uuid/2.38.1-2oluzsj")

prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/fontconfig-2.14.2-pnelxjulm7au3e4s2n2navqrq45jryjs/bin", ":")
prepend_path("CPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/fontconfig-2.14.2-pnelxjulm7au3e4s2n2navqrq45jryjs/include", ":")
prepend_path("LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/fontconfig-2.14.2-pnelxjulm7au3e4s2n2navqrq45jryjs/lib", ":")
prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/fontconfig-2.14.2-pnelxjulm7au3e4s2n2navqrq45jryjs/lib", ":")
prepend_path("PKG_CONFIG_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/fontconfig-2.14.2-pnelxjulm7au3e4s2n2navqrq45jryjs/lib/pkgconfig", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/fontconfig-2.14.2-pnelxjulm7au3e4s2n2navqrq45jryjs/.", ":")

