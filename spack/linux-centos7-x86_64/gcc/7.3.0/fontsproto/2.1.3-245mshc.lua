-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2023-05-09 15:48:12.758433
--
-- fontsproto@2.1.3%gcc@7.3.0 build_system=autotools arch=linux-centos7-broadwell/245mshc
--

whatis([[Name : fontsproto]])
whatis([[Version : 2.1.3]])
whatis([[Target : broadwell]])
whatis([[Short description : X Fonts Extension.]])

help([[Name   : fontsproto]])
help([[Version: 2.1.3]])
help([[Target : broadwell]])
help()
help([[X Fonts Extension.]])



prepend_path("PKG_CONFIG_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/fontsproto-2.1.3-245mshc46gpet3fmcmoo4tqih7vou2in/lib/pkgconfig", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/fontsproto-2.1.3-245mshc46gpet3fmcmoo4tqih7vou2in/.", ":")

