-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2023-05-09 15:50:10.059439
--
-- freetype@2.11.1%gcc@7.3.0 build_system=autotools arch=linux-centos7-broadwell/5stkwjm
--

whatis([[Name : freetype]])
whatis([[Version : 2.11.1]])
whatis([[Target : broadwell]])
whatis([[Short description : FreeType is a freely available software library to render fonts. It is written in C, designed to be small, efficient, highly customizable, and portable while capable of producing high-quality output (glyph images) of most vector and bitmap font formats.]])

help([[Name   : freetype]])
help([[Version: 2.11.1]])
help([[Target : broadwell]])
help()
help([[FreeType is a freely available software library to render fonts. It is
written in C, designed to be small, efficient, highly customizable, and
portable while capable of producing high-quality output (glyph images)
of most vector and bitmap font formats.]])


depends_on("linux-centos7-x86_64/gcc/7.3.0/bzip2/1.0.8-zl4fo3v")
depends_on("linux-centos7-x86_64/gcc/7.3.0/libpng/1.6.39-zr33swm")

prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/freetype-2.11.1-5stkwjm3uqgcbr2rhrmgribnpkblolrs/bin", ":")
prepend_path("CPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/freetype-2.11.1-5stkwjm3uqgcbr2rhrmgribnpkblolrs/include", ":")
prepend_path("LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/freetype-2.11.1-5stkwjm3uqgcbr2rhrmgribnpkblolrs/lib", ":")
prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/freetype-2.11.1-5stkwjm3uqgcbr2rhrmgribnpkblolrs/lib", ":")
prepend_path("MANPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/freetype-2.11.1-5stkwjm3uqgcbr2rhrmgribnpkblolrs/share/man", ":")
prepend_path("ACLOCAL_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/freetype-2.11.1-5stkwjm3uqgcbr2rhrmgribnpkblolrs/share/aclocal", ":")
prepend_path("PKG_CONFIG_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/freetype-2.11.1-5stkwjm3uqgcbr2rhrmgribnpkblolrs/lib/pkgconfig", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/freetype-2.11.1-5stkwjm3uqgcbr2rhrmgribnpkblolrs/.", ":")

