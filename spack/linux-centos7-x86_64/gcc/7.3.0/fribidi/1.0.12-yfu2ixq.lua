-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2023-05-09 16:28:55.324101
--
-- fribidi@1.0.12%gcc@7.3.0 build_system=autotools arch=linux-centos7-broadwell/yfu2ixq
--

whatis([[Name : fribidi]])
whatis([[Version : 1.0.12]])
whatis([[Target : broadwell]])
whatis([[Short description : GNU FriBidi: The Free Implementation of the Unicode Bidirectional Algorithm.]])

help([[Name   : fribidi]])
help([[Version: 1.0.12]])
help([[Target : broadwell]])
help()
help([[GNU FriBidi: The Free Implementation of the Unicode Bidirectional
Algorithm.]])



prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/fribidi-1.0.12-yfu2ixq7qskkufqktqpw5q62ysv4vv2f/bin", ":")
prepend_path("MANPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/fribidi-1.0.12-yfu2ixq7qskkufqktqpw5q62ysv4vv2f/share/man", ":")
prepend_path("PKG_CONFIG_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/fribidi-1.0.12-yfu2ixq7qskkufqktqpw5q62ysv4vv2f/lib/pkgconfig", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/fribidi-1.0.12-yfu2ixq7qskkufqktqpw5q62ysv4vv2f/.", ":")

