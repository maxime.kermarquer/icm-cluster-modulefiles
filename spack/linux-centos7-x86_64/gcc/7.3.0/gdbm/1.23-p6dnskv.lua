-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2023-05-04 14:48:02.421519
--
-- gdbm@1.23%gcc@7.3.0 build_system=autotools arch=linux-centos7-broadwell/p6dnskv
--

whatis([[Name : gdbm]])
whatis([[Version : 1.23]])
whatis([[Target : broadwell]])
whatis([[Short description : GNU dbm (or GDBM, for short) is a library of database functions that use extensible hashing and work similar to the standard UNIX dbm. These routines are provided to a programmer needing to create and manipulate a hashed database.]])
whatis([[Configure options : --enable-libgdbm-compat CPPFLAGS=-D_GNU_SOURCE]])

help([[Name   : gdbm]])
help([[Version: 1.23]])
help([[Target : broadwell]])
help()
help([[GNU dbm (or GDBM, for short) is a library of database functions that use
extensible hashing and work similar to the standard UNIX dbm. These
routines are provided to a programmer needing to create and manipulate a
hashed database.]])


depends_on("linux-centos7-x86_64/gcc/7.3.0/readline/8.2-cqkbqun")

prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/gdbm-1.23-p6dnskvvtze7pclicneitegxamdmox64/bin", ":")
prepend_path("CPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/gdbm-1.23-p6dnskvvtze7pclicneitegxamdmox64/include", ":")
prepend_path("LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/gdbm-1.23-p6dnskvvtze7pclicneitegxamdmox64/lib", ":")
prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/gdbm-1.23-p6dnskvvtze7pclicneitegxamdmox64/lib", ":")
prepend_path("MANPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/gdbm-1.23-p6dnskvvtze7pclicneitegxamdmox64/share/man", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/gdbm-1.23-p6dnskvvtze7pclicneitegxamdmox64/.", ":")

