-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2023-05-11 11:45:06.781375
--
-- gdk-pixbuf@2.42.10%gcc@7.3.0~man~tiff~x11 build_system=generic arch=linux-centos7-broadwell/o4l3nje
--

whatis([[Name : gdk-pixbuf]])
whatis([[Version : 2.42.10]])
whatis([[Target : broadwell]])
whatis([[Short description : The Gdk Pixbuf is a toolkit for image loading and pixel buffer manipulation. It is used by GTK+ 2 and GTK+ 3 to load and manipulate images. In the past it was distributed as part of GTK+ 2 but it was split off into a separate package in preparation for the change to GTK+ 3.]])
whatis([[Configure options : --disable-gtk-doc-html GTKDOC_CHECK=/usr/bin/true GTKDOC_CHECK_PATH=/usr/bin/true GTKDOC_MKPDF=/usr/bin/true GTKDOC_REBASE=/usr/bin/true]])

help([[Name   : gdk-pixbuf]])
help([[Version: 2.42.10]])
help([[Target : broadwell]])
help()
help([[The Gdk Pixbuf is a toolkit for image loading and pixel buffer
manipulation. It is used by GTK+ 2 and GTK+ 3 to load and manipulate
images. In the past it was distributed as part of GTK+ 2 but it was
split off into a separate package in preparation for the change to GTK+
3.]])


depends_on("linux-centos7-x86_64/gcc/7.3.0/gettext/0.21.1-nfzsems")
depends_on("linux-centos7-x86_64/gcc/7.3.0/glib/2.76.1-webav7s")
depends_on("linux-centos7-x86_64/gcc/7.3.0/gobject-introspection/1.72.1-hxu4bzn")
depends_on("linux-centos7-x86_64/gcc/7.3.0/libjpeg-turbo/2.1.4-wdiktsm")
depends_on("linux-centos7-x86_64/gcc/7.3.0/libpng/1.6.39-zr33swm")
depends_on("linux-centos7-x86_64/gcc/7.3.0/shared-mime-info/1.9-6etij5m")
depends_on("linux-centos7-x86_64/gcc/7.3.0/zlib/1.2.13-7shounl")

prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/gdk-pixbuf-2.42.10-o4l3njentoxlwkbwcqrsssa4gueih5nv/bin", ":")
prepend_path("PKG_CONFIG_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/gdk-pixbuf-2.42.10-o4l3njentoxlwkbwcqrsssa4gueih5nv/lib/pkgconfig", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/gdk-pixbuf-2.42.10-o4l3njentoxlwkbwcqrsssa4gueih5nv/.", ":")
prepend_path("XDG_DATA_DIRS", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/gobject-introspection-1.72.1-hxu4bzn4ko7cxpwgjld2mm6yqnxwtojo/share", ":")
prepend_path("GI_TYPELIB_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/gobject-introspection-1.72.1-hxu4bzn4ko7cxpwgjld2mm6yqnxwtojo/lib/girepository-1.0", ":")
prepend_path("XDG_DATA_DIRS", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/shared-mime-info-1.9-6etij5mjgjpq4x6qnmac2uplbftk62nd/share", ":")
prepend_path("GI_TYPELIB_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/gdk-pixbuf-2.42.10-o4l3njentoxlwkbwcqrsssa4gueih5nv/lib/girepository-1.0", ":")

