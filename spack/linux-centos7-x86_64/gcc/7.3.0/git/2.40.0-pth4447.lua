-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2023-06-05 18:04:59.744919
--
-- git@2.40.0%gcc@7.3.0+man+nls+perl+subtree~svn~tcltk build_system=autotools arch=linux-centos7-broadwell/pth4447
--

whatis([[Name : git]])
whatis([[Version : 2.40.0]])
whatis([[Target : broadwell]])
whatis([[Short description : Git is a free and open source distributed version control system designed to handle everything from small to very large projects with speed and efficiency. ]])
whatis([[Configure options : --with-curl=/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/curl-8.0.1-r5jfu73a3ou477rpnswzsuokvyhdyzzk --with-expat=/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/expat-2.5.0-yfs4wfvmxzjltd3f7bkdwnu3egav3evt --with-iconv=/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/libiconv-1.17-afgvmbc7dt2refbap2qamy32yibwnrcr --with-openssl=/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/openssl-1.1.1t-f5klqzusu6fj4i4bdrxkxvvydvzj7ofa --with-zlib=/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/zlib-1.2.13-7shounlfxhfmsqyucuh5nfp447lvnxn4 --with-perl=/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/perl-5.36.0-3zk6frfcwciyq3jht4bvkevq7n2m6rdr/bin/perl --with-libpcre2=/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/pcre2-10.42-g7g6n3abbu4gmm5tfthrs2hbwo7olrzz --without-tcltk]])

help([[Name   : git]])
help([[Version: 2.40.0]])
help([[Target : broadwell]])
help()
help([[Git is a free and open source distributed version control system
designed to handle everything from small to very large projects with
speed and efficiency.]])


depends_on("curl/8.0.1-r5jfu73")
depends_on("expat/2.5.0-yfs4wfv")
depends_on("gettext/0.21.1-nfzsems")
depends_on("libiconv/1.17-afgvmbc")
depends_on("libidn2/2.3.4-qtseck2")
depends_on("openssh/9.3p1-mnosany")
depends_on("openssl/1.1.1t-f5klqzu")
depends_on("pcre2/10.42-g7g6n3a")
depends_on("perl/5.36.0-3zk6frf")
depends_on("zlib/1.2.13-7shounl")

prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/git-2.40.0-pth4447rxzxagqf6lrlpowokhdvmvfzt/bin", ":")
prepend_path("MANPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/git-2.40.0-pth4447rxzxagqf6lrlpowokhdvmvfzt/share/man", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/git-2.40.0-pth4447rxzxagqf6lrlpowokhdvmvfzt/.", ":")

