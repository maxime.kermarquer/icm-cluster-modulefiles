-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2023-05-09 15:21:03.244563
--
-- glib@2.76.1%gcc@7.3.0~libmount build_system=generic patches=fa31180 tracing=none arch=linux-centos7-broadwell/oc4a7h7
--

whatis([[Name : glib]])
whatis([[Version : 2.76.1]])
whatis([[Target : broadwell]])
whatis([[Short description : GLib provides the core application building blocks for libraries and applications written in C.]])
whatis([[Configure options : --disable-libmount --with-python=python3.10 --with-libiconv=gnu --disable-dtrace --disable-systemtap --disable-selinux --disable-gtk-doc-html GTKDOC_CHECK=/usr/bin/true GTKDOC_CHECK_PATH=/usr/bin/true GTKDOC_MKPDF=/usr/bin/true GTKDOC_REBASE=/usr/bin/true]])

help([[Name   : glib]])
help([[Version: 2.76.1]])
help([[Target : broadwell]])
help()
help([[GLib provides the core application building blocks for libraries and
applications written in C. The GLib package contains a low-level
libraries useful for providing data structure handling for C,
portability wrappers and interfaces for such runtime functionality as an
event loop, threads, dynamic loading and an object system.]])


depends_on("linux-centos7-x86_64/gcc/7.3.0/elfutils/0.189-zvdvtdj")
depends_on("linux-centos7-x86_64/gcc/7.3.0/gettext/0.21.1-nfzsems")
depends_on("linux-centos7-x86_64/gcc/7.3.0/libffi/3.4.4-cacrjpo")
depends_on("linux-centos7-x86_64/gcc/7.3.0/libiconv/1.17-afgvmbc")
depends_on("linux-centos7-x86_64/gcc/7.3.0/pcre2/10.42-g7g6n3a")
depends_on("linux-centos7-x86_64/gcc/7.3.0/perl/5.36.0-3zk6frf")
depends_on("linux-centos7-x86_64/gcc/7.3.0/python/3.10.10-3muxe4q")
depends_on("linux-centos7-x86_64/gcc/7.3.0/zlib/1.2.13-7shounl")

prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/glib-2.76.1-oc4a7h7d2sh5b3azrd6sksdi5s3yuqeq/bin", ":")
prepend_path("ACLOCAL_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/glib-2.76.1-oc4a7h7d2sh5b3azrd6sksdi5s3yuqeq/share/aclocal", ":")
prepend_path("PKG_CONFIG_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/glib-2.76.1-oc4a7h7d2sh5b3azrd6sksdi5s3yuqeq/lib/pkgconfig", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/glib-2.76.1-oc4a7h7d2sh5b3azrd6sksdi5s3yuqeq/.", ":")

