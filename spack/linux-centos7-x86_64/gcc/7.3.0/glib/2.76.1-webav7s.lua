-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2023-05-09 16:37:05.214608
--
-- glib@2.76.1%gcc@7.3.0~libmount build_system=generic patches=fa31180 tracing=none arch=linux-centos7-broadwell/webav7s
--

whatis([[Name : glib]])
whatis([[Version : 2.76.1]])
whatis([[Target : broadwell]])
whatis([[Short description : GLib provides the core application building blocks for libraries and applications written in C.]])
whatis([[Configure options : --disable-libmount --with-python=python3.10 --with-libiconv=gnu --disable-dtrace --disable-systemtap --disable-selinux --disable-gtk-doc-html GTKDOC_CHECK=/usr/bin/true GTKDOC_CHECK_PATH=/usr/bin/true GTKDOC_MKPDF=/usr/bin/true GTKDOC_REBASE=/usr/bin/true]])

help([[Name   : glib]])
help([[Version: 2.76.1]])
help([[Target : broadwell]])
help()
help([[GLib provides the core application building blocks for libraries and
applications written in C. The GLib package contains a low-level
libraries useful for providing data structure handling for C,
portability wrappers and interfaces for such runtime functionality as an
event loop, threads, dynamic loading and an object system.]])


depends_on("linux-centos7-x86_64/gcc/7.3.0/elfutils/0.189-zvdvtdj")
depends_on("linux-centos7-x86_64/gcc/7.3.0/gettext/0.21.1-nfzsems")
depends_on("linux-centos7-x86_64/gcc/7.3.0/libffi/3.3-bbd3for")
depends_on("linux-centos7-x86_64/gcc/7.3.0/libiconv/1.17-afgvmbc")
depends_on("linux-centos7-x86_64/gcc/7.3.0/pcre2/10.42-g7g6n3a")
depends_on("linux-centos7-x86_64/gcc/7.3.0/perl/5.36.0-3zk6frf")
depends_on("linux-centos7-x86_64/gcc/7.3.0/python/3.10.10-x6q4jmz")
depends_on("linux-centos7-x86_64/gcc/7.3.0/zlib/1.2.13-7shounl")

prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/glib-2.76.1-webav7s3gr2ewolfkgt4gsl5rr2nx7gw/bin", ":")
prepend_path("CPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/glib-2.76.1-webav7s3gr2ewolfkgt4gsl5rr2nx7gw/include", ":")
prepend_path("LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/glib-2.76.1-webav7s3gr2ewolfkgt4gsl5rr2nx7gw/lib", ":")
prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/glib-2.76.1-webav7s3gr2ewolfkgt4gsl5rr2nx7gw/lib", ":")
prepend_path("ACLOCAL_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/glib-2.76.1-webav7s3gr2ewolfkgt4gsl5rr2nx7gw/share/aclocal", ":")
prepend_path("PKG_CONFIG_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/glib-2.76.1-webav7s3gr2ewolfkgt4gsl5rr2nx7gw/lib/pkgconfig", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/glib-2.76.1-webav7s3gr2ewolfkgt4gsl5rr2nx7gw/.", ":")

