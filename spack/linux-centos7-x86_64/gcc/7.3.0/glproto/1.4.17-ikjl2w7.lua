-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2023-05-10 09:27:29.583741
--
-- glproto@1.4.17%gcc@7.3.0 build_system=autotools arch=linux-centos7-broadwell/ikjl2w7
--

whatis([[Name : glproto]])
whatis([[Version : 1.4.17]])
whatis([[Target : broadwell]])
whatis([[Short description : OpenGL Extension to the X Window System.]])

help([[Name   : glproto]])
help([[Version: 1.4.17]])
help([[Target : broadwell]])
help()
help([[OpenGL Extension to the X Window System. This extension defines a
protocol for the client to send 3D rendering commands to the X server.]])



prepend_path("PKG_CONFIG_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/glproto-1.4.17-ikjl2w7slbsf2gvbvm5qx6oozu7f7ris/lib/pkgconfig", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/glproto-1.4.17-ikjl2w7slbsf2gvbvm5qx6oozu7f7ris/.", ":")

