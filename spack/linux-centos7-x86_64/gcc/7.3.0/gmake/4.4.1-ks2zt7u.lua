-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2023-05-04 15:45:40.279125
--
-- gmake@4.4.1%gcc@7.3.0~guile build_system=autotools arch=linux-centos7-broadwell/ks2zt7u
--

whatis([[Name : gmake]])
whatis([[Version : 4.4.1]])
whatis([[Target : broadwell]])
whatis([[Short description : GNU Make is a tool which controls the generation of executables and other non-source files of a program from the program's source files.]])
whatis([[Configure options : --without-guile --disable-nls]])

help([[Name   : gmake]])
help([[Version: 4.4.1]])
help([[Target : broadwell]])
help()
help([[GNU Make is a tool which controls the generation of executables and
other non-source files of a program from the program's source files.]])



prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/gmake-4.4.1-ks2zt7ulkjiauhehxssbxeujimihgrwj/bin", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/gmake-4.4.1-ks2zt7ulkjiauhehxssbxeujimihgrwj/.", ":")

