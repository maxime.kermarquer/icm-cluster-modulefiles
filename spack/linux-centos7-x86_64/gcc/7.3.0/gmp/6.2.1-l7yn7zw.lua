-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2023-05-10 10:39:38.210685
--
-- gmp@6.2.1%gcc@7.3.0+cxx build_system=autotools libs=shared,static patches=69ad2e2 arch=linux-centos7-broadwell/l7yn7zw
--

whatis([[Name : gmp]])
whatis([[Version : 6.2.1]])
whatis([[Target : broadwell]])
whatis([[Short description : GMP is a free library for arbitrary precision arithmetic, operating on signed integers, rational numbers, and floating-point numbers.]])
whatis([[Configure options : --enable-cxx --enable-shared --enable-static --with-pic]])

help([[Name   : gmp]])
help([[Version: 6.2.1]])
help([[Target : broadwell]])
help()
help([[GMP is a free library for arbitrary precision arithmetic, operating on
signed integers, rational numbers, and floating-point numbers.]])



prepend_path("CPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/gmp-6.2.1-l7yn7zwk5whymcmwqbydkmtebtk4mxj3/include", ":")
prepend_path("LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/gmp-6.2.1-l7yn7zwk5whymcmwqbydkmtebtk4mxj3/lib", ":")
prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/gmp-6.2.1-l7yn7zwk5whymcmwqbydkmtebtk4mxj3/lib", ":")
prepend_path("PKG_CONFIG_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/gmp-6.2.1-l7yn7zwk5whymcmwqbydkmtebtk4mxj3/lib/pkgconfig", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/gmp-6.2.1-l7yn7zwk5whymcmwqbydkmtebtk4mxj3/.", ":")

