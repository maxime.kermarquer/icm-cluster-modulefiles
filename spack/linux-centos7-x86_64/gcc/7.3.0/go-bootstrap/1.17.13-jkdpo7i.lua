-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2023-06-05 18:07:28.129296
--
-- go-bootstrap@1.17.13%gcc@7.3.0 build_system=generic arch=linux-centos7-broadwell/jkdpo7i
--

whatis([[Name : go-bootstrap]])
whatis([[Version : 1.17.13]])
whatis([[Target : broadwell]])
whatis([[Short description : Old C-bootstrapped go to bootstrap real go]])

help([[Name   : go-bootstrap]])
help([[Version: 1.17.13]])
help([[Target : broadwell]])
help()
help([[Old C-bootstrapped go to bootstrap real go]])


depends_on("git/2.40.0-pth4447")

prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/go-bootstrap-1.17.13-jkdpo7izjwlci33ji4qt2fgspvs7np77/bin", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/go-bootstrap-1.17.13-jkdpo7izjwlci33ji4qt2fgspvs7np77/.", ":")

