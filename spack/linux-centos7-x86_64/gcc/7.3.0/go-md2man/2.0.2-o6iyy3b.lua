-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2023-06-05 18:12:52.285791
--
-- go-md2man@2.0.2%gcc@7.3.0 build_system=generic arch=linux-centos7-broadwell/o6iyy3b
--

whatis([[Name : go-md2man]])
whatis([[Version : 2.0.2]])
whatis([[Target : broadwell]])
whatis([[Short description : go-md2man converts markdown into roff (man pages)]])

help([[Name   : go-md2man]])
help([[Version: 2.0.2]])
help([[Target : broadwell]])
help()
help([[go-md2man converts markdown into roff (man pages)]])


depends_on("go/1.20.3-7gfmlz5")

prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/go-md2man-2.0.2-o6iyy3bwia3ivuhxke6oulxzcs4eauha/bin", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/go-md2man-2.0.2-o6iyy3bwia3ivuhxke6oulxzcs4eauha/.", ":")
prepend_path("GOPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/go-md2man-2.0.2-o6iyy3bwia3ivuhxke6oulxzcs4eauha", ":")

