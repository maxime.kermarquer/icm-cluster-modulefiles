-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2023-06-05 18:12:25.528746
--
-- go@1.20.3%gcc@7.3.0 build_system=generic arch=linux-centos7-broadwell/7gfmlz5
--

whatis([[Name : go]])
whatis([[Version : 1.20.3]])
whatis([[Target : broadwell]])
whatis([[Short description : The golang compiler and build environment]])

help([[Name   : go]])
help([[Version: 1.20.3]])
help([[Target : broadwell]])
help()
help([[The golang compiler and build environment]])


depends_on("git/2.40.0-pth4447")

prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/go-1.20.3-7gfmlz52p777s5dviutew6qcc73zndte/bin", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/go-1.20.3-7gfmlz52p777s5dviutew6qcc73zndte/.", ":")

