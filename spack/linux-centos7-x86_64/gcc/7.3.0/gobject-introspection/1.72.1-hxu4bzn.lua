-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2023-05-09 16:43:16.902081
--
-- gobject-introspection@1.72.1%gcc@7.3.0~strip build_system=meson buildtype=debugoptimized default_library=shared arch=linux-centos7-broadwell/hxu4bzn
--

whatis([[Name : gobject-introspection]])
whatis([[Version : 1.72.1]])
whatis([[Target : broadwell]])
whatis([[Short description : The GObject Introspection is used to describe the program APIs and collect them in a uniform, machine readable format.Cairo is a 2D graphics library with support for multiple output ]])

help([[Name   : gobject-introspection]])
help([[Version: 1.72.1]])
help([[Target : broadwell]])
help()
help([[The GObject Introspection is used to describe the program APIs and
collect them in a uniform, machine readable format.Cairo is a 2D
graphics library with support for multiple output]])


depends_on("linux-centos7-x86_64/gcc/7.3.0/cairo/1.16.0-g7r3f6p")
depends_on("linux-centos7-x86_64/gcc/7.3.0/glib/2.76.1-webav7s")
depends_on("linux-centos7-x86_64/gcc/7.3.0/libffi/3.3-bbd3for")
depends_on("linux-centos7-x86_64/gcc/7.3.0/python/3.10.10-x6q4jmz")

prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/gobject-introspection-1.72.1-hxu4bzn4ko7cxpwgjld2mm6yqnxwtojo/bin", ":")
prepend_path("MANPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/gobject-introspection-1.72.1-hxu4bzn4ko7cxpwgjld2mm6yqnxwtojo/share/man", ":")
prepend_path("ACLOCAL_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/gobject-introspection-1.72.1-hxu4bzn4ko7cxpwgjld2mm6yqnxwtojo/share/aclocal", ":")
prepend_path("PKG_CONFIG_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/gobject-introspection-1.72.1-hxu4bzn4ko7cxpwgjld2mm6yqnxwtojo/lib/pkgconfig", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/gobject-introspection-1.72.1-hxu4bzn4ko7cxpwgjld2mm6yqnxwtojo/.", ":")
prepend_path("GI_TYPELIB_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/gobject-introspection-1.72.1-hxu4bzn4ko7cxpwgjld2mm6yqnxwtojo/lib/girepository-1.0", ":")

