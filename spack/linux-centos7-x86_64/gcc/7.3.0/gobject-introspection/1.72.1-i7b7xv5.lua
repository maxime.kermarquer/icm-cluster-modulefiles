-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2023-05-15 14:46:52.367456
--
-- gobject-introspection@1.72.1%gcc@7.3.0~strip build_system=meson buildtype=debugoptimized default_library=shared arch=linux-centos7-broadwell/i7b7xv5
--

whatis([[Name : gobject-introspection]])
whatis([[Version : 1.72.1]])
whatis([[Target : broadwell]])
whatis([[Short description : The GObject Introspection is used to describe the program APIs and collect them in a uniform, machine readable format.Cairo is a 2D graphics library with support for multiple output ]])

help([[Name   : gobject-introspection]])
help([[Version: 1.72.1]])
help([[Target : broadwell]])
help()
help([[The GObject Introspection is used to describe the program APIs and
collect them in a uniform, machine readable format.Cairo is a 2D
graphics library with support for multiple output]])


depends_on("linux-centos7-x86_64/gcc/7.3.0/cairo/1.16.0-rfsizbl")
depends_on("linux-centos7-x86_64/gcc/7.3.0/glib/2.76.1-webav7s")
depends_on("linux-centos7-x86_64/gcc/7.3.0/libffi/3.3-bbd3for")
depends_on("linux-centos7-x86_64/gcc/7.3.0/python/3.10.10-x6q4jmz")

prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/gobject-introspection-1.72.1-i7b7xv5y2xgpj237dvp2otbo67ex6l6d/bin", ":")
prepend_path("MANPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/gobject-introspection-1.72.1-i7b7xv5y2xgpj237dvp2otbo67ex6l6d/share/man", ":")
prepend_path("ACLOCAL_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/gobject-introspection-1.72.1-i7b7xv5y2xgpj237dvp2otbo67ex6l6d/share/aclocal", ":")
prepend_path("PKG_CONFIG_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/gobject-introspection-1.72.1-i7b7xv5y2xgpj237dvp2otbo67ex6l6d/lib/pkgconfig", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/gobject-introspection-1.72.1-i7b7xv5y2xgpj237dvp2otbo67ex6l6d/.", ":")
prepend_path("XLOCALEDIR", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/libx11-1.8.4-mygw7c45hwjfojavmnf2f3qnjb3bugq4/share/X11/locale", ":")
prepend_path("GI_TYPELIB_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/gobject-introspection-1.72.1-i7b7xv5y2xgpj237dvp2otbo67ex6l6d/lib/girepository-1.0", ":")

