-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2023-05-09 15:47:33.992256
--
-- gperf@3.1%gcc@7.3.0 build_system=autotools arch=linux-centos7-broadwell/rqtoziu
--

whatis([[Name : gperf]])
whatis([[Version : 3.1]])
whatis([[Target : broadwell]])
whatis([[Short description : GNU gperf is a perfect hash function generator. For a given list of strings, it produces a hash function and hash table, in form of C or C++ code, for looking up a value depending on the input string. The hash function is perfect, which means that the hash table has no collisions, and the hash table lookup needs a single string comparison only.]])

help([[Name   : gperf]])
help([[Version: 3.1]])
help([[Target : broadwell]])
help()
help([[GNU gperf is a perfect hash function generator. For a given list of
strings, it produces a hash function and hash table, in form of C or C++
code, for looking up a value depending on the input string. The hash
function is perfect, which means that the hash table has no collisions,
and the hash table lookup needs a single string comparison only.]])



prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/gperf-3.1-rqtoziu76ueop5pi6agpoy4fucuh7dn5/bin", ":")
prepend_path("MANPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/gperf-3.1-rqtoziu76ueop5pi6agpoy4fucuh7dn5/share/man", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/gperf-3.1-rqtoziu76ueop5pi6agpoy4fucuh7dn5/.", ":")

