-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2023-05-10 09:32:23.632500
--
-- graphite2@1.3.14%gcc@7.3.0~ipo build_system=cmake build_type=RelWithDebInfo generator=make patches=39e58d6 arch=linux-centos7-broadwell/vg7xesc
--

whatis([[Name : graphite2]])
whatis([[Version : 1.3.14]])
whatis([[Target : broadwell]])
whatis([[Short description : Graphite is a system that can be used to create 'smart fonts' capable of displaying writing systems with various complex behaviors. A smart font contains not only letter shapes but also additional instructions indicating how to combine and position the letters in complex ways.]])

help([[Name   : graphite2]])
help([[Version: 1.3.14]])
help([[Target : broadwell]])
help()
help([[Graphite is a system that can be used to create "smart fonts" capable of
displaying writing systems with various complex behaviors. A smart font
contains not only letter shapes but also additional instructions
indicating how to combine and position the letters in complex ways.]])


depends_on("linux-centos7-x86_64/gcc/7.3.0/freetype/2.11.1-5stkwjm")

prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/graphite2-1.3.14-vg7xescv6tbl5vu6326t2rzhrzk7r6hx/bin", ":")
prepend_path("PKG_CONFIG_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/graphite2-1.3.14-vg7xescv6tbl5vu6326t2rzhrzk7r6hx/lib/pkgconfig", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/graphite2-1.3.14-vg7xescv6tbl5vu6326t2rzhrzk7r6hx/.", ":")

