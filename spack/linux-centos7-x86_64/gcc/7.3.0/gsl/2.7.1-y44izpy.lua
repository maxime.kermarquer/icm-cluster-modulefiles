-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2023-05-10 15:03:16.331533
--
-- gsl@2.7.1%gcc@7.3.0~external-cblas build_system=autotools arch=linux-centos7-broadwell/y44izpy
--

whatis([[Name : gsl]])
whatis([[Version : 2.7.1]])
whatis([[Target : broadwell]])
whatis([[Short description : The GNU Scientific Library (GSL) is a numerical library for C and C++ programmers. It is free software under the GNU General Public License. The library provides a wide range of mathematical routines such as random number generators, special functions and least-squares fitting. There are over 1000 functions in total with an extensive test suite.]])

help([[Name   : gsl]])
help([[Version: 2.7.1]])
help([[Target : broadwell]])
help()
help([[The GNU Scientific Library (GSL) is a numerical library for C and C++
programmers. It is free software under the GNU General Public License.
The library provides a wide range of mathematical routines such as
random number generators, special functions and least-squares fitting.
There are over 1000 functions in total with an extensive test suite.]])



prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/gsl-2.7.1-y44izpyngiycrkchl7d6kmyilulfyyto/bin", ":")
prepend_path("CPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/gsl-2.7.1-y44izpyngiycrkchl7d6kmyilulfyyto/include", ":")
prepend_path("LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/gsl-2.7.1-y44izpyngiycrkchl7d6kmyilulfyyto/lib", ":")
prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/gsl-2.7.1-y44izpyngiycrkchl7d6kmyilulfyyto/lib", ":")
prepend_path("MANPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/gsl-2.7.1-y44izpyngiycrkchl7d6kmyilulfyyto/share/man", ":")
prepend_path("ACLOCAL_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/gsl-2.7.1-y44izpyngiycrkchl7d6kmyilulfyyto/share/aclocal", ":")
prepend_path("PKG_CONFIG_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/gsl-2.7.1-y44izpyngiycrkchl7d6kmyilulfyyto/lib/pkgconfig", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/gsl-2.7.1-y44izpyngiycrkchl7d6kmyilulfyyto/.", ":")

