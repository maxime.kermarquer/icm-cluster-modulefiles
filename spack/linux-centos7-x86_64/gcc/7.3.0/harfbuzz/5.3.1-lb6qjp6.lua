-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2023-05-09 16:47:05.827593
--
-- harfbuzz@5.3.1%gcc@7.3.0~graphite2~strip build_system=meson buildtype=debugoptimized default_library=shared arch=linux-centos7-broadwell/lb6qjp6
--

whatis([[Name : harfbuzz]])
whatis([[Version : 5.3.1]])
whatis([[Target : broadwell]])
whatis([[Short description : The Harfbuzz package contains an OpenType text shaping engine.]])

help([[Name   : harfbuzz]])
help([[Version: 5.3.1]])
help([[Target : broadwell]])
help()
help([[The Harfbuzz package contains an OpenType text shaping engine.]])


depends_on("linux-centos7-x86_64/gcc/7.3.0/cairo/1.16.0-g7r3f6p")
depends_on("linux-centos7-x86_64/gcc/7.3.0/freetype/2.11.1-5stkwjm")
depends_on("linux-centos7-x86_64/gcc/7.3.0/glib/2.76.1-webav7s")
depends_on("linux-centos7-x86_64/gcc/7.3.0/gobject-introspection/1.72.1-hxu4bzn")
depends_on("linux-centos7-x86_64/gcc/7.3.0/icu4c/67.1-u2hrcsv")
depends_on("linux-centos7-x86_64/gcc/7.3.0/zlib/1.2.13-7shounl")

prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/harfbuzz-5.3.1-lb6qjp6723kuxl6kwudvipgeynmsbz3j/bin", ":")
prepend_path("CPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/harfbuzz-5.3.1-lb6qjp6723kuxl6kwudvipgeynmsbz3j/include", ":")
prepend_path("LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/harfbuzz-5.3.1-lb6qjp6723kuxl6kwudvipgeynmsbz3j/lib", ":")
prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/harfbuzz-5.3.1-lb6qjp6723kuxl6kwudvipgeynmsbz3j/lib", ":")
prepend_path("PKG_CONFIG_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/harfbuzz-5.3.1-lb6qjp6723kuxl6kwudvipgeynmsbz3j/lib/pkgconfig", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/harfbuzz-5.3.1-lb6qjp6723kuxl6kwudvipgeynmsbz3j/.", ":")
prepend_path("XDG_DATA_DIRS", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/gobject-introspection-1.72.1-hxu4bzn4ko7cxpwgjld2mm6yqnxwtojo/share", ":")
prepend_path("GI_TYPELIB_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/gobject-introspection-1.72.1-hxu4bzn4ko7cxpwgjld2mm6yqnxwtojo/lib/girepository-1.0", ":")
prepend_path("GI_TYPELIB_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/harfbuzz-5.3.1-lb6qjp6723kuxl6kwudvipgeynmsbz3j/lib/girepository-1.0", ":")

