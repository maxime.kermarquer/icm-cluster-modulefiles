-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2023-05-15 14:51:04.345979
--
-- harfbuzz@5.3.1%gcc@7.3.0+graphite2~strip build_system=meson buildtype=debugoptimized default_library=shared arch=linux-centos7-broadwell/obk64wt
--

whatis([[Name : harfbuzz]])
whatis([[Version : 5.3.1]])
whatis([[Target : broadwell]])
whatis([[Short description : The Harfbuzz package contains an OpenType text shaping engine.]])

help([[Name   : harfbuzz]])
help([[Version: 5.3.1]])
help([[Target : broadwell]])
help()
help([[The Harfbuzz package contains an OpenType text shaping engine.]])


depends_on("linux-centos7-x86_64/gcc/7.3.0/cairo/1.16.0-rfsizbl")
depends_on("linux-centos7-x86_64/gcc/7.3.0/freetype/2.11.1-5stkwjm")
depends_on("linux-centos7-x86_64/gcc/7.3.0/glib/2.76.1-webav7s")
depends_on("linux-centos7-x86_64/gcc/7.3.0/gobject-introspection/1.72.1-i7b7xv5")
depends_on("linux-centos7-x86_64/gcc/7.3.0/graphite2/1.3.14-vg7xesc")
depends_on("linux-centos7-x86_64/gcc/7.3.0/icu4c/67.1-u2hrcsv")
depends_on("linux-centos7-x86_64/gcc/7.3.0/zlib/1.2.13-7shounl")

prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/harfbuzz-5.3.1-obk64wt5uophjhoeg753nspxanxuqet2/bin", ":")
prepend_path("CPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/harfbuzz-5.3.1-obk64wt5uophjhoeg753nspxanxuqet2/include/harfbuzz", ":")
prepend_path("LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/harfbuzz-5.3.1-obk64wt5uophjhoeg753nspxanxuqet2/lib", ":")
prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/harfbuzz-5.3.1-obk64wt5uophjhoeg753nspxanxuqet2/lib", ":")
prepend_path("PKG_CONFIG_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/harfbuzz-5.3.1-obk64wt5uophjhoeg753nspxanxuqet2/lib/pkgconfig", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/harfbuzz-5.3.1-obk64wt5uophjhoeg753nspxanxuqet2/.", ":")
prepend_path("XLOCALEDIR", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/libx11-1.8.4-mygw7c45hwjfojavmnf2f3qnjb3bugq4/share/X11/locale", ":")
prepend_path("XDG_DATA_DIRS", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/gobject-introspection-1.72.1-i7b7xv5y2xgpj237dvp2otbo67ex6l6d/share", ":")
prepend_path("GI_TYPELIB_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/gobject-introspection-1.72.1-i7b7xv5y2xgpj237dvp2otbo67ex6l6d/lib/girepository-1.0", ":")
prepend_path("GI_TYPELIB_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/harfbuzz-5.3.1-obk64wt5uophjhoeg753nspxanxuqet2/lib/girepository-1.0", ":")

