-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2023-05-09 16:32:32.630878
--
-- icu4c@67.1%gcc@7.3.0 build_system=autotools cxxstd=11 arch=linux-centos7-broadwell/u2hrcsv
--

whatis([[Name : icu4c]])
whatis([[Version : 67.1]])
whatis([[Target : broadwell]])
whatis([[Short description : ICU is a mature, widely used set of C/C++ and Java libraries providing Unicode and Globalization support for software applications. ICU4C is the C/C++ interface.]])
whatis([[Configure options : PYTHON=/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/python-3.10.10-x6q4jmzgwldfop6pv6coqud2iumb4le3/bin/python3.10]])

help([[Name   : icu4c]])
help([[Version: 67.1]])
help([[Target : broadwell]])
help()
help([[ICU is a mature, widely used set of C/C++ and Java libraries providing
Unicode and Globalization support for software applications. ICU4C is
the C/C++ interface.]])



prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/icu4c-67.1-u2hrcsvovxdcqn4kjmi3ooam737mph23/bin", ":")
prepend_path("CPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/icu4c-67.1-u2hrcsvovxdcqn4kjmi3ooam737mph23/include", ":")
prepend_path("LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/icu4c-67.1-u2hrcsvovxdcqn4kjmi3ooam737mph23/lib", ":")
prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/icu4c-67.1-u2hrcsvovxdcqn4kjmi3ooam737mph23/lib", ":")
prepend_path("MANPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/icu4c-67.1-u2hrcsvovxdcqn4kjmi3ooam737mph23/share/man", ":")
prepend_path("PKG_CONFIG_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/icu4c-67.1-u2hrcsvovxdcqn4kjmi3ooam737mph23/lib/pkgconfig", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/icu4c-67.1-u2hrcsvovxdcqn4kjmi3ooam737mph23/.", ":")

