-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2023-05-15 14:13:43.032174
--
-- imagemagick@7.0.8-7%gcc@7.3.0~ghostscript build_system=autotools arch=linux-centos7-broadwell/f6xpt54
--

whatis([[Name : imagemagick]])
whatis([[Version : 7.0.8-7]])
whatis([[Target : broadwell]])
whatis([[Short description : ImageMagick is a software suite to create, edit, compose, or convert bitmap images.]])
whatis([[Configure options : --without-gslib]])

help([[Name   : imagemagick]])
help([[Version: 7.0.8-7]])
help([[Target : broadwell]])
help()
help([[ImageMagick is a software suite to create, edit, compose, or convert
bitmap images.]])


depends_on("linux-centos7-x86_64/gcc/7.3.0/fontconfig/2.14.2-pnelxju")
depends_on("linux-centos7-x86_64/gcc/7.3.0/freetype/2.11.1-5stkwjm")
depends_on("linux-centos7-x86_64/gcc/7.3.0/libjpeg-turbo/2.1.4-wdiktsm")
depends_on("linux-centos7-x86_64/gcc/7.3.0/libpng/1.6.39-zr33swm")
depends_on("linux-centos7-x86_64/gcc/7.3.0/libsm/1.2.3-sfa6dzj")
depends_on("linux-centos7-x86_64/gcc/7.3.0/libtiff/4.4.0-lqfoker")
depends_on("linux-centos7-x86_64/gcc/7.3.0/libtool/2.4.7-xblvlsf")
depends_on("linux-centos7-x86_64/gcc/7.3.0/pango/1.50.13-iq2usq6")

prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/imagemagick-7.0.8-7-f6xpt54ogjmux6zc3fqdc5gy6ah32h4o/bin", ":")
prepend_path("MANPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/imagemagick-7.0.8-7-f6xpt54ogjmux6zc3fqdc5gy6ah32h4o/share/man", ":")
prepend_path("PKG_CONFIG_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/imagemagick-7.0.8-7-f6xpt54ogjmux6zc3fqdc5gy6ah32h4o/lib/pkgconfig", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/imagemagick-7.0.8-7-f6xpt54ogjmux6zc3fqdc5gy6ah32h4o/.", ":")
prepend_path("XDG_DATA_DIRS", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/gobject-introspection-1.72.1-hxu4bzn4ko7cxpwgjld2mm6yqnxwtojo/share", ":")
prepend_path("GI_TYPELIB_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/gobject-introspection-1.72.1-hxu4bzn4ko7cxpwgjld2mm6yqnxwtojo/lib/girepository-1.0", ":")
prepend_path("XDG_DATA_DIRS", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/harfbuzz-5.3.1-lb6qjp6723kuxl6kwudvipgeynmsbz3j/share", ":")
prepend_path("GI_TYPELIB_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/harfbuzz-5.3.1-lb6qjp6723kuxl6kwudvipgeynmsbz3j/lib/girepository-1.0", ":")
prepend_path("XDG_DATA_DIRS", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/pango-1.50.13-iq2usq6qsfedmeoaphnymkjbgna7rdvo/share", ":")
prepend_path("GI_TYPELIB_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/pango-1.50.13-iq2usq6qsfedmeoaphnymkjbgna7rdvo/lib/girepository-1.0", ":")

