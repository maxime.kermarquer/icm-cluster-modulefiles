-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2023-05-15 15:02:42.636287
--
-- imagemagick@7.0.8-7%gcc@7.3.0~ghostscript build_system=autotools arch=linux-centos7-broadwell/roh2duu
--

whatis([[Name : imagemagick]])
whatis([[Version : 7.0.8-7]])
whatis([[Target : broadwell]])
whatis([[Short description : ImageMagick is a software suite to create, edit, compose, or convert bitmap images.]])
whatis([[Configure options : --without-gslib]])

help([[Name   : imagemagick]])
help([[Version: 7.0.8-7]])
help([[Target : broadwell]])
help()
help([[ImageMagick is a software suite to create, edit, compose, or convert
bitmap images.]])


depends_on("linux-centos7-x86_64/gcc/7.3.0/fontconfig/2.14.2-pnelxju")
depends_on("linux-centos7-x86_64/gcc/7.3.0/freetype/2.11.1-5stkwjm")
depends_on("linux-centos7-x86_64/gcc/7.3.0/libjpeg-turbo/2.1.4-wdiktsm")
depends_on("linux-centos7-x86_64/gcc/7.3.0/libpng/1.6.39-zr33swm")
depends_on("linux-centos7-x86_64/gcc/7.3.0/libsm/1.2.3-sfa6dzj")
depends_on("linux-centos7-x86_64/gcc/7.3.0/libtiff/4.4.0-lqfoker")
depends_on("linux-centos7-x86_64/gcc/7.3.0/libtool/2.4.7-xblvlsf")
depends_on("linux-centos7-x86_64/gcc/7.3.0/pango/1.50.13-6ttjq4j")

prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/imagemagick-7.0.8-7-roh2duuqtb3ltqoedbwoyxxwmslwgh66/bin", ":")
prepend_path("MANPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/imagemagick-7.0.8-7-roh2duuqtb3ltqoedbwoyxxwmslwgh66/share/man", ":")
prepend_path("PKG_CONFIG_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/imagemagick-7.0.8-7-roh2duuqtb3ltqoedbwoyxxwmslwgh66/lib/pkgconfig", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/imagemagick-7.0.8-7-roh2duuqtb3ltqoedbwoyxxwmslwgh66/.", ":")
prepend_path("XLOCALEDIR", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/libx11-1.8.4-mygw7c45hwjfojavmnf2f3qnjb3bugq4/share/X11/locale", ":")
prepend_path("XDG_DATA_DIRS", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/gobject-introspection-1.72.1-i7b7xv5y2xgpj237dvp2otbo67ex6l6d/share", ":")
prepend_path("GI_TYPELIB_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/gobject-introspection-1.72.1-i7b7xv5y2xgpj237dvp2otbo67ex6l6d/lib/girepository-1.0", ":")
prepend_path("XDG_DATA_DIRS", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/harfbuzz-5.3.1-obk64wt5uophjhoeg753nspxanxuqet2/share", ":")
prepend_path("GI_TYPELIB_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/harfbuzz-5.3.1-obk64wt5uophjhoeg753nspxanxuqet2/lib/girepository-1.0", ":")
prepend_path("XDG_DATA_DIRS", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/pango-1.50.13-6ttjq4j7oomi6oxobumn5z65osghuduh/share", ":")
prepend_path("GI_TYPELIB_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/pango-1.50.13-6ttjq4j7oomi6oxobumn5z65osghuduh/lib/girepository-1.0", ":")

