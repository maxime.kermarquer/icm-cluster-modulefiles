-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2023-05-09 15:48:36.298670
--
-- inputproto@2.3.2%gcc@7.3.0 build_system=autotools arch=linux-centos7-broadwell/hzvihyh
--

whatis([[Name : inputproto]])
whatis([[Version : 2.3.2]])
whatis([[Target : broadwell]])
whatis([[Short description : X Input Extension.]])

help([[Name   : inputproto]])
help([[Version: 2.3.2]])
help([[Target : broadwell]])
help()
help([[X Input Extension. This extension defines a protocol to provide
additional input devices management such as graphic tablets.]])



prepend_path("PKG_CONFIG_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/inputproto-2.3.2-hzvihyhgqnvc2i4be2m5m6aqaw32k225/lib/pkgconfig", ":")
prepend_path("CPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/inputproto-2.3.2-hzvihyhgqnvc2i4be2m5m6aqaw32k225/include", ":")
prepend_path("LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/inputproto-2.3.2-hzvihyhgqnvc2i4be2m5m6aqaw32k225/lib", ":")
prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/inputproto-2.3.2-hzvihyhgqnvc2i4be2m5m6aqaw32k225/lib", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/inputproto-2.3.2-hzvihyhgqnvc2i4be2m5m6aqaw32k225/.", ":")

