-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2023-06-05 18:01:00.025417
--
-- json-c@0.16%gcc@7.3.0~ipo build_system=cmake build_type=RelWithDebInfo generator=make arch=linux-centos7-broadwell/r67yc2a
--

whatis([[Name : json-c]])
whatis([[Version : 0.16]])
whatis([[Target : broadwell]])
whatis([[Short description : A JSON implementation in C.]])

help([[Name   : json-c]])
help([[Version: 0.16]])
help([[Target : broadwell]])
help()
help([[A JSON implementation in C.]])



prepend_path("PKG_CONFIG_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/json-c-0.16-r67yc2aieesjo4gphbahzlxes7knk27f/lib64/pkgconfig", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/json-c-0.16-r67yc2aieesjo4gphbahzlxes7knk27f/.", ":")

