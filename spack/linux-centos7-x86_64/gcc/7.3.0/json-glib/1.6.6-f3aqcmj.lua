-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2023-05-09 16:47:33.015517
--
-- json-glib@1.6.6%gcc@7.3.0~strip build_system=meson buildtype=debugoptimized default_library=shared arch=linux-centos7-broadwell/f3aqcmj
--

whatis([[Name : json-glib]])
whatis([[Version : 1.6.6]])
whatis([[Target : broadwell]])
whatis([[Short description : JSON-GLib is a library for reading and parsing JSON using GLib and GObject data types and API.]])

help([[Name   : json-glib]])
help([[Version: 1.6.6]])
help([[Target : broadwell]])
help()
help([[JSON-GLib is a library for reading and parsing JSON using GLib and
GObject data types and API.]])


depends_on("linux-centos7-x86_64/gcc/7.3.0/glib/2.76.1-webav7s")
depends_on("linux-centos7-x86_64/gcc/7.3.0/gobject-introspection/1.72.1-hxu4bzn")

prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/json-glib-1.6.6-f3aqcmjqmmquslohzhxjlc3v42xb63ku/bin", ":")
prepend_path("PKG_CONFIG_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/json-glib-1.6.6-f3aqcmjqmmquslohzhxjlc3v42xb63ku/lib/pkgconfig", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/json-glib-1.6.6-f3aqcmjqmmquslohzhxjlc3v42xb63ku/.", ":")
prepend_path("XDG_DATA_DIRS", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/gobject-introspection-1.72.1-hxu4bzn4ko7cxpwgjld2mm6yqnxwtojo/share", ":")
prepend_path("GI_TYPELIB_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/gobject-introspection-1.72.1-hxu4bzn4ko7cxpwgjld2mm6yqnxwtojo/lib/girepository-1.0", ":")

