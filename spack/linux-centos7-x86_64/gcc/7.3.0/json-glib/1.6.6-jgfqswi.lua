-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2023-05-15 14:51:35.915559
--
-- json-glib@1.6.6%gcc@7.3.0~strip build_system=meson buildtype=debugoptimized default_library=shared arch=linux-centos7-broadwell/jgfqswi
--

whatis([[Name : json-glib]])
whatis([[Version : 1.6.6]])
whatis([[Target : broadwell]])
whatis([[Short description : JSON-GLib is a library for reading and parsing JSON using GLib and GObject data types and API.]])

help([[Name   : json-glib]])
help([[Version: 1.6.6]])
help([[Target : broadwell]])
help()
help([[JSON-GLib is a library for reading and parsing JSON using GLib and
GObject data types and API.]])


depends_on("linux-centos7-x86_64/gcc/7.3.0/glib/2.76.1-webav7s")
depends_on("linux-centos7-x86_64/gcc/7.3.0/gobject-introspection/1.72.1-i7b7xv5")

prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/json-glib-1.6.6-jgfqswinxps3vi6sae5mwzaoiwvsztum/bin", ":")
prepend_path("PKG_CONFIG_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/json-glib-1.6.6-jgfqswinxps3vi6sae5mwzaoiwvsztum/lib/pkgconfig", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/json-glib-1.6.6-jgfqswinxps3vi6sae5mwzaoiwvsztum/.", ":")
prepend_path("XLOCALEDIR", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/libx11-1.8.4-mygw7c45hwjfojavmnf2f3qnjb3bugq4/share/X11/locale", ":")
prepend_path("XDG_DATA_DIRS", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/gobject-introspection-1.72.1-i7b7xv5y2xgpj237dvp2otbo67ex6l6d/share", ":")
prepend_path("GI_TYPELIB_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/gobject-introspection-1.72.1-i7b7xv5y2xgpj237dvp2otbo67ex6l6d/lib/girepository-1.0", ":")

