-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2023-05-09 15:48:20.344825
--
-- kbproto@1.0.7%gcc@7.3.0 build_system=autotools arch=linux-centos7-broadwell/espyygc
--

whatis([[Name : kbproto]])
whatis([[Version : 1.0.7]])
whatis([[Target : broadwell]])
whatis([[Short description : X Keyboard Extension.]])

help([[Name   : kbproto]])
help([[Version: 1.0.7]])
help([[Target : broadwell]])
help()
help([[X Keyboard Extension. This extension defines a protcol to provide a
number of new capabilities and controls for text keyboards.]])



prepend_path("PKG_CONFIG_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/kbproto-1.0.7-espyygciivjhuzrkoozyql2shha43y4s/lib/pkgconfig", ":")
prepend_path("CPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/kbproto-1.0.7-espyygciivjhuzrkoozyql2shha43y4s/include", ":")
prepend_path("LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/kbproto-1.0.7-espyygciivjhuzrkoozyql2shha43y4s/lib", ":")
prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/kbproto-1.0.7-espyygciivjhuzrkoozyql2shha43y4s/lib", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/kbproto-1.0.7-espyygciivjhuzrkoozyql2shha43y4s/.", ":")

