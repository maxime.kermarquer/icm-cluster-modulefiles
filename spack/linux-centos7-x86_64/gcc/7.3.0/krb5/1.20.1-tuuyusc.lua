-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2023-05-04 16:28:45.005421
--
-- krb5@1.20.1%gcc@7.3.0+shared build_system=autotools arch=linux-centos7-broadwell/tuuyusc
--

whatis([[Name : krb5]])
whatis([[Version : 1.20.1]])
whatis([[Target : broadwell]])
whatis([[Short description : Network authentication protocol]])
whatis([[Configure options : --without-system-verto --disable-static]])

help([[Name   : krb5]])
help([[Version: 1.20.1]])
help([[Target : broadwell]])
help()
help([[Network authentication protocol]])


depends_on("gettext/0.21.1-nfzsems")
depends_on("openssl/1.1.1t-f5klqzu")

prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/krb5-1.20.1-tuuyuscpjisyiq43roatbza2vk4g5vqn/bin", ":")
prepend_path("MANPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/krb5-1.20.1-tuuyuscpjisyiq43roatbza2vk4g5vqn/share/man", ":")
prepend_path("PKG_CONFIG_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/krb5-1.20.1-tuuyuscpjisyiq43roatbza2vk4g5vqn/lib/pkgconfig", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/krb5-1.20.1-tuuyuscpjisyiq43roatbza2vk4g5vqn/.", ":")

