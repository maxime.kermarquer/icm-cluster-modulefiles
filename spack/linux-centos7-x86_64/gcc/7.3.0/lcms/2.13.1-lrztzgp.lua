-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2023-05-10 10:35:31.112744
--
-- lcms@2.13.1%gcc@7.3.0 build_system=autotools arch=linux-centos7-broadwell/lrztzgp
--

whatis([[Name : lcms]])
whatis([[Version : 2.13.1]])
whatis([[Target : broadwell]])
whatis([[Short description : Little cms is a color management library. Implements fast transforms between ICC profiles. It is focused on speed, and is portable across several platforms (MIT license).]])

help([[Name   : lcms]])
help([[Version: 2.13.1]])
help([[Target : broadwell]])
help()
help([[Little cms is a color management library. Implements fast transforms
between ICC profiles. It is focused on speed, and is portable across
several platforms (MIT license).]])


depends_on("libjpeg-turbo/2.1.4-wdiktsm")
depends_on("libtiff/4.4.0-lqfoker")
depends_on("zlib/1.2.13-7shounl")

prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/lcms-2.13.1-lrztzgpunobofzixbnel373qtfmr2pjr/bin", ":")
prepend_path("MANPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/lcms-2.13.1-lrztzgpunobofzixbnel373qtfmr2pjr/share/man", ":")
prepend_path("PKG_CONFIG_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/lcms-2.13.1-lrztzgpunobofzixbnel373qtfmr2pjr/lib/pkgconfig", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/lcms-2.13.1-lrztzgpunobofzixbnel373qtfmr2pjr/.", ":")

