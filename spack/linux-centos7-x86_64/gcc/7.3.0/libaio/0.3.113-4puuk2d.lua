-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2023-06-05 17:58:28.957423
--
-- libaio@0.3.113%gcc@7.3.0 build_system=makefile arch=linux-centos7-broadwell/4puuk2d
--

whatis([[Name : libaio]])
whatis([[Version : 0.3.113]])
whatis([[Target : broadwell]])
whatis([[Short description : Linux native Asynchronous I/O interface library.]])

help([[Name   : libaio]])
help([[Version: 0.3.113]])
help([[Target : broadwell]])
help()
help([[Linux native Asynchronous I/O interface library. AIO enables even a
single application thread to overlap I/O operations with other
processing, by providing an interface for submitting one or more I/O
requests in one system call (io_submit()) without waiting for
completion, and a separate interface (io_getevents()) to reap completed
I/O operations associated with a given completion group.]])



prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/libaio-0.3.113-4puuk2d7am5sboiqt5taj65bwmfaqwx7/.", ":")

