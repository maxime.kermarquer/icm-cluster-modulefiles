-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2023-05-04 15:59:10.560762
--
-- libbsd@0.11.7%gcc@7.3.0 build_system=autotools arch=linux-centos7-broadwell/ka2ajwy
--

whatis([[Name : libbsd]])
whatis([[Version : 0.11.7]])
whatis([[Target : broadwell]])
whatis([[Short description : This library provides useful functions commonly found on BSD systems, and lacking on others like GNU systems, thus making it easier to port projects with strong BSD origins, without needing to embed the same code over and over again on each project. ]])

help([[Name   : libbsd]])
help([[Version: 0.11.7]])
help([[Target : broadwell]])
help()
help([[This library provides useful functions commonly found on BSD systems,
and lacking on others like GNU systems, thus making it easier to port
projects with strong BSD origins, without needing to embed the same code
over and over again on each project.]])


depends_on("linux-centos7-x86_64/gcc/7.3.0/libmd/1.0.4-7bdb5dh")

prepend_path("MANPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/libbsd-0.11.7-ka2ajwyocu6opndsuon6epg2glh3fqnh/share/man", ":")
prepend_path("CPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/libbsd-0.11.7-ka2ajwyocu6opndsuon6epg2glh3fqnh/include", ":")
prepend_path("LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/libbsd-0.11.7-ka2ajwyocu6opndsuon6epg2glh3fqnh/lib", ":")
prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/libbsd-0.11.7-ka2ajwyocu6opndsuon6epg2glh3fqnh/lib", ":")
prepend_path("PKG_CONFIG_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/libbsd-0.11.7-ka2ajwyocu6opndsuon6epg2glh3fqnh/lib/pkgconfig", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/libbsd-0.11.7-ka2ajwyocu6opndsuon6epg2glh3fqnh/.", ":")

