-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2023-06-16 15:28:26.001739
--
-- libcap@2.68%gcc@7.3.0 build_system=makefile arch=linux-centos7-broadwell/xapytpw
--

whatis([[Name : libcap]])
whatis([[Version : 2.68]])
whatis([[Target : broadwell]])
whatis([[Short description : Libcap implements the user-space interfaces to the POSIX 1003.1e capabilities available in Linux kernels. These capabilities are a partitioning of the all powerful root privilege into a set of distinct privileges.]])

help([[Name   : libcap]])
help([[Version: 2.68]])
help([[Target : broadwell]])
help()
help([[Libcap implements the user-space interfaces to the POSIX 1003.1e
capabilities available in Linux kernels. These capabilities are a
partitioning of the all powerful root privilege into a set of distinct
privileges.]])



prepend_path("LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/libcap-2.68-xapytpwt26qwc6boc7cvaltpu33hv2x5/lib", ":")
prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/libcap-2.68-xapytpwt26qwc6boc7cvaltpu33hv2x5/lib", ":")
prepend_path("CPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/libcap-2.68-xapytpwt26qwc6boc7cvaltpu33hv2x5/include", ":")
prepend_path("MANPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/libcap-2.68-xapytpwt26qwc6boc7cvaltpu33hv2x5/share/man", ":")
prepend_path("PKG_CONFIG_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/libcap-2.68-xapytpwt26qwc6boc7cvaltpu33hv2x5/lib/pkgconfig", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/libcap-2.68-xapytpwt26qwc6boc7cvaltpu33hv2x5/.", ":")

