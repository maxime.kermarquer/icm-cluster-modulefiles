-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2023-06-13 14:27:57.676846
--
-- libcerf@1.3%gcc@7.3.0 build_system=autotools arch=linux-centos7-broadwell/dg6ur5h
--

whatis([[Name : libcerf]])
whatis([[Version : 1.3]])
whatis([[Target : broadwell]])
whatis([[Short description : A self-contained C library providing complex error functions, based on Faddeeva's plasma dispersion function w(z). Also provides Dawson's integral and Voigt's convolution of a Gaussian and a Lorentzian]])

help([[Name   : libcerf]])
help([[Version: 1.3]])
help([[Target : broadwell]])
help()
help([[A self-contained C library providing complex error functions, based on
Faddeeva's plasma dispersion function w(z). Also provides Dawson's
integral and Voigt's convolution of a Gaussian and a Lorentzian]])



prepend_path("MANPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/libcerf-1.3-dg6ur5hk4bmxv2vzxh5v7g5jxjuzsuig/share/man", ":")
prepend_path("PKG_CONFIG_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/libcerf-1.3-dg6ur5hk4bmxv2vzxh5v7g5jxjuzsuig/lib/pkgconfig", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/libcerf-1.3-dg6ur5hk4bmxv2vzxh5v7g5jxjuzsuig/.", ":")

