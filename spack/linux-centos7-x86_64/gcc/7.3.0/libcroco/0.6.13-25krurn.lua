-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2023-05-11 11:42:06.355899
--
-- libcroco@0.6.13%gcc@7.3.0~doc build_system=autotools arch=linux-centos7-broadwell/25krurn
--

whatis([[Name : libcroco]])
whatis([[Version : 0.6.13]])
whatis([[Target : broadwell]])
whatis([[Short description : Libcroco is a standalone css2 parsing and manipulation library.]])
whatis([[Configure options : --disable-gtk-doc --disable-gtk-doc-html --disable-gtk-doc-pdf]])

help([[Name   : libcroco]])
help([[Version: 0.6.13]])
help([[Target : broadwell]])
help()
help([[Libcroco is a standalone css2 parsing and manipulation library.]])


depends_on("linux-centos7-x86_64/gcc/7.3.0/glib/2.76.1-webav7s")
depends_on("linux-centos7-x86_64/gcc/7.3.0/libxml2/2.10.3-23vb7ui")

prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/libcroco-0.6.13-25krurn3oacgh6b442x6oadpdqhbjw2v/bin", ":")
prepend_path("PKG_CONFIG_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/libcroco-0.6.13-25krurn3oacgh6b442x6oadpdqhbjw2v/lib/pkgconfig", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/libcroco-0.6.13-25krurn3oacgh6b442x6oadpdqhbjw2v/.", ":")

