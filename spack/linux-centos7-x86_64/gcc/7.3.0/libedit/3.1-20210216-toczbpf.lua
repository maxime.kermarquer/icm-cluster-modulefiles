-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2023-05-04 16:01:02.674481
--
-- libedit@3.1-20210216%gcc@7.3.0 build_system=autotools arch=linux-centos7-broadwell/toczbpf
--

whatis([[Name : libedit]])
whatis([[Version : 3.1-20210216]])
whatis([[Target : broadwell]])
whatis([[Short description : An autotools compatible port of the NetBSD editline library]])
whatis([[Configure options : ac_cv_lib_curses_tgetent=no ac_cv_lib_termcap_tgetent=no ac_cv_lib_ncurses_tgetent=no]])

help([[Name   : libedit]])
help([[Version: 3.1-20210216]])
help([[Target : broadwell]])
help()
help([[An autotools compatible port of the NetBSD editline library]])


depends_on("ncurses/6.4-6n6u7qv")

prepend_path("MANPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/libedit-3.1-20210216-toczbpfyhyxjaec7htjm2aoo3mpi2o37/share/man", ":")
prepend_path("PKG_CONFIG_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/libedit-3.1-20210216-toczbpfyhyxjaec7htjm2aoo3mpi2o37/lib/pkgconfig", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/libedit-3.1-20210216-toczbpfyhyxjaec7htjm2aoo3mpi2o37/.", ":")

