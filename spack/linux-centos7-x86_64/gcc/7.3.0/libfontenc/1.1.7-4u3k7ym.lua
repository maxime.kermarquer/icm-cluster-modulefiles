-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2023-05-09 15:49:14.981084
--
-- libfontenc@1.1.7%gcc@7.3.0 build_system=autotools arch=linux-centos7-broadwell/4u3k7ym
--

whatis([[Name : libfontenc]])
whatis([[Version : 1.1.7]])
whatis([[Target : broadwell]])
whatis([[Short description : libfontenc - font encoding library.]])

help([[Name   : libfontenc]])
help([[Version: 1.1.7]])
help([[Target : broadwell]])
help()
help([[libfontenc - font encoding library.]])


depends_on("xproto/7.0.31-2iu6dcc")
depends_on("zlib/1.2.13-7shounl")

prepend_path("PKG_CONFIG_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/libfontenc-1.1.7-4u3k7ymlxaxsbo5v56juxsd26c7pow7y/lib/pkgconfig", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/libfontenc-1.1.7-4u3k7ymlxaxsbo5v56juxsd26c7pow7y/.", ":")

