-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2023-06-16 14:38:10.143375
--
-- libfuse@2.9.9%gcc@7.3.0~strip~system_install~useroot+utils build_system=meson buildtype=debugoptimized default_library=shared patches=2e265d3,6b7ea84,94d5c6d arch=linux-centos7-broadwell/4eaw3bf
--

whatis([[Name : libfuse]])
whatis([[Version : 2.9.9]])
whatis([[Target : broadwell]])
whatis([[Short description : The reference implementation of the Linux FUSE (Filesystem in Userspace) interface]])

help([[Name   : libfuse]])
help([[Version: 2.9.9]])
help([[Target : broadwell]])
help()
help([[The reference implementation of the Linux FUSE (Filesystem in Userspace)
interface]])



prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/libfuse-2.9.9-4eaw3bfmfnyopvfwnc7ehzmzalyraswx/bin", ":")
prepend_path("MANPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/libfuse-2.9.9-4eaw3bfmfnyopvfwnc7ehzmzalyraswx/share/man", ":")
prepend_path("PKG_CONFIG_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/libfuse-2.9.9-4eaw3bfmfnyopvfwnc7ehzmzalyraswx/lib/pkgconfig", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/libfuse-2.9.9-4eaw3bfmfnyopvfwnc7ehzmzalyraswx/.", ":")

