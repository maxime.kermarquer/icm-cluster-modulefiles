-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2023-06-15 15:13:41.701050
--
-- libfuse@3.10.4%gcc@7.3.0~strip~system_install~useroot+utils build_system=meson buildtype=debugoptimized default_library=shared patches=fa7a3a5 arch=linux-centos7-broadwell/4irvbgr
--

whatis([[Name : libfuse]])
whatis([[Version : 3.10.4]])
whatis([[Target : broadwell]])
whatis([[Short description : The reference implementation of the Linux FUSE (Filesystem in Userspace) interface]])

help([[Name   : libfuse]])
help([[Version: 3.10.4]])
help([[Target : broadwell]])
help()
help([[The reference implementation of the Linux FUSE (Filesystem in Userspace)
interface]])



prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/libfuse-3.10.4-4irvbgrtm3cy6dghfiddheyzcdvgdej7/bin", ":")
prepend_path("CPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/libfuse-3.10.4-4irvbgrtm3cy6dghfiddheyzcdvgdej7/include", ":")
prepend_path("LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/libfuse-3.10.4-4irvbgrtm3cy6dghfiddheyzcdvgdej7/lib", ":")
prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/libfuse-3.10.4-4irvbgrtm3cy6dghfiddheyzcdvgdej7/lib", ":")
prepend_path("MANPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/libfuse-3.10.4-4irvbgrtm3cy6dghfiddheyzcdvgdej7/share/man", ":")
prepend_path("PKG_CONFIG_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/libfuse-3.10.4-4irvbgrtm3cy6dghfiddheyzcdvgdej7/lib/pkgconfig", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/libfuse-3.10.4-4irvbgrtm3cy6dghfiddheyzcdvgdej7/.", ":")

