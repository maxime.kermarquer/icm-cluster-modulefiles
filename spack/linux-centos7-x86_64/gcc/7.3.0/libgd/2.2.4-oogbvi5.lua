-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2023-06-13 14:29:00.090915
--
-- libgd@2.2.4%gcc@7.3.0 build_system=autotools arch=linux-centos7-broadwell/oogbvi5
--

whatis([[Name : libgd]])
whatis([[Version : 2.2.4]])
whatis([[Target : broadwell]])
whatis([[Short description : GD is an open source code library for the dynamic creation of images by programmers. GD is written in C, and 'wrappers' are available for Perl, PHP and other languages. GD creates PNG, JPEG, GIF, WebP, XPM, BMP images, among other formats. GD is commonly used to generate charts, graphics, thumbnails, and most anything else, on the fly. While not restricted to use on the web, the most common applications of GD involve website development.]])

help([[Name   : libgd]])
help([[Version: 2.2.4]])
help([[Target : broadwell]])
help()
help([[GD is an open source code library for the dynamic creation of images by
programmers. GD is written in C, and "wrappers" are available for Perl,
PHP and other languages. GD creates PNG, JPEG, GIF, WebP, XPM, BMP
images, among other formats. GD is commonly used to generate charts,
graphics, thumbnails, and most anything else, on the fly. While not
restricted to use on the web, the most common applications of GD involve
website development.]])


depends_on("fontconfig/2.14.2-pnelxju")
depends_on("libiconv/1.17-afgvmbc")
depends_on("libjpeg-turbo/2.1.4-wdiktsm")
depends_on("libpng/1.6.39-zr33swm")
depends_on("libtiff/4.4.0-lqfoker")
depends_on("libx11/1.8.4-mygw7c4")

prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/libgd-2.2.4-oogbvi5tjn2x25q63jjvwdnb44nvkuos/bin", ":")
prepend_path("PKG_CONFIG_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/libgd-2.2.4-oogbvi5tjn2x25q63jjvwdnb44nvkuos/lib/pkgconfig", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/libgd-2.2.4-oogbvi5tjn2x25q63jjvwdnb44nvkuos/.", ":")
prepend_path("XLOCALEDIR", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/libx11-1.8.4-mygw7c45hwjfojavmnf2f3qnjb3bugq4/share/X11/locale", ":")

