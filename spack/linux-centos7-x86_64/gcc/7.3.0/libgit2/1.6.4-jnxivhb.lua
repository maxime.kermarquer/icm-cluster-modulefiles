-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2023-05-10 09:35:52.992110
--
-- libgit2@1.6.4%gcc@7.3.0~curl~ipo+mmap+ssh build_system=cmake build_type=RelWithDebInfo generator=make https=system arch=linux-centos7-broadwell/jnxivhb
--

whatis([[Name : libgit2]])
whatis([[Version : 1.6.4]])
whatis([[Target : broadwell]])
whatis([[Short description : libgit2 is a portable, pure C implementation of the Git core methods provided as a re-entrant linkable library with a solid API, allowing you to write native speed custom Git applications in any language which supports C bindings. ]])
whatis([[Configure options : -DUSE_HTTPS=OpenSSL -DUSE_SSH=ON -DBUILD_CLAR:BOOL=OFF]])

help([[Name   : libgit2]])
help([[Version: 1.6.4]])
help([[Target : broadwell]])
help()
help([[libgit2 is a portable, pure C implementation of the Git core methods
provided as a re-entrant linkable library with a solid API, allowing you
to write native speed custom Git applications in any language which
supports C bindings.]])


depends_on("linux-centos7-x86_64/gcc/7.3.0/libssh2/1.10.0-722ghfr")
depends_on("linux-centos7-x86_64/gcc/7.3.0/openssl/1.1.1t-f5klqzu")
depends_on("linux-centos7-x86_64/gcc/7.3.0/pcre/8.45-4inu54v")

prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/libgit2-1.6.4-jnxivhbsipjrmuijdaqd67dmg4utsmau/bin", ":")
prepend_path("LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/libgit2-1.6.4-jnxivhbsipjrmuijdaqd67dmg4utsmau/lib64", ":")
prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/libgit2-1.6.4-jnxivhbsipjrmuijdaqd67dmg4utsmau/lib64", ":")
prepend_path("CPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/libgit2-1.6.4-jnxivhbsipjrmuijdaqd67dmg4utsmau/include", ":")
prepend_path("PKG_CONFIG_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/libgit2-1.6.4-jnxivhbsipjrmuijdaqd67dmg4utsmau/lib64/pkgconfig", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/libgit2-1.6.4-jnxivhbsipjrmuijdaqd67dmg4utsmau/.", ":")

