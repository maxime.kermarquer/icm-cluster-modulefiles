-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2023-06-05 18:02:16.332467
--
-- libgpg-error@1.47%gcc@7.3.0 build_system=autotools arch=linux-centos7-broadwell/o4yooqs
--

whatis([[Name : libgpg-error]])
whatis([[Version : 1.47]])
whatis([[Target : broadwell]])
whatis([[Short description : Common error values for all GnuPG components.]])
whatis([[Configure options : --enable-static --enable-shared --disable-tests --enable-install-gpg-error-config]])

help([[Name   : libgpg-error]])
help([[Version: 1.47]])
help([[Target : broadwell]])
help()
help([[Common error values for all GnuPG components.]])



prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/libgpg-error-1.47-o4yooqse2rgvnncxtmxoeegys4qysemx/bin", ":")
prepend_path("MANPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/libgpg-error-1.47-o4yooqse2rgvnncxtmxoeegys4qysemx/share/man", ":")
prepend_path("ACLOCAL_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/libgpg-error-1.47-o4yooqse2rgvnncxtmxoeegys4qysemx/share/aclocal", ":")
prepend_path("PKG_CONFIG_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/libgpg-error-1.47-o4yooqse2rgvnncxtmxoeegys4qysemx/lib/pkgconfig", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/libgpg-error-1.47-o4yooqse2rgvnncxtmxoeegys4qysemx/.", ":")

