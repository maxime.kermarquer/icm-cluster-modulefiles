-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2023-05-10 09:30:26.697034
--
-- libice@1.0.9%gcc@7.3.0 build_system=autotools arch=linux-centos7-broadwell/eqomujj
--

whatis([[Name : libice]])
whatis([[Version : 1.0.9]])
whatis([[Target : broadwell]])
whatis([[Short description : libICE - Inter-Client Exchange Library.]])

help([[Name   : libice]])
help([[Version: 1.0.9]])
help([[Target : broadwell]])
help()
help([[libICE - Inter-Client Exchange Library.]])


depends_on("linux-centos7-x86_64/gcc/7.3.0/xproto/7.0.31-2iu6dcc")
depends_on("linux-centos7-x86_64/gcc/7.3.0/xtrans/1.4.0-g2tksfe")

prepend_path("CPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/libice-1.0.9-eqomujjauim6tcqkakkaahakzeyv6s7y/include", ":")
prepend_path("LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/libice-1.0.9-eqomujjauim6tcqkakkaahakzeyv6s7y/lib", ":")
prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/libice-1.0.9-eqomujjauim6tcqkakkaahakzeyv6s7y/lib", ":")
prepend_path("PKG_CONFIG_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/libice-1.0.9-eqomujjauim6tcqkakkaahakzeyv6s7y/lib/pkgconfig", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/libice-1.0.9-eqomujjauim6tcqkakkaahakzeyv6s7y/.", ":")

