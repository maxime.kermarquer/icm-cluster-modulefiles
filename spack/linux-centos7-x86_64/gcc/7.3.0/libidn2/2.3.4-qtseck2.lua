-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2023-05-15 14:38:41.684256
--
-- libidn2@2.3.4%gcc@7.3.0 build_system=autotools arch=linux-centos7-broadwell/qtseck2
--

whatis([[Name : libidn2]])
whatis([[Version : 2.3.4]])
whatis([[Target : broadwell]])
whatis([[Short description : Libidn2 is a free software implementation of IDNA2008, Punycode and TR46. Its purpose is to encode and decode internationalized domain names.]])

help([[Name   : libidn2]])
help([[Version: 2.3.4]])
help([[Target : broadwell]])
help()
help([[Libidn2 is a free software implementation of IDNA2008, Punycode and
TR46. Its purpose is to encode and decode internationalized domain
names.]])


depends_on("libunistring/1.1-bam5y7a")

prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/libidn2-2.3.4-qtseck2fk5fobwktssni53jjpjf64j33/bin", ":")
prepend_path("MANPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/libidn2-2.3.4-qtseck2fk5fobwktssni53jjpjf64j33/share/man", ":")
prepend_path("PKG_CONFIG_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/libidn2-2.3.4-qtseck2fk5fobwktssni53jjpjf64j33/lib/pkgconfig", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/libidn2-2.3.4-qtseck2fk5fobwktssni53jjpjf64j33/.", ":")

