-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2023-05-10 10:35:51.587800
--
-- libinih@master%gcc@7.3.0~strip build_system=meson buildtype=debugoptimized default_library=shared arch=linux-centos7-broadwell/3mb65o2
--

whatis([[Name : libinih]])
whatis([[Version : master]])
whatis([[Target : broadwell]])
whatis([[Short description :  inih (INI Not Invented Here) is a simple .INI file parser written in C. ]])

help([[Name   : libinih]])
help([[Version: master]])
help([[Target : broadwell]])
help()
help([[ inih (INI Not Invented Here) is a simple .INI file parser written in C.]])



prepend_path("PKG_CONFIG_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/libinih-master-3mb65o2wlni26bcygb6egvgumv3efvkx/lib/pkgconfig", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/libinih-master-3mb65o2wlni26bcygb6egvgumv3efvkx/.", ":")

