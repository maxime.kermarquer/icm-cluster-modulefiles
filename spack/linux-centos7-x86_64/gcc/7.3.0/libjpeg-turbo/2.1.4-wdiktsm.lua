-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2023-05-10 09:31:26.309516
--
-- libjpeg-turbo@2.1.4%gcc@7.3.0~ipo~jpeg8+shared+static build_system=cmake build_type=RelWithDebInfo generator=make arch=linux-centos7-broadwell/wdiktsm
--

whatis([[Name : libjpeg-turbo]])
whatis([[Version : 2.1.4]])
whatis([[Target : broadwell]])
whatis([[Short description : libjpeg-turbo is a fork of the original IJG libjpeg which uses SIMD to accelerate baseline JPEG compression and decompression.]])

help([[Name   : libjpeg-turbo]])
help([[Version: 2.1.4]])
help([[Target : broadwell]])
help()
help([[libjpeg-turbo is a fork of the original IJG libjpeg which uses SIMD to
accelerate baseline JPEG compression and decompression. libjpeg is a
library that implements JPEG image encoding, decoding and transcoding.]])



prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/libjpeg-turbo-2.1.4-wdiktsmeinkabmm44szwrzyarmybs2f2/bin", ":")
prepend_path("CPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/libjpeg-turbo-2.1.4-wdiktsmeinkabmm44szwrzyarmybs2f2/include", ":")
prepend_path("LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/libjpeg-turbo-2.1.4-wdiktsmeinkabmm44szwrzyarmybs2f2/lib64", ":")
prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/libjpeg-turbo-2.1.4-wdiktsmeinkabmm44szwrzyarmybs2f2/lib64", ":")
prepend_path("MANPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/libjpeg-turbo-2.1.4-wdiktsmeinkabmm44szwrzyarmybs2f2/share/man", ":")
prepend_path("PKG_CONFIG_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/libjpeg-turbo-2.1.4-wdiktsmeinkabmm44szwrzyarmybs2f2/lib64/pkgconfig", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/libjpeg-turbo-2.1.4-wdiktsmeinkabmm44szwrzyarmybs2f2/.", ":")

