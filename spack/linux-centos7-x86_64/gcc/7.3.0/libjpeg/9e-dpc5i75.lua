-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2023-05-23 09:55:28.536477
--
-- libjpeg@9e%gcc@7.3.0 build_system=autotools arch=linux-centos7-broadwell/dpc5i75
--

whatis([[Name : libjpeg]])
whatis([[Version : 9e]])
whatis([[Target : broadwell]])
whatis([[Short description : libjpeg is a widely used free library with functions for handling the JPEG image data format. It implements a JPEG codec (encoding and decoding) alongside various utilities for handling JPEG data.]])

help([[Name   : libjpeg]])
help([[Version: 9e]])
help([[Target : broadwell]])
help()
help([[libjpeg is a widely used free library with functions for handling the
JPEG image data format. It implements a JPEG codec (encoding and
decoding) alongside various utilities for handling JPEG data.]])



prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/libjpeg-9e-dpc5i75luuujwoksajvwy32pxdwxaypk/bin", ":")
prepend_path("CPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/libjpeg-9e-dpc5i75luuujwoksajvwy32pxdwxaypk/include", ":")
prepend_path("LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/libjpeg-9e-dpc5i75luuujwoksajvwy32pxdwxaypk/lib", ":")
prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/libjpeg-9e-dpc5i75luuujwoksajvwy32pxdwxaypk/lib", ":")
prepend_path("MANPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/libjpeg-9e-dpc5i75luuujwoksajvwy32pxdwxaypk/share/man", ":")
prepend_path("PKG_CONFIG_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/libjpeg-9e-dpc5i75luuujwoksajvwy32pxdwxaypk/lib/pkgconfig", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/libjpeg-9e-dpc5i75luuujwoksajvwy32pxdwxaypk/.", ":")

