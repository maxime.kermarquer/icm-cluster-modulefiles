-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2023-05-04 15:45:54.403310
--
-- libmd@1.0.4%gcc@7.3.0 build_system=autotools arch=linux-centos7-broadwell/7bdb5dh
--

whatis([[Name : libmd]])
whatis([[Version : 1.0.4]])
whatis([[Target : broadwell]])
whatis([[Short description : This library provides message digest functions found on BSD systems either on their libc (NetBSD, OpenBSD) or libmd (FreeBSD, DragonflyBSD, macOS, Solaris) libraries and lacking on others like GNU systems.]])

help([[Name   : libmd]])
help([[Version: 1.0.4]])
help([[Target : broadwell]])
help()
help([[This library provides message digest functions found on BSD systems
either on their libc (NetBSD, OpenBSD) or libmd (FreeBSD, DragonflyBSD,
macOS, Solaris) libraries and lacking on others like GNU systems.]])



prepend_path("MANPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/libmd-1.0.4-7bdb5dheajopwc2lbao7vcd34wa7pkch/share/man", ":")
prepend_path("CPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/libmd-1.0.4-7bdb5dheajopwc2lbao7vcd34wa7pkch/include", ":")
prepend_path("LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/libmd-1.0.4-7bdb5dheajopwc2lbao7vcd34wa7pkch/lib", ":")
prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/libmd-1.0.4-7bdb5dheajopwc2lbao7vcd34wa7pkch/lib", ":")
prepend_path("PKG_CONFIG_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/libmd-1.0.4-7bdb5dheajopwc2lbao7vcd34wa7pkch/lib/pkgconfig", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/libmd-1.0.4-7bdb5dheajopwc2lbao7vcd34wa7pkch/.", ":")

