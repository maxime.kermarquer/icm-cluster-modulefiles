-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2023-05-10 10:37:27.542707
--
-- libpaper@1.1.28%gcc@7.3.0 build_system=autotools arch=linux-centos7-broadwell/shpp232
--

whatis([[Name : libpaper]])
whatis([[Version : 1.1.28]])
whatis([[Target : broadwell]])
whatis([[Short description : The paper library and accompanying files are intended to provide a simple way for applications to take actions based on a system- or user-specified paper size.]])

help([[Name   : libpaper]])
help([[Version: 1.1.28]])
help([[Target : broadwell]])
help()
help([[The paper library and accompanying files are intended to provide a
simple way for applications to take actions based on a system- or user-
specified paper size.]])



prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/libpaper-1.1.28-shpp2323mdbq5p7gc7clgsovdpck22po/bin", ":")
prepend_path("MANPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/libpaper-1.1.28-shpp2323mdbq5p7gc7clgsovdpck22po/share/man", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/libpaper-1.1.28-shpp2323mdbq5p7gc7clgsovdpck22po/.", ":")

