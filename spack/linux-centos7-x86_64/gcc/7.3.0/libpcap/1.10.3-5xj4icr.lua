-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2023-06-16 10:48:01.946083
--
-- libpcap@1.10.3%gcc@7.3.0 build_system=autotools arch=linux-centos7-broadwell/5xj4icr
--

whatis([[Name : libpcap]])
whatis([[Version : 1.10.3]])
whatis([[Target : broadwell]])
whatis([[Short description : libpcap is a portable library in C/C++ for packet capture.]])

help([[Name   : libpcap]])
help([[Version: 1.10.3]])
help([[Target : broadwell]])
help()
help([[libpcap is a portable library in C/C++ for packet capture.]])



prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/libpcap-1.10.3-5xj4icr3olhkr75dymucxnckg27mop6g/bin", ":")
prepend_path("MANPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/libpcap-1.10.3-5xj4icr3olhkr75dymucxnckg27mop6g/share/man", ":")
prepend_path("PKG_CONFIG_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/libpcap-1.10.3-5xj4icr3olhkr75dymucxnckg27mop6g/lib/pkgconfig", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/libpcap-1.10.3-5xj4icr3olhkr75dymucxnckg27mop6g/.", ":")

