-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2023-05-09 14:18:01.012809
--
-- libpng@1.6.37%gcc@7.3.0~ipo build_system=cmake build_type=RelWithDebInfo generator=make libs=shared,static arch=linux-centos7-broadwell/bydly5y
--

whatis([[Name : libpng]])
whatis([[Version : 1.6.37]])
whatis([[Target : broadwell]])
whatis([[Short description : libpng is the official PNG reference library.]])

help([[Name   : libpng]])
help([[Version: 1.6.37]])
help([[Target : broadwell]])
help()
help([[libpng is the official PNG reference library.]])


depends_on("zlib/1.2.13-7shounl")

prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/libpng-1.6.37-bydly5y2aelq34kyhdgl5mhzss4dgjlx/bin", ":")
prepend_path("MANPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/libpng-1.6.37-bydly5y2aelq34kyhdgl5mhzss4dgjlx/share/man", ":")
prepend_path("PKG_CONFIG_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/libpng-1.6.37-bydly5y2aelq34kyhdgl5mhzss4dgjlx/lib64/pkgconfig", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/libpng-1.6.37-bydly5y2aelq34kyhdgl5mhzss4dgjlx/.", ":")

