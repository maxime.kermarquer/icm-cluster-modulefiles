-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2023-05-09 14:42:08.606132
--
-- libpng@1.6.39%gcc@7.3.0~ipo build_system=cmake build_type=RelWithDebInfo generator=make libs=shared,static arch=linux-centos7-broadwell/zr33swm
--

whatis([[Name : libpng]])
whatis([[Version : 1.6.39]])
whatis([[Target : broadwell]])
whatis([[Short description : libpng is the official PNG reference library.]])

help([[Name   : libpng]])
help([[Version: 1.6.39]])
help([[Target : broadwell]])
help()
help([[libpng is the official PNG reference library.]])


depends_on("linux-centos7-x86_64/gcc/7.3.0/zlib/1.2.13-7shounl")

prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/libpng-1.6.39-zr33swmh6iajj7vfpkwcyxi54y5rv22f/bin", ":")
prepend_path("CPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/libpng-1.6.39-zr33swmh6iajj7vfpkwcyxi54y5rv22f/include", ":")
prepend_path("LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/libpng-1.6.39-zr33swmh6iajj7vfpkwcyxi54y5rv22f/lib64", ":")
prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/libpng-1.6.39-zr33swmh6iajj7vfpkwcyxi54y5rv22f/lib64", ":")
prepend_path("MANPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/libpng-1.6.39-zr33swmh6iajj7vfpkwcyxi54y5rv22f/share/man", ":")
prepend_path("PKG_CONFIG_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/libpng-1.6.39-zr33swmh6iajj7vfpkwcyxi54y5rv22f/lib64/pkgconfig", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/libpng-1.6.39-zr33swmh6iajj7vfpkwcyxi54y5rv22f/.", ":")

