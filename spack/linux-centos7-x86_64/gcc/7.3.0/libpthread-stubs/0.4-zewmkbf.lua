-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2023-05-09 15:47:10.991892
--
-- libpthread-stubs@0.4%gcc@7.3.0 build_system=autotools arch=linux-centos7-broadwell/zewmkbf
--

whatis([[Name : libpthread-stubs]])
whatis([[Version : 0.4]])
whatis([[Target : broadwell]])
whatis([[Short description : The libpthread-stubs package provides weak aliases for pthread functions not provided in libc or otherwise available by default.]])

help([[Name   : libpthread-stubs]])
help([[Version: 0.4]])
help([[Target : broadwell]])
help()
help([[The libpthread-stubs package provides weak aliases for pthread functions
not provided in libc or otherwise available by default.]])



prepend_path("PKG_CONFIG_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/libpthread-stubs-0.4-zewmkbfczkcdhmm5c3awyyjkfjn5ebst/lib/pkgconfig", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/libpthread-stubs-0.4-zewmkbfczkcdhmm5c3awyyjkfjn5ebst/.", ":")

