-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2023-05-19 16:40:36.784405
--
-- librsvg@2.50.0%gcc@7.3.0~doc build_system=autotools arch=linux-centos7-broadwell/hghbf6d
--

whatis([[Name : librsvg]])
whatis([[Version : 2.50.0]])
whatis([[Target : broadwell]])
whatis([[Short description : Library to render SVG files using Cairo]])
whatis([[Configure options : --disable-gtk-doc GTKDOC_MKPDF=/bin/true GTKDOC_REBASE=/bin/true GTKDOC_CHECK_PATH=/bin/true]])

help([[Name   : librsvg]])
help([[Version: 2.50.0]])
help([[Target : broadwell]])
help()
help([[Library to render SVG files using Cairo]])


depends_on("linux-centos7-x86_64/gcc/7.3.0/cairo/1.16.0-g7r3f6p")
depends_on("linux-centos7-x86_64/gcc/7.3.0/gdk-pixbuf/2.42.10-o4l3nje")
depends_on("linux-centos7-x86_64/gcc/7.3.0/glib/2.76.1-webav7s")
depends_on("linux-centos7-x86_64/gcc/7.3.0/libcroco/0.6.13-25krurn")
depends_on("linux-centos7-x86_64/gcc/7.3.0/libffi/3.3-bbd3for")
depends_on("linux-centos7-x86_64/gcc/7.3.0/libxml2/2.10.3-23vb7ui")
depends_on("linux-centos7-x86_64/gcc/7.3.0/pango/1.50.13-iq2usq6")
depends_on("linux-centos7-x86_64/gcc/7.3.0/shared-mime-info/1.9-6etij5m")

prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/librsvg-2.50.0-hghbf6dh2l2sozq6xzzk5pu7c3dcqjxx/bin", ":")
prepend_path("CPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/librsvg-2.50.0-hghbf6dh2l2sozq6xzzk5pu7c3dcqjxx/include", ":")
prepend_path("LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/librsvg-2.50.0-hghbf6dh2l2sozq6xzzk5pu7c3dcqjxx/lib", ":")
prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/librsvg-2.50.0-hghbf6dh2l2sozq6xzzk5pu7c3dcqjxx/lib", ":")
prepend_path("MANPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/librsvg-2.50.0-hghbf6dh2l2sozq6xzzk5pu7c3dcqjxx/share/man", ":")
prepend_path("PKG_CONFIG_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/librsvg-2.50.0-hghbf6dh2l2sozq6xzzk5pu7c3dcqjxx/lib/pkgconfig", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/librsvg-2.50.0-hghbf6dh2l2sozq6xzzk5pu7c3dcqjxx/.", ":")
prepend_path("XDG_DATA_DIRS", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/gobject-introspection-1.72.1-hxu4bzn4ko7cxpwgjld2mm6yqnxwtojo/share", ":")
prepend_path("GI_TYPELIB_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/gobject-introspection-1.72.1-hxu4bzn4ko7cxpwgjld2mm6yqnxwtojo/lib/girepository-1.0", ":")
prepend_path("XDG_DATA_DIRS", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/shared-mime-info-1.9-6etij5mjgjpq4x6qnmac2uplbftk62nd/share", ":")
prepend_path("XDG_DATA_DIRS", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/gdk-pixbuf-2.42.10-o4l3njentoxlwkbwcqrsssa4gueih5nv/share", ":")
prepend_path("GI_TYPELIB_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/gdk-pixbuf-2.42.10-o4l3njentoxlwkbwcqrsssa4gueih5nv/lib/girepository-1.0", ":")
prepend_path("XDG_DATA_DIRS", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/harfbuzz-5.3.1-lb6qjp6723kuxl6kwudvipgeynmsbz3j/share", ":")
prepend_path("GI_TYPELIB_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/harfbuzz-5.3.1-lb6qjp6723kuxl6kwudvipgeynmsbz3j/lib/girepository-1.0", ":")
prepend_path("XDG_DATA_DIRS", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/pango-1.50.13-iq2usq6qsfedmeoaphnymkjbgna7rdvo/share", ":")
prepend_path("GI_TYPELIB_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/pango-1.50.13-iq2usq6qsfedmeoaphnymkjbgna7rdvo/lib/girepository-1.0", ":")
prepend_path("XDG_DATA_DIRS", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/librsvg-2.50.0-hghbf6dh2l2sozq6xzzk5pu7c3dcqjxx/share", ":")

