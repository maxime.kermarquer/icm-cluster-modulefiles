-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2023-06-05 18:08:04.090989
--
-- libseccomp@2.5.4%gcc@7.3.0+python build_system=autotools arch=linux-centos7-broadwell/jajgsv5
--

whatis([[Name : libseccomp]])
whatis([[Version : 2.5.4]])
whatis([[Target : broadwell]])
whatis([[Short description : The main libseccomp repository]])
whatis([[Configure options : --enable-python]])

help([[Name   : libseccomp]])
help([[Version: 2.5.4]])
help([[Target : broadwell]])
help()
help([[The main libseccomp repository]])



prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/libseccomp-2.5.4-jajgsv5knmyyagvzblx3xy63ijieita5/bin", ":")
prepend_path("CPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/libseccomp-2.5.4-jajgsv5knmyyagvzblx3xy63ijieita5/include", ":")
prepend_path("LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/libseccomp-2.5.4-jajgsv5knmyyagvzblx3xy63ijieita5/lib", ":")
prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/libseccomp-2.5.4-jajgsv5knmyyagvzblx3xy63ijieita5/lib", ":")
prepend_path("MANPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/libseccomp-2.5.4-jajgsv5knmyyagvzblx3xy63ijieita5/share/man", ":")
prepend_path("PKG_CONFIG_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/libseccomp-2.5.4-jajgsv5knmyyagvzblx3xy63ijieita5/lib/pkgconfig", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/libseccomp-2.5.4-jajgsv5knmyyagvzblx3xy63ijieita5/.", ":")

