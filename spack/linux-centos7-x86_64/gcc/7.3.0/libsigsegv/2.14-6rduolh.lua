-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2023-05-04 15:41:27.132567
--
-- libsigsegv@2.14%gcc@7.3.0 build_system=autotools arch=linux-centos7-broadwell/6rduolh
--

whatis([[Name : libsigsegv]])
whatis([[Version : 2.14]])
whatis([[Target : broadwell]])
whatis([[Short description : GNU libsigsegv is a library for handling page faults in user mode.]])
whatis([[Configure options : --enable-shared]])

help([[Name   : libsigsegv]])
help([[Version: 2.14]])
help([[Target : broadwell]])
help()
help([[GNU libsigsegv is a library for handling page faults in user mode.]])



prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/libsigsegv-2.14-6rduolhl7kuerjs652lcesv37skt7mly/.", ":")

