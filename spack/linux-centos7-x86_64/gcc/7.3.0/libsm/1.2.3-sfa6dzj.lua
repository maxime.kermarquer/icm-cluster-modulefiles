-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2023-05-10 09:32:05.190398
--
-- libsm@1.2.3%gcc@7.3.0 build_system=autotools arch=linux-centos7-broadwell/sfa6dzj
--

whatis([[Name : libsm]])
whatis([[Version : 1.2.3]])
whatis([[Target : broadwell]])
whatis([[Short description : libSM - X Session Management Library.]])

help([[Name   : libsm]])
help([[Version: 1.2.3]])
help([[Target : broadwell]])
help()
help([[libSM - X Session Management Library.]])


depends_on("linux-centos7-x86_64/gcc/7.3.0/libice/1.0.9-eqomujj")
depends_on("linux-centos7-x86_64/gcc/7.3.0/util-linux-uuid/2.38.1-2oluzsj")
depends_on("linux-centos7-x86_64/gcc/7.3.0/xproto/7.0.31-2iu6dcc")
depends_on("linux-centos7-x86_64/gcc/7.3.0/xtrans/1.4.0-g2tksfe")

prepend_path("CPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/libsm-1.2.3-sfa6dzjsv5wpu4ajpyyxswnjgvjeomyo/include", ":")
prepend_path("LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/libsm-1.2.3-sfa6dzjsv5wpu4ajpyyxswnjgvjeomyo/lib", ":")
prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/libsm-1.2.3-sfa6dzjsv5wpu4ajpyyxswnjgvjeomyo/lib", ":")
prepend_path("PKG_CONFIG_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/libsm-1.2.3-sfa6dzjsv5wpu4ajpyyxswnjgvjeomyo/lib/pkgconfig", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/libsm-1.2.3-sfa6dzjsv5wpu4ajpyyxswnjgvjeomyo/.", ":")

