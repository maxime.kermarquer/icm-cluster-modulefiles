-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2023-05-10 09:30:45.019069
--
-- libssh2@1.10.0%gcc@7.3.0~ipo+shared build_system=cmake build_type=RelWithDebInfo crypto=openssl generator=make arch=linux-centos7-broadwell/722ghfr
--

whatis([[Name : libssh2]])
whatis([[Version : 1.10.0]])
whatis([[Target : broadwell]])
whatis([[Short description : libssh2 is a client-side C library implementing the SSH2 protocol]])
whatis([[Configure options : -DBUILD_TESTING:STRING=OFF -DBUILD_SHARED_LIBS:BOOL=ON -DCRYPTO_BACKEND:STRING=OpenSSL]])

help([[Name   : libssh2]])
help([[Version: 1.10.0]])
help([[Target : broadwell]])
help()
help([[libssh2 is a client-side C library implementing the SSH2 protocol]])


depends_on("linux-centos7-x86_64/gcc/7.3.0/openssl/1.1.1t-f5klqzu")
depends_on("linux-centos7-x86_64/gcc/7.3.0/xz/5.4.1-pthvj6a")
depends_on("linux-centos7-x86_64/gcc/7.3.0/zlib/1.2.13-7shounl")

prepend_path("MANPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/libssh2-1.10.0-722ghfrkwpaew6glolwg27sd3qhxi5jk/share/man", ":")
prepend_path("PKG_CONFIG_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/libssh2-1.10.0-722ghfrkwpaew6glolwg27sd3qhxi5jk/lib64/pkgconfig", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/libssh2-1.10.0-722ghfrkwpaew6glolwg27sd3qhxi5jk/.", ":")

