-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2023-05-10 09:36:19.064186
--
-- libtiff@4.4.0%gcc@7.3.0+ccitt~ipo~jbig+jpeg~jpeg12~lerc~libdeflate+logluv~lzma+lzw+next~old-jpeg+packbits~pixarlog+thunder~webp+zlib~zstd build_system=cmake build_type=RelWithDebInfo generator=make arch=linux-centos7-broadwell/lqfoker
--

whatis([[Name : libtiff]])
whatis([[Version : 4.4.0]])
whatis([[Target : broadwell]])
whatis([[Short description : LibTIFF - Tag Image File Format (TIFF) Library and Utilities.]])

help([[Name   : libtiff]])
help([[Version: 4.4.0]])
help([[Target : broadwell]])
help()
help([[LibTIFF - Tag Image File Format (TIFF) Library and Utilities.]])


depends_on("linux-centos7-x86_64/gcc/7.3.0/libjpeg-turbo/2.1.4-wdiktsm")
depends_on("linux-centos7-x86_64/gcc/7.3.0/zlib/1.2.13-7shounl")

prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/libtiff-4.4.0-lqfokersa5lmayy74edesjd5k54qxmvt/bin", ":")
prepend_path("CPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/libtiff-4.4.0-lqfokersa5lmayy74edesjd5k54qxmvt/include", ":")
prepend_path("LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/libtiff-4.4.0-lqfokersa5lmayy74edesjd5k54qxmvt/lib64", ":")
prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/libtiff-4.4.0-lqfokersa5lmayy74edesjd5k54qxmvt/lib64", ":")
prepend_path("MANPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/libtiff-4.4.0-lqfokersa5lmayy74edesjd5k54qxmvt/share/man", ":")
prepend_path("PKG_CONFIG_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/libtiff-4.4.0-lqfokersa5lmayy74edesjd5k54qxmvt/lib64/pkgconfig", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/libtiff-4.4.0-lqfokersa5lmayy74edesjd5k54qxmvt/.", ":")

