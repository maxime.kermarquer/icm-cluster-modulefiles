-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2023-05-04 16:29:40.512086
--
-- libtirpc@1.2.6%gcc@7.3.0 build_system=autotools arch=linux-centos7-broadwell/f7isgrk
--

whatis([[Name : libtirpc]])
whatis([[Version : 1.2.6]])
whatis([[Target : broadwell]])
whatis([[Short description : Libtirpc is a port of Suns Transport-Independent RPC library to Linux.]])

help([[Name   : libtirpc]])
help([[Version: 1.2.6]])
help([[Target : broadwell]])
help()
help([[Libtirpc is a port of Suns Transport-Independent RPC library to Linux.]])


depends_on("krb5/1.20.1-tuuyusc")

prepend_path("MANPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/libtirpc-1.2.6-f7isgrk2ynjjbh5667o2pils7icqy2fu/share/man", ":")
prepend_path("PKG_CONFIG_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/libtirpc-1.2.6-f7isgrk2ynjjbh5667o2pils7icqy2fu/lib/pkgconfig", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/libtirpc-1.2.6-f7isgrk2ynjjbh5667o2pils7icqy2fu/.", ":")

