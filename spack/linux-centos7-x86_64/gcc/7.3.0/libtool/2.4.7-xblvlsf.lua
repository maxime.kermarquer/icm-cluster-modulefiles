-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2023-05-09 15:12:00.248984
--
-- libtool@2.4.7%gcc@7.3.0 build_system=autotools arch=linux-centos7-broadwell/xblvlsf
--

whatis([[Name : libtool]])
whatis([[Version : 2.4.7]])
whatis([[Target : broadwell]])
whatis([[Short description : libtool -- library building part of autotools.]])

help([[Name   : libtool]])
help([[Version: 2.4.7]])
help([[Target : broadwell]])
help()
help([[libtool -- library building part of autotools.]])



prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/libtool-2.4.7-xblvlsfctlmjxywxqmts3xl4ngjrusif/bin", ":")
prepend_path("CPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/libtool-2.4.7-xblvlsfctlmjxywxqmts3xl4ngjrusif/include", ":")
prepend_path("LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/libtool-2.4.7-xblvlsfctlmjxywxqmts3xl4ngjrusif/lib", ":")
prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/libtool-2.4.7-xblvlsfctlmjxywxqmts3xl4ngjrusif/lib", ":")
prepend_path("MANPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/libtool-2.4.7-xblvlsfctlmjxywxqmts3xl4ngjrusif/share/man", ":")
prepend_path("ACLOCAL_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/libtool-2.4.7-xblvlsfctlmjxywxqmts3xl4ngjrusif/share/aclocal", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/libtool-2.4.7-xblvlsfctlmjxywxqmts3xl4ngjrusif/.", ":")

