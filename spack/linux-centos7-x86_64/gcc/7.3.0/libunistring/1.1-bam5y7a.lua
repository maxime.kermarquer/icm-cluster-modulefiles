-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2023-05-15 14:37:35.536499
--
-- libunistring@1.1%gcc@7.3.0 build_system=autotools arch=linux-centos7-broadwell/bam5y7a
--

whatis([[Name : libunistring]])
whatis([[Version : 1.1]])
whatis([[Target : broadwell]])
whatis([[Short description : This library provides functions for manipulating Unicode strings and for manipulating C strings according to the Unicode standard.]])

help([[Name   : libunistring]])
help([[Version: 1.1]])
help([[Target : broadwell]])
help()
help([[This library provides functions for manipulating Unicode strings and for
manipulating C strings according to the Unicode standard.]])


depends_on("libiconv/1.17-afgvmbc")

prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/libunistring-1.1-bam5y7ayqybec5wx7ubzbrrqn4sdcomi/.", ":")

