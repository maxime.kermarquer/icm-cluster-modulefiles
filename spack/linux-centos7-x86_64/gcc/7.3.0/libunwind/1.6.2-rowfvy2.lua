-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2023-05-10 09:24:21.000528
--
-- libunwind@1.6.2%gcc@7.3.0~block_signals~conservative_checks~cxx_exceptions~debug~debug_frame+docs~pic+tests+weak_backtrace~xz~zlib build_system=autotools components=none libs=shared,static arch=linux-centos7-broadwell/rowfvy2
--

whatis([[Name : libunwind]])
whatis([[Version : 1.6.2]])
whatis([[Target : broadwell]])
whatis([[Short description : A portable and efficient C programming interface (API) to determine the call-chain of a program.]])
whatis([[Configure options : --enable-documentation --enable-shared --enable-static --enable-tests --disable-block-signals --disable-ptrace --disable-coredump --disable-setjump --disable-conservative-checks --disable-cxx-exceptions --disable-debug --disable-debug-frame --disable-minidebuginfo --enable-weak-backtrace --disable-zlibdebuginfo]])

help([[Name   : libunwind]])
help([[Version: 1.6.2]])
help([[Target : broadwell]])
help()
help([[A portable and efficient C programming interface (API) to determine the
call-chain of a program.]])



prepend_path("MANPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/libunwind-1.6.2-rowfvy2ycbncin77xbfwaa4tiovwo4ga/share/man", ":")
prepend_path("PKG_CONFIG_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/libunwind-1.6.2-rowfvy2ycbncin77xbfwaa4tiovwo4ga/lib/pkgconfig", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/libunwind-1.6.2-rowfvy2ycbncin77xbfwaa4tiovwo4ga/.", ":")

