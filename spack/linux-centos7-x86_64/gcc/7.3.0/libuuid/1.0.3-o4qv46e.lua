-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2023-06-14 10:04:58.951806
--
-- libuuid@1.0.3%gcc@7.3.0 build_system=autotools arch=linux-centos7-broadwell/o4qv46e
--

whatis([[Name : libuuid]])
whatis([[Version : 1.0.3]])
whatis([[Target : broadwell]])
whatis([[Short description : Portable uuid C library]])

help([[Name   : libuuid]])
help([[Version: 1.0.3]])
help([[Target : broadwell]])
help()
help([[Portable uuid C library]])



prepend_path("PKG_CONFIG_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/libuuid-1.0.3-o4qv46e6gfue6jme3ohaordvtzarsv5f/lib/pkgconfig", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/libuuid-1.0.3-o4qv46e6gfue6jme3ohaordvtzarsv5f/.", ":")

