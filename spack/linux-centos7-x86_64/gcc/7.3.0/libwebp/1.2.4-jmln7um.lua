-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2023-05-10 16:12:04.754995
--
-- libwebp@1.2.4%gcc@7.3.0~gif~jpeg~libwebpdecoder~libwebpdemux~libwebpextras~libwebpmux~png~tiff build_system=autotools arch=linux-centos7-broadwell/jmln7um
--

whatis([[Name : libwebp]])
whatis([[Version : 1.2.4]])
whatis([[Target : broadwell]])
whatis([[Short description : WebP is a modern image format that provides superior lossless and lossy compression for images on the web. Using WebP, webmasters and web developers can create smaller, richer images that make the web faster.]])
whatis([[Configure options : --disable-gl --disable-sdl --disable-wic --disable-gif --disable-jpeg --disable-png --disable-tiff --disable-libwebpmux --disable-libwebpdemux --disable-libwebpdecoder --disable-libwebpextras]])

help([[Name   : libwebp]])
help([[Version: 1.2.4]])
help([[Target : broadwell]])
help()
help([[WebP is a modern image format that provides superior lossless and lossy
compression for images on the web. Using WebP, webmasters and web
developers can create smaller, richer images that make the web faster.]])



prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/libwebp-1.2.4-jmln7umck64lynu5eokk3zziqqu63d6e/bin", ":")
prepend_path("CPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/libwebp-1.2.4-jmln7umck64lynu5eokk3zziqqu63d6e/include", ":")
prepend_path("LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/libwebp-1.2.4-jmln7umck64lynu5eokk3zziqqu63d6e/lib", ":")
prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/libwebp-1.2.4-jmln7umck64lynu5eokk3zziqqu63d6e/lib", ":")
prepend_path("MANPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/libwebp-1.2.4-jmln7umck64lynu5eokk3zziqqu63d6e/share/man", ":")
prepend_path("PKG_CONFIG_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/libwebp-1.2.4-jmln7umck64lynu5eokk3zziqqu63d6e/lib/pkgconfig", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/libwebp-1.2.4-jmln7umck64lynu5eokk3zziqqu63d6e/.", ":")

