-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2023-05-09 15:55:24.005140
--
-- libx11@1.8.4%gcc@7.3.0 build_system=autotools arch=linux-centos7-broadwell/mygw7c4
--

whatis([[Name : libx11]])
whatis([[Version : 1.8.4]])
whatis([[Target : broadwell]])
whatis([[Short description : libX11 - Core X11 protocol client library.]])

help([[Name   : libx11]])
help([[Version: 1.8.4]])
help([[Target : broadwell]])
help()
help([[libX11 - Core X11 protocol client library.]])


depends_on("linux-centos7-x86_64/gcc/7.3.0/inputproto/2.3.2-hzvihyh")
depends_on("linux-centos7-x86_64/gcc/7.3.0/kbproto/1.0.7-espyygc")
depends_on("linux-centos7-x86_64/gcc/7.3.0/libxcb/1.14-34nijb6")
depends_on("linux-centos7-x86_64/gcc/7.3.0/xextproto/7.3.0-jmtrxor")
depends_on("linux-centos7-x86_64/gcc/7.3.0/xproto/7.0.31-2iu6dcc")
depends_on("linux-centos7-x86_64/gcc/7.3.0/xtrans/1.4.0-g2tksfe")

prepend_path("CPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/libx11-1.8.4-mygw7c45hwjfojavmnf2f3qnjb3bugq4/include", ":")
prepend_path("LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/libx11-1.8.4-mygw7c45hwjfojavmnf2f3qnjb3bugq4/lib", ":")
prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/libx11-1.8.4-mygw7c45hwjfojavmnf2f3qnjb3bugq4/lib", ":")
prepend_path("MANPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/libx11-1.8.4-mygw7c45hwjfojavmnf2f3qnjb3bugq4/share/man", ":")
prepend_path("PKG_CONFIG_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/libx11-1.8.4-mygw7c45hwjfojavmnf2f3qnjb3bugq4/lib/pkgconfig", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/libx11-1.8.4-mygw7c45hwjfojavmnf2f3qnjb3bugq4/.", ":")

