-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2023-05-09 15:49:01.001532
--
-- libxau@1.0.8%gcc@7.3.0 build_system=autotools arch=linux-centos7-broadwell/iwyawfw
--

whatis([[Name : libxau]])
whatis([[Version : 1.0.8]])
whatis([[Target : broadwell]])
whatis([[Short description : The libXau package contains a library implementing the X11 Authorization Protocol. This is useful for restricting client access to the display.]])

help([[Name   : libxau]])
help([[Version: 1.0.8]])
help([[Target : broadwell]])
help()
help([[The libXau package contains a library implementing the X11 Authorization
Protocol. This is useful for restricting client access to the display.]])


depends_on("linux-centos7-x86_64/gcc/7.3.0/xproto/7.0.31-2iu6dcc")

prepend_path("MANPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/libxau-1.0.8-iwyawfwrfnkg5dsgahjbx2p6v3wikgl6/share/man", ":")
prepend_path("CPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/libxau-1.0.8-iwyawfwrfnkg5dsgahjbx2p6v3wikgl6/include", ":")
prepend_path("LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/libxau-1.0.8-iwyawfwrfnkg5dsgahjbx2p6v3wikgl6/lib", ":")
prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/libxau-1.0.8-iwyawfwrfnkg5dsgahjbx2p6v3wikgl6/lib", ":")
prepend_path("PKG_CONFIG_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/libxau-1.0.8-iwyawfwrfnkg5dsgahjbx2p6v3wikgl6/lib/pkgconfig", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/libxau-1.0.8-iwyawfwrfnkg5dsgahjbx2p6v3wikgl6/.", ":")

