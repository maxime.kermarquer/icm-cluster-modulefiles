-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2023-05-09 15:51:02.881182
--
-- libxcb@1.14%gcc@7.3.0 build_system=autotools arch=linux-centos7-broadwell/34nijb6
--

whatis([[Name : libxcb]])
whatis([[Version : 1.14]])
whatis([[Target : broadwell]])
whatis([[Short description : The X protocol C-language Binding (XCB) is a replacement for Xlib featuring a small footprint, latency hiding, direct access to the protocol, improved threading support, and extensibility.]])

help([[Name   : libxcb]])
help([[Version: 1.14]])
help([[Target : broadwell]])
help()
help([[The X protocol C-language Binding (XCB) is a replacement for Xlib
featuring a small footprint, latency hiding, direct access to the
protocol, improved threading support, and extensibility.]])


depends_on("linux-centos7-x86_64/gcc/7.3.0/libpthread-stubs/0.4-zewmkbf")
depends_on("linux-centos7-x86_64/gcc/7.3.0/libxau/1.0.8-iwyawfw")
depends_on("linux-centos7-x86_64/gcc/7.3.0/libxdmcp/1.1.4-bxdymj6")
depends_on("linux-centos7-x86_64/gcc/7.3.0/xcb-proto/1.15.2-yxxgxwl")

prepend_path("CPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/libxcb-1.14-34nijb6mnjyoq5vknrhoczj5ufw4kpeg/include", ":")
prepend_path("LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/libxcb-1.14-34nijb6mnjyoq5vknrhoczj5ufw4kpeg/lib", ":")
prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/libxcb-1.14-34nijb6mnjyoq5vknrhoczj5ufw4kpeg/lib", ":")
prepend_path("MANPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/libxcb-1.14-34nijb6mnjyoq5vknrhoczj5ufw4kpeg/share/man", ":")
prepend_path("PKG_CONFIG_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/libxcb-1.14-34nijb6mnjyoq5vknrhoczj5ufw4kpeg/lib/pkgconfig", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/libxcb-1.14-34nijb6mnjyoq5vknrhoczj5ufw4kpeg/.", ":")

