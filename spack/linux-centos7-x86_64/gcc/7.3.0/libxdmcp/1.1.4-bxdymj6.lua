-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2023-05-09 15:49:30.460291
--
-- libxdmcp@1.1.4%gcc@7.3.0 build_system=autotools arch=linux-centos7-broadwell/bxdymj6
--

whatis([[Name : libxdmcp]])
whatis([[Version : 1.1.4]])
whatis([[Target : broadwell]])
whatis([[Short description : libXdmcp - X Display Manager Control Protocol library.]])

help([[Name   : libxdmcp]])
help([[Version: 1.1.4]])
help([[Target : broadwell]])
help()
help([[libXdmcp - X Display Manager Control Protocol library.]])


depends_on("linux-centos7-x86_64/gcc/7.3.0/libbsd/0.11.7-ka2ajwy")
depends_on("linux-centos7-x86_64/gcc/7.3.0/xproto/7.0.31-2iu6dcc")

prepend_path("PKG_CONFIG_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/libxdmcp-1.1.4-bxdymj6xfvk4qze23vij52ihzmbixewh/lib/pkgconfig", ":")
prepend_path("CPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/libxdmcp-1.1.4-bxdymj6xfvk4qze23vij52ihzmbixewh/include", ":")
prepend_path("LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/libxdmcp-1.1.4-bxdymj6xfvk4qze23vij52ihzmbixewh/lib", ":")
prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/libxdmcp-1.1.4-bxdymj6xfvk4qze23vij52ihzmbixewh/lib", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/libxdmcp-1.1.4-bxdymj6xfvk4qze23vij52ihzmbixewh/.", ":")

