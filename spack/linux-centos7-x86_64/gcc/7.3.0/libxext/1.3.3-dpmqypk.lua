-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2023-05-09 15:55:57.869707
--
-- libxext@1.3.3%gcc@7.3.0 build_system=autotools arch=linux-centos7-broadwell/dpmqypk
--

whatis([[Name : libxext]])
whatis([[Version : 1.3.3]])
whatis([[Target : broadwell]])
whatis([[Short description : libXext - library for common extensions to the X11 protocol.]])

help([[Name   : libxext]])
help([[Version: 1.3.3]])
help([[Target : broadwell]])
help()
help([[libXext - library for common extensions to the X11 protocol.]])


depends_on("linux-centos7-x86_64/gcc/7.3.0/libx11/1.8.4-mygw7c4")
depends_on("linux-centos7-x86_64/gcc/7.3.0/xextproto/7.3.0-jmtrxor")
depends_on("linux-centos7-x86_64/gcc/7.3.0/xproto/7.0.31-2iu6dcc")

prepend_path("CPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/libxext-1.3.3-dpmqypkms2mvlptstnmxu2yxtwo6okmi/include", ":")
prepend_path("LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/libxext-1.3.3-dpmqypkms2mvlptstnmxu2yxtwo6okmi/lib", ":")
prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/libxext-1.3.3-dpmqypkms2mvlptstnmxu2yxtwo6okmi/lib", ":")
prepend_path("MANPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/libxext-1.3.3-dpmqypkms2mvlptstnmxu2yxtwo6okmi/share/man", ":")
prepend_path("PKG_CONFIG_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/libxext-1.3.3-dpmqypkms2mvlptstnmxu2yxtwo6okmi/lib/pkgconfig", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/libxext-1.3.3-dpmqypkms2mvlptstnmxu2yxtwo6okmi/.", ":")
prepend_path("XLOCALEDIR", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/libx11-1.8.4-mygw7c45hwjfojavmnf2f3qnjb3bugq4/share/X11/locale", ":")

