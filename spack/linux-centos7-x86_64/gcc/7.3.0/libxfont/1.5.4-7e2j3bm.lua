-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2023-05-09 15:51:57.719008
--
-- libxfont@1.5.4%gcc@7.3.0 build_system=autotools arch=linux-centos7-broadwell/7e2j3bm
--

whatis([[Name : libxfont]])
whatis([[Version : 1.5.4]])
whatis([[Target : broadwell]])
whatis([[Short description : libXfont provides the core of the legacy X11 font system, handling the index files (fonts.dir, fonts.alias, fonts.scale), the various font file formats, and rasterizing them. It is used by the X servers, the X Font Server (xfs), and some font utilities (bdftopcf for instance), but should not be used by normal X11 clients. X11 clients access fonts via either the new API's in libXft, or the legacy API's in libX11.]])

help([[Name   : libxfont]])
help([[Version: 1.5.4]])
help([[Target : broadwell]])
help()
help([[libXfont provides the core of the legacy X11 font system, handling the
index files (fonts.dir, fonts.alias, fonts.scale), the various font file
formats, and rasterizing them. It is used by the X servers, the X Font
Server (xfs), and some font utilities (bdftopcf for instance), but
should not be used by normal X11 clients. X11 clients access fonts via
either the new API's in libXft, or the legacy API's in libX11.]])


depends_on("fontsproto/2.1.3-245mshc")
depends_on("freetype/2.11.1-5stkwjm")
depends_on("libfontenc/1.1.7-4u3k7ym")
depends_on("xproto/7.0.31-2iu6dcc")
depends_on("xtrans/1.4.0-g2tksfe")

prepend_path("PKG_CONFIG_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/libxfont-1.5.4-7e2j3bmxashmeapot6reiqit4ape7s56/lib/pkgconfig", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/libxfont-1.5.4-7e2j3bmxashmeapot6reiqit4ape7s56/.", ":")

