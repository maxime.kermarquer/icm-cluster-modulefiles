-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2023-05-11 11:01:19.965425
--
-- libxfont2@2.0.6%gcc@7.3.0 build_system=autotools arch=linux-centos7-broadwell/d2rotvw
--

whatis([[Name : libxfont2]])
whatis([[Version : 2.0.6]])
whatis([[Target : broadwell]])
whatis([[Short description : libXfont provides the core of the legacy X11 font system, handling the index files (fonts.dir, fonts.alias, fonts.scale), the various font file formats, and rasterizing them. It is used by the X servers, the X Font Server (xfs), and some font utilities (bdftopcf for instance), but should not be used by normal X11 clients. X11 clients access fonts via either the new API's in libXft, or the legacy API's in libX11.]])

help([[Name   : libxfont2]])
help([[Version: 2.0.6]])
help([[Target : broadwell]])
help()
help([[libXfont provides the core of the legacy X11 font system, handling the
index files (fonts.dir, fonts.alias, fonts.scale), the various font file
formats, and rasterizing them. It is used by the X servers, the X Font
Server (xfs), and some font utilities (bdftopcf for instance), but
should not be used by normal X11 clients. X11 clients access fonts via
either the new API's in libXft, or the legacy API's in libX11.]])


depends_on("fontsproto/2.1.3-245mshc")
depends_on("freetype/2.11.1-5stkwjm")
depends_on("libfontenc/1.1.7-4u3k7ym")
depends_on("xproto/7.0.31-2iu6dcc")
depends_on("xtrans/1.4.0-g2tksfe")

prepend_path("PKG_CONFIG_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/libxfont2-2.0.6-d2rotvwgmxk64dnmaix56w7jrux3atdl/lib/pkgconfig", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/libxfont2-2.0.6-d2rotvwgmxk64dnmaix56w7jrux3atdl/.", ":")

