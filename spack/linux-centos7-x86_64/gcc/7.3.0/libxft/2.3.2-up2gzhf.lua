-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2023-05-09 16:05:09.687865
--
-- libxft@2.3.2%gcc@7.3.0 build_system=autotools arch=linux-centos7-broadwell/up2gzhf
--

whatis([[Name : libxft]])
whatis([[Version : 2.3.2]])
whatis([[Target : broadwell]])
whatis([[Short description : X FreeType library.]])

help([[Name   : libxft]])
help([[Version: 2.3.2]])
help([[Target : broadwell]])
help()
help([[X FreeType library. Xft version 2.1 was the first stand alone release of
Xft, a library that connects X applications with the FreeType font
rasterization library. Xft uses fontconfig to locate fonts so it has no
configuration files.]])


depends_on("linux-centos7-x86_64/gcc/7.3.0/fontconfig/2.14.2-pnelxju")
depends_on("linux-centos7-x86_64/gcc/7.3.0/freetype/2.11.1-5stkwjm")
depends_on("linux-centos7-x86_64/gcc/7.3.0/libx11/1.8.4-mygw7c4")
depends_on("linux-centos7-x86_64/gcc/7.3.0/libxrender/0.9.10-perzwwz")

prepend_path("MANPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/libxft-2.3.2-up2gzhfs6lue7ipfheteue54llbop6eh/share/man", ":")
prepend_path("PKG_CONFIG_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/libxft-2.3.2-up2gzhfs6lue7ipfheteue54llbop6eh/lib/pkgconfig", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/libxft-2.3.2-up2gzhfs6lue7ipfheteue54llbop6eh/.", ":")
prepend_path("XLOCALEDIR", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/libx11-1.8.4-mygw7c45hwjfojavmnf2f3qnjb3bugq4/share/X11/locale", ":")

