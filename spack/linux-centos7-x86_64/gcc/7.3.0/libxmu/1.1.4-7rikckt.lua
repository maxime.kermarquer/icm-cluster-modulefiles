-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2023-05-15 14:41:14.915059
--
-- libxmu@1.1.4%gcc@7.3.0 build_system=autotools arch=linux-centos7-broadwell/7rikckt
--

whatis([[Name : libxmu]])
whatis([[Version : 1.1.4]])
whatis([[Target : broadwell]])
whatis([[Short description : This library contains miscellaneous utilities and is not part of the Xlib standard. It contains routines which only use public interfaces so that it may be layered on top of any proprietary implementation of Xlib or Xt.]])

help([[Name   : libxmu]])
help([[Version: 1.1.4]])
help([[Target : broadwell]])
help()
help([[This library contains miscellaneous utilities and is not part of the
Xlib standard. It contains routines which only use public interfaces so
that it may be layered on top of any proprietary implementation of Xlib
or Xt.]])


depends_on("libx11/1.8.4-mygw7c4")
depends_on("libxext/1.3.3-dpmqypk")
depends_on("libxt/1.1.5-34x6pje")
depends_on("xextproto/7.3.0-jmtrxor")

prepend_path("PKG_CONFIG_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/libxmu-1.1.4-7rikckt2eqz5dh26dedb7sanunhjv4ed/lib/pkgconfig", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/libxmu-1.1.4-7rikckt2eqz5dh26dedb7sanunhjv4ed/.", ":")
prepend_path("XLOCALEDIR", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/libx11-1.8.4-mygw7c45hwjfojavmnf2f3qnjb3bugq4/share/X11/locale", ":")

