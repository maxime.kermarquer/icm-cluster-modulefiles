-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2023-06-13 14:28:23.106781
--
-- libxpm@3.5.12%gcc@7.3.0 build_system=autotools arch=linux-centos7-broadwell/mygkxo7
--

whatis([[Name : libxpm]])
whatis([[Version : 3.5.12]])
whatis([[Target : broadwell]])
whatis([[Short description : libXpm - X Pixmap (XPM) image file format library.]])

help([[Name   : libxpm]])
help([[Version: 3.5.12]])
help([[Target : broadwell]])
help()
help([[libXpm - X Pixmap (XPM) image file format library.]])


depends_on("gettext/0.21.1-nfzsems")
depends_on("libx11/1.8.4-mygw7c4")
depends_on("xproto/7.0.31-2iu6dcc")

prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/libxpm-3.5.12-mygkxo7rwhhvwjcn4fiioohg5cp2nmbw/bin", ":")
prepend_path("MANPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/libxpm-3.5.12-mygkxo7rwhhvwjcn4fiioohg5cp2nmbw/share/man", ":")
prepend_path("PKG_CONFIG_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/libxpm-3.5.12-mygkxo7rwhhvwjcn4fiioohg5cp2nmbw/lib/pkgconfig", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/libxpm-3.5.12-mygkxo7rwhhvwjcn4fiioohg5cp2nmbw/.", ":")
prepend_path("XLOCALEDIR", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/libx11-1.8.4-mygw7c45hwjfojavmnf2f3qnjb3bugq4/share/X11/locale", ":")

