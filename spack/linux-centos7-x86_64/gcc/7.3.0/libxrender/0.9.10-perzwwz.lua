-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2023-05-09 15:55:41.497645
--
-- libxrender@0.9.10%gcc@7.3.0 build_system=autotools arch=linux-centos7-broadwell/perzwwz
--

whatis([[Name : libxrender]])
whatis([[Version : 0.9.10]])
whatis([[Target : broadwell]])
whatis([[Short description : libXrender - library for the Render Extension to the X11 protocol.]])

help([[Name   : libxrender]])
help([[Version: 0.9.10]])
help([[Target : broadwell]])
help()
help([[libXrender - library for the Render Extension to the X11 protocol.]])


depends_on("linux-centos7-x86_64/gcc/7.3.0/libx11/1.8.4-mygw7c4")
depends_on("linux-centos7-x86_64/gcc/7.3.0/renderproto/0.11.1-uhct4pu")

prepend_path("CPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/libxrender-0.9.10-perzwwzqffbgi3aa6ndetvmbyitrkrny/include", ":")
prepend_path("LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/libxrender-0.9.10-perzwwzqffbgi3aa6ndetvmbyitrkrny/lib", ":")
prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/libxrender-0.9.10-perzwwzqffbgi3aa6ndetvmbyitrkrny/lib", ":")
prepend_path("PKG_CONFIG_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/libxrender-0.9.10-perzwwzqffbgi3aa6ndetvmbyitrkrny/lib/pkgconfig", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/libxrender-0.9.10-perzwwzqffbgi3aa6ndetvmbyitrkrny/.", ":")
prepend_path("XLOCALEDIR", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/libx11-1.8.4-mygw7c45hwjfojavmnf2f3qnjb3bugq4/share/X11/locale", ":")

