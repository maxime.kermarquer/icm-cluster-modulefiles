-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2023-05-09 16:04:01.365295
--
-- libxscrnsaver@1.2.2%gcc@7.3.0 build_system=autotools arch=linux-centos7-broadwell/7vp2siu
--

whatis([[Name : libxscrnsaver]])
whatis([[Version : 1.2.2]])
whatis([[Target : broadwell]])
whatis([[Short description : XScreenSaver - X11 Screen Saver extension client library]])

help([[Name   : libxscrnsaver]])
help([[Version: 1.2.2]])
help([[Target : broadwell]])
help()
help([[XScreenSaver - X11 Screen Saver extension client library]])


depends_on("linux-centos7-x86_64/gcc/7.3.0/libx11/1.8.4-mygw7c4")
depends_on("linux-centos7-x86_64/gcc/7.3.0/libxext/1.3.3-dpmqypk")
depends_on("linux-centos7-x86_64/gcc/7.3.0/scrnsaverproto/1.2.2-72lyvk3")
depends_on("linux-centos7-x86_64/gcc/7.3.0/xextproto/7.3.0-jmtrxor")

prepend_path("MANPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/libxscrnsaver-1.2.2-7vp2siubvof26s4hudwdl62yvjavayo6/share/man", ":")
prepend_path("PKG_CONFIG_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/libxscrnsaver-1.2.2-7vp2siubvof26s4hudwdl62yvjavayo6/lib/pkgconfig", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/libxscrnsaver-1.2.2-7vp2siubvof26s4hudwdl62yvjavayo6/.", ":")
prepend_path("XLOCALEDIR", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/libx11-1.8.4-mygw7c45hwjfojavmnf2f3qnjb3bugq4/share/X11/locale", ":")

