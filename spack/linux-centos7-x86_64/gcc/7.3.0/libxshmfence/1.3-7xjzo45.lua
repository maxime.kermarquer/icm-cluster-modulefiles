-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2023-05-11 11:00:16.723275
--
-- libxshmfence@1.3%gcc@7.3.0 build_system=autotools arch=linux-centos7-broadwell/7xjzo45
--

whatis([[Name : libxshmfence]])
whatis([[Version : 1.3]])
whatis([[Target : broadwell]])
whatis([[Short description : libxshmfence - Shared memory 'SyncFence' synchronization primitive.]])

help([[Name   : libxshmfence]])
help([[Version: 1.3]])
help([[Target : broadwell]])
help()
help([[libxshmfence - Shared memory 'SyncFence' synchronization primitive. This
library offers a CPU-based synchronization primitive compatible with the
X SyncFence objects that can be shared between processes using file
descriptor passing.]])


depends_on("xproto/7.0.31-2iu6dcc")

prepend_path("PKG_CONFIG_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/libxshmfence-1.3-7xjzo45aqsepaecsrvcatkzcsk6b46bp/lib/pkgconfig", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/libxshmfence-1.3-7xjzo45aqsepaecsrvcatkzcsk6b46bp/.", ":")

