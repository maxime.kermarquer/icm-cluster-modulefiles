-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2023-05-11 10:33:27.498490
--
-- libxt@1.1.5%gcc@7.3.0 build_system=autotools arch=linux-centos7-broadwell/34x6pje
--

whatis([[Name : libxt]])
whatis([[Version : 1.1.5]])
whatis([[Target : broadwell]])
whatis([[Short description : libXt - X Toolkit Intrinsics library.]])

help([[Name   : libxt]])
help([[Version: 1.1.5]])
help([[Target : broadwell]])
help()
help([[libXt - X Toolkit Intrinsics library.]])


depends_on("linux-centos7-x86_64/gcc/7.3.0/kbproto/1.0.7-espyygc")
depends_on("linux-centos7-x86_64/gcc/7.3.0/libice/1.0.9-eqomujj")
depends_on("linux-centos7-x86_64/gcc/7.3.0/libsm/1.2.3-sfa6dzj")
depends_on("linux-centos7-x86_64/gcc/7.3.0/libx11/1.8.4-mygw7c4")
depends_on("linux-centos7-x86_64/gcc/7.3.0/xproto/7.0.31-2iu6dcc")

prepend_path("CPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/libxt-1.1.5-34x6pjeyx26wkwliojnzh4oqzqlcbn5o/include", ":")
prepend_path("LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/libxt-1.1.5-34x6pjeyx26wkwliojnzh4oqzqlcbn5o/lib", ":")
prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/libxt-1.1.5-34x6pjeyx26wkwliojnzh4oqzqlcbn5o/lib", ":")
prepend_path("MANPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/libxt-1.1.5-34x6pjeyx26wkwliojnzh4oqzqlcbn5o/share/man", ":")
prepend_path("PKG_CONFIG_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/libxt-1.1.5-34x6pjeyx26wkwliojnzh4oqzqlcbn5o/lib/pkgconfig", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/libxt-1.1.5-34x6pjeyx26wkwliojnzh4oqzqlcbn5o/.", ":")
prepend_path("XLOCALEDIR", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/libx11-1.8.4-mygw7c45hwjfojavmnf2f3qnjb3bugq4/share/X11/locale", ":")

