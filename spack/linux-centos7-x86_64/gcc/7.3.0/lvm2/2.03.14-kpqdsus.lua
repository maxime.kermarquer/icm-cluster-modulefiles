-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2023-06-05 18:00:37.894440
--
-- lvm2@2.03.14%gcc@7.3.0+pkgconfig build_system=autotools arch=linux-centos7-broadwell/kpqdsus
--

whatis([[Name : lvm2]])
whatis([[Version : 2.03.14]])
whatis([[Target : broadwell]])
whatis([[Short description : LVM2 is the userspace toolset that provides logical volume management facilities on linux.]])
whatis([[Configure options : --with-confdir=/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/lvm2-2.03.14-kpqdsusi34idrwef4gr5vsd7uh4enunc/etc --with-default-system-dir=/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/lvm2-2.03.14-kpqdsusi34idrwef4gr5vsd7uh4enunc/etc/lvm --enable-pkgconfig]])

help([[Name   : lvm2]])
help([[Version: 2.03.14]])
help([[Target : broadwell]])
help()
help([[LVM2 is the userspace toolset that provides logical volume management
facilities on linux. To use it you need 3 things: device-mapper in your
kernel, the userspace device-mapper support library (libdevmapper) and
the userspace LVM2 tools (dmsetup). These userspace components, and
associated header files, are provided by this package. See
http://sources.redhat.com/dm/ for additional information about the
device-mapper kernel and userspace components.]])


depends_on("libaio/0.3.113-4puuk2d")

prepend_path("MANPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/lvm2-2.03.14-kpqdsusi34idrwef4gr5vsd7uh4enunc/share/man", ":")
prepend_path("PKG_CONFIG_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/lvm2-2.03.14-kpqdsusi34idrwef4gr5vsd7uh4enunc/lib/pkgconfig", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/lvm2-2.03.14-kpqdsusi34idrwef4gr5vsd7uh4enunc/.", ":")

