-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2023-05-04 16:02:26.093566
--
-- m4@1.4.19%gcc@7.3.0+sigsegv build_system=autotools patches=9dc5fbd,bfdffa7 arch=linux-centos7-broadwell/qurp35e
--

whatis([[Name : m4]])
whatis([[Version : 1.4.19]])
whatis([[Target : broadwell]])
whatis([[Short description : GNU M4 is an implementation of the traditional Unix macro processor.]])
whatis([[Configure options : --enable-c++ --with-libsigsegv-prefix=/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/libsigsegv-2.14-6rduolhl7kuerjs652lcesv37skt7mly]])

help([[Name   : m4]])
help([[Version: 1.4.19]])
help([[Target : broadwell]])
help()
help([[GNU M4 is an implementation of the traditional Unix macro processor.]])


depends_on("linux-centos7-x86_64/gcc/7.3.0/libsigsegv/2.14-6rduolh")

prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/m4-1.4.19-qurp35ez7hgyn44ncldh6egeyrsj2vdz/bin", ":")
prepend_path("MANPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/m4-1.4.19-qurp35ez7hgyn44ncldh6egeyrsj2vdz/share/man", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/m4-1.4.19-qurp35ez7hgyn44ncldh6egeyrsj2vdz/.", ":")
setenv("M4", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/m4-1.4.19-qurp35ez7hgyn44ncldh6egeyrsj2vdz/bin/m4")

