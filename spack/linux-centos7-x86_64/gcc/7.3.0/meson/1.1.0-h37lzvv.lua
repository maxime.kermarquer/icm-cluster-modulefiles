-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2023-05-09 15:16:41.652565
--
-- meson@1.1.0%gcc@7.3.0 build_system=python_pip patches=0f0b1bd arch=linux-centos7-broadwell/h37lzvv
--

whatis([[Name : meson]])
whatis([[Version : 1.1.0]])
whatis([[Target : broadwell]])
whatis([[Short description : Meson is a portable open source build system meant to be both extremely fast, and as user friendly as possible.]])

help([[Name   : meson]])
help([[Version: 1.1.0]])
help([[Target : broadwell]])
help()
help([[Meson is a portable open source build system meant to be both extremely
fast, and as user friendly as possible.]])


depends_on("ninja/1.11.1-hqqlyl4")
depends_on("py-setuptools/67.6.0-ealma7f")
depends_on("python/3.10.10-3muxe4q")

prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/meson-1.1.0-h37lzvvzkg3jxzpotts5cp5z3jqsohry/bin", ":")
prepend_path("MANPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/meson-1.1.0-h37lzvvzkg3jxzpotts5cp5z3jqsohry/share/man", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/meson-1.1.0-h37lzvvzkg3jxzpotts5cp5z3jqsohry/.", ":")
prepend_path("PYTHONPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/meson-1.1.0-h37lzvvzkg3jxzpotts5cp5z3jqsohry/lib/python3.10/site-packages", ":")
prepend_path("PYTHONPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/py-setuptools-67.6.0-ealma7fri6akxxbrmiyxkbx6wlnpfh5r/lib/python3.10/site-packages", ":")

