-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2023-05-09 16:32:56.634720
--
-- meson@1.1.0%gcc@7.3.0 build_system=python_pip patches=0f0b1bd arch=linux-centos7-broadwell/kfdj6zh
--

whatis([[Name : meson]])
whatis([[Version : 1.1.0]])
whatis([[Target : broadwell]])
whatis([[Short description : Meson is a portable open source build system meant to be both extremely fast, and as user friendly as possible.]])

help([[Name   : meson]])
help([[Version: 1.1.0]])
help([[Target : broadwell]])
help()
help([[Meson is a portable open source build system meant to be both extremely
fast, and as user friendly as possible.]])


depends_on("ninja/1.11.1-hqqlyl4")
depends_on("py-setuptools/67.6.0-7lhakw5")
depends_on("python/3.10.10-x6q4jmz")

prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/meson-1.1.0-kfdj6zh7lfqd5bs7av4n2jtmwxk4zykj/bin", ":")
prepend_path("MANPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/meson-1.1.0-kfdj6zh7lfqd5bs7av4n2jtmwxk4zykj/share/man", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/meson-1.1.0-kfdj6zh7lfqd5bs7av4n2jtmwxk4zykj/.", ":")
prepend_path("PYTHONPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/meson-1.1.0-kfdj6zh7lfqd5bs7av4n2jtmwxk4zykj/lib/python3.10/site-packages", ":")
prepend_path("PYTHONPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/py-setuptools-67.6.0-7lhakw5nhh4oohbgarrxuo37dno5he7g/lib/python3.10/site-packages", ":")

