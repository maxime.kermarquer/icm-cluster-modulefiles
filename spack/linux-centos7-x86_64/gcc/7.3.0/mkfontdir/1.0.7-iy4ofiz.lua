-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2023-05-09 15:52:07.357898
--
-- mkfontdir@1.0.7%gcc@7.3.0 build_system=autotools arch=linux-centos7-broadwell/iy4ofiz
--

whatis([[Name : mkfontdir]])
whatis([[Version : 1.0.7]])
whatis([[Target : broadwell]])
whatis([[Short description : mkfontdir creates the fonts.dir files needed by the legacy X server core font system. The current implementation is a simple wrapper script around the mkfontscale program, which must be built and installed first.]])

help([[Name   : mkfontdir]])
help([[Version: 1.0.7]])
help([[Target : broadwell]])
help()
help([[mkfontdir creates the fonts.dir files needed by the legacy X server core
font system. The current implementation is a simple wrapper script
around the mkfontscale program, which must be built and installed first.]])


depends_on("mkfontscale/1.2.2-w3rq3gk")

prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/mkfontdir-1.0.7-iy4ofizqw3omhgcozpbxjtbn2t4tyw5z/bin", ":")
prepend_path("MANPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/mkfontdir-1.0.7-iy4ofizqw3omhgcozpbxjtbn2t4tyw5z/share/man", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/mkfontdir-1.0.7-iy4ofizqw3omhgcozpbxjtbn2t4tyw5z/.", ":")

