-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2023-05-09 15:51:17.051826
--
-- mkfontscale@1.2.2%gcc@7.3.0 build_system=autotools arch=linux-centos7-broadwell/w3rq3gk
--

whatis([[Name : mkfontscale]])
whatis([[Version : 1.2.2]])
whatis([[Target : broadwell]])
whatis([[Short description : mkfontscale creates the fonts.scale and fonts.dir index files used by the legacy X11 font system.]])

help([[Name   : mkfontscale]])
help([[Version: 1.2.2]])
help([[Target : broadwell]])
help()
help([[mkfontscale creates the fonts.scale and fonts.dir index files used by
the legacy X11 font system.]])


depends_on("freetype/2.11.1-5stkwjm")
depends_on("libfontenc/1.1.7-4u3k7ym")
depends_on("xproto/7.0.31-2iu6dcc")

prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/mkfontscale-1.2.2-w3rq3gkxb7vpvu343ussqanc63jwsrga/bin", ":")
prepend_path("MANPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/mkfontscale-1.2.2-w3rq3gkxb7vpvu343ussqanc63jwsrga/share/man", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/mkfontscale-1.2.2-w3rq3gkxb7vpvu343ussqanc63jwsrga/.", ":")

