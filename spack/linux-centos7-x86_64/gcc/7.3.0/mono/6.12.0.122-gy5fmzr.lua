-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2023-06-05 12:58:21.455948
--
-- mono@6.12.0.122%gcc@7.3.0~patch-folder-path build_system=autotools arch=linux-centos7-broadwell/gy5fmzr
--

whatis([[Name : mono]])
whatis([[Version : 6.12.0.122]])
whatis([[Target : broadwell]])
whatis([[Short description : Mono is a software platform designed to allow developers to easily create cross platform applications. It is an open source implementation of Microsoft's .NET Framework based on the ECMA standards for C# and the Common Language Runtime. ]])
whatis([[Configure options : --with-libiconv-prefix=/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/libiconv-1.17-afgvmbc7dt2refbap2qamy32yibwnrcr]])

help([[Name   : mono]])
help([[Version: 6.12.0.122]])
help([[Target : broadwell]])
help()
help([[Mono is a software platform designed to allow developers to easily
create cross platform applications. It is an open source implementation
of Microsoft's .NET Framework based on the ECMA standards for C# and the
Common Language Runtime.]])


depends_on("linux-centos7-x86_64/gcc/7.3.0/libiconv/1.17-afgvmbc")

prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/mono-6.12.0.122-gy5fmzrfvzjqrpuixpji4qirruwzn3pi/bin", ":")
prepend_path("MANPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/mono-6.12.0.122-gy5fmzrfvzjqrpuixpji4qirruwzn3pi/share/man", ":")
prepend_path("PKG_CONFIG_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/mono-6.12.0.122-gy5fmzrfvzjqrpuixpji4qirruwzn3pi/lib/pkgconfig", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/mono-6.12.0.122-gy5fmzrfvzjqrpuixpji4qirruwzn3pi/.", ":")

