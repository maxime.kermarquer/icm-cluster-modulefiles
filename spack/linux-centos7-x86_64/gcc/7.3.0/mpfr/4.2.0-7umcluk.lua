-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2023-05-15 14:27:00.262018
--
-- mpfr@4.2.0%gcc@7.3.0 build_system=autotools libs=shared,static arch=linux-centos7-broadwell/7umcluk
--

whatis([[Name : mpfr]])
whatis([[Version : 4.2.0]])
whatis([[Target : broadwell]])
whatis([[Short description : The MPFR library is a C library for multiple-precision floating-point computations with correct rounding.]])
whatis([[Configure options : --with-gmp=/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/gmp-6.2.1-l7yn7zwk5whymcmwqbydkmtebtk4mxj3 --enable-shared --enable-static --with-pic]])

help([[Name   : mpfr]])
help([[Version: 4.2.0]])
help([[Target : broadwell]])
help()
help([[The MPFR library is a C library for multiple-precision floating-point
computations with correct rounding.]])


depends_on("linux-centos7-x86_64/gcc/7.3.0/gmp/6.2.1-l7yn7zw")

prepend_path("CPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/mpfr-4.2.0-7umclukwdsljt5cho3oqann4wq3vfc2w/include", ":")
prepend_path("LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/mpfr-4.2.0-7umclukwdsljt5cho3oqann4wq3vfc2w/lib", ":")
prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/mpfr-4.2.0-7umclukwdsljt5cho3oqann4wq3vfc2w/lib", ":")
prepend_path("PKG_CONFIG_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/mpfr-4.2.0-7umclukwdsljt5cho3oqann4wq3vfc2w/lib/pkgconfig", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/mpfr-4.2.0-7umclukwdsljt5cho3oqann4wq3vfc2w/.", ":")

