-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2023-05-10 09:23:52.007024
--
-- nasm@2.15.05%gcc@7.3.0 build_system=autotools arch=linux-centos7-broadwell/hd7oga6
--

whatis([[Name : nasm]])
whatis([[Version : 2.15.05]])
whatis([[Target : broadwell]])
whatis([[Short description : NASM (Netwide Assembler) is an 80x86 assembler designed for portability and modularity. It includes a disassembler as well.]])

help([[Name   : nasm]])
help([[Version: 2.15.05]])
help([[Target : broadwell]])
help()
help([[NASM (Netwide Assembler) is an 80x86 assembler designed for portability
and modularity. It includes a disassembler as well.]])



prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/nasm-2.15.05-hd7oga6453g7akfozhu4cynaazti2ayf/bin", ":")
prepend_path("MANPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/nasm-2.15.05-hd7oga6453g7akfozhu4cynaazti2ayf/share/man", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/nasm-2.15.05-hd7oga6453g7akfozhu4cynaazti2ayf/.", ":")

