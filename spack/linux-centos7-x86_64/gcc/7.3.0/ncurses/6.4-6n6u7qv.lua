-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2023-05-04 14:46:07.580295
--
-- ncurses@6.4%gcc@7.3.0~symlinks+termlib abi=none build_system=autotools arch=linux-centos7-broadwell/6n6u7qv
--

whatis([[Name : ncurses]])
whatis([[Version : 6.4]])
whatis([[Target : broadwell]])
whatis([[Short description : The ncurses (new curses) library is a free software emulation of curses in System V Release 4.0, and more. It uses terminfo format, supports pads and color and multiple highlights and forms characters and function-key mapping, and has all the other SYSV-curses enhancements over BSD curses.]])

help([[Name   : ncurses]])
help([[Version: 6.4]])
help([[Target : broadwell]])
help()
help([[The ncurses (new curses) library is a free software emulation of curses
in System V Release 4.0, and more. It uses terminfo format, supports
pads and color and multiple highlights and forms characters and
function-key mapping, and has all the other SYSV-curses enhancements
over BSD curses.]])



prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/ncurses-6.4-6n6u7qv2o4rszzmabo3ykcf6cwuetgzl/bin", ":")
prepend_path("CPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/ncurses-6.4-6n6u7qv2o4rszzmabo3ykcf6cwuetgzl/include", ":")
prepend_path("LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/ncurses-6.4-6n6u7qv2o4rszzmabo3ykcf6cwuetgzl/lib", ":")
prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/ncurses-6.4-6n6u7qv2o4rszzmabo3ykcf6cwuetgzl/lib", ":")
prepend_path("PKG_CONFIG_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/ncurses-6.4-6n6u7qv2o4rszzmabo3ykcf6cwuetgzl/lib/pkgconfig", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/ncurses-6.4-6n6u7qv2o4rszzmabo3ykcf6cwuetgzl/.", ":")

