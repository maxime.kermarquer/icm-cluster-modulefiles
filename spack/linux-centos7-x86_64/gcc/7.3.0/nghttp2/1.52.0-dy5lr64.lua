-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2023-05-10 09:27:22.341444
--
-- nghttp2@1.52.0%gcc@7.3.0 build_system=autotools arch=linux-centos7-broadwell/dy5lr64
--

whatis([[Name : nghttp2]])
whatis([[Version : 1.52.0]])
whatis([[Target : broadwell]])
whatis([[Short description : nghttp2 is an implementation of HTTP/2 and its header compression algorithm HPACK in C.]])
whatis([[Configure options : --enable-lib-only --with-libxml2=no --with-jansson=no --with-zlib=no --with-libevent-openssl=no --with-libcares=no --with-openssl=no --with-libev=no --with-cunit=no --with-jemalloc=no --with-systemd=no --with-mruby=no --with-neverbleed=no --with-boost=no]])

help([[Name   : nghttp2]])
help([[Version: 1.52.0]])
help([[Target : broadwell]])
help()
help([[nghttp2 is an implementation of HTTP/2 and its header compression
algorithm HPACK in C.]])



prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/nghttp2-1.52.0-dy5lr64zo4czteehf6oxbudw3bpah7st/bin", ":")
prepend_path("CPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/nghttp2-1.52.0-dy5lr64zo4czteehf6oxbudw3bpah7st/include", ":")
prepend_path("LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/nghttp2-1.52.0-dy5lr64zo4czteehf6oxbudw3bpah7st/lib", ":")
prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/nghttp2-1.52.0-dy5lr64zo4czteehf6oxbudw3bpah7st/lib", ":")
prepend_path("MANPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/nghttp2-1.52.0-dy5lr64zo4czteehf6oxbudw3bpah7st/share/man", ":")
prepend_path("PKG_CONFIG_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/nghttp2-1.52.0-dy5lr64zo4czteehf6oxbudw3bpah7st/lib/pkgconfig", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/nghttp2-1.52.0-dy5lr64zo4czteehf6oxbudw3bpah7st/.", ":")

