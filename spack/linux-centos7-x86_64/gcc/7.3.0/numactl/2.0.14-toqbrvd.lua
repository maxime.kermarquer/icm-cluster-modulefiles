-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2023-05-15 15:04:18.769992
--
-- numactl@2.0.14%gcc@7.3.0 build_system=autotools patches=4e1d78c,62fc8a8,ff37630 arch=linux-centos7-broadwell/toqbrvd
--

whatis([[Name : numactl]])
whatis([[Version : 2.0.14]])
whatis([[Target : broadwell]])
whatis([[Short description : NUMA support for Linux]])

help([[Name   : numactl]])
help([[Version: 2.0.14]])
help([[Target : broadwell]])
help()
help([[NUMA support for Linux]])



prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/numactl-2.0.14-toqbrvdqq57q7qvp23l3v7t7kazpd4gq/bin", ":")
prepend_path("CPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/numactl-2.0.14-toqbrvdqq57q7qvp23l3v7t7kazpd4gq/include", ":")
prepend_path("LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/numactl-2.0.14-toqbrvdqq57q7qvp23l3v7t7kazpd4gq/lib", ":")
prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/numactl-2.0.14-toqbrvdqq57q7qvp23l3v7t7kazpd4gq/lib", ":")
prepend_path("MANPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/numactl-2.0.14-toqbrvdqq57q7qvp23l3v7t7kazpd4gq/share/man", ":")
prepend_path("PKG_CONFIG_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/numactl-2.0.14-toqbrvdqq57q7qvp23l3v7t7kazpd4gq/lib/pkgconfig", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/numactl-2.0.14-toqbrvdqq57q7qvp23l3v7t7kazpd4gq/.", ":")

