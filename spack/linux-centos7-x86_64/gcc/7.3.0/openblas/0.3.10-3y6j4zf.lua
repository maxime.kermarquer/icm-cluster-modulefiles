-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2023-05-09 17:17:40.173917
--
-- openblas@0.3.10%gcc@7.3.0~bignuma~consistent_fpcsr~ilp64+locking+pic+shared build_system=makefile patches=865703b symbol_suffix=none threads=none arch=linux-centos7-broadwell/3y6j4zf
--

whatis([[Name : openblas]])
whatis([[Version : 0.3.10]])
whatis([[Target : broadwell]])
whatis([[Short description : OpenBLAS: An optimized BLAS library]])

help([[Name   : openblas]])
help([[Version: 0.3.10]])
help([[Target : broadwell]])
help()
help([[OpenBLAS: An optimized BLAS library]])



prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/openblas-0.3.10-3y6j4zfwqc24lgbqxbdmzrwfijmosiov/bin", ":")
prepend_path("CPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/openblas-0.3.10-3y6j4zfwqc24lgbqxbdmzrwfijmosiov/include", ":")
prepend_path("LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/openblas-0.3.10-3y6j4zfwqc24lgbqxbdmzrwfijmosiov/lib", ":")
prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/openblas-0.3.10-3y6j4zfwqc24lgbqxbdmzrwfijmosiov/lib", ":")
prepend_path("PKG_CONFIG_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/openblas-0.3.10-3y6j4zfwqc24lgbqxbdmzrwfijmosiov/lib/pkgconfig", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/openblas-0.3.10-3y6j4zfwqc24lgbqxbdmzrwfijmosiov/.", ":")

