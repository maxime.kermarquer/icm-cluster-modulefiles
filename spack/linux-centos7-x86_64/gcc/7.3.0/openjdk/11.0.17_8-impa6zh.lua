-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2023-05-15 14:33:11.366464
--
-- openjdk@11.0.17_8%gcc@7.3.0 build_system=generic arch=linux-centos7-broadwell/impa6zh
--

whatis([[Name : openjdk]])
whatis([[Version : 11.0.17_8]])
whatis([[Target : broadwell]])
whatis([[Short description : The free and opensource java implementation]])

help([[Name   : openjdk]])
help([[Version: 11.0.17_8]])
help([[Target : broadwell]])
help()
help([[The free and opensource java implementation]])



prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/openjdk-11.0.17_8-impa6zhsex37mpldajplkcejmhxkcqox/bin", ":")
prepend_path("MANPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/openjdk-11.0.17_8-impa6zhsex37mpldajplkcejmhxkcqox/man", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/openjdk-11.0.17_8-impa6zhsex37mpldajplkcejmhxkcqox/.", ":")
setenv("JAVA_HOME", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/openjdk-11.0.17_8-impa6zhsex37mpldajplkcejmhxkcqox")

