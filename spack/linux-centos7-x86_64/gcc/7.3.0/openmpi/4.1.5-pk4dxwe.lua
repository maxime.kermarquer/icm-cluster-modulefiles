-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2023-05-15 15:28:27.882576
--
-- openmpi@4.1.5%gcc@7.3.0~atomics~cuda~cxx~cxx_exceptions~gpfs~internal-hwloc~java~legacylaunchers~lustre~memchecker~orterunprefix+romio+rsh~singularity+static+vt+wrapper-rpath build_system=autotools fabrics=none schedulers=none arch=linux-centos7-broadwell/pk4dxwe
--

whatis([[Name : openmpi]])
whatis([[Version : 4.1.5]])
whatis([[Target : broadwell]])
whatis([[Short description : An open source Message Passing Interface implementation.]])
whatis([[Configure options : --enable-shared --disable-silent-rules --disable-builtin-atomics --enable-static --enable-mpi1-compatibility --without-mxm --without-cma --without-ofi --without-hcoll --without-knem --without-verbs --without-xpmem --without-psm2 --without-ucx --without-psm --without-fca --without-cray-xpmem --without-alps --without-lsf --without-loadleveler --without-sge --without-slurm --without-tm --disable-memchecker --with-libevent=/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/libevent-2.1.12-de5q4gk7vzmojm3aqz5bkfeijm6ljpeo --with-pmix=/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/pmix-4.2.3-ourmnkswyplrtmptn6duobl3otnzxsgz --with-zlib=/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/zlib-1.2.13-7shounlfxhfmsqyucuh5nfp447lvnxn4 --with-hwloc=/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/hwloc-2.9.1-khjkbbdzxifpffz5nngs6fomtw2cwiws --disable-java --disable-mpi-java --with-gpfs=no --without-cuda --enable-wrapper-rpath --disable-wrapper-runpath --disable-mpi-cxx --disable-cxx-exceptions --with-wrapper-ldflags=-Wl,-rpath,/network/lustre/iss01/apps/software/linux-centos7-x86_64/gcc/4.8.5/gcc/7.3.0/tex4qcxfmj2ny4phn5bkoqu5c4q2kbse/lib/gcc/x86_64-pc-linux-gnu/7.3.0 -Wl,-rpath,/network/lustre/iss01/apps/software/linux-centos7-x86_64/gcc/4.8.5/gcc/7.3.0/tex4qcxfmj2ny4phn5bkoqu5c4q2kbse/lib64]])

help([[Name   : openmpi]])
help([[Version: 4.1.5]])
help([[Target : broadwell]])
help()
help([[An open source Message Passing Interface implementation. The Open MPI
Project is an open source Message Passing Interface implementation that
is developed and maintained by a consortium of academic, research, and
industry partners. Open MPI is therefore able to combine the expertise,
technologies, and resources from all across the High Performance
Computing community in order to build the best MPI library available.
Open MPI offers advantages for system and software vendors, application
developers and computer science researchers.]])

-- Services provided by the package
family("mpi")

-- Loading this module unlocks the path below unconditionally
prepend_path("MODULEPATH", "/network/lustre/iss01/apps/modules/spack/linux-centos7-x86_64/openmpi/4.1.5-pk4dxwe/gcc/7.3.0")

-- Try to load variables into path to see if providers are there

-- Change MODULEPATH based on the result of the tests above

-- Set variables to notify the provider of the new services
setenv("LMOD_MPI_NAME", "openmpi")
setenv("LMOD_MPI_VERSION", "4.1.5-pk4dxwe")

depends_on("linux-centos7-x86_64/gcc/7.3.0/hwloc/2.9.1-khjkbbd")
depends_on("linux-centos7-x86_64/gcc/7.3.0/numactl/2.0.14-toqbrvd")
depends_on("linux-centos7-x86_64/gcc/7.3.0/openssh/9.3p1-mnosany")
depends_on("linux-centos7-x86_64/gcc/7.3.0/pmix/4.2.3-ourmnks")
depends_on("linux-centos7-x86_64/gcc/7.3.0/zlib/1.2.13-7shounl")

prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/openmpi-4.1.5-pk4dxweks7pifv2nljvg2wm2bigonqu7/bin", ":")
prepend_path("CPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/openmpi-4.1.5-pk4dxweks7pifv2nljvg2wm2bigonqu7/include", ":")
prepend_path("LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/openmpi-4.1.5-pk4dxweks7pifv2nljvg2wm2bigonqu7/lib", ":")
prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/openmpi-4.1.5-pk4dxweks7pifv2nljvg2wm2bigonqu7/lib", ":")
prepend_path("MANPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/openmpi-4.1.5-pk4dxweks7pifv2nljvg2wm2bigonqu7/share/man", ":")
prepend_path("PKG_CONFIG_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/openmpi-4.1.5-pk4dxweks7pifv2nljvg2wm2bigonqu7/lib/pkgconfig", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/openmpi-4.1.5-pk4dxweks7pifv2nljvg2wm2bigonqu7/.", ":")
setenv("MPICC", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/openmpi-4.1.5-pk4dxweks7pifv2nljvg2wm2bigonqu7/bin/mpicc")
setenv("MPICXX", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/openmpi-4.1.5-pk4dxweks7pifv2nljvg2wm2bigonqu7/bin/mpic++")
setenv("MPIF77", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/openmpi-4.1.5-pk4dxweks7pifv2nljvg2wm2bigonqu7/bin/mpif77")
setenv("MPIF90", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/openmpi-4.1.5-pk4dxweks7pifv2nljvg2wm2bigonqu7/bin/mpif90")

