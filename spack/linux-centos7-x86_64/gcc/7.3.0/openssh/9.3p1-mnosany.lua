-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2023-05-15 15:03:51.991702
--
-- openssh@9.3p1%gcc@7.3.0+gssapi build_system=autotools arch=linux-centos7-broadwell/mnosany
--

whatis([[Name : openssh]])
whatis([[Version : 9.3p1]])
whatis([[Target : broadwell]])
whatis([[Short description : OpenSSH is the premier connectivity tool for remote login with the SSH protocol. It encrypts all traffic to eliminate eavesdropping, connection hijacking, and other attacks. In addition, OpenSSH provides a large suite of secure tunneling capabilities, several authentication methods, and sophisticated configuration options. ]])
whatis([[Configure options : --with-privsep-path=/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/openssh-9.3p1-mnosanyep2eowjqs65tbuj6htt323owg/var/empty --with-kerberos5=/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/krb5-1.20.1-tuuyuscpjisyiq43roatbza2vk4g5vqn]])

help([[Name   : openssh]])
help([[Version: 9.3p1]])
help([[Target : broadwell]])
help()
help([[OpenSSH is the premier connectivity tool for remote login with the SSH
protocol. It encrypts all traffic to eliminate eavesdropping, connection
hijacking, and other attacks. In addition, OpenSSH provides a large
suite of secure tunneling capabilities, several authentication methods,
and sophisticated configuration options.]])


depends_on("krb5/1.20.1-tuuyusc")
depends_on("libedit/3.1-20210216-toczbpf")
depends_on("libxcrypt/4.4.33-v3yo2l4")
depends_on("ncurses/6.4-6n6u7qv")
depends_on("openssl/1.1.1t-f5klqzu")
depends_on("zlib/1.2.13-7shounl")

prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/openssh-9.3p1-mnosanyep2eowjqs65tbuj6htt323owg/bin", ":")
prepend_path("MANPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/openssh-9.3p1-mnosanyep2eowjqs65tbuj6htt323owg/share/man", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/openssh-9.3p1-mnosanyep2eowjqs65tbuj6htt323owg/.", ":")

