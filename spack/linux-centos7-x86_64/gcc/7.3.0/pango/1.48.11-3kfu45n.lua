-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2023-05-23 16:15:28.714074
--
-- pango@1.48.11%gcc@7.3.0~X~strip build_system=meson buildtype=debugoptimized default_library=shared arch=linux-centos7-broadwell/3kfu45n
--

whatis([[Name : pango]])
whatis([[Version : 1.48.11]])
whatis([[Target : broadwell]])
whatis([[Short description : Pango is a library for laying out and rendering of text, with an emphasis on internationalization. It can be used anywhere that text layout is needed, though most of the work on Pango so far has been done in the context of the GTK+ widget toolkit.]])

help([[Name   : pango]])
help([[Version: 1.48.11]])
help([[Target : broadwell]])
help()
help([[Pango is a library for laying out and rendering of text, with an
emphasis on internationalization. It can be used anywhere that text
layout is needed, though most of the work on Pango so far has been done
in the context of the GTK+ widget toolkit.]])


depends_on("linux-centos7-x86_64/gcc/7.3.0/cairo/1.16.0-g7r3f6p")
depends_on("linux-centos7-x86_64/gcc/7.3.0/fontconfig/2.14.2-pnelxju")
depends_on("linux-centos7-x86_64/gcc/7.3.0/freetype/2.11.1-5stkwjm")
depends_on("linux-centos7-x86_64/gcc/7.3.0/fribidi/1.0.12-yfu2ixq")
depends_on("linux-centos7-x86_64/gcc/7.3.0/glib/2.76.1-webav7s")
depends_on("linux-centos7-x86_64/gcc/7.3.0/gobject-introspection/1.72.1-hxu4bzn")
depends_on("linux-centos7-x86_64/gcc/7.3.0/harfbuzz/5.3.1-lb6qjp6")
depends_on("linux-centos7-x86_64/gcc/7.3.0/libffi/3.3-bbd3for")

prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/pango-1.48.11-3kfu45n4emdjvltjqpmq7ikaq3jbt24z/bin", ":")
prepend_path("PKG_CONFIG_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/pango-1.48.11-3kfu45n4emdjvltjqpmq7ikaq3jbt24z/lib/pkgconfig", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/pango-1.48.11-3kfu45n4emdjvltjqpmq7ikaq3jbt24z/.", ":")
prepend_path("XDG_DATA_DIRS", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/gobject-introspection-1.72.1-hxu4bzn4ko7cxpwgjld2mm6yqnxwtojo/share", ":")
prepend_path("GI_TYPELIB_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/gobject-introspection-1.72.1-hxu4bzn4ko7cxpwgjld2mm6yqnxwtojo/lib/girepository-1.0", ":")
prepend_path("XDG_DATA_DIRS", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/harfbuzz-5.3.1-lb6qjp6723kuxl6kwudvipgeynmsbz3j/share", ":")
prepend_path("GI_TYPELIB_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/harfbuzz-5.3.1-lb6qjp6723kuxl6kwudvipgeynmsbz3j/lib/girepository-1.0", ":")
prepend_path("GI_TYPELIB_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/pango-1.48.11-3kfu45n4emdjvltjqpmq7ikaq3jbt24z/lib/girepository-1.0", ":")

