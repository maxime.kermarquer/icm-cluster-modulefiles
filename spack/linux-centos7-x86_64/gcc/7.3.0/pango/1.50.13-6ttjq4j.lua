-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2023-05-15 14:53:09.188269
--
-- pango@1.50.13%gcc@7.3.0+X~strip build_system=meson buildtype=debugoptimized default_library=shared arch=linux-centos7-broadwell/6ttjq4j
--

whatis([[Name : pango]])
whatis([[Version : 1.50.13]])
whatis([[Target : broadwell]])
whatis([[Short description : Pango is a library for laying out and rendering of text, with an emphasis on internationalization. It can be used anywhere that text layout is needed, though most of the work on Pango so far has been done in the context of the GTK+ widget toolkit.]])

help([[Name   : pango]])
help([[Version: 1.50.13]])
help([[Target : broadwell]])
help()
help([[Pango is a library for laying out and rendering of text, with an
emphasis on internationalization. It can be used anywhere that text
layout is needed, though most of the work on Pango so far has been done
in the context of the GTK+ widget toolkit.]])


depends_on("linux-centos7-x86_64/gcc/7.3.0/cairo/1.16.0-rfsizbl")
depends_on("linux-centos7-x86_64/gcc/7.3.0/fontconfig/2.14.2-pnelxju")
depends_on("linux-centos7-x86_64/gcc/7.3.0/freetype/2.11.1-5stkwjm")
depends_on("linux-centos7-x86_64/gcc/7.3.0/fribidi/1.0.12-yfu2ixq")
depends_on("linux-centos7-x86_64/gcc/7.3.0/glib/2.76.1-webav7s")
depends_on("linux-centos7-x86_64/gcc/7.3.0/gobject-introspection/1.72.1-i7b7xv5")
depends_on("linux-centos7-x86_64/gcc/7.3.0/harfbuzz/5.3.1-obk64wt")
depends_on("linux-centos7-x86_64/gcc/7.3.0/json-glib/1.6.6-jgfqswi")
depends_on("linux-centos7-x86_64/gcc/7.3.0/libffi/3.3-bbd3for")
depends_on("linux-centos7-x86_64/gcc/7.3.0/libxft/2.3.2-up2gzhf")

prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/pango-1.50.13-6ttjq4j7oomi6oxobumn5z65osghuduh/bin", ":")
prepend_path("CPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/pango-1.50.13-6ttjq4j7oomi6oxobumn5z65osghuduh/include", ":")
prepend_path("LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/pango-1.50.13-6ttjq4j7oomi6oxobumn5z65osghuduh/lib", ":")
prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/pango-1.50.13-6ttjq4j7oomi6oxobumn5z65osghuduh/lib", ":")
prepend_path("PKG_CONFIG_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/pango-1.50.13-6ttjq4j7oomi6oxobumn5z65osghuduh/lib/pkgconfig", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/pango-1.50.13-6ttjq4j7oomi6oxobumn5z65osghuduh/.", ":")
prepend_path("XLOCALEDIR", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/libx11-1.8.4-mygw7c45hwjfojavmnf2f3qnjb3bugq4/share/X11/locale", ":")
prepend_path("XDG_DATA_DIRS", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/gobject-introspection-1.72.1-i7b7xv5y2xgpj237dvp2otbo67ex6l6d/share", ":")
prepend_path("GI_TYPELIB_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/gobject-introspection-1.72.1-i7b7xv5y2xgpj237dvp2otbo67ex6l6d/lib/girepository-1.0", ":")
prepend_path("XDG_DATA_DIRS", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/harfbuzz-5.3.1-obk64wt5uophjhoeg753nspxanxuqet2/share", ":")
prepend_path("GI_TYPELIB_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/harfbuzz-5.3.1-obk64wt5uophjhoeg753nspxanxuqet2/lib/girepository-1.0", ":")
prepend_path("GI_TYPELIB_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/pango-1.50.13-6ttjq4j7oomi6oxobumn5z65osghuduh/lib/girepository-1.0", ":")

