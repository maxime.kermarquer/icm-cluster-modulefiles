-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2023-05-12 15:51:29.704184
--
-- patchelf@0.17.2%gcc@7.3.0 build_system=autotools arch=linux-centos7-broadwell/2fmkbke
--

whatis([[Name : patchelf]])
whatis([[Version : 0.17.2]])
whatis([[Target : broadwell]])
whatis([[Short description : PatchELF is a small utility to modify the dynamic linker and RPATH of ELF executables.]])

help([[Name   : patchelf]])
help([[Version: 0.17.2]])
help([[Target : broadwell]])
help()
help([[PatchELF is a small utility to modify the dynamic linker and RPATH of
ELF executables.]])



prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/patchelf-0.17.2-2fmkbkewb6ez7wohuveievekjn6gdu7r/bin", ":")
prepend_path("MANPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/patchelf-0.17.2-2fmkbkewb6ez7wohuveievekjn6gdu7r/share/man", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/patchelf-0.17.2-2fmkbkewb6ez7wohuveievekjn6gdu7r/.", ":")

