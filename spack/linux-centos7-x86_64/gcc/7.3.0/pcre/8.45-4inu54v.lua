-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2023-05-09 17:26:22.375635
--
-- pcre@8.45%gcc@7.3.0~jit+multibyte+utf build_system=autotools arch=linux-centos7-broadwell/4inu54v
--

whatis([[Name : pcre]])
whatis([[Version : 8.45]])
whatis([[Target : broadwell]])
whatis([[Short description : The PCRE package contains Perl Compatible Regular Expression libraries. These are useful for implementing regular expression pattern matching using the same syntax and semantics as Perl 5.]])
whatis([[Configure options : --enable-pcre16 --enable-pcre32 --enable-utf --enable-unicode-properties]])

help([[Name   : pcre]])
help([[Version: 8.45]])
help([[Target : broadwell]])
help()
help([[The PCRE package contains Perl Compatible Regular Expression libraries.
These are useful for implementing regular expression pattern matching
using the same syntax and semantics as Perl 5.]])



prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/pcre-8.45-4inu54v3q74ddagvtaah7oquiawvos6c/bin", ":")
prepend_path("MANPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/pcre-8.45-4inu54v3q74ddagvtaah7oquiawvos6c/share/man", ":")
prepend_path("PKG_CONFIG_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/pcre-8.45-4inu54v3q74ddagvtaah7oquiawvos6c/lib/pkgconfig", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/pcre-8.45-4inu54v3q74ddagvtaah7oquiawvos6c/.", ":")

