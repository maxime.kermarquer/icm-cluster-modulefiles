-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2023-05-10 09:32:39.827908
--
-- perl-data-dumper@2.173%gcc@7.3.0 build_system=perl arch=linux-centos7-broadwell/6qyetku
--

whatis([[Name : perl-data-dumper]])
whatis([[Version : 2.173]])
whatis([[Target : broadwell]])
whatis([[Short description : Stringified perl data structures, suitable for both printing and eval]])

help([[Name   : perl-data-dumper]])
help([[Version: 2.173]])
help([[Target : broadwell]])
help()
help([[Stringified perl data structures, suitable for both printing and eval]])


depends_on("perl/5.36.0-3zk6frf")

prepend_path("MANPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/perl-data-dumper-2.173-6qyetkupmfbge6juihoc26aje5og3gqv/man", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/perl-data-dumper-2.173-6qyetkupmfbge6juihoc26aje5og3gqv/.", ":")
prepend_path("PERL5LIB", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/perl-data-dumper-2.173-6qyetkupmfbge6juihoc26aje5og3gqv/lib/perl5", ":")

