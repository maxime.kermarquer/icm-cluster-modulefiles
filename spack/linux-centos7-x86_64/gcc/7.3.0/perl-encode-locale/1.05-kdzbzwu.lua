-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2023-05-10 09:32:43.240577
--
-- perl-encode-locale@1.05%gcc@7.3.0 build_system=perl arch=linux-centos7-broadwell/kdzbzwu
--

whatis([[Name : perl-encode-locale]])
whatis([[Version : 1.05]])
whatis([[Target : broadwell]])
whatis([[Short description : Determine the locale encoding]])

help([[Name   : perl-encode-locale]])
help([[Version: 1.05]])
help([[Target : broadwell]])
help()
help([[Determine the locale encoding]])


depends_on("perl/5.36.0-3zk6frf")

prepend_path("MANPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/perl-encode-locale-1.05-kdzbzwuv2vw7amsmzllz4ijp7mkkzpyg/man", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/perl-encode-locale-1.05-kdzbzwuv2vw7amsmzllz4ijp7mkkzpyg/.", ":")
prepend_path("PERL5LIB", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/perl-encode-locale-1.05-kdzbzwuv2vw7amsmzllz4ijp7mkkzpyg/lib/perl5", ":")

