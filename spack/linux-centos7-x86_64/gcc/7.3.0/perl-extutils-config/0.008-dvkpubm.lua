-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2023-05-10 09:32:46.862472
--
-- perl-extutils-config@0.008%gcc@7.3.0 build_system=perl arch=linux-centos7-broadwell/dvkpubm
--

whatis([[Name : perl-extutils-config]])
whatis([[Version : 0.008]])
whatis([[Target : broadwell]])
whatis([[Short description : ExtUtils::Config - A wrapper for perl's configuration]])

help([[Name   : perl-extutils-config]])
help([[Version: 0.008]])
help([[Target : broadwell]])
help()
help([[ExtUtils::Config - A wrapper for perl's configuration]])


depends_on("perl/5.36.0-3zk6frf")

prepend_path("MANPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/perl-extutils-config-0.008-dvkpubmase4eynqwadztdvwihn5ro4wv/man", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/perl-extutils-config-0.008-dvkpubmase4eynqwadztdvwihn5ro4wv/.", ":")
prepend_path("PERL5LIB", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/perl-extutils-config-0.008-dvkpubmase4eynqwadztdvwihn5ro4wv/lib/perl5", ":")

