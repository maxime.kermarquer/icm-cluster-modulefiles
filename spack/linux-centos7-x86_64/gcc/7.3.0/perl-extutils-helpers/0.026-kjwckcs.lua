-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2023-05-10 09:32:55.061610
--
-- perl-extutils-helpers@0.026%gcc@7.3.0 build_system=perl arch=linux-centos7-broadwell/kjwckcs
--

whatis([[Name : perl-extutils-helpers]])
whatis([[Version : 0.026]])
whatis([[Target : broadwell]])
whatis([[Short description : ExtUtils::Helpers - Various portability utilities for module builders]])

help([[Name   : perl-extutils-helpers]])
help([[Version: 0.026]])
help([[Target : broadwell]])
help()
help([[ExtUtils::Helpers - Various portability utilities for module builders]])


depends_on("perl/5.36.0-3zk6frf")

prepend_path("MANPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/perl-extutils-helpers-0.026-kjwckcs7qfmf7wtpcu5u6tabmrfbeqf3/man", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/perl-extutils-helpers-0.026-kjwckcs7qfmf7wtpcu5u6tabmrfbeqf3/.", ":")
prepend_path("PERL5LIB", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/perl-extutils-helpers-0.026-kjwckcs7qfmf7wtpcu5u6tabmrfbeqf3/lib/perl5", ":")

