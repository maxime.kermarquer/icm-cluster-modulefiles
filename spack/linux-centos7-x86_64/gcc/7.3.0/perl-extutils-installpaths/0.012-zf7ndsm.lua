-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2023-05-10 09:36:26.956953
--
-- perl-extutils-installpaths@0.012%gcc@7.3.0 build_system=perl arch=linux-centos7-broadwell/zf7ndsm
--

whatis([[Name : perl-extutils-installpaths]])
whatis([[Version : 0.012]])
whatis([[Target : broadwell]])
whatis([[Short description : ExtUtils::InstallPaths - Build.PL install path logic made easy]])

help([[Name   : perl-extutils-installpaths]])
help([[Version: 0.012]])
help([[Target : broadwell]])
help()
help([[ExtUtils::InstallPaths - Build.PL install path logic made easy]])


depends_on("perl/5.36.0-3zk6frf")
depends_on("perl-extutils-config/0.008-dvkpubm")

prepend_path("MANPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/perl-extutils-installpaths-0.012-zf7ndsmprlsqvc3srkwqqouxetirgunv/man", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/perl-extutils-installpaths-0.012-zf7ndsmprlsqvc3srkwqqouxetirgunv/.", ":")
prepend_path("PERL5LIB", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/perl-extutils-installpaths-0.012-zf7ndsmprlsqvc3srkwqqouxetirgunv/lib/perl5:/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/perl-extutils-config-0.008-dvkpubmase4eynqwadztdvwihn5ro4wv/lib/perl5", ":")

