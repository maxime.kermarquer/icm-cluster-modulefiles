-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2023-05-10 09:36:23.343236
--
-- perl-file-listing@6.04%gcc@7.3.0 build_system=perl arch=linux-centos7-broadwell/zdbqlu3
--

whatis([[Name : perl-file-listing]])
whatis([[Version : 6.04]])
whatis([[Target : broadwell]])
whatis([[Short description : Parse directory listing]])

help([[Name   : perl-file-listing]])
help([[Version: 6.04]])
help([[Target : broadwell]])
help()
help([[Parse directory listing]])


depends_on("perl/5.36.0-3zk6frf")
depends_on("perl-http-date/6.02-xq5uttz")

prepend_path("MANPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/perl-file-listing-6.04-zdbqlu33ufo7zixvzz2y4rv24vuscyrw/man", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/perl-file-listing-6.04-zdbqlu33ufo7zixvzz2y4rv24vuscyrw/.", ":")
prepend_path("PERL5LIB", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/perl-file-listing-6.04-zdbqlu33ufo7zixvzz2y4rv24vuscyrw/lib/perl5:/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/perl-http-date-6.02-xq5uttz7gbyl52dilcvyyoxz4u2ntimq/lib/perl5", ":")

