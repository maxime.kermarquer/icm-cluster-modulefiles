-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2023-05-10 09:36:37.878002
--
-- perl-html-parser@3.72%gcc@7.3.0 build_system=perl arch=linux-centos7-broadwell/mojrncz
--

whatis([[Name : perl-html-parser]])
whatis([[Version : 3.72]])
whatis([[Target : broadwell]])
whatis([[Short description : HTML parser class]])

help([[Name   : perl-html-parser]])
help([[Version: 3.72]])
help([[Target : broadwell]])
help()
help([[HTML parser class]])


depends_on("perl/5.36.0-3zk6frf")
depends_on("perl-html-tagset/3.20-gyn6cl7")

prepend_path("MANPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/perl-html-parser-3.72-mojrnczxyxsej62keodnctzyfrgp54ry/man", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/perl-html-parser-3.72-mojrnczxyxsej62keodnctzyfrgp54ry/.", ":")
prepend_path("PERL5LIB", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/perl-html-parser-3.72-mojrnczxyxsej62keodnctzyfrgp54ry/lib/perl5:/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/perl-html-tagset-3.20-gyn6cl7dyopqkowyybb5kobmhvhxreza/lib/perl5", ":")

