-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2023-05-10 09:32:58.601080
--
-- perl-html-tagset@3.20%gcc@7.3.0 build_system=perl arch=linux-centos7-broadwell/gyn6cl7
--

whatis([[Name : perl-html-tagset]])
whatis([[Version : 3.20]])
whatis([[Target : broadwell]])
whatis([[Short description : Data tables useful in parsing HTML]])

help([[Name   : perl-html-tagset]])
help([[Version: 3.20]])
help([[Target : broadwell]])
help()
help([[Data tables useful in parsing HTML]])


depends_on("perl/5.36.0-3zk6frf")

prepend_path("MANPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/perl-html-tagset-3.20-gyn6cl7dyopqkowyybb5kobmhvhxreza/man", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/perl-html-tagset-3.20-gyn6cl7dyopqkowyybb5kobmhvhxreza/.", ":")
prepend_path("PERL5LIB", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/perl-html-tagset-3.20-gyn6cl7dyopqkowyybb5kobmhvhxreza/lib/perl5", ":")

