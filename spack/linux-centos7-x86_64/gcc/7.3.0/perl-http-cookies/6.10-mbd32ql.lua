-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2023-05-11 11:42:22.518560
--
-- perl-http-cookies@6.10%gcc@7.3.0 build_system=perl arch=linux-centos7-broadwell/mbd32ql
--

whatis([[Name : perl-http-cookies]])
whatis([[Version : 6.10]])
whatis([[Target : broadwell]])
whatis([[Short description : HTTP cookie jars]])

help([[Name   : perl-http-cookies]])
help([[Version: 6.10]])
help([[Target : broadwell]])
help()
help([[HTTP cookie jars]])


depends_on("perl/5.36.0-3zk6frf")
depends_on("perl-http-message/6.44-3ggggxv")
depends_on("perl-uri/5.08-2syiw7i")

prepend_path("MANPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/perl-http-cookies-6.10-mbd32qltoa63mxqqh5nbu4prh4hi3aku/man", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/perl-http-cookies-6.10-mbd32qltoa63mxqqh5nbu4prh4hi3aku/.", ":")
prepend_path("PERL5LIB", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/perl-http-cookies-6.10-mbd32qltoa63mxqqh5nbu4prh4hi3aku/lib/perl5:/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/perl-http-message-6.44-3ggggxvejtroez42q2dg7zmb7bebxu46/lib/perl5:/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/perl-encode-locale-1.05-kdzbzwuv2vw7amsmzllz4ijp7mkkzpyg/lib/perl5:/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/perl-http-date-6.02-xq5uttz7gbyl52dilcvyyoxz4u2ntimq/lib/perl5:/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/perl-io-html-1.004-lalftdg7t5fxtdyovznqcz5bic7gfk4x/lib/perl5:/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/perl-lwp-mediatypes-6.02-lzijfu6etngjbkshdqz4fp2d323t435e/lib/perl5:/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/perl-try-tiny-0.31-6aqsf3xidxct3g5yj43ugnfxy3trh3ws/lib/perl5:/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/perl-uri-5.08-2syiw7iefezddbbotw67rbpn27qyyt6d/lib/perl5", ":")

