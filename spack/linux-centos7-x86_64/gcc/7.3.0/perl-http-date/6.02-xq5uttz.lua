-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2023-05-10 09:32:27.197875
--
-- perl-http-date@6.02%gcc@7.3.0 build_system=perl arch=linux-centos7-broadwell/xq5uttz
--

whatis([[Name : perl-http-date]])
whatis([[Version : 6.02]])
whatis([[Target : broadwell]])
whatis([[Short description : Date conversion routines]])

help([[Name   : perl-http-date]])
help([[Version: 6.02]])
help([[Target : broadwell]])
help()
help([[Date conversion routines]])


depends_on("perl/5.36.0-3zk6frf")

prepend_path("MANPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/perl-http-date-6.02-xq5uttz7gbyl52dilcvyyoxz4u2ntimq/man", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/perl-http-date-6.02-xq5uttz7gbyl52dilcvyyoxz4u2ntimq/.", ":")
prepend_path("PERL5LIB", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/perl-http-date-6.02-xq5uttz7gbyl52dilcvyyoxz4u2ntimq/lib/perl5", ":")

