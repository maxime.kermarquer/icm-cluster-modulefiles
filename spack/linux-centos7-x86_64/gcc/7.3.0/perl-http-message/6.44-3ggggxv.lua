-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2023-05-10 10:37:03.021711
--
-- perl-http-message@6.44%gcc@7.3.0 build_system=perl arch=linux-centos7-broadwell/3ggggxv
--

whatis([[Name : perl-http-message]])
whatis([[Version : 6.44]])
whatis([[Target : broadwell]])
whatis([[Short description : HTTP style message (base class)]])

help([[Name   : perl-http-message]])
help([[Version: 6.44]])
help([[Target : broadwell]])
help()
help([[HTTP style message (base class)]])


depends_on("perl/5.36.0-3zk6frf")
depends_on("perl-encode-locale/1.05-kdzbzwu")
depends_on("perl-http-date/6.02-xq5uttz")
depends_on("perl-io-html/1.004-lalftdg")
depends_on("perl-lwp-mediatypes/6.02-lzijfu6")
depends_on("perl-try-tiny/0.31-6aqsf3x")
depends_on("perl-uri/5.08-2syiw7i")

prepend_path("MANPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/perl-http-message-6.44-3ggggxvejtroez42q2dg7zmb7bebxu46/man", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/perl-http-message-6.44-3ggggxvejtroez42q2dg7zmb7bebxu46/.", ":")
prepend_path("PERL5LIB", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/perl-http-message-6.44-3ggggxvejtroez42q2dg7zmb7bebxu46/lib/perl5:/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/perl-encode-locale-1.05-kdzbzwuv2vw7amsmzllz4ijp7mkkzpyg/lib/perl5:/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/perl-http-date-6.02-xq5uttz7gbyl52dilcvyyoxz4u2ntimq/lib/perl5:/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/perl-io-html-1.004-lalftdg7t5fxtdyovznqcz5bic7gfk4x/lib/perl5:/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/perl-lwp-mediatypes-6.02-lzijfu6etngjbkshdqz4fp2d323t435e/lib/perl5:/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/perl-try-tiny-0.31-6aqsf3xidxct3g5yj43ugnfxy3trh3ws/lib/perl5:/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/perl-uri-5.08-2syiw7iefezddbbotw67rbpn27qyyt6d/lib/perl5", ":")

