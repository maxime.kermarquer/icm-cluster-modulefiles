-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2023-05-10 09:32:35.798190
--
-- perl-io-html@1.004%gcc@7.3.0 build_system=perl arch=linux-centos7-broadwell/lalftdg
--

whatis([[Name : perl-io-html]])
whatis([[Version : 1.004]])
whatis([[Target : broadwell]])
whatis([[Short description : Open an HTML file with automatic charset detection.]])

help([[Name   : perl-io-html]])
help([[Version: 1.004]])
help([[Target : broadwell]])
help()
help([[Open an HTML file with automatic charset detection.]])


depends_on("perl/5.36.0-3zk6frf")

prepend_path("MANPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/perl-io-html-1.004-lalftdg7t5fxtdyovznqcz5bic7gfk4x/man", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/perl-io-html-1.004-lalftdg7t5fxtdyovznqcz5bic7gfk4x/.", ":")
prepend_path("PERL5LIB", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/perl-io-html-1.004-lalftdg7t5fxtdyovznqcz5bic7gfk4x/lib/perl5", ":")

