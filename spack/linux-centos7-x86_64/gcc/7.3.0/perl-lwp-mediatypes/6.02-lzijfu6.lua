-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2023-05-10 09:33:02.188709
--
-- perl-lwp-mediatypes@6.02%gcc@7.3.0 build_system=perl arch=linux-centos7-broadwell/lzijfu6
--

whatis([[Name : perl-lwp-mediatypes]])
whatis([[Version : 6.02]])
whatis([[Target : broadwell]])
whatis([[Short description : Guess media type for a file or a URL]])

help([[Name   : perl-lwp-mediatypes]])
help([[Version: 6.02]])
help([[Target : broadwell]])
help()
help([[Guess media type for a file or a URL]])


depends_on("perl/5.36.0-3zk6frf")

prepend_path("MANPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/perl-lwp-mediatypes-6.02-lzijfu6etngjbkshdqz4fp2d323t435e/man", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/perl-lwp-mediatypes-6.02-lzijfu6etngjbkshdqz4fp2d323t435e/.", ":")
prepend_path("PERL5LIB", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/perl-lwp-mediatypes-6.02-lzijfu6etngjbkshdqz4fp2d323t435e/lib/perl5", ":")

