-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2023-05-11 11:42:11.743362
--
-- perl-module-build-tiny@0.039%gcc@7.3.0 build_system=perl arch=linux-centos7-broadwell/x6ae4hl
--

whatis([[Name : perl-module-build-tiny]])
whatis([[Version : 0.039]])
whatis([[Target : broadwell]])
whatis([[Short description : Module::Build::Tiny - A tiny replacement for Module::Build]])

help([[Name   : perl-module-build-tiny]])
help([[Version: 0.039]])
help([[Target : broadwell]])
help()
help([[Module::Build::Tiny - A tiny replacement for Module::Build]])


depends_on("perl/5.36.0-3zk6frf")
depends_on("perl-extutils-config/0.008-dvkpubm")
depends_on("perl-extutils-helpers/0.026-kjwckcs")
depends_on("perl-extutils-installpaths/0.012-zf7ndsm")

prepend_path("MANPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/perl-module-build-tiny-0.039-x6ae4hl2xq7xcuk63vtt3aaiw6eqzmck/man", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/perl-module-build-tiny-0.039-x6ae4hl2xq7xcuk63vtt3aaiw6eqzmck/.", ":")
prepend_path("PERL5LIB", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/perl-module-build-tiny-0.039-x6ae4hl2xq7xcuk63vtt3aaiw6eqzmck/lib/perl5:/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/perl-extutils-config-0.008-dvkpubmase4eynqwadztdvwihn5ro4wv/lib/perl5:/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/perl-extutils-helpers-0.026-kjwckcs7qfmf7wtpcu5u6tabmrfbeqf3/lib/perl5:/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/perl-extutils-installpaths-0.012-zf7ndsmprlsqvc3srkwqqouxetirgunv/lib/perl5", ":")

