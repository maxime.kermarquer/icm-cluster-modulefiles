-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2023-05-10 09:33:07.492825
--
-- perl-module-build@0.4232%gcc@7.3.0 build_system=perl arch=linux-centos7-broadwell/awwxymc
--

whatis([[Name : perl-module-build]])
whatis([[Version : 0.4232]])
whatis([[Target : broadwell]])
whatis([[Short description : Module::Build is a system for building, testing, and installing Perl modules. It is meant to be an alternative to ExtUtils::MakeMaker. Developers may alter the behavior of the module through subclassing in a much more straightforward way than with MakeMaker. It also does not require a make on your system - most of the Module::Build code is pure-perl and written in a very cross-platform way. ]])

help([[Name   : perl-module-build]])
help([[Version: 0.4232]])
help([[Target : broadwell]])
help()
help([[Module::Build is a system for building, testing, and installing Perl
modules. It is meant to be an alternative to ExtUtils::MakeMaker.
Developers may alter the behavior of the module through subclassing in a
much more straightforward way than with MakeMaker. It also does not
require a make on your system - most of the Module::Build code is pure-
perl and written in a very cross-platform way.]])


depends_on("perl/5.36.0-3zk6frf")

prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/perl-module-build-0.4232-awwxymcqn4fwqhwb7s6cqggxkryodfa4/bin", ":")
prepend_path("MANPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/perl-module-build-0.4232-awwxymcqn4fwqhwb7s6cqggxkryodfa4/man", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/perl-module-build-0.4232-awwxymcqn4fwqhwb7s6cqggxkryodfa4/.", ":")
prepend_path("PERL5LIB", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/perl-module-build-0.4232-awwxymcqn4fwqhwb7s6cqggxkryodfa4/lib/perl5", ":")

