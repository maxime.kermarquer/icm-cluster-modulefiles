-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2023-05-10 10:36:57.247343
--
-- perl-net-http@6.22%gcc@7.3.0 build_system=perl arch=linux-centos7-broadwell/ibhe4ny
--

whatis([[Name : perl-net-http]])
whatis([[Version : 6.22]])
whatis([[Target : broadwell]])
whatis([[Short description : Low-level HTTP connection (client)]])

help([[Name   : perl-net-http]])
help([[Version: 6.22]])
help([[Target : broadwell]])
help()
help([[Low-level HTTP connection (client)]])


depends_on("perl/5.36.0-3zk6frf")
depends_on("perl-uri/5.08-2syiw7i")

prepend_path("MANPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/perl-net-http-6.22-ibhe4nyr2prd636mqtlub55iamk5wizj/man", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/perl-net-http-6.22-ibhe4nyr2prd636mqtlub55iamk5wizj/.", ":")
prepend_path("PERL5LIB", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/perl-net-http-6.22-ibhe4nyr2prd636mqtlub55iamk5wizj/lib/perl5:/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/perl-uri-5.08-2syiw7iefezddbbotw67rbpn27qyyt6d/lib/perl5", ":")

