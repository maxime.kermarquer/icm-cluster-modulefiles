-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2023-05-10 09:32:51.396192
--
-- perl-test-needs@0.002010%gcc@7.3.0 build_system=perl arch=linux-centos7-broadwell/pe2ruzo
--

whatis([[Name : perl-test-needs]])
whatis([[Version : 0.002010]])
whatis([[Target : broadwell]])
whatis([[Short description : Skip tests when modules not available.]])

help([[Name   : perl-test-needs]])
help([[Version: 0.002010]])
help([[Target : broadwell]])
help()
help([[Skip tests when modules not available.]])


depends_on("perl/5.36.0-3zk6frf")

prepend_path("MANPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/perl-test-needs-0.002010-pe2ruzod5ygr5r7d32mavg75lz6sqoqa/man", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/perl-test-needs-0.002010-pe2ruzod5ygr5r7d32mavg75lz6sqoqa/.", ":")
prepend_path("PERL5LIB", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/perl-test-needs-0.002010-pe2ruzod5ygr5r7d32mavg75lz6sqoqa/lib/perl5", ":")

