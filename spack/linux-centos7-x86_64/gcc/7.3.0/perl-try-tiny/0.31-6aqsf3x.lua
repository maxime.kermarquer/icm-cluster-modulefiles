-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2023-05-10 09:32:31.566668
--
-- perl-try-tiny@0.31%gcc@7.3.0 build_system=perl arch=linux-centos7-broadwell/6aqsf3x
--

whatis([[Name : perl-try-tiny]])
whatis([[Version : 0.31]])
whatis([[Target : broadwell]])
whatis([[Short description : Minimal try/catch with proper preservation of $@]])

help([[Name   : perl-try-tiny]])
help([[Version: 0.31]])
help([[Target : broadwell]])
help()
help([[Minimal try/catch with proper preservation of $@]])


depends_on("perl/5.36.0-3zk6frf")

prepend_path("MANPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/perl-try-tiny-0.31-6aqsf3xidxct3g5yj43ugnfxy3trh3ws/man", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/perl-try-tiny-0.31-6aqsf3xidxct3g5yj43ugnfxy3trh3ws/.", ":")
prepend_path("PERL5LIB", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/perl-try-tiny-0.31-6aqsf3xidxct3g5yj43ugnfxy3trh3ws/lib/perl5", ":")

