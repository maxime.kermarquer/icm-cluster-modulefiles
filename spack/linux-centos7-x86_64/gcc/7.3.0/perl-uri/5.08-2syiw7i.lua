-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2023-05-10 09:36:31.600843
--
-- perl-uri@5.08%gcc@7.3.0 build_system=perl arch=linux-centos7-broadwell/2syiw7i
--

whatis([[Name : perl-uri]])
whatis([[Version : 5.08]])
whatis([[Target : broadwell]])
whatis([[Short description : Uniform Resource Identifiers (absolute and relative)]])

help([[Name   : perl-uri]])
help([[Version: 5.08]])
help([[Target : broadwell]])
help()
help([[Uniform Resource Identifiers (absolute and relative)]])


depends_on("perl/5.36.0-3zk6frf")

prepend_path("MANPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/perl-uri-5.08-2syiw7iefezddbbotw67rbpn27qyyt6d/man", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/perl-uri-5.08-2syiw7iefezddbbotw67rbpn27qyyt6d/.", ":")
prepend_path("PERL5LIB", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/perl-uri-5.08-2syiw7iefezddbbotw67rbpn27qyyt6d/lib/perl5", ":")

