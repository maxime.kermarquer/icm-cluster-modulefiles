-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2023-05-10 10:36:52.415938
--
-- perl-www-robotrules@6.02%gcc@7.3.0 build_system=perl arch=linux-centos7-broadwell/mv4ll2l
--

whatis([[Name : perl-www-robotrules]])
whatis([[Version : 6.02]])
whatis([[Target : broadwell]])
whatis([[Short description : Database of robots.txt-derived permissions]])

help([[Name   : perl-www-robotrules]])
help([[Version: 6.02]])
help([[Target : broadwell]])
help()
help([[Database of robots.txt-derived permissions]])


depends_on("perl/5.36.0-3zk6frf")
depends_on("perl-uri/5.08-2syiw7i")

prepend_path("MANPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/perl-www-robotrules-6.02-mv4ll2lejuetjfd3tceggjplrv3ufhhw/man", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/perl-www-robotrules-6.02-mv4ll2lejuetjfd3tceggjplrv3ufhhw/.", ":")
prepend_path("PERL5LIB", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/perl-www-robotrules-6.02-mv4ll2lejuetjfd3tceggjplrv3ufhhw/lib/perl5:/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/perl-uri-5.08-2syiw7iefezddbbotw67rbpn27qyyt6d/lib/perl5", ":")

