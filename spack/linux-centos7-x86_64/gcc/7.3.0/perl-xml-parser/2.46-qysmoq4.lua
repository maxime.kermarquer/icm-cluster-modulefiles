-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2023-05-11 11:42:52.755899
--
-- perl-xml-parser@2.46%gcc@7.3.0 build_system=perl arch=linux-centos7-broadwell/qysmoq4
--

whatis([[Name : perl-xml-parser]])
whatis([[Version : 2.46]])
whatis([[Target : broadwell]])
whatis([[Short description : XML::Parser - A perl module for parsing XML documents]])
whatis([[Configure options : EXPATLIBPATH=/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/expat-2.5.0-yfs4wfvmxzjltd3f7bkdwnu3egav3evt/lib EXPATINCPATH=/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/expat-2.5.0-yfs4wfvmxzjltd3f7bkdwnu3egav3evt/include]])

help([[Name   : perl-xml-parser]])
help([[Version: 2.46]])
help([[Target : broadwell]])
help()
help([[XML::Parser - A perl module for parsing XML documents]])


depends_on("expat/2.5.0-yfs4wfv")
depends_on("perl/5.36.0-3zk6frf")
depends_on("perl-libwww-perl/6.68-x24kbab")

prepend_path("MANPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/perl-xml-parser-2.46-qysmoq4vrmrw4gwo6jq7eae6ednqoxod/man", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/perl-xml-parser-2.46-qysmoq4vrmrw4gwo6jq7eae6ednqoxod/.", ":")
prepend_path("PERL5LIB", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/perl-xml-parser-2.46-qysmoq4vrmrw4gwo6jq7eae6ednqoxod/lib/perl5:/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/perl-libwww-perl-6.68-x24kbabjfuftfs5aeqiklyewkjygmpxb/lib/perl5:/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/perl-encode-locale-1.05-kdzbzwuv2vw7amsmzllz4ijp7mkkzpyg/lib/perl5:/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/perl-file-listing-6.04-zdbqlu33ufo7zixvzz2y4rv24vuscyrw/lib/perl5:/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/perl-http-date-6.02-xq5uttz7gbyl52dilcvyyoxz4u2ntimq/lib/perl5:/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/perl-html-parser-3.72-mojrnczxyxsej62keodnctzyfrgp54ry/lib/perl5:/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/perl-html-tagset-3.20-gyn6cl7dyopqkowyybb5kobmhvhxreza/lib/perl5:/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/perl-http-cookies-6.10-mbd32qltoa63mxqqh5nbu4prh4hi3aku/lib/perl5:/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/perl-http-message-6.44-3ggggxvejtroez42q2dg7zmb7bebxu46/lib/perl5:/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/perl-io-html-1.004-lalftdg7t5fxtdyovznqcz5bic7gfk4x/lib/perl5:/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/perl-lwp-mediatypes-6.02-lzijfu6etngjbkshdqz4fp2d323t435e/lib/perl5:/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/perl-try-tiny-0.31-6aqsf3xidxct3g5yj43ugnfxy3trh3ws/lib/perl5:/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/perl-uri-5.08-2syiw7iefezddbbotw67rbpn27qyyt6d/lib/perl5:/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/perl-http-daemon-6.01-7yqoi7hwpvasmq3ykdkt5vbzaev2labc/lib/perl5:/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/perl-http-negotiate-6.01-qpiqa3eamvtf264d36zb5bemcmvyqvhd/lib/perl5:/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/perl-net-http-6.22-ibhe4nyr2prd636mqtlub55iamk5wizj/lib/perl5:/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/perl-www-robotrules-6.02-mv4ll2lejuetjfd3tceggjplrv3ufhhw/lib/perl5", ":")

