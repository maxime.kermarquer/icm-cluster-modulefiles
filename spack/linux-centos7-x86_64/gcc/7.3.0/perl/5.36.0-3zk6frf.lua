-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2023-05-04 14:53:07.157371
--
-- perl@5.36.0%gcc@7.3.0+cpanm+open+shared+threads build_system=generic arch=linux-centos7-broadwell/3zk6frf
--

whatis([[Name : perl]])
whatis([[Version : 5.36.0]])
whatis([[Target : broadwell]])
whatis([[Short description : Perl 5 is a highly capable, feature-rich programming language with over 27 years of development.]])
whatis([[Configure options : -des -Dprefix=/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/perl-5.36.0-3zk6frfcwciyq3jht4bvkevq7n2m6rdr -Dlocincpth=/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/gdbm-1.23-p6dnskvvtze7pclicneitegxamdmox64/include -Dloclibpth=/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/gdbm-1.23-p6dnskvvtze7pclicneitegxamdmox64/lib -Accflags=-DAPPLLIB_EXP=\"/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/perl-5.36.0-3zk6frfcwciyq3jht4bvkevq7n2m6rdr/lib/perl5\" -Duseshrplib -Dusethreads]])

help([[Name   : perl]])
help([[Version: 5.36.0]])
help([[Target : broadwell]])
help()
help([[Perl 5 is a highly capable, feature-rich programming language with over
27 years of development.]])


depends_on("linux-centos7-x86_64/gcc/7.3.0/berkeley-db/18.1.40-yu7vbpe")
depends_on("linux-centos7-x86_64/gcc/7.3.0/bzip2/1.0.8-zl4fo3v")
depends_on("linux-centos7-x86_64/gcc/7.3.0/gdbm/1.23-p6dnskv")
depends_on("linux-centos7-x86_64/gcc/7.3.0/zlib/1.2.13-7shounl")

prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/perl-5.36.0-3zk6frfcwciyq3jht4bvkevq7n2m6rdr/bin", ":")
prepend_path("MANPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/perl-5.36.0-3zk6frfcwciyq3jht4bvkevq7n2m6rdr/man", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/perl-5.36.0-3zk6frfcwciyq3jht4bvkevq7n2m6rdr/.", ":")

