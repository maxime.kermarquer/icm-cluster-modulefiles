-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2023-05-04 15:46:16.797149
--
-- pigz@2.7%gcc@7.3.0 build_system=makefile arch=linux-centos7-broadwell/56lubfh
--

whatis([[Name : pigz]])
whatis([[Version : 2.7]])
whatis([[Target : broadwell]])
whatis([[Short description : A parallel implementation of gzip for modern multi-processor, multi-core machines.]])

help([[Name   : pigz]])
help([[Version: 2.7]])
help([[Target : broadwell]])
help()
help([[A parallel implementation of gzip for modern multi-processor, multi-core
machines.]])


depends_on("linux-centos7-x86_64/gcc/7.3.0/zlib/1.2.13-7shounl")

prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/pigz-2.7-56lubfhjqosatb73pocq22sdoqljd4ok/bin", ":")
prepend_path("MANPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/pigz-2.7-56lubfhjqosatb73pocq22sdoqljd4ok/man", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/pigz-2.7-56lubfhjqosatb73pocq22sdoqljd4ok/.", ":")

