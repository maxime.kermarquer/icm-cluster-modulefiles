-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2023-05-09 15:12:58.687884
--
-- pixman@0.42.2%gcc@7.3.0 build_system=autotools arch=linux-centos7-broadwell/vfkphva
--

whatis([[Name : pixman]])
whatis([[Version : 0.42.2]])
whatis([[Target : broadwell]])
whatis([[Short description : The Pixman package contains a library that provides low-level pixel manipulation features such as image compositing and trapezoid rasterization.]])
whatis([[Configure options : --enable-libpng --disable-gtk]])

help([[Name   : pixman]])
help([[Version: 0.42.2]])
help([[Target : broadwell]])
help()
help([[The Pixman package contains a library that provides low-level pixel
manipulation features such as image compositing and trapezoid
rasterization.]])


depends_on("linux-centos7-x86_64/gcc/7.3.0/libpng/1.6.39-zr33swm")

prepend_path("CPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/pixman-0.42.2-vfkphvajtlsc5d2bpzrvf4kiwa3xieyk/include", ":")
prepend_path("LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/pixman-0.42.2-vfkphvajtlsc5d2bpzrvf4kiwa3xieyk/lib", ":")
prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/pixman-0.42.2-vfkphvajtlsc5d2bpzrvf4kiwa3xieyk/lib", ":")
prepend_path("PKG_CONFIG_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/pixman-0.42.2-vfkphvajtlsc5d2bpzrvf4kiwa3xieyk/lib/pkgconfig", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/pixman-0.42.2-vfkphvajtlsc5d2bpzrvf4kiwa3xieyk/.", ":")

