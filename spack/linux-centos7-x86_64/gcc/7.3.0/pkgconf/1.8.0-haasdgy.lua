-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2023-05-04 14:40:17.374625
--
-- pkgconf@1.8.0%gcc@7.3.0 build_system=autotools arch=linux-centos7-broadwell/haasdgy
--

whatis([[Name : pkgconf]])
whatis([[Version : 1.8.0]])
whatis([[Target : broadwell]])
whatis([[Short description : pkgconf is a program which helps to configure compiler and linker flags for development frameworks. It is similar to pkg-config from freedesktop.org, providing additional functionality while also maintaining compatibility.]])

help([[Name   : pkgconf]])
help([[Version: 1.8.0]])
help([[Target : broadwell]])
help()
help([[pkgconf is a program which helps to configure compiler and linker flags
for development frameworks. It is similar to pkg-config from
freedesktop.org, providing additional functionality while also
maintaining compatibility.]])



prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/pkgconf-1.8.0-haasdgyhe4pnwbx4ie24o5h4wzpf7pv5/bin", ":")
prepend_path("CPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/pkgconf-1.8.0-haasdgyhe4pnwbx4ie24o5h4wzpf7pv5/include", ":")
prepend_path("LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/pkgconf-1.8.0-haasdgyhe4pnwbx4ie24o5h4wzpf7pv5/lib", ":")
prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/pkgconf-1.8.0-haasdgyhe4pnwbx4ie24o5h4wzpf7pv5/lib", ":")
prepend_path("MANPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/pkgconf-1.8.0-haasdgyhe4pnwbx4ie24o5h4wzpf7pv5/share/man", ":")
prepend_path("ACLOCAL_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/pkgconf-1.8.0-haasdgyhe4pnwbx4ie24o5h4wzpf7pv5/share/aclocal", ":")
prepend_path("PKG_CONFIG_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/pkgconf-1.8.0-haasdgyhe4pnwbx4ie24o5h4wzpf7pv5/lib/pkgconfig", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/pkgconf-1.8.0-haasdgyhe4pnwbx4ie24o5h4wzpf7pv5/.", ":")

