-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2023-05-10 10:35:37.629112
--
-- poppler-data@0.4.12%gcc@7.3.0~ipo build_system=cmake build_type=RelWithDebInfo generator=make arch=linux-centos7-broadwell/7mvywhz
--

whatis([[Name : poppler-data]])
whatis([[Version : 0.4.12]])
whatis([[Target : broadwell]])
whatis([[Short description : This package consists of encoding files for use with poppler. The encoding files are optional and poppler will automatically read them if they are present. When installed, the encoding files enables poppler to correctly render CJK and Cyrrilic properly. While poppler is licensed under the GPL, these encoding files have different license, and thus distributed separately.]])

help([[Name   : poppler-data]])
help([[Version: 0.4.12]])
help([[Target : broadwell]])
help()
help([[This package consists of encoding files for use with poppler. The
encoding files are optional and poppler will automatically read them if
they are present. When installed, the encoding files enables poppler to
correctly render CJK and Cyrrilic properly. While poppler is licensed
under the GPL, these encoding files have different license, and thus
distributed separately.]])



prepend_path("PKG_CONFIG_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/poppler-data-0.4.12-7mvywhzelzfsvv3qeb6txzqvjnqx6obq/share/pkgconfig", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/poppler-data-0.4.12-7mvywhzelzfsvv3qeb6txzqvjnqx6obq/.", ":")

