-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2023-05-10 10:36:47.501843
--
-- poppler@0.79.0%gcc@7.3.0~boost~cms~cpp~glib~gobject~iconv~ipo~jpeg~libcurl~openjpeg~png~qt~tiff~zlib build_system=cmake build_type=RelWithDebInfo generator=make arch=linux-centos7-broadwell/u7pcyb5
--

whatis([[Name : poppler]])
whatis([[Version : 0.79.0]])
whatis([[Target : broadwell]])
whatis([[Short description : Poppler is a PDF rendering library based on the xpdf-3.0 code base.]])
whatis([[Configure options : -DTESTDATADIR=/network/lustre/iss01/apps/software/spack/0.20.0.dev0/build_stage_tmp/sangmin.park/spack-stage-poppler-0.79.0-u7pcyb5kb7d2l3sfdixqhuaauh5sx3zb/spack-src/testdata -DENABLE_SPLASH=OFF -DWITH_NSS3=OFF -DENABLE_UNSTABLE_API_ABI_HEADERS=ON -DENABLE_BOOST=OFF -DENABLE_CMS=none -DENABLE_CPP=OFF -DENABLE_GLIB=OFF -DWITH_GLIB=OFF -DWITH_Cairo=OFF -DENABLE_GOBJECT_INTROSPECTION=OFF -DENABLE_LIBCURL=OFF -DENABLE_LIBOPENJPEG=none -DENABLE_QT4=OFF -DENABLE_QT5=OFF -DENABLE_ZLIB=OFF -DWITH_Iconv=OFF -DENABLE_DCTDECODER=none -DWITH_JPEG=OFF -DWITH_PNG=OFF -DWITH_TIFF=OFF]])

help([[Name   : poppler]])
help([[Version: 0.79.0]])
help([[Target : broadwell]])
help()
help([[Poppler is a PDF rendering library based on the xpdf-3.0 code base.]])


depends_on("linux-centos7-x86_64/gcc/7.3.0/fontconfig/2.14.2-pnelxju")
depends_on("linux-centos7-x86_64/gcc/7.3.0/freetype/2.11.1-5stkwjm")
depends_on("linux-centos7-x86_64/gcc/7.3.0/poppler-data/0.4.12-7mvywhz")

prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/poppler-0.79.0-u7pcyb5kb7d2l3sfdixqhuaauh5sx3zb/bin", ":")
prepend_path("CPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/poppler-0.79.0-u7pcyb5kb7d2l3sfdixqhuaauh5sx3zb/include", ":")
prepend_path("LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/poppler-0.79.0-u7pcyb5kb7d2l3sfdixqhuaauh5sx3zb/lib64", ":")
prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/poppler-0.79.0-u7pcyb5kb7d2l3sfdixqhuaauh5sx3zb/lib64", ":")
prepend_path("MANPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/poppler-0.79.0-u7pcyb5kb7d2l3sfdixqhuaauh5sx3zb/share/man", ":")
prepend_path("PKG_CONFIG_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/poppler-0.79.0-u7pcyb5kb7d2l3sfdixqhuaauh5sx3zb/lib64/pkgconfig", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/poppler-0.79.0-u7pcyb5kb7d2l3sfdixqhuaauh5sx3zb/.", ":")

