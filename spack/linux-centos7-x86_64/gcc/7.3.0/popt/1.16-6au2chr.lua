-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2023-06-05 17:59:25.591093
--
-- popt@1.16%gcc@7.3.0 build_system=autotools arch=linux-centos7-broadwell/6au2chr
--

whatis([[Name : popt]])
whatis([[Version : 1.16]])
whatis([[Target : broadwell]])
whatis([[Short description : The popt library parses command line options.]])

help([[Name   : popt]])
help([[Version: 1.16]])
help([[Target : broadwell]])
help()
help([[The popt library parses command line options.]])


depends_on("libiconv/1.17-afgvmbc")

prepend_path("MANPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/popt-1.16-6au2chrxsvdqiprmif4ysog4do3aabj4/share/man", ":")
prepend_path("PKG_CONFIG_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/popt-1.16-6au2chrxsvdqiprmif4ysog4do3aabj4/lib/pkgconfig", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/popt-1.16-6au2chrxsvdqiprmif4ysog4do3aabj4/.", ":")

