-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2023-05-11 10:58:47.658257
--
-- presentproto@1.1%gcc@7.3.0 build_system=autotools arch=linux-centos7-broadwell/5u6ebs6
--

whatis([[Name : presentproto]])
whatis([[Version : 1.1]])
whatis([[Target : broadwell]])
whatis([[Short description : Present protocol specification and Xlib/Xserver headers.]])

help([[Name   : presentproto]])
help([[Version: 1.1]])
help([[Target : broadwell]])
help()
help([[Present protocol specification and Xlib/Xserver headers.]])



prepend_path("PKG_CONFIG_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/presentproto-1.1-5u6ebs66ix35r2y2k4kqyn6qvyexcy5p/lib/pkgconfig", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/presentproto-1.1-5u6ebs66ix35r2y2k4kqyn6qvyexcy5p/.", ":")

