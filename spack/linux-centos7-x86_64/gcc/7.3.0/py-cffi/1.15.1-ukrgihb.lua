-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2023-06-15 17:35:30.770809
--
-- py-cffi@1.15.1%gcc@7.3.0 build_system=python_pip arch=linux-centos7-broadwell/ukrgihb
--

whatis([[Name : py-cffi]])
whatis([[Version : 1.15.1]])
whatis([[Target : broadwell]])
whatis([[Short description : Foreign Function Interface for Python calling C code]])

help([[Name   : py-cffi]])
help([[Version: 1.15.1]])
help([[Target : broadwell]])
help()
help([[Foreign Function Interface for Python calling C code]])


depends_on("libffi/3.4.4-cacrjpo")
depends_on("py-pycparser/2.21-bwukfgo")
depends_on("python/3.10.10-3muxe4q")

prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/py-cffi-1.15.1-ukrgihbhjgu6jutmpren5cqo6bpsh556/.", ":")
prepend_path("PYTHONPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/py-cffi-1.15.1-ukrgihbhjgu6jutmpren5cqo6bpsh556/lib/python3.10/site-packages", ":")
prepend_path("PYTHONPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/py-pycparser-2.21-bwukfgoctxmassrgnprz2bwd6ufw4ukj/lib/python3.10/site-packages", ":")

