-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2023-06-05 18:06:33.100088
--
-- py-cython@0.29.33%gcc@7.3.0 build_system=python_pip patches=71de066 arch=linux-centos7-broadwell/tbsbtih
--

whatis([[Name : py-cython]])
whatis([[Version : 0.29.33]])
whatis([[Target : broadwell]])
whatis([[Short description : The Cython compiler for writing C extensions for the Python language.]])

help([[Name   : py-cython]])
help([[Version: 0.29.33]])
help([[Target : broadwell]])
help()
help([[The Cython compiler for writing C extensions for the Python language.]])


depends_on("py-setuptools/67.6.0-ealma7f")
depends_on("python/3.10.10-3muxe4q")

prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/py-cython-0.29.33-tbsbtihsamggeknxk4jcjaaaliya447j/bin", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/py-cython-0.29.33-tbsbtihsamggeknxk4jcjaaaliya447j/.", ":")
prepend_path("PYTHONPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/py-cython-0.29.33-tbsbtihsamggeknxk4jcjaaaliya447j/lib/python3.10/site-packages", ":")
prepend_path("PYTHONPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/py-setuptools-67.6.0-ealma7fri6akxxbrmiyxkbx6wlnpfh5r/lib/python3.10/site-packages", ":")

