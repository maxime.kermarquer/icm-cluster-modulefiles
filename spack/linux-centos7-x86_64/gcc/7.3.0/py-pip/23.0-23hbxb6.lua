-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2023-05-09 15:16:16.813379
--
-- py-pip@23.0%gcc@7.3.0 build_system=generic arch=linux-centos7-broadwell/23hbxb6
--

whatis([[Name : py-pip]])
whatis([[Version : 23.0]])
whatis([[Target : broadwell]])
whatis([[Short description : The PyPA recommended tool for installing Python packages.]])

help([[Name   : py-pip]])
help([[Version: 23.0]])
help([[Target : broadwell]])
help()
help([[The PyPA recommended tool for installing Python packages.]])


depends_on("linux-centos7-x86_64/gcc/7.3.0/python/3.10.10-3muxe4q")

prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/py-pip-23.0-23hbxb6uyimlr62zwii35d2ycc4sbws3/bin", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/py-pip-23.0-23hbxb6uyimlr62zwii35d2ycc4sbws3/.", ":")
prepend_path("PYTHONPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/py-pip-23.0-23hbxb6uyimlr62zwii35d2ycc4sbws3/lib/python3.10/site-packages", ":")

