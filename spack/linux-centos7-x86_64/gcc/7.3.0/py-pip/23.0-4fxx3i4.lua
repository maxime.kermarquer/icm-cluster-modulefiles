-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2023-05-09 16:29:03.971032
--
-- py-pip@23.0%gcc@7.3.0 build_system=generic arch=linux-centos7-broadwell/4fxx3i4
--

whatis([[Name : py-pip]])
whatis([[Version : 23.0]])
whatis([[Target : broadwell]])
whatis([[Short description : The PyPA recommended tool for installing Python packages.]])

help([[Name   : py-pip]])
help([[Version: 23.0]])
help([[Target : broadwell]])
help()
help([[The PyPA recommended tool for installing Python packages.]])


depends_on("python/3.10.10-x6q4jmz")

prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/py-pip-23.0-4fxx3i42xigszilckzupbc6krrqiy5yp/bin", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/py-pip-23.0-4fxx3i42xigszilckzupbc6krrqiy5yp/.", ":")
prepend_path("PYTHONPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/py-pip-23.0-4fxx3i42xigszilckzupbc6krrqiy5yp/lib/python3.10/site-packages", ":")

