-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2023-06-15 17:35:20.393654
--
-- py-pycparser@2.21%gcc@7.3.0 build_system=python_pip arch=linux-centos7-broadwell/bwukfgo
--

whatis([[Name : py-pycparser]])
whatis([[Version : 2.21]])
whatis([[Target : broadwell]])
whatis([[Short description : A complete parser of the C language, written in pure python.]])

help([[Name   : py-pycparser]])
help([[Version: 2.21]])
help([[Target : broadwell]])
help()
help([[A complete parser of the C language, written in pure python.]])


depends_on("python/3.10.10-3muxe4q")

prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/py-pycparser-2.21-bwukfgoctxmassrgnprz2bwd6ufw4ukj/.", ":")
prepend_path("PYTHONPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/py-pycparser-2.21-bwukfgoctxmassrgnprz2bwd6ufw4ukj/lib/python3.10/site-packages", ":")

