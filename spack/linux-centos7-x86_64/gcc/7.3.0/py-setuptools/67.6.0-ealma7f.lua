-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2023-05-09 15:16:26.517087
--
-- py-setuptools@67.6.0%gcc@7.3.0 build_system=generic arch=linux-centos7-broadwell/ealma7f
--

whatis([[Name : py-setuptools]])
whatis([[Version : 67.6.0]])
whatis([[Target : broadwell]])
whatis([[Short description : A Python utility that aids in the process of downloading, building, upgrading, installing, and uninstalling Python packages.]])

help([[Name   : py-setuptools]])
help([[Version: 67.6.0]])
help([[Target : broadwell]])
help()
help([[A Python utility that aids in the process of downloading, building,
upgrading, installing, and uninstalling Python packages.]])


depends_on("python/3.10.10-3muxe4q")

prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/py-setuptools-67.6.0-ealma7fri6akxxbrmiyxkbx6wlnpfh5r/.", ":")
prepend_path("PYTHONPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/py-setuptools-67.6.0-ealma7fri6akxxbrmiyxkbx6wlnpfh5r/lib/python3.10/site-packages", ":")

