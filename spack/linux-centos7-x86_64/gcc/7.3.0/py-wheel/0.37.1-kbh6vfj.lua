-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2023-05-09 16:32:38.626994
--
-- py-wheel@0.37.1%gcc@7.3.0 build_system=generic arch=linux-centos7-broadwell/kbh6vfj
--

whatis([[Name : py-wheel]])
whatis([[Version : 0.37.1]])
whatis([[Target : broadwell]])
whatis([[Short description : A built-package format for Python.]])

help([[Name   : py-wheel]])
help([[Version: 0.37.1]])
help([[Target : broadwell]])
help()
help([[A built-package format for Python.]])


depends_on("python/3.10.10-x6q4jmz")

prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/py-wheel-0.37.1-kbh6vfj3vp22k4lpo5bt4h72hrc222mr/bin", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/py-wheel-0.37.1-kbh6vfj3vp22k4lpo5bt4h72hrc222mr/.", ":")
prepend_path("PYTHONPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/py-wheel-0.37.1-kbh6vfj3vp22k4lpo5bt4h72hrc222mr/lib/python3.10/site-packages", ":")

