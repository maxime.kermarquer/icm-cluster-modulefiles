-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2023-05-09 15:16:20.815403
--
-- py-wheel@0.37.1%gcc@7.3.0 build_system=generic arch=linux-centos7-broadwell/pr3bm6y
--

whatis([[Name : py-wheel]])
whatis([[Version : 0.37.1]])
whatis([[Target : broadwell]])
whatis([[Short description : A built-package format for Python.]])

help([[Name   : py-wheel]])
help([[Version: 0.37.1]])
help([[Target : broadwell]])
help()
help([[A built-package format for Python.]])


depends_on("python/3.10.10-3muxe4q")

prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/py-wheel-0.37.1-pr3bm6y3mpbmlzpx5wl2klr6rmgdcaox/bin", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/py-wheel-0.37.1-pr3bm6y3mpbmlzpx5wl2klr6rmgdcaox/.", ":")
prepend_path("PYTHONPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/py-wheel-0.37.1-pr3bm6y3mpbmlzpx5wl2klr6rmgdcaox/lib/python3.10/site-packages", ":")

