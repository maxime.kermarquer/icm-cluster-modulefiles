-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2023-05-04 16:25:51.884891
--
-- python@3.10.10%gcc@7.3.0+bz2+crypt+ctypes+dbm~debug+libxml2+lzma~nis~optimizations+pic+pyexpat+pythoncmd+readline+shared+sqlite3+ssl~tkinter+uuid+zlib build_system=generic patches=0d98e93,7d40923,f2fd060 arch=linux-centos7-broadwell/3muxe4q
--

whatis([[Name : python]])
whatis([[Version : 3.10.10]])
whatis([[Target : broadwell]])
whatis([[Short description : The Python programming language.]])
whatis([[Configure options : CPPFLAGS=-I/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/bzip2-1.0.8-zl4fo3vieedck4w2x53bgnrxdzxesfhc/include -I/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/expat-2.5.0-yfs4wfvmxzjltd3f7bkdwnu3egav3evt/include -I/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/gdbm-1.23-p6dnskvvtze7pclicneitegxamdmox64/include -I/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/gettext-0.21.1-nfzsemsi3me4hskjjbph4z5wbopxpwk7/include -I/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/libffi-3.4.4-cacrjpo6tmxilcbpni4frcdmvtoocx36/include -I/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/libxcrypt-4.4.33-v3yo2l42q5mchoftxbjj3voisxmbapxf/include -I/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/ncurses-6.4-6n6u7qv2o4rszzmabo3ykcf6cwuetgzl/include -I/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/openssl-1.1.1t-f5klqzusu6fj4i4bdrxkxvvydvzj7ofa/include -I/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/readline-8.2-cqkbqunz7kg34rc6w4sf6ehx36qn2ldt/include -I/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/sqlite-3.40.1-umnclto24dx7a5nxwbzvnmefntzpmae6/include -I/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/util-linux-uuid-2.38.1-2oluzsj4ijtey5yn6rles6hh7lhahuls/include -I/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/xz-5.4.1-pthvj6auvqjxwcbmuif5za7acqovwri5/include -I/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/zlib-1.2.13-7shounlfxhfmsqyucuh5nfp447lvnxn4/include LDFLAGS=-L/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/bzip2-1.0.8-zl4fo3vieedck4w2x53bgnrxdzxesfhc/lib -L/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/expat-2.5.0-yfs4wfvmxzjltd3f7bkdwnu3egav3evt/lib -L/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/gdbm-1.23-p6dnskvvtze7pclicneitegxamdmox64/lib -L/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/gettext-0.21.1-nfzsemsi3me4hskjjbph4z5wbopxpwk7/lib -L/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/libffi-3.4.4-cacrjpo6tmxilcbpni4frcdmvtoocx36/lib64 -L/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/libxcrypt-4.4.33-v3yo2l42q5mchoftxbjj3voisxmbapxf/lib -L/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/ncurses-6.4-6n6u7qv2o4rszzmabo3ykcf6cwuetgzl/lib -L/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/openssl-1.1.1t-f5klqzusu6fj4i4bdrxkxvvydvzj7ofa/lib -L/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/readline-8.2-cqkbqunz7kg34rc6w4sf6ehx36qn2ldt/lib -L/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/sqlite-3.40.1-umnclto24dx7a5nxwbzvnmefntzpmae6/lib -L/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/util-linux-uuid-2.38.1-2oluzsj4ijtey5yn6rles6hh7lhahuls/lib -L/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/xz-5.4.1-pthvj6auvqjxwcbmuif5za7acqovwri5/lib -L/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/zlib-1.2.13-7shounlfxhfmsqyucuh5nfp447lvnxn4/lib --without-pydebug --enable-shared --without-ensurepip --with-openssl=/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/openssl-1.1.1t-f5klqzusu6fj4i4bdrxkxvvydvzj7ofa --with-dbmliborder=gdbm --with-system-expat --with-system-ffi --enable-loadable-sqlite-extensions CFLAGS=-fPIC]])

help([[Name   : python]])
help([[Version: 3.10.10]])
help([[Target : broadwell]])
help()
help([[The Python programming language.]])


depends_on("linux-centos7-x86_64/gcc/7.3.0/bzip2/1.0.8-zl4fo3v")
depends_on("linux-centos7-x86_64/gcc/7.3.0/expat/2.5.0-yfs4wfv")
depends_on("linux-centos7-x86_64/gcc/7.3.0/gdbm/1.23-p6dnskv")
depends_on("linux-centos7-x86_64/gcc/7.3.0/gettext/0.21.1-nfzsems")
depends_on("linux-centos7-x86_64/gcc/7.3.0/libffi/3.4.4-cacrjpo")
depends_on("linux-centos7-x86_64/gcc/7.3.0/libxcrypt/4.4.33-v3yo2l4")
depends_on("linux-centos7-x86_64/gcc/7.3.0/ncurses/6.4-6n6u7qv")
depends_on("linux-centos7-x86_64/gcc/7.3.0/openssl/1.1.1t-f5klqzu")
depends_on("linux-centos7-x86_64/gcc/7.3.0/readline/8.2-cqkbqun")
depends_on("linux-centos7-x86_64/gcc/7.3.0/sqlite/3.40.1-umnclto")
depends_on("linux-centos7-x86_64/gcc/7.3.0/util-linux-uuid/2.38.1-2oluzsj")
depends_on("linux-centos7-x86_64/gcc/7.3.0/xz/5.4.1-pthvj6a")
depends_on("linux-centos7-x86_64/gcc/7.3.0/zlib/1.2.13-7shounl")

prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/python-3.10.10-3muxe4qnheiapjqxmhqtqzjr7gdml3oi/bin", ":")
prepend_path("MANPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/python-3.10.10-3muxe4qnheiapjqxmhqtqzjr7gdml3oi/share/man", ":")
prepend_path("PKG_CONFIG_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/python-3.10.10-3muxe4qnheiapjqxmhqtqzjr7gdml3oi/lib/pkgconfig", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/python-3.10.10-3muxe4qnheiapjqxmhqtqzjr7gdml3oi/.", ":")

