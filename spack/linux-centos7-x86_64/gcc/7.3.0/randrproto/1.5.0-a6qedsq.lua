-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2023-05-10 09:27:36.568851
--
-- randrproto@1.5.0%gcc@7.3.0 build_system=autotools arch=linux-centos7-broadwell/a6qedsq
--

whatis([[Name : randrproto]])
whatis([[Version : 1.5.0]])
whatis([[Target : broadwell]])
whatis([[Short description : X Resize and Rotate Extension (RandR).]])

help([[Name   : randrproto]])
help([[Version: 1.5.0]])
help([[Target : broadwell]])
help()
help([[X Resize and Rotate Extension (RandR). This extension defines a protocol
for clients to dynamically change X screens, so as to resize, rotate and
reflect the root window of a screen.]])



prepend_path("PKG_CONFIG_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/randrproto-1.5.0-a6qedsqv4zxridlacvy72fkvp54wvlua/lib/pkgconfig", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/randrproto-1.5.0-a6qedsqv4zxridlacvy72fkvp54wvlua/.", ":")

