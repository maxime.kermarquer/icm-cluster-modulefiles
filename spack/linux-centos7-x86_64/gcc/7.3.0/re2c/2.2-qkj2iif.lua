-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2023-05-09 15:10:40.702512
--
-- re2c@2.2%gcc@7.3.0 build_system=generic arch=linux-centos7-broadwell/qkj2iif
--

whatis([[Name : re2c]])
whatis([[Version : 2.2]])
whatis([[Target : broadwell]])
whatis([[Short description : re2c: a free and open-source lexer generator for C and C++]])
whatis([[Configure options : --disable-benchmarks --disable-debug --disable-dependency-tracking --disable-docs --disable-lexers --disable-libs --enable-golang]])

help([[Name   : re2c]])
help([[Version: 2.2]])
help([[Target : broadwell]])
help()
help([[re2c: a free and open-source lexer generator for C and C++]])



prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/re2c-2.2-qkj2iifizbbgfugz32ldwskoujrrci6v/bin", ":")
prepend_path("MANPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/re2c-2.2-qkj2iifizbbgfugz32ldwskoujrrci6v/share/man", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/re2c-2.2-qkj2iifizbbgfugz32ldwskoujrrci6v/.", ":")

