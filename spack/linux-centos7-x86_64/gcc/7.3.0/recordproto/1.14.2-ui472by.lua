-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2023-05-10 09:28:07.691737
--
-- recordproto@1.14.2%gcc@7.3.0 build_system=autotools arch=linux-centos7-broadwell/ui472by
--

whatis([[Name : recordproto]])
whatis([[Version : 1.14.2]])
whatis([[Target : broadwell]])
whatis([[Short description : X Record Extension.]])

help([[Name   : recordproto]])
help([[Version: 1.14.2]])
help([[Target : broadwell]])
help()
help([[X Record Extension. This extension defines a protocol for the recording
and playback of user actions in the X Window System.]])



prepend_path("PKG_CONFIG_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/recordproto-1.14.2-ui472bympcvjtxue2eh54b4fs6ueaedv/lib/pkgconfig", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/recordproto-1.14.2-ui472bympcvjtxue2eh54b4fs6ueaedv/.", ":")

