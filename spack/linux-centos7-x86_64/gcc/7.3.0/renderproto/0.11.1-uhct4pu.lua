-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2023-05-09 15:48:49.593341
--
-- renderproto@0.11.1%gcc@7.3.0 build_system=autotools arch=linux-centos7-broadwell/uhct4pu
--

whatis([[Name : renderproto]])
whatis([[Version : 0.11.1]])
whatis([[Target : broadwell]])
whatis([[Short description : X Rendering Extension.]])

help([[Name   : renderproto]])
help([[Version: 0.11.1]])
help([[Target : broadwell]])
help()
help([[X Rendering Extension. This extension defines the protcol for a digital
image composition as the foundation of a new rendering model within the
X Window System.]])



prepend_path("CPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/renderproto-0.11.1-uhct4pu6awf23gaqhszgsfmxu2mqqpjw/include", ":")
prepend_path("LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/renderproto-0.11.1-uhct4pu6awf23gaqhszgsfmxu2mqqpjw/lib", ":")
prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/renderproto-0.11.1-uhct4pu6awf23gaqhszgsfmxu2mqqpjw/lib", ":")
prepend_path("PKG_CONFIG_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/renderproto-0.11.1-uhct4pu6awf23gaqhszgsfmxu2mqqpjw/lib/pkgconfig", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/renderproto-0.11.1-uhct4pu6awf23gaqhszgsfmxu2mqqpjw/.", ":")

