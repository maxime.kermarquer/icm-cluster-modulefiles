-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2023-05-11 10:58:28.504837
--
-- resourceproto@1.2.0%gcc@7.3.0 build_system=autotools arch=linux-centos7-broadwell/sbzde77
--

whatis([[Name : resourceproto]])
whatis([[Version : 1.2.0]])
whatis([[Target : broadwell]])
whatis([[Short description : X Resource Extension.]])

help([[Name   : resourceproto]])
help([[Version: 1.2.0]])
help([[Target : broadwell]])
help()
help([[X Resource Extension. This extension defines a protocol that allows a
client to query the X server about its usage of various resources.]])



prepend_path("PKG_CONFIG_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/resourceproto-1.2.0-sbzde77ryhwtxnvlxn3uwpx274twektc/lib/pkgconfig", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/resourceproto-1.2.0-sbzde77ryhwtxnvlxn3uwpx274twektc/.", ":")

