-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2023-05-04 16:21:22.620344
--
-- rpcsvc-proto@1.4.3%gcc@7.3.0 build_system=autotools arch=linux-centos7-broadwell/uvvvswm
--

whatis([[Name : rpcsvc-proto]])
whatis([[Version : 1.4.3]])
whatis([[Target : broadwell]])
whatis([[Short description : rpcsvc protocol definitions from glibc.]])
whatis([[Configure options : LIBS=-lintl]])

help([[Name   : rpcsvc-proto]])
help([[Version: 1.4.3]])
help([[Target : broadwell]])
help()
help([[rpcsvc protocol definitions from glibc.]])


depends_on("gettext/0.21.1-nfzsems")

prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/rpcsvc-proto-1.4.3-uvvvswm5waqrqto6iuaozj7yt5l6b2qc/bin", ":")
prepend_path("MANPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/rpcsvc-proto-1.4.3-uvvvswm5waqrqto6iuaozj7yt5l6b2qc/share/man", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/rpcsvc-proto-1.4.3-uvvvswm5waqrqto6iuaozj7yt5l6b2qc/.", ":")

