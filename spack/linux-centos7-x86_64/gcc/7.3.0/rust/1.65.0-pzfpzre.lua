-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2023-05-11 11:39:21.987162
--
-- rust@1.65.0%gcc@7.3.0+analysis+clippy~rls+rustfmt+src build_system=generic extra_targets=none arch=linux-centos7-broadwell/pzfpzre
--

whatis([[Name : rust]])
whatis([[Version : 1.65.0]])
whatis([[Target : broadwell]])
whatis([[Short description : The Rust programming language toolchain]])

help([[Name   : rust]])
help([[Version: 1.65.0]])
help([[Target : broadwell]])
help()
help([[The Rust programming language toolchain This package can bootstrap any
version of the Rust compiler since Rust 1.23. It does this by
downloading the platform-appropriate binary distribution of the desired
version of the rust compiler, and then building that compiler from
source.]])


depends_on("curl/8.0.1-cxhf7ga")
depends_on("libgit2/1.6.4-jnxivhb")
depends_on("libssh2/1.10.0-722ghfr")
depends_on("openssl/1.1.1t-f5klqzu")

prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/rust-1.65.0-pzfpzre6f7mdskwgod7j7gofswyi2v33/bin", ":")
prepend_path("MANPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/rust-1.65.0-pzfpzre6f7mdskwgod7j7gofswyi2v33/share/man", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/rust-1.65.0-pzfpzre6f7mdskwgod7j7gofswyi2v33/.", ":")

