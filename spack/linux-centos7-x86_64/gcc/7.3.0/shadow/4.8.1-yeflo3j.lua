-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2023-06-05 17:59:10.512017
--
-- shadow@4.8.1%gcc@7.3.0 build_system=autotools arch=linux-centos7-broadwell/yeflo3j
--

whatis([[Name : shadow]])
whatis([[Version : 4.8.1]])
whatis([[Target : broadwell]])
whatis([[Short description : Tools to help unprivileged users create uid and gid mappings in user namespaces.]])

help([[Name   : shadow]])
help([[Version: 4.8.1]])
help([[Target : broadwell]])
help()
help([[Tools to help unprivileged users create uid and gid mappings in user
namespaces.]])



prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/shadow-4.8.1-yeflo3ju236x5q4pmo7idhyaea3tgqt5/bin", ":")
prepend_path("MANPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/shadow-4.8.1-yeflo3ju236x5q4pmo7idhyaea3tgqt5/share/man", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/shadow-4.8.1-yeflo3ju236x5q4pmo7idhyaea3tgqt5/.", ":")

