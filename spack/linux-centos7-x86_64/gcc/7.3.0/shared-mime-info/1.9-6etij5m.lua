-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2023-05-11 11:44:16.071647
--
-- shared-mime-info@1.9%gcc@7.3.0 build_system=autotools arch=linux-centos7-broadwell/6etij5m
--

whatis([[Name : shared-mime-info]])
whatis([[Version : 1.9]])
whatis([[Target : broadwell]])
whatis([[Short description : Database of common MIME types.]])

help([[Name   : shared-mime-info]])
help([[Version: 1.9]])
help([[Target : broadwell]])
help()
help([[Database of common MIME types.]])


depends_on("linux-centos7-x86_64/gcc/7.3.0/glib/2.76.1-webav7s")
depends_on("linux-centos7-x86_64/gcc/7.3.0/libxml2/2.10.3-23vb7ui")

prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/shared-mime-info-1.9-6etij5mjgjpq4x6qnmac2uplbftk62nd/bin", ":")
prepend_path("MANPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/shared-mime-info-1.9-6etij5mjgjpq4x6qnmac2uplbftk62nd/share/man", ":")
prepend_path("PKG_CONFIG_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/shared-mime-info-1.9-6etij5mjgjpq4x6qnmac2uplbftk62nd/share/pkgconfig", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/shared-mime-info-1.9-6etij5mjgjpq4x6qnmac2uplbftk62nd/.", ":")

