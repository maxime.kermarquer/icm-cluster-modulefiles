-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2023-05-04 16:04:20.845706
--
-- sqlite@3.40.1%gcc@7.3.0+column_metadata+dynamic_extensions+fts~functions+rtree build_system=autotools arch=linux-centos7-broadwell/umnclto
--

whatis([[Name : sqlite]])
whatis([[Version : 3.40.1]])
whatis([[Target : broadwell]])
whatis([[Short description : SQLite is a C-language library that implements a small, fast, self-contained, high-reliability, full-featured, SQL database engine. ]])
whatis([[Configure options : --enable-fts4 --enable-fts5 --enable-rtree --enable-dynamic-extensions CPPFLAGS=-DSQLITE_ENABLE_COLUMN_METADATA=1]])

help([[Name   : sqlite]])
help([[Version: 3.40.1]])
help([[Target : broadwell]])
help()
help([[SQLite is a C-language library that implements a small, fast, self-
contained, high-reliability, full-featured, SQL database engine.]])


depends_on("linux-centos7-x86_64/gcc/7.3.0/readline/8.2-cqkbqun")
depends_on("linux-centos7-x86_64/gcc/7.3.0/zlib/1.2.13-7shounl")

prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/sqlite-3.40.1-umnclto24dx7a5nxwbzvnmefntzpmae6/bin", ":")
prepend_path("CPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/sqlite-3.40.1-umnclto24dx7a5nxwbzvnmefntzpmae6/include", ":")
prepend_path("LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/sqlite-3.40.1-umnclto24dx7a5nxwbzvnmefntzpmae6/lib", ":")
prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/sqlite-3.40.1-umnclto24dx7a5nxwbzvnmefntzpmae6/lib", ":")
prepend_path("MANPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/sqlite-3.40.1-umnclto24dx7a5nxwbzvnmefntzpmae6/share/man", ":")
prepend_path("PKG_CONFIG_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/sqlite-3.40.1-umnclto24dx7a5nxwbzvnmefntzpmae6/lib/pkgconfig", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/sqlite-3.40.1-umnclto24dx7a5nxwbzvnmefntzpmae6/.", ":")

