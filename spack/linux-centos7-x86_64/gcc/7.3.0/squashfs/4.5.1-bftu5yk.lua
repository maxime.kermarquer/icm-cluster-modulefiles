-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2023-06-05 17:59:34.008707
--
-- squashfs@4.5.1%gcc@7.3.0+gzip~lz4~lzo~xz~zstd build_system=makefile default_compression=gzip arch=linux-centos7-broadwell/bftu5yk
--

whatis([[Name : squashfs]])
whatis([[Version : 4.5.1]])
whatis([[Target : broadwell]])
whatis([[Short description : Squashfs - read only compressed filesystem]])

help([[Name   : squashfs]])
help([[Version: 4.5.1]])
help([[Target : broadwell]])
help()
help([[Squashfs - read only compressed filesystem]])


depends_on("zlib/1.2.13-7shounl")

prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/squashfs-4.5.1-bftu5ykud7qdh67gq3tfg76p5wyj4aor/bin", ":")
prepend_path("MANPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/squashfs-4.5.1-bftu5ykud7qdh67gq3tfg76p5wyj4aor/man", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/squashfs-4.5.1-bftu5ykud7qdh67gq3tfg76p5wyj4aor/.", ":")

