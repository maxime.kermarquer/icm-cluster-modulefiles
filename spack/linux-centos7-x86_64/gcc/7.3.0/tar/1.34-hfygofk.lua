-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2023-05-04 16:11:10.582793
--
-- tar@1.34%gcc@7.3.0 build_system=autotools zip=pigz arch=linux-centos7-broadwell/hfygofk
--

whatis([[Name : tar]])
whatis([[Version : 1.34]])
whatis([[Target : broadwell]])
whatis([[Short description : GNU Tar provides the ability to create tar archives, as well as various other kinds of manipulation.]])
whatis([[Configure options : --with-libiconv-prefix=/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/libiconv-1.17-afgvmbc7dt2refbap2qamy32yibwnrcr --with-xz=/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/xz-5.4.1-pthvj6auvqjxwcbmuif5za7acqovwri5/bin/xz --with-lzma=/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/xz-5.4.1-pthvj6auvqjxwcbmuif5za7acqovwri5/bin/lzma --with-bzip2=/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/bzip2-1.0.8-zl4fo3vieedck4w2x53bgnrxdzxesfhc/bin/bzip2 --with-zstd=/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/zstd-1.5.5-nwuxakczlignlp3a3etw5rzf6qpetd6g/bin/zstd --with-gzip=/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/pigz-2.7-56lubfhjqosatb73pocq22sdoqljd4ok/bin/pigz]])

help([[Name   : tar]])
help([[Version: 1.34]])
help([[Target : broadwell]])
help()
help([[GNU Tar provides the ability to create tar archives, as well as various
other kinds of manipulation.]])


depends_on("linux-centos7-x86_64/gcc/7.3.0/bzip2/1.0.8-zl4fo3v")
depends_on("linux-centos7-x86_64/gcc/7.3.0/libiconv/1.17-afgvmbc")
depends_on("linux-centos7-x86_64/gcc/7.3.0/pigz/2.7-56lubfh")
depends_on("linux-centos7-x86_64/gcc/7.3.0/xz/5.4.1-pthvj6a")
depends_on("linux-centos7-x86_64/gcc/7.3.0/zstd/1.5.5-nwuxakc")

prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/tar-1.34-hfygofkv75vh55mkl5ks2dhpqb5imesx/bin", ":")
prepend_path("MANPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/tar-1.34-hfygofkv75vh55mkl5ks2dhpqb5imesx/share/man", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/tar-1.34-hfygofkv75vh55mkl5ks2dhpqb5imesx/.", ":")

