-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2023-05-09 15:42:16.509234
--
-- tcl@8.6.12%gcc@7.3.0 build_system=autotools arch=linux-centos7-broadwell/zeqq76q
--

whatis([[Name : tcl]])
whatis([[Version : 8.6.12]])
whatis([[Target : broadwell]])
whatis([[Short description : Tcl (Tool Command Language) is a very powerful but easy to learn dynamic programming language, suitable for a very wide range of uses, including web and desktop applications, networking, administration, testing and many more. Open source and business-friendly, Tcl is a mature yet evolving language that is truly cross platform, easily deployed and highly extensible.]])

help([[Name   : tcl]])
help([[Version: 8.6.12]])
help([[Target : broadwell]])
help()
help([[Tcl (Tool Command Language) is a very powerful but easy to learn dynamic
programming language, suitable for a very wide range of uses, including
web and desktop applications, networking, administration, testing and
many more. Open source and business-friendly, Tcl is a mature yet
evolving language that is truly cross platform, easily deployed and
highly extensible.]])


depends_on("linux-centos7-x86_64/gcc/7.3.0/zlib/1.2.13-7shounl")

prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/tcl-8.6.12-zeqq76qsypomfy5lwqdwbndbk7n4upad/bin", ":")
prepend_path("CPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/tcl-8.6.12-zeqq76qsypomfy5lwqdwbndbk7n4upad/include", ":")
prepend_path("LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/tcl-8.6.12-zeqq76qsypomfy5lwqdwbndbk7n4upad/lib", ":")
prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/tcl-8.6.12-zeqq76qsypomfy5lwqdwbndbk7n4upad/lib", ":")
prepend_path("MANPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/tcl-8.6.12-zeqq76qsypomfy5lwqdwbndbk7n4upad/man", ":")
prepend_path("MANPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/tcl-8.6.12-zeqq76qsypomfy5lwqdwbndbk7n4upad/share/man", ":")
prepend_path("PKG_CONFIG_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/tcl-8.6.12-zeqq76qsypomfy5lwqdwbndbk7n4upad/lib/pkgconfig", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/tcl-8.6.12-zeqq76qsypomfy5lwqdwbndbk7n4upad/.", ":")
setenv("TCL_LIBRARY", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/tcl-8.6.12-zeqq76qsypomfy5lwqdwbndbk7n4upad/lib/tcl8.6")

