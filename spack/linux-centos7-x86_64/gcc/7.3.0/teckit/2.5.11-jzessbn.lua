-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2023-05-10 10:38:08.333945
--
-- teckit@2.5.11%gcc@7.3.0 build_system=autotools arch=linux-centos7-broadwell/jzessbn
--

whatis([[Name : teckit]])
whatis([[Version : 2.5.11]])
whatis([[Target : broadwell]])
whatis([[Short description : TECkit is a low-level toolkit intended to be used by applications for conversions between text encodings. For example, it can be used when importing legacy text into a Unicode-based application.]])
whatis([[Configure options : --with-system-zlib]])

help([[Name   : teckit]])
help([[Version: 2.5.11]])
help([[Target : broadwell]])
help()
help([[TECkit is a low-level toolkit intended to be used by applications for
conversions between text encodings. For example, it can be used when
importing legacy text into a Unicode-based application. The primary
component of TECkit is a library: the TECkit engine. The engine relies
on mapping tables in a specific, documented binary format. The TECkit
compiler creates these tables from plain-text, human-readable
descriptions.]])


depends_on("zlib/1.2.13-7shounl")

prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/teckit-2.5.11-jzessbnb4q23zfwbwxvhljz7psp5d6fx/bin", ":")
prepend_path("MANPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/teckit-2.5.11-jzessbnb4q23zfwbwxvhljz7psp5d6fx/share/man", ":")
prepend_path("PKG_CONFIG_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/teckit-2.5.11-jzessbnb4q23zfwbwxvhljz7psp5d6fx/lib/pkgconfig", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/teckit-2.5.11-jzessbnb4q23zfwbwxvhljz7psp5d6fx/.", ":")

