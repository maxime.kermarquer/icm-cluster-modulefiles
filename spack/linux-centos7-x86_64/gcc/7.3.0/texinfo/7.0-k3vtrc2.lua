-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2023-05-10 09:34:17.205563
--
-- texinfo@7.0%gcc@7.3.0 build_system=autotools arch=linux-centos7-broadwell/k3vtrc2
--

whatis([[Name : texinfo]])
whatis([[Version : 7.0]])
whatis([[Target : broadwell]])
whatis([[Short description : Texinfo is the official documentation format of the GNU project.]])

help([[Name   : texinfo]])
help([[Version: 7.0]])
help([[Target : broadwell]])
help()
help([[Texinfo is the official documentation format of the GNU project. It was
invented by Richard Stallman and Bob Chassell many years ago, loosely
based on Brian Reid's Scribe and other formatting languages of the time.
It is used by many non-GNU projects as well.]])


depends_on("linux-centos7-x86_64/gcc/7.3.0/gettext/0.21.1-nfzsems")
depends_on("linux-centos7-x86_64/gcc/7.3.0/ncurses/6.4-6n6u7qv")
depends_on("linux-centos7-x86_64/gcc/7.3.0/perl/5.36.0-3zk6frf")

prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/texinfo-7.0-k3vtrc2szruxtydjnng24vvpk4o2xi6t/bin", ":")
prepend_path("MANPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/texinfo-7.0-k3vtrc2szruxtydjnng24vvpk4o2xi6t/share/man", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/texinfo-7.0-k3vtrc2szruxtydjnng24vvpk4o2xi6t/.", ":")

