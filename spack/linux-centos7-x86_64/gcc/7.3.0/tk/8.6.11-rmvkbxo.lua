-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2023-05-09 16:05:56.377822
--
-- tk@8.6.11%gcc@7.3.0+xft+xss build_system=autotools arch=linux-centos7-broadwell/rmvkbxo
--

whatis([[Name : tk]])
whatis([[Version : 8.6.11]])
whatis([[Target : broadwell]])
whatis([[Short description : Tk is a graphical user interface toolkit that takes developing desktop applications to a higher level than conventional approaches. Tk is the standard GUI not only for Tcl, but for many other dynamic languages, and can produce rich, native applications that run unchanged across Windows, Mac OS X, Linux and more.]])
whatis([[Configure options : --with-tcl=/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/tcl-8.6.12-zeqq76qsypomfy5lwqdwbndbk7n4upad/lib --x-includes=/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/libx11-1.8.4-mygw7c45hwjfojavmnf2f3qnjb3bugq4/include --x-libraries=/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/libx11-1.8.4-mygw7c45hwjfojavmnf2f3qnjb3bugq4/lib --enable-xft --enable-xss]])

help([[Name   : tk]])
help([[Version: 8.6.11]])
help([[Target : broadwell]])
help()
help([[Tk is a graphical user interface toolkit that takes developing desktop
applications to a higher level than conventional approaches. Tk is the
standard GUI not only for Tcl, but for many other dynamic languages, and
can produce rich, native applications that run unchanged across Windows,
Mac OS X, Linux and more.]])


depends_on("linux-centos7-x86_64/gcc/7.3.0/libx11/1.8.4-mygw7c4")
depends_on("linux-centos7-x86_64/gcc/7.3.0/libxft/2.3.2-up2gzhf")
depends_on("linux-centos7-x86_64/gcc/7.3.0/libxscrnsaver/1.2.2-7vp2siu")
depends_on("linux-centos7-x86_64/gcc/7.3.0/tcl/8.6.12-zeqq76q")

prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/tk-8.6.11-rmvkbxogb2fxklwcifci5aie2dh7g6bs/bin", ":")
prepend_path("CPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/tk-8.6.11-rmvkbxogb2fxklwcifci5aie2dh7g6bs/include", ":")
prepend_path("LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/tk-8.6.11-rmvkbxogb2fxklwcifci5aie2dh7g6bs/lib", ":")
prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/tk-8.6.11-rmvkbxogb2fxklwcifci5aie2dh7g6bs/lib", ":")
prepend_path("MANPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/tk-8.6.11-rmvkbxogb2fxklwcifci5aie2dh7g6bs/man", ":")
prepend_path("PKG_CONFIG_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/tk-8.6.11-rmvkbxogb2fxklwcifci5aie2dh7g6bs/lib/pkgconfig", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/tk-8.6.11-rmvkbxogb2fxklwcifci5aie2dh7g6bs/.", ":")
prepend_path("XLOCALEDIR", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/libx11-1.8.4-mygw7c45hwjfojavmnf2f3qnjb3bugq4/share/X11/locale", ":")
prepend_path("TCLLIBPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/tk-8.6.11-rmvkbxogb2fxklwcifci5aie2dh7g6bs/lib", " ")
setenv("TK_LIBRARY", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/tk-8.6.11-rmvkbxogb2fxklwcifci5aie2dh7g6bs/lib/tk8.6")

