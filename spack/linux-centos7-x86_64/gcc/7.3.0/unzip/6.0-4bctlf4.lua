-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2023-05-10 09:26:50.103173
--
-- unzip@6.0%gcc@7.3.0 build_system=makefile arch=linux-centos7-broadwell/4bctlf4
--

whatis([[Name : unzip]])
whatis([[Version : 6.0]])
whatis([[Target : broadwell]])
whatis([[Short description : Unzip is a compression and file packaging/archive utility.]])

help([[Name   : unzip]])
help([[Version: 6.0]])
help([[Target : broadwell]])
help()
help([[Unzip is a compression and file packaging/archive utility.]])



prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/unzip-6.0-4bctlf45suxqbt6cnzm53azgbs64seui/bin", ":")
prepend_path("MANPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/unzip-6.0-4bctlf45suxqbt6cnzm53azgbs64seui/man", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/unzip-6.0-4bctlf45suxqbt6cnzm53azgbs64seui/.", ":")

