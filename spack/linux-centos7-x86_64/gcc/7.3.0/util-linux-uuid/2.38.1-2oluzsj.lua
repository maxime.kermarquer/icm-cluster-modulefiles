-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2023-05-04 15:59:48.483445
--
-- util-linux-uuid@2.38.1%gcc@7.3.0 build_system=autotools arch=linux-centos7-broadwell/2oluzsj
--

whatis([[Name : util-linux-uuid]])
whatis([[Version : 2.38.1]])
whatis([[Target : broadwell]])
whatis([[Short description : Util-linux is a suite of essential utilities for any Linux system.]])
whatis([[Configure options : --disable-use-tty-group --disable-makeinstall-chown --without-systemd --disable-all-programs --without-python --enable-libuuid --disable-bash-completion]])

help([[Name   : util-linux-uuid]])
help([[Version: 2.38.1]])
help([[Target : broadwell]])
help()
help([[Util-linux is a suite of essential utilities for any Linux system.]])



prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/util-linux-uuid-2.38.1-2oluzsj4ijtey5yn6rles6hh7lhahuls/bin", ":")
prepend_path("CPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/util-linux-uuid-2.38.1-2oluzsj4ijtey5yn6rles6hh7lhahuls/include", ":")
prepend_path("LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/util-linux-uuid-2.38.1-2oluzsj4ijtey5yn6rles6hh7lhahuls/lib", ":")
prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/util-linux-uuid-2.38.1-2oluzsj4ijtey5yn6rles6hh7lhahuls/lib", ":")
prepend_path("MANPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/util-linux-uuid-2.38.1-2oluzsj4ijtey5yn6rles6hh7lhahuls/share/man", ":")
prepend_path("PKG_CONFIG_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/util-linux-uuid-2.38.1-2oluzsj4ijtey5yn6rles6hh7lhahuls/lib/pkgconfig", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/util-linux-uuid-2.38.1-2oluzsj4ijtey5yn6rles6hh7lhahuls/.", ":")

