-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2023-05-10 10:35:41.995572
--
-- util-linux@2.38.1%gcc@7.3.0~bash build_system=autotools arch=linux-centos7-broadwell/j6mejf3
--

whatis([[Name : util-linux]])
whatis([[Version : 2.38.1]])
whatis([[Target : broadwell]])
whatis([[Short description : Util-linux is a suite of essential utilities for any Linux system.]])
whatis([[Configure options : --disable-use-tty-group --disable-makeinstall-chown --without-systemd --disable-libuuid --disable-bash-completion]])

help([[Name   : util-linux]])
help([[Version: 2.38.1]])
help([[Target : broadwell]])
help()
help([[Util-linux is a suite of essential utilities for any Linux system.]])


depends_on("libxcrypt/4.4.33-v3yo2l4")
depends_on("ncurses/6.4-6n6u7qv")
depends_on("zlib/1.2.13-7shounl")

prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/util-linux-2.38.1-j6mejf352ziuz5suq4wgf7bntxasrq3s/bin", ":")
prepend_path("MANPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/util-linux-2.38.1-j6mejf352ziuz5suq4wgf7bntxasrq3s/share/man", ":")
prepend_path("PKG_CONFIG_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/util-linux-2.38.1-j6mejf352ziuz5suq4wgf7bntxasrq3s/lib/pkgconfig", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/util-linux-2.38.1-j6mejf352ziuz5suq4wgf7bntxasrq3s/.", ":")

