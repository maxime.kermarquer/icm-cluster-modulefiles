-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2023-05-09 15:47:01.638160
--
-- util-macros@1.19.3%gcc@7.3.0 build_system=autotools arch=linux-centos7-broadwell/dn4gy5h
--

whatis([[Name : util-macros]])
whatis([[Version : 1.19.3]])
whatis([[Target : broadwell]])
whatis([[Short description : This is a set of autoconf macros used by the configure.ac scripts in other Xorg modular packages, and is needed to generate new versions of their configure scripts with autoconf.]])

help([[Name   : util-macros]])
help([[Version: 1.19.3]])
help([[Target : broadwell]])
help()
help([[This is a set of autoconf macros used by the configure.ac scripts in
other Xorg modular packages, and is needed to generate new versions of
their configure scripts with autoconf.]])



prepend_path("ACLOCAL_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/util-macros-1.19.3-dn4gy5h5yni6x6trsuduw5hwxwtbwtxf/share/aclocal", ":")
prepend_path("PKG_CONFIG_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/util-macros-1.19.3-dn4gy5h5yni6x6trsuduw5hwxwtbwtxf/share/pkgconfig", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/util-macros-1.19.3-dn4gy5h5yni6x6trsuduw5hwxwtbwtxf/.", ":")

