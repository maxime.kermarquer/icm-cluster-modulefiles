-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2023-05-11 10:58:56.355301
--
-- videoproto@2.3.3%gcc@7.3.0 build_system=autotools arch=linux-centos7-broadwell/4i52pqe
--

whatis([[Name : videoproto]])
whatis([[Version : 2.3.3]])
whatis([[Target : broadwell]])
whatis([[Short description : X Video Extension.]])

help([[Name   : videoproto]])
help([[Version: 2.3.3]])
help([[Target : broadwell]])
help()
help([[X Video Extension. This extension provides a protocol for a video output
mechanism, mainly to rescale video playback in the video controller
hardware.]])



prepend_path("PKG_CONFIG_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/videoproto-2.3.3-4i52pqezgti7ivshilfbtyfhmj66jv6k/lib/pkgconfig", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/videoproto-2.3.3-4i52pqezgti7ivshilfbtyfhmj66jv6k/.", ":")

