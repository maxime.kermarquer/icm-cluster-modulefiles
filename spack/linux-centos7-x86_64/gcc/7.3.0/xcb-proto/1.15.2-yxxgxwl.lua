-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2023-05-09 15:47:21.602223
--
-- xcb-proto@1.15.2%gcc@7.3.0 build_system=autotools arch=linux-centos7-broadwell/yxxgxwl
--

whatis([[Name : xcb-proto]])
whatis([[Version : 1.15.2]])
whatis([[Target : broadwell]])
whatis([[Short description : xcb-proto provides the XML-XCB protocol descriptions that libxcb uses to generate the majority of its code and API.]])

help([[Name   : xcb-proto]])
help([[Version: 1.15.2]])
help([[Target : broadwell]])
help()
help([[xcb-proto provides the XML-XCB protocol descriptions that libxcb uses to
generate the majority of its code and API.]])



prepend_path("PKG_CONFIG_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/xcb-proto-1.15.2-yxxgxwlq4ecg3adn6tuezr722uww5tt3/share/pkgconfig", ":")
prepend_path("LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/xcb-proto-1.15.2-yxxgxwlq4ecg3adn6tuezr722uww5tt3/lib", ":")
prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/xcb-proto-1.15.2-yxxgxwlq4ecg3adn6tuezr722uww5tt3/lib", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/xcb-proto-1.15.2-yxxgxwlq4ecg3adn6tuezr722uww5tt3/.", ":")

