-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2023-05-11 10:59:16.351772
--
-- xcmiscproto@1.2.2%gcc@7.3.0 build_system=autotools arch=linux-centos7-broadwell/q563fci
--

whatis([[Name : xcmiscproto]])
whatis([[Version : 1.2.2]])
whatis([[Target : broadwell]])
whatis([[Short description : XC-MISC Extension.]])

help([[Name   : xcmiscproto]])
help([[Version: 1.2.2]])
help([[Target : broadwell]])
help()
help([[XC-MISC Extension. This extension defines a protocol that provides Xlib
two ways to query the server for available resource IDs.]])



prepend_path("PKG_CONFIG_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/xcmiscproto-1.2.2-q563fcir3oy6gkgvuu4vkyxyqgsmavvc/lib/pkgconfig", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/xcmiscproto-1.2.2-q563fcir3oy6gkgvuu4vkyxyqgsmavvc/.", ":")

