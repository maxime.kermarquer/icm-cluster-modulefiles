-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2023-05-09 15:48:44.006143
--
-- xextproto@7.3.0%gcc@7.3.0 build_system=autotools arch=linux-centos7-broadwell/jmtrxor
--

whatis([[Name : xextproto]])
whatis([[Version : 7.3.0]])
whatis([[Target : broadwell]])
whatis([[Short description : X Protocol Extensions.]])

help([[Name   : xextproto]])
help([[Version: 7.3.0]])
help([[Target : broadwell]])
help()
help([[X Protocol Extensions.]])



prepend_path("PKG_CONFIG_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/xextproto-7.3.0-jmtrxoryllff2ducuvm4n367hdkpwnj3/lib/pkgconfig", ":")
prepend_path("CPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/xextproto-7.3.0-jmtrxoryllff2ducuvm4n367hdkpwnj3/include", ":")
prepend_path("LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/xextproto-7.3.0-jmtrxoryllff2ducuvm4n367hdkpwnj3/lib", ":")
prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/xextproto-7.3.0-jmtrxoryllff2ducuvm4n367hdkpwnj3/lib", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/xextproto-7.3.0-jmtrxoryllff2ducuvm4n367hdkpwnj3/.", ":")

