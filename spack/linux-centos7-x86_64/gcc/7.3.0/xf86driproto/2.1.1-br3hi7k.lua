-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2023-05-11 10:58:36.589573
--
-- xf86driproto@2.1.1%gcc@7.3.0 build_system=autotools arch=linux-centos7-broadwell/br3hi7k
--

whatis([[Name : xf86driproto]])
whatis([[Version : 2.1.1]])
whatis([[Target : broadwell]])
whatis([[Short description : XFree86 Direct Rendering Infrastructure Extension.]])

help([[Name   : xf86driproto]])
help([[Version: 2.1.1]])
help([[Target : broadwell]])
help()
help([[XFree86 Direct Rendering Infrastructure Extension. This extension
defines a protocol to allow user applications to access the video
hardware without requiring data to be passed through the X server.]])



prepend_path("PKG_CONFIG_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/xf86driproto-2.1.1-br3hi7ko7xejdsu56h6n7dt3f6z2ja55/lib/pkgconfig", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/xf86driproto-2.1.1-br3hi7ko7xejdsu56h6n7dt3f6z2ja55/.", ":")

