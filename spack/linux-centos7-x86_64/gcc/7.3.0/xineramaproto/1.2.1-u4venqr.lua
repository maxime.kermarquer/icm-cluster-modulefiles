-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2023-05-11 10:59:02.299466
--
-- xineramaproto@1.2.1%gcc@7.3.0 build_system=autotools arch=linux-centos7-broadwell/u4venqr
--

whatis([[Name : xineramaproto]])
whatis([[Version : 1.2.1]])
whatis([[Target : broadwell]])
whatis([[Short description : X Xinerama Extension.]])

help([[Name   : xineramaproto]])
help([[Version: 1.2.1]])
help([[Target : broadwell]])
help()
help([[X Xinerama Extension. This is an X extension that allows multiple
physical screens controlled by a single X server to appear as a single
screen.]])



prepend_path("PKG_CONFIG_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/xineramaproto-1.2.1-u4venqrw6x6qqdazknuvbpitrhskxum4/lib/pkgconfig", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/xineramaproto-1.2.1-u4venqrw6x6qqdazknuvbpitrhskxum4/.", ":")

