-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2023-05-09 15:47:44.857443
--
-- xproto@7.0.31%gcc@7.3.0 build_system=autotools arch=linux-centos7-broadwell/2iu6dcc
--

whatis([[Name : xproto]])
whatis([[Version : 7.0.31]])
whatis([[Target : broadwell]])
whatis([[Short description : X Window System Core Protocol.]])

help([[Name   : xproto]])
help([[Version: 7.0.31]])
help([[Target : broadwell]])
help()
help([[X Window System Core Protocol. This package provides the headers and
specification documents defining the X Window System Core Protocol,
Version 11. It also includes a number of headers that aren't purely
protocol related, but are depended upon by many other X Window System
packages to provide common definitions and porting layer.]])



prepend_path("CPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/xproto-7.0.31-2iu6dcctshvz5p4zefy56mih4hiukayt/include", ":")
prepend_path("LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/xproto-7.0.31-2iu6dcctshvz5p4zefy56mih4hiukayt/lib", ":")
prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/xproto-7.0.31-2iu6dcctshvz5p4zefy56mih4hiukayt/lib", ":")
prepend_path("PKG_CONFIG_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/xproto-7.0.31-2iu6dcctshvz5p4zefy56mih4hiukayt/lib/pkgconfig", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/xproto-7.0.31-2iu6dcctshvz5p4zefy56mih4hiukayt/.", ":")

