-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2023-05-09 15:48:02.832765
--
-- xtrans@1.4.0%gcc@7.3.0 build_system=autotools arch=linux-centos7-broadwell/g2tksfe
--

whatis([[Name : xtrans]])
whatis([[Version : 1.4.0]])
whatis([[Target : broadwell]])
whatis([[Short description : xtrans is a library of code that is shared among various X packages to handle network protocol transport in a modular fashion, allowing a single place to add new transport types. It is used by the X server, libX11, libICE, the X font server, and related components.]])

help([[Name   : xtrans]])
help([[Version: 1.4.0]])
help([[Target : broadwell]])
help()
help([[xtrans is a library of code that is shared among various X packages to
handle network protocol transport in a modular fashion, allowing a
single place to add new transport types. It is used by the X server,
libX11, libICE, the X font server, and related components.]])



prepend_path("CPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/xtrans-1.4.0-g2tksfedbvqnxu5dtrv3lcme6bf5eoza/include", ":")
prepend_path("ACLOCAL_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/xtrans-1.4.0-g2tksfedbvqnxu5dtrv3lcme6bf5eoza/share/aclocal", ":")
prepend_path("PKG_CONFIG_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/xtrans-1.4.0-g2tksfedbvqnxu5dtrv3lcme6bf5eoza/share/pkgconfig", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/xtrans-1.4.0-g2tksfedbvqnxu5dtrv3lcme6bf5eoza/.", ":")

