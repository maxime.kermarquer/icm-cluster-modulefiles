-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2023-05-04 15:42:21.241865
--
-- xz@5.4.1%gcc@7.3.0~pic build_system=autotools libs=shared,static arch=linux-centos7-broadwell/pthvj6a
--

whatis([[Name : xz]])
whatis([[Version : 5.4.1]])
whatis([[Target : broadwell]])
whatis([[Short description : XZ Utils is free general-purpose data compression software with high compression ratio. XZ Utils were written for POSIX-like systems, but also work on some not-so-POSIX systems. XZ Utils are the successor to LZMA Utils.]])

help([[Name   : xz]])
help([[Version: 5.4.1]])
help([[Target : broadwell]])
help()
help([[XZ Utils is free general-purpose data compression software with high
compression ratio. XZ Utils were written for POSIX-like systems, but
also work on some not-so-POSIX systems. XZ Utils are the successor to
LZMA Utils.]])



prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/xz-5.4.1-pthvj6auvqjxwcbmuif5za7acqovwri5/bin", ":")
prepend_path("CPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/xz-5.4.1-pthvj6auvqjxwcbmuif5za7acqovwri5/include", ":")
prepend_path("LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/xz-5.4.1-pthvj6auvqjxwcbmuif5za7acqovwri5/lib", ":")
prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/xz-5.4.1-pthvj6auvqjxwcbmuif5za7acqovwri5/lib", ":")
prepend_path("MANPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/xz-5.4.1-pthvj6auvqjxwcbmuif5za7acqovwri5/share/man", ":")
prepend_path("PKG_CONFIG_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/xz-5.4.1-pthvj6auvqjxwcbmuif5za7acqovwri5/lib/pkgconfig", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/xz-5.4.1-pthvj6auvqjxwcbmuif5za7acqovwri5/.", ":")

