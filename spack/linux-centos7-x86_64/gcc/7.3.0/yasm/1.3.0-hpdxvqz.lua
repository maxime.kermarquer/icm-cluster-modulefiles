-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2023-05-10 15:57:23.324449
--
-- yasm@1.3.0%gcc@7.3.0 build_system=autotools arch=linux-centos7-broadwell/hpdxvqz
--

whatis([[Name : yasm]])
whatis([[Version : 1.3.0]])
whatis([[Target : broadwell]])
whatis([[Short description : Yasm is a complete rewrite of the NASM-2.11.06 assembler. It supports the x86 and AMD64 instruction sets, accepts NASM and GAS assembler syntaxes and outputs binary, ELF32 and ELF64 object formats.]])

help([[Name   : yasm]])
help([[Version: 1.3.0]])
help([[Target : broadwell]])
help()
help([[Yasm is a complete rewrite of the NASM-2.11.06 assembler. It supports
the x86 and AMD64 instruction sets, accepts NASM and GAS assembler
syntaxes and outputs binary, ELF32 and ELF64 object formats.]])



prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/yasm-1.3.0-hpdxvqztzpvvriqczozr2wihj7ijb3sv/bin", ":")
prepend_path("MANPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/yasm-1.3.0-hpdxvqztzpvvriqczozr2wihj7ijb3sv/share/man", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/yasm-1.3.0-hpdxvqztzpvvriqczozr2wihj7ijb3sv/.", ":")

