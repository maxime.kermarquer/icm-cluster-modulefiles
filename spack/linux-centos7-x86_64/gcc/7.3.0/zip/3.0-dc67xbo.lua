-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2023-05-10 09:26:56.769122
--
-- zip@3.0%gcc@7.3.0 build_system=makefile patches=14dc880,3bc30ba,5068e7c,51f48db,66ab4ce,a92fc4e,a95ed93,eb83fc8,f7d0bc4,fa8312c arch=linux-centos7-broadwell/dc67xbo
--

whatis([[Name : zip]])
whatis([[Version : 3.0]])
whatis([[Target : broadwell]])
whatis([[Short description : Zip is a compression and file packaging/archive utility.]])

help([[Name   : zip]])
help([[Version: 3.0]])
help([[Target : broadwell]])
help()
help([[Zip is a compression and file packaging/archive utility.]])


depends_on("bzip2/1.0.8-zl4fo3v")

prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/zip-3.0-dc67xboffv5o5fidtftssl42uwqs4ocr/bin", ":")
prepend_path("MANPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/zip-3.0-dc67xboffv5o5fidtftssl42uwqs4ocr/man", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/zip-3.0-dc67xboffv5o5fidtftssl42uwqs4ocr/.", ":")

