-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2023-05-04 14:42:50.879356
--
-- zlib@1.2.13%gcc@7.3.0+optimize+pic+shared build_system=makefile arch=linux-centos7-broadwell/7shounl
--

whatis([[Name : zlib]])
whatis([[Version : 1.2.13]])
whatis([[Target : broadwell]])
whatis([[Short description : A free, general-purpose, legally unencumbered lossless data-compression library. ]])

help([[Name   : zlib]])
help([[Version: 1.2.13]])
help([[Target : broadwell]])
help()
help([[A free, general-purpose, legally unencumbered lossless data-compression
library.]])



prepend_path("CPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/zlib-1.2.13-7shounlfxhfmsqyucuh5nfp447lvnxn4/include", ":")
prepend_path("LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/zlib-1.2.13-7shounlfxhfmsqyucuh5nfp447lvnxn4/lib", ":")
prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/zlib-1.2.13-7shounlfxhfmsqyucuh5nfp447lvnxn4/lib", ":")
prepend_path("MANPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/zlib-1.2.13-7shounlfxhfmsqyucuh5nfp447lvnxn4/share/man", ":")
prepend_path("PKG_CONFIG_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/zlib-1.2.13-7shounlfxhfmsqyucuh5nfp447lvnxn4/lib/pkgconfig", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/zlib-1.2.13-7shounlfxhfmsqyucuh5nfp447lvnxn4/.", ":")

