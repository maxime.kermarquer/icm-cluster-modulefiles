-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2023-05-10 09:39:57.495670
--
-- zziplib@0.13.72%gcc@7.3.0~ipo build_system=cmake build_type=RelWithDebInfo generator=make arch=linux-centos7-broadwell/lsntgk3
--

whatis([[Name : zziplib]])
whatis([[Version : 0.13.72]])
whatis([[Target : broadwell]])
whatis([[Short description : The zziplib provides read access to zipped files in a zip-archive, using compression based solely on free algorithms provided by zlib. It also provides a functionality to overlay the archive filesystem with the filesystem of the operating system environment.]])

help([[Name   : zziplib]])
help([[Version: 0.13.72]])
help([[Target : broadwell]])
help()
help([[The zziplib provides read access to zipped files in a zip-archive, using
compression based solely on free algorithms provided by zlib. It also
provides a functionality to overlay the archive filesystem with the
filesystem of the operating system environment.]])


depends_on("zlib/1.2.13-7shounl")

prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/zziplib-0.13.72-lsntgk3oeu2xvfgq2slzoe2k3pzmhpzz/bin", ":")
prepend_path("MANPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/zziplib-0.13.72-lsntgk3oeu2xvfgq2slzoe2k3pzmhpzz/share/man", ":")
prepend_path("ACLOCAL_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/zziplib-0.13.72-lsntgk3oeu2xvfgq2slzoe2k3pzmhpzz/share/aclocal", ":")
prepend_path("PKG_CONFIG_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/zziplib-0.13.72-lsntgk3oeu2xvfgq2slzoe2k3pzmhpzz/lib64/pkgconfig", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/zziplib-0.13.72-lsntgk3oeu2xvfgq2slzoe2k3pzmhpzz/.", ":")

