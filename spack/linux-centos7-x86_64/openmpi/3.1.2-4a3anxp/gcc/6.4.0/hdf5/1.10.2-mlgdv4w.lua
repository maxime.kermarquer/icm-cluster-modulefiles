-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2018-10-18 15:43:57.405454
--
-- hdf5@1.10.2%gcc@6.4.0~cxx~debug~fortran~hl+mpi+pic+shared~szip~threadsafe arch=linux-centos7-x86_64 /mlgdv4w
--

whatis([[Name : hdf5]])
whatis([[Version : 1.10.2]])
whatis([[Short description : HDF5 is a data model, library, and file format for storing and managing data. It supports an unlimited variety of datatypes, and is designed for flexible and efficient I/O and for high volume and complex data. ]])
whatis([[Configure options : --enable-unsupported --disable-threadsafe --disable-cxx --disable-hl --disable-fortran --without-szlib --enable-build-mode=production --enable-shared CFLAGS=-fPIC CXXFLAGS=-fPIC FCFLAGS=-fPIC --enable-parallel CC=/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/openmpi-3.1.2-4a3anxpyy6wrfznuarkcfzlijfpwd577/bin/mpicc --with-zlib=/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/zlib-1.2.11-ufwqfd4zrwuuu7fmguamaqokn5plxyns]])

help([[HDF5 is a data model, library, and file format for storing and managing
data. It supports an unlimited variety of datatypes, and is designed for
flexible and efficient I/O and for high volume and complex data.]])



prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/hdf5-1.10.2-mlgdv4w74juzgpcmeql45ztu7ydcqz53/bin", ":")
prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/hdf5-1.10.2-mlgdv4w74juzgpcmeql45ztu7ydcqz53/lib", ":")
prepend_path("LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/hdf5-1.10.2-mlgdv4w74juzgpcmeql45ztu7ydcqz53/lib", ":")
prepend_path("CPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/hdf5-1.10.2-mlgdv4w74juzgpcmeql45ztu7ydcqz53/include", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/hdf5-1.10.2-mlgdv4w74juzgpcmeql45ztu7ydcqz53/", ":")
prepend_path("CPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/libxml2-2.9.8-m3rdnxm66wlzs5gqsm5pqlzqvideutnk/include/libxml2", ":")

