-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2018-10-18 15:44:19.608948
--
-- matio@1.5.9%gcc@6.4.0+hdf5+shared+zlib arch=linux-centos7-x86_64 /zj56han
--

whatis([[Name : matio]])
whatis([[Version : 1.5.9]])
whatis([[Short description : matio is an C library for reading and writing Matlab MAT files]])
whatis([[Configure options : --with-zlib=/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/zlib-1.2.11-ufwqfd4zrwuuu7fmguamaqokn5plxyns --with-hdf5=/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/hdf5-1.10.2-mlgdv4w74juzgpcmeql45ztu7ydcqz53]])

help([[matio is an C library for reading and writing Matlab MAT files]])



prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/matio-1.5.9-zj56hanqarh45dyqt6yx3nnj6jluzrl7/bin", ":")
prepend_path("MANPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/matio-1.5.9-zj56hanqarh45dyqt6yx3nnj6jluzrl7/share/man", ":")
prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/matio-1.5.9-zj56hanqarh45dyqt6yx3nnj6jluzrl7/lib", ":")
prepend_path("LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/matio-1.5.9-zj56hanqarh45dyqt6yx3nnj6jluzrl7/lib", ":")
prepend_path("CPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/matio-1.5.9-zj56hanqarh45dyqt6yx3nnj6jluzrl7/include", ":")
prepend_path("PKG_CONFIG_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/matio-1.5.9-zj56hanqarh45dyqt6yx3nnj6jluzrl7/lib/pkgconfig", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/matio-1.5.9-zj56hanqarh45dyqt6yx3nnj6jluzrl7/", ":")
prepend_path("CPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/libxml2-2.9.8-m3rdnxm66wlzs5gqsm5pqlzqvideutnk/include/libxml2", ":")

