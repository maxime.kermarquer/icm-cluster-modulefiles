-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2020-12-12 16:39:35.125037
--
-- dtcmp@1.0.3%gcc@6.4.0 arch=linux-centos7-x86_64 /bajug4r
--

whatis([[Name : dtcmp]])
whatis([[Version : 1.0.3]])
whatis([[Short description : The Datatype Comparison Library provides comparison operations and parallel sort algorithms for MPI applications.]])

help([[The Datatype Comparison Library provides comparison operations and
parallel sort algorithms for MPI applications.]])



prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/dtcmp-1.0.3-bajug4rmqmn7hqlpn22yu5mfdh2df3zv/lib", ":")
prepend_path("LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/dtcmp-1.0.3-bajug4rmqmn7hqlpn22yu5mfdh2df3zv/lib", ":")
prepend_path("CPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/dtcmp-1.0.3-bajug4rmqmn7hqlpn22yu5mfdh2df3zv/include", ":")
prepend_path("PKG_CONFIG_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/dtcmp-1.0.3-bajug4rmqmn7hqlpn22yu5mfdh2df3zv/lib/pkgconfig", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/dtcmp-1.0.3-bajug4rmqmn7hqlpn22yu5mfdh2df3zv/", ":")
prepend_path("CPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/libxml2-2.9.8-m3rdnxm66wlzs5gqsm5pqlzqvideutnk/include/libxml2", ":")

