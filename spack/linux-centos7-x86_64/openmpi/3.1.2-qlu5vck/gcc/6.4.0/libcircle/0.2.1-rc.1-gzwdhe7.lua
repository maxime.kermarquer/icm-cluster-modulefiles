-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2020-12-12 16:41:21.468251
--
-- libcircle@0.2.1-rc.1%gcc@6.4.0 arch=linux-centos7-x86_64 /gzwdhe7
--

whatis([[Name : libcircle]])
whatis([[Version : 0.2.1-rc.1]])
whatis([[Short description : libcircle provides an efficient distributed queue on a cluster, using self-stabilizing work stealing.]])

help([[libcircle provides an efficient distributed queue on a cluster, using
self-stabilizing work stealing.]])



prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/libcircle-0.2.1-rc.1-gzwdhe7357oswsax6r27texdz7csncw5/lib", ":")
prepend_path("LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/libcircle-0.2.1-rc.1-gzwdhe7357oswsax6r27texdz7csncw5/lib", ":")
prepend_path("CPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/libcircle-0.2.1-rc.1-gzwdhe7357oswsax6r27texdz7csncw5/include", ":")
prepend_path("PKG_CONFIG_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/libcircle-0.2.1-rc.1-gzwdhe7357oswsax6r27texdz7csncw5/lib/pkgconfig", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/libcircle-0.2.1-rc.1-gzwdhe7357oswsax6r27texdz7csncw5/", ":")
prepend_path("CPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/libxml2-2.9.8-m3rdnxm66wlzs5gqsm5pqlzqvideutnk/include/libxml2", ":")

