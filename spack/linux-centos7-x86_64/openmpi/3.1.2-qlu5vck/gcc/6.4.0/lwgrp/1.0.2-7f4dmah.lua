-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2020-12-12 16:39:23.798894
--
-- lwgrp@1.0.2%gcc@6.4.0 arch=linux-centos7-x86_64 /7f4dmah
--

whatis([[Name : lwgrp]])
whatis([[Version : 1.0.2]])
whatis([[Short description : Thie light-weight group library provides process group representations using O(log N) space and time.]])

help([[Thie light-weight group library provides process group representations
using O(log N) space and time.]])



prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/lwgrp-1.0.2-7f4dmahghqdd6csu3kpinmxmicwz7ura/lib", ":")
prepend_path("LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/lwgrp-1.0.2-7f4dmahghqdd6csu3kpinmxmicwz7ura/lib", ":")
prepend_path("CPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/lwgrp-1.0.2-7f4dmahghqdd6csu3kpinmxmicwz7ura/include", ":")
prepend_path("PKG_CONFIG_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/lwgrp-1.0.2-7f4dmahghqdd6csu3kpinmxmicwz7ura/lib/pkgconfig", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/lwgrp-1.0.2-7f4dmahghqdd6csu3kpinmxmicwz7ura/", ":")
prepend_path("CPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/libxml2-2.9.8-m3rdnxm66wlzs5gqsm5pqlzqvideutnk/include/libxml2", ":")

