-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2020-12-27 23:12:34.199761
--
-- mpifileutils@0.7%gcc@6.4.0+experimental+lustre+xattr arch=linux-centos7-x86_64 /wpzrafi
--

whatis([[Name : mpifileutils]])
whatis([[Version : 0.7]])
whatis([[Short description : mpiFileUtils is a suite of MPI-based tools to manage large datasets, which may vary from large directory trees to large files. High-performance computing users often generate large datasets with parallel applications that run with many processes (millions in some cases). However those users are then stuck with single-process tools like cp and rm to manage their datasets. This suite provides MPI-based tools to handle typical jobs like copy, remove, and compare for such datasets, providing speedups of up to 20-30x.]])
whatis([[Configure options : --enable-lustre --enable-experimental]])

help([[mpiFileUtils is a suite of MPI-based tools to manage large datasets,
which may vary from large directory trees to large files. High-
performance computing users often generate large datasets with parallel
applications that run with many processes (millions in some cases).
However those users are then stuck with single-process tools like cp and
rm to manage their datasets. This suite provides MPI-based tools to
handle typical jobs like copy, remove, and compare for such datasets,
providing speedups of up to 20-30x.]])



prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/mpifileutils-0.7-wpzraficrm626snsyem5r55rj5jmgl63/bin", ":")
prepend_path("MANPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/mpifileutils-0.7-wpzraficrm626snsyem5r55rj5jmgl63/share/man", ":")
prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/mpifileutils-0.7-wpzraficrm626snsyem5r55rj5jmgl63/lib", ":")
prepend_path("LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/mpifileutils-0.7-wpzraficrm626snsyem5r55rj5jmgl63/lib", ":")
prepend_path("CPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/mpifileutils-0.7-wpzraficrm626snsyem5r55rj5jmgl63/include", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/mpifileutils-0.7-wpzraficrm626snsyem5r55rj5jmgl63/", ":")
prepend_path("CPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-x86_64/gcc-6.4.0/libxml2-2.9.8-m3rdnxm66wlzs5gqsm5pqlzqvideutnk/include/libxml2", ":")

