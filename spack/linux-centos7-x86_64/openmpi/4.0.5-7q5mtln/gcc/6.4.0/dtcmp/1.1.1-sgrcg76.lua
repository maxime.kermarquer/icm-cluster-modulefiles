-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2021-01-13 19:29:05.669251
--
-- dtcmp@1.1.1%gcc@6.4.0 arch=linux-centos7-broadwell/sgrcg76
--

whatis([[Name : dtcmp]])
whatis([[Version : 1.1.1]])
whatis([[Target : broadwell]])
whatis([[Short description : The Datatype Comparison Library provides comparison operations and parallel sort algorithms for MPI applications.]])
whatis([[Configure options : --with-lwgrp=/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/lwgrp-1.0.3-rlsp5p5x2vgu2eip3m7txwl7r2xfqu5d]])

help([[The Datatype Comparison Library provides comparison operations and
parallel sort algorithms for MPI applications.]])



prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/dtcmp-1.1.1-sgrcg76pjtppnb4qtool3ij7zub4rjiz/lib", ":")
prepend_path("LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/dtcmp-1.1.1-sgrcg76pjtppnb4qtool3ij7zub4rjiz/lib", ":")
prepend_path("C_INCLUDE_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/dtcmp-1.1.1-sgrcg76pjtppnb4qtool3ij7zub4rjiz/include", ":")
prepend_path("CPLUS_INCLUDE_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/dtcmp-1.1.1-sgrcg76pjtppnb4qtool3ij7zub4rjiz/include", ":")
prepend_path("INCLUDE", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/dtcmp-1.1.1-sgrcg76pjtppnb4qtool3ij7zub4rjiz/include", ":")
prepend_path("PKG_CONFIG_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/dtcmp-1.1.1-sgrcg76pjtppnb4qtool3ij7zub4rjiz/lib/pkgconfig", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/dtcmp-1.1.1-sgrcg76pjtppnb4qtool3ij7zub4rjiz/", ":")

