-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2022-05-27 15:19:00.531542
--
-- hdf5@1.10.7%gcc@6.4.0~cxx~debug~fortran+hl~java+mpi+pic+shared~szip~threadsafe api=none arch=linux-centos7-broadwell/iwmtcn2
--

whatis([[Name : hdf5]])
whatis([[Version : 1.10.7]])
whatis([[Target : broadwell]])
whatis([[Short description : HDF5 is a data model, library, and file format for storing and managing data. It supports an unlimited variety of datatypes, and is designed for flexible and efficient I/O and for high volume and complex data. ]])
whatis([[Configure options : --enable-unsupported --enable-symbols=yes --disable-threadsafe --disable-cxx --enable-hl --disable-fortran --disable-java --without-szlib --enable-build-mode=production --enable-shared CFLAGS=-fPIC CXXFLAGS=-fPIC FCFLAGS=-fPIC --enable-parallel CC=/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/openmpi-4.0.5-7q5mtlntifhl2ttzzw4uy4kyo32o3pjk/bin/mpicc --with-zlib=/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/zlib-1.2.11-wp5daspjexwgd5rft64ekrbjk4uarol2]])

help([[HDF5 is a data model, library, and file format for storing and managing
data. It supports an unlimited variety of datatypes, and is designed for
flexible and efficient I/O and for high volume and complex data.]])



prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/hdf5-1.10.7-iwmtcn2pvikv3z7fcgsroq6tu63ynddd/lib", ":")
prepend_path("LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/hdf5-1.10.7-iwmtcn2pvikv3z7fcgsroq6tu63ynddd/lib", ":")
prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/hdf5-1.10.7-iwmtcn2pvikv3z7fcgsroq6tu63ynddd/bin", ":")
prepend_path("C_INCLUDE_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/hdf5-1.10.7-iwmtcn2pvikv3z7fcgsroq6tu63ynddd/include", ":")
prepend_path("CPLUS_INCLUDE_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/hdf5-1.10.7-iwmtcn2pvikv3z7fcgsroq6tu63ynddd/include", ":")
prepend_path("INCLUDE", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/hdf5-1.10.7-iwmtcn2pvikv3z7fcgsroq6tu63ynddd/include", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/hdf5-1.10.7-iwmtcn2pvikv3z7fcgsroq6tu63ynddd/", ":")

