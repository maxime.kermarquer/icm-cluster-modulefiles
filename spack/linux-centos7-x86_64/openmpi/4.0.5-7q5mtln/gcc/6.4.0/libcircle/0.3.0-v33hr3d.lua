-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2021-01-13 19:28:48.132987
--
-- libcircle@0.3.0%gcc@6.4.0 arch=linux-centos7-broadwell/v33hr3d
--

whatis([[Name : libcircle]])
whatis([[Version : 0.3.0]])
whatis([[Target : broadwell]])
whatis([[Short description : libcircle provides an efficient distributed queue on a cluster, using self-stabilizing work stealing.]])

help([[libcircle provides an efficient distributed queue on a cluster, using
self-stabilizing work stealing.]])



prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/libcircle-0.3.0-v33hr3dls3b4gj5h2lalogc4irxnckst/lib", ":")
prepend_path("LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/libcircle-0.3.0-v33hr3dls3b4gj5h2lalogc4irxnckst/lib", ":")
prepend_path("C_INCLUDE_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/libcircle-0.3.0-v33hr3dls3b4gj5h2lalogc4irxnckst/include", ":")
prepend_path("CPLUS_INCLUDE_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/libcircle-0.3.0-v33hr3dls3b4gj5h2lalogc4irxnckst/include", ":")
prepend_path("INCLUDE", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/libcircle-0.3.0-v33hr3dls3b4gj5h2lalogc4irxnckst/include", ":")
prepend_path("PKG_CONFIG_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/libcircle-0.3.0-v33hr3dls3b4gj5h2lalogc4irxnckst/lib/pkgconfig", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/libcircle-0.3.0-v33hr3dls3b4gj5h2lalogc4irxnckst/", ":")

