-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2021-01-13 19:28:32.391248
--
-- lwgrp@1.0.3%gcc@6.4.0 arch=linux-centos7-broadwell/rlsp5p5
--

whatis([[Name : lwgrp]])
whatis([[Version : 1.0.3]])
whatis([[Target : broadwell]])
whatis([[Short description : Thie light-weight group library provides process group representations using O(log N) space and time.]])

help([[Thie light-weight group library provides process group representations
using O(log N) space and time.]])



prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/lwgrp-1.0.3-rlsp5p5x2vgu2eip3m7txwl7r2xfqu5d/lib", ":")
prepend_path("LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/lwgrp-1.0.3-rlsp5p5x2vgu2eip3m7txwl7r2xfqu5d/lib", ":")
prepend_path("C_INCLUDE_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/lwgrp-1.0.3-rlsp5p5x2vgu2eip3m7txwl7r2xfqu5d/include", ":")
prepend_path("CPLUS_INCLUDE_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/lwgrp-1.0.3-rlsp5p5x2vgu2eip3m7txwl7r2xfqu5d/include", ":")
prepend_path("INCLUDE", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/lwgrp-1.0.3-rlsp5p5x2vgu2eip3m7txwl7r2xfqu5d/include", ":")
prepend_path("PKG_CONFIG_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/lwgrp-1.0.3-rlsp5p5x2vgu2eip3m7txwl7r2xfqu5d/lib/pkgconfig", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/lwgrp-1.0.3-rlsp5p5x2vgu2eip3m7txwl7r2xfqu5d/", ":")

