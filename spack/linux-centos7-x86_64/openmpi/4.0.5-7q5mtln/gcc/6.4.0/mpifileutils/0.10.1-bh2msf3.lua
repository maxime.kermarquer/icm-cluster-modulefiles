-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2021-01-13 19:32:47.427408
--
-- mpifileutils@0.10.1%gcc@6.4.0+experimental~gpfs+lustre+xattr arch=linux-centos7-broadwell/bh2msf3
--

whatis([[Name : mpifileutils]])
whatis([[Version : 0.10.1]])
whatis([[Target : broadwell]])
whatis([[Short description : mpiFileUtils is a suite of MPI-based tools to manage large datasets, which may vary from large directory trees to large files. High-performance computing users often generate large datasets with parallel applications that run with many processes (millions in some cases). However those users are then stuck with single-process tools like cp and rm to manage their datasets. This suite provides MPI-based tools to handle typical jobs like copy, remove, and compare for such datasets, providing speedups of up to 20-30x.]])
whatis([[Configure options : --prefix=/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/mpifileutils-0.10.1-bh2msf3rute3ok3m4oisqnrhcxq5cdve CPPFLAGS=-I/network/lustre/iss01/home/maxime.kermarquer/old-scratch/maxime.kermarquer/tmp/spack-stage-mpifileutils-0.10.1-bh2msf3rute3ok3m4oisqnrhcxq5cdve/spack-src/src/common libarchive_CFLAGS=-I/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/libarchive-3.5.1-ehwfqolp7siafzodfcuwqq6mzk7pswew/include libarchive_LIBS=-L/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/libarchive-3.5.1-ehwfqolp7siafzodfcuwqq6mzk7pswew/lib -larchive libcircle_CFLAGS=-I/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/libcircle-0.3.0-v33hr3dls3b4gj5h2lalogc4irxnckst/include libcircle_LIBS=-L/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/libcircle-0.3.0-v33hr3dls3b4gj5h2lalogc4irxnckst/lib -lcircle --with-dtcmp=/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/dtcmp-1.1.1-sgrcg76pjtppnb4qtool3ij7zub4rjiz CFLAGS=-DDCOPY_USE_XATTRS --enable-lustre --enable-experimental]])

help([[mpiFileUtils is a suite of MPI-based tools to manage large datasets,
which may vary from large directory trees to large files. High-
performance computing users often generate large datasets with parallel
applications that run with many processes (millions in some cases).
However those users are then stuck with single-process tools like cp and
rm to manage their datasets. This suite provides MPI-based tools to
handle typical jobs like copy, remove, and compare for such datasets,
providing speedups of up to 20-30x.]])



prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/mpifileutils-0.10.1-bh2msf3rute3ok3m4oisqnrhcxq5cdve/lib64", ":")
prepend_path("LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/mpifileutils-0.10.1-bh2msf3rute3ok3m4oisqnrhcxq5cdve/lib64", ":")
prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/mpifileutils-0.10.1-bh2msf3rute3ok3m4oisqnrhcxq5cdve/bin", ":")
prepend_path("MANPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/mpifileutils-0.10.1-bh2msf3rute3ok3m4oisqnrhcxq5cdve/share/man", ":")
prepend_path("C_INCLUDE_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/mpifileutils-0.10.1-bh2msf3rute3ok3m4oisqnrhcxq5cdve/include", ":")
prepend_path("CPLUS_INCLUDE_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/mpifileutils-0.10.1-bh2msf3rute3ok3m4oisqnrhcxq5cdve/include", ":")
prepend_path("INCLUDE", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/mpifileutils-0.10.1-bh2msf3rute3ok3m4oisqnrhcxq5cdve/include", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/mpifileutils-0.10.1-bh2msf3rute3ok3m4oisqnrhcxq5cdve/", ":")

