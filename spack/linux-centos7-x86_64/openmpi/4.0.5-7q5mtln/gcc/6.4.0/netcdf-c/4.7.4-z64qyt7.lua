-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2022-05-27 15:20:02.464810
--
-- netcdf-c@4.7.4%gcc@6.4.0~dap~hdf4~jna+mpi~parallel-netcdf+pic+shared arch=linux-centos7-broadwell/z64qyt7
--

whatis([[Name : netcdf-c]])
whatis([[Version : 4.7.4]])
whatis([[Target : broadwell]])
whatis([[Short description : NetCDF (network Common Data Form) is a set of software libraries and machine-independent data formats that support the creation, access, and sharing of array-oriented scientific data. This is the C distribution.]])
whatis([[Configure options : --enable-v2 --enable-utilities --enable-static --enable-largefile --enable-netcdf-4 --enable-fsync --enable-dynamic-loading --enable-shared --disable-dap --enable-parallel4 --disable-jna --disable-pnetcdf CC=/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/openmpi-4.0.5-7q5mtlntifhl2ttzzw4uy4kyo32o3pjk/bin/mpicc --disable-hdf4 CFLAGS=-fPIC CPPFLAGS=-I/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/hdf5-1.10.7-iwmtcn2pvikv3z7fcgsroq6tu63ynddd/include LDFLAGS=-L/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/hdf5-1.10.7-iwmtcn2pvikv3z7fcgsroq6tu63ynddd/lib LIBS=]])

help([[NetCDF (network Common Data Form) is a set of software libraries and
machine-independent data formats that support the creation, access, and
sharing of array-oriented scientific data. This is the C distribution.]])



prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/netcdf-c-4.7.4-z64qyt7zaffhwkqxcbowvp2jo3ciykc3/lib", ":")
prepend_path("LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/netcdf-c-4.7.4-z64qyt7zaffhwkqxcbowvp2jo3ciykc3/lib", ":")
prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/netcdf-c-4.7.4-z64qyt7zaffhwkqxcbowvp2jo3ciykc3/bin", ":")
prepend_path("MANPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/netcdf-c-4.7.4-z64qyt7zaffhwkqxcbowvp2jo3ciykc3/share/man", ":")
prepend_path("C_INCLUDE_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/netcdf-c-4.7.4-z64qyt7zaffhwkqxcbowvp2jo3ciykc3/include", ":")
prepend_path("CPLUS_INCLUDE_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/netcdf-c-4.7.4-z64qyt7zaffhwkqxcbowvp2jo3ciykc3/include", ":")
prepend_path("INCLUDE", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/netcdf-c-4.7.4-z64qyt7zaffhwkqxcbowvp2jo3ciykc3/include", ":")
prepend_path("PKG_CONFIG_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/netcdf-c-4.7.4-z64qyt7zaffhwkqxcbowvp2jo3ciykc3/lib/pkgconfig", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-6.4.0/netcdf-c-4.7.4-z64qyt7zaffhwkqxcbowvp2jo3ciykc3/", ":")

