-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2023-05-15 16:14:27.473696
--
-- fftw@3.3.10%gcc@7.3.0+mpi~openmp~pfft_patches build_system=autotools precision=double,float arch=linux-centos7-broadwell/6khy7eg
--

whatis([[Name : fftw]])
whatis([[Version : 3.3.10]])
whatis([[Target : broadwell]])
whatis([[Short description : FFTW is a C subroutine library for computing the discrete Fourier transform (DFT) in one or more dimensions, of arbitrary input size, and of both real and complex data (as well as of even/odd data, i.e. the discrete cosine/sine transforms or DCT/DST). We believe that FFTW, which is free software, should become the FFT library of choice for most applications.]])

help([[Name   : fftw]])
help([[Version: 3.3.10]])
help([[Target : broadwell]])
help()
help([[FFTW is a C subroutine library for computing the discrete Fourier
transform (DFT) in one or more dimensions, of arbitrary input size, and
of both real and complex data (as well as of even/odd data, i.e. the
discrete cosine/sine transforms or DCT/DST). We believe that FFTW, which
is free software, should become the FFT library of choice for most
applications.]])


depends_on("linux-centos7-x86_64/gcc/7.3.0/openmpi/4.1.5-pk4dxwe")

prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/fftw-3.3.10-6khy7eg5cbychjbpbmuyvnsmxbz2dkcb/bin", ":")
prepend_path("CPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/fftw-3.3.10-6khy7eg5cbychjbpbmuyvnsmxbz2dkcb/include", ":")
prepend_path("LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/fftw-3.3.10-6khy7eg5cbychjbpbmuyvnsmxbz2dkcb/lib", ":")
prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/fftw-3.3.10-6khy7eg5cbychjbpbmuyvnsmxbz2dkcb/lib", ":")
prepend_path("MANPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/fftw-3.3.10-6khy7eg5cbychjbpbmuyvnsmxbz2dkcb/share/man", ":")
prepend_path("PKG_CONFIG_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/fftw-3.3.10-6khy7eg5cbychjbpbmuyvnsmxbz2dkcb/lib/pkgconfig", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/fftw-3.3.10-6khy7eg5cbychjbpbmuyvnsmxbz2dkcb/.", ":")

