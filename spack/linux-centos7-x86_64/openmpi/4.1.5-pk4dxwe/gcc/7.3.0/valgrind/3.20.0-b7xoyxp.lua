-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2023-06-15 13:27:16.013481
--
-- valgrind@3.20.0%gcc@7.3.0+boost+mpi+only64bit~ubsan build_system=autotools libs=shared,static arch=linux-centos7-broadwell/b7xoyxp
--

whatis([[Name : valgrind]])
whatis([[Version : 3.20.0]])
whatis([[Target : broadwell]])
whatis([[Short description : An instrumentation framework for building dynamic analysis.]])
whatis([[Configure options : --enable-shared --enable-static --enable-only64bit]])

help([[Name   : valgrind]])
help([[Version: 3.20.0]])
help([[Target : broadwell]])
help()
help([[An instrumentation framework for building dynamic analysis. There are
Valgrind tools that can automatically detect many memory management and
threading bugs, and profile your programs in detail. You can also use
Valgrind to build new tools. Valgrind is Open Source / Free Software,
and is freely available under the GNU General Public License, version 2.]])


depends_on("boost/1.77.0-3b5krc2")
depends_on("openmpi/4.1.5-pk4dxwe")

prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/valgrind-3.20.0-b7xoyxpz3a22fwag5ke2icuix3oeyy2j/bin", ":")
prepend_path("MANPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/valgrind-3.20.0-b7xoyxpz3a22fwag5ke2icuix3oeyy2j/share/man", ":")
prepend_path("PKG_CONFIG_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/valgrind-3.20.0-b7xoyxpz3a22fwag5ke2icuix3oeyy2j/lib/pkgconfig", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-centos7-broadwell/gcc-7.3.0/valgrind-3.20.0-b7xoyxpz3a22fwag5ke2icuix3oeyy2j/.", ":")

