-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2018-08-28 11:46:17.242610
--
-- autoconf@2.69%gcc@5.4.0 arch=linux-linuxmint18-x86_64 /g3ihbjx
--

whatis([[Name : autoconf]])
whatis([[Version : 2.69]])
whatis([[Short description : Autoconf -- system configuration part of autotools]])

help([[Autoconf -- system configuration part of autotools]])



prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/autoconf-2.69-g3ihbjxgy4kry6aa56wxjdfcvt4mxigo/bin", ":")
prepend_path("MANPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/autoconf-2.69-g3ihbjxgy4kry6aa56wxjdfcvt4mxigo/share/man", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/autoconf-2.69-g3ihbjxgy4kry6aa56wxjdfcvt4mxigo/", ":")

