-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2018-08-28 11:46:21.374485
--
-- automake@1.16.1%gcc@5.4.0 arch=linux-linuxmint18-x86_64 /o4hr3fs
--

whatis([[Name : automake]])
whatis([[Version : 1.16.1]])
whatis([[Short description : Automake -- make file builder part of autotools]])

help([[Automake -- make file builder part of autotools]])



prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/automake-1.16.1-o4hr3fs5fbx4ewaz6t3tzwmlqyen4luq/bin", ":")
prepend_path("MANPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/automake-1.16.1-o4hr3fs5fbx4ewaz6t3tzwmlqyen4luq/share/man", ":")
prepend_path("ACLOCAL_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/automake-1.16.1-o4hr3fs5fbx4ewaz6t3tzwmlqyen4luq/share/aclocal", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/automake-1.16.1-o4hr3fs5fbx4ewaz6t3tzwmlqyen4luq/", ":")

