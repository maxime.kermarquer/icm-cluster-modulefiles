-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2018-08-31 10:58:34.160630
--
-- bison@3.0.5%gcc@5.4.0 arch=linux-linuxmint18-x86_64 /ihrvdzi
--

whatis([[Name : bison]])
whatis([[Version : 3.0.5]])
whatis([[Short description : Bison is a general-purpose parser generator that converts an annotated context-free grammar into a deterministic LR or generalized LR (GLR) parser employing LALR(1) parser tables.]])

help([[Bison is a general-purpose parser generator that converts an annotated
context-free grammar into a deterministic LR or generalized LR (GLR)
parser employing LALR(1) parser tables.]])



prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/bison-3.0.5-ihrvdziixs65q4woc5z7lrvtpanqt4bg/bin", ":")
prepend_path("MANPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/bison-3.0.5-ihrvdziixs65q4woc5z7lrvtpanqt4bg/share/man", ":")
prepend_path("ACLOCAL_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/bison-3.0.5-ihrvdziixs65q4woc5z7lrvtpanqt4bg/share/aclocal", ":")
prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/bison-3.0.5-ihrvdziixs65q4woc5z7lrvtpanqt4bg/lib", ":")
prepend_path("LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/bison-3.0.5-ihrvdziixs65q4woc5z7lrvtpanqt4bg/lib", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/bison-3.0.5-ihrvdziixs65q4woc5z7lrvtpanqt4bg/", ":")

