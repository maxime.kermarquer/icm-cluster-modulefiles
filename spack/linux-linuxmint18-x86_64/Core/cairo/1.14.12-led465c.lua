-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2018-09-20 15:19:31.151464
--
-- cairo@1.14.12%gcc@5.4.0~X arch=linux-linuxmint18-x86_64 /led465c
--

whatis([[Name : cairo]])
whatis([[Version : 1.14.12]])
whatis([[Short description : Cairo is a 2D graphics library with support for multiple output devices.]])
whatis([[Configure options : --disable-trace --enable-tee --disable-xlib --disable-xcb]])

help([[Cairo is a 2D graphics library with support for multiple output devices.]])



prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/cairo-1.14.12-led465c55j7pb3mww3ewtld5qlmcytrb/bin", ":")
prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/cairo-1.14.12-led465c55j7pb3mww3ewtld5qlmcytrb/lib", ":")
prepend_path("LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/cairo-1.14.12-led465c55j7pb3mww3ewtld5qlmcytrb/lib", ":")
prepend_path("CPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/cairo-1.14.12-led465c55j7pb3mww3ewtld5qlmcytrb/include", ":")
prepend_path("PKG_CONFIG_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/cairo-1.14.12-led465c55j7pb3mww3ewtld5qlmcytrb/lib/pkgconfig", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/cairo-1.14.12-led465c55j7pb3mww3ewtld5qlmcytrb/", ":")
prepend_path("CPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/libxml2-2.9.8-55hho2ks47dasihj2dsz3xg67rbssfsr/include/libxml2", ":")
prepend_path("CPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/freetype-2.7.1-4z3kpzdhhm623mmmrcnphm22rn4mi6o5/include/freetype2", ":")

