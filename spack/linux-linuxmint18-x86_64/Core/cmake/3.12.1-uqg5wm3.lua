-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2018-09-17 16:07:16.520646
--
-- cmake@3.12.1%gcc@5.4.0~doc+ncurses+openssl+ownlibs patches=dd3a40d4d92f6b2158b87d6fb354c277947c776424aa03f6dc8096cf3135f5d0 ~qt arch=linux-linuxmint18-x86_64 /uqg5wm3
--

whatis([[Name : cmake]])
whatis([[Version : 3.12.1]])
whatis([[Short description : A cross-platform, open-source build system. CMake is a family of tools designed to build, test and package software.]])

help([[A cross-platform, open-source build system. CMake is a family of tools
designed to build, test and package software.]])



prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/cmake-3.12.1-uqg5wm3gljbcaakjuxzenfoeby2ctvq3/bin", ":")
prepend_path("ACLOCAL_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/cmake-3.12.1-uqg5wm3gljbcaakjuxzenfoeby2ctvq3/share/aclocal", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/cmake-3.12.1-uqg5wm3gljbcaakjuxzenfoeby2ctvq3/", ":")

