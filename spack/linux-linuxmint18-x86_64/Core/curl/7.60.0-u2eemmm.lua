-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2018-08-28 11:48:36.111530
--
-- curl@7.60.0%gcc@5.4.0~darwinssl~libssh~libssh2~nghttp2 arch=linux-linuxmint18-x86_64 /u2eemmm
--

whatis([[Name : curl]])
whatis([[Version : 7.60.0]])
whatis([[Short description : cURL is an open source command line tool and library for transferring data with URL syntax]])
whatis([[Configure options : --with-zlib=/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/zlib-1.2.11-k5hg4kkxiutkfl6n53ogz5wnlbdrsdtf --with-ssl=/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/openssl-1.0.2o-5q4t4qkqw2nkza7pwqx4jbn42mpnxthe --without-nghttp2 --without-libssh2 --without-libssh]])

help([[cURL is an open source command line tool and library for transferring
data with URL syntax]])



prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/curl-7.60.0-u2eemmm5woui6xim3qc43xyges4mhytf/bin", ":")
prepend_path("MANPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/curl-7.60.0-u2eemmm5woui6xim3qc43xyges4mhytf/share/man", ":")
prepend_path("ACLOCAL_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/curl-7.60.0-u2eemmm5woui6xim3qc43xyges4mhytf/share/aclocal", ":")
prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/curl-7.60.0-u2eemmm5woui6xim3qc43xyges4mhytf/lib", ":")
prepend_path("LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/curl-7.60.0-u2eemmm5woui6xim3qc43xyges4mhytf/lib", ":")
prepend_path("CPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/curl-7.60.0-u2eemmm5woui6xim3qc43xyges4mhytf/include", ":")
prepend_path("PKG_CONFIG_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/curl-7.60.0-u2eemmm5woui6xim3qc43xyges4mhytf/lib/pkgconfig", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/curl-7.60.0-u2eemmm5woui6xim3qc43xyges4mhytf/", ":")

