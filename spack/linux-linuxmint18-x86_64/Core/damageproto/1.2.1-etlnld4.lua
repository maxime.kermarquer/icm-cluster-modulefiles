-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2018-08-31 11:06:57.686045
--
-- damageproto@1.2.1%gcc@5.4.0 arch=linux-linuxmint18-x86_64 /etlnld4
--

whatis([[Name : damageproto]])
whatis([[Version : 1.2.1]])
whatis([[Short description : X Damage Extension.]])

help([[X Damage Extension. This package contains header files and documentation
for the X Damage extension. Library and server implementations are
separate.]])



prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/damageproto-1.2.1-etlnld4xfb3okmgnspgysbmpvg5mpneg/lib", ":")
prepend_path("LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/damageproto-1.2.1-etlnld4xfb3okmgnspgysbmpvg5mpneg/lib", ":")
prepend_path("CPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/damageproto-1.2.1-etlnld4xfb3okmgnspgysbmpvg5mpneg/include", ":")
prepend_path("PKG_CONFIG_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/damageproto-1.2.1-etlnld4xfb3okmgnspgysbmpvg5mpneg/lib/pkgconfig", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/damageproto-1.2.1-etlnld4xfb3okmgnspgysbmpvg5mpneg/", ":")

