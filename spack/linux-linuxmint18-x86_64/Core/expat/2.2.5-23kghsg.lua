-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2018-08-28 11:48:57.819901
--
-- expat@2.2.5%gcc@5.4.0+libbsd arch=linux-linuxmint18-x86_64 /23kghsg
--

whatis([[Name : expat]])
whatis([[Version : 2.2.5]])
whatis([[Short description : Expat is an XML parser library written in C.]])
whatis([[Configure options : --with-libbsd]])

help([[Expat is an XML parser library written in C.]])



prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/expat-2.2.5-23kghsg3bsyurnducu5fi4kzpu44eiim/bin", ":")
prepend_path("MANPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/expat-2.2.5-23kghsg3bsyurnducu5fi4kzpu44eiim/share/man", ":")
prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/expat-2.2.5-23kghsg3bsyurnducu5fi4kzpu44eiim/lib", ":")
prepend_path("LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/expat-2.2.5-23kghsg3bsyurnducu5fi4kzpu44eiim/lib", ":")
prepend_path("CPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/expat-2.2.5-23kghsg3bsyurnducu5fi4kzpu44eiim/include", ":")
prepend_path("PKG_CONFIG_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/expat-2.2.5-23kghsg3bsyurnducu5fi4kzpu44eiim/lib/pkgconfig", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/expat-2.2.5-23kghsg3bsyurnducu5fi4kzpu44eiim/", ":")

