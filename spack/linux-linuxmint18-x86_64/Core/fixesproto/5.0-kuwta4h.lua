-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2018-08-31 11:07:01.354330
--
-- fixesproto@5.0%gcc@5.4.0 arch=linux-linuxmint18-x86_64 /kuwta4h
--

whatis([[Name : fixesproto]])
whatis([[Version : 5.0]])
whatis([[Short description : X Fixes Extension.]])

help([[X Fixes Extension. The extension makes changes to many areas of the
protocol to resolve issues raised by application interaction with core
protocol mechanisms that cannot be adequately worked around on the
client side of the wire.]])



prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/fixesproto-5.0-kuwta4hanmjpvqejqrkn7l4gnfxqdtbl/lib", ":")
prepend_path("LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/fixesproto-5.0-kuwta4hanmjpvqejqrkn7l4gnfxqdtbl/lib", ":")
prepend_path("CPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/fixesproto-5.0-kuwta4hanmjpvqejqrkn7l4gnfxqdtbl/include", ":")
prepend_path("PKG_CONFIG_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/fixesproto-5.0-kuwta4hanmjpvqejqrkn7l4gnfxqdtbl/lib/pkgconfig", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/fixesproto-5.0-kuwta4hanmjpvqejqrkn7l4gnfxqdtbl/", ":")

