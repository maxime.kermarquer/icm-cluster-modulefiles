-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2018-08-31 10:58:47.557175
--
-- flex@2.6.3%gcc@5.4.0+lex arch=linux-linuxmint18-x86_64 /ogph6zt
--

whatis([[Name : flex]])
whatis([[Version : 2.6.3]])
whatis([[Short description : Flex is a tool for generating scanners.]])

help([[Flex is a tool for generating scanners.]])



prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/flex-2.6.3-ogph6zteyihazef4nkrwh5ihbzapea3t/bin", ":")
prepend_path("MANPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/flex-2.6.3-ogph6zteyihazef4nkrwh5ihbzapea3t/share/man", ":")
prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/flex-2.6.3-ogph6zteyihazef4nkrwh5ihbzapea3t/lib", ":")
prepend_path("LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/flex-2.6.3-ogph6zteyihazef4nkrwh5ihbzapea3t/lib", ":")
prepend_path("CPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/flex-2.6.3-ogph6zteyihazef4nkrwh5ihbzapea3t/include", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/flex-2.6.3-ogph6zteyihazef4nkrwh5ihbzapea3t/", ":")

