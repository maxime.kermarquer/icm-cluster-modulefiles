-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2018-09-17 16:02:19.213191
--
-- font-util@1.3.1%gcc@5.4.0 arch=linux-linuxmint18-x86_64 /6vyuvjv
--

whatis([[Name : font-util]])
whatis([[Version : 1.3.1]])
whatis([[Short description : X.Org font package creation/installation utilities.]])

help([[X.Org font package creation/installation utilities.]])



prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/font-util-1.3.1-6vyuvjvx4kjh6gvn4dixje6mwfwqamv5/bin", ":")
prepend_path("MANPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/font-util-1.3.1-6vyuvjvx4kjh6gvn4dixje6mwfwqamv5/share/man", ":")
prepend_path("ACLOCAL_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/font-util-1.3.1-6vyuvjvx4kjh6gvn4dixje6mwfwqamv5/share/aclocal", ":")
prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/font-util-1.3.1-6vyuvjvx4kjh6gvn4dixje6mwfwqamv5/lib", ":")
prepend_path("LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/font-util-1.3.1-6vyuvjvx4kjh6gvn4dixje6mwfwqamv5/lib", ":")
prepend_path("PKG_CONFIG_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/font-util-1.3.1-6vyuvjvx4kjh6gvn4dixje6mwfwqamv5/lib/pkgconfig", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/font-util-1.3.1-6vyuvjvx4kjh6gvn4dixje6mwfwqamv5/", ":")

