-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2018-09-17 16:02:59.870364
--
-- fontconfig@2.12.3%gcc@5.4.0 arch=linux-linuxmint18-x86_64 /5yu2b3o
--

whatis([[Name : fontconfig]])
whatis([[Version : 2.12.3]])
whatis([[Short description : Fontconfig is a library for configuring/customizing font access]])
whatis([[Configure options : --enable-libxml2 --disable-docs --with-default-fonts=/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/font-util-1.3.1-6vyuvjvx4kjh6gvn4dixje6mwfwqamv5/share/fonts]])

help([[Fontconfig is a library for configuring/customizing font access]])



prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/fontconfig-2.12.3-5yu2b3o22e6oso7dzyehwdlaxjwih3wt/bin", ":")
prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/fontconfig-2.12.3-5yu2b3o22e6oso7dzyehwdlaxjwih3wt/lib", ":")
prepend_path("LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/fontconfig-2.12.3-5yu2b3o22e6oso7dzyehwdlaxjwih3wt/lib", ":")
prepend_path("CPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/fontconfig-2.12.3-5yu2b3o22e6oso7dzyehwdlaxjwih3wt/include", ":")
prepend_path("PKG_CONFIG_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/fontconfig-2.12.3-5yu2b3o22e6oso7dzyehwdlaxjwih3wt/lib/pkgconfig", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/fontconfig-2.12.3-5yu2b3o22e6oso7dzyehwdlaxjwih3wt/", ":")
prepend_path("CPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/libxml2-2.9.8-55hho2ks47dasihj2dsz3xg67rbssfsr/include/libxml2", ":")
prepend_path("CPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/freetype-2.7.1-4z3kpzdhhm623mmmrcnphm22rn4mi6o5/include/freetype2", ":")

