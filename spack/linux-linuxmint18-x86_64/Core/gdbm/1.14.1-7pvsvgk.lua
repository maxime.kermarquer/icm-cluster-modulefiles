-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2018-08-28 11:44:24.117117
--
-- gdbm@1.14.1%gcc@5.4.0 arch=linux-linuxmint18-x86_64 /7pvsvgk
--

whatis([[Name : gdbm]])
whatis([[Version : 1.14.1]])
whatis([[Short description : GNU dbm (or GDBM, for short) is a library of database functions that use extensible hashing and work similar to the standard UNIX dbm. These routines are provided to a programmer needing to create and manipulate a hashed database.]])
whatis([[Configure options : --enable-libgdbm-compat CPPFLAGS=-D_GNU_SOURCE]])

help([[GNU dbm (or GDBM, for short) is a library of database functions that use
extensible hashing and work similar to the standard UNIX dbm. These
routines are provided to a programmer needing to create and manipulate a
hashed database.]])



prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/gdbm-1.14.1-7pvsvgk4yv6y3ezc52pajdvx2vffy3qi/bin", ":")
prepend_path("MANPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/gdbm-1.14.1-7pvsvgk4yv6y3ezc52pajdvx2vffy3qi/share/man", ":")
prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/gdbm-1.14.1-7pvsvgk4yv6y3ezc52pajdvx2vffy3qi/lib", ":")
prepend_path("LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/gdbm-1.14.1-7pvsvgk4yv6y3ezc52pajdvx2vffy3qi/lib", ":")
prepend_path("CPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/gdbm-1.14.1-7pvsvgk4yv6y3ezc52pajdvx2vffy3qi/include", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/gdbm-1.14.1-7pvsvgk4yv6y3ezc52pajdvx2vffy3qi/", ":")

