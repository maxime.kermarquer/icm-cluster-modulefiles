-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2018-08-28 11:53:51.864140
--
-- git@2.18.0%gcc@5.4.0~tcltk arch=linux-linuxmint18-x86_64 /dqpkbrr
--

whatis([[Name : git]])
whatis([[Version : 2.18.0]])
whatis([[Short description : Git is a free and open source distributed version control system designed to handle everything from small to very large projects with speed and efficiency. ]])
whatis([[Configure options : --with-curl=/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/curl-7.60.0-u2eemmm5woui6xim3qc43xyges4mhytf --with-expat=/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/expat-2.2.5-23kghsg3bsyurnducu5fi4kzpu44eiim --with-iconv=/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/libiconv-1.15-5bezopblejpv6shv2io742rmfwwbvr4j --with-libpcre=/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/pcre-8.42-agwabtbxl5oprfay4mymw5ienpj64coi --with-openssl=/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/openssl-1.0.2o-5q4t4qkqw2nkza7pwqx4jbn42mpnxthe --with-perl=/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/perl-5.26.2-guvov3rwv2otb37lyfzoxb5g5w4t7q5g/bin/perl --with-zlib=/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/zlib-1.2.11-k5hg4kkxiutkfl6n53ogz5wnlbdrsdtf --without-tcltk]])

help([[Git is a free and open source distributed version control system
designed to handle everything from small to very large projects with
speed and efficiency.]])



prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/git-2.18.0-dqpkbrrsmcy7rrecyjuaukdzojw4dzya/bin", ":")
prepend_path("MANPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/git-2.18.0-dqpkbrrsmcy7rrecyjuaukdzojw4dzya/share/man", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/git-2.18.0-dqpkbrrsmcy7rrecyjuaukdzojw4dzya/", ":")
prepend_path("CPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/libxml2-2.9.8-55hho2ks47dasihj2dsz3xg67rbssfsr/include/libxml2", ":")

