-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2018-08-31 11:03:29.566033
--
-- glproto@1.4.17%gcc@5.4.0 arch=linux-linuxmint18-x86_64 /tr6qnem
--

whatis([[Name : glproto]])
whatis([[Version : 1.4.17]])
whatis([[Short description : OpenGL Extension to the X Window System.]])

help([[OpenGL Extension to the X Window System. This extension defines a
protocol for the client to send 3D rendering commands to the X server.]])



prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/glproto-1.4.17-tr6qnemiciyfqowab3wcfmrf6gseirig/lib", ":")
prepend_path("LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/glproto-1.4.17-tr6qnemiciyfqowab3wcfmrf6gseirig/lib", ":")
prepend_path("CPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/glproto-1.4.17-tr6qnemiciyfqowab3wcfmrf6gseirig/include", ":")
prepend_path("PKG_CONFIG_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/glproto-1.4.17-tr6qnemiciyfqowab3wcfmrf6gseirig/lib/pkgconfig", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/glproto-1.4.17-tr6qnemiciyfqowab3wcfmrf6gseirig/", ":")

