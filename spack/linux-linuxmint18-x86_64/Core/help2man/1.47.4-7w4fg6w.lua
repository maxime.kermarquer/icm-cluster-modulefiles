-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2018-08-31 10:58:37.707214
--
-- help2man@1.47.4%gcc@5.4.0 arch=linux-linuxmint18-x86_64 /7w4fg6w
--

whatis([[Name : help2man]])
whatis([[Version : 1.47.4]])
whatis([[Short description : help2man produces simple manual pages from the '--help' and '--version' output of other commands.]])

help([[help2man produces simple manual pages from the '--help' and '--version'
output of other commands.]])



prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/help2man-1.47.4-7w4fg6wxziwfruqv3lo7lis7dspkmdz6/bin", ":")
prepend_path("MANPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/help2man-1.47.4-7w4fg6wxziwfruqv3lo7lis7dspkmdz6/share/man", ":")
prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/help2man-1.47.4-7w4fg6wxziwfruqv3lo7lis7dspkmdz6/lib", ":")
prepend_path("LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/help2man-1.47.4-7w4fg6wxziwfruqv3lo7lis7dspkmdz6/lib", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/help2man-1.47.4-7w4fg6wxziwfruqv3lo7lis7dspkmdz6/", ":")

