-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2018-11-05 14:38:50.941108
--
-- hwloc@1.11.9%gcc@5.4.0~cairo~cuda+libxml2+pci+shared arch=linux-linuxmint18-x86_64 /tfrbzvb
--

whatis([[Name : hwloc]])
whatis([[Version : 1.11.9]])
whatis([[Short description : The Hardware Locality (hwloc) software project.]])
whatis([[Configure options : --disable-opencl --disable-cairo --disable-cuda --enable-libxml2 --enable-pci --enable-shared]])

help([[The Hardware Locality (hwloc) software project. The Portable Hardware
Locality (hwloc) software package provides a portable abstraction
(across OS, versions, architectures, ...) of the hierarchical topology
of modern architectures, including NUMA memory nodes, sockets, shared
caches, cores and simultaneous multithreading. It also gathers various
system attributes such as cache and memory information as well as the
locality of I/O devices such as network interfaces, InfiniBand HCAs or
GPUs. It primarily aims at helping applications with gathering
information about modern computing hardware so as to exploit it
accordingly and efficiently.]])



prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/hwloc-1.11.9-tfrbzvbbbj6253dm5urweertkizawxs7/bin", ":")
prepend_path("MANPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/hwloc-1.11.9-tfrbzvbbbj6253dm5urweertkizawxs7/share/man", ":")
prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/hwloc-1.11.9-tfrbzvbbbj6253dm5urweertkizawxs7/lib", ":")
prepend_path("LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/hwloc-1.11.9-tfrbzvbbbj6253dm5urweertkizawxs7/lib", ":")
prepend_path("CPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/hwloc-1.11.9-tfrbzvbbbj6253dm5urweertkizawxs7/include", ":")
prepend_path("PKG_CONFIG_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/hwloc-1.11.9-tfrbzvbbbj6253dm5urweertkizawxs7/lib/pkgconfig", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/hwloc-1.11.9-tfrbzvbbbj6253dm5urweertkizawxs7/", ":")
prepend_path("CPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/libxml2-2.9.8-55hho2ks47dasihj2dsz3xg67rbssfsr/include/libxml2", ":")

