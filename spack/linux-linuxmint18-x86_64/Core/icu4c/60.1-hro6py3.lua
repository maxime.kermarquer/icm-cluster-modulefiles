-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2018-08-31 11:04:29.188403
--
-- icu4c@60.1%gcc@5.4.0 arch=linux-linuxmint18-x86_64 /hro6py3
--

whatis([[Name : icu4c]])
whatis([[Version : 60.1]])
whatis([[Short description : ICU is a mature, widely used set of C/C++ and Java libraries providing Unicode and Globalization support for software applications. ICU4C is the C/C++ interface.]])
whatis([[Configure options : --enable-rpath]])

help([[ICU is a mature, widely used set of C/C++ and Java libraries providing
Unicode and Globalization support for software applications. ICU4C is
the C/C++ interface.]])



prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/icu4c-60.1-hro6py3un5qjk7wi4jtvni4vbxmc6ttv/bin", ":")
prepend_path("MANPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/icu4c-60.1-hro6py3un5qjk7wi4jtvni4vbxmc6ttv/share/man", ":")
prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/icu4c-60.1-hro6py3un5qjk7wi4jtvni4vbxmc6ttv/lib", ":")
prepend_path("LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/icu4c-60.1-hro6py3un5qjk7wi4jtvni4vbxmc6ttv/lib", ":")
prepend_path("CPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/icu4c-60.1-hro6py3un5qjk7wi4jtvni4vbxmc6ttv/include", ":")
prepend_path("PKG_CONFIG_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/icu4c-60.1-hro6py3un5qjk7wi4jtvni4vbxmc6ttv/lib/pkgconfig", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/icu4c-60.1-hro6py3un5qjk7wi4jtvni4vbxmc6ttv/", ":")

