-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2018-08-31 11:04:37.371515
--
-- inputproto@2.3.2%gcc@5.4.0 arch=linux-linuxmint18-x86_64 /zewza67
--

whatis([[Name : inputproto]])
whatis([[Version : 2.3.2]])
whatis([[Short description : X Input Extension.]])

help([[X Input Extension. This extension defines a protocol to provide
additional input devices management such as graphic tablets.]])



prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/inputproto-2.3.2-zewza67nifi375sj6mmnuoxichtqqglg/lib", ":")
prepend_path("LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/inputproto-2.3.2-zewza67nifi375sj6mmnuoxichtqqglg/lib", ":")
prepend_path("CPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/inputproto-2.3.2-zewza67nifi375sj6mmnuoxichtqqglg/include", ":")
prepend_path("PKG_CONFIG_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/inputproto-2.3.2-zewza67nifi375sj6mmnuoxichtqqglg/lib/pkgconfig", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/inputproto-2.3.2-zewza67nifi375sj6mmnuoxichtqqglg/", ":")

