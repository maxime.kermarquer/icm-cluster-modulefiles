-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2018-09-20 14:59:17.095111
--
-- jemalloc@4.5.0%gcc@5.4.0~prof~stats arch=linux-linuxmint18-x86_64 /7irqd3r
--

whatis([[Name : jemalloc]])
whatis([[Version : 4.5.0]])
whatis([[Short description : jemalloc is a general purpose malloc(3) implementation that emphasizes fragmentation avoidance and scalable concurrency support.]])

help([[jemalloc is a general purpose malloc(3) implementation that emphasizes
fragmentation avoidance and scalable concurrency support.]])



prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/jemalloc-4.5.0-7irqd3rmmomgp2zrlldgtn2dtbp4dhjm/bin", ":")
prepend_path("MANPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/jemalloc-4.5.0-7irqd3rmmomgp2zrlldgtn2dtbp4dhjm/share/man", ":")
prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/jemalloc-4.5.0-7irqd3rmmomgp2zrlldgtn2dtbp4dhjm/lib", ":")
prepend_path("LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/jemalloc-4.5.0-7irqd3rmmomgp2zrlldgtn2dtbp4dhjm/lib", ":")
prepend_path("CPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/jemalloc-4.5.0-7irqd3rmmomgp2zrlldgtn2dtbp4dhjm/include", ":")
prepend_path("PKG_CONFIG_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/jemalloc-4.5.0-7irqd3rmmomgp2zrlldgtn2dtbp4dhjm/lib/pkgconfig", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/jemalloc-4.5.0-7irqd3rmmomgp2zrlldgtn2dtbp4dhjm/", ":")

