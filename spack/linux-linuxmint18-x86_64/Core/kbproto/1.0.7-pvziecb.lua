-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2018-08-31 11:04:41.596637
--
-- kbproto@1.0.7%gcc@5.4.0 arch=linux-linuxmint18-x86_64 /pvziecb
--

whatis([[Name : kbproto]])
whatis([[Version : 1.0.7]])
whatis([[Short description : X Keyboard Extension.]])

help([[X Keyboard Extension. This extension defines a protcol to provide a
number of new capabilities and controls for text keyboards.]])



prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/kbproto-1.0.7-pvziecbunc2wwnac6kyulm734avqam5a/lib", ":")
prepend_path("LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/kbproto-1.0.7-pvziecbunc2wwnac6kyulm734avqam5a/lib", ":")
prepend_path("CPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/kbproto-1.0.7-pvziecbunc2wwnac6kyulm734avqam5a/include", ":")
prepend_path("PKG_CONFIG_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/kbproto-1.0.7-pvziecbunc2wwnac6kyulm734avqam5a/lib/pkgconfig", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/kbproto-1.0.7-pvziecbunc2wwnac6kyulm734avqam5a/", ":")

