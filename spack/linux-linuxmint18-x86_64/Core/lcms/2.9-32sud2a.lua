-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2018-09-17 16:08:18.973657
--
-- lcms@2.9%gcc@5.4.0 arch=linux-linuxmint18-x86_64 /32sud2a
--

whatis([[Name : lcms]])
whatis([[Version : 2.9]])
whatis([[Short description : Little cms is a color management library. Implements fast transforms between ICC profiles. It is focused on speed, and is portable across several platforms (MIT license).]])

help([[Little cms is a color management library. Implements fast transforms
between ICC profiles. It is focused on speed, and is portable across
several platforms (MIT license).]])



prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/lcms-2.9-32sud2auuw54aiwh6oiwkm4v6kiysd7q/bin", ":")
prepend_path("MANPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/lcms-2.9-32sud2auuw54aiwh6oiwkm4v6kiysd7q/share/man", ":")
prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/lcms-2.9-32sud2auuw54aiwh6oiwkm4v6kiysd7q/lib", ":")
prepend_path("LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/lcms-2.9-32sud2auuw54aiwh6oiwkm4v6kiysd7q/lib", ":")
prepend_path("CPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/lcms-2.9-32sud2auuw54aiwh6oiwkm4v6kiysd7q/include", ":")
prepend_path("PKG_CONFIG_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/lcms-2.9-32sud2auuw54aiwh6oiwkm4v6kiysd7q/lib/pkgconfig", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/lcms-2.9-32sud2auuw54aiwh6oiwkm4v6kiysd7q/", ":")

