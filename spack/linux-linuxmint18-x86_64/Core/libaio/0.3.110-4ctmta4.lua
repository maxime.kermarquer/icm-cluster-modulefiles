-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2018-09-20 14:59:17.809975
--
-- libaio@0.3.110%gcc@5.4.0 arch=linux-linuxmint18-x86_64 /4ctmta4
--

whatis([[Name : libaio]])
whatis([[Version : 0.3.110]])
whatis([[Short description : This is the linux native Asynchronous I/O interface library.]])

help([[This is the linux native Asynchronous I/O interface library.]])



prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/libaio-0.3.110-4ctmta4xf5fow62rmbzft5fbx2754plj/lib", ":")
prepend_path("LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/libaio-0.3.110-4ctmta4xf5fow62rmbzft5fbx2754plj/lib", ":")
prepend_path("CPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/libaio-0.3.110-4ctmta4xf5fow62rmbzft5fbx2754plj/include", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/libaio-0.3.110-4ctmta4xf5fow62rmbzft5fbx2754plj/", ":")

