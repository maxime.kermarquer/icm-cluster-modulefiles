-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2018-08-28 11:48:48.831193
--
-- libbsd@0.8.6%gcc@5.4.0 arch=linux-linuxmint18-x86_64 /bfntqjl
--

whatis([[Name : libbsd]])
whatis([[Version : 0.8.6]])
whatis([[Short description : This library provides useful functions commonly found on BSD systems, and lacking on others like GNU systems, thus making it easier to port projects with strong BSD origins, without needing to embed the same code over and over again on each project. ]])

help([[This library provides useful functions commonly found on BSD systems,
and lacking on others like GNU systems, thus making it easier to port
projects with strong BSD origins, without needing to embed the same code
over and over again on each project.]])



prepend_path("MANPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/libbsd-0.8.6-bfntqjliqws3gandp5i6xxfha2i3fdxs/share/man", ":")
prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/libbsd-0.8.6-bfntqjliqws3gandp5i6xxfha2i3fdxs/lib", ":")
prepend_path("LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/libbsd-0.8.6-bfntqjliqws3gandp5i6xxfha2i3fdxs/lib", ":")
prepend_path("CPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/libbsd-0.8.6-bfntqjliqws3gandp5i6xxfha2i3fdxs/include", ":")
prepend_path("PKG_CONFIG_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/libbsd-0.8.6-bfntqjliqws3gandp5i6xxfha2i3fdxs/lib/pkgconfig", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/libbsd-0.8.6-bfntqjliqws3gandp5i6xxfha2i3fdxs/", ":")

