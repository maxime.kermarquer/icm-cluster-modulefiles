-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2018-09-20 14:59:26.357349
--
-- libedit@3.1-20170329%gcc@5.4.0 arch=linux-linuxmint18-x86_64 /47a7sfx
--

whatis([[Name : libedit]])
whatis([[Version : 3.1-20170329]])
whatis([[Short description : An autotools compatible port of the NetBSD editline library]])

help([[An autotools compatible port of the NetBSD editline library]])



prepend_path("MANPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/libedit-3.1-20170329-47a7sfxexquz2ryls4vnqfb7n3apqp7k/share/man", ":")
prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/libedit-3.1-20170329-47a7sfxexquz2ryls4vnqfb7n3apqp7k/lib", ":")
prepend_path("LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/libedit-3.1-20170329-47a7sfxexquz2ryls4vnqfb7n3apqp7k/lib", ":")
prepend_path("CPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/libedit-3.1-20170329-47a7sfxexquz2ryls4vnqfb7n3apqp7k/include", ":")
prepend_path("PKG_CONFIG_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/libedit-3.1-20170329-47a7sfxexquz2ryls4vnqfb7n3apqp7k/lib/pkgconfig", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/libedit-3.1-20170329-47a7sfxexquz2ryls4vnqfb7n3apqp7k/", ":")

