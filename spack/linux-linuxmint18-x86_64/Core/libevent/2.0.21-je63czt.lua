-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2018-09-20 14:59:41.908248
--
-- libevent@2.0.21%gcc@5.4.0+openssl arch=linux-linuxmint18-x86_64 /je63czt
--

whatis([[Name : libevent]])
whatis([[Version : 2.0.21]])
whatis([[Short description : The libevent API provides a mechanism to execute a callback function when a specific event occurs on a file descriptor or after a timeout has been reached. Furthermore, libevent also support callbacks due to signals or regular timeouts.]])
whatis([[Configure options : --enable-openssl]])

help([[The libevent API provides a mechanism to execute a callback function
when a specific event occurs on a file descriptor or after a timeout has
been reached. Furthermore, libevent also support callbacks due to
signals or regular timeouts.]])



prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/libevent-2.0.21-je63cztfszxxhrpa7ufcs4qrztfug6ax/bin", ":")
prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/libevent-2.0.21-je63cztfszxxhrpa7ufcs4qrztfug6ax/lib", ":")
prepend_path("LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/libevent-2.0.21-je63cztfszxxhrpa7ufcs4qrztfug6ax/lib", ":")
prepend_path("CPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/libevent-2.0.21-je63cztfszxxhrpa7ufcs4qrztfug6ax/include", ":")
prepend_path("PKG_CONFIG_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/libevent-2.0.21-je63cztfszxxhrpa7ufcs4qrztfug6ax/lib/pkgconfig", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/libevent-2.0.21-je63cztfszxxhrpa7ufcs4qrztfug6ax/", ":")

