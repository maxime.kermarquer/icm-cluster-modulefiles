-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2018-09-17 16:03:05.756814
--
-- libffi@3.2.1%gcc@5.4.0 arch=linux-linuxmint18-x86_64 /eomx3hn
--

whatis([[Name : libffi]])
whatis([[Version : 3.2.1]])
whatis([[Short description : The libffi library provides a portable, high level programming interface to various calling conventions. This allows a programmer to call any function specified by a call interface description at run time.]])

help([[The libffi library provides a portable, high level programming interface
to various calling conventions. This allows a programmer to call any
function specified by a call interface description at run time.]])



prepend_path("MANPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/libffi-3.2.1-eomx3hnz56shvz4a4xjokwrnoun5kwjf/share/man", ":")
prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/libffi-3.2.1-eomx3hnz56shvz4a4xjokwrnoun5kwjf/lib", ":")
prepend_path("LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/libffi-3.2.1-eomx3hnz56shvz4a4xjokwrnoun5kwjf/lib", ":")
prepend_path("PKG_CONFIG_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/libffi-3.2.1-eomx3hnz56shvz4a4xjokwrnoun5kwjf/lib/pkgconfig", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/libffi-3.2.1-eomx3hnz56shvz4a4xjokwrnoun5kwjf/", ":")

