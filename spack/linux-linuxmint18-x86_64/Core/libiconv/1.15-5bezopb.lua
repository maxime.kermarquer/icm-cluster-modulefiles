-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2018-08-28 11:53:06.522171
--
-- libiconv@1.15%gcc@5.4.0 arch=linux-linuxmint18-x86_64 /5bezopb
--

whatis([[Name : libiconv]])
whatis([[Version : 1.15]])
whatis([[Short description : GNU libiconv provides an implementation of the iconv() function and the iconv program for character set conversion.]])

help([[GNU libiconv provides an implementation of the iconv() function and the
iconv program for character set conversion.]])



prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/libiconv-1.15-5bezopblejpv6shv2io742rmfwwbvr4j/bin", ":")
prepend_path("MANPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/libiconv-1.15-5bezopblejpv6shv2io742rmfwwbvr4j/share/man", ":")
prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/libiconv-1.15-5bezopblejpv6shv2io742rmfwwbvr4j/lib", ":")
prepend_path("LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/libiconv-1.15-5bezopblejpv6shv2io742rmfwwbvr4j/lib", ":")
prepend_path("CPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/libiconv-1.15-5bezopblejpv6shv2io742rmfwwbvr4j/include", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/libiconv-1.15-5bezopblejpv6shv2io742rmfwwbvr4j/", ":")

