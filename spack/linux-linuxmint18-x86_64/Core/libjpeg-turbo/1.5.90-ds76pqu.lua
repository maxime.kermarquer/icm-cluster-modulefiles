-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2018-09-17 16:07:47.109826
--
-- libjpeg-turbo@1.5.90%gcc@5.4.0 arch=linux-linuxmint18-x86_64 /ds76pqu
--

whatis([[Name : libjpeg-turbo]])
whatis([[Version : 1.5.90]])
whatis([[Short description : libjpeg-turbo is a fork of the original IJG libjpeg which uses SIMD to accelerate baseline JPEG compression and decompression. libjpeg is a library that implements JPEG image encoding, decoding and transcoding.]])

help([[libjpeg-turbo is a fork of the original IJG libjpeg which uses SIMD to
accelerate baseline JPEG compression and decompression. libjpeg is a
library that implements JPEG image encoding, decoding and transcoding.]])



prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/libjpeg-turbo-1.5.90-ds76pquyx4auc6pfjxmusxmep622fppi/bin", ":")
prepend_path("MANPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/libjpeg-turbo-1.5.90-ds76pquyx4auc6pfjxmusxmep622fppi/share/man", ":")
prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/libjpeg-turbo-1.5.90-ds76pquyx4auc6pfjxmusxmep622fppi/lib", ":")
prepend_path("LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/libjpeg-turbo-1.5.90-ds76pquyx4auc6pfjxmusxmep622fppi/lib", ":")
prepend_path("CPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/libjpeg-turbo-1.5.90-ds76pquyx4auc6pfjxmusxmep622fppi/include", ":")
prepend_path("PKG_CONFIG_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/libjpeg-turbo-1.5.90-ds76pquyx4auc6pfjxmusxmep622fppi/lib/pkgconfig", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/libjpeg-turbo-1.5.90-ds76pquyx4auc6pfjxmusxmep622fppi/", ":")

