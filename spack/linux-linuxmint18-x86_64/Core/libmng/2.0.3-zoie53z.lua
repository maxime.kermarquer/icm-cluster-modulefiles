-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2018-09-17 16:08:31.675694
--
-- libmng@2.0.3%gcc@5.4.0 arch=linux-linuxmint18-x86_64 /zoie53z
--

whatis([[Name : libmng]])
whatis([[Version : 2.0.3]])
whatis([[Short description : libmng -THE reference library for reading, displaying, writing and examining Multiple-Image Network Graphics. MNG is the animation extension to the popular PNG image-format.]])

help([[libmng -THE reference library for reading, displaying, writing and
examining Multiple-Image Network Graphics. MNG is the animation
extension to the popular PNG image-format.]])



prepend_path("MANPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/libmng-2.0.3-zoie53zy4gm3ffq7qfdtuv3czhujylrn/share/man", ":")
prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/libmng-2.0.3-zoie53zy4gm3ffq7qfdtuv3czhujylrn/lib", ":")
prepend_path("LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/libmng-2.0.3-zoie53zy4gm3ffq7qfdtuv3czhujylrn/lib", ":")
prepend_path("CPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/libmng-2.0.3-zoie53zy4gm3ffq7qfdtuv3czhujylrn/include", ":")
prepend_path("PKG_CONFIG_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/libmng-2.0.3-zoie53zy4gm3ffq7qfdtuv3czhujylrn/lib/pkgconfig", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/libmng-2.0.3-zoie53zy4gm3ffq7qfdtuv3czhujylrn/", ":")

