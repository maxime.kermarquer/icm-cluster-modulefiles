-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2018-11-05 14:37:58.341394
--
-- libpciaccess@0.13.5%gcc@5.4.0 arch=linux-linuxmint18-x86_64 /voyxt6w
--

whatis([[Name : libpciaccess]])
whatis([[Version : 0.13.5]])
whatis([[Short description : Generic PCI access library.]])

help([[Generic PCI access library.]])



prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/libpciaccess-0.13.5-voyxt6ws4mtn33hvkgfh32aothdfbmch/lib", ":")
prepend_path("LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/libpciaccess-0.13.5-voyxt6ws4mtn33hvkgfh32aothdfbmch/lib", ":")
prepend_path("CPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/libpciaccess-0.13.5-voyxt6ws4mtn33hvkgfh32aothdfbmch/include", ":")
prepend_path("PKG_CONFIG_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/libpciaccess-0.13.5-voyxt6ws4mtn33hvkgfh32aothdfbmch/lib/pkgconfig", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/libpciaccess-0.13.5-voyxt6ws4mtn33hvkgfh32aothdfbmch/", ":")

