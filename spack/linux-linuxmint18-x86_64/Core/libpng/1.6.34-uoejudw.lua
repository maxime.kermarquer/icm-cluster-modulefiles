-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2018-09-17 16:02:27.595744
--
-- libpng@1.6.34%gcc@5.4.0 arch=linux-linuxmint18-x86_64 /uoejudw
--

whatis([[Name : libpng]])
whatis([[Version : 1.6.34]])
whatis([[Short description : libpng is the official PNG reference library.]])
whatis([[Configure options : CPPFLAGS=-I/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/zlib-1.2.11-k5hg4kkxiutkfl6n53ogz5wnlbdrsdtf/include LDFLAGS=-L/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/zlib-1.2.11-k5hg4kkxiutkfl6n53ogz5wnlbdrsdtf/lib]])

help([[libpng is the official PNG reference library.]])



prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/libpng-1.6.34-uoejudwrnleszaclqsydjbbpoexjibqs/bin", ":")
prepend_path("MANPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/libpng-1.6.34-uoejudwrnleszaclqsydjbbpoexjibqs/share/man", ":")
prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/libpng-1.6.34-uoejudwrnleszaclqsydjbbpoexjibqs/lib", ":")
prepend_path("LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/libpng-1.6.34-uoejudwrnleszaclqsydjbbpoexjibqs/lib", ":")
prepend_path("CPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/libpng-1.6.34-uoejudwrnleszaclqsydjbbpoexjibqs/include", ":")
prepend_path("PKG_CONFIG_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/libpng-1.6.34-uoejudwrnleszaclqsydjbbpoexjibqs/lib/pkgconfig", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/libpng-1.6.34-uoejudwrnleszaclqsydjbbpoexjibqs/", ":")

