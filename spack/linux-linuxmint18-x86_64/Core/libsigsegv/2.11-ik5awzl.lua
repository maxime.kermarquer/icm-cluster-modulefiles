-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2018-08-28 11:42:28.680736
--
-- libsigsegv@2.11%gcc@5.4.0 arch=linux-linuxmint18-x86_64 /ik5awzl
--

whatis([[Name : libsigsegv]])
whatis([[Version : 2.11]])
whatis([[Short description : GNU libsigsegv is a library for handling page faults in user mode.]])
whatis([[Configure options : --enable-shared]])

help([[GNU libsigsegv is a library for handling page faults in user mode.]])



prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/libsigsegv-2.11-ik5awzlx7k35lmxvz6q6rmuiwrmpowhs/lib", ":")
prepend_path("LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/libsigsegv-2.11-ik5awzlx7k35lmxvz6q6rmuiwrmpowhs/lib", ":")
prepend_path("CPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/libsigsegv-2.11-ik5awzlx7k35lmxvz6q6rmuiwrmpowhs/include", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/libsigsegv-2.11-ik5awzlx7k35lmxvz6q6rmuiwrmpowhs/", ":")

