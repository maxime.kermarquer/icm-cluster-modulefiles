-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2018-09-17 16:08:07.691127
--
-- libtiff@4.0.9%gcc@5.4.0 arch=linux-linuxmint18-x86_64 /t3otdew
--

whatis([[Name : libtiff]])
whatis([[Version : 4.0.9]])
whatis([[Short description : LibTIFF - Tag Image File Format (TIFF) Library and Utilities.]])

help([[LibTIFF - Tag Image File Format (TIFF) Library and Utilities.]])



prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/libtiff-4.0.9-t3otdewfnrgnqixxfjcfhtp2y7wjzv6q/bin", ":")
prepend_path("MANPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/libtiff-4.0.9-t3otdewfnrgnqixxfjcfhtp2y7wjzv6q/share/man", ":")
prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/libtiff-4.0.9-t3otdewfnrgnqixxfjcfhtp2y7wjzv6q/lib", ":")
prepend_path("LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/libtiff-4.0.9-t3otdewfnrgnqixxfjcfhtp2y7wjzv6q/lib", ":")
prepend_path("CPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/libtiff-4.0.9-t3otdewfnrgnqixxfjcfhtp2y7wjzv6q/include", ":")
prepend_path("PKG_CONFIG_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/libtiff-4.0.9-t3otdewfnrgnqixxfjcfhtp2y7wjzv6q/lib/pkgconfig", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/libtiff-4.0.9-t3otdewfnrgnqixxfjcfhtp2y7wjzv6q/", ":")

