-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2018-08-28 11:53:13.987232
--
-- libtool@2.4.6%gcc@5.4.0 arch=linux-linuxmint18-x86_64 /ievaqgh
--

whatis([[Name : libtool]])
whatis([[Version : 2.4.6]])
whatis([[Short description : libtool -- library building part of autotools.]])

help([[libtool -- library building part of autotools.]])



prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/libtool-2.4.6-ievaqgh5p2f55uizw6ew4iind6tuwaxa/bin", ":")
prepend_path("MANPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/libtool-2.4.6-ievaqgh5p2f55uizw6ew4iind6tuwaxa/share/man", ":")
prepend_path("ACLOCAL_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/libtool-2.4.6-ievaqgh5p2f55uizw6ew4iind6tuwaxa/share/aclocal", ":")
prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/libtool-2.4.6-ievaqgh5p2f55uizw6ew4iind6tuwaxa/lib", ":")
prepend_path("LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/libtool-2.4.6-ievaqgh5p2f55uizw6ew4iind6tuwaxa/lib", ":")
prepend_path("CPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/libtool-2.4.6-ievaqgh5p2f55uizw6ew4iind6tuwaxa/include", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/libtool-2.4.6-ievaqgh5p2f55uizw6ew4iind6tuwaxa/", ":")

