-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2018-08-31 11:06:53.349060
--
-- libx11@1.6.5%gcc@5.4.0 arch=linux-linuxmint18-x86_64 /lehicl4
--

whatis([[Name : libx11]])
whatis([[Version : 1.6.5]])
whatis([[Short description : libX11 - Core X11 protocol client library.]])

help([[libX11 - Core X11 protocol client library.]])



prepend_path("MANPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/libx11-1.6.5-lehicl4pfy5ncxlcpyglbcfe4gsbdjos/share/man", ":")
prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/libx11-1.6.5-lehicl4pfy5ncxlcpyglbcfe4gsbdjos/lib", ":")
prepend_path("LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/libx11-1.6.5-lehicl4pfy5ncxlcpyglbcfe4gsbdjos/lib", ":")
prepend_path("CPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/libx11-1.6.5-lehicl4pfy5ncxlcpyglbcfe4gsbdjos/include", ":")
prepend_path("PKG_CONFIG_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/libx11-1.6.5-lehicl4pfy5ncxlcpyglbcfe4gsbdjos/lib/pkgconfig", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/libx11-1.6.5-lehicl4pfy5ncxlcpyglbcfe4gsbdjos/", ":")

