-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2018-08-31 11:04:53.518792
--
-- libxau@1.0.8%gcc@5.4.0 arch=linux-linuxmint18-x86_64 /4ec7nv4
--

whatis([[Name : libxau]])
whatis([[Version : 1.0.8]])
whatis([[Short description : The libXau package contains a library implementing the X11 Authorization Protocol. This is useful for restricting client access to the display.]])

help([[The libXau package contains a library implementing the X11 Authorization
Protocol. This is useful for restricting client access to the display.]])



prepend_path("MANPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/libxau-1.0.8-4ec7nv4udukg3oamdiabxdziuzgjejyv/share/man", ":")
prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/libxau-1.0.8-4ec7nv4udukg3oamdiabxdziuzgjejyv/lib", ":")
prepend_path("LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/libxau-1.0.8-4ec7nv4udukg3oamdiabxdziuzgjejyv/lib", ":")
prepend_path("CPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/libxau-1.0.8-4ec7nv4udukg3oamdiabxdziuzgjejyv/include", ":")
prepend_path("PKG_CONFIG_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/libxau-1.0.8-4ec7nv4udukg3oamdiabxdziuzgjejyv/lib/pkgconfig", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/libxau-1.0.8-4ec7nv4udukg3oamdiabxdziuzgjejyv/", ":")

