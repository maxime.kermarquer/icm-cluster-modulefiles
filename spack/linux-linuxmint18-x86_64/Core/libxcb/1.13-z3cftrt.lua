-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2018-08-31 11:05:34.867937
--
-- libxcb@1.13%gcc@5.4.0 arch=linux-linuxmint18-x86_64 /z3cftrt
--

whatis([[Name : libxcb]])
whatis([[Version : 1.13]])
whatis([[Short description : The X protocol C-language Binding (XCB) is a replacement for Xlib featuring a small footprint, latency hiding, direct access to the protocol, improved threading support, and extensibility.]])

help([[The X protocol C-language Binding (XCB) is a replacement for Xlib
featuring a small footprint, latency hiding, direct access to the
protocol, improved threading support, and extensibility.]])



prepend_path("MANPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/libxcb-1.13-z3cftrttb7hrb3udb7swrc5s33ylbwap/share/man", ":")
prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/libxcb-1.13-z3cftrttb7hrb3udb7swrc5s33ylbwap/lib", ":")
prepend_path("LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/libxcb-1.13-z3cftrttb7hrb3udb7swrc5s33ylbwap/lib", ":")
prepend_path("CPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/libxcb-1.13-z3cftrttb7hrb3udb7swrc5s33ylbwap/include", ":")
prepend_path("PKG_CONFIG_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/libxcb-1.13-z3cftrttb7hrb3udb7swrc5s33ylbwap/lib/pkgconfig", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/libxcb-1.13-z3cftrttb7hrb3udb7swrc5s33ylbwap/", ":")

