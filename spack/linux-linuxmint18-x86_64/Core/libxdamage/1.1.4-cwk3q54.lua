-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2018-08-31 11:07:14.741015
--
-- libxdamage@1.1.4%gcc@5.4.0 arch=linux-linuxmint18-x86_64 /cwk3q54
--

whatis([[Name : libxdamage]])
whatis([[Version : 1.1.4]])
whatis([[Short description : This package contains the library for the X Damage extension.]])

help([[This package contains the library for the X Damage extension.]])



prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/libxdamage-1.1.4-cwk3q547b5chh7g43yr3d5aaoofi4gt6/lib", ":")
prepend_path("LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/libxdamage-1.1.4-cwk3q547b5chh7g43yr3d5aaoofi4gt6/lib", ":")
prepend_path("CPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/libxdamage-1.1.4-cwk3q547b5chh7g43yr3d5aaoofi4gt6/include", ":")
prepend_path("PKG_CONFIG_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/libxdamage-1.1.4-cwk3q547b5chh7g43yr3d5aaoofi4gt6/lib/pkgconfig", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/libxdamage-1.1.4-cwk3q547b5chh7g43yr3d5aaoofi4gt6/", ":")

