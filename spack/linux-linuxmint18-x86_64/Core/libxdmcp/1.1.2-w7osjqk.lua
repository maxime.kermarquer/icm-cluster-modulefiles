-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2018-08-31 11:04:59.691600
--
-- libxdmcp@1.1.2%gcc@5.4.0 arch=linux-linuxmint18-x86_64 /w7osjqk
--

whatis([[Name : libxdmcp]])
whatis([[Version : 1.1.2]])
whatis([[Short description : libXdmcp - X Display Manager Control Protocol library.]])

help([[libXdmcp - X Display Manager Control Protocol library.]])



prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/libxdmcp-1.1.2-w7osjqkvntl2wi2t3kajljnz6aabach6/lib", ":")
prepend_path("LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/libxdmcp-1.1.2-w7osjqkvntl2wi2t3kajljnz6aabach6/lib", ":")
prepend_path("CPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/libxdmcp-1.1.2-w7osjqkvntl2wi2t3kajljnz6aabach6/include", ":")
prepend_path("PKG_CONFIG_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/libxdmcp-1.1.2-w7osjqkvntl2wi2t3kajljnz6aabach6/lib/pkgconfig", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/libxdmcp-1.1.2-w7osjqkvntl2wi2t3kajljnz6aabach6/", ":")

