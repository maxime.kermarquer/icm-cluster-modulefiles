-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2018-08-31 11:07:23.261675
--
-- libxext@1.3.3%gcc@5.4.0 arch=linux-linuxmint18-x86_64 /xhilyk4
--

whatis([[Name : libxext]])
whatis([[Version : 1.3.3]])
whatis([[Short description : libXext - library for common extensions to the X11 protocol.]])

help([[libXext - library for common extensions to the X11 protocol.]])



prepend_path("MANPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/libxext-1.3.3-xhilyk4qxaudxlhh7c7fntqjyjahaan6/share/man", ":")
prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/libxext-1.3.3-xhilyk4qxaudxlhh7c7fntqjyjahaan6/lib", ":")
prepend_path("LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/libxext-1.3.3-xhilyk4qxaudxlhh7c7fntqjyjahaan6/lib", ":")
prepend_path("CPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/libxext-1.3.3-xhilyk4qxaudxlhh7c7fntqjyjahaan6/include", ":")
prepend_path("PKG_CONFIG_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/libxext-1.3.3-xhilyk4qxaudxlhh7c7fntqjyjahaan6/lib/pkgconfig", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/libxext-1.3.3-xhilyk4qxaudxlhh7c7fntqjyjahaan6/", ":")

