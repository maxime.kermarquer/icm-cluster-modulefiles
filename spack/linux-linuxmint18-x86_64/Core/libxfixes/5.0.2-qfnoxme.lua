-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2018-08-31 11:07:07.908528
--
-- libxfixes@5.0.2%gcc@5.4.0 arch=linux-linuxmint18-x86_64 /qfnoxme
--

whatis([[Name : libxfixes]])
whatis([[Version : 5.0.2]])
whatis([[Short description : This package contains header files and documentation for the XFIXES extension. Library and server implementations are separate.]])

help([[This package contains header files and documentation for the XFIXES
extension. Library and server implementations are separate.]])



prepend_path("MANPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/libxfixes-5.0.2-qfnoxmezdg5ijli4an275ho5xizcdl37/share/man", ":")
prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/libxfixes-5.0.2-qfnoxmezdg5ijli4an275ho5xizcdl37/lib", ":")
prepend_path("LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/libxfixes-5.0.2-qfnoxmezdg5ijli4an275ho5xizcdl37/lib", ":")
prepend_path("CPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/libxfixes-5.0.2-qfnoxmezdg5ijli4an275ho5xizcdl37/include", ":")
prepend_path("PKG_CONFIG_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/libxfixes-5.0.2-qfnoxmezdg5ijli4an275ho5xizcdl37/lib/pkgconfig", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/libxfixes-5.0.2-qfnoxmezdg5ijli4an275ho5xizcdl37/", ":")

