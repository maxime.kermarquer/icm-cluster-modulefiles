-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2018-08-31 11:07:28.797983
--
-- libxshmfence@1.3%gcc@5.4.0 arch=linux-linuxmint18-x86_64 /tdgabbn
--

whatis([[Name : libxshmfence]])
whatis([[Version : 1.3]])
whatis([[Short description : libxshmfence - Shared memory 'SyncFence' synchronization primitive.]])

help([[libxshmfence - Shared memory 'SyncFence' synchronization primitive. This
library offers a CPU-based synchronization primitive compatible with the
X SyncFence objects that can be shared between processes using file
descriptor passing.]])



prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/libxshmfence-1.3-tdgabbnbi72ys6bofjq6cxjztgxnd2x7/lib", ":")
prepend_path("LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/libxshmfence-1.3-tdgabbnbi72ys6bofjq6cxjztgxnd2x7/lib", ":")
prepend_path("CPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/libxshmfence-1.3-tdgabbnbi72ys6bofjq6cxjztgxnd2x7/include", ":")
prepend_path("PKG_CONFIG_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/libxshmfence-1.3-tdgabbnbi72ys6bofjq6cxjztgxnd2x7/lib/pkgconfig", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/libxshmfence-1.3-tdgabbnbi72ys6bofjq6cxjztgxnd2x7/", ":")

