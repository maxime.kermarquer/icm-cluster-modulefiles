-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2018-08-31 11:07:39.122546
--
-- libxv@1.0.10%gcc@5.4.0 arch=linux-linuxmint18-x86_64 /befb3xj
--

whatis([[Name : libxv]])
whatis([[Version : 1.0.10]])
whatis([[Short description : libXv - library for the X Video (Xv) extension to the X Window System.]])

help([[libXv - library for the X Video (Xv) extension to the X Window System.]])



prepend_path("MANPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/libxv-1.0.10-befb3xjm4htt2ghk5c4mexramwphtfmm/share/man", ":")
prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/libxv-1.0.10-befb3xjm4htt2ghk5c4mexramwphtfmm/lib", ":")
prepend_path("LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/libxv-1.0.10-befb3xjm4htt2ghk5c4mexramwphtfmm/lib", ":")
prepend_path("CPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/libxv-1.0.10-befb3xjm4htt2ghk5c4mexramwphtfmm/include", ":")
prepend_path("PKG_CONFIG_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/libxv-1.0.10-befb3xjm4htt2ghk5c4mexramwphtfmm/lib/pkgconfig", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/libxv-1.0.10-befb3xjm4htt2ghk5c4mexramwphtfmm/", ":")

