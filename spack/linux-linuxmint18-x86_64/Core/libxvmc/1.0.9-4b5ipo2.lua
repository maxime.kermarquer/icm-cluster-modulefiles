-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2018-08-31 11:07:46.534792
--
-- libxvmc@1.0.9%gcc@5.4.0 arch=linux-linuxmint18-x86_64 /4b5ipo2
--

whatis([[Name : libxvmc]])
whatis([[Version : 1.0.9]])
whatis([[Short description : X.org libXvMC library.]])

help([[X.org libXvMC library.]])



prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/libxvmc-1.0.9-4b5ipo26matk3ksged5aiu42ns7mn5kr/lib", ":")
prepend_path("LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/libxvmc-1.0.9-4b5ipo26matk3ksged5aiu42ns7mn5kr/lib", ":")
prepend_path("CPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/libxvmc-1.0.9-4b5ipo26matk3ksged5aiu42ns7mn5kr/include", ":")
prepend_path("PKG_CONFIG_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/libxvmc-1.0.9-4b5ipo26matk3ksged5aiu42ns7mn5kr/lib/pkgconfig", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/libxvmc-1.0.9-4b5ipo26matk3ksged5aiu42ns7mn5kr/", ":")

