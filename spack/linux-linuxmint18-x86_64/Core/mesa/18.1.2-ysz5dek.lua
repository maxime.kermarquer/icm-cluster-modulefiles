-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2018-08-31 11:12:56.867001
--
-- mesa@18.1.2%gcc@5.4.0~hwrender~llvm+swrender arch=linux-linuxmint18-x86_64 /ysz5dek
--

whatis([[Name : mesa]])
whatis([[Version : 18.1.2]])
whatis([[Short description : Mesa is an open-source implementation of the OpenGL specification - a system for rendering interactive 3D graphics.]])
whatis([[Configure options : --enable-glx --enable-glx-tls --disable-osmesa --enable-gallium-osmesa --enable-texture-float --disable-xa --disable-dri --disable-dri3 --disable-egl --disable-gbm --disable-xvmc --with-platforms=x11 --with-gallium-drivers=swrast LIBS=-lrt]])

help([[Mesa is an open-source implementation of the OpenGL specification - a
system for rendering interactive 3D graphics.]])



prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/mesa-18.1.2-ysz5dekfnpkjnr3q6xd2obnifbdvttjq/lib", ":")
prepend_path("LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/mesa-18.1.2-ysz5dekfnpkjnr3q6xd2obnifbdvttjq/lib", ":")
prepend_path("CPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/mesa-18.1.2-ysz5dekfnpkjnr3q6xd2obnifbdvttjq/include", ":")
prepend_path("PKG_CONFIG_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/mesa-18.1.2-ysz5dekfnpkjnr3q6xd2obnifbdvttjq/lib/pkgconfig", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/mesa-18.1.2-ysz5dekfnpkjnr3q6xd2obnifbdvttjq/", ":")
prepend_path("CPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/libxml2-2.9.8-55hho2ks47dasihj2dsz3xg67rbssfsr/include/libxml2", ":")

