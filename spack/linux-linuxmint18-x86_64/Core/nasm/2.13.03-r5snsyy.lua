-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2018-09-17 16:07:26.762013
--
-- nasm@2.13.03%gcc@5.4.0 arch=linux-linuxmint18-x86_64 /r5snsyy
--

whatis([[Name : nasm]])
whatis([[Version : 2.13.03]])
whatis([[Short description : NASM (Netwide Assembler) is an 80x86 assembler designed for portability and modularity. It includes a disassembler as well.]])

help([[NASM (Netwide Assembler) is an 80x86 assembler designed for portability
and modularity. It includes a disassembler as well.]])



prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/nasm-2.13.03-r5snsyyewwp5nmvuh5bnz35nxe6vt5ej/bin", ":")
prepend_path("MANPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/nasm-2.13.03-r5snsyyewwp5nmvuh5bnz35nxe6vt5ej/share/man", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/nasm-2.13.03-r5snsyyewwp5nmvuh5bnz35nxe6vt5ej/", ":")

