-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2018-08-28 11:44:05.851912
--
-- ncurses@6.1%gcc@5.4.0~symlinks~termlib arch=linux-linuxmint18-x86_64 /iqv7c2c
--

whatis([[Name : ncurses]])
whatis([[Version : 6.1]])
whatis([[Short description : The ncurses (new curses) library is a free software emulation of curses in System V Release 4.0, and more. It uses terminfo format, supports pads and color and multiple highlights and forms characters and function-key mapping, and has all the other SYSV-curses enhancements over BSD curses.]])

help([[The ncurses (new curses) library is a free software emulation of curses
in System V Release 4.0, and more. It uses terminfo format, supports
pads and color and multiple highlights and forms characters and
function-key mapping, and has all the other SYSV-curses enhancements
over BSD curses.]])



prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/ncurses-6.1-iqv7c2c63zwz7laz7u6nqg3ghlqd2jjv/bin", ":")
prepend_path("MANPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/ncurses-6.1-iqv7c2c63zwz7laz7u6nqg3ghlqd2jjv/share/man", ":")
prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/ncurses-6.1-iqv7c2c63zwz7laz7u6nqg3ghlqd2jjv/lib", ":")
prepend_path("LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/ncurses-6.1-iqv7c2c63zwz7laz7u6nqg3ghlqd2jjv/lib", ":")
prepend_path("CPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/ncurses-6.1-iqv7c2c63zwz7laz7u6nqg3ghlqd2jjv/include", ":")
prepend_path("PKG_CONFIG_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/ncurses-6.1-iqv7c2c63zwz7laz7u6nqg3ghlqd2jjv/lib/pkgconfig", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/ncurses-6.1-iqv7c2c63zwz7laz7u6nqg3ghlqd2jjv/", ":")

