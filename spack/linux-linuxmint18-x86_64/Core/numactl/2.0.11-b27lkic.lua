-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2018-11-05 14:38:21.730696
--
-- numactl@2.0.11%gcc@5.4.0 patches=592f30f7f5f757dfc239ad0ffd39a9a048487ad803c26b419e0f96b8cda08c1a arch=linux-linuxmint18-x86_64 /b27lkic
--

whatis([[Name : numactl]])
whatis([[Version : 2.0.11]])
whatis([[Short description : NUMA support for Linux]])

help([[NUMA support for Linux]])



prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/numactl-2.0.11-b27lkicqdj674czurafax3p7oppbxly2/bin", ":")
prepend_path("MANPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/numactl-2.0.11-b27lkicqdj674czurafax3p7oppbxly2/share/man", ":")
prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/numactl-2.0.11-b27lkicqdj674czurafax3p7oppbxly2/lib", ":")
prepend_path("LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/numactl-2.0.11-b27lkicqdj674czurafax3p7oppbxly2/lib", ":")
prepend_path("CPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/numactl-2.0.11-b27lkicqdj674czurafax3p7oppbxly2/include", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/numactl-2.0.11-b27lkicqdj674czurafax3p7oppbxly2/", ":")

