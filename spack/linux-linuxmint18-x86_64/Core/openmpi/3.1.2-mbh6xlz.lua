-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2018-11-05 15:16:59.832831
--
-- openmpi@3.1.2%gcc@5.4.0~cuda+cxx_exceptions fabrics= ~java~memchecker~pmi schedulers= ~sqlite3~thread_multiple+vt arch=linux-linuxmint18-x86_64 /mbh6xlz
--

whatis([[Name : openmpi]])
whatis([[Version : 3.1.2]])
whatis([[Short description : An open source Message Passing Interface implementation.]])
whatis([[Configure options : --enable-shared --with-wrapper-ldflags= --enable-static --without-pmi --enable-mpi-cxx --with-zlib=/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/zlib-1.2.11-k5hg4kkxiutkfl6n53ogz5wnlbdrsdtf --without-psm --without-psm2 --without-verbs --without-mxm --without-ucx --without-libfabric --without-alps --without-lsf --without-tm --without-slurm --without-sge --without-loadleveler --disable-memchecker --with-hwloc=/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/hwloc-1.11.9-tfrbzvbbbj6253dm5urweertkizawxs7 --disable-java --disable-mpi-java --without-cuda --enable-cxx-exceptions]])

help([[An open source Message Passing Interface implementation. The Open MPI
Project is an open source Message Passing Interface implementation that
is developed and maintained by a consortium of academic, research, and
industry partners. Open MPI is therefore able to combine the expertise,
technologies, and resources from all across the High Performance
Computing community in order to build the best MPI library available.
Open MPI offers advantages for system and software vendors, application
developers and computer science researchers.]])

-- Services provided by the package
family("mpi")

-- Loading this module unlocks the path below unconditionally
prepend_path("MODULEPATH", "/network/lustre/iss01/home/maxime.kermarquer/modules/linux-linuxmint18-x86_64/openmpi/3.1.2-mbh6xlz/Core")

-- Try to load variables into path to see if providers are there

-- Change MODULEPATH based on the result of the tests above

-- Set variables to notify the provider of the new services
setenv("LMOD_MPI_NAME", "openmpi")
setenv("LMOD_MPI_VERSION", "3.1.2-mbh6xlz")


prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/openmpi-3.1.2-mbh6xlz5pw4wjlvruzyqzn4fkiryteaw/bin", ":")
prepend_path("MANPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/openmpi-3.1.2-mbh6xlz5pw4wjlvruzyqzn4fkiryteaw/share/man", ":")
prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/openmpi-3.1.2-mbh6xlz5pw4wjlvruzyqzn4fkiryteaw/lib", ":")
prepend_path("LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/openmpi-3.1.2-mbh6xlz5pw4wjlvruzyqzn4fkiryteaw/lib", ":")
prepend_path("CPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/openmpi-3.1.2-mbh6xlz5pw4wjlvruzyqzn4fkiryteaw/include", ":")
prepend_path("PKG_CONFIG_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/openmpi-3.1.2-mbh6xlz5pw4wjlvruzyqzn4fkiryteaw/lib/pkgconfig", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/openmpi-3.1.2-mbh6xlz5pw4wjlvruzyqzn4fkiryteaw/", ":")
prepend_path("CPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/libxml2-2.9.8-55hho2ks47dasihj2dsz3xg67rbssfsr/include/libxml2", ":")

