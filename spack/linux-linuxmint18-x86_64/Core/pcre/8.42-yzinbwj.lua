-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2018-08-29 10:51:30.890361
--
-- pcre@8.42%gcc@5.4.0~jit+utf arch=linux-linuxmint18-x86_64 /yzinbwj
--

whatis([[Name : pcre]])
whatis([[Version : 8.42]])
whatis([[Short description : The PCRE package contains Perl Compatible Regular Expression libraries. These are useful for implementing regular expression pattern matching using the same syntax and semantics as Perl 5.]])
whatis([[Configure options : --enable-utf --enable-unicode-properties]])

help([[The PCRE package contains Perl Compatible Regular Expression libraries.
These are useful for implementing regular expression pattern matching
using the same syntax and semantics as Perl 5.]])



prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/pcre-8.42-yzinbwjbs4enf4yyhujp7efpry4z6opq/bin", ":")
prepend_path("MANPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/pcre-8.42-yzinbwjbs4enf4yyhujp7efpry4z6opq/share/man", ":")
prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/pcre-8.42-yzinbwjbs4enf4yyhujp7efpry4z6opq/lib", ":")
prepend_path("LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/pcre-8.42-yzinbwjbs4enf4yyhujp7efpry4z6opq/lib", ":")
prepend_path("CPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/pcre-8.42-yzinbwjbs4enf4yyhujp7efpry4z6opq/include", ":")
prepend_path("PKG_CONFIG_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/pcre-8.42-yzinbwjbs4enf4yyhujp7efpry4z6opq/lib/pkgconfig", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/pcre-8.42-yzinbwjbs4enf4yyhujp7efpry4z6opq/", ":")

