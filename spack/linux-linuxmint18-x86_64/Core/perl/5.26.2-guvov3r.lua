-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2018-08-28 11:46:07.676711
--
-- perl@5.26.2%gcc@5.4.0+cpanm patches=0eac10ed90aeb0459ad8851f88081d439a4e41978e586ec743069e8b059370ac +shared+threads arch=linux-linuxmint18-x86_64 /guvov3r
--

whatis([[Name : perl]])
whatis([[Version : 5.26.2]])
whatis([[Short description : Perl 5 is a highly capable, feature-rich programming language with over 27 years of development.]])
whatis([[Configure options : -des -Dprefix=/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/perl-5.26.2-guvov3rwv2otb37lyfzoxb5g5w4t7q5g -Dlocincpth=/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/gdbm-1.14.1-7pvsvgk4yv6y3ezc52pajdvx2vffy3qi/include -Dloclibpth=/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/gdbm-1.14.1-7pvsvgk4yv6y3ezc52pajdvx2vffy3qi/lib -Accflags=-DAPPLLIB_EXP=\"/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/perl-5.26.2-guvov3rwv2otb37lyfzoxb5g5w4t7q5g/lib/perl5\" -Duseshrplib -Dusethreads]])

help([[Perl 5 is a highly capable, feature-rich programming language with over
27 years of development.]])



prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/perl-5.26.2-guvov3rwv2otb37lyfzoxb5g5w4t7q5g/bin", ":")
prepend_path("MANPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/perl-5.26.2-guvov3rwv2otb37lyfzoxb5g5w4t7q5g/man", ":")
prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/perl-5.26.2-guvov3rwv2otb37lyfzoxb5g5w4t7q5g/lib", ":")
prepend_path("LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/perl-5.26.2-guvov3rwv2otb37lyfzoxb5g5w4t7q5g/lib", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/perl-5.26.2-guvov3rwv2otb37lyfzoxb5g5w4t7q5g/", ":")

