-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2018-09-20 15:18:09.259437
--
-- pixman@0.34.0%gcc@5.4.0 arch=linux-linuxmint18-x86_64 /mq4ag34
--

whatis([[Name : pixman]])
whatis([[Version : 0.34.0]])
whatis([[Short description : The Pixman package contains a library that provides low-level pixel manipulation features such as image compositing and trapezoid rasterization.]])
whatis([[Configure options : --enable-libpng --disable-gtk]])

help([[The Pixman package contains a library that provides low-level pixel
manipulation features such as image compositing and trapezoid
rasterization.]])



prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/pixman-0.34.0-mq4ag34lhtv7ssxkxqp2hq3t34nxk2v7/lib", ":")
prepend_path("LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/pixman-0.34.0-mq4ag34lhtv7ssxkxqp2hq3t34nxk2v7/lib", ":")
prepend_path("CPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/pixman-0.34.0-mq4ag34lhtv7ssxkxqp2hq3t34nxk2v7/include", ":")
prepend_path("PKG_CONFIG_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/pixman-0.34.0-mq4ag34lhtv7ssxkxqp2hq3t34nxk2v7/lib/pkgconfig", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/pixman-0.34.0-mq4ag34lhtv7ssxkxqp2hq3t34nxk2v7/", ":")

