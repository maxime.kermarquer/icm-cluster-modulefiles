-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2018-08-28 11:42:53.573830
--
-- pkgconf@1.4.2%gcc@5.4.0 arch=linux-linuxmint18-x86_64 /nripncd
--

whatis([[Name : pkgconf]])
whatis([[Version : 1.4.2]])
whatis([[Short description : pkgconf is a program which helps to configure compiler and linker flags for development frameworks. It is similar to pkg-config from freedesktop.org, providing additional functionality while also maintaining compatibility.]])

help([[pkgconf is a program which helps to configure compiler and linker flags
for development frameworks. It is similar to pkg-config from
freedesktop.org, providing additional functionality while also
maintaining compatibility.]])



prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/pkgconf-1.4.2-nripncddsxvqtx4t4tutddfkibqdoiej/bin", ":")
prepend_path("MANPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/pkgconf-1.4.2-nripncddsxvqtx4t4tutddfkibqdoiej/share/man", ":")
prepend_path("ACLOCAL_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/pkgconf-1.4.2-nripncddsxvqtx4t4tutddfkibqdoiej/share/aclocal", ":")
prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/pkgconf-1.4.2-nripncddsxvqtx4t4tutddfkibqdoiej/lib", ":")
prepend_path("LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/pkgconf-1.4.2-nripncddsxvqtx4t4tutddfkibqdoiej/lib", ":")
prepend_path("CPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/pkgconf-1.4.2-nripncddsxvqtx4t4tutddfkibqdoiej/include", ":")
prepend_path("PKG_CONFIG_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/pkgconf-1.4.2-nripncddsxvqtx4t4tutddfkibqdoiej/lib/pkgconfig", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/pkgconf-1.4.2-nripncddsxvqtx4t4tutddfkibqdoiej/", ":")

