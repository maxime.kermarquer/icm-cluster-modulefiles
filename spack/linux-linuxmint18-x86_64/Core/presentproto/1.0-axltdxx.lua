-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2018-08-31 11:07:50.150913
--
-- presentproto@1.0%gcc@5.4.0 arch=linux-linuxmint18-x86_64 /axltdxx
--

whatis([[Name : presentproto]])
whatis([[Version : 1.0]])
whatis([[Short description : Present protocol specification and Xlib/Xserver headers.]])

help([[Present protocol specification and Xlib/Xserver headers.]])



prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/presentproto-1.0-axltdxx3zaj7ofmtbdjn3tadr452rqlw/lib", ":")
prepend_path("LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/presentproto-1.0-axltdxx3zaj7ofmtbdjn3tadr452rqlw/lib", ":")
prepend_path("CPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/presentproto-1.0-axltdxx3zaj7ofmtbdjn3tadr452rqlw/include", ":")
prepend_path("PKG_CONFIG_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/presentproto-1.0-axltdxx3zaj7ofmtbdjn3tadr452rqlw/lib/pkgconfig", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/presentproto-1.0-axltdxx3zaj7ofmtbdjn3tadr452rqlw/", ":")

