-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2018-08-31 11:10:29.257208
--
-- py-mako@1.0.4%gcc@5.4.0 arch=linux-linuxmint18-x86_64 /trl5l2g
--

whatis([[Name : py-mako]])
whatis([[Version : 1.0.4]])
whatis([[Short description : A super-fast templating language that borrows the best ideas from the existing templating languages.]])

help([[A super-fast templating language that borrows the best ideas from the
existing templating languages.]])



prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/py-mako-1.0.4-trl5l2g2zpw5sboyfvvz3qb6dudncc6t/bin", ":")
prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/py-mako-1.0.4-trl5l2g2zpw5sboyfvvz3qb6dudncc6t/lib", ":")
prepend_path("LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/py-mako-1.0.4-trl5l2g2zpw5sboyfvvz3qb6dudncc6t/lib", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/py-mako-1.0.4-trl5l2g2zpw5sboyfvvz3qb6dudncc6t/", ":")
prepend_path("PYTHONPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/py-mako-1.0.4-trl5l2g2zpw5sboyfvvz3qb6dudncc6t/lib/python2.7/site-packages", ":")

