-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2018-08-31 11:10:27.069804
--
-- py-markupsafe@1.0%gcc@5.4.0 arch=linux-linuxmint18-x86_64 /uzu7pap
--

whatis([[Name : py-markupsafe]])
whatis([[Version : 1.0]])
whatis([[Short description : MarkupSafe is a library for Python that implements a unicode string that is aware of HTML escaping rules and can be used to implement automatic string escaping. It is used by Jinja 2, the Mako templating engine, the Pylons web framework and many more.]])

help([[MarkupSafe is a library for Python that implements a unicode string that
is aware of HTML escaping rules and can be used to implement automatic
string escaping. It is used by Jinja 2, the Mako templating engine, the
Pylons web framework and many more.]])



prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/py-markupsafe-1.0-uzu7pap6veolfas5o5yr35n3delnqbq5/lib", ":")
prepend_path("LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/py-markupsafe-1.0-uzu7pap6veolfas5o5yr35n3delnqbq5/lib", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/py-markupsafe-1.0-uzu7pap6veolfas5o5yr35n3delnqbq5/", ":")
prepend_path("PYTHONPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/py-markupsafe-1.0-uzu7pap6veolfas5o5yr35n3delnqbq5/lib/python2.7/site-packages", ":")

