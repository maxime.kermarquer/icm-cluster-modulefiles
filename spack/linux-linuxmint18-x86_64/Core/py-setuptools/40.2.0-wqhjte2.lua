-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2018-08-31 11:10:24.772444
--
-- py-setuptools@40.2.0%gcc@5.4.0 arch=linux-linuxmint18-x86_64 /wqhjte2
--

whatis([[Name : py-setuptools]])
whatis([[Version : 40.2.0]])
whatis([[Short description : A Python utility that aids in the process of downloading, building, upgrading, installing, and uninstalling Python packages.]])

help([[A Python utility that aids in the process of downloading, building,
upgrading, installing, and uninstalling Python packages.]])



prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/py-setuptools-40.2.0-wqhjte2sol7rvoby6pov2gzupyykg3ig/bin", ":")
prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/py-setuptools-40.2.0-wqhjte2sol7rvoby6pov2gzupyykg3ig/lib", ":")
prepend_path("LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/py-setuptools-40.2.0-wqhjte2sol7rvoby6pov2gzupyykg3ig/lib", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/py-setuptools-40.2.0-wqhjte2sol7rvoby6pov2gzupyykg3ig/", ":")
prepend_path("PYTHONPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/py-setuptools-40.2.0-wqhjte2sol7rvoby6pov2gzupyykg3ig/lib/python2.7/site-packages", ":")

