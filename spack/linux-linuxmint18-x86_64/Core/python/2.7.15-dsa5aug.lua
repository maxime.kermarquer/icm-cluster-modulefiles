-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2018-08-31 11:10:20.807363
--
-- python@2.7.15%gcc@5.4.0+dbm~optimizations patches=123082ab3483ded78e86d7c809e98a804b3465b4683c96bd79a2fd799f572244 +pic+pythoncmd+shared~tk~ucs4 arch=linux-linuxmint18-x86_64 /dsa5aug
--

whatis([[Name : python]])
whatis([[Version : 2.7.15]])
whatis([[Short description : The Python programming language.]])
whatis([[Configure options : --with-threads CPPFLAGS=-I/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/bzip2-1.0.6-q6vnl3rwqewixycv47ax32li3bz2e45d/include -I/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/ncurses-6.1-iqv7c2c63zwz7laz7u6nqg3ghlqd2jjv/include -I/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/zlib-1.2.11-k5hg4kkxiutkfl6n53ogz5wnlbdrsdtf/include -I/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/openssl-1.0.2o-5q4t4qkqw2nkza7pwqx4jbn42mpnxthe/include -I/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/sqlite-3.23.1-xybn23mmbstcfmbxlpdqkgb2v3d2fvlv/include -I/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/readline-7.0-fkte7wk6pgxtkwiwrzlcmomii3kzqzj6/include -I/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/gdbm-1.14.1-7pvsvgk4yv6y3ezc52pajdvx2vffy3qi/include LDFLAGS=-L/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/bzip2-1.0.6-q6vnl3rwqewixycv47ax32li3bz2e45d/lib -L/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/ncurses-6.1-iqv7c2c63zwz7laz7u6nqg3ghlqd2jjv/lib -L/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/zlib-1.2.11-k5hg4kkxiutkfl6n53ogz5wnlbdrsdtf/lib -L/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/openssl-1.0.2o-5q4t4qkqw2nkza7pwqx4jbn42mpnxthe/lib -L/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/sqlite-3.23.1-xybn23mmbstcfmbxlpdqkgb2v3d2fvlv/lib -L/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/readline-7.0-fkte7wk6pgxtkwiwrzlcmomii3kzqzj6/lib -L/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/gdbm-1.14.1-7pvsvgk4yv6y3ezc52pajdvx2vffy3qi/lib --enable-shared CFLAGS=-fPIC]])

help([[The Python programming language.]])



prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/python-2.7.15-dsa5aughbobmz7acz4xgzvfr74fcmvsi/bin", ":")
prepend_path("MANPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/python-2.7.15-dsa5aughbobmz7acz4xgzvfr74fcmvsi/share/man", ":")
prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/python-2.7.15-dsa5aughbobmz7acz4xgzvfr74fcmvsi/lib", ":")
prepend_path("LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/python-2.7.15-dsa5aughbobmz7acz4xgzvfr74fcmvsi/lib", ":")
prepend_path("CPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/python-2.7.15-dsa5aughbobmz7acz4xgzvfr74fcmvsi/include", ":")
prepend_path("PKG_CONFIG_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/python-2.7.15-dsa5aughbobmz7acz4xgzvfr74fcmvsi/lib/pkgconfig", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/python-2.7.15-dsa5aughbobmz7acz4xgzvfr74fcmvsi/", ":")

