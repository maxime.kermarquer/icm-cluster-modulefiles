-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2018-09-17 16:44:55.279189
--
-- qt@5.10.0%gcc@5.4.0~dbus~examples~gtk~krellpatch~opengl patches=7f34d48d2faaa108dc3fcc47187af1ccd1d37ee0f931b42597b820f03a99864c,c52f72dac7fdff5a296467536cc9ea024d78f94b49903286395f53fd0eb66e5e ~phonon~webkit arch=linux-linuxmint18-x86_64 /njwpo33
--

whatis([[Name : qt]])
whatis([[Version : 5.10.0]])
whatis([[Short description : Qt is a comprehensive cross-platform C++ application framework.]])

help([[Qt is a comprehensive cross-platform C++ application framework.]])



prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/qt-5.10.0-njwpo332fwpvkn5t74a2ap47jxhwpgyw/bin", ":")
prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/qt-5.10.0-njwpo332fwpvkn5t74a2ap47jxhwpgyw/lib", ":")
prepend_path("LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/qt-5.10.0-njwpo332fwpvkn5t74a2ap47jxhwpgyw/lib", ":")
prepend_path("CPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/qt-5.10.0-njwpo332fwpvkn5t74a2ap47jxhwpgyw/include", ":")
prepend_path("PKG_CONFIG_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/qt-5.10.0-njwpo332fwpvkn5t74a2ap47jxhwpgyw/lib/pkgconfig", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/qt-5.10.0-njwpo332fwpvkn5t74a2ap47jxhwpgyw/", ":")
prepend_path("CPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/libxml2-2.9.8-55hho2ks47dasihj2dsz3xg67rbssfsr/include/libxml2", ":")
prepend_path("CPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/freetype-2.7.1-4z3kpzdhhm623mmmrcnphm22rn4mi6o5/include/freetype2", ":")
setenv("QTDIR", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/qt-5.10.0-njwpo332fwpvkn5t74a2ap47jxhwpgyw")

