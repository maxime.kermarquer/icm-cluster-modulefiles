-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2018-08-28 11:44:15.899264
--
-- readline@7.0%gcc@5.4.0 arch=linux-linuxmint18-x86_64 /fkte7wk
--

whatis([[Name : readline]])
whatis([[Version : 7.0]])
whatis([[Short description : The GNU Readline library provides a set of functions for use by applications that allow users to edit command lines as they are typed in. Both Emacs and vi editing modes are available. The Readline library includes additional functions to maintain a list of previously-entered command lines, to recall and perhaps reedit those lines, and perform csh-like history expansion on previous commands.]])

help([[The GNU Readline library provides a set of functions for use by
applications that allow users to edit command lines as they are typed
in. Both Emacs and vi editing modes are available. The Readline library
includes additional functions to maintain a list of previously-entered
command lines, to recall and perhaps reedit those lines, and perform
csh-like history expansion on previous commands.]])



prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/readline-7.0-fkte7wk6pgxtkwiwrzlcmomii3kzqzj6/bin", ":")
prepend_path("MANPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/readline-7.0-fkte7wk6pgxtkwiwrzlcmomii3kzqzj6/share/man", ":")
prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/readline-7.0-fkte7wk6pgxtkwiwrzlcmomii3kzqzj6/lib", ":")
prepend_path("LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/readline-7.0-fkte7wk6pgxtkwiwrzlcmomii3kzqzj6/lib", ":")
prepend_path("CPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/readline-7.0-fkte7wk6pgxtkwiwrzlcmomii3kzqzj6/include", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/readline-7.0-fkte7wk6pgxtkwiwrzlcmomii3kzqzj6/", ":")

