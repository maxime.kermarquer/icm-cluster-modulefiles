-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2018-08-30 10:29:09.590682
--
-- rsync@3.1.3%gcc@5.4.0 arch=linux-linuxmint18-x86_64 /6vlqmrv
--

whatis([[Name : rsync]])
whatis([[Version : 3.1.3]])
whatis([[Short description : An open source utility that provides fast incremental file transfer.]])

help([[An open source utility that provides fast incremental file transfer.]])



prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/rsync-3.1.3-6vlqmrvud3jlqicjgxa2734rhtadwmff/bin", ":")
prepend_path("MANPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/rsync-3.1.3-6vlqmrvud3jlqicjgxa2734rhtadwmff/share/man", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/rsync-3.1.3-6vlqmrvud3jlqicjgxa2734rhtadwmff/", ":")

