-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2018-08-31 11:08:31.866840
--
-- sqlite@3.23.1%gcc@5.4.0~functions arch=linux-linuxmint18-x86_64 /xybn23m
--

whatis([[Name : sqlite]])
whatis([[Version : 3.23.1]])
whatis([[Short description : SQLite3 is an SQL database engine in a C library. Programs that link the SQLite3 library can have SQL database access without running a separate RDBMS process. ]])

help([[SQLite3 is an SQL database engine in a C library. Programs that link the
SQLite3 library can have SQL database access without running a separate
RDBMS process.]])



prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/sqlite-3.23.1-xybn23mmbstcfmbxlpdqkgb2v3d2fvlv/bin", ":")
prepend_path("MANPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/sqlite-3.23.1-xybn23mmbstcfmbxlpdqkgb2v3d2fvlv/share/man", ":")
prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/sqlite-3.23.1-xybn23mmbstcfmbxlpdqkgb2v3d2fvlv/lib", ":")
prepend_path("LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/sqlite-3.23.1-xybn23mmbstcfmbxlpdqkgb2v3d2fvlv/lib", ":")
prepend_path("CPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/sqlite-3.23.1-xybn23mmbstcfmbxlpdqkgb2v3d2fvlv/include", ":")
prepend_path("PKG_CONFIG_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/sqlite-3.23.1-xybn23mmbstcfmbxlpdqkgb2v3d2fvlv/lib/pkgconfig", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/sqlite-3.23.1-xybn23mmbstcfmbxlpdqkgb2v3d2fvlv/", ":")

