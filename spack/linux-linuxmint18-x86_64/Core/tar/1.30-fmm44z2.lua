-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2018-08-28 11:50:12.026255
--
-- tar@1.30%gcc@5.4.0 arch=linux-linuxmint18-x86_64 /fmm44z2
--

whatis([[Name : tar]])
whatis([[Version : 1.30]])
whatis([[Short description : GNU Tar provides the ability to create tar archives, as well as various other kinds of manipulation.]])

help([[GNU Tar provides the ability to create tar archives, as well as various
other kinds of manipulation.]])



prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/tar-1.30-fmm44z2gxndvl2uhinjwnmyoiapimu3f/bin", ":")
prepend_path("MANPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/tar-1.30-fmm44z2gxndvl2uhinjwnmyoiapimu3f/share/man", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/tar-1.30-fmm44z2gxndvl2uhinjwnmyoiapimu3f/", ":")

