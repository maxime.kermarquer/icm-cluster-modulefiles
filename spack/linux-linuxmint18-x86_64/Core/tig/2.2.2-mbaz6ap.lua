-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2018-09-17 10:33:13.205609
--
-- tig@2.2.2%gcc@5.4.0 arch=linux-linuxmint18-x86_64 /mbaz6ap
--

whatis([[Name : tig]])
whatis([[Version : 2.2.2]])
whatis([[Short description : Text-mode interface for git]])

help([[Text-mode interface for git]])



prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/tig-2.2.2-mbaz6aprhwc56rgs7mrga7vdc3dlseag/bin", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/tig-2.2.2-mbaz6aprhwc56rgs7mrga7vdc3dlseag/", ":")

