-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2018-08-31 11:03:25.252096
--
-- util-macros@1.19.1%gcc@5.4.0 arch=linux-linuxmint18-x86_64 /nsd2led
--

whatis([[Name : util-macros]])
whatis([[Version : 1.19.1]])
whatis([[Short description : This is a set of autoconf macros used by the configure.ac scripts in other Xorg modular packages, and is needed to generate new versions of their configure scripts with autoconf.]])

help([[This is a set of autoconf macros used by the configure.ac scripts in
other Xorg modular packages, and is needed to generate new versions of
their configure scripts with autoconf.]])



prepend_path("ACLOCAL_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/util-macros-1.19.1-nsd2led5j7jiejwfxivr3lssfpwkn2lg/share/aclocal", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/util-macros-1.19.1-nsd2led5j7jiejwfxivr3lssfpwkn2lg/", ":")

