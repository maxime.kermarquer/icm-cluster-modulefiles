-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2018-08-31 11:07:32.480036
--
-- videoproto@2.3.3%gcc@5.4.0 arch=linux-linuxmint18-x86_64 /dx5df36
--

whatis([[Name : videoproto]])
whatis([[Version : 2.3.3]])
whatis([[Short description : X Video Extension.]])

help([[X Video Extension. This extension provides a protocol for a video output
mechanism, mainly to rescale video playback in the video controller
hardware.]])



prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/videoproto-2.3.3-dx5df362zx2poifolcdqtgfwq77azwgy/lib", ":")
prepend_path("LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/videoproto-2.3.3-dx5df362zx2poifolcdqtgfwq77azwgy/lib", ":")
prepend_path("CPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/videoproto-2.3.3-dx5df362zx2poifolcdqtgfwq77azwgy/include", ":")
prepend_path("PKG_CONFIG_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/videoproto-2.3.3-dx5df362zx2poifolcdqtgfwq77azwgy/lib/pkgconfig", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/videoproto-2.3.3-dx5df362zx2poifolcdqtgfwq77azwgy/", ":")

