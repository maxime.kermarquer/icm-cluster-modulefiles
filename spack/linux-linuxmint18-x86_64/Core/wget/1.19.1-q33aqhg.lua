-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2018-08-28 11:54:23.102698
--
-- wget@1.19.1%gcc@5.4.0~libpsl~pcre~python ssl=openssl +zlib arch=linux-linuxmint18-x86_64 /q33aqhg
--

whatis([[Name : wget]])
whatis([[Version : 1.19.1]])
whatis([[Short description : GNU Wget is a free software package for retrieving files using HTTP, HTTPS and FTP, the most widely-used Internet protocols. It is a non-interactive commandline tool, so it may easily be called from scripts, cron jobs, terminals without X-Windows support, etc.]])
whatis([[Configure options : --with-ssl=openssl --with-zlib --without-libpsl --disable-pcre --disable-valgrind-tests]])

help([[GNU Wget is a free software package for retrieving files using HTTP,
HTTPS and FTP, the most widely-used Internet protocols. It is a non-
interactive commandline tool, so it may easily be called from scripts,
cron jobs, terminals without X-Windows support, etc.]])



prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/wget-1.19.1-q33aqhgjfa7prxjpax2tnbmzcjx7p43j/bin", ":")
prepend_path("MANPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/wget-1.19.1-q33aqhgjfa7prxjpax2tnbmzcjx7p43j/share/man", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/wget-1.19.1-q33aqhgjfa7prxjpax2tnbmzcjx7p43j/", ":")

