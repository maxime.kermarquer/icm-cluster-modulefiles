-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2018-08-31 11:05:03.411038
--
-- xcb-proto@1.13%gcc@5.4.0 arch=linux-linuxmint18-x86_64 /ulbo7nz
--

whatis([[Name : xcb-proto]])
whatis([[Version : 1.13]])
whatis([[Short description : xcb-proto provides the XML-XCB protocol descriptions that libxcb uses to generate the majority of its code and API.]])

help([[xcb-proto provides the XML-XCB protocol descriptions that libxcb uses to
generate the majority of its code and API.]])



prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/xcb-proto-1.13-ulbo7nzjhmt4ypgtrx5ytnp4n4ldoun7/lib", ":")
prepend_path("LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/xcb-proto-1.13-ulbo7nzjhmt4ypgtrx5ytnp4n4ldoun7/lib", ":")
prepend_path("PKG_CONFIG_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/xcb-proto-1.13-ulbo7nzjhmt4ypgtrx5ytnp4n4ldoun7/lib/pkgconfig", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/xcb-proto-1.13-ulbo7nzjhmt4ypgtrx5ytnp4n4ldoun7/", ":")

