-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2018-08-31 11:05:39.489781
--
-- xextproto@7.3.0%gcc@5.4.0 arch=linux-linuxmint18-x86_64 /v4yd623
--

whatis([[Name : xextproto]])
whatis([[Version : 7.3.0]])
whatis([[Short description : X Protocol Extensions.]])

help([[X Protocol Extensions.]])



prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/xextproto-7.3.0-v4yd623rlplrucjauc32z6ibtby2sii7/lib", ":")
prepend_path("LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/xextproto-7.3.0-v4yd623rlplrucjauc32z6ibtby2sii7/lib", ":")
prepend_path("CPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/xextproto-7.3.0-v4yd623rlplrucjauc32z6ibtby2sii7/include", ":")
prepend_path("PKG_CONFIG_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/xextproto-7.3.0-v4yd623rlplrucjauc32z6ibtby2sii7/lib/pkgconfig", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/xextproto-7.3.0-v4yd623rlplrucjauc32z6ibtby2sii7/", ":")

