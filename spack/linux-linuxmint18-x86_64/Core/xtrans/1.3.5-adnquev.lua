-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2018-08-31 11:05:43.431149
--
-- xtrans@1.3.5%gcc@5.4.0 arch=linux-linuxmint18-x86_64 /adnquev
--

whatis([[Name : xtrans]])
whatis([[Version : 1.3.5]])
whatis([[Short description : xtrans is a library of code that is shared among various X packages to handle network protocol transport in a modular fashion, allowing a single place to add new transport types. It is used by the X server, libX11, libICE, the X font server, and related components.]])

help([[xtrans is a library of code that is shared among various X packages to
handle network protocol transport in a modular fashion, allowing a
single place to add new transport types. It is used by the X server,
libX11, libICE, the X font server, and related components.]])



prepend_path("ACLOCAL_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/xtrans-1.3.5-adnquevsuwx6qwtfqh4dxfhimsrvx2a2/share/aclocal", ":")
prepend_path("CPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/xtrans-1.3.5-adnquevsuwx6qwtfqh4dxfhimsrvx2a2/include", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/xtrans-1.3.5-adnquevsuwx6qwtfqh4dxfhimsrvx2a2/", ":")

