-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2018-08-28 11:49:14.702055
--
-- xz@5.2.4%gcc@5.4.0 arch=linux-linuxmint18-x86_64 /hmunxeh
--

whatis([[Name : xz]])
whatis([[Version : 5.2.4]])
whatis([[Short description : XZ Utils is free general-purpose data compression software with high compression ratio. XZ Utils were written for POSIX-like systems, but also work on some not-so-POSIX systems. XZ Utils are the successor to LZMA Utils.]])

help([[XZ Utils is free general-purpose data compression software with high
compression ratio. XZ Utils were written for POSIX-like systems, but
also work on some not-so-POSIX systems. XZ Utils are the successor to
LZMA Utils.]])



prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/xz-5.2.4-hmunxehevaujzgmiycq3vjtfn23gwzv4/bin", ":")
prepend_path("MANPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/xz-5.2.4-hmunxehevaujzgmiycq3vjtfn23gwzv4/share/man", ":")
prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/xz-5.2.4-hmunxehevaujzgmiycq3vjtfn23gwzv4/lib", ":")
prepend_path("LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/xz-5.2.4-hmunxehevaujzgmiycq3vjtfn23gwzv4/lib", ":")
prepend_path("CPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/xz-5.2.4-hmunxehevaujzgmiycq3vjtfn23gwzv4/include", ":")
prepend_path("PKG_CONFIG_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/xz-5.2.4-hmunxehevaujzgmiycq3vjtfn23gwzv4/lib/pkgconfig", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/xz-5.2.4-hmunxehevaujzgmiycq3vjtfn23gwzv4/", ":")

