-- -*- lua -*-
-- Module file created by spack (https://github.com/spack/spack) on 2018-11-05 15:21:55.312870
--
-- matio@1.5.9%gcc@5.4.0+hdf5+shared+zlib arch=linux-linuxmint18-x86_64 /rox52pu
--

whatis([[Name : matio]])
whatis([[Version : 1.5.9]])
whatis([[Short description : matio is an C library for reading and writing Matlab MAT files]])
whatis([[Configure options : --with-zlib=/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/zlib-1.2.11-k5hg4kkxiutkfl6n53ogz5wnlbdrsdtf --with-hdf5=/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/hdf5-1.10.2-va2ep546wcruwbnxqayrcff7ix63xs4s]])

help([[matio is an C library for reading and writing Matlab MAT files]])



prepend_path("PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/matio-1.5.9-rox52pucr4lpyif2tg6wq35ltwvv3rs5/bin", ":")
prepend_path("MANPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/matio-1.5.9-rox52pucr4lpyif2tg6wq35ltwvv3rs5/share/man", ":")
prepend_path("LD_LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/matio-1.5.9-rox52pucr4lpyif2tg6wq35ltwvv3rs5/lib", ":")
prepend_path("LIBRARY_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/matio-1.5.9-rox52pucr4lpyif2tg6wq35ltwvv3rs5/lib", ":")
prepend_path("CPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/matio-1.5.9-rox52pucr4lpyif2tg6wq35ltwvv3rs5/include", ":")
prepend_path("PKG_CONFIG_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/matio-1.5.9-rox52pucr4lpyif2tg6wq35ltwvv3rs5/lib/pkgconfig", ":")
prepend_path("CMAKE_PREFIX_PATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/matio-1.5.9-rox52pucr4lpyif2tg6wq35ltwvv3rs5/", ":")
prepend_path("CPATH", "/network/lustre/iss01/apps/software/scit/spack_installs/linux-linuxmint18-x86_64/gcc-5.4.0/libxml2-2.9.8-55hho2ks47dasihj2dsz3xg67rbssfsr/include/libxml2", ":")

