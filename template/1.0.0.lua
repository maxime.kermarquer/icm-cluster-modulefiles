-- -*- lua -*-
-- Date:
-- Author : Maxime KERMARQUER
--
--

-- Module description
whatis([[Name : ]])
whatis([[Version : ]])
whatis([[Short description : ]])
whatis([[Configure options : ]])
help([[ ]])


-- Execute a command
execute {cmd="source deactivate deformetrica",modeA={"unload"}} 

-- For module dependencies
if(not isloaded("modulefile")) then
        load("modulefile")
end

depends_on("modulefile")

-- Set up environment variables
prepend_path("PATH", "bin", ":")
prepend_path("LD_LIBRARY_PATH", "lib", ":")
prepend_path("LIBRARY_PATH", "lib", ":")
prepend_path("CPATH", "include", ":")
prepend_path("MANPATH", "share", ":")
prepend_path("CMAKE_PREFIX_PATH", "", ":")

